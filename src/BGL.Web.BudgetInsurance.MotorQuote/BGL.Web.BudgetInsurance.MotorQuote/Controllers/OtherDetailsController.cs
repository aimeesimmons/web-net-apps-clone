﻿using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Controllers
{
    public class OtherDetailsController : SecuredController
    {
        public ActionResult PaymentsRegularity()
        {
            return View();
        }

        public ActionResult AndFinally()
        {
            return View();
        }
    }
}