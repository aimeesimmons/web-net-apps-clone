﻿using System.Web;
using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Controllers
{
    public class LegalController : SecuredController
    {
        public ActionResult PostPriceImportantInformation()
        {
            return PartialView();
        }

        public ActionResult PrePriceImportantInformation()
        {
            return PartialView();
        }

        public ActionResult Direct()
        {
            return View();
        }

        public ActionResult DirectPrePriceLegal()
        {
            return View();
        }

        public ActionResult AboutUs()
        {
            return PartialView();
        }

        public ActionResult QuoteAcceptanceInformation()
        {
            return PartialView();
        }
    }
}