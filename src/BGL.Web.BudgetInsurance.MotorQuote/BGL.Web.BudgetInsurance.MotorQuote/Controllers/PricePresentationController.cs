﻿using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Controllers
{
    public class PricePresentationController : SecuredController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WhatsIncluded()
        {
            return View();
        }

        public ActionResult WhatsIncludedMVT()
        {
            return View();
        }

        public ActionResult AggReferrer()
        {
            return View();
        }
    }
}