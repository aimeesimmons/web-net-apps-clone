﻿using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Controllers
{
    public class YourCoverController : SecuredController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MainDriver()
        {
            return View();
        }

        public ActionResult CoverLevel()
        {
            return View();
        }

        public ActionResult CoverExcesses()
        {
            return View();
        }

        public ActionResult AnnualMileage()
        {
            return View();
        }

        public ActionResult RegisteredKeeperRelationship()
        {
            return View();
        }

        public ActionResult AboutRegisteredKeeper()
        {
            return View();
        }

        public ActionResult RegisteredKeeperMaritalStatus()
        {
            return View();
        }

        public ActionResult RegisteredKeeperSameAddress()
        {
            return View();
        }

        public ActionResult VehicleUse()
        {
            return View();
        }

        public ActionResult NcdYears()
        {
            return View();
        }

        public ActionResult NcdSource()
        {
            return View();
        }

        public ActionResult OtherVehicles()
        {
            return View();
        }

        public ActionResult CoverStartDate()
        {
            return View();
        }

        public ActionResult YearsBusinessEstablished()
        {
            return View();
        }

        public ActionResult CarriageOfDangerousGoods()
        {
            return View();
        }
    }
}