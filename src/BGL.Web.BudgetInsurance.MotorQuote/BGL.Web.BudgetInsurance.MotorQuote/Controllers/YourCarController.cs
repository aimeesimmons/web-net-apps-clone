﻿using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Controllers
{
    public class YourCarController : SecuredController
    {
        public ActionResult VehicleLookup()
        {
            return View();
        }

        public ActionResult VehicleDetails()
        {
            return View();
        }

        public ActionResult OvernightParking()
        {
            return View();
        }
    }
}