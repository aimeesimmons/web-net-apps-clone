﻿using System.Web.Mvc;

namespace BGL.Web.BudgetInsurance.MotorQuote.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UnableToQuoteOnline()
        {
            return View();
        }

        public ActionResult NoDocumentFound()
        {
            return View();
        }

        public ActionResult UnderwritingTimeOut()
        {
            return View("Index");
        }

        public ActionResult RecaptchaBot()
        {
            return View("Index");
        }
    }
}