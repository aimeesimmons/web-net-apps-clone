﻿namespace BGL.Web.BudgetInsurance.MotorQuote.Constants
{
    public class BrandConstants
    {
        public const string VanErrorMessageUrl = "https://www.budgetinsurance.com/Van/B139/NLE";

        public const string CarErrorMessageUrl = "https://www.budgetinsurance.com/Car/B139/NLE";

        public const string WrongVehicleTypeForVanInsValidationErrorMessage = "We have matched your vehicle to a van. Please click &lt;a href=\"{0}\"&gt;here&lt;/a&gt; to get a van insurance quote.";

        public const string WrongVehicleTypeForCarInsValidationErrorMessage = "We have matched your vehicle to a car. Please click &lt;a href=\"{0}\"&gt;here&lt;/a&gt; to get a car insurance quote. ";

        public const string HoustonContactCentrePhoneNumber = "0344 871 0099";

        public const string ContactCentrePhoneNumber = "0344 412 2118";

        public const string ContactCentrePhoneNumberVan = "0344 871 5035";
    }
}