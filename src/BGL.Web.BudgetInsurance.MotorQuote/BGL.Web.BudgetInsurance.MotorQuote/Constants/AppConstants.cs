﻿namespace BGL.Web.BudgetInsurance.MotorQuote.Constants
{
    public class AppConstants
    {
        public const string ProductCar = "Car";

        public const string ProductVan = "Van";
    }
}