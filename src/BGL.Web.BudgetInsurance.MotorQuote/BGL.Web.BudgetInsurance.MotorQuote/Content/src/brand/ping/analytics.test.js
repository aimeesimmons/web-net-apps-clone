﻿window.siteId = '31631473';
window.NinaVars = window.NinaVars || {};
window.NinaVars.isSelfService = true;
window.NinaVars.isSales = false;
window.NinaVars.preprod = true;
window.livepersonImgPath = "https://lp.sabio.co.uk/bis";

function gtmStart(containerId) {
    window['dataLayer'] = window['dataLayer'] || []; 
    window['dataLayer'].push({
        'gtm.start':
            new Date().getTime(), event: 'gtm.js'
    });

    var script = document.getElementsByTagName('script')[0];
    var newScript = document.createElement('script');
    newScript.async = true;
    newScript.src = 'https://www.googletagmanager.com/gtm.js?id=' + containerId;

    for (var i = 0; i < document.getElementsByTagName('script').length; i++) {
        if (i > 0) script = document.getElementsByTagName('script')[i];
        if ((script.outerHTML.indexOf("cdn.cookielaw.org") === -1) && (script.outerHTML.indexOf("Optanon") === -1)) {
            break;
        }
    }
    script.parentNode.insertBefore(newScript, script);
}

var firstScript = document.getElementsByTagName('script')[0];
var oneTrustScript1 = document.createElement('script');
oneTrustScript1.type = 'text/javascript';
oneTrustScript1.src = "https://cdn.cookielaw.org/consent/b8b9a78d-2685-4250-8bb7-8e3b0d1b05ce-test/OtAutoBlock.js";
firstScript.parentNode.insertBefore(oneTrustScript1, firstScript);

var oneTrustScript2 = document.createElement('script');
oneTrustScript2.type = 'text/javascript';
oneTrustScript2.src = "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js";
oneTrustScript2.charset = "UTF-8";
oneTrustScript2.setAttribute("data-domain-script", "b8b9a78d-2685-4250-8bb7-8e3b0d1b05ce-test");
firstScript.parentNode.insertBefore(oneTrustScript2, firstScript);

var oneTrustScript3 = document.createElement('script');
oneTrustScript3.type = 'text/javascript';
oneTrustScript3.text = "function OptanonWrapper() { }";
firstScript.parentNode.insertBefore(oneTrustScript3, firstScript);

var maskCode = 'BGSI';

var optrial = optrial || {};
optrial.channel = '';
optrial.aggregator = '';
optrial.affinitycode = maskCode;
optrial.productclass = '';

var dataLayer = window.dataLayer || [];
dataLayer.push({
    "channel": null,
    "maskCode": maskCode,
    "sessionId": null,
    "product": null,
    "aggregatorName": null,
    "pQuoteNumber": null
});

(function () { gtmStart('GTM-WXDHC49'); })();
(function () { gtmStart('GTM-M5BKCCV'); })();

var tagProcessingDone = false;
function setTagProcessingDone() {
    tagProcessingDone = true;
}

var digitalData = { "page_data": {} };
$.getScript('https://assets.adobedtm.com/5b4eb01e0d29/1728f971d27d/launch-f81d526d4c69-development.min.js', function()
{
    digitalData.page_data.page_url = window.location.href;
    digitalData.page_data.page_title = document.title;
    digitalData.page_data.page_path = window.location.pathname;
    _satellite.pageBottom();
});