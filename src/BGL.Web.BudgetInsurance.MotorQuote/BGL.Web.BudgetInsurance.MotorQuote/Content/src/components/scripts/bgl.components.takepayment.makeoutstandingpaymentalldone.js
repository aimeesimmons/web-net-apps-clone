﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.makeoutstandingpaymentalldone = (function () {

    function initComponent() {
        clearOneOffPaymentSessionData();
    }

    function clearOneOffPaymentSessionData() {
        sessionStorage.removeItem("OneOffPaymentSelectedPersonIdText");
        sessionStorage.removeItem("OneOffPaymentCardCaptureUri");
        sessionStorage.removeItem("OneOffPaymentSecureSessionId");
        sessionStorage.removeItem("OneOffPaymentSelectedPersonIdVal");
        sessionStorage.removeItem("OneOffPaymentSelectedPersonId");
    }

    return {
        init: initComponent
    };
})();