﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

var selectedModifications = 'SelectedModifications';

var slidePanelContentKey = "SlidePanelContent";

bgl.components.vehicledetails.journeyTypes = {
    SingleEdit: "0",
    LookupEdit: "1",
    MultiStepEdit: "2"
}

bgl.components.vehicledetails.searching = (function () {

    var containerId;
    var configuration;
    var form;

    function loadComponent(model, componentUrl, containerElementId) {

        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.searching.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        if (configuration == undefined) {
            configuration = componentConfiguration;
        }

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeToEvents(configuration);
    }

    function subscribeToEvents(configuration) {

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                var inputField = $("#registration-number");
                inputField.val(inputField.val().replace(/\s+/gi, ""));

                $(this).find("input[type='submit']").prop('disabled', true);
                if (bgl.common.validator.validate(configuration, $(this)) === false) {
                    $(this).find("input[type='submit']").prop('disabled', false);
                }
            });

        $("#registration-number").on("input",
            function () {

                var start = this.selectionStart;
                var end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });

        if (containerId === undefined) {
            containerId = slidePanelContentKey;
        }

        var vehicleObj = bgl.common.datastore.getData(form);
        var vehicleId = $("#VehicleId").val();

        if (vehicleObj.VehicleId === undefined && vehicleId) {
            bgl.common.datastore.extendCollectedData(form, { VehicleId: vehicleId });
        }

        bgl.components.vehicledetails.stepbuilder.init(configuration, containerId);
    }

    function onSuccess(componentConfiguration, data) {

        if (componentConfiguration.JourneyType.toString() !==
            bgl.components.vehicledetails.journeyTypes.MultiStepEdit) {

            bgl.components.vehicledetails.summary.load(data.searchingResult,
                data.componentUrl,
                containerId,
                true);
        } else {

            delete data.searchingResult.ComponentConfiguration;

            data.searchingResult.isFormDirty = true;

            bgl.components.vehicledetails.summary.resetSearchedData(data.searchingResult, componentConfiguration.DataStoreKey);

            bgl.common.datastore.extendCollectedData(componentConfiguration.DataStoreKey, data.searchingResult);

            bgl.common.utilities.redirectToUrl(data.componentUrl);
        }
    }

    function onError(componentConfiguration, error) {
        bgl.components.vehicledetails.searching.init(componentConfiguration, containerId);

        form.find("input[type='submit']").prop('disabled', false);
    }

    return {
        load: loadComponent,
        init: initComponent,
        subscribe: subscribeToEvents,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.vehicledetails.summary = (function () {

    var containerId;
    var configuration;
    var form;
    var globalContainerId;

    function loadComponent(componentContent, componentUrl, containerElementId, needToResetSearchedData) {

        containerId = containerElementId;
        configuration = componentContent.ComponentConfiguration;

        if (needToResetSearchedData === true) {
            var dataStoreKey = configuration.DataStoreKey !== undefined && configuration.DataStoreKey !== null
                ? configuration.DataStoreKey
                : "VehicleLookupJourney";

            resetSearchedData(componentContent, dataStoreKey);
        }

        bgl.common.loader.load(componentContent,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.summary.init(configuration);
            });
    }

    function resetSearchedData(foundData, dataStoreKey) {

        var formData = bgl.common.datastore.getData(dataStoreKey);

        var registrationYear = (foundData.Registration.length > 4
            ? foundData.Registration.substr(0, 4)
            : foundData.Registration) -
            0;

        var selectedPurchasedYear = (formData.Year === undefined ? "" : formData.Year) - 0;

        if (registrationYear > selectedPurchasedYear) {
            formData = deleteFieldsFromModel(formData, "Year");
        }

        if (foundData.RegistrationNumber === "") {
            formData = deleteFieldsFromModel(formData, "RegistrationNumber");
        }

        if (foundData.SecurityDeviceCode !== formData.SecurityDeviceCode) {
            formData = deleteFieldsFromModel(formData, "SecurityDeviceCode");
        }

        if (foundData.HasRightHandDrive !== formData.HasRightHandDrive) {
            formData = deleteFieldsFromModel(formData, "HasRightHandDrive");
        }

        if (foundData.HasTrackingDevice !== formData.HasTrackingDevice) {
            formData = deleteFieldsFromModel(formData, "HasTrackingDevice");
        }

        if (foundData.Value !== formData.Value) {
            formData = deleteFieldsFromModel(formData, "Value");
        }

        formData.isFormDirty = true;

        sessionStorage.setItem(dataStoreKey, JSON.stringify(formData));
    }

    function deleteFieldsFromModel(formData, searchingCriteria) {
        $.each(Object.keys(formData),
            function (key, value) {
                if (value.substring(0, searchingCriteria.length) === searchingCriteria) {
                    delete formData[value];
                }
            });
        return formData;
    }

    function commonInit() {
        saveVehicleLookupJourney();
        var assumedDataSummaryLink = $("#assumedDataSummary");

        assumedDataSummaryLink.off("click").on("click",
            function (event) {

                event.preventDefault();

                var assumedDataSection = $("#assumedDataSection");
                var assumedDataSummarySection = $("#assumedDataSummarySection");

                assumedDataSection.toggleClass('hide');
                assumedDataSummarySection.toggleClass('hide');
            });

        bgl.common.validator.init(form);

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                bgl.common.validator.validate(configuration, $(this));
            });
    }

    function initComponent(componentConfiguration) {
        if (configuration == undefined) {
            configuration = componentConfiguration;
        }

        commonInit();

        if (configuration.JourneyType.toString() !== bgl.components.vehicledetails.journeyTypes.MultiStepEdit) {
            var linkButton = $("#vehicle-search-button");

            linkButton.off("click").on("click",
                function (event) {

                    event.preventDefault();

                    var link = $(event.currentTarget);
                    var containerSelector = link.data("container");

                    var onEventName = link.data("init-event");
                    var callback = eval(onEventName);
                    var vehicleId = link.data("vehicle-id") || bgl.common.datastore.getData(form).VehicleId;

                    var model = {
                        ComponentConfiguration: configuration,
                        vehicleId: vehicleId
                    };

                    if (typeof callback === "function") {
                        bgl.common.loader.load(model,
                            link.data("action-url"),
                            containerSelector,
                            function () {
                                callback(configuration);
                            });
                    }
                });
        }

        bgl.components.vehicledetails.stepbuilder.init(configuration, containerId);
    }

    function saveVehicleLookupJourney() {
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        var vehicleObject = bgl.common.datastore.getData(form);

        form.find("input[type='radio']:checked").trigger("change");

        bgl.common.datastore.extendCollectedData(form, { isFormDirty: vehicleObject.isFormDirty });
    }

    function onSuccess(componentConfiguration, data) {

        var vehicleObject = bgl.common.datastore.getData(form);

        var model = data.modifications;

        model.ComponentConfiguration = componentConfiguration;
        model.VehicleId = vehicleObject.VehicleId;
        model.JourneyType = vehicleObject.JourneyType;
        model.Modifications = vehicleObject[selectedModifications] || [];
        model.Model = vehicleObject.Model;
        model.Manufacturer = vehicleObject.Manufacturer;

        if (componentConfiguration.JourneyType.toString() !==
            bgl.components.vehicledetails.journeyTypes.MultiStepEdit) {

            if (model.VehicleType === "van") {
                bgl.components.vehicledetails.vehiclebodytype.load(model,
                    data.componentUrl,
                    containerId);
            }
            else if (componentConfiguration.IsService) {
                bgl.components.vehicledetails.importantinformation.load(model,
                    data.componentUrl,
                    containerId);
            }
            else {
                bgl.components.vehicledetails.modifications.load(model,
                    data.componentUrl,
                    containerId);
            }
        } else {
            bgl.common.utilities.redirectToUrl(data.componentUrl);
        }
    }

    function onError(componentConfiguration, error) {

        if (componentConfiguration.JourneyType.toString() !==
            bgl.components.vehicledetails.journeyTypes.MultiStepEdit) {
            initComponent(componentConfiguration);
        } else {
            initPage(componentConfiguration, globalContainerId);
        }

        var assumedDataSummaryLink = $("#assumedDataSummary");
        assumedDataSummaryLink.trigger("click");
    }

    return {
        load: loadComponent,
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        resetSearchedData: resetSearchedData
    };
})();

bgl.components.vehicledetails.stepbuilder = (function () {

    var containerId;
    var configuration;
    var needToRefreshPage;

    function getData(stepType) {

        var vehicleDataContainer = $('.data-container');
        var currentStep = vehicleDataContainer.data('currentStep');
        var vehicleBuilderValues = vehicleDataContainer.data('vehicleBuilderValues');
        var isBackButtonClicked = false;
        var quoteType = getQuoteType();

        if (stepType === 'previous-step') {
            isBackButtonClicked = true;
            --currentStep;
        } else if (stepType === 'next-step') {
            ++currentStep;
        }

        return {
            componentConfiguration: configuration,
            currentStep: currentStep,
            vehicleBuilderValues: vehicleBuilderValues,
            isBackButtonClicked: isBackButtonClicked,
            quoteType: quoteType
        };
    }

    function getQuoteType() {
        var dataContainerQuoteType = $('.data-container').data('quoteType');
        var vehicleConfirmationQuoteType = $('a[data-quote-type]').data('quoteType');

        return dataContainerQuoteType || vehicleConfirmationQuoteType;
    }

    function scrollToTop() {
        $("#" + configuration.Id + "__SlidePanel").scrollTop(1);
    }

    function subscribeDropdownOnChange() {
        var dropdown = $('#builder-dropdown');
        var confirmButton = $('#builder-dropdown-confirm-button');

        dropdown.off('change')
            .off('keypress')
            .on('change',
                function () {
                    var value = $(this).find(':selected').attr('value');

                    confirmButton.prop('disabled', !value);
                })
            .on('keypress',
                function (event) {

                    if (event.keyCode === 13) {
                        $(this).click();
                    }
                });

        confirmButton.on('click',
            function (event) {
                event.preventDefault();

                var selectedId = dropdown.find(':selected').attr('value');
                var value = dropdown.find(':selected').text();

                changeStep('next-step', { Id: selectedId, Value: value }, $(this).data('redirect-url'));
            });
    }

    function subscribe() {

        subscribeDropdownOnChange();

        var editButton = $("#vehicle-edit-button");
        if (editButton && editButton.length > 0) {
            needToRefreshPage = editButton.data("refreshPage") === true;
        }

        $('.vehicle-builder-button, .vehicle-builder-back-button').off('click').on('click',
            function (event) {

                event.preventDefault();

                var stepType = $(this).data('stepType');

                var stepOptionItem = {
                    Id: $(this).data('itemId'),
                    Value: $(this).text()
                };

                var redirectUrl = $(this).is('a')
                    ? $(this).attr('href')
                    : $(this).data('redirect-url');

                changeStep(stepType, stepOptionItem, redirectUrl);
            });

        $('.vehicle-lookup__breadcrumb-link').off('click').on('click',
            function (event) {

                event.preventDefault();

                var data = getData('next-step');

                data.currentStep = $(this).data('vehicleBuilderStep');

                data.StepOptionItem = {
                    Id: $(this).data('itemId'),
                    Value: $(this).text()
                };

                data.isBreadcrumbLinkClicked = true;

                bgl.common.loader.load(data,
                    $(this).data('redirect-url'),
                    slidePanelContentKey,
                    onInit);
            });

        $('.vehicle-lookup__radio-input, .vehicle-lookup__vehicle-radio').off('click').on('click',
            function () {
                var button = $('#builder-confirm-button');

                if (button.is(':disabled')) {
                    button.prop('disabled', false);
                }
            });

        $('#builder-confirm-button').off('click').on('click',
            function (event) {

                event.preventDefault();

                var foundData = $('.vehicle-lookup__radio-input:checked').data('specificationModel');
                var url = $(this).data('redirect-url');

                resetData();

                foundData.RegistrationNumber = "";

                if (configuration.JourneyType.toString() === bgl.components.vehicledetails.journeyTypes.MultiStepEdit) {
                    $("[data-dismiss='slide-panel']").click();

                    delete foundData.ComponentConfiguration;

                    bgl.components.vehicledetails.summary.resetSearchedData(foundData, configuration.DataStoreKey);

                    bgl.common.datastore.extendCollectedData(configuration.DataStoreKey, foundData);

                    if (needToRefreshPage === true) {
                        bgl.common.utilities.refreshPage();
                    } else {
                        bgl.common.utilities.redirectToUrl(configuration.ComponentUrl + "#summary");
                    }

                } else {
                    foundData.ComponentConfiguration = configuration;

                    bgl.components.vehicledetails.summary.load(foundData, url, slidePanelContentKey, true);
                }
            });

        $('.vehicle-lookup__other-button').off('click').on('click', subscribeDropdownOnChange);

        $('.vehicle-lookup__other-button, .vehicle-lookup__vehicle-radio').off('keypress').on('keypress',
            function (event) {

                if (event.keyCode === 13 || event.keyCode === 32) {
                    $(this).click()
                        .focus();
                }
            });
    }

    function changeStep(stepType, selectedItem, href) {
        var data = getData(stepType);

        data.StepOptionItem = selectedItem;

        var currentContainerId = "SlidePanelContent";

        bgl.common.loader.load(data,
            href,
            currentContainerId,
            onInit);
    }

    function resetData() {
        var vehicleDataContainer = $('.data-container');

        if (vehicleDataContainer && vehicleDataContainer.length > 0) {
            $('.data-container').removeData("vehicleBuilderValues");
            $('.data-container').removeAttr("data-vehicle-builder-values");
            $('.data-container').removeData("currentStep");
            $('.data-container').removeAttr("data-current-step");
        }
    }

    function onInit() {
        scrollToTop();
        subscribe();
    }

    function init(config, containerSelector) {
        configuration = config;
        containerId = containerSelector;
        needToRefreshPage = false;
        onInit();
    }

    return {
        subscribe: subscribe,
        init: init
    };
})();

bgl.components.vehicledetails.importantinformation = (function () {
    var configuration,
        form;

    function loadComponent(model, componentUrl, containerElementId) {
        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.importantinformation.init(configuration);
            });
    }

    function init(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeForEvents();
    }

    function subscribeForEvents() {
        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                if (!$("#agreement-checkbox").prop("checked")) {
                    $("#agreement-container").addClass("error");
                } else {
                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                }
            });


        $("#back-button").off("click").on("click",
            function (event) {
                event.preventDefault();

                var link = $(event.currentTarget);
                var containerSelector = link.data("container");

                var onEventName = link.data("init-event");
                var callback = eval(onEventName);

                var model = bgl.common.datastore.getData(form);
                model.ComponentConfiguration = configuration;

                if (typeof callback === "function") {
                    bgl.common.loader.load(model,
                        link.data("action-url"),
                        containerSelector,
                        function () {
                            callback(configuration);
                        });
                }
            });
    }

    function onSuccess(componentConfiguration, data) {

        var vehicleObject = bgl.common.datastore.getData(form);

        var model = {
            ComponentConfiguration: componentConfiguration,
            VehicleId: vehicleObject.VehicleId,
            JourneyType: vehicleObject.JourneyType,
            Registration: vehicleObject.Registration,
            HasValue: vehicleObject.HasValue
        };

        bgl.components.vehicledetails.purchasedate.load(model,
            data.componentUrl,
            containerId);
    }

    function onError(componentConfiguration, error) {

    }

    return {
        load: loadComponent,
        init: init,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.vehicledetails;
}
