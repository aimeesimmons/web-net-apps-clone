﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;
var slidePanelContainer = 'SlidePanelContent';

var additionalDriverKey = 'AdditionalDriver';

var journeyTypes = {
    singleEdit: 0,
    additionalDriver: 1,
    multiStepEdit: 2
};

bgl.components.driverdetails.common = (function () {
    var component;
    var actions = {
        aboutYou: "AboutYou",
        employmentStatus: "EmploymentStatus",
        existencePartTimeOccupation: "ExistencePartTimeOccupation",
        address: "SearchAddress",
        ukResidentFiveYears: "UkResidentFiveYears",
        drivingLicenceNumber: "DrivingLicenceNumber",
        homeOwner: "HomeOwner",
        typeOfLicence: "TypeOfLicence",
        licenceHeldSince: "LicenceHeldSince",
        motorClaims: "MotorClaims",
        declinedInsurance: "DeclinedInsurance",
        criminalConvictions: "CriminalConvictions",
        medicalCondition: "MedicalCondition",
        medicalConditionDvlaNotified: "MedicalConditionDvlaNotified",
        contactInformation: "ContactInformation",
        contactInformationPolicy: "ContactInformationPolicy",
        motorConvictions: "MotorConvictions",
        useOfOtherVehicles: "UseOfOtherVehicles",
        maritalStatus: "MaritalStatus",
        deleterFrequentDriver: "DeleteFrequentDriverSlider"
    };

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            bgl.components.driverdetails.common.init(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        component = $('#' + componentConfiguration.Id);
        bgl.common.accordions.init();
        setupChangeTelephoneEventBinder(componentConfiguration);
        subscribeForEvents(componentConfiguration);
    }

    function getPreviouslyStoredChangedUserData() {
        var data = [];

        if (sessionStorage.getItem("ChangedUserData") !== null) {
            data = JSON.parse(sessionStorage.getItem("ChangedUserData"));
        }

        return data;
    }

    function setPreviouslyStoredChangedUserData(data) {
        sessionStorage.setItem("ChangedUserData", JSON.stringify(data));
    }

    function getAmountOfYears(dateEntered) {
        if (dateEntered) {
            var formattedDate = dateEntered.split("/").reverse().join("/");
            var dateToday = new Date();
            var fullDateEntered = new Date(formattedDate);
            var year = dateToday.getFullYear() - fullDateEntered.getFullYear();
            var month = dateToday.getMonth() - fullDateEntered.getMonth();
            if (month < 0 || (month === 0 && dateToday.getDate() < fullDateEntered.getDate())) {
                year--;
            }
            return year;
        }
        else {
            return 0;
        }
    }

    function subscribeForEvents(componentConfiguration) {
        subscribeEditLinks(componentConfiguration);
        subscribeDriverCarousel(componentConfiguration);

    }

    function subscribeDriverCarousel(componentConfiguration) {
        bgl.components.drivertabs.init();

        var selectedDriver = bgl.common.utilities.getUrlHashValue();
        if (selectedDriver !== '') {
            bgl.components.drivertabs.setActiveDriver(selectedDriver);
        }

        $('.named-driver-tab').off('click.driverdetails').on('click.driverdetails', function () {
            bgl.common.utilities.setUrlHashValue(bgl.components.drivertabs.getActiveDriver($(this)));
        });

        $('[data-accordion-key=' + componentConfiguration.Id + '-driver-details-accordion]').off('click.driver-details-accordion').on('click.driver-details-accordion', function () {
            if ($(this).attr('aria-expanded') === 'true') {
                bgl.components.drivertabs.showArrow();
            }
        });
    }

    function setupChangeTelephoneEventBinder(componentConfiguration) {
        bgl.common.pubsub.on('ChangeTelephoneEventBinder', function (link) {
            var containerSelector = slidePanelContainer;
            var actionUrl = link.data("action-url");
            var action = link.data("action");
            var personId = link.data("personId");
            var policyHolderId = link.data("policyHolderId");
            var source = link.data("source");
            var requestBodyData = { componentConfiguration: componentConfiguration, personId: personId, source: source };
            requestBodyData.personId = policyHolderId;
            bgl.common.loader.load(requestBodyData,
                actionUrl,
                containerSelector,
                function () {
                    bgl.components.contactinformationpolicy.init(componentConfiguration);
                });
        });
    }

    function subscribeEditLinks(componentConfiguration) {
        var editLinks = component.find('[data-action-url]');

        editLinks.off()
            .on({
                'keyup': function (event) {
                    var code = event.charCode || event.keyCode;

                    if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                        $(this).click();
                    }
                },
                'click': function (event) {
                    event.preventDefault();

                    var link = $(this);

                    var containerSelector = slidePanelContainer;
                    var actionUrl = link.data("action-url");
                    var action = link.data("action");
                    var personId = link.data("personId");
                    var policyHolderId = link.data("policyHolderId");
                    var firstName = link.data("driverFirstName");
                    var isPolicyHolder = link.data("isPolicyHolder");

                    var source = link.data("source");
                    var requestBodyData = { componentConfiguration: componentConfiguration, personId: personId, source: source };

                    if (isPolicyHolder !== "True") {
                        requestBodyData.FirstName = firstName;
                    }

                    requestBodyData.isPolicyHolder = isPolicyHolder === "True";

                    switch (action) {
                        case actions.aboutYou:
                            requestBodyData.JourneyType = journeyTypes.singleEdit;
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.aboutyoudriverdetails.init(componentConfiguration);
                                });
                            break;

                        case actions.employmentStatus:
                        case actions.existencePartTimeOccupation:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.employmentstatusbuilder.init(componentConfiguration, containerSelector);
                                });
                            break;

                        case actions.address:
                            bgl.components.editaddress.load(componentConfiguration,
                                actionUrl,
                                containerSelector);
                            return;

                        case actions.drivingLicenceNumber:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector, function () {
                                    bgl.components.drivinglicencenumber.init(componentConfiguration);
                                });
                            break;

                        case actions.homeOwner:
                            requestBodyData.personId = policyHolderId;

                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.homeowner.init(componentConfiguration);
                                });
                            break;

                        case actions.ukResidentFiveYears:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.ukresidentfiveyears.init(componentConfiguration);
                                });
                            break;

                        case actions.typeOfLicence:
                            requestBodyData.firstStep = actions.typeOfLicence;
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.typeoflicence.init(componentConfiguration);
                                });
                            break;

                        case actions.licenceHeldSince:
                            requestBodyData.firstStep = actions.licenceHeldSince;
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.licenceheldsince.init(componentConfiguration);
                                });
                            break;

                        case actions.motorClaims:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.motorclaims.init(componentConfiguration);
                                });
                            break;

                        case actions.declinedInsurance:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.declinedinsurance.init(componentConfiguration);
                                });
                            break;

                        case actions.medicalCondition:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.medicalcondition.init(componentConfiguration);
                                });
                            break;

                        case actions.medicalConditionDvlaNotified:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.medicalconditiondvlanotified.init(componentConfiguration);
                                });
                            break;

                        case actions.contactInformation:
                            requestBodyData.personId = policyHolderId;
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.contactinformation.init(componentConfiguration);
                                });
                            break;

                        case actions.contactInformationPolicy:
                            requestBodyData.personId = policyHolderId;
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.contactinformationpolicy.init(componentConfiguration);
                                });
                            break;

                        case actions.motorConvictions:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.motorconvictions.init(componentConfiguration);
                                });
                            break;

                        case actions.criminalConvictions:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.criminalconvictions.init(componentConfiguration, false);
                                });
                            break;

                        case actions.useOfOtherVehicles:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.useofothervehicles.init(componentConfiguration);
                                });
                            break;

                        case actions.maritalStatus:
                            bgl.common.loader.load(requestBodyData,
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.maritalstatus.init(componentConfiguration, containerSelector);
                                });
                            break;

                        case actions.deleterFrequentDriver:
                            bgl.common.loader.load({
                                    componentConfiguration: componentConfiguration,
                                    driverId: link.data('driverId'),
                                    firstName: link.data('driverFirstName'),
                                    lastName: link.data('driverLastName')
                                },
                                actionUrl,
                                containerSelector,
                                function () {
                                    bgl.components.deletefrequentdriver.init(componentConfiguration);
                                });
                            break;

                    }
                }
            });
    }

    return {
        init: initComponent,
        load: loadComponent,
        getPreviouslyStoredChangedUserData: getPreviouslyStoredChangedUserData,
        setPreviouslyStoredChangedUserData: setPreviouslyStoredChangedUserData,
        getAmountOfYears: getAmountOfYears
    };
})();

bgl.components.aboutyoudriverdetails = (function () {
    var form;
    var configuration;

    function initComponent(componentConfiguration) {
        initObject(componentConfiguration);

        bgl.common.datastore.init(form);
    }

    function initJourney(componentConfiguration) {
        initObject(componentConfiguration);

        bgl.components.additionaldriver.initJourneyStep();

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();
    }

    function initObject(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#about-you-form');

        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init(form);

        bgl.common.date.initDateFormat($('#DateOfBirth'));

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(event.target))) {
                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(event.target)) === true);
                        storeUserChangesForTagManager();
                        bgl.common.validator.submitForm($(event.target), configuration);
                    }
                });

    }

    function storeUserChangesForTagManager() {
        var data = bgl.components.driverdetails.common.getPreviouslyStoredChangedUserData();
        if ($("#IsPolicyHolder").val() === "True") {
            var newDateOfBirth = $("#DateOfBirth").val();
            data.push(["UserAge", bgl.components.driverdetails.common.getAmountOfYears(newDateOfBirth)]);
            bgl.components.driverdetails.common.setPreviouslyStoredChangedUserData(data)
        } else {
            return data;
        }
    }

    function updateNotification(houstonMessage) {
        var notification = JSON.stringify({
            cat: "pricechange",
            msg: houstonMessage
        });

        sessionStorage.setItem("notification", notification);

        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        if (data.nextStepUrl) {
            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                if (data.houstonMessage) {

                    var params = {
                        callback: function () {
                            updateNotification(data.houstonMessage);
                        }
                    };

                    bgl.common.pubsub.emit('PollBasket', params);
                } else {
                    bgl.common.pubsub.emit('PollBasket');
                }
            }

            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.aboutyoudriverdetails.initJourney(configuration);
                break;
            default:
                bgl.components.aboutyoudriverdetails.init(configuration);
                break;
        }
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        initJourney: initJourney,
        storeUserChangesForTagManager: storeUserChangesForTagManager,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.employmentstatusbuilder = (function () {
    var containerId;
    var configuration;
    var form;
    var journeyType = 0;
    var hasPreselectedValues;

    function init(componentConfiguration, containerElementId, resetDirtyForm) {
        initObject(componentConfiguration, containerElementId);

        bgl.common.datastore.init(form, resetDirtyForm);
    }

    function initJourney(componentConfiguration, containerElementId) {
        initObject(componentConfiguration, containerElementId);

        bgl.components.additionaldriver.initJourneyStep();

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        journeyType = 1;

        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

        if (hasPreselectedValues === true) {
            showContinueButton();
        }

        if (hasPreselectedValues === false) {
            form.find("input[type=radio]")
                .off('click')
                .on('click', continueHandler);
        }
    }

    function initObject(componentConfiguration, containerElementId) {
        configuration = componentConfiguration;
        containerId = containerElementId;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);
        bgl.common.validator.initAutocompleteSearchInputs(form);

        bgl.common.autocompletesearch.init(3);

        $("a[data-step-type='previous-step']").one("click", function (event) {
            event.preventDefault();

            var backUrl = $(this).attr("href");

            var elementsArray = form.serializeArray();

            var model = {
                componentConfiguration: configuration
            };

            $.map(elementsArray, function (item, i) {
                model[item['name']] = item['value'];
            });

            loadViewPage(model, backUrl);
        });

        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                if (bgl.common.validator.isFormValid($(event.target))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(event.target)) === true);

                    bgl.common.validator.submitForm($(event.target), configuration);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function onSuccess(configuration, data) {
        if (data.needDeleteOccupation === true) {
            resetOccupationInfo(data.occupationStep, data.hasChanges, form);
        }

        if (data.hasChanges === true) {
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });
        }

        if (data.nextStepUrl) {
            bgl.components.additionaldriver.nextStep(configuration, data, form);
            return;
        }

        if (data.componentUrl) {
            loadViewPage(data.content, data.componentUrl);
            return;
        }

        bgl.common.datastore.removeSessionStorage(form);

        if (configuration && configuration.IsService) {
            if (configuration.IsInitialChange) {
                if (data.hasChanges === true) {
                    sessionStorage.setItem("MTAStartPage", window.location.href);
                    bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                } else {
                    bgl.common.contentpanel.hideSlidePanel();
                }
            } else {
                bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
            }
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }

            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function loadViewPage(model, url) {
        var initFunc = getInitByJourneyType((model.journeyType || model.JourneyType) - 0);

        bgl.common.loader.load(model, url, containerId,
            function () {
                initFunc(configuration, containerId, false);
            });
    }

    function resetOccupationInfo(stepType, hasChanges, form) {

        if (hasChanges === true) {

            switch (stepType) {
                case 1:
                    bgl.common.datastore.deleteFields(form,
                        [
                            "PartTimeOccupationId", "PartTimeOccupation",
                            "PartTimeBusinessCategoryId", "PartTimeBusinessCategory"
                        ]);
                    break;
                default:
                    bgl.common.datastore.deleteFields(form,
                        [
                            "OccupationId", "Occupation",
                            "BusinessCategoryId", "BusinessCategory"
                        ]);
                    break;
            }
        }
    }

    function onError() {
        var initFunc = getInitByJourneyType($("#JourneyType").val() - 0);

        initFunc(configuration, containerId);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function getInitByJourneyType(journeyType) {
        switch (journeyType) {
            case 1:
                return bgl.components.employmentstatusbuilder.initJourney;
            default:
                return bgl.components.employmentstatusbuilder.init;
        }
    }

    return {
        init: init,
        initJourney: initJourney,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.editaddress = (function () {

    var configuration,
        containerId,
        form,
        BACK_URL_STACK_KEY = "backUrlStack",
        isConfirmStep = false,
        mileageForm = "AnnualMileage",
        overnightParkingForm = "OvernightParking";

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.editaddress.init(configuration);
                sessionStorage.removeItem(BACK_URL_STACK_KEY);
            }
        );
    }

    function initComponent(componentConfiguration, resetDirtyFlag) {

        configuration = componentConfiguration;

        initValidation(resetDirtyFlag);

        initAjaxLink();

        initBackButtonLink();

        initPostcodeFieldTrim();
    }

    function initValidation(resetDirtyFlag) {

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);
        bgl.common.validator.enableMaxLength();

        bgl.common.datastore.init(form, resetDirtyFlag, true);
        bgl.common.datastore.setupReadAndWrite();

        isConfirmStep = form.data("step") === "confirm";

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                form.find(".error-messagebox").removeClass("show");

                if (bgl.common.validator.isFormValid($(this))) {
                    var hasChanges = bgl.common.datastore.isDataChanged($(this)) === true;
                    $("#HasChanges").val(hasChanges);
                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        subscribeToEventDismissAddressPanel();
    }

    function initAjaxLink() {
        $('.ajax-address-link')
            .off('click')
            .on('click',
                function (e) {
                    e.preventDefault();

                    var url = $(this).attr('href');
                    var backButtonUrl = $(this).data('back-url');

                    pushBackButtonUrl(backButtonUrl);
                    loadView(url);
                });
    }

    function initBackButtonLink() {
        $('.back-button-link')
            .off('click')
            .on('click',
                function (e) {
                    e.preventDefault();
                    $(this).hide();
                    var url = getBackButtonUrl();
                    loadView(url);
                });
    }

    function initPostcodeFieldTrim() {
        $('#PostCode')
            .on('blur',
                function (e) {
                    $(this).val($(this).val().trim());
                });
    }

    function subscribeToEventDismissAddressPanel() {
        $(document).off('click.address', '[data-dismiss="slide-panel"]').on('click.address',
            '[data-dismiss="slide-panel"]',
            function () {
                unSubscribeToEventDismissAddressPanel();
                dismissSlidePanelHandler();
            });

        $(document).off('keyup.address').on('keyup.address',
            function (event) {
                if (event.key === "Escape") {
                    unSubscribeToEventDismissAddressPanel();
                    dismissSlidePanelHandler();
                }
            });

        $('.overlay').off('click.address').on('click.address',
            function () {
                unSubscribeToEventDismissAddressPanel();
                dismissSlidePanelHandler();
            });
    }

    function unSubscribeToEventDismissAddressPanel() {
        $(document).off('click.address', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.address');
        $('.overlay').off('click.address');

    }

    function dismissSlidePanelHandler() {
        cleanUp(null, { needRaisingEvent: false });
    }


    function getBackButtonUrl() {
        var stack = JSON.parse(sessionStorage.getItem(BACK_URL_STACK_KEY));
        var url = stack.pop();

        sessionStorage.setItem(BACK_URL_STACK_KEY, JSON.stringify(stack));

        return url;
    }

    function pushBackButtonUrl(url) {
        var stack = JSON.parse(sessionStorage.getItem(BACK_URL_STACK_KEY)) || [];

        stack.push(url);

        sessionStorage.setItem(BACK_URL_STACK_KEY, JSON.stringify(stack));
    }

    function loadView(url) {
        var data = bgl.common.datastore.getData(form);

        data.ComponentConfiguration = configuration;

        var resetDirtyFlag = false;

        if (url.indexOf('SearchAddress') !== -1) {
            sessionStorage.removeItem(BACK_URL_STACK_KEY);
            data = configuration;
        } else if (url.indexOf('SelectAddress') === -1) {
            delete data.PostCode;
            delete data.Home;
        }

        bgl.common.loader.load(data,
            url,
            containerId,
            function () {
                initComponent(configuration, resetDirtyFlag);
            });
    }

    function onSuccess(configuration, data) {
        pushBackButtonUrl(data.backButtonUrl);

        bgl.common.loader.load(data.content, data.url, containerId, function () {
            bgl.components.editaddress.init(configuration, data.resetDirtyFlag);
        });
    }

    function onSuccessSearch(configuration, data) {
        onSuccess(configuration, data);
    }

    function cleanUp() {
        bgl.common.datastore.removeSessionStorage(form);
        unSubscribeToEventDismissAddressPanel();
        bgl.common.datastore.removeSessionStorage(BACK_URL_STACK_KEY);

        bgl.common.datastore.removeSessionStorage(mileageForm);
        bgl.common.datastore.removeSessionStorage(overnightParkingForm);
        $('[data-dismiss="slide-panel"]').click();
    }

    function onSuccessConfirmation(configuration, responsePayload) {
        var newSessionObject = $.extend(responsePayload.address, { isFormDirty: responsePayload.hasChanges, HasChanges: responsePayload.hasChanges });
        bgl.common.datastore.extendCollectedData(form, newSessionObject);
        pushBackButtonUrl(responsePayload.backButtonUrl);
        bgl.common.loader.load(configuration, responsePayload.url, containerId, function () {
            bgl.components.driverdetails.overnightparking.init(configuration, containerId);
        });   
    }

    function onError(configuration) {
        bgl.components.editaddress.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function postAddressChange(configuration, postUrl, frmData) {
        bgl.common.datastore.removeSessionStorage(form);
        unSubscribeToEventDismissAddressPanel();
        bgl.common.datastore.removeSessionStorage(BACK_URL_STACK_KEY);
        bgl.common.datastore.removeSessionStorage(mileageForm);
        bgl.common.datastore.removeSessionStorage(overnightParkingForm);

        $.post(postUrl, frmData, function (response) {
            if (response.errorRedirectUrl !== undefined) {
                bgl.common.utilities.errorRedirect(response.errorRedirectUrl);
                return;
            }
            if (configuration && configuration.IsService) {
                if (configuration.IsInitialChange) {
                    if (response.needRaisingEvent === true) {
                        sessionStorage.setItem("MTAStartPage", window.location.href);
                        bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                    } else {
                        $('[data-dismiss="slide-panel"]').click();
                    }
                } else {
                    bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                }
            } else {
                if (response.needRaisingEvent === true) {
                    bgl.common.pubsub.emit('PollBasket');
                }

                $('[data-dismiss="slide-panel"]').click();
            }
        });
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onSuccessSearch: onSuccessSearch,
        onSuccessConfirmation: onSuccessConfirmation,
        postAddressChange: postAddressChange,
        onError: onError
    };
})();

bgl.components.ukresidentfiveyears = (function () {
    var form,
        configuration,
        residencyStartDateDropdowns,
        fullYearMonthOptions,
        currentYearMonthOptions,
        firstYearMonthOptions,
        monthDropdown,
        yearDropdown,
        HIDE_CLASS = "hide",
        hasPreselectedValues;

    function initComponent(componentConfiguration) {
        form = $('#uk-resident-five-years');

        bgl.common.datastore.init(form);

        initObject(componentConfiguration);
    }

    function initJourney(componentConfiguration) {
        form = $('#uk-resident-five-years');

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        initObject(componentConfiguration);

        bgl.components.additionaldriver.initJourneyStep();
    }

    function initObject(componentConfiguration) {
        var currentMonth = $('#CurrentMonth').val() * 1;

        configuration = componentConfiguration;

        residencyStartDateDropdowns = form.find('#residency-start-date-dropdowns');
        monthDropdown = form.find('#Resident-Since-Month');
        yearDropdown = form.find('#Resident-Since-Year');

        bgl.common.validator.init(form);

        fullYearMonthOptions = monthDropdown.find('option');
        currentYearMonthOptions = fullYearMonthOptions.slice(0, currentMonth + 1);
        firstYearMonthOptions = fullYearMonthOptions.slice(0);
        firstYearMonthOptions.splice(1, currentMonth - 1);

        subscribeForEvents();

        updateMonthDropdown();

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeForEvents() {

        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

        if (hasPreselectedValues) {
            showContinueButton();

            var selectedButton = $(form.find("input[type=radio]:checked")[0]);
            switch (selectedButton.val().toLowerCase()) {
                case "false":
                    if (residencyStartDateDropdowns.hasClass(HIDE_CLASS)) {
                        residencyStartDateDropdowns.removeClass(HIDE_CLASS);
                    }
                    break;
            }
        }

        form.find("input[type=radio]").off('click').on('click',
            function (event) {
                $(event.target).prop("checked", true);

                updateResidencyStartDateVisibility($(event.target));

                if (hasPreselectedValues !== true) {
                    continueHandler(event);
                }
            });

        yearDropdown.on("change", updateMonthDropdown);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });
    }

    function updateResidencyStartDateVisibility(element) {

        var value = element.val();

        if (value.toLowerCase() === "false") {
            if (residencyStartDateDropdowns.hasClass(HIDE_CLASS)) {
                residencyStartDateDropdowns.removeClass(HIDE_CLASS);
            }

            hasPreselectedValues = true;
        } else {
            if (!residencyStartDateDropdowns.hasClass(HIDE_CLASS)) {
                residencyStartDateDropdowns.addClass(HIDE_CLASS);
            }
        }

        if (hasPreselectedValues === true) {
            showContinueButton();
        }
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $('#CurrentYear').val();
        var firstYear = (currentYear - 5).toString();

        switch (yearDropdown.val()) {
            case currentYear:
                months.html(currentYearMonthOptions);
                break;
            case firstYear:
                months.html(firstYearMonthOptions);
                break;
            default:
                months.html(fullYearMonthOptions);
                break;
        };

        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function onSuccess(configuration, data) {
        if (data.nextStepUrl) {

            if (data.hasChanges === true && data.isUkResident === true) {
                bgl.common.datastore.deleteFields(form,
                    [
                        "UkResidentYear", "UkResidentYearVal", "UkResidentYearText",
                        "UkResidentMonth", "UkResidentMonthVal", "UkResidentMonthText"
                    ]);
                bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });
            }

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }
            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.ukresidentfiveyears.initJourney(configuration);
                break;
            default:
                bgl.components.ukresidentfiveyears.init(configuration);
                break;
        }
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        initJourney: initJourney
    };
})();

bgl.components.typeoflicence = (function () {
    var form;
    var configuration;
    var licenceInputs;
    var containerId = 'SlidePanelContent';
    var internationalLabel = 'I';
    var provisionalLabel = 'P';
    var hasPreselectedValues;
    var continueButton;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#type-of-licence');

        licenceInputs = form.find('#licence-section [type=radio]');
        continueButton = $(form.find("[type='submit']"))

        bgl.common.validator.init(form);
    }

    function initComponent(componentConfiguration, resetDirtyFlag) {
        ctor(componentConfiguration);

        initObject();

        bgl.common.datastore.init(form, resetDirtyFlag);
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        initObject();

        bgl.components.additionaldriver.initJourneyStep();
    }

    function initObject() {
        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0 || continueButton.hasClass("hide") === false;

        updateCountrySectionVisibility();

        subscribeOnEvents();
    }

    function subscribeOnEvents() {
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        licenceInputs.off('click').on('click',
            function (event) {
                updateCountrySectionVisibility();

                if (hasPreselectedValues !== true) {
                    continueHandler(event);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function updateCountrySectionVisibility() {
        var countriesSection = form.find('#licence-row');

        var checkedLicenceValue = findCheckedLicenceValue();

        if (checkedLicenceValue === internationalLabel) {
            hasPreselectedValues = true;
            countriesSection.removeClass('hide');
        } else {
            countriesSection.addClass('hide');
        }

        if (hasPreselectedValues === true) {
            showContinueButton();
        }
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        var journeyType = $("#JourneyType").val() - 0;
        var content = JSON.parse(data.content);

        switch (journeyType) {
            case 1:
                bgl.common.loader.load(content,
                    data.componentUrl,
                    containerId,
                    function () {
                        bgl.components.licenceheldsince.initJourney(configuration);
                    });
                break;
            default:
                var checkedLicenceValue = findCheckedLicenceValue();

                if (checkedLicenceValue === provisionalLabel && data.hasChanges === true) {
                    window.sessionStorage.removeItem("DaNAddons");
                }

                bgl.common.loader.load(content,
                    data.componentUrl,
                    containerId,
                    function () {
                        bgl.components.licenceheldsince.init(configuration, false);
                    });
                break;
        }
    }

    function onError() {
        var initFunc = getInitByJourneyType($("#JourneyType").val() - 0);

        initFunc(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function findCheckedLicenceValue() {
        return form.find('#licence-section [type=radio]:checked').val();
    }

    function getInitByJourneyType(journeyType) {
        switch (journeyType) {
            case 1:
                return bgl.components.typeoflicence.initJourney;
            default:
                return bgl.components.typeoflicence.init;
        }
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        initJourney: initJourney,
        getInitByJourneyType: getInitByJourneyType
    };
})();

bgl.components.licenceheldsince = (function () {
    var form;
    var configuration;
    var monthDropdown;
    var yearDropdown;
    var fullYearMonthOptions;

    function initComponent(componentConfiguration, resetDirtyFlag) {
        initObject(componentConfiguration);

        bgl.common.datastore.init(form, resetDirtyFlag);
    }

    function initJourney(componentConfiguration) {
        form = $('#licence-held-since');
        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        initObject(componentConfiguration);

        bgl.components.additionaldriver.initJourneyStep();
    }

    function initObject(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#licence-held-since');

        bgl.common.validator.init(form);

        subscribeOnEvents();

        updateMonthDropdown();
    }

    function subscribeOnEvents() {
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        storeUserChangesForTagManager();

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        $("a[data-step-type='previous-step']").one("click", function (event) {
            event.preventDefault();

            var backUrl = $(this).attr("href");

            $(this).hide();

            var model = {
                componentConfiguration: configuration,
                country: $('#Country').val(),
                licence: $('#Licence').val(),
                firstStep: $("#FirstStep").val(),
                personId: $('#PersonId').val(),
                hasChanges: $('#HasChanges').val(),
                journeyType: $('#JourneyType').val(),
                firstName: $('#FirstName').val()
            };

            var initFunc = bgl.components.typeoflicence.getInitByJourneyType(model.journeyType - 0);

            bgl.common.loader.load(model, backUrl, slidePanelContainer,
                function () {
                    initFunc(configuration, false);
                });
        });

        monthDropdown = form.find('#Since-Month');
        yearDropdown = form.find('#Since-Year');

        fullYearMonthOptions = monthDropdown.find('option');

        yearDropdown.on("change", updateMonthDropdown);
    }

    function storeUserChangesForTagManager() {
        var data = bgl.components.driverdetails.common.getPreviouslyStoredChangedUserData();
        if ($("#IsPolicyHolder").val() === "True") {
            var monthDropdown = $('#Since-Month').val();
            var yearDropdown = $('#Since-Year').val();
            var dateHeldSince = monthDropdown + "/" + yearDropdown;
            data.push(["YearsLicenceHeld", bgl.components.driverdetails.common.getAmountOfYears(dateHeldSince)]);
            bgl.components.driverdetails.common.setPreviouslyStoredChangedUserData(data)
        } else {
            return data;
        }
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $('#CurrentYear').val() * 1;
        var currentMonth = $('#CurrentMonth').val() * 1;
        var selectedYear = yearDropdown.val() * 1;
        var personBirthYear = $("#PersonBirthYear").val() * 1;
        var personBirthMonth = $('#PersonBirthMonth').val() * 1;
        var adulthood = personBirthYear + 17;

        if (selectedYear === currentYear) {
            months.html(fullYearMonthOptions.slice(0, currentMonth + 1));
        } else if (selectedYear === adulthood) {
            // merge months after birth month and please select...
            var dropdownOptions = fullYearMonthOptions.slice(personBirthMonth);
            months.html($.merge([fullYearMonthOptions[0]], dropdownOptions));
        } else {
            months.html(fullYearMonthOptions);
        }
        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function updateNotification(houstonMessage) {
        var notification = JSON.stringify({
            cat: "pricechange",
            msg: houstonMessage
        });

        sessionStorage.setItem("notification", notification);

        window.location.reload(true);
    }

    function onSuccess(configuration, data) {

        if (data.nextStepUrl) {
            var licence = $("#Licence").val();

            if (data.hasChanges === true) {

                if (licence === "I") {
                    bgl.common.datastore.deleteFields(form, ["HasDrivingLicenceNumber", "DrivingLicenceNumber"]);
                } else {
                    bgl.common.datastore.deleteFields(form, ["Country", "CountryVal", "CountryText"]);
                }
            }

            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                if (data.houstonMessage) {

                    var params = {
                        callback: function () {
                            updateNotification(data.houstonMessage);
                        }
                    };

                    bgl.common.pubsub.emit('PollBasket', params);
                } else {
                    bgl.common.pubsub.emit('PollBasket');
                }
            }
            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.licenceheldsince.initJourney(configuration);
                break;
            default:
                bgl.components.licenceheldsince.init(configuration);
                break;
        }
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        storeUserChangesForTagManager: storeUserChangesForTagManager,
        onError: onError,
        initJourney: initJourney
    };
})();

bgl.components.drivinglicencenumber = (function () {

    var configuration, form, continueButton, hasPreselectedValues;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        continueButton = $(form.find("[type='submit']"));

        bgl.common.validator.init(form);
    }

    function initComponent(componentConfiguration) {
        ctor(componentConfiguration);

        initObject();

        bgl.common.datastore.init(form);
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        initObject();
        bgl.components.additionaldriver.initJourneyStep();
    }

    function initObject() {
        initValidation();

        initRadioButtons();
    }

    function initValidation() {
        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });
    }

    function initRadioButtons() {

        if (continueButton.hasClass("hide") === false) {
            hasPreselectedValues = true;
        } else {
            hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

            if (hasPreselectedValues === true) {
                showContinueButton();
                setDrivingLicenceRowVisibility(form.find("input[type='radio']:checked").val().toLowerCase() === "true");
            }
        }

        $('#has-driving-licence-number-yes').on("click",
            function () {
                setDrivingLicenceRowVisibility(true);

                hasPreselectedValues = true;
                showContinueButton();
            });

        $('#has-driving-licence-number-no').on("click",
            function (event) {
                setDrivingLicenceRowVisibility(false);

                if (hasPreselectedValues === false) {
                    $(event.target).prop("checked", "checked");
                    continueHandler(event);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function showContinueButton() {
        continueButton.removeClass("hide");
    }

    function setDrivingLicenceRowVisibility(needToShow) {
        var drivingLicenceRow = form.find("#driving-licence-number-row");

        if (needToShow === true) {
            drivingLicenceRow.removeClass('hide');
        } else {
            drivingLicenceRow.addClass('hide');
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.nextStepUrl) {

            if (data.hasChanges === true && data.hasDrivingLicenceNumber === false) {
                bgl.common.datastore.deleteFields(form, ["DrivingLicenceNumber"]);
            }

            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }

            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.drivinglicencenumber.initJourney(configuration);
                break;
            default:
                bgl.components.drivinglicencenumber.init(configuration);
                break;
        }
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        initJourney: initJourney
    };
})();

bgl.components.declinedinsurance = (function () {
    var form;
    var configuration;
    var submitButton;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#declined-insurance');
        submitButton = $(form.find("[type='submit']"));

        bgl.common.validator.init(form);
    }

    function initComponent(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form);

        subscribeToEvents();
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeToEvents();
        bgl.components.additionaldriver.initJourneyStep();
    }

    function subscribeToEvents() {
        if (submitButton.hasClass("hide") !== false) {
            if (form.find("input[type=radio]:checked").length === 0) {
                form.find("input[type=radio]")
                    .off('keypress', generateClickOnKeypress)
                    .on('keypress', generateClickOnKeypress)
                    .off('click', continueHandler)
                    .on('click', continueHandler);
            } else {
                submitButton.removeClass("hide");
            }
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(event.target).prop("checked", true);
            $(event.target).click();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.nextStepUrl) {
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }
            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.declinedinsurance.initJourney(configuration);
                break;
            default:
                bgl.components.declinedinsurance.init(configuration);
                break;
        }
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        initJourney: initJourney
    };
})();

bgl.components.homeowner = (function () {
    var form;
    var configuration;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#home-owner');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        if (bgl.common.datastore.isDataChanged($(this)) === false) {
                            bgl.common.datastore.removeSessionStorage($(this));
                            $('[data-dismiss="slide-panel"]').click();
                            return;
                        }

                        bgl.common.validator.submitForm($(this), componentConfiguration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function onSuccess() {
        bgl.common.datastore.removeSessionStorage(form);
        bgl.common.pubsub.emit('PollBasket');
        $('[data-dismiss="slide-panel"]').click();
    }

    function onError() {
        bgl.components.homeowner.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.motorclaims = (function () {
    var configuration;
    var motorClaimList;
    var form;
    var HIDE_CLASS = "hide";
    var haveMotorClaimsBeenChanged = false;
    var isJourneyType = false;
    var motorClaimItems;
    var hasPreselectedValues;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;

        isJourneyType = false;

        hasPreselectedValues = true;

        form = $("#" + componentConfiguration.Id + "-motor-claims-form");

        motorClaimList = $('#motor-claims-list');

        motorClaimItems = $('.step-form__summary-item');
    }

    function initComponent(componentConfiguration, hasChanges) {
        ctor(componentConfiguration);

        haveMotorClaimsBeenChanged = hasChanges !== undefined && hasChanges === true;

        subscribeForEvents();
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);
        isJourneyType = true;

        bgl.components.additionaldriver.initJourneyStep();

        bgl.common.datastore.init(form, true, true);

        var noButton = $("#driver-has-claims-no");

        var isAnyClaims = motorClaimItems.length > 0;

        if (isAnyClaims === false) {
            hasPreselectedValues = false;
            var sessionStorageValue = readDataStoreValue(noButton);

            if (sessionStorageValue !== undefined) {
                noButton.prop('checked', true);
                hasPreselectedValues = true;
            }
        }

        bgl.common.datastore.setupWrite();

        subscribeForEvents();
    }

    function readDataStoreValue($element) {
        var journeyData = bgl.common.datastore.getData(form);
        var key = $element.attr("data-store");

        return journeyData[key];
    }

    function subscribeForEvents() {
        subscribeToEventShowMotorClaims();

        subscribeToEventDeleteMotorClaim();

        subscribeToEventRemoveAllMotorClaims();

        subscribeToEventAddMotorClaim();

        subscribeToEventDismissMotorClaimsPanel();

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToEventDismissMotorClaimsPanel() {
        $(document).off('click.motorclaims', '[data-dismiss="slide-panel"]').on('click.motorclaims', '[data-dismiss="slide-panel"]', function () {
            unSubscribeToEventDismissMotorClaimsPanel();
            dismissSlidePanelHandler(haveMotorClaimsBeenChanged);
        });

        $(document).off('keyup.motorclaims').on('keyup.motorclaims', function (event) {
            if (event.key === "Escape") {
                unSubscribeToEventDismissMotorClaimsPanel();
                dismissSlidePanelHandler(haveMotorClaimsBeenChanged);
            }
        });

        $('.overlay').off('click.motorclaims').on('click.motorclaims', function () {
            unSubscribeToEventDismissMotorClaimsPanel();
            dismissSlidePanelHandler(haveMotorClaimsBeenChanged);
        });
    }

    function unSubscribeToEventDismissMotorClaimsPanel() {
        $(document).off('click.motorclaims', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.motorclaims');
        $('.overlay').off('click.motorclaims');
    }

    function dismissSlidePanelHandler(flag) {
        if (flag === true) {
            bgl.common.pubsub.emit('PollBasket');
        }
    }

    function subscribeToEventShowMotorClaims() {
        var radioButtons = $("input[type='radio']");

        var clickFunction;

        if (hasPreselectedValues === true) {
            $('button[data-remove-all-url]').removeClass(HIDE_CLASS);

            clickFunction = function (key, item) {
                var button = $(item);

                button.off("change").on("change",
                    function (event) {
                        event.preventDefault();

                        switch ($(this).val()) {
                            case "True":
                                if (motorClaimItems.length === 0) {
                                    loadAddMotorClaims($(event.target).data("url"));
                                } else {
                                    $("#driver-has-claims-no").attr("data-need-to-delete", "false");
                                    if (motorClaimList.hasClass(HIDE_CLASS)) {
                                        motorClaimList.removeClass(HIDE_CLASS);
                                    }
                                }
                                break;
                            case "False":
                                var isNeededToBeDelete = motorClaimItems.length !== 0;
                                $("#driver-has-claims-no").attr("data-need-to-delete", isNeededToBeDelete);

                                if (!motorClaimList.hasClass(HIDE_CLASS)) {
                                    motorClaimList.addClass(HIDE_CLASS);
                                }
                                break;
                        }
                    });
            }
        } else {
            clickFunction = function (key, item) {
                var button = $(item);

                button.on("change",
                    function (event) {
                        event.preventDefault();

                        switch ($(this).val()) {
                            case "True":
                                if (motorClaimItems.length === 0) {
                                    loadAddMotorClaims($(event.target).data("url"));
                                }
                                break;
                            case "False":
                                redirectToNextJourneyStep();
                                break;
                        }
                    });
            }
        }

        $.each(radioButtons, clickFunction);
    }

    function subscribeToEventAddMotorClaim() {

        $('#add-new-motor-claim').off("click").on("click",
            function (event) {
                event.preventDefault();

                var url = $(event.target).attr('href');

                loadAddMotorClaims(url);
            });
    }

    function subscribeToEventRemoveAllMotorClaims() {

        var removeAllButton = $("#remove-all-motor-claims");
        var noButton = $("#driver-has-claims-no");
        var doneButton = $('button[data-remove-all-url]');

        removeAllButton.off('click');
        removeAllButton.on('click',
            function (event) {

                event.preventDefault();

                noButton.click();
            });

        doneButton.off('click').on('click',
            function () {
                unSubscribeToEventDismissMotorClaimsPanel();
                var isNeededToDelete = noButton.attr("data-need-to-delete") === "true";
                var isNoButtonChecked = noButton.is(':checked');
                var patchUrl = $(this).data("remove-all-url");
                var journeyType = $('#JourneyType').val();

                if (isNeededToDelete && isNoButtonChecked) {
                    var dataToPost = $.param({
                        __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
                        personId: $("#PersonId").val(),
                        journeyType: journeyType
                    });

                    $.ajax({
                        url: patchUrl,
                        type: "POST",
                        data: dataToPost,
                        success: function (data, textStatus, request) {
                            if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                if (!data.isSuccess && data.errorRedirectUrl !== null) {
                                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                } else if (data.isSuccess) {
                                    (!isJourneyType)
                                        ? bgl.common.pubsub.emit('PollBasket')
                                        : redirectToNextJourneyStep(data);
                                }
                            }
                        }
                    });
                }
                else if (isJourneyType) {
                    redirectToNextJourneyStep();
                }
                else if (haveMotorClaimsBeenChanged === true) {
                    bgl.common.pubsub.emit('PollBasket');
                }

                if (!isJourneyType) sessionStorage.removeItem("drivers");
            });
    }

    function redirectToNextJourneyStep(data) {
        var dataToPost = data || {};
        dataToPost.nextStepUrl = form.data("next-step-url");

        bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

        bgl.components.additionaldriver.nextStep(configuration, dataToPost, form);
    }

    function subscribeToEventDeleteMotorClaim() {
        var deleteMotorClaimButtons = $("button[data-role='delete']");

        $.each(deleteMotorClaimButtons,
            function (key, item) {
                $(item).off("click");
                $(item).on("click", function (event) {
                    event.preventDefault();
                    $(item).attr("disabled", "disabled");

                    var target = $(event.target);

                    var link = target.is("button") ? target : target.parent();

                    var token = $("input[name='__RequestVerificationToken']").val();
                    var patchUrl = link.attr('href');

                    var personId = $('#PersonId').val();
                    var journeyType = $('#JourneyType').val();

                    var dataToPost = $.param({
                        __RequestVerificationToken: token,
                        motorclaimId: link.data("claim-id"),
                        journeyType: journeyType
                    });

                    var model = {
                        componentConfiguration: configuration,
                        personId: personId,
                        getPersons: false,
                        journeyType: journeyType,
                        firstName: $('#FirstName').val()
                    };

                    $.ajax({
                        url: patchUrl,
                        type: "POST",
                        data: dataToPost,
                        success: function (data, textStatus, request) {
                            if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                if (!data.isSuccess) {
                                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                } else {
                                    bgl.common.loader.load(model,
                                        data.redirectUrl,
                                        slidePanelContainer,
                                        function () {
                                            if (isJourneyType === true) {
                                                if (motorClaimItems.length === 1) {
                                                    var additionalDriver = JSON.parse(sessionStorage.getItem(additionalDriverKey));
                                                    additionalDriver['HasClaims'] = 'False';
                                                    sessionStorage.setItem(additionalDriverKey, JSON.stringify(additionalDriver));
                                                }

                                                bgl.components.motorclaims.initJourney(configuration);
                                            } else {
                                                bgl.components.motorclaims.init(configuration, true);
                                            }
                                        });

                                }
                            }
                        }
                    });
                });
            });
    }

    function loadAddMotorClaims(url) {

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            JourneyType: $('#JourneyType').val(),
            FirstName: $('#FirstName').val()
        };

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                bgl.components.motorclaimbuilder.init(configuration);
            });
    }

    function storeDrivers(value) {
        sessionStorage.setItem("drivers", value);
    }

    return {
        init: initComponent,
        initJourney: initJourney,
        storeDrivers: storeDrivers
    };
})();

bgl.components.motorclaimbuilder = (function () {
    var form,
        configuration,
        HIDE_CLASS = "hide";

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#add-motor-claim');

        if (form) {
            bgl.common.validator.init(form);
            bgl.common.validator.initNumberInputField($("#Claim-Amount"), 5);
        }

        subscribeForEvents();
    }

    function subscribeForEvents() {
        $('.step-form__button').off('click').on('click',
            function (event) {
                event.preventDefault();

                var link = $(event.target);

                selectNextStep(link);
            });

        $('.form-row__breadcrumb-link').off('click').on('click',
            function (event) {
                event.preventDefault();

                gotoBack($(event.target));
            });

        $('[data-step-type="previous-step"]').off('click').on('click',
            function (event) {
                event.preventDefault();

                gotoBack($(event.target));
            });

        var claimAmountRow = $('#claim-amount-row');

        $("#claim-has-amount-no").on("click",
            function () {
                if (!claimAmountRow.hasClass(HIDE_CLASS)) {
                    claimAmountRow.addClass(HIDE_CLASS);
                }
            });

        $("#claim-has-amount-yes").on("click",
            function () {
                if (claimAmountRow.hasClass(HIDE_CLASS)) {
                    claimAmountRow.removeClass(HIDE_CLASS);
                }
            });

        if (form) {
            form.off('submit')
                .on('submit',
                    function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        var submit = $(this).find(':submit');
                        submit.attr('disabled', 'disabled');

                        if (bgl.common.validator.validate(configuration, form) === false) {
                            submit.removeAttr('disabled');
                        }
                    });
        }
    }

    function gotoBack(link) {
        selectNextStep(link, true);
    }

    function selectNextStep(link, isBack) {
        var nextStep = link.data("next-step");

        var currentStep = $("#motor-claim-tiles").data("current-step");

        var url = link.attr('href');

        if (currentStep === "Year" && isBack === true) {
            loadMotorClaims(url);

            return;
        }

        var currentValue = {
            Code: link.data('code'),
            Value: $.trim(link.text()),
            BuilderStep: currentStep
        };

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            CurrentStep: nextStep,
            JourneyType: $('#JourneyType').val(),
            FirstName: $('#FirstName').val()
        };

        if (nextStep === "Driving") {
            model.BuilderTileItems = JSON.parse(sessionStorage.getItem("drivers"));
        }

        if (isBack) {
            currentValue = undefined;
            model.IsBackButtonClicked = true;
        }

        model.SelectedValues = getSelectedValues(currentValue);

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                bgl.components.motorclaimbuilder.init(configuration);
            });
    }

    function getSelectedValues(currentValue) {
        var result = [];

        var breadcrumbLinks = $('.form-row__breadcrumb-link') || [];

        $.each(breadcrumbLinks,
            function (idx, element) {

                var value = {
                    Code: $(element).data('code'),
                    Value: $.trim($(element).text()),
                    BuilderStep: $(element).data('next-step')
                };

                result.push(value);
            });

        if (currentValue !== undefined) {
            result.push(currentValue);
        }

        return result;
    }

    function onSuccess(configuration, data) {
        loadMotorClaims(data.redirectUrl);
    }

    function loadMotorClaims(url) {

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            getPersons: false,
            JourneyType: $('#JourneyType').val(),
            FirstName: $('#FirstName').val()
        };

        var initFunc = getInitByJourneyType((model.journeyType || model.JourneyType) - 0);

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                initFunc(configuration, true);
            });
    }

    function getInitByJourneyType(journeyType) {
        switch (journeyType) {
            case 1:
                return bgl.components.motorclaims.initJourney;
            default:
                return bgl.components.motorclaims.init;
        }
    }

    function onError() {
        bgl.components.motorclaimbuilder.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.criminalconvictions = (function () {
    var form,
        configuration;
    var criminalConvictionsContainer,
        criminalConvictionAddAnotherButton,
        criminalConvictionRemoveAllButton;
    var noButton;
    var yesButton;
    var noLabel;
    var yesLabel;
    var fullYearMonthOptions,
        monthDropdown,
        yearDropdown,
        isChanged = false;
    var HIDE_CLASS = "hide",
        journeyType = journeyTypes.singleEdit;
    var hasPreselectedValues;
    var doneButton;

    function ctor(componentConfiguration, isChangedComponent) {
        configuration = componentConfiguration;
        isChanged = isChangedComponent;

        hasPreselectedValues = true;

        form = $('#criminal-convictions');
        criminalConvictionsContainer = $('#criminal-convictions-container');
        criminalConvictionAddAnotherButton = criminalConvictionsContainer.find('#criminal-conviction-add-another');
        criminalConvictionRemoveAllButton = criminalConvictionsContainer.find("#remove-all-criminal-convictions");

        doneButton = $("#criminal-conviction_done-button");
        noButton = $('#has-criminal-convictions-no');
        yesButton = $('#has-criminal-convictions-yes');

        noLabel = $('#has-criminal-convictions-no-label');
        yesLabel = $('#has-criminal-convictions-yes-label');

        bgl.common.validator.init(form, {ignore: ""});
    }

    function initComponent(componentConfiguration, isChangedComponent) {
        ctor(componentConfiguration, isChangedComponent);

        subscribeForEvents();
    }

    function initJourney(componentConfiguration, isChangedComponent) {
        ctor(componentConfiguration, isChangedComponent);

        bgl.components.additionaldriver.initJourneyStep();

        if (criminalConvictionsnCount() === 0) {

            var additionalDriver = bgl.common.datastore.getData(form);

            if (additionalDriver !== null && additionalDriver["HasCriminalConvictions"] === 'False') {
                noButton.prop('checked', true);
            }
        }

        bgl.common.datastore.init(form, true, true);
        bgl.common.datastore.setupWrite();

        subscribeForEvents();

        journeyType = journeyTypes.additionalDriver;
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            var forAttr = $(event.target).attr('for');

            if (forAttr !== null) {
                $('#' + forAttr).click();
            } else {
                $(this).click();
            }
        }
    }

    function subscribeToEventShowCriminalConvictions() {
        yesButton.off('click').on("click",
            function () {
                var hasCriminalConviction =
                    criminalConvictionsContainer.find('.criminal-conviction-item').length > 0;
                if (!hasCriminalConviction) {
                    criminalConvictionAddAnotherButton.click();
                } else if (criminalConvictionsContainer.hasClass(HIDE_CLASS)) {
                    criminalConvictionsContainer.removeClass(HIDE_CLASS);
                }
            });

        noButton.off('click').on("click",
            function () {

                if (!criminalConvictionsContainer.hasClass(HIDE_CLASS)) {
                    criminalConvictionsContainer.addClass(HIDE_CLASS);
                }
            });
    }

    function subscribeForEvents() {
        subscribeToEventShowCriminalConvictions();

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

        $("button[data-role='delete']").off('click').on("click",
            function (event) {
                event.preventDefault();
                $(this).attr("disabled", "disabled");

                var target = $(event.target);

                var object = target.is("button") ? target : target.parent();

                var token = $("input[name='__RequestVerificationToken']").val();
                var patchUrl = object.data('url');

                var dataToPost = {
                    __RequestVerificationToken: token,
                    motorCriminalConvictionId: object.data("value"),
                    journeyType: journeyType
                };

                var requestBodyData = {
                    componentConfiguration: configuration,
                    personId: $('#PersonId').val(),
                    firstName: $("#FirstName").val(),
                    journeyType: journeyType
                };

                $.ajax({
                    url: patchUrl,
                    type: "POST",
                    data: dataToPost,
                    success: function (data) {
                        if (!data.isSuccess) {
                            bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                        } else {
                            isChanged = true;
                            bgl.common.loader.load(requestBodyData,
                                data.redirectUrl,
                                slidePanelContainer,
                                function () {
                                    switch (journeyType) {
                                        case 1:
                                            if (criminalConvictionsnCount() === 0) {
                                                var additionalDriver = JSON.parse(sessionStorage.getItem(additionalDriverKey));
                                                additionalDriver['HasCriminalConvictions'] = 'False';
                                                sessionStorage.setItem(additionalDriverKey,
                                                    JSON.stringify(additionalDriver));
                                            }
                                            bgl.components.criminalconvictions.initJourney(configuration);
                                            break;
                                        default:
                                            bgl.components.criminalconvictions.init(configuration, true);
                                            break;
                                    }
                                });
                        }
                    }
                });
            }).off('keypress').on("keypress", generateClickOnKeypress);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    isChanged = true;

                    bgl.common.validator.validate(configuration, form);
                });

        criminalConvictionAddAnotherButton.off('click').on('click', function (event) {
            event.preventDefault();

            bgl.components.criminalconvictions.goToWizardStep(1);
        }).off('keypress').on("keypress", generateClickOnKeypress);

        criminalConvictionRemoveAllButton.off('click').on('click', function (event) {
            event.preventDefault();
            noButton.click();
        }).off('keypress').on("keypress", generateClickOnKeypress);

        doneButton.off('click').on('click',
            function () {
                doneButton.attr('disabled', 'disabled');
                if (!bgl.common.validator.isFormValid(form)) {
                    doneButton.removeAttr('disabled');
                    return;
                }

                unSubscribeToEventDismissCriminalConvictionsPanel();
                var isNoButtonChecked = noButton.is(':checked');
                var hasCriminalConviction = criminalConvictionsContainer.find('.criminal-conviction-item').length > 0;

                if (isNoButtonChecked && hasCriminalConviction) {
                    isChanged = true;

                    bgl.common.validator.validate(configuration, form);
                } else if (bgl.components.additionaldriver.isAdditionalDriverJourney()) {
                    if ((isNoButtonChecked && !hasCriminalConviction) || yesButton.is(':checked')) {
                        isChanged = true;
                        bgl.components.additionaldriver.createDriver(configuration, $(this).data("createDriverUrl"), form);
                    }

                } else {
                    $('[data-dismiss="slide-panel"]').click();
                    dismissSlidePanelHandler(isChanged);
                }
            }).off('keypress').on("keypress", generateClickOnKeypress);

        subscribeToEventDismissCriminalConvictionsPanel();
    }

    function subscribeToEventDismissCriminalConvictionsPanel() {
        $(document).off('click.criminalconvictions', '[data-dismiss="slide-panel"]').on('click.criminalconvictions', '[data-dismiss="slide-panel"]', function () {
            unSubscribeToEventDismissCriminalConvictionsPanel();
            dismissSlidePanelHandler(isChanged);
        });

        $(document).off('keyup.criminalconvictions').on('keyup.criminalconvictions', function (event) {
            if (event.key === "Escape") {
                unSubscribeToEventDismissCriminalConvictionsPanel();
                dismissSlidePanelHandler(isChanged);
            }
        });

        $('.overlay').off('click.criminalconvictions').on('click.criminalconvictions', function () {
            unSubscribeToEventDismissCriminalConvictionsPanel();
            dismissSlidePanelHandler(isChanged);
        });
    }

    function unSubscribeToEventDismissCriminalConvictionsPanel() {
        $(document).off('click.criminalconvictions', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.criminalconvictions');
        $('.overlay').off('click.criminalconvictions');
    }

    function dismissSlidePanelHandler(flag) {
        if (flag === true) {
            bgl.common.pubsub.emit('PollBasket');
        }
    }

    function subscribeForEventsWizardStep1() {
        var formstep1 = $("#criminal-convictions-addanother-step1");
        bgl.common.validator.init(formstep1);

        formstep1.off('submit').on('submit', function (event) {
            event.preventDefault();

            $('#MonthText').val($('#Criminal-Conviction-Month option:selected').text());

            bgl.common.validator.validate(configuration, formstep1);
        });

        monthDropdown = formstep1.find('#Criminal-Conviction-Month');
        yearDropdown = formstep1.find('#Criminal-Conviction-Year');

        fullYearMonthOptions = monthDropdown.find('option');

        yearDropdown.on("change", updateMonthDropdown);

        $("a[data-step-type='previous-step']").one("click", function (event) {
            event.preventDefault();

            $(this).hide();

            var backUrl = $(this).attr("href");
            var personId = $('#PersonId').val();

            var model = {
                componentConfiguration: configuration,
                personId: personId,
                firstName: $("#FirstName").val(),
                journeyType: journeyType
            };

            switch (model.journeyType) {
                case 1:
                    bgl.common.loader.load(model,
                        backUrl,
                        slidePanelContainer,
                        function () {
                            bgl.components.criminalconvictions.initJourney(configuration);
                        });
                    break;
                default:
                    bgl.common.loader.load(model,
                        backUrl,
                        slidePanelContainer,
                        function () {
                            bgl.components.criminalconvictions.init(configuration, true);
                        });
                    break;
            }
        });
    }

    function subscribeForEventsWizardStep2() {
        var formStep2 = $('#criminal-convictions-addanother-step2');

        bgl.common.validator.init(formStep2);

        formStep2.off('submit').on('submit', function (event) {
            event.preventDefault();

            $('#ConvictionsTypeText').val($('#Conviction-type option:selected').text());

            bgl.common.validator.validate(configuration, formStep2);
        });

        subscribeBreadcrumbs();
    }

    function subscribeForEventsWizardStep3() {
        var formStep3 = $('#criminal-convictions-addanother-step3');

        bgl.common.validator.init(formStep3);

        formStep3.off('submit').on('submit', function (event) {
            event.preventDefault();
            var submit = $(this).find(':submit');
            submit.attr('disabled', 'disabled');

            if (bgl.common.validator.validate(configuration, formStep3) === false) {
                submit.removeAttr('disabled');
            }
        });

        subscribeBreadcrumbs();
    }

    function subscribeBreadcrumbs() {
        $('.criminal-conviction-back-to-first-step').off('click').on('click', function (event) {
            event.preventDefault();

            bgl.components.criminalconvictions.goToWizardStep(1);
        });

        $('.criminal-conviction-back-to-second-step').off('click').on('click', function (event) {
            event.preventDefault();

            bgl.components.criminalconvictions.goToWizardStep(2, { Year: $('#Year').val(), Month: $('#Month').val(), MonthText: $('#MonthText').val() });
        });
    }

    function goToWizardStep(step, data) {
        var href = $('#UrlToWizard').val();

        var defaultData = getDefaultData(step);

        if (data !== null) {
            $.extend(defaultData, data);
        }

        isChanged = true;

        $.ajax({
            url: href,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(defaultData),
            success: function (data) {
                if (typeof data.isSuccess === 'undefined') {
                    bgl.components.criminalconvictions.updateSlidePanel(data);
                } else if (data.isSuccess === true) {
                    bgl.components.criminalconvictions.updateSlidePanel(data.html);
                } else {
                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                }

                switch (step) {
                    case 1:
                        bgl.components.criminalconvictions.subscribeForEventsWizardStep1();
                        break;

                    case 2:
                        bgl.components.criminalconvictions.subscribeForEventsWizardStep2();
                        break;

                    case 3:
                        bgl.components.criminalconvictions.subscribeForEventsWizardStep3();
                        break;
                }
            }
        });
    }

    function getDefaultData(step) {
        var personId = $('#PersonId').val();
        var firstName = $('#FirstName').val();

        var additionalDriverData = bgl.components.additionaldriver.isAdditionalDriverJourney()
            ? JSON.parse(sessionStorage.getItem('AdditionalDriver'))
            : {};

        additionalDriverData = $.extend(additionalDriverData,
            { ComponentConfiguration: configuration, PersonId: personId, CriminalConvictionStep: step });

        if (firstName) {
            additionalDriverData.FirstName = firstName;
        }

        return additionalDriverData;
    }

    function onSuccess(configuration, data) {
        unSubscribeToEventDismissCriminalConvictionsPanel();
        if (data.nextStepUrl) {
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.createDriver(configuration, data.nextStepUrl, form);
        } else if (configuration.IsService) {
            bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
        } else if (configuration.HasPageRefresh) {
            bgl.common.utilities.refreshPage();
        } else {
            if (isChanged) {
                bgl.common.pubsub.emit('PollBasket');
            }
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.criminalconvictions.initJourney(configuration);
                break;
            default:
                bgl.components.criminalconvictions.init(configuration);
                break;
        }
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $('#CurrentYear').val() * 1;
        var currentMonth = $('#CurrentMonth').val() * 1;
        var selectedYear = yearDropdown.val() * 1;
        var personBirthYear = $("#PersonBirthYear").val() * 1;
        var personBirthMonth = $('#PersonBirthMonth').val() * 1;

        if (selectedYear === currentYear) {
            months.html(fullYearMonthOptions.slice(0, currentMonth + 1));
        } else if (selectedYear === personBirthYear) {
            // merge months after birth month and please select...
            var dropdownOptions = fullYearMonthOptions.slice(personBirthMonth + 1);
            months.html($.merge([fullYearMonthOptions[0]], dropdownOptions));
        } else {
            months.html(fullYearMonthOptions);
        }
        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function criminalconvictionsDateOnSuccess(configuration, data) {
        bgl.components.criminalconvictions.updateSlidePanel(data.html);
        bgl.components.criminalconvictions.subscribeForEventsWizardStep2();
    }

    function criminalconvictionsDateOnError() {
        bgl.components.criminalconvictions.subscribeForEventsWizardStep1();
    }

    function criminalconvictionsConvictionTypeOnSuccess(configuration, data) {
        bgl.components.criminalconvictions.updateSlidePanel(data.html);
        bgl.components.criminalconvictions.subscribeForEventsWizardStep3();
    }

    function criminalconvictionsConvictionTypeOnError() {
        bgl.components.criminalconvictions.subscribeForEventsWizardStep2();
    }

    function criminalconvictionsSentenceTypeOnSuccess(configuration, data) {
        bgl.components.criminalconvictions.updateSlidePanel(data.html);
        if (bgl.components.additionaldriver.isAdditionalDriverJourney()) {
            bgl.components.additionaldriver.initJourneyStep();
        }

        bgl.components.criminalconvictions.init(configuration, true);
    }

    function criminalconvictionsSentenceTypeOnError() {
        bgl.components.criminalconvictions.subscribeForEventsWizardStep3();
    }

    function updateSlidePanel(html) {
        $('#' + slidePanelContainer).html(html);
    }

    function criminalConvictionsnCount() {
        return $('#Criminal-Convictions-List .step-form__summary-item').length;
    }

    return {
        init: initComponent,
        initJourney: initJourney,
        onSuccess: onSuccess,
        onError: onError,
        goToWizardStep: goToWizardStep,
        updateSlidePanel: updateSlidePanel,
        criminalconvictionsDateOnSuccess: criminalconvictionsDateOnSuccess,
        criminalconvictionsDateOnError: criminalconvictionsDateOnError,
        criminalconvictionsConvictionTypeOnError: criminalconvictionsConvictionTypeOnError,
        criminalconvictionsConvictionTypeOnSuccess: criminalconvictionsConvictionTypeOnSuccess,
        criminalconvictionsSentenceTypeOnSuccess: criminalconvictionsSentenceTypeOnSuccess,
        criminalconvictionsSentenceTypeOnError: criminalconvictionsSentenceTypeOnError,
        subscribeForEventsWizardStep1: subscribeForEventsWizardStep1,
        subscribeForEventsWizardStep2: subscribeForEventsWizardStep2,
        subscribeForEventsWizardStep3: subscribeForEventsWizardStep3
    };
})();

bgl.components.medicalcondition = (function () {
    var form,
        configuration,
        containerId = "SlidePanelContent",
        submitButton,
        needToContinue;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#medical-condition');
        submitButton = $(form.find("[type='submit']"));

        bgl.common.validator.init(form, configuration);
    }

    function initComponent(componentConfiguration, resetDirtyFlag) {
        ctor(componentConfiguration);

        initObject();

        bgl.common.datastore.init(form, resetDirtyFlag);
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        initObject();

        bgl.components.additionaldriver.initJourneyStep();
    }

    function initObject() {

        needToContinue = $('#medical-yes').is(':checked') === true;

        subscribeForEvents();
    }

    function subscribeForEvents() {
        if (submitButton.hasClass("hide") === false) {
            $('#medical-yes').off("click")
                .on("click",
                    function () {
                        submitButton.text("Continue");
                    });

            $('#medical-no').off("click")
                .on("click",
                    function () {
                        var journeyType = $("#JourneyType").val() - 0;

                        if (journeyType === 0) {
                            submitButton.text("Done");
                        }
                    });
        } else {
            if (form.find("input[type=radio]:checked").length === 0) {
                form.find("input[type=radio]")
                    .off('keypress', generateClickOnKeypress)
                    .on('keypress', generateClickOnKeypress)
                    .off('click', continueHandler)
                    .on('click', continueHandler);
            } else {
                submitButton.removeClass("hide");
            }
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(event.target).prop("checked", true);
            $(event.target).click();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.content !== undefined) {
            var callbackFunc =
                bgl.components.medicalconditiondvlanotified.getInitByJourneyType($("#JourneyType").val() - 0);

            bgl.common.loader.load(data.content,
                data.componentUrl,
                containerId,
                function () {
                    callbackFunc(configuration, false);
                });
        } else if (data.nextStepUrl) {

            bgl.common.datastore.deleteFields(form, ["MedicalConditionDvlaNotified"]);
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }

            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var initFunc = getInitByJourneyType($("#JourneyType").val() - 0);

        initFunc(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function getInitByJourneyType(journeyType) {
        switch (journeyType) {
            case 1:
                return bgl.components.medicalcondition.initJourney;
            default:
                return bgl.components.medicalcondition.init;
        }
    }

    return {
        init: initComponent,
        initJourney: initJourney,
        onSuccess: onSuccess,
        onError: onError,
        getInitByJourneyType: getInitByJourneyType
    };
})();

bgl.components.medicalconditiondvlanotified = (function () {
    var form,
        configuration,
        containerId = "SlidePanelContent";
    var continueButton;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#medical-condition-dvla-notified');
        continueButton = $(form.find("[type='submit']"));

        bgl.common.validator.init(form);
    }

    function initComponent(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form);

        subscribeForEvents();
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeForEvents();
        bgl.components.additionaldriver.initJourneyStep();
    }

    function subscribeForEvents() {

        if (form.find("input[type=radio]:checked").length === 0 || continueButton.hasClass("hide") === false) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', continueHandler)
                .on('click', continueHandler);
        } else {
            continueButton.removeClass("hide");
        }

        $("#" + configuration.Id + "-back-button")
            .off("click")
            .on("click",
                function (event) {
                    event.preventDefault();

                    var url = $(this).attr("href");

                    var journeyType = $("#JourneyType").val() - 0;

                    var model = {
                        componentConfiguration: configuration,
                        personId: $('#PersonId').val(),
                        medicalCondition: true,
                        firstName: $("#FirstName").val(),
                        journeyType: journeyType,
                        licence: $("#Licence").val()
                    };

                    var callbackFunc = bgl.components.medicalcondition.getInitByJourneyType(journeyType);

                    bgl.common.loader.load(model,
                        url,
                        containerId,
                        function () {
                            callbackFunc(configuration, false);
                        });
                });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(event.target).prop("checked", true);
            $(event.target).click();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.nextStepUrl) {
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }
            bgl.common.datastore.removeSessionStorage(form);
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var initFunc = getInitByJourneyType($("#JourneyType").val() - 0);

        initFunc(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function getInitByJourneyType(journeyType) {
        switch (journeyType) {
            case 1:
                return bgl.components.medicalconditiondvlanotified.initJourney;
            default:
                return bgl.components.medicalconditiondvlanotified.init;
        }
    }

    return {
        init: initComponent,
        initJourney: initJourney,
        onSuccess: onSuccess,
        onError: onError,
        getInitByJourneyType: getInitByJourneyType
    };
})();

bgl.components.contactinformation = (function () {
    var configuration;
    var form;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('[data-component-id="' + configuration.Id + '"] form');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        if (bgl.common.datastore.isDataChanged($(this)) === false) {
                            bgl.common.datastore.removeSessionStorage($(this));
                            $('[data-dismiss="slide-panel"]').click();
                            return;
                        }

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });
    }

    function onSuccess() {
        bgl.common.datastore.removeSessionStorage(form);
        bgl.common.pubsub.emit('PollBasket', { hideNotification: true });
        $('[data-dismiss="slide-panel"]').click();
    }

    function onError() {
        bgl.components.contactinformation.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.contactinformationpolicy = (function () {
    var configuration;
    var form;

    function initComponent(componentConfiguration) {

        configuration = componentConfiguration;
        form = $('[data-component-id="' + configuration.Id + '"] form');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        if (bgl.common.datastore.isDataChanged($(this)) === false) {
                            bgl.common.datastore.removeSessionStorage($(this));
                            bgl.common.contentpanel.hideSlidePanel();
                            return;
                        }

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });
    }

    function onSuccess() {
        bgl.common.datastore.removeSessionStorage(form);
        bgl.common.pubsub.emit('PollBasket');
        window.location.reload(true);
    }

    function onError() {
        bgl.components.contactinformationpolicy.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }



    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.motorconvictions = (function () {
    var configuration,
        motorConvictionList,
        form,
        noButton,
        yesButton,
        HIDE_CLASS = "hide",
        journeyType = 0,
        haveMotorConvictionsBeenChanged = false;
    var hasPreselectedValues;
    var haveMotorConvictionsKey = 'HaveMotorConvictions';

    function ctor(componentConfiguration, hasChanges) {
        configuration = componentConfiguration;

        hasPreselectedValues = true;

        noButton = $("#motor-convictions-radio-no");
        yesButton = $("#motor-convictions-radio-yes");
        form = $('div[id=' + configuration.Id + '-motor-convictions] form');
        motorConvictionList = $('#motor-convictions-list');

        haveMotorConvictionsBeenChanged = hasChanges !== undefined && hasChanges === true;
    }

    function initComponent(componentConfiguration, hasChanges) {
        ctor(componentConfiguration, hasChanges);

        subscribeForEvents();
    }

    function initJourney(componentConfiguration, hasChanges) {
        journeyType = 1;

        ctor(componentConfiguration, hasChanges);

        bgl.common.datastore.init(form, true, true);

        bgl.components.additionaldriver.initJourneyStep();

        if (getMotorConvictionCount() === 0) {
            hasPreselectedValues = false;

            var additionalDriver = bgl.common.datastore.getData(form);

            if (additionalDriver !== null && additionalDriver[haveMotorConvictionsKey] === 'False') {
                noButton.prop('checked', true);
                hasPreselectedValues = true;
            }
        }

        bgl.common.datastore.setupWrite();

        subscribeForEvents();
    }

    function subscribeForEvents() {
        subscribeToEventShowMotorConvictions();

        subscribeToEventDeleteMotorConviction();

        subscribeToEventRemoveAllMotorConvictions();

        subscribeToEventAddMotorConviction();

        subscribeToEventDismissMotorConvictionPanel();

        subscribeToEventDoneButtonMotorConvictions();

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToEventShowMotorConvictions() {
        var radioButtons = $("input[type='radio']");

        var clickFunction;

        if (hasPreselectedValues === true) {
            $('button[data-remove-all-url]').removeClass(HIDE_CLASS);

            clickFunction = function (key, item) {
                var button = $(item);

                button.off("change").on("change",
                    function (event) {
                        event.preventDefault();

                        var itemCount = getMotorConvictionCount();

                        switch ($(this).val()) {
                            case "True":
                                if (itemCount === 0) {
                                    loadAddMotorConvictions($(event.target).data("url"));
                                } else {
                                    $("#motor-convictions-radio-no").attr("data-need-to-delete", "false");
                                    if (motorConvictionList.hasClass(HIDE_CLASS)) {
                                        motorConvictionList.removeClass(HIDE_CLASS);
                                    }
                                }
                                break;
                            case "False":
                                var isNeedToBeDeleted = itemCount !== 0;
                                $("#motor-convictions-radio-no").attr("data-need-to-delete", isNeedToBeDeleted);

                                if (!motorConvictionList.hasClass(HIDE_CLASS)) {
                                    motorConvictionList.addClass(HIDE_CLASS);
                                }
                                break;
                        }
                    });
            }
        } else {
            clickFunction = function (key, item) {
                var button = $(item);

                button.on("change",
                    function (event) {
                        event.preventDefault();

                        switch ($(this).val()) {
                            case "True":
                                if (getMotorConvictionCount() === 0) {
                                    loadAddMotorConvictions($(event.target).data("url"));
                                }
                                break;
                            case "False":
                                redirectToNextJourneyStep();
                                break;
                        }
                    });
            }
        }

        $.each(radioButtons, clickFunction);
    }

    function subscribeToEventAddMotorConviction() {
        $("#motor-convictions-add-another").off("click").on("click",
            function (event) {
                event.preventDefault();

                var url = $(event.target).attr('href');

                loadAddMotorConvictions(url);
            });
    }

    function subscribeToEventDismissMotorConvictionPanel() {
        $(document).off('click.motorconvictions', '[data-dismiss="slide-panel"]').on('click.motorconvictions',
            '[data-dismiss="slide-panel"]',
            function () {
                unSubscribeToEventDismissMotorConvictionPanel();
                dismissSlidePanelHandler(haveMotorConvictionsBeenChanged);
            });

        $(document).off('keyup.motorconvictions').on('keyup.motorconvictions',
            function (event) {
                if (event.key === "Escape") {
                    unSubscribeToEventDismissMotorConvictionPanel();
                    dismissSlidePanelHandler(haveMotorConvictionsBeenChanged);
                }
            });

        $('.overlay').off('click.motorconvictions').on('click.motorconvictions',
            function () {
                unSubscribeToEventDismissMotorConvictionPanel();
                dismissSlidePanelHandler(haveMotorConvictionsBeenChanged);
            });
    }

    function unSubscribeToEventDismissMotorConvictionPanel() {
        $(document).off('click.motorconvictions', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.motorconvictions');
        $('.overlay').off('click.motorconvictions');

    }

    function dismissSlidePanelHandler(flag) {
        if (flag === true) {
            bgl.common.pubsub.emit('PollBasket');
        }
    }

    function subscribeToEventRemoveAllMotorConvictions() {

        var removeAllButton = $("#remove-all-motor-convictions");

        removeAllButton.off('click').on('click',
            function (event) {
                event.preventDefault();
                noButton.click();
            });
    }

    function subscribeToEventDoneButtonMotorConvictions() {
        var doneButton = $('button[data-remove-all-url]');

        doneButton.off('click').on('click',
            function () {
                unSubscribeToEventDismissMotorConvictionPanel();
                var isNeededToDelete = noButton.attr("data-need-to-delete") === 'true';
                var isNoButtonChecked = noButton.is(':checked');
                var patchUrl = $(this).data("remove-all-url");

                if (isNeededToDelete && isNoButtonChecked) {
                    var dataToPost = $.param({
                        __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
                        personId: $("#PersonId").val(),
                        JourneyType: $('#JourneyType').val(),
                        FirstName: $('#FirstName').val()
                    });

                    $.ajax({
                        url: patchUrl,
                        type: "POST",
                        data: dataToPost,
                        success: function (data, textStatus, request) {
                            if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                if (!data.isSuccess && data.errorRedirectUrl !== null) {
                                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                } else if (data.isSuccess) {
                                    if (journeyType === 1) {
                                        redirectToNextJourneyStep();
                                    } else {
                                        bgl.common.pubsub.emit('PollBasket');
                                        $('.overlay').click();
                                    }
                                }
                            }
                        }
                    });
                } else if (haveMotorConvictionsBeenChanged === true && journeyType === journeyTypes.singleEdit) {
                    bgl.common.pubsub.emit('PollBasket');
                    $('.overlay').click();
                } else {
                    if (journeyType === journeyTypes.additionalDriver) {
                        redirectToNextJourneyStep();
                    } else {
                        $('.overlay').click();
                    }
                }
            });
    }

    function redirectToNextJourneyStep() {
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

        bgl.components.additionaldriver.nextStep(configuration,
            {
                personId: $('#PersonId').val(),
                nextStepUrl: form.data('next-step-url')
            },
            form);
    }

    function subscribeToEventDeleteMotorConviction() {
        var deleteMotorConvictionButtons = $("button[data-role='delete']");

        $.each(deleteMotorConvictionButtons,
            function (key, item) {
                $(item).off("click").on("click",
                    function (event) {
                        event.preventDefault();
                        $(item).attr("disabled", "disabled");

                        var target = $(event.target);
                        var link = target.is("button") ? target : target.parent();

                        var token = $("input[name='__RequestVerificationToken']").val();
                        var journeyType = $('#JourneyType').val();
                        var patchUrl = link.attr('href');

                        var personId = $('#PersonId').val();

                        var dataToPost = {
                            __RequestVerificationToken: token,
                            motorConvictionId: link.data("conviction-id"),
                            journeyType: journeyType
                        };

                        var model = {
                            componentConfiguration: configuration,
                            personId: personId,
                            JourneyType: $('#JourneyType').val(),
                            FirstName: $('#FirstName').val()
                        };

                        var panelId = configuration.Id + "-motor-convictions";

                        $.ajax({
                            url: patchUrl,
                            type: "POST",
                            data: dataToPost,
                            success: function (data, textStatus, request) {
                                if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                    if (!data.isSuccess) {
                                        bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                    } else {
                                        bgl.common.loader.load(model,
                                            data.redirectUrl,
                                            panelId,
                                            function () {
                                                if (($('#JourneyType').val() * 1) === 1) {

                                                    if (getMotorConvictionCount() === 0) {
                                                        var additionalDriver = JSON.parse(sessionStorage.getItem(additionalDriverKey));
                                                        additionalDriver[haveMotorConvictionsKey] = 'False';
                                                        sessionStorage.setItem(additionalDriverKey, JSON.stringify(additionalDriver));
                                                    }

                                                    bgl.components.motorconvictions.initJourney(configuration, true);
                                                } else {
                                                    bgl.components.motorconvictions.init(configuration, true);
                                                }
                                            });
                                    }
                                }
                            }
                        });
                    });
            });
    }

    function loadAddMotorConvictions(url) {
        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            JourneyType: $('#JourneyType').val(),
            FirstName: $('#FirstName').val()
        };

        var panelId = configuration.Id + "-motor-convictions";

        bgl.common.loader.load(model,
            url,
            panelId,
            function () {
                bgl.components.motorconvictionsbuilder.init(configuration);
            });
    }

    function getMotorConvictionCount() {
        return $('#motor-convictions-list .step-form__summary-item').length;
    }

    return {
        init: initComponent,
        initJourney: initJourney
    };

})();

bgl.components.motorconvictionsbuilder = (function () {
    var form,
        configuration,
        HIDE_CLASS = "hide";

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#add-motor-conviction');

        if (form) {
            bgl.common.validator.init(form);
            bgl.common.validator.initNumberInputField($("#Months-Ban"), 2);
            bgl.common.validator.initNumberInputField($("#Fine-Amount"), 5);
        }

        subscribeForEvents();
    }

    function subscribeForEvents() {
        $('.step-form__button, .form-row__breadcrumb-link').off('click').on('click',
            function (event) {
                stepClick(event, false);
            });

        $('[data-step-type="previous-step"]').off('click').on('click',
            function (event) {
                stepClick(event, true);
            });

        function stepClick(event, isBackButtonClicked) {
            event.preventDefault();
            var link = $(event.target);
            selectNextStep(link, isBackButtonClicked);
        }

        var banLengthRow = $('#ban-length-row');

        $('#conviction-has-ban-no').on("click",
            function () {
                if (!banLengthRow.hasClass(HIDE_CLASS)) {
                    banLengthRow.addClass(HIDE_CLASS);
                }
                $("#Months-Ban").val("");
            });

        $('#conviction-has-ban-yes').on("click",
            function () {
                if (banLengthRow.hasClass(HIDE_CLASS)) {
                    banLengthRow.removeClass(HIDE_CLASS);
                }
            });

        if (form) {
            form.off('submit').on('submit',
                function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    var submit = $(this).find(':submit');
                    submit.attr('disabled', 'disabled');

                    if (bgl.common.validator.validate(configuration, form) === false) {
                        submit.removeAttr('disabled');
                    }
                });
        }
    }

    function selectNextStep(link, isBack) {
        var nextStep = link.data("next-step");
        var currentStep = $("#motor-conviction-tiles").data("current-step");
        var journeyType = $('#JourneyType').val() * 1;

        var currentValue = {
            Code: link.data('code'),
            Value: $.trim(link.text()),
            BuilderStep: currentStep
        };

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            JourneyType: journeyType,
            FirstName: $('#FirstName').val(),
            CurrentStep: nextStep
        };

        if (isBack) {
            currentValue = undefined;
            model.IsBackButtonClicked = true;
        }

        model.SelectedValues = getSelectedValues(currentValue);

        var url = link.attr('href');
        var panelId = configuration.Id + "-motor-convictions";

        if (link.data("current-step") === "Type" && isBack === true) {
            bgl.common.loader.load(model,
                link.attr('href'),
                panelId,
                function () {
                    if ($('#JourneyType').val() * 1 === 1) {
                        bgl.components.motorconvictions.initJourney(configuration, true);
                    } else {
                        bgl.components.motorconvictions.init(configuration, true);
                    }
                });

            return;
        }

        bgl.common.loader.load(model,
            url,
            panelId,
            function () {
                bgl.components.motorconvictionsbuilder.init(configuration);
            });
    }

    function getSelectedValues(currentValue) {
        var result = [];
        var breadcrumbLinks = $('.form-row__breadcrumb-link') || [];

        $.each(breadcrumbLinks,
            function (idx, element) {
                var value = {
                    Code: $(element).data('code'),
                    Value: $.trim($(element).text()),
                    BuilderStep: $(element).data('next-step')
                };

                result.push(value);
            });

        if (currentValue !== undefined) {
            result.push(currentValue);
        }

        return result;
    }

    function onSuccess(configuration, data) {

        var panelId = configuration.Id + "-motor-convictions";
        var journeyType = $('#JourneyType').val() * 1;

        if (data.lastStep === true) {
            var model = {
                ComponentConfiguration: configuration,
                PersonId: $('#PersonId').val(),
                JourneyType: journeyType,
                FirstName: $('#FirstName').val()
            };

            bgl.common.loader.load(model,
                data.redirectUrl,
                panelId,
                function () {
                    if (journeyType === 1) {
                        bgl.components.motorconvictions.initJourney(configuration, true);
                    } else {
                        bgl.components.motorconvictions.init(configuration, true);
                    }
                });
        } else {
            var newModel = JSON.parse(data.content);

            configuration = newModel.ComponentConfiguration;

            bgl.common.loader.load(newModel,
                data.redirectUrl,
                panelId,
                function () {
                    bgl.components.motorconvictionsbuilder.init(configuration);
                });
        }
    }

    function onError() {
        bgl.components.motorconvictionsbuilder.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.useofothervehicles = (function () {
    var form;
    var configuration;
    var submitButton;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#' + configuration.Id + '__other-vehicles-form');
        submitButton = $(form.find("[type='submit']"));

        bgl.common.validator.init(form);
    }

    function initComponent(componentConfiguration) {
        var isOtherVehiclesNcd = componentConfiguration.IsOtherVehiclesNcd;

        ctor(componentConfiguration);

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupWrite();

        subscribeToEvents();

        if (isOtherVehiclesNcd) {
            initElements();
            subscribeToEventsForNcd();
        }
    }

    function initJourney(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeToEvents();
        bgl.components.additionaldriver.initJourneyStep();
    }

    function initElements() {
        form.find('#use-of-other-vehicles-radio-yes, #Other-Vehicle-Yes').each(function (index, element) {
            var sections = $(element).data('sections');

            if ($(element).prop('checked')) {
                $(sections).removeClass('hide');
            }
        });
    }

    function subscribeToEvents() {
        if (submitButton.hasClass("hide") !== false) {
            if (form.find("input[type=radio]:checked").length === 0) {
                form.find("input[type=radio]")
                    .off('keypress', generateClickOnKeypress)
                    .on('keypress', generateClickOnKeypress)
                    .off('click', continueHandler)
                    .on('click', continueHandler);
            } else {
                submitButton.removeClass("hide");
            }
        }

        subscribeForSubmit();
    }

    function subscribeToEventsForNcd() {
        form.find('#use-of-other-vehicles-radio-yes, #Other-Vehicle-Yes').on('click',
            function (e) {
                var sections = $(this).data('sections');

                $(sections).removeClass('hide');
            });

        form.find('#use-of-other-vehicles-radio-no, #Other-Vehicle-No').on('click',
            function (e) {
                var sections = $(this).data('sections');

                $(sections).addClass('hide');

                deleteFields(sections);
            });
    }

    function deleteFields(sections) {
        var elements = $(sections).removeClass('error').find('[data-store]');
        var dropdownKey;

        var fieldsToDelete = $.map(elements,
            function (element) {

                var elementName = $(element).attr('name');

                if ($(element).is('select')) {
                    $(element).val("");
                    dropdownKey = elementName;
                } else if ($(element).is('input')) {
                    $(element).prop('checked', false);
                }

                return elementName;
            });

        if (dropdownKey) {
            fieldsToDelete.push(dropdownKey + "Text", dropdownKey + "Val");
        }

        bgl.common.datastore.deleteFields(form, fieldsToDelete);
    }


    function subscribeForSubmit() {

        form.off('submit')
            .on('submit',
                function(event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid(form)) {
                        var journeyType = $("#JourneyType").val() - 0;

                        if (!bgl.common.datastore.isDataChanged(form) && journeyType !== journeyTypes.additionalDriver) {
                            bgl.common.datastore.removeSessionStorage(form);
                            $('[data-dismiss="slide-panel"]').click();
                        } else {
                            bgl.common.validator.submitForm(form, configuration);
                        }
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(event.target).prop("checked", true);
            $(event.target).click();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }


    function onSuccess(configuration, data) {

        if (data.nextStepUrl) {
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.components.additionaldriver.nextStep(configuration, data, form);
        } else {
            bgl.common.datastore.removeSessionStorage(form);
            bgl.common.pubsub.emit('PollBasket');
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
        case (journeyTypes.additionalDriver):
            bgl.components.useofothervehicles.initJourney(configuration);
            break;
        default:
            bgl.components.useofothervehicles.init(configuration);
            break;
        }

        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        initJourney: initJourney
    };
})();

bgl.components.maritalstatus = (function () {
    var form;
    var configuration;
    var containerId;

    function initComponent(componentConfiguration, containerElementId) {
        containerId = containerElementId;
        initObject(componentConfiguration);

        bgl.common.datastore.init(form, false, true);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });
    }

    function initJourney(componentConfiguration) {
        initObject(componentConfiguration);

        bgl.components.additionaldriver.initJourneyStep();

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', postPerson)
                .on('click', postPerson);
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
            continueButton.off("click").on("click", postPerson);
        }
    }

    function initSurnameStep(componentConfiguration) {
        initObject(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();
    }

    function initObject(componentConfiguration) {
        configuration = componentConfiguration;
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        $('.back-button-link').one("click", function (event) {
            event.preventDefault();

            var backUrl = $(this).attr("href");

            $(this).hide();

            var elementsArray = form.serializeArray();

            var model = {
                componentConfiguration: configuration
            };

            $.map(elementsArray, function (item, i) {
                model[item['name']] = item['value'];
            });

            bgl.common.loader.load(model, backUrl, containerId,
                function () {
                    initComponent(configuration, containerId);
                });
        });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function postPerson(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.find("input[type=radio]").off();
        form.submit();
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(this).click();
        }
    }

    function onSuccess(configuration, data) {
        if (data.journeyType === "AdditionalDriver" || data.journeyType === "MultiStepEdit") {
            if (data.nextStep) {
                bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });
                bgl.components.additionaldriver.nextStep(configuration, data, form);
            } else {
                bgl.components.additionaldriver.createPerson(configuration, data.nextStepUrl, form);
            }
        } else {
            bgl.common.loader.load(data.content, data.nextStepUrl, containerId,
                function () {
                    bgl.components.maritalstatus.initNextStep(configuration, containerId);
                });
        }
    }

    function onSurnameSuccess(configuration, data) {
        bgl.common.datastore.removeSessionStorage(form);

        if (configuration.IsService) {
            if (configuration.IsInitialChange) {
                if (data.hasChanges === true) {
                    sessionStorage.setItem("MTAStartPage", window.location.href);
                    bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                } else {
                    bgl.common.contentpanel.hideSlidePanel();
                }
            } else {
                bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
            }
        } else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit('PollBasket');
            }

            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError() {
        var journeyType = $("#JourneyType").val() - 0;

        switch (journeyType) {
            case 1:
                bgl.components.maritalstatus.initJourney(configuration);
                break;
            default:
                bgl.components.maritalstatus.init(configuration);
                break;
        }
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function onSurnameError() {
        bgl.components.maritalstatus.initNextStep(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        initJourney: initJourney,
        initNextStep: initSurnameStep,
        init: initComponent,
        onSuccess: onSuccess,
        onSurnameSuccess: onSurnameSuccess,
        onError: onError,
        onSurnameError: onSurnameError
    };
})();

bgl.components.relationshipstatus = (function () {
    var configuration;
    var form;

    function initJourney(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("#relationship-status-form");

        bgl.common.validator.init(form);

        bgl.components.additionaldriver.initJourneyStep();

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', submitRelationshipStatusForm)
                .on('click', submitRelationshipStatusForm);
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
            continueButton.off("click").on("click", submitRelationshipStatusForm);
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        bgl.common.validator.submitForm(form, configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(this).click();
        }
    }

    function submitRelationshipStatusForm(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.find("input[type=radio]").off();
        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.nextStepUrl) {
            bgl.components.additionaldriver.nextStep(configuration, data, form);
        }
    }

    function onError(configuration) {
        bgl.components.relationshipstatus.initJourney(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        initJourney: initJourney,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.additionaldriver = (function () {
    var configuration;
    var containerId = slidePanelContainer;
    var additionalDriverKey = "AdditionalDriver";

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        sessionStorage.removeItem(additionalDriverKey);

        subscribeToEventDismissPanel();

        bgl.components.relationshipstatus.initJourney(configuration);
    }

    function initJourneyStep() {

        var backButton = $('a[data-step-type="previous-step-journey"]');

        if (backButton.length !== 0) {
            backButton.off('click').one('click', function (event) {
                event.preventDefault();

                $(backButton).hide();

                var form = $(this).closest("form");

                if (!form.length) {
                    form = $("#" + containerId).find('form');
                }

                var componentName = $(this).data("component");

                var data = getDataToPost(form);

                if (componentName === "relationshipstatus" || componentName === "motorclaims") {
                    var drivers = {
                        Drivers: JSON.parse(sessionStorage.getItem("drivers")),
                        GetPersons: false
                    };

                    data = $.extend(data, drivers);
                }

                var element = bgl.components[componentName];

                var callback = element.initJourney;

                bgl.common.loader.load(data,
                    $(this).attr('href'),
                    containerId,
                    function () {
                        callback(configuration, containerId);
                        initJourneyStep();
                    });
            });
        }
    }

    function getDataToPost(form) {
        var model = {
            ComponentConfiguration: configuration,
            JourneyType: journeyTypes.additionalDriver,
            PersonId: $('#PersonId').val(),
            FirstName: $('#FirstName').val()
        };

        var key = additionalDriverKey;

        if (form && form.length) {
            key = form.data('store-key');
        }

        var collectedData = JSON.parse(sessionStorage.getItem(key));

        model = $.extend(model, collectedData);

        return model;
    }

    function expandSessionStorageWithPersonDriver(personId) {
        var driversData = JSON.parse(sessionStorage.getItem("AdditionalDriver"));
        var drivers = JSON.parse(sessionStorage.getItem("drivers"));

        var initials = driversData.FirstName + " " + driversData.LastName;
        var person = { Code: personId, Value: convertNameToTitleCase(initials), IsMainDriver: false };
        var smb = { Code: "", Value: "Someone else driving", IsMainDriver: false };

        var newdrivers = drivers.filter(function (value) {
            return value.Code !== personId && value.Code !== "";
        });

        newdrivers.push(person);
        newdrivers.push(smb);

        sessionStorage.setItem("drivers", JSON.stringify(newdrivers));
    }

    function convertNameToTitleCase(inputStr) {
        if (inputStr) {
            inputStr = inputStr.trim().toLowerCase().split(" ");
            for (var i = 0; i < inputStr.length; i++) {
                inputStr[i] = inputStr[i].charAt(0).toUpperCase() + inputStr[i].slice(1);
            }
            return inputStr.join(" ");
        }

        return "";
    }

    function nextStep(configuration, data, form) {
        if (data.personId) {
            var dataToExtend = { PersonId: data.personId, isFormDirty: false };
            expandSessionStorageWithPersonDriver(data.personId);

            var formData = JSON.parse(sessionStorage.getItem(additionalDriverKey));
            formData = $.extend(formData, dataToExtend);
            sessionStorage.setItem(additionalDriverKey, JSON.stringify(formData));
        }

        if (data.nextStepUrl && form) {
            var componentName = form.data("next-journey-component");

            var storedDate = getDataToPost(form);

            if (componentName === "motorclaims") {
                var drivers = {
                    Drivers: JSON.parse(sessionStorage.getItem("drivers")),
                    GetPersons: false
                };

                storedDate = $.extend(storedDate, drivers);
            }

            var element = bgl.components[componentName];

            var callback = element.initJourney;

            bgl.common.loader.load(storedDate,
                data.nextStepUrl,
                containerId,
                function () {
                    callback(configuration, containerId);
                });
        } else {
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function createPerson(configuration, nextUrl, form) {
        var dataToPost = getDataToPost();
        executeCreatingAction(configuration, nextUrl, form, dataToPost);
    }

    function createDriver(configuration, nextUrl, form) {
        var dataToPost = getDataToPost();
        var driverData = {
            ComponentConfiguration: dataToPost.ComponentConfiguration,
            PersonId: dataToPost.PersonId,
            CoverId: $("#CoverId").val()
        };

        unSubscribeToEventDismissPanel();

        executeCreatingAction(configuration, nextUrl, form, driverData);
    }

    function executeCreatingAction(configuration, nextUrl, form, dataToPost) {
        dataToPost.__RequestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

        form.attr("action", nextUrl);

        bgl.common.validator.submitForm(form, configuration, dataToPost);
    }

    function subscribeToEventDismissPanel() {
        $(document).off('click.additionaldriver', '[data-dismiss="slide-panel"]').on('click.additionaldriver', '[data-dismiss="slide-panel"]', function () {
            unSubscribeToEventDismissPanel();
            dismissSlidePanelHandler();
        });

        $(document).off('keyup.additionaldriver').on('keyup.additionaldriver', function (event) {
            if (event.key === "Escape") {
                unSubscribeToEventDismissPanel();
                dismissSlidePanelHandler();
            }
        });

        $('.overlay').off('click.additionaldriver').on('click.additionaldriver', function () {
            unSubscribeToEventDismissPanel();
            dismissSlidePanelHandler();
        });
    }

    function unSubscribeToEventDismissPanel() {
        $(document).off('click.additionaldriver', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.additionaldriver');
        $('.overlay').off('click.additionaldriver');
    }

    function dismissSlidePanelHandler() {
        switch (configuration.JourneyType) {
            case journeyTypes.additionalDriver:
                break;

            case journeyTypes.multiStepEdit:
                bgl.common.utilities.refreshPage();
                break;
        }
    }

    function isAdditionalDriverJourney() {
        return $('#JourneyType').val() - 0 === journeyTypes.additionalDriver;
    }

    return {
        init: initComponent,
        initJourneyStep: initJourneyStep,
        nextStep: nextStep,
        createPerson: createPerson,
        createDriver: createDriver,
        isAdditionalDriverJourney: isAdditionalDriverJourney
    };
})();

bgl.components.drivertabs = (function () {
    /*SELECTED ELEMENTS (CACHED)*/
    var leftArrow, rightArrow, tabScrollVisibleArea, tabList, tabContent;

    function init() {
        leftArrow = $('.named-driver-pagination__left');
        rightArrow = $('.named-driver-pagination__right');
        tabScrollVisibleArea = $('.named-driver-scroller__scroll-area');
        tabList = $('.named-driver-tab');
        tabContent = $('.named-driver-table');

        /*ATTACH EVENT LISTENERS*/

        tabScrollVisibleArea.off('scroll').on('scroll',
            function () {
                bgl.components.drivertabs.showArrow();
            });

        rightArrow.off('click').on('click',
            function () {
                handleRightArrowClick();
                bgl.components.drivertabs.showArrow();
            });

        leftArrow.off('click').on('click',
            function () {
                handleLeftArrowClick();
                bgl.components.drivertabs.showArrow();
            });

        $(window).off('resize').on('resize',
            function () {
                bgl.components.drivertabs.showArrow();
            });

        tabList.each(function (index, tab) {
            $(tab).off('click').on('click',
                function () {
                    handleTabClick($(this));
                    bgl.components.drivertabs.showArrow();
                });
        });

        bgl.components.drivertabs.showArrow();
    }

    var settings = {
        ANIMIATE_DURATION: 300
    };

    function deactivateTab() {
        var currentlyActiveTab = $('.named-driver-tab--active');
        $(currentlyActiveTab).removeClass('named-driver-tab--active');
        $(currentlyActiveTab).attr('aria-selected', 'false');
    }

    function activateTab(clickedTab) {
        /*remove previously active tab*/
        deactivateTab();

        /*select selected tab to active*/
        clickedTab.addClass('named-driver-tab--active');
        clickedTab.attr('aria-selected', 'true');
        clickedTab.focus();

        /*TODO: Show associate tab-content-panel*/
        var contentIndex = $(tabList).index(clickedTab);

        showRelatedTabContent(contentIndex);
    }

    function showRelatedTabContent(contentIndex) {
        $(tabContent).each(function () {
            $(this).addClass('hide');
        });
        $(tabContent[contentIndex]).removeClass('hide');
    }

    function getLeftmostTabIndex() {
        var hiddenAreaLeft = $(tabScrollVisibleArea)[0].scrollLeft;
        var widthAccumulator = 0;
        var closestToLeftIndex = 0;
        $(tabList).each(function (index) {

            if (widthAccumulator > hiddenAreaLeft) {
                /*return false (break) to exit .each()*/
                return false;
            }
            widthAccumulator += $(this)[0].offsetWidth;
            closestToLeftIndex = index;
        });

        return closestToLeftIndex;
    }

    function getRightmostTabIndex() {
        var totalScrollArea = $(tabScrollVisibleArea)[0].offsetWidth;
        var hiddenAreaLeft = $(tabScrollVisibleArea)[0].scrollLeft;
        var rootRight = totalScrollArea + hiddenAreaLeft;
        var widthAccumulator = 0;
        var closestToRightIndex = 0;
        $(tabList).each(function (index) {
            if (widthAccumulator > rootRight) {
                /*return false (break) to exit .each()*/
                return false;
            }
            widthAccumulator += $(this)[0].offsetWidth;
            closestToRightIndex = index;
        });
        return closestToRightIndex;
    }

    function getRightmostTabScrollPosition(tabIndex) {
        var accumulator = 0;
        $(tabList).each(function (index) {
            if (index > tabIndex) {
                /*return false (break) to exit .each()*/
                return false;
            }
            accumulator += $(this)[0].offsetWidth;
        });
        return accumulator;
    }

    function handleTabClick(clickedTab) {

        var previousActiveTabIndex = $(tabList).index($('.named-driver-tab--active'));
        var clickedTabIndex = $(tabList).index(clickedTab);
        var tabScrollerVisibleAreaWidth = $(tabScrollVisibleArea)[0].offsetWidth;
        var leftmostTabIndex = getLeftmostTabIndex();
        var rightmostTabIndex = getRightmostTabIndex();

        // If the tab is NOT already active
        if (previousActiveTabIndex !== clickedTabIndex) {
            activateTab(clickedTab);
        }

        // If the tab is already within the visible scroll area, nothing else to do so, exit
        if (clickedTabIndex > getLeftmostTabIndex && clickedTabIndex < getRightmostTabIndex) {
            return; // Early exit - no scroll required
        }

        var newScrollPosition;
        // If clicked tab is in the leftmost position, bring entire tab into view?
        if (clickedTabIndex === leftmostTabIndex) {
            newScrollPosition = $(tabList)[clickedTabIndex].offsetLeft;
            $(tabScrollVisibleArea).animate({ scrollLeft: newScrollPosition }, settings.ANIMIATE_DURATION);
            return;
        }

        // If clicked tab is in the rightmost position, bring entire tab into view?
        if (clickedTabIndex === rightmostTabIndex) {
            newScrollPosition = getRightmostTabScrollPosition(rightmostTabIndex) - tabScrollerVisibleAreaWidth;
            $(tabScrollVisibleArea).animate({ scrollLeft: newScrollPosition }, settings.ANIMIATE_DURATION);
            return;
        }
    }

    function handleLeftArrowClick() {
        var newIndex = getLeftmostTabIndex();
        var tabScrollerVisibleAreaWidth = $(tabScrollVisibleArea)[0].offsetWidth;
        var newPosition = getRightmostTabScrollPosition(newIndex) - tabScrollerVisibleAreaWidth;
        $(tabScrollVisibleArea).animate({ scrollLeft: newPosition }, settings.ANIMIATE_DURATION, function () {
            /* offset may not change when name is too long*/
            if (newIndex === getLeftmostTabIndex()) {
                var position = getRightmostTabScrollPosition(newIndex - 1) - tabScrollerVisibleAreaWidth;
                $(tabScrollVisibleArea).animate({ scrollLeft: position }, settings.ANIMIATE_DURATION);
            }
        });
    }

    function handleRightArrowClick() {
        var newIndex = getRightmostTabIndex();
        var tabScrollerVisibleAreaWidth = $(tabScrollVisibleArea)[0].offsetWidth;
        var newPosition = getRightmostTabScrollPosition(newIndex) - tabScrollerVisibleAreaWidth;
        $(tabScrollVisibleArea).animate({ scrollLeft: newPosition }, settings.ANIMIATE_DURATION, function () {
            /* offset may not change when name is too long*/
            if (newIndex === 0 || newIndex === getRightmostTabIndex()) {
                var position = getRightmostTabScrollPosition(newIndex + 1) - tabScrollerVisibleAreaWidth;
                $(tabScrollVisibleArea).animate({ scrollLeft: position }, settings.ANIMIATE_DURATION);
            }
        });
    }

    function showArrow() {
        var tabScrollerVisibleAreaScrollPosition = $('.named-driver-scroller__scroll-area:eq(0)').scrollLeft();
        var tabScrollerVisibleAreaWidth = $('.named-driver-scroller__scroll-area:eq(0)').width();
        var tabScrollerContentWidth = $('.named-driver-scroller__scroll-content:eq(0)').width();

        if (tabScrollerVisibleAreaScrollPosition > 0) {
            $(leftArrow).removeClass('hide');
        } else {
            $(leftArrow).addClass('hide');
        }

        if (tabScrollerVisibleAreaWidth < tabScrollerContentWidth - tabScrollerVisibleAreaScrollPosition) {
            $(rightArrow).removeClass('hide');
        } else {
            $(rightArrow).addClass('hide');
        }
    }

    function setActiveDriver(driverIndex) {
        $('.named-driver-tab[data-index=' + driverIndex + ']').click();
    }

    function getActiveDriver(thisObj) {
        return thisObj.data('index');
    }

    return {
        init: init,
        handleTabClick: handleTabClick,
        showArrow: showArrow,
        setActiveDriver: setActiveDriver,
        getActiveDriver: getActiveDriver
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.driverdetails.common;
}
