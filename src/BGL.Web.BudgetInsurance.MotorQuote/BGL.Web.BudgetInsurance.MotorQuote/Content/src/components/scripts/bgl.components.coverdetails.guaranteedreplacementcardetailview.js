﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.guaranteedreplacementcardetailview = (function() {
    var configuration,
        form;
    var selectedValue;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function() {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        form = $("#guaranteed-replacement-car-form");
        configuration = componentConfiguration;
        if (configuration.DetailUrl !== null) {
            sessionStorage.setItem(configuration.SelectedAddon, configuration.DetailUrl);
        }
        changeNextButtonText();

        var coverLevelCount = $("#grc-coverlevels-count").val();
        if (coverLevelCount !== undefined && coverLevelCount !== "1") {
            displaySelectedCoverLevelFbels($("#SelectedCoverLevelCode").val());
            onCoverLevelChange();
        }

        shouldRadioBeSelectedOnLoad();
        onRadioButtonClick();
        onNextButtonClick();
        bgl.components.coverdetails.common.onAddonBackButtonClick();
        form.off('submit')
            .on('submit',
                function(event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function changeNextButtonText() {
        var nextaddonvalue = $("#next-addon").val();

        $('#next-button-span').text(nextaddonvalue ? "Optional Extras" : "Review details");

    }

    function shouldRadioBeSelectedOnLoad() {
        if ($('.form-row__input--radio').is(':checked')) {
            selectedValue = $('input[name=IsAddonSelected]:checked').val();
            var isValueSetForGRC = sessionStorage.getItem("GRC-Selected");
            if (selectedValue === "No" && isValueSetForGRC === null) {
                $('.form-row__input--radio').prop('checked', false);
            }
            if (selectedValue === "Yes") {
                sessionStorage.setItem("GRC-Selected", true);
            }
        }
    }

    function onNextButtonClick() {
        $("#next-button").on("click",
            function(e) {
                e.preventDefault();
                var nextaddonvalue = $("#next-addon").val();
                if ($('.form-row__input--radio').is(':checked')) {
                    if (nextaddonvalue) {
                        var nextaddonNonSelected = sessionStorage.getItem(("NonSelectedAddon").concat(nextaddonvalue));
                        var nextAddonSelected = sessionStorage.getItem(("SelectedAddon").concat(nextaddonvalue));
                        if (nextaddonNonSelected || nextAddonSelected) {
                            if (nextAddonSelected) {
                                window.location.href = nextAddonSelected;
                            } else {
                                window.location.href = nextaddonvalue;
                            }
                        } else {
                            var nextaddonDetailUrl = sessionStorage.getItem(nextaddonvalue);
                            if (nextaddonDetailUrl) {
                                window.location.href = nextaddonDetailUrl;
                            } else {
                                window.location.href = nextaddonvalue;
                            }
                        }
                    } else {
                        window.location.href = configuration.NextStepUrl;
                    }
                } else {
                    displayValidationMessage();
                }
            });
    }

    function onRadioButtonClick() {
        $(".form-row__input--radio").off("change").on("change",
            function() {
                if ($('#button-panel').hasClass('error-text__heading')) {
                    removeValidationMessage();
                }
                bgl.common.focusHolder.setFocusedElement($(this).data("focusid"));
                var id = $(this).prop('id');
                var isGrcAdded = sessionStorage.getItem("GRC-Selected");
                if (id === "GRC-No") {
                    if (isGrcAdded === "true") {
                        submitForm();
                    }
                    sessionStorage.setItem("GRC-Selected", false);
                } else if (id === "GRC-Yes") {
                    if (isGrcAdded === "false" || isGrcAdded === null) {
                        submitForm();
                    }
                    sessionStorage.setItem("GRC-Selected", true);
                }
            });
    }

    function removeValidationMessage() {
        $('#addon-questions').removeClass('addon-question-error');
        $('#button-panel').removeClass('error-text__heading');
        $('.form-row__validation-text').hide();
    }

    function displayValidationMessage() {
        $('#addon-questions').addClass('addon-question-error');
        $('#button-panel').addClass('error-text__heading');
        $('.form-row__validation-text').show();
    }

    function submitForm() {
        form.submit();
        setLinksDisabled(true);
    }

    function onCoverLevelChange() {
        $("#SelectedCoverLevelCode").off("change").on("change",
            function () {
                displaySelectedCoverLevelPrice($(this).val());
                displaySelectedCoverLevelFbels($(this).val());
                if ($('.form-row__input--radio').is(':checked')) {
                    if ($('input[name=IsAddonSelected]:checked').val() === "Yes") {
                        submitForm();
                    }
                }
            });
    }

    function displaySelectedCoverLevelPrice(selectedValue) {
        $('#' + selectedValue).removeClass('hide');
        $('#' + selectedValue + 'bottom').removeClass('hide');
        $('.addon-radio:not(#' + selectedValue + ')').addClass('hide');
        $('.addon-radio-bottom:not(#' + selectedValue + 'bottom)').addClass('hide');
    }

    function displaySelectedCoverLevelFbels(selectedValue) {
        var flebsCount = $('#' + configuration.Id).find('.fbels').length > 1;

        if (flebsCount) {
            $('#' + selectedValue + '-fbels').removeClass('hide');
            $('.fbels:not(#' + selectedValue + '-fbels)').addClass('hide');
        }
    }

    function successCallback(message) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: message
        });

        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        var message = data.houstonMessage || data.message;

        if (message) {
            var params = {
                callback: function() {
                    successCallback(message);
                },

                spinnerName: "addonSpinner"
            };

            bgl.common.pubsub.emit("PollBasket", params);
        } else {
            bgl.common.pubsub.emit("PollBasket");
        }
        setLinksDisabled(false);
    }

    function setLinksDisabled(disabled) {
        var elements = $('button, a, select, input, label, [data-tier], .shopping-basket, #Houston');
        elements.prop("disabled", disabled);

        if (disabled) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        }
    }

    function onError() {
        setLinksDisabled(false);
        init(configuration);
    }


    return {
        onSuccess: onSuccess,
        init: initComponent,
        load: loadComponent,
        setLinksDisabled: setLinksDisabled,
        onError: onError
    };
})();