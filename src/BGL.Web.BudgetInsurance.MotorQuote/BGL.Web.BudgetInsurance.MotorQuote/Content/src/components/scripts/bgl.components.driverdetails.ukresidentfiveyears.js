﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.driverdetails.ukresidentfiveyears = (function () {
    var form,
        configuration,
        residencyStartDateDropdowns,
        fullYearMonthOptions,
        currentYearMonthOptions,
        firstYearMonthOptions,
        monthDropdown,
        yearDropdown,
        HIDE_CLASS = "hide",
        hasPreselectedValues = false;

    function initComponent(componentConfiguration) {
        initObject(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeForEvents();

        updateMonthDropdown();
    }

    function initObject(componentConfiguration) {
        var currentMonth = $('#CurrentMonth').val() * 1;

        configuration = componentConfiguration;
        form = $('#uk-resident-five-years');
        residencyStartDateDropdowns = form.find('#residency-start-date-dropdowns');
        monthDropdown = form.find('#Resident-Since-Month');
        yearDropdown = form.find('#Resident-Since-Year');

        bgl.common.validator.init(form);

        fullYearMonthOptions = monthDropdown.find('option');
        currentYearMonthOptions = fullYearMonthOptions.slice(0, currentMonth + 1);
        firstYearMonthOptions = fullYearMonthOptions.slice(0);
        firstYearMonthOptions.splice(1, currentMonth - 1);
    }

    function subscribeForEvents() {

        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

        if (hasPreselectedValues) {
            showContinueButton();

            var selectedButton = $(form.find("input[type=radio]:checked")[0]);
            switch (selectedButton.val().toLowerCase()) {
                case "false":
                    if (residencyStartDateDropdowns.hasClass(HIDE_CLASS)) {
                        residencyStartDateDropdowns.removeClass(HIDE_CLASS);
                    }
                break;
            }
        }

        form.find("input[type=radio]").off('click').on('click',
            function (event) {
                $(event.target).prop("checked", true);

                updateResidencyStartDateVisibility();

                if (hasPreselectedValues !== true) {
                    continueHandler(event);
                }
            });

        yearDropdown.on("change", updateMonthDropdown);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function updateResidencyStartDateVisibility() {

        var value = form.find('input[type=radio]:checked').val();

        if (value.toLowerCase() === "false") {
            if (residencyStartDateDropdowns.hasClass(HIDE_CLASS)) {
                residencyStartDateDropdowns.removeClass(HIDE_CLASS);
            }

            hasPreselectedValues = true;
        } else {
            if (!residencyStartDateDropdowns.hasClass(HIDE_CLASS)) {
                residencyStartDateDropdowns.addClass(HIDE_CLASS);
            }
        }

        if (hasPreselectedValues === true) {
            showContinueButton();
        }
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $('#CurrentYear').val();
        var firstYear = (currentYear - 5).toString();

        switch (yearDropdown.val()) {
            case currentYear:
                months.html(currentYearMonthOptions);
                break;
            case firstYear:
                months.html(firstYearMonthOptions);
                break;
            default:
                months.html(fullYearMonthOptions);
                break;
        };

        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function onSuccess(configuration, data) {
        if (data.isUkResident === true) {
            bgl.common.datastore.deleteFields(form,
                [
                    "UkResidentYear", "UkResidentYearVal", "UkResidentYearText",
                    "UkResidentMonth", "UkResidentMonthVal", "UkResidentMonthText"
                ]);
        }

        if (data.nextDtsPageUrl) {
            bgl.common.utilities.redirectToUrl(data.nextDtsPageUrl);
        }
    }

    function onError() {
        bgl.components.driverdetails.ukresidentfiveyears.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
