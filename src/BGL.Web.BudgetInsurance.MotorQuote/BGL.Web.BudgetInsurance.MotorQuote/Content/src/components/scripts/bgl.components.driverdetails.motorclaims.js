﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;
var slidePanelContainer = 'SlidePanelContent';
var hasClaims = 'HasClaims';

bgl.components.driverdetails.motorclaims = (function () {
    var configuration,
        motorClaimList,
        form,
        doneButton,
        radioButtons,
        HIDE_CLASS = "hide";
    var addNewClaim;

    function initComponent(componentConfiguration) {
        form = $("#" + componentConfiguration.Id + "-motor-claims-form");
        doneButton = $('button[data-remove-all-url]');
        radioButtons = $("input[type='radio']");
        configuration = componentConfiguration;
        motorClaimList = $('#motor-claims-list');

        addNewClaim = $('#add-new-motor-claim');

        subscribeForEvents();

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupWrite();
    
        var noButton = $("#driver-has-claims-no");
        var isAnyClaims = $('.step-form__summary-item').length > 0;

        if (!isAnyClaims) {
            var model = bgl.common.datastore.getData(form);

            if (model && model.HasClaims) {
                noButton.prop('checked', 'checked');
            }
        }

        if (!radioButtons.is(':checked')) {
            doneButton.addClass('hide');
        } else {
            doneButton.removeClass('hide');
        }
    }

    function subscribeForEvents() {
        subscribeToEventShowMotorClaims();

        subscribeToEventDeleteMotorClaim();

        subscribeToEventRemoveAllMotorClaims();

        subscribeToEventAddMotorClaim();

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToEventShowMotorClaims() {
        $.each(radioButtons,
            function (key, item) {
                var button = $(item);

                button.off("change").on("change",
                    function (event) {
                        event.preventDefault();
                        
                        switch ($(this).val()) {
                            case "True":
                                if (getMotorClaims() === 0) {
                                    addNewClaim.click();
                                } else {
                                    $("#driver-has-claims-no").attr("data-need-to-delete", "false");
                                    if (motorClaimList.hasClass(HIDE_CLASS)) {
                                        motorClaimList.removeClass(HIDE_CLASS);
                                    }
                                }
                                break;
                            case "False":
                                var isNeededToBeDelete = getMotorClaims() !== 0;
                                $("#driver-has-claims-no").attr("data-need-to-delete", isNeededToBeDelete);

                                if (!motorClaimList.hasClass(HIDE_CLASS)) {
                                    motorClaimList.addClass(HIDE_CLASS);
                                }

                                if (getMotorClaims() === 0) {
                                    bgl.common.utilities.redirectToUrl(doneButton.data('next-step-url'));
                                }
                                break;
                        }
                    });
            });
    }

    function subscribeToEventAddMotorClaim() {

        addNewClaim.off("click").on("click",
            function (event) {
                event.preventDefault();

                var url = $(this).attr('href');
                loadAddMotorClaims(url);
            });
    }

    function subscribeToEventRemoveAllMotorClaims() {

        var removeAllButton = $("#remove-all-motor-claims");
        var noButton = $("#driver-has-claims-no");

        removeAllButton.off('click');
        removeAllButton.on('click',
            function (event) {

                event.preventDefault();

                noButton.click();
            });

        doneButton.off('click').on('click',
            function () {
                var isNeededToDelete = noButton.attr("data-need-to-delete") === "true";
                var isNoButtonChecked = noButton.is(':checked');
                var patchUrl = $(this).data("remove-all-url");

                if (isNeededToDelete && isNoButtonChecked) {
                    var dataToPost = $.param({
                        __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
                        personId: $("#PersonId").val()
                    });

                    $.ajax({
                        url: patchUrl,
                        type: "POST",
                        data: dataToPost,
                        success: function (data, textStatus, request) {
                            if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                if (!data.isSuccess && data.errorRedirectUrl !== null) {
                                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                } else if (data.isSuccess) {
                                    bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
                                }
                            }
                        }
                    });
                }
                else {
                    bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
                }
            });
    }

    function subscribeToEventDeleteMotorClaim() {
        var deleteMotorClaimButtons = $("button[data-role='delete']");

        $.each(deleteMotorClaimButtons,
            function (key, item) {
                $(item).off("click");
                $(item).on("click", function (event) {
                    event.preventDefault();
                    $(item).attr("disabled", "disabled");

                    var target = $(event.target);

                    var link = target.is("button") ? target : target.parent();

                    var token = $("input[name='__RequestVerificationToken']").val();
                    var patchUrl = link.attr('href');
                    var journeyType = $('#JourneyType').val();

                    var dataToPost = $.param({
                        __RequestVerificationToken: token,
                        motorclaimId: link.data("claim-id"),
                        journeyType: journeyType
                    });

                    $.ajax({
                        url: patchUrl,
                        type: "POST",
                        data: dataToPost,
                        success: function (data, textStatus, request) {
                            if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                if (!data.isSuccess) {
                                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                }

                                if (getMotorClaims() === 1) {
                                    var key = form.data('store-key');
                                    var yourDetails = JSON.parse(sessionStorage.getItem(key)) || {};
                                    yourDetails[hasClaims] = 'False';
                                    sessionStorage.setItem(key, JSON.stringify(yourDetails));
                                }

                                bgl.common.utilities.refreshPage();
                            }
                        }
                    });
                });
            });
    }

    function getMotorClaims() {
        return $(".step-form__summary-item").length;
    }

    function loadAddMotorClaims(url) {

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            JourneyType: $("#JourneyType").val()
        };

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                bgl.components.driverdetails.motorclaimbuilder.init(configuration);
            });
    }

    return {
        init: initComponent
    };
})();

bgl.components.driverdetails.motorclaimbuilder = (function () {
    var form,
        configuration,
        HIDE_CLASS = "hide";

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#add-motor-claim');

        if (form) {
            bgl.common.validator.init(form);
            bgl.common.validator.initNumberInputField($("#Claim-Amount"), 5);
        }

        subscribeForEvents();

        subscribeToEventDismissMotorClaimsPanel();

        groupSpanAndButton();
    }

    function groupSpanAndButton() {
        $('.form-row__label.step-form__question-label').append('<span id="tile-container"></span>');
        $('.form-row__label.step-form__question-label #questionmark').prev().appendTo('#tile-container');
        $('.form-row__label.step-form__question-label #questionmark').appendTo('#tile-container');
    }

    function subscribeForEvents() {
        $('.step-form__button').off('click').on('click',
            function (event) {
                event.preventDefault();

                var link = $(event.target);

                selectNextStep(link);
            });

        $('.form-row__breadcrumb-link').off('click').on('click',
            function (event) {
                event.preventDefault();

                gotoBack($(event.target));
            });

        $('[data-step-type="previous-step"]').off('click').on('click',
            function (event) {
                event.preventDefault();

                gotoBack($(event.target));
            });

        var claimAmountRow = $('#claim-amount-row');

        $("#claim-has-amount-no").on("click",
            function () {
                if (!claimAmountRow.hasClass(HIDE_CLASS)) {
                    claimAmountRow.addClass(HIDE_CLASS);
                }
            });

        $("#claim-has-amount-yes").on("click",
            function () {
                if (claimAmountRow.hasClass(HIDE_CLASS)) {
                    claimAmountRow.removeClass(HIDE_CLASS);
                }
            });

        if (form) {
            form.off('submit').on('submit',
                function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    var submit = $(this).find(':submit');
                    submit.attr('disabled', 'disabled');

                    if (bgl.common.validator.validate(configuration, form) === false) {
                        submit.removeAttr('disabled');
                    }
                });
        }
    }

    function subscribeToEventDismissMotorClaimsPanel() {
        $(document).off('click.motorclaims', '[data-dismiss="slide-panel"]').on('click.motorclaims', '[data-dismiss="slide-panel"]', function () {
            unSubscribeToEventDismissMotorClaimsPanel();
            dismissSlidePanelHandler();
        });

        $(document).off('keyup.motorclaims').on('keyup.motorclaims', function (event) {
            if (event.key === "Escape") {
                unSubscribeToEventDismissMotorClaimsPanel();
                dismissSlidePanelHandler();
            }
        });

        $('.overlay').off('click.motorclaims').on('click.motorclaims', function () {
            unSubscribeToEventDismissMotorClaimsPanel();
            dismissSlidePanelHandler();
        });
    }

    function unSubscribeToEventDismissMotorClaimsPanel() {
        $(document).off('click.motorclaims', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.motorclaims');
        $('.overlay').off('click.motorclaims');
    }

    function dismissSlidePanelHandler() {
        bgl.common.utilities.refreshPage();
    }

    function gotoBack(link) {
        selectNextStep(link, true);
    }

    function selectNextStep(link, isBack) {
        var nextStep = link.data("next-step");

        var currentStep = $("#motor-claim-tiles").data("current-step");

        var url = link.attr('href');

        if (currentStep === "Year" && isBack === true) {
            bgl.common.utilities.refreshPage();
            return;
        }

        var currentValue = {
            Code: link.data('code'),
            Value: $.trim(link.text()),
            BuilderStep: currentStep
        };

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            CurrentStep: nextStep,
            JourneyType: $('#JourneyType').val(),
            FirstName: $('#FirstName').val()
        };

        if (isBack) {
            currentValue = undefined;
            model.IsBackButtonClicked = true;
        }

        model.SelectedValues = getSelectedValues(currentValue);

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                bgl.components.driverdetails.motorclaimbuilder.init(configuration);
            });
    }

    function getSelectedValues(currentValue) {
        var result = [];

        var breadcrumbLinks = $('.form-row__breadcrumb-link') || [];

        $.each(breadcrumbLinks,
            function (idx, element) {

                var value = {
                    Code: $(element).data('code'),
                    Value: $.trim($(element).text()),
                    BuilderStep: $(element).data('next-step')
                };

                result.push(value);
            });

        if (currentValue !== undefined) {
            result.push(currentValue);
        }

        return result;
    }

    function onSuccess() {
        bgl.common.utilities.refreshPage();
    }

    function onError() {
        bgl.components.driverdetails.motorclaimbuilder.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
