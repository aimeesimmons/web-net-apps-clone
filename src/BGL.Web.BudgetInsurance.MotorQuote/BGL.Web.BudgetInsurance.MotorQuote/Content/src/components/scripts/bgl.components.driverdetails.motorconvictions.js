﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;
var slidePanelContainer = 'SlidePanelContent';
var hasConvictions = "HasConvictions";

bgl.components.driverdetails.motorconvictions = (function () {
    var configuration,
        form,
        motorConvictionList,
        noButton,
        yesButton,
        doneButton,
        container,
        HIDE_CLASS = "hide";

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        form = $('div[id=' + configuration.Id + '-motor-convictions] form');
        noButton = $("#motor-convictions-radio-no");
        yesButton = $("#motor-convictions-radio-yes");
        motorConvictionList = $('#motor-convictions-list');
        doneButton = $('button[data-remove-all-url]');
        container = $("#" + configuration.Id + "-motor-convictions form");

        bgl.common.datastore.init(container, false, true);
        bgl.common.datastore.setupWrite();

        if (getMotorConvictions() === 0) {
            getConvictionsFromStorageAndSetNoButton();
        }

        subscribeForEvents();
    }

    function subscribeForEvents() {
        subscribeToEventShowMotorConvictions();

        subscribeToEventDeleteMotorConviction();

        subscribeToEventRemoveAllMotorConvictions();

        subscribeToEventAddMotorConviction();

        subscribeToEventDoneButtonMotorConvictions();

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToEventShowMotorConvictions() {
        var radioButtons = $("input[type='radio']");
        if (!radioButtons.is(':checked')) {
            doneButton.addClass("hide");
        } else {
            doneButton.removeClass("hide");
        }

        $.each(radioButtons,
            function (key, item) {
                var button = $(item);

                button.off("change").on("change",
                    function (event) {
                        event.preventDefault();

                        switch ($(this).val()) {
                            case "True":
                                if (getMotorConvictions() === 0) {
                                    $("#motor-convictions-add-another").click();
                                    $(this).prop("checked", false);
                                } else {
                                    $("#motor-convictions-radio-no").attr("data-need-to-delete", "false");
                                    if (motorConvictionList.hasClass(HIDE_CLASS)) {
                                        motorConvictionList.removeClass(HIDE_CLASS);
                                    }
                                }
                                break;
                            case "False":
                                var isNeedToBeDeleted = getMotorConvictions() !== 0;
                                $("#motor-convictions-radio-no").attr("data-need-to-delete", isNeedToBeDeleted);

                                if (!isNeedToBeDeleted) {
                                    setSessionStorageValue();
                                    goToNextStep();
                                } else if (!motorConvictionList.hasClass(HIDE_CLASS)) {
                                    motorConvictionList.addClass(HIDE_CLASS);
                                }
                                break;
                        }
                    });
            });
    }

    function subscribeToEventDeleteMotorConviction() {
        var deleteMotorConvictionButtons = $("button[data-role='delete']");

        $.each(deleteMotorConvictionButtons,
            function (key, item) {
                $(item).off("click").on("click",
                    function (event) {
                        event.preventDefault();
                        $(item).attr("disabled", "disabled");

                        var target = $(event.target);
                        var link = target.is("button") ? target : target.parent();

                        var token = $("input[name='__RequestVerificationToken']").val();
                        var journeyType = $('#JourneyType').val();
                        var patchUrl = link.attr('href');

                        var dataToPost = {
                            __RequestVerificationToken: token,
                            motorConvictionId: link.data("conviction-id"),
                            journeyType: journeyType
                        };

                        $.ajax({
                            url: patchUrl,
                            type: "POST",
                            data: dataToPost,
                            success: function (data, textStatus, request) {
                                if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                    if (!data.isSuccess) {
                                        bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                    }

                                    if (getMotorConvictions() === 1) {
                                        setSessionStorageValue();
                                    }

                                    bgl.common.utilities.refreshPage();
                                }
                            }
                        });
                    });
            });
    }

    function subscribeToEventRemoveAllMotorConvictions() {

        var removeAllButton = $("#remove-all-motor-convictions");

        removeAllButton.off('click').on('click',
            function (event) {
                event.preventDefault();
                noButton.click();
            });
    }

    function subscribeToEventAddMotorConviction() {
        $("#motor-convictions-add-another").off("click").on("click",
            function (event) {
                event.preventDefault();

                var url = $(event.target).attr('href');

                loadAddMotorConvictions(url);
            });
    }

    function subscribeToEventDoneButtonMotorConvictions() {

        doneButton.off('click').on('click',
            function () {
                var isNeededToDelete = noButton.attr("data-need-to-delete") === 'true';
                var isNoButtonChecked = noButton.is(':checked');
                var patchUrl = $(this).data("remove-all-url");

                if (isNeededToDelete && isNoButtonChecked) {
                    var dataToPost = $.param({
                        __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
                        personId: $("#PersonId").val(),
                        JourneyType: $('#JourneyType').val()
                    });

                    $.ajax({
                        url: patchUrl,
                        type: "POST",
                        data: dataToPost,
                        success: function (data, textStatus, request) {
                            if (request.getResponseHeader('Content-Type') === 'application/json; charset=utf-8') {
                                if (!data.isSuccess && data.errorRedirectUrl !== null) {
                                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                } else if (data.isSuccess) {
                                    goToNextStep();
                                }
                            }
                        }
                    });
                } else {
                    goToNextStep();
                }
            });
    }

    function goToNextStep() {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function getMotorConvictions() {
        return $(".step-form__summary-item").length;
    }

    function setSessionStorageValue() {
        var key = container.data('store-key');
        var yourDetails = JSON.parse(sessionStorage.getItem(key));
        yourDetails[hasConvictions] = 'False';
        sessionStorage.setItem(key, JSON.stringify(yourDetails));
    }

    function loadAddMotorConvictions(url) {
        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            JourneyType: $('#JourneyType').val()
        };

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                bgl.components.driverdetails.motorconvictionsbuilder.init(configuration);
            });
    }

    function resetYesNoButtonHandler(flag) {
        if (!flag && getMotorConvictions() === 0) {
            yesButton.prop("checked", false);
            getConvictionsFromStorageAndSetNoButton();
        }
    }

    function getConvictionsFromStorageAndSetNoButton() {
        var model = bgl.common.datastore.getData(container);
        if (model && model.HasConvictions) {
            noButton.prop('checked', 'checked');
        }
    }

    return {
        init: initComponent,
        resetButtons: resetYesNoButtonHandler
    };
})();

bgl.components.driverdetails.motorconvictionsbuilder = (function () {
    var form,
        configuration,
        HIDE_CLASS = "hide",
        haveMotorConvictionsBeenChanged = false;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#add-motor-conviction');

        if (form) {
            bgl.common.validator.init(form);
        }

        subscribeForEvents();

        subscribeToEventDismissMotorConvictionPannel();
    }

    function subscribeToEventDismissMotorConvictionPannel() {
        $(document).off('click.drivermotorconvictions', '[data-dismiss="slide-panel"]').on('click.drivermotorconvictions', '[data-dismiss="slide-panel"]', function () {
            unSubscribeToEventDismissMotorConvictionPannel();
            bgl.components.driverdetails.motorconvictions.resetButtons(haveMotorConvictionsBeenChanged);
        });

        $(document).off('keyup.drivermotorconvictions').on('keyup.drivermotorconvictions', function (event) {
            if (event.key === "Escape") {
                unSubscribeToEventDismissMotorConvictionPannel();
                bgl.components.driverdetails.motorconvictions.resetButtons(haveMotorConvictionsBeenChanged);
            }
        });

        $('.overlay').off('click.drivermotorconvictions').on('click.drivermotorconvictions', function () {
            unSubscribeToEventDismissMotorConvictionPannel();
            bgl.components.driverdetails.motorconvictions.resetButtons(haveMotorConvictionsBeenChanged);
        });
    }

    function unSubscribeToEventDismissMotorConvictionPannel() {
        $(document).off('click.drivermotorconvictions', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.drivermotorconvictions');
        $('.overlay').off('click.drivermotorconvictions');

    }

    function subscribeForEvents() {
        $('.step-form__button, .form-row__breadcrumb-link').off('click').on('click',
            function (event) {
                stepClick(event, false);
            });

        $('[data-step-type="previous-step"]').off('click').on('click',
            function (event) {
                stepClick(event, true);
            });

        function stepClick(event, isBackButtonClicked) {
            event.preventDefault();
            var link = $(event.target);
            selectNextStep(link, isBackButtonClicked);
        }

        var banLengthRow = $('#ban-length-row');

        $('#conviction-has-ban-no').on("click",
            function () {
                if (!banLengthRow.hasClass(HIDE_CLASS)) {
                    banLengthRow.addClass(HIDE_CLASS);
                }
                $("#Months-Ban").val("");
            });

        $('#conviction-has-ban-yes').on("click",
            function () {
                if (banLengthRow.hasClass(HIDE_CLASS)) {
                    banLengthRow.removeClass(HIDE_CLASS);
                }
            });

        bgl.common.validator.initNumberInputField($("#Months-Ban"), 2);
        bgl.common.validator.initNumberInputField($("#Fine-Amount"), 5);

        if (form) {
            form.off('submit').on('submit',
                function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    var submit = $(this).find(':submit');
                    submit.attr('disabled', 'disabled');

                    if (bgl.common.validator.validate(configuration, form) === false) {
                        submit.removeAttr('disabled');
                    }
                });
        }
    }

    function selectNextStep(link, isBack) {
        var nextStep = link.data("next-step");
        var currentStep = $("#motor-conviction-tiles").data("current-step");
        var journeyType = $('#JourneyType').val();

        var currentValue = {
            Code: link.data('code'),
            Value: $.trim(link.text()),
            BuilderStep: currentStep
        };

        var model = {
            ComponentConfiguration: configuration,
            PersonId: $('#PersonId').val(),
            JourneyType: journeyType,
            CurrentStep: nextStep
        };

        if (isBack) {
            currentValue = undefined;
            model.IsBackButtonClicked = true;
        }

        model.SelectedValues = getSelectedValues(currentValue);

        var url = link.attr('href');
        if (link.data("current-step") === "Type" && isBack === true) {
            $("[data-dismiss]").click();
            return;
        }

        bgl.common.loader.load(model,
            url,
            slidePanelContainer,
            function () {
                bgl.components.driverdetails.motorconvictionsbuilder.init(configuration);
            });
    }

    function getSelectedValues(currentValue) {
        var result = [];
        var breadcrumbLinks = $('.form-row__breadcrumb-link') || [];

        $.each(breadcrumbLinks,
            function (idx, element) {
                var value = {
                    Code: $(element).data('code'),
                    Value: $.trim($(element).text()),
                    BuilderStep: $(element).data('next-step')
                };

                result.push(value);
            });

        if (currentValue !== undefined) {
            result.push(currentValue);
        }

        return result;
    }

    function onSuccess(configuration, data) {
        if (data.lastStep === true) {
            haveMotorConvictionsBeenChanged = true;
            $("[data-dismiss]").click();
            bgl.common.utilities.refreshPage();
        } else {
            var newModel = JSON.parse(data.content);

            configuration = newModel.ComponentConfiguration;

            bgl.common.loader.load(newModel,
                data.redirectUrl,
                slidePanelContainer,
                function () {
                    bgl.components.driverdetails.motorconvictionsbuilder.init(configuration);
                });
        }
    }

    function onError() {
        bgl.components.driverdetails.motorconvictionsbuilder.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();