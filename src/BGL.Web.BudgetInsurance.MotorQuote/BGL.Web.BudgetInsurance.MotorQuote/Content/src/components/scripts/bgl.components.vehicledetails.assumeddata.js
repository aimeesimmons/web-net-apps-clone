﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

bgl.components.journeyTypesVehicleDetails = {
    SingleEdit: "0",
    LookupEdit: "1",
    MultiStepEdit: "2"
}

bgl.components.vehicledetails.editassumeddata = (function () {
    var containerId;
    var configuration;
    var immobilizer,
        trackingDevice,
        rightHandDrive;
    var form;

    function loadComponent(model, componentUrl, containerElementId) {
        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.editassumeddata.init(model.ComponentConfiguration, containerElementId);
            });
    }

    function init(componentConfiguration, containerElementId) {
        configuration = componentConfiguration;
        containerId = containerElementId;
        immobilizer = getImmobilizerValue();
        trackingDevice = getTrackingDeviceCheckedState();
        rightHandDrive = getRightHandDriveCheckedState();

        form = $("[data-component-id='" + configuration.Id + "'] form");
        bgl.common.datastore.init(form);

        form.each(function () {
            bgl.common.validator.init($(this));
        });

        form.on("submit",
            function (e) {
                e.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {

                    if (!bgl.common.datastore.isDataChanged(form)) {
                        bgl.common.datastore.removeSessionStorage(form);
                        $('[data-dismiss="slide-panel"]').click();
                        return;
                    }

                    bgl.common.validator.submitForm($(this), componentConfiguration);
                }
            });
    }

    function onSuccess() {
        if (immobilizer !== getImmobilizerValue() ||
            trackingDevice !== getTrackingDeviceCheckedState() ||
            rightHandDrive !== getRightHandDriveCheckedState()) {
            bgl.common.utilities.startUnderwritingPolling();
            bgl.common.datastore.removeSessionStorage(form);
        }

        $('[data-dismiss="slide-panel"]').click();
    }

    function onError() {
        bgl.components.vehicledetails.editassumeddata.init(configuration, containerId);
    }

    function getImmobilizerValue() {
        return $('#SecurityDeviceCode').val();
    }

    function getTrackingDeviceCheckedState() {
        return $("#Tracking-Device-Yes").prop('checked');
    }

    function getRightHandDriveCheckedState() {
        return $("#Right-Hand-Drive-Yes").prop('checked');
    }

    return {
        load: loadComponent,
        init: init,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.vehicledetails;
}
