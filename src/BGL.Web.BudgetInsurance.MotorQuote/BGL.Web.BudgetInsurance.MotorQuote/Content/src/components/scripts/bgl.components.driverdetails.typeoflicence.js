﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.driverdetails.typeoflicence = (function () {
    var form;
    var configuration;
    var licenceInputs;
    var internationalLabel = 'I';
    var hasPreselectedValues;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        form = $('#type-of-licence');

        licenceInputs = form.find('#licence-section [type=radio]');

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        var person = bgl.common.datastore.getData(form);

        var hasPartTimeOccupation = (person.HasPartTimeOccupation == undefined) ?
            false :
            person.HasPartTimeOccupation.toLowerCase() === "true";

        if (hasPartTimeOccupation) {
            $("#" + configuration.Id + "-back-button").attr("href", configuration.PreviousBusinessTypePageUrl);
        }

        subscribeOnEvents();
    }

    function subscribeOnEvents() {

        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

        updateCountrySectionVisibility();

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                });

        licenceInputs.off('click').on('click',
            function(event) {
                updateCountrySectionVisibility();

                if (hasPreselectedValues !== true) {
                    continueHandler(event);
               }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function updateCountrySectionVisibility() {
        var countriesSection = form.find('#licence-row');

        var checkedLicenceValue = findCheckedLicenceValue();

        if (checkedLicenceValue === internationalLabel) {
            hasPreselectedValues = true;
            countriesSection.removeClass('hide');
        } else {
            countriesSection.addClass('hide');
        }

        if (hasPreselectedValues === true) {
            showContinueButton();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.isInternationalType === false) {
            bgl.common.datastore.deleteFields(form,
                [
                    "Country", "CountryVal", "CountryText"
                ]);
        }

        if (data.isInternationalType === true) {
            bgl.common.datastore.deleteFields(form,
                [
                    "HasDrivingLicenceNumber", "DrivingLicenceNumber"
                ]);
        }

        location.href = data.nextDtsPageUrl;
    }

    function onError() {
        bgl.components.driverdetails.typeoflicence.init(configuration);

        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    function findCheckedLicenceValue() {
        return form.find('#licence-section [type=radio]:checked').val();
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
