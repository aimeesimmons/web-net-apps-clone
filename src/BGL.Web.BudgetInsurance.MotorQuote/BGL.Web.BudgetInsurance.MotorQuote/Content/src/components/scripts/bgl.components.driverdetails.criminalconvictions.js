﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;
var slidePanelContainer = 'SlidePanelContent';

bgl.components.driverdetails.criminalconvictions = (function () {
    var form,
        configuration,
        criminalConvictionsContainer,
        criminalConvictionAddAnotherButton,
        criminalConvictionRemoveAllButton,
        doneButton,
        noButton,
        yesButton,
        fullYearMonthOptions,
        monthDropdown,
        yearDropdown,
        isChanged = false,
        HIDE_CLASS = "hide",
        journeyType = 2;

    function initComponent(componentConfiguration, isChangedComponent) {
        configuration = componentConfiguration;
        isChanged = isChangedComponent;

        form = $('#criminal-convictions');
        criminalConvictionsContainer = $('#criminal-convictions-container');
        criminalConvictionAddAnotherButton = criminalConvictionsContainer.find('#criminal-conviction-add-another');
        criminalConvictionRemoveAllButton = criminalConvictionsContainer.find("#remove-all-criminal-convictions");

        doneButton = $("#criminal-conviction_done-button");
        noButton = $('#has-criminal-convictions-no');
        yesButton = $('#has-criminal-convictions-yes');

        subscribeForEvents();

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupWrite();

        var isAnyCriminalConvictions = criminalConvictionsCount() > 0;
        
        if (!isAnyCriminalConvictions) {
            var model = bgl.common.datastore.getData(form);
        
                if (model && model.HasCriminalConvictions) {
                    noButton.prop('checked', 'checked');
                }
        }

        if (yesButton.is(':checked') || noButton.is(':checked')) {
            if (doneButton.hasClass(HIDE_CLASS)) {
                doneButton.removeClass(HIDE_CLASS);
            }
        }
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            var forAttr = $(event.target).attr('for');

            if (forAttr !== null) {
                $('#' + forAttr).click();
            } else {
                $(this).click();
            }
        }
    }

    function subscribeForEvents() {
        yesButton.off('click').on("click",
            function () {
                var hasCriminalConviction = criminalConvictionsCount() > 0;
                if (!hasCriminalConviction) {
                    criminalConvictionAddAnotherButton.click();
                } else if (criminalConvictionsContainer.hasClass(HIDE_CLASS)) {
                    criminalConvictionsContainer.removeClass(HIDE_CLASS);
                }
            });

        noButton.off('click').on("click",
            function () {
                if (criminalConvictionsCount() === 0) {
                    onSuccess();
                }

                if (!criminalConvictionsContainer.hasClass(HIDE_CLASS)) {
                    criminalConvictionsContainer.addClass(HIDE_CLASS);
                }
            });

        $("button[data-role='delete']").off('click').on("click",
            function (event) {
                event.preventDefault();
                $(this).attr("disabled", "disabled");

                var target = $(event.target);

                var object = target.is("button") ? target : target.parent();

                var token = $("input[name='__RequestVerificationToken']").val();
                var patchUrl = object.data('url');

                var dataToPost = {
                    __RequestVerificationToken: token,
                    motorCriminalConvictionId: object.data("value"),
                    journeyType: journeyType
                };

                $.ajax({
                    url: patchUrl,
                    type: "POST",
                    data: dataToPost,
                    success: function (data) {
                        if (!data.isSuccess) {
                            bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                        } else {
                            isChanged = true;
                            if (criminalConvictionsCount() === 1) {
                                var key = form.data('store-key');
                                var yourDetails = JSON.parse(sessionStorage.getItem(key)) || {};
                                yourDetails["HasCriminalConvictions"] = 'False';
                                sessionStorage.setItem(key, JSON.stringify(yourDetails));
                            }
                            bgl.common.utilities.refreshPage();
                        }
                    }
                });
            }).off('keypress').on("keypress", generateClickOnKeypress);

        criminalConvictionAddAnotherButton.off('click').on('click', function (event) {
            event.preventDefault();

            bgl.components.driverdetails.criminalconvictions.goToWizardStep(1);
        }).off('keypress').on("keypress", generateClickOnKeypress);

        criminalConvictionRemoveAllButton.off('click').on('click', function (event) {
            event.preventDefault();
            noButton.click();
        }).off('keypress').on("keypress", generateClickOnKeypress);

        doneButton.off('click').on('click',
            function () {
                var isNoButtonChecked = noButton.is(':checked');
                var hasCriminalConviction = criminalConvictionsCount() > 0;

                if (isNoButtonChecked && hasCriminalConviction) {
                    isChanged = true;

                    bgl.common.validator.validate(configuration, form);
                } else {
                    onSuccess();
                }
            }).off('keypress').on("keypress", generateClickOnKeypress);

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToEventDismissCriminalConvictionsPanel() {
        $(document).on('click.criminalconvictions', '[data-dismiss="slide-panel"]', function () {
            unSubscribeToEventDismissCriminalConvictionsPanel();
            dismissSlidePanelHandler(isChanged);
        });

        $(document).on('keyup.criminalconvictions', function (event) {
            if (event.key === "Escape") {
                unSubscribeToEventDismissCriminalConvictionsPanel();
                dismissSlidePanelHandler(isChanged);
            }
        });

        $('.overlay').on('click.criminalconvictions', function () {
            unSubscribeToEventDismissCriminalConvictionsPanel();
            dismissSlidePanelHandler(isChanged);
        });
    }

    function unSubscribeToEventDismissCriminalConvictionsPanel() {
        $(document).off('click.criminalconvictions', '[data-dismiss="slide-panel"]');
        $(document).off('keyup.criminalconvictions');
        $('.overlay').off('click');
    }

    function dismissSlidePanelHandler(flag) {
        if (flag === true) {
            bgl.common.utilities.refreshPage();
        }
    }

    function subscribeForEventsWizardStep1() {
        var formstep1 = $("#criminal-convictions-addanother-step1");
        bgl.common.validator.init(formstep1);

        formstep1.off('submit').on('submit', function (event) {
            event.preventDefault();

            $('#MonthText').val($('#Criminal-Conviction-Month option:selected').text());

            bgl.common.validator.validate(configuration, formstep1);
        });

        monthDropdown = formstep1.find('#Criminal-Conviction-Month');
        yearDropdown = formstep1.find('#Criminal-Conviction-Year');

        fullYearMonthOptions = monthDropdown.find('option');

        yearDropdown.on("change", updateMonthDropdown);

        $("a[data-step-type='previous-step']").one("click", function (event) {
            event.preventDefault();
            bgl.common.utilities.refreshPage();
        });
    }

    function subscribeForEventsWizardStep2() {
        var formStep2 = $('#criminal-convictions-addanother-step2');

        bgl.common.validator.init(formStep2);

        formStep2.off('submit').on('submit', function (event) {
            event.preventDefault();

            $('#ConvictionsTypeText').val($('#Conviction-type option:selected').text());

            bgl.common.validator.validate(configuration, formStep2);
        });

        subscribeBreadcrumbs();
    }

    function subscribeForEventsWizardStep3() {
        var formStep3 = $('#criminal-convictions-addanother-step3');

        bgl.common.validator.init(formStep3);

        formStep3.off('submit').on('submit', function (event) {
            event.preventDefault();
            var submit = $(this).find(':submit');
            submit.attr('disabled', 'disabled');

            if (bgl.common.validator.validate(configuration, formStep3) === false) {
                submit.removeAttr('disabled');
            }
        });

        subscribeBreadcrumbs();
    }

    function subscribeBreadcrumbs() {
        $('.criminal-conviction-back-to-first-step').off('click').on('click', function (event) {
            event.preventDefault();

            bgl.components.driverdetails.criminalconvictions.goToWizardStep(1);
        });

        $('.criminal-conviction-back-to-second-step').off('click').on('click', function (event) {
            event.preventDefault();

            bgl.components.driverdetails.criminalconvictions.goToWizardStep(2, { Year: $('#Year').val(), Month: $('#Month').val(), MonthText: $('#MonthText').val() });
        });
    }

    function goToWizardStep(step, data) {
        var href = $('#UrlToWizard').val();

        var defaultData = getDefaultData(step);

        if (data !== null) {
            $.extend(defaultData, data);
        }

        isChanged = true;

        $.ajax({
            url: href,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(defaultData),
            success: function (data) {
                if (step === 1) {
                    subscribeToEventDismissCriminalConvictionsPanel();
                }

                if (typeof data.isSuccess === 'undefined') {
                    bgl.components.driverdetails.criminalconvictions.updateSlidePanel(data);
                } else if (data.isSuccess === true) {
                    bgl.components.driverdetails.criminalconvictions.updateSlidePanel(data.html);
                } else {
                    bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                }

                switch (step) {
                    case 1:
                        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep1();
                        break;

                    case 2:
                        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep2();
                        break;

                    case 3:
                        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep3();
                        break;
                }
            }
        });
    }

    function getDefaultData(step) {
        var personId = $('#PersonId').val();
        var firstName = $('#FirstName').val();

        var data = { ComponentConfiguration: configuration, PersonId: personId, CriminalConvictionStep: step, JourneyType: 2 }

        if (firstName) {
            data.FirstName = firstName;
        }
        
        return data;
    }

    function onSuccess() {
        bgl.common.utilities.redirectToUrl(doneButton.data("nextStepUrl"));
    }

    function onError() {
        bgl.components.driverdetails.criminalconvictions.init(configuration);
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $('#CurrentYear').val() * 1;
        var currentMonth = $('#CurrentMonth').val() * 1;
        var selectedYear = yearDropdown.val() * 1;
        var personBirthYear = $("#PersonBirthYear").val() * 1;
        var personBirthMonth = $('#PersonBirthMonth').val() * 1;

        if (selectedYear === currentYear) {
            months.html(fullYearMonthOptions.slice(0, currentMonth + 1));
        } else if (selectedYear === personBirthYear) {
            // merge months after birth month and please select...
            var dropdownOptions = fullYearMonthOptions.slice(personBirthMonth + 1);
            months.html($.merge([fullYearMonthOptions[0]], dropdownOptions));
        } else {
            months.html(fullYearMonthOptions);
        }
        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function criminalconvictionsDateOnSuccess(configuration, data) {
        bgl.components.driverdetails.criminalconvictions.updateSlidePanel(data.html);
        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep2();
    }

    function criminalconvictionsDateOnError() {
        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep1();
    }

    function criminalconvictionsConvictionTypeOnSuccess(configuration, data) {
        bgl.components.driverdetails.criminalconvictions.updateSlidePanel(data.html);
        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep3();
    }

    function criminalconvictionsConvictionTypeOnError() {
        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep2();
    }

    function criminalconvictionsSentenceTypeOnSuccess() {
        unSubscribeToEventDismissCriminalConvictionsPanel();
        $('[data-dismiss="slide-panel"]').click();
        bgl.common.utilities.refreshPage();
    }

    function criminalconvictionsSentenceTypeOnError() {
        bgl.components.driverdetails.criminalconvictions.subscribeForEventsWizardStep3();
    }

    function updateSlidePanel(html) {
        $('#' + slidePanelContainer).html(html);
    }

    function criminalConvictionsCount() {
        return $('#Criminal-Convictions-List .step-form__summary-item').length;
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        goToWizardStep: goToWizardStep,
        updateSlidePanel: updateSlidePanel,
        criminalconvictionsDateOnSuccess: criminalconvictionsDateOnSuccess,
        criminalconvictionsDateOnError: criminalconvictionsDateOnError,
        criminalconvictionsConvictionTypeOnError: criminalconvictionsConvictionTypeOnError,
        criminalconvictionsConvictionTypeOnSuccess: criminalconvictionsConvictionTypeOnSuccess,
        criminalconvictionsSentenceTypeOnSuccess: criminalconvictionsSentenceTypeOnSuccess,
        criminalconvictionsSentenceTypeOnError: criminalconvictionsSentenceTypeOnError,
        subscribeForEventsWizardStep1: subscribeForEventsWizardStep1,
        subscribeForEventsWizardStep2: subscribeForEventsWizardStep2,
        subscribeForEventsWizardStep3: subscribeForEventsWizardStep3
    };
})();