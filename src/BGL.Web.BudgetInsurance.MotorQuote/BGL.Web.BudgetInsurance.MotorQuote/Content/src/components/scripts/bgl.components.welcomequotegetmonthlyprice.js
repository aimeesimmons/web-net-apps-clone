﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequotegetmonthlyprice = (function () {

    var configuration, monthlyPrice;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        monthlyPrice = $('#' + configuration.Id + '__monthly-data');

        bgl.common.pubsub.on('switchPaymentType', togglePrice);
    }

    function togglePrice(isMonthly) {

        if (isMonthly) {
            monthlyPrice.removeClass("hide");
        } else {
            monthlyPrice.addClass("hide");
        }
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequotegetmonthlyprice;
}