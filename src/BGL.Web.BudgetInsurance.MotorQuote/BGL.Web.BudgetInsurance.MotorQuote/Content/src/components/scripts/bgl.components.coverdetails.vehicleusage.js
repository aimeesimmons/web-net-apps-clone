﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.vehicleusage = (function () {
  var configuration,
    isVan,
    form;
  var carriageOfOwnGoods = '21',
    haulage = '22';

  function loadComponent(componentConfiguration, componentUrl, containerElementId) {
    configuration = componentConfiguration;

    bgl.common.loader.load(componentConfiguration,
      componentUrl,
      containerElementId,
      function () {
        initComponent(componentConfiguration);
      });
  }

  function initComponent(componentConfiguration) {
    configuration = componentConfiguration;
    isVan = $('#IsVan').val();
    form = $('[data-component-id="' + configuration.Id + '"] form');

    bgl.common.datastore.init(form);
    bgl.common.datastore.setupReadAndWrite();
    bgl.common.validator.init(form);

    subscribeToEvents();

    form.find('label').on('keyup', bgl.common.keyboardAccessibility.generateClickOnKeypress);
  }

  function subscribeToEvents() {
    if (form.find('input[type=radio]:checked').length === 0) {
      form.find('input[type=radio]')
        .off('keypress', generateClickOnKeypress)
        .on('keypress', generateClickOnKeypress)
        .off('click', continueHandler)
        .on('click', function (event) {
          $(this).focusout();
          submitForm();
        });
    } else {
      var continueButton = $(form.find('[type="submit"]'));
      continueButton.removeClass('hide');
      continueButton.off('click').on('click', continueHandler);
    }

    form.off('submit')
      .on('submit',
        function (event) {
          event.preventDefault();

          setDataHasChangedFlagForQuestionSet();

          if (isVan === 'True') {
            vanNavigation();
          } else {
            if (bgl.common.validator.isFormValid(form)) {
              bgl.common.validator.submitForm(form, configuration);
            }
          }
        });
  }

  function submitForm() {
    form.submit();
  }

  function onSuccess(componentConfiguration) {
    location.href = componentConfiguration.NextStepUrl;
  }

  function generateClickOnKeypress(event) {
    var code = event.charCode || event.keyCode;
    if (code === 13 || code === 32) {
      $(this).click();
    }
  }

  function continueHandler(event, element) {
    event.preventDefault();

    if (bgl.common.datastore.isDataChanged(form)) {
      submitForm();
    } else {
      if (isVan === 'True') {
        vanNavigation();
      } else {
        location.href = configuration.NextStepUrl;
      }
    }
  }

  function vanNavigation() {
    var vehicleUsage = $('input[name=ClassOfUseCode]:checked', '#vehicle-usage').val();
    if (typeof bgl.common.pubsub !== 'undefined') {
      bgl.common.pubsub.emit('fakeFormSubmit', 'VehicleUsage');
    }
    if (vehicleUsage === carriageOfOwnGoods || vehicleUsage === haulage) {
      bgl.common.utilities.redirectToUrl(configuration.YearsBusinessEstablishedUrl);
    } else {
      bgl.common.utilities.redirectToUrl(configuration.CarriageOfDangerousGoodsUrl);
    }
  }

  function setDataHasChangedFlagForQuestionSet() {
    var vehicleUseSessionData = JSON.parse(sessionStorage.getItem('CoverDetailsVehicleUse'));

    if (bgl.common.datastore.isDataChanged(form)) {
      vehicleUseSessionData.DataHasChanged = 'True';
      sessionStorage.setItem('CoverDetailsVehicleUse', JSON.stringify(vehicleUseSessionData));
    }
  }

  return {
    init: initComponent,
    load: loadComponent,
    onSuccess: onSuccess
  };
})();