﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.maritalstatus = (function () {
    var form;
    var configuration;
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {

            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#marital-status-form');

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();

        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', continueHandler)
                .on('click', function(event) {
                    $(this).focusout();
                    continueHandler(event);
                });
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
            continueButton.off("click").on("click", continueHandler);
        }

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;
        if (code === 13 || code === 32) {
            $(this).click();
        }
    }

    function continueHandler(event, element) {
        event.preventDefault();
        form.find("input[type=radio]").off();
        location.href = configuration.NextStepUrl;
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

