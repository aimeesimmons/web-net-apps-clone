﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.carriageofdangerousgoods = (function () {
    var form,
        configuration,
        container,
        SPACE_KEYCODE = 32,
        ENTER_KEYCODE = 13;

    var socialDpClassOfUseCode = "05";

    function loadComponent(config, url, containerId) {
        bgl.common.loader.load(config,
            url,
            containerId,
            function () {
                initComponent(config, containerId);
            });
    }

    function initComponent(componentConfiguration, containerId) {
        initElements(componentConfiguration, containerId);

        subscribeForEvents();
    }

    function onSuccess(componentConfiguration, data) {
        if (isMultiStepEdit()) {
            location.href = configuration.NextStepUrl;
        } else {
            bgl.common.datastore.removeSessionStorage(form);
            bgl.common.pubsub.emit('PollBasket');
        }
    }

    function onError() {
        initComponent(configuration);
    }

    function initElements(componentConfiguration, containerId) {
        configuration = componentConfiguration;
        form = $("#carriage-of-dangerous-goods-form");
        container = containerId;

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
    }

    function subscribeForEvents() {
        var vehicleUseSessionData = JSON.parse(sessionStorage.getItem("CoverDetailsVehicleUse"));
        var continueButton = $("#continueButton");

        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress')
                .off('click', continueHandler)
                .on({
                    "keypress": function (e) {
                        if (e.keyCode === ENTER_KEYCODE || e.keyCode === SPACE_KEYCODE) {
                            $(this).click();
                        }
                    },
                    'click': function (event) {
                        $(this).focusout();
                        submitForm(form);
                    }
                });
        } else {
            continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
            continueButton.off("click").on("click", () => {
                submitForm(form);
            });
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    setDataHasChangedFlagForQuestionSet(vehicleUseSessionData);

                    continueButton.attr("disabled", true);
                    if (bgl.common.validator.isFormValid(form)) {
                        var model = configuration;

                        if (!isMultiStepEdit()) {
                            vehicleUseSessionData = JSON.parse(sessionStorage.getItem("CoverDetailsVehicleUse"));

                            if (vehicleUseSessionData["DataHasChanged"] !== "True") {
                                if (configuration.IsService) {
                                    bgl.common.contentpanel.hideSlidePanel();
                                } else {
                                    $('[data-dismiss="slide-panel"]').click();
                                }
                                
                                bgl.common.datastore.removeSessionStorage(form);
                                return;
                            }

                            model = bgl.common.datastore.getData(form);
                            model.ComponentConfiguration = configuration;
                        }

                        var formData = getStoredData();

                        bgl.common.validator.submitForm(form, model, formData);

                    } else {
                        continueButton.attr("disabled", false);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

        form.find('#carriageOfDangerousGoodsBackBtn').off('click').one('click',
            function (event) {
                event.preventDefault();
                var classOfUseCode = vehicleUseSessionData["ClassOfUseCode"];
                var componentActionUrl = $(this).attr('href');

                if (isMultiStepEdit()) {
                    if (classOfUseCode !== socialDpClassOfUseCode) {
                        componentActionUrl = $(this).data('years-business-established-action-url');
                    }

                    location.href = componentActionUrl;
                } else {
                    if (classOfUseCode !== socialDpClassOfUseCode) {
                        componentActionUrl = $(this).data('years-business-established-action-url');

                        bgl.components.coverdetails.yearsbusinessestablished.load(configuration,
                            componentActionUrl,
                            container);
                    } else {
                        var model = bgl.common.datastore.getData(form);
                        model.ComponentConfiguration = configuration;

                        bgl.components.vehicleusage.load(model,
                            componentActionUrl,
                            container);
                    }
                }
            });
    }

    function submitForm(form) {
        var submitButton = form.find(':submit');

        if (submitButton.hasClass("hide")) {
            form.change().submit();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        form.submit();
    }

    function isMultiStepEdit() {
        var coverDetailsCommon = bgl.components.coverdetails.common;
        return configuration.JourneyType === coverDetailsCommon.journeyTypes.multiStepEdit;
    }

    function getStoredData() {
        var data = getDataToPost();
        var formData = form.serialize();

        return formData + "&" + $.param(data);
    }

    function getDataToPost() {
        var data = bgl.common.datastore.getData(form);
        var config = { ComponentConfiguration: configuration };

        $.extend(data, config);

        return data;
    }

    function setDataHasChangedFlagForQuestionSet(vehicleUseSessionData) {
        var dataHasChanged = vehicleUseSessionData["DataHasChanged"] === "True";

        if (dataHasChanged || bgl.common.datastore.isDataChanged(form)) {
            vehicleUseSessionData.DataHasChanged = "True";
            sessionStorage.setItem("CoverDetailsVehicleUse", JSON.stringify(vehicleUseSessionData));
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();