﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

var coverKeys = {
    totalExcess: "total-excess",
    coverLevel: "cover-level",
    courtesyVehicle: "courtesy-vehicle",
    legalProtection: "legal-protection",
    uninsuredDriverPromise: "uninsured-driver-promise",
    vandalismPromise: "vandalism-promise",
    windscreenCover: "windscreen-cover",
    driveOtherVehicles: "drive-other-vehicles",
    childSeatCover: "child-seat-cover",
    personalAccident: "personal-accident",
    personalBelongings: "personal-belongings",
    emergencyTransportAndAccommodation: "emergency-transport-and-accommodation",
    emergencyTransport: "emergency-transport",
    transferHome: "transfer-home",
    drivingAbroad: "driving-abroad",
    medicalExpenses: "medical-expenses",
    audioEquipment: "audio-equipment",
    replacementLocks: "replacement-locks",
    annualMileage: "annual-mileage",
    noClaimsDiscount: "ncd",
    vehicleUsage: "vehicle-usage",
    mainDriver: "main-driver",
    registeredKeeper: "registered-keeper",
    breakdown: "breakdown",
    guaranteedReplacementVehicle: "guaranteed-replacement-vehicle",
    keyCover: "key-cover",
    toolsCover: "tools-cover"
};

var sessionStorageConstants = {
    IsPncdTriggerPageRedirectionKey: "PncdTriggerPageRedirection",
    IsPriceProtectionTriggerPageRedirectionKey: "PriceProtectionTriggerPageRedirection",
    CoverDetailsPncdAddonIsYesClicked: "CoverDetailsPncdAddonIsYesClicked",
    CoverDetailsPriceProtectionAddonIsYesClicked: "CoverDetailsPriceProtectionAddonIsYesClicked"
};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;
var ESCAPE_KEYCODE = 27;

bgl.components.coverdetails.common = (function () {

    var component;

    var journeyTypes = {
        singleEdit: 0,
        additionalDriver: 1,
        multiStepEdit: 2
    };

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.coverdetails.common.init(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {

        component = $('#' + componentConfiguration.Id);
        var links = component.find('[data-action-url]');

        links.off()
        links.on({
            'keyup': function (event) {
                var code = event.charCode || event.keyCode;

                if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                    $(this).click();
                }
            },
            'click': function (event) {
                event.preventDefault();
                var link = $(this);

                loadComponentByCoverKey(link.data("cover-key"),
                    link.data("container"),
                    componentConfiguration,
                    link.data("action-url"),
                    {
                        coverId: link.data("cover-id"),
                        coverText: link.data("cover-text"),
                        contentUrl: link.data("content-url")
                    });
            }
        });

        $('#' + componentConfiguration.Id + '_feature-banner-button').on('click',
            function () {
                var $this = $(this);

                $this.attr('aria-expanded', function (index, value) {
                    var isExpanded = value === "true";
                    $('.card-container').toggleClass('is-active');
                    $this.text(isExpanded ? "View more features" : "View less features");

                    return !isExpanded;
                });
            });
    }

    function loadComponentByCoverKey(key, containerSelector, configuration, actionUrl, options) {

        if (configuration.CoverFeaturePriority !== undefined
            && key in configuration.CoverFeaturePriority
            && configuration.CoverFeaturePriority[key].ViewName
            && configuration.CoverFeaturePriority[key].ViewName.toLowerCase() === "none") {
            $("#" + containerSelector).empty();
            return;
        }

        switch (key) {
            case coverKeys.totalExcess:
                bgl.components.excesses.load(
                    { ComponentConfiguration: configuration, CoverId: options.coverId },
                    actionUrl,
                    containerSelector);
                break;

            case coverKeys.coverLevel:
                bgl.components.coverlevel.load(
                    { ComponentConfiguration: configuration, CoverId: options.coverId },
                    actionUrl,
                    containerSelector);
                break;

            case coverKeys.noClaimsDiscount:
                bgl.components.noclaimsdiscount.load(
                    { ComponentConfiguration: configuration, CoverId: options.coverId },
                    actionUrl,
                    containerSelector);
                break;

            case coverKeys.vehicleUsage:
                bgl.components.vehicleusage.load(
                    { ComponentConfiguration: configuration, CoverId: options.coverId, ClassOfUse: options.coverText },
                    actionUrl,
                    containerSelector);
                break;

            case coverKeys.annualMileage:
                bgl.components.annualmileage.load(
                    { ComponentConfiguration: configuration, CoverId: options.coverId },
                    actionUrl,
                    containerSelector);
                break;

            case coverKeys.mainDriver:
                bgl.components.maindriver.load(
                    { ComponentConfiguration: configuration, CoverId: options.coverId },
                    actionUrl,
                    containerSelector);
                break;
            case coverKeys.registeredKeeper:
                bgl.common.loader.load(configuration,
                    actionUrl,
                    containerSelector,
                    function () {
                        bgl.components.coverdetails.registeredkeeper.init(configuration, containerSelector);
                    });
                break;

            case coverKeys.legalProtection:
            case coverKeys.courtesyVehicle:
            case coverKeys.uninsuredDriverPromise:
            case coverKeys.vandalismPromise:
            case coverKeys.windscreenCover:
            case coverKeys.driveOtherVehicles:
            case coverKeys.childSeatCover:
            case coverKeys.personalAccident:
            case coverKeys.personalBelongings:
            case coverKeys.emergencyTransportAndAccommodation:
            case coverKeys.emergencyTransport:
            case coverKeys.transferHome:
            case coverKeys.drivingAbroad:
            case coverKeys.medicalExpenses:
            case coverKeys.audioEquipment:
            case coverKeys.replacementLocks:
            case coverKeys.breakdown:
            case coverKeys.keyCover:
            case coverKeys.toolsCover:
            case coverKeys.guaranteedReplacementVehicle:
                bgl.common.loader.load({}, options.contentUrl, containerSelector);
                break;
            default:
                $("#" + containerSelector).empty();
                break;
        }
    }

    function onAddonBackButtonClick() {
        onAddonBackButtonClickOrKeyUp();
    }

    function onAddonBackButtonClickOrKeyUp() {

        var component = $('[data-cover-details-addon-back-button]');

        component.off();
        component.on({
            'keyup': function (event) {
                var code = event.charCode || event.keyCode;
                if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                    $(this).click();
                }
            },
            'click': function (event) {
                event.preventDefault();
                if (this.getAttribute('data-cover-details-addon-back-button') === 'review') {
                    reviewBackButtonClickHandler($(this));
                } else {
                    backButtonClickHandler();
                }
            }
        });
    }

    function backButtonClickHandler() {
        var previousAddon = $("#previous-addon").val();
        if (previousAddon) {
            var previousAddonSelected = sessionStorage.getItem(("SelectedAddon").concat(previousAddon));
            if (previousAddonSelected !== null) {
                bgl.common.utilities.redirectToUrl(previousAddonSelected);
            } else {
                bgl.common.utilities.redirectToUrl(previousAddon);
            }
        } else {
            var previousStepUrl = $("#previous-stepUrl").val();
            bgl.common.utilities.redirectToUrl(previousStepUrl);
        }
    }

    function reviewBackButtonClickHandler($this) {

        var reviewBackBtnUrl = $this.attr("href");

        var index = reviewBackBtnUrl.lastIndexOf('/');

        var pageUrl = reviewBackBtnUrl.substring(index + 1, reviewBackBtnUrl.length);

        var previousAddonSelected = sessionStorage.getItem(("SelectedAddon").concat(pageUrl));

        if (previousAddonSelected) {

            var detailUrl = reviewBackBtnUrl.replace(pageUrl, previousAddonSelected);

            bgl.common.utilities.redirectToUrl(detailUrl);
        } else {
            bgl.common.utilities.redirectToUrl(reviewBackBtnUrl);
        }
    }

    function setNextAction(configuration, isDataChanges) {

        if (configuration.IsService) {
            if (configuration.IsInitialChange) {
                if (isDataChanges) {
                    sessionStorage.setItem("MTAStartPage", window.location.href);
                    window.location.href = configuration.AdditionalChangesUrl;
                } else {
                    bgl.common.contentpanel.hideSlidePanel();
                }
            } else {
                window.location.href = configuration.AdditionalChangesUrl;
            }
        } else {
            $('[data-dismiss="slide-panel"]').click();
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        journeyTypes: journeyTypes,
        onAddonBackButtonClick: onAddonBackButtonClick,
        setNextAction: setNextAction
    };
})();

bgl.components.excessesbar = (function () {

    var configuration;

    function initComponent(componentConfiguration) {

        configuration = componentConfiguration;
        var excessBarVoluntary = $(".voluntary-excess");
        var originalWidth = excessBarVoluntary.outerWidth();
        excessBarVoluntary.css({ 'minWidth': originalWidth + 'px' });

        var animate = setInterval(load, 10);
        var pos = 0;

        function load() {
            if (pos === $('.policy-excess').data('voluntary-percentage')) {
                clearInterval(animate);
            } else {
                pos++;
                excessBarVoluntary.css({ 'width': pos + '%' });
            }
        }

        bgl.components.coverdetails.common.init(configuration);
    }

    return {
        init: initComponent
    };
})();


bgl.components.coverlevel = (function () {

    var configuration,
        form;

    function loadComponent(data, componentUrl, containerElementId) {

        configuration = data.ComponentConfiguration;


        bgl.common.loader.load(data,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.coverlevel.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {

        configuration = componentConfiguration;
        if (configuration.IsSummary) {
            $("#BtnSubmit").off("click").on("click", this.onSuccess);
        } else {
            initValidation();
            initDynamicText();
        }
    }

    function initDynamicText() {
        $(".cover-level").on("click",
            function () {
                var text = $(this).text().trim();

                $("#feature-header-cover-level").text(text);
                $("#feature-question-box-cover-level").text(text);
            });
    }

    function initValidation() {
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form);

        bgl.common.validator.init(form);


        $("#BtnSubmit").off("click").on("click",
            function () {
                form.submit();
            });

        form.on("submit",
            function (e) {
                e.preventDefault();

                if (!bgl.common.datastore.isDataChanged(form)) {
                    bgl.components.coverdetails.common.setNextAction(configuration, false);
                    bgl.common.datastore.removeSessionStorage(form);
                    return;
                }

                storeCoverDetailsChangesForTagManager();

                bgl.common.validator.validate(configuration, $(this));
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function storeCoverDetailsChangesForTagManager() {
        var data = getPreviouslyStoredChangedCoverDetailsData();
        var coverLevel = getCoverLevelListValue();

        if (coverLevel) {
            data.push(["CoverLevel", coverLevel]);
            setPreviouslyStoredChangedCoverDetailsData(data);
        }
    }

    function getCoverLevelListValue() {
        var checkedCoverLevelButtonId = $('input[name=CoverLevelCode]:checked').attr('id');
        var valueForCheckedCoverLevelButton = $(`label[for="${checkedCoverLevelButtonId}"] p`, ).html();

        return valueForCheckedCoverLevelButton;
    }

    function getPreviouslyStoredChangedCoverDetailsData() {
        var data = [];

        if (sessionStorage.getItem("ChangedVehicleData") !== null) {
            data = JSON.parse(sessionStorage.getItem("ChangedVehicleData"));
        }

        return data;
    }

    function setPreviouslyStoredChangedCoverDetailsData(data) {
        sessionStorage.setItem("ChangedVehicleData", JSON.stringify(data));
    }

    function successCallback(message) {
        var notification = JSON.stringify({
            cat: "pricechange",
            msg: message
        });

        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(config, response) {
        if (configuration.IsSummary === true) {
            $('[data-dismiss="slide-panel"]').click();
            return;
        }
        var isDataChanges = bgl.common.datastore.isDataChanged(form);
        bgl.common.datastore.removeSessionStorage(form);

        bgl.components.coverdetails.common.setNextAction(configuration, isDataChanges);

        if (!configuration.IsService) {
            if (response.houstonMessage) {
                var params = {
                    callback: function () {
                        successCallback(response.houstonMessage);
                    }
                };

                bgl.common.pubsub.emit("PollBasket", params);

            } else {
                bgl.common.pubsub.emit("PollBasket");
            }
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        storeCoverDetailsChangesForTagManager: storeCoverDetailsChangesForTagManager,
        onSuccess: onSuccess
    };
})();

bgl.components.excesses = (function () {

    var configuration;
    var container;
    var excessBar;
    var form;
    var customSelectControl;

    function loadComponent(data, componentUrl, containerElementId) {
        configuration = data.ComponentConfiguration;

        bgl.common.loader.load(data,
            componentUrl,
            containerElementId,
            function () {
                initComponent(configuration, containerElementId);
            });
    }

    function initComponent(componentConfiguration, containerElementId) {

        configuration = componentConfiguration;

        container = $('#' + containerElementId);
        excessBar = container.find(".bar-graph__bar--two");

        updateExcessesBar();

        initSubmitForm();

        bgl.common.accordions.init();

        initCustomSelect();
    }

    function initCustomSelect() {
        customSelectControl = $(".excess-bar__button-wrapper");

        $(".slide-panel").on("click", function () {
            bgl.common.customselect.closeOptions(customSelectControl, toggleCustomSelectButtonText);
        });;

        bgl.common.customselect.getButton(customSelectControl).on("click", function (event) {
            event.stopPropagation();
            bgl.common.customselect.toggleOptions(customSelectControl, toggleCustomSelectButtonText);
        });

        bgl.common.customselect.getContainer(customSelectControl).on("click keydown keypress keyup", function (event) {
            event.stopPropagation();
        });

        bgl.common.customselect.getItems(customSelectControl).each(function () {
            $(this).on("click", function (event) {
                var activeElement = $(event.target);
                bgl.common.customselect.selectItem(activeElement, selectExcess);
                bgl.common.customselect.closeOptions(customSelectControl, toggleCustomSelectButtonText);
            });

            $(this).on("keydown", function (event) {
                bgl.common.customselect.navigate(event, customSelectControl, toggleCustomSelectButtonText);
            });

            $(this).on("keyup", function (event) {
                if (event.keyCode === ESCAPE_KEYCODE) {
                    bgl.common.customselect.closeOptions(customSelectControl, toggleCustomSelectButtonText);
                }
            })
        });
    }

    function initSubmitForm() {

        var submitButton = $('#' + configuration.Id + '_submit-button');
        form = $('#' + configuration.Id + '__cover_excesses-form');
        bgl.common.datastore.init(form);

        submitButton
            .off('click')
            .on("click",
                function (e) {
                    e.preventDefault();

                    var isEditable = submitButton.data('isEditable') === 'True';

                    if (!isEditable || !bgl.common.datastore.isDataChanged(form)) {
                        $('[data-dismiss="slide-panel"]').click();
                        bgl.common.datastore.removeSessionStorage(form);
                    } else {
                        bgl.common.validator.submitForm($(form), configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function updateExcessesBar() {

        var compulsoryAmount = parseInt(container.find(".compulsory-excess-amount").data("value"));
        var totalAmount = parseInt(container.find(".total-excess-amount").data("value"));

        excessBar.css('height', totalAmount ? (((totalAmount - compulsoryAmount) * 100 / totalAmount).toFixed(0) + '%') : '50%');
    }

    function toggleCustomSelectButtonText() {
        var buttonText = bgl.common.customselect.isOpen(customSelectControl) ? 'Cancel' : 'Edit';
        bgl.common.customselect.getButton(customSelectControl).text(buttonText);
    }

    function selectExcess() {
        var selectedItem = bgl.common.customselect.getSelectedItem(customSelectControl);
        var voluntaryExcessAmount = parseInt(selectedItem.data('value'));
        var compulsoryExcessAmount = parseInt(container.find(".compulsory-excess-amount").text());
        var totalExcessAmount = voluntaryExcessAmount + compulsoryExcessAmount;
        var totalExcess = container.find(".total-excess-amount");

        totalExcess.data('value', totalExcessAmount).text(totalExcessAmount);

        container.find(".voluntary-excess-amount").text(voluntaryExcessAmount);
        container.find('#Amount').val(voluntaryExcessAmount);

        updateExcessesBar();
    }

    function onSuccess() {
        $('[data-dismiss="slide-panel"]').click();
        bgl.common.datastore.removeSessionStorage(form);

        bgl.common.utilities.startUnderwritingPolling();
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess
    };
})();

bgl.components.noclaimsdiscount = (function () {
    var configuration,
        BUTTON_TEXT = 'Edit number of years',
        ncdYears,
        form,
        customSelectControl;

    function updateNoClaimsText() {
        var selectedItem = bgl.common.customselect.getSelectedItem(customSelectControl);
        var selectedValue = selectedItem.text().replace('+', '');
        ncdYears = parseInt(selectedValue);

        $('#NcdYears').val(selectedValue);

        selectedValue = appendUnit(selectedValue);

        $(".ncd-edit__value").html(selectedValue);
    }

    function appendUnit(selectedValue) {
        switch (selectedValue) {

            case '1':
                selectedValue += ' year';
                break;

            case '9':
                selectedValue += ' years or more';
                break;

            default:
                selectedValue += ' years';
                break;
        }

        return selectedValue;
    }

    function toggleCustomButtonText() {
        var buttonText = bgl.common.customselect.isOpen(customSelectControl) ? "Cancel" : BUTTON_TEXT;
        bgl.common.customselect.getButton(customSelectControl).text(buttonText);
    }

    function initCustomSelect() {
        customSelectControl = $(".ncd-edit");

        $(".slide-panel").on("click", function () {
            bgl.common.customselect.closeOptions(customSelectControl, toggleCustomButtonText);
        });;

        bgl.common.customselect.getButton(customSelectControl).on("click", function (event) {
            event.stopPropagation();
            bgl.common.customselect.toggleOptions(customSelectControl, toggleCustomButtonText);
        });

        bgl.common.customselect.getContainer(customSelectControl).on("click keydown keypress keyup", function (event) {
            event.stopPropagation();
        });

        bgl.common.customselect.getItems(customSelectControl).each(function () {
            $(this).on("click", function (event) {
                var activeElement = $(event.target);
                bgl.common.customselect.selectItem(activeElement, updateNoClaimsText);
                bgl.common.customselect.closeOptions(customSelectControl, toggleCustomButtonText);
            });

            $(this).on("keydown", function (event) {
                bgl.common.customselect.navigate(event, customSelectControl, toggleCustomButtonText);
            });

            $(this).on("keyup", function (event) {
                if (event.keyCode === ESCAPE_KEYCODE) {
                    bgl.common.customselect.closeOptions(customSelectControl, toggleCustomButtonText);
                }
            })
        });
    }

    function initValidation() {
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        form.off("submit")
            .on("submit",
                function (e) {
                    e.preventDefault();

                    if (!bgl.common.datastore.isDataChanged(form)) {
                        $('[data-dismiss="slide-panel"]').click();
                        bgl.common.datastore.removeSessionStorage(form);
                        return;
                    }
                    storeUserChangesForTagManager();
                    bgl.common.validator.validate(configuration, $(this));
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function storeUserChangesForTagManager() {
        var data = getPreviouslyStoredChangedUserData();
        data.push(["NcdYears", $("#NcdYears").val()]);
        setPreviouslyStoredChangedVehicleData(data);
    }

    function getPreviouslyStoredChangedUserData() {
        var data = [];

        if (sessionStorage.getItem("ChangedUserData") !== null) {
            data = JSON.parse(sessionStorage.getItem("ChangedUserData"));
        }

        return data;
    }

    function setPreviouslyStoredChangedVehicleData(data) {
        sessionStorage.setItem("ChangedUserData", JSON.stringify(data));
    }

    function subscribeForEvents() {
        if (!configuration.IsService) {
            initValidation();
            initCustomSelect();
        } else {
            $('input[type="submit"]').on('click', function (e) {
                e.preventDefault();
                $('[data-dismiss="slide-panel"]').click();
            })
        }
        bgl.common.accordions.init();
    }

    function loadComponent(data, componentUrl, containerElementId) {
        configuration = data.ComponentConfiguration;
        bgl.common.loader.load(data,
            componentUrl,
            containerElementId,
            function () {
                initComponent(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        subscribeForEvents();
    }

    function successCallback(message) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: message
        });

        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        if (!configuration.IsService) {
            if (ncdYears <= 3) {
                sessionStorage.removeItem(sessionStorageConstants.CoverDetailsPncdAddonIsYesClicked);
                sessionStorage.removeItem(sessionStorageConstants.CoverDetailsPriceProtectionAddonIsYesClicked);
            }

            if (data.houstonMessage) {
                var params = {
                    callback: function () {
                        successCallback(data.houstonMessage);
                    }
                };

                bgl.common.pubsub.emit("PollBasket", params);

            } else {
                bgl.common.pubsub.emit("PollBasket");
            }

            bgl.common.datastore.removeSessionStorage(form);
        }
        $('[data-dismiss="slide-panel"]').click();
    }

    function onError() {
        bgl.components.noclaimsdiscount.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        load: loadComponent,
        storeUserChangesForTagManager: storeUserChangesForTagManager,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.annualmileage = (function () {
    var containerId;
    var configuration;
    var form;

    function loadComponent(model, componentUrl, containerElementId) {

        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.annualmileage.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        subscribeToEvents(componentConfiguration);
    }

    function subscribeToEvents(configuration) {

        form = $('#' + configuration.Id + '__annual-mileage-form');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                if (!bgl.common.datastore.isDataChanged(form)) {
                    bgl.components.coverdetails.common.setNextAction(configuration, false);
                    bgl.common.datastore.removeSessionStorage(form);
                    return;
                }

                bgl.common.validator.validate(configuration, form);
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

        var numberBox = $("#" + configuration.Id + "__annual-mileage");

        calculateAverageMiles(numberBox.val());

        numberBox.off("keydown").on("keydown",
            function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ||
                    $(this).val().length === 5 ||
                    (e.keyCode === 96 && $(this).val().length === 0)) {
                    e.preventDefault();
                }
            });

        numberBox.off("keyup").on("keyup",
            function () {
                var val = $(this).val();
                while (val.substring(0, 1) === '0') {
                    val = val.substring(1);
                }
                $(this).val(val);
            });

        numberBox.off("input").on("input",
            function (e) {
                e.preventDefault();

                var value = $(e.target).val();

                calculateAverageMiles(value);
            });
    }

    function calculateAverageMiles(value) {

        var label = $("#" + configuration.Id + "__annual-mileage-label");
        var labelTemplateValue = $("#labelTemplate").val();

        if (value !== undefined && value !== "") {
            var averageMiles = Math.round(value / 52, 0);

            if (averageMiles === 0) {
                label.hide();
            } else {
                label.show();
                var text = $.parseHTML(labelTemplateValue.replace("{mileage}", averageMiles));
                label.html(text);
            }
        }
    }

    function onSuccess() {
        var isDataChanges = bgl.common.datastore.isDataChanged(form);
        bgl.common.datastore.removeSessionStorage(form);

        bgl.components.coverdetails.common.setNextAction(configuration, isDataChanges);

        if (!configuration.IsService) {
            bgl.common.utilities.startUnderwritingPolling();
        }
    }

    function onError(componentConfiguration, error) {
        subscribeToEvents(componentConfiguration);
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.vehicleusage = (function () {
    var configuration,
        containerId,
        url,
        coverId,
        isVan,
        form;
    var carriageOfOwnGoods = "21",
        haulage = "22";

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;
        containerId = containerElementId;
        url = componentUrl;
        coverId = componentConfiguration.CoverId;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        isVan = $('#IsVan').val();
        form = $('#vehicle-usage');

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {
        form.off('submit')
            .on('submit',
                function (e) {

                    e.preventDefault();
                    
                    setDataHasChangedFlagForQuestionSet();

                    if (isVan === "True") {
                        vanNavigation();
                        return;
                    } else {
                        if (!bgl.common.datastore.isDataChanged(form)) {
                            bgl.components.coverdetails.common.setNextAction(configuration.ComponentConfiguration,
                                false);
                            bgl.common.datastore.removeSessionStorage(form);
                            return;
                        } else {
                            bgl.common.validator.validate(configuration, form);
                        }
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function onSuccess(configuration, data) {
        var isDataChanges = bgl.common.datastore.isDataChanged(form);
        bgl.common.datastore.removeSessionStorage(form);

        bgl.components.coverdetails.common.setNextAction(configuration.ComponentConfiguration, isDataChanges);

        if (!configuration.ComponentConfiguration.IsService) {
            bgl.common.utilities.startUnderwritingPolling();
        }
    }

  function vanNavigation() {
    var vehicleUsage = $('input[name=ClassOfUseCode]:checked', '#vehicle-usage').val();
    if (typeof bgl.common.pubsub !== 'undefined') {
      bgl.common.pubsub.emit('fakeFormSubmit', 'VehicleUsage');
    }
    if (vehicleUsage === carriageOfOwnGoods || vehicleUsage === haulage) {
      var yearsEstablishedActionUrl = $('#vehicleUseSubmitBtn').data('years-established-action-url');    
      bgl.components.coverdetails.yearsbusinessestablished.load(configuration.ComponentConfiguration, yearsEstablishedActionUrl, containerId);
    } else {
      var dangerousGoodsActionUrl = $('#vehicleUseSubmitBtn').data('dangerous-goods-action-url');
      bgl.components.coverdetails.carriageofdangerousgoods.load(configuration.ComponentConfiguration, dangerousGoodsActionUrl, containerId);
    }
  }

    function onError() {
        bgl.components.vehicleusage.init(configuration);
    }

    function setDataHasChangedFlagForQuestionSet() {
        var vehicleUseSessionData = JSON.parse(sessionStorage.getItem("CoverDetailsVehicleUse"));

        if (bgl.common.datastore.isDataChanged(form)) {
            vehicleUseSessionData.DataHasChanged = "True";
            sessionStorage.setItem("CoverDetailsVehicleUse", JSON.stringify(vehicleUseSessionData));
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.maindriver = (function () {
    var configuration,
        form;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#main-driver');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (!bgl.common.datastore.isDataChanged(form)) {
                        $('[data-dismiss="slide-panel"]').click();
                        bgl.common.datastore.removeSessionStorage(form);
                        return;
                    }

                    bgl.common.validator.validate(configuration, form);
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function successCallback(houstonMessage) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: houstonMessage
        });
        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        bgl.common.utilities.setUrlHashValue('main');

        if (data.houstonMessage) {
            var params = {
                callback: function () {
                    successCallback(data.houstonMessage);
                }
            };

            bgl.common.pubsub.emit("PollBasket", params);

        } else {
            bgl.common.pubsub.emit("PollBasket");
        }

        $('[data-dismiss="slide-panel"]').click();
        bgl.common.datastore.removeSessionStorage(form);
    }

    function onError() {
        bgl.components.maindriver.init(configuration);
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.coverdetails.common;
}