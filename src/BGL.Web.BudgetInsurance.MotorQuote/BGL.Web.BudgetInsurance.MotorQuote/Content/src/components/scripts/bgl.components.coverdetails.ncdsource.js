﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.ncdsource = (function () {
    var configuration,
        form;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#ncd-source-form');

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {

        form.off('submit').on('submit',
            function () {
                event.preventDefault();

                if (bgl.common.validator.isFormValid(form)) {

                    if (bgl.common.datastore.isDataChanged(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    } else {
                        onSuccess();
                    }
                }
            });

    }

    function onSuccess() {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onError() {
        bgl.components.coverdetails.ncdsource(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();