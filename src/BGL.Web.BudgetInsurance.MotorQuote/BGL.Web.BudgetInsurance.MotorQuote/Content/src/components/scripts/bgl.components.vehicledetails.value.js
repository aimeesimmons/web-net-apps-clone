﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

bgl.components.vehicledetails.value = (function () {

    var containerId;
    var configuration;
    var form;

    function loadComponent(model, componentUrl, containerElementId) {

        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.value.init(configuration);
            });
    }

    function initComponent(configuration) {
        subscribeToEvents(configuration);
    }

    function subscribeToEvents(configuration) {
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        bgl.common.validator.init(form);

        bgl.common.validator.initNumberInputField($("#Vehicle-Value"), $("#Vehicle-Value").data("valRangeMax").toString().length);

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));

                    storeVehicleChangesForTagManager();

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        function storeVehicleChangesForTagManager() {
            var data = bgl.components.vehicledetails.common.getPreviouslyStoredChangedVehicleData();
            var vehicleDetails = getCurrentVehicleChanges();
            var changesInSessionStorage = sessionStorage.getItem("VehicleLookupJourney");
            var currentVehicleChanges = JSON.parse(changesInSessionStorage);

            if (vehicleDetails !== null) {
                data.push(["CustomerVehicleValue", currentVehicleChanges.VehicleValue]);
                bgl.components.vehicledetails.common.setPreviouslyStoredChangedVehicleData(data);
            }
        }

        function getCurrentVehicleChanges() {

            var changesInSessionStorage = sessionStorage.getItem("VehicleLookupJourney");

            if (changesInSessionStorage !== null) {
                var currentVehicleChanges = JSON.parse(changesInSessionStorage);

                return {
                    VehicleValue: currentVehicleChanges.VehicleValue
                };
            }
            else {
                return null;
            }
        }

        if ($("#JourneyType").val() !== bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
            $("[data-step-type='previous-step']").on("click",
                function(e) {
                    e.preventDefault();

                    var model = bgl.common.datastore.getData(form);

                    model.ComponentConfiguration = configuration;

                    var url = $(e.target).attr("href");

                    bgl.components.vehicledetails.purchasedate.load(model, url, containerId);
                });
        }
    }

    function onSuccess(configuration, data) {
        if (data.journeyType.toString() === bgl.components.journeyTypesVehicleDetails.SingleEdit) {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit("PollBasket");
            }
            bgl.common.datastore.removeSessionStorage(form);
            return;
        }

        if (data.hasChanges === true) {
            bgl.components.vehicledetails.lookup.postVehicle(form, data.redirectUrl);
            return;
        }

        if (data.journeyType.toString() === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
            bgl.common.utilities.redirectToUrl(data.redirectUrl);
        }
    }

    function onError(configuration) {
        subscribeToEvents(configuration);
    }

    return {
        load: loadComponent,
        init: initComponent,
        subscribe: subscribeToEvents,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.vehicledetails;
}
