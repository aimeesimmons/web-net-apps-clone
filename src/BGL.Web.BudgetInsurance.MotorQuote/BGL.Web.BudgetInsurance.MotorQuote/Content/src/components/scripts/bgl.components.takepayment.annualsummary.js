﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.annualsummary = (function () {
  var componentConfig;
  var redirectInterval;
  var redirectTick = 0;
  var maxRedirectWait = 25;
  var requestVerificationToken;
  var isPaymentSuccessful = false;
  var componentConfiguration;
  var hideElementsWhen3DSecureIsDisplayedTimeout;

  function loadComponent(componentConfiguration, componentUrl, containerElementId) {
    componentConfiguration.CardSessionData = getTodaysPaymentSessionData();
    componentConfiguration.ContinuousPaymentCardSessionData = getContinuousPaymentCardSecureSessionData();

    var continuousPaymentCardEdit = sessionStorage.getItem("ContinuousPaymentCardEdit");
    componentConfiguration.ContinuousPaymentCardEdit = continuousPaymentCardEdit !== null ? continuousPaymentCardEdit : false;
    sessionStorage.setItem("ContinuousPaymentCardEdit", false);

    componentConfig = componentConfiguration;
    bgl.common.loader.load(componentConfiguration,
      componentUrl,
      containerElementId,
      function () {
        initComponent(componentConfiguration);
      });
  }

  function initComponent(configuration) {
    componentConfiguration = configuration;
    bgl.common.datastore.setupReadAndWrite();
    loadIframe();
    setupConfirmCheckbox();
    setupImportantInformation();
    requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

    $("#Card-Capture-Iframe").on("load", function () {
      hideElementsWhen3DSecureIsDisplayed();
    });

    if (isSecondPaymentMethodRequired()) {
      if ($("#Direct-Debit-Form").length > 0) {
        initDirectDebit();
      }

      if ($("#Pay-Now-Button").length > 0) {
        $("#Pay-Now-Button").on("click",
          function () {
            var continuousPaymentPaymentType = sessionStorage.getItem("ContinuousPaymentPaymentType");

            if (!$("#Read-Confirm-Checkbox").prop("checked")) {
              $("#Read-Confirm-Container").addClass("error");
            }
            else if (CreditCardIsSelected(continuousPaymentPaymentType)) {
              commitPaymentAndBasket(true);
            } else {
              DirectDebitIsSelectedAndValid(continuousPaymentPaymentType);
            }
          });
      }
      setupContinuousPaymentCardEditLink();
    } else {
      $("#Pay-Now-Button").on("click",
        function () {
          if (!$("#Read-Confirm-Checkbox").prop("checked")) {
            $("#Read-Confirm-Container").addClass("error");
          } else {
            commitPaymentAndBasket(false);
          }
        });
    }

    $("#Credit-Card-Selected-Person-Id").change(
      function () {
        initCardCapture();
      });
  }

  function initCardCapture() {
    sessionStorage.removeItem("ContinuousPaymentCardCaptureUri");
    sessionStorage.removeItem("ContinuousPaymentCardSecureSessionId");
    sessionStorage.removeItem("ContinuousPaymentCardThreeDSecureUri");

    if (hideElementsWhen3DSecureIsDisplayedTimeout) {
      clearTimeout(hideElementsWhen3DSecureIsDisplayedTimeout);
    }

    bgl.components.takepayment.cardcaptureiframe.clearIframe();

    componentConfiguration.CardSessionData = getTodaysPaymentSessionData();
    componentConfiguration.ContinuousPaymentCardSessionData = { "SelectedPersonId": $("#Credit-Card-Selected-Person-Id").val() };

    var dataToPost = {
      __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
      configuration: componentConfiguration
    };

    $.ajax({
      url: componentConfiguration.ApplicationPath + "TakePaymentComponent/InitialiseAnnualSummaryCardCapture",
      type: "POST",
      data: dataToPost,
      success: function (data) {
        sessionStorage.setItem("ContinuousPaymentCardCaptureUri", data.cardCaptureUri);
        sessionStorage.setItem("ContinuousPaymentCardSecureSessionId", data.secureSessionId);
        sessionStorage.setItem("ContinuousPaymentCardThreeDSecureUri", data.threeDSecureUri);

        loadIframe();
      }
    });
  }

  function loadIframe() {
    var cardSessionData = getContinuousPaymentCardSecureSessionData();
    bgl.components.takepayment.cardcaptureiframe.loadIframe(
      'Card-Capture-Iframe',
      cardSessionData.CardCaptureUri,
      cardSessionData.SecureSessionId,
      cardSessionData.ThreeDSecureUri);
  }

  function DirectDebitIsSelectedAndValid(continuousPaymentPaymentType) {
    if (continuousPaymentPaymentType === "DirectDebit" && $("#Direct-Debit-Form").length > 0) {
      isDirectDebitFormValid();
    }
  }

  function CreditCardIsSelected(continuousPaymentPaymentType) {
    return continuousPaymentPaymentType === "CreditCard";
  }

  function hideElementsWhen3DSecureIsDisplayed() {
    if ($("#Card-Capture-Iframe").contents().find("#continue").length === 0) {
      $("#Cardholder-Name-Form-Row").hide();
      $("#Monthly-Payment-DD-Label").hide();
      $("#Monthly-Payment-Card-Label").hide();
      scrollToCardPayment();
      return;
    }
    hideElementsWhen3DSecureIsDisplayedTimeout = setTimeout(function () {
      hideElementsWhen3DSecureIsDisplayed();
    }, 1000);
  }

  function scrollToCardPayment() {
    var requiredField = $('.card-payment');
    $('html,body').animate({ scrollTop: requiredField.offset().top }, 0);
  }

  function isSecondPaymentMethodRequired() {
    var isContinuousPaymentCard = sessionStorage.getItem("IsContinuousPaymentCard");
    if (isContinuousPaymentCard !== "False") {
      return false;
    }
    return true;
  }

  function initDirectDebit() {
    sortCodeAutoTab();
    setupDirectDebitSlideInPanel();
    bgl.common.validator.enableMaxLength();
    setupDirectDebitFormValidation();
    setupTabOffAuthoriseDirectDebitButtons();
  }

  function setupTabOffAuthoriseDirectDebitButtons() {
    $("#Direct-Debit-Authorise-Row label").on("click",
      function () {
        $("#" + $(this).attr("for")).click().focusout();
      });
  }

  function setupDirectDebitSlideInPanel() {
    $("#direct-debit-details").on("click",
      function () {
        waitForSlidePanelToLoad();
      });
  }

  function setupDirectDebitFormValidation() {
    bgl.common.validator.init($("#Direct-Debit-Form"),
      {
        onfocusout: function (element) {
          $(element).valid();
          checkSortcodeValidation(element);
          checkAccountNumberValidation(element);
        },
        hasSubmit: false
      });
  }

  function isDirectDebitFormValid() {
    if (!$("#Direct-Debit-Form").valid()) {
      checkSortcodeValidation($("#SortCodePart1"));
    } else {
      validateBankAccountAndSortCode();
    }
  }

  function setupContinuousPaymentCardEditLink() {
    if ($("#Edit-Card-Link").length > 0) {
      $("#Edit-Card-Link").on("click",
        function () {
          sessionStorage.setItem("ContinuousPaymentCardEdit", true);
          location.reload();
        });
    }
  }

  function setupImportantInformation() {
    $("#importantInfo-payments-annual, #importantInfo-bottom, #moreInfo-payments, #intermediary-services-contract-summary-slider-link, #intermediary-services-contract-summary-slider-link-read-confirm").off("click").on("click",
      function (event) {
        event.preventDefault();
        var link = $(this);
        bgl.common.loader.load({}, link.data("action-url"), link.data("container"));
      });
  }

  function setupConfirmCheckbox() {
    $("#Read-Confirm-Checkbox").on("click",
      function () {
        if ($("#Read-Confirm-Checkbox").prop("checked")) {
          $("#Read-Confirm-Container").removeClass("error");
        }
      });
  }

  function commitPaymentAndBasket(commitMultiplePayment) {
    var postMethod = commitMultiplePayment ? "CommitMultiplePaymentBasket" : "CommitSinglePaymentBasket";

    var dataToPost = {
      __RequestVerificationToken: requestVerificationToken,
      commitData: commitMultiplePayment ? getCommitMultiplePaymentData() : getCommitCardData()
    };

    $("body").html(bgl.common.spinner.getSpinnerBody("Processing your payment!",
      "Please do not hit the back button or refresh the page"));

    var urlPrefix = componentConfig.ApplicationPath !== null ? componentConfig.ApplicationPath : "/";

    $.ajax({
      url: urlPrefix + "TakePaymentComponent/" + postMethod,
      type: "POST",
      cache: false,
      dataType: "json",
      data: dataToPost,
      success: function (result) {

        if (result.isSuccess === false) {
          bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
          return;
        }
        if (result.PaymentSuccessful === true) {
          if (typeof bgl.common.pubsub !== 'undefined') {
            bgl.common.pubsub.emit('fakeFormSubmit', 'PaymentSuccessful');
          }
          dataLayerPushPaymentSuccessful();
          if (appInsights !== undefined) {
              appInsights.trackEvent("PaymentSuccessful", { "PolicyId": result.PolicyId });
              appInsights.flush();
          }
          isPaymentSuccessful = true;
        }
        sessionStorage.setItem("PaymentProcessedRedirectUrl", result.RedirectUrl);
        if (result.PolicyId) {
          var stringValue = result.PolicyId + "";
          var length = stringValue.indexOf("-");
          length = length > 0 ? length : stringValue.length;
          sessionStorage.setItem("PaymentProcessedPolicyId", stringValue.substring(0, length));
        }
      },
      complete: function () {
        redirectInterval = setInterval(checkAiComplete, 200);
      }
    });
  }

  function getCommitMultiplePaymentData() {
    var model = {};
    model.TodaysPaymentCard = getTodaysPaymentCardData();
    model.ContinuousPaymentCard = getContinuousPaymentCardData();
    model.ContinuousPaymentDirectDebit = getDirectDebitData();
    model.PaymentType = sessionStorage.getItem("ContinuousPaymentPaymentType");
    model.PaymentDeclinedUrl = componentConfig.PaymentDeclinedUrl;
    model.ComponentConfiguration = componentConfig;
    return model;
  }

  function getTodaysPaymentCardData() {
    var model = {};
    model.SelectedPersonIdText = sessionStorage.getItem("AnnualSelectedPersonIdText");
    model.CardCaptureUri = sessionStorage.getItem("AnnualCardCaptureUri");
    model.SecureSessionId = sessionStorage.getItem("AnnualSecureSessionId");
    model.SelectedPersonIdVal = sessionStorage.getItem("AnnualSelectedPersonIdVal");
    return model;
  }

  function getContinuousPaymentCardData() {
    var model = {};
    model.SelectedPersonIdText = sessionStorage.getItem("ContinuousPaymentCardSelectedPersonIdText");
    model.CardCaptureUri = sessionStorage.getItem("ContinuousPaymentCardCaptureUri");
    model.SecureSessionId = sessionStorage.getItem("ContinuousPaymentCardSecureSessionId");
    model.SelectedPersonIdVal = sessionStorage.getItem("ContinuousPaymentCardSelectedPersonIdVal");
    return model;
  }

  function getDirectDebitData() {
    var model = {};
    model.SelectedPersonIdText = sessionStorage.getItem("ContinuousPaymentDirectDebitSelectedPersonIdText");
    model.SelectedPersonIdVal = sessionStorage.getItem("ContinuousPaymentDirectDebitSelectedPersonIdVal");
    model.AccountNumber = sessionStorage.getItem("ContinuousPaymentDirectDebitAccountNumber");
    model.SortCode = buildSortCode();
    return model;
  }

  function buildSortCode() {
    if (sessionStorage.getItem("ContinuousPaymentDirectDebitSortCodePart1") !== null &&
      sessionStorage.getItem("ContinuousPaymentDirectDebitSortCodePart2") !== null &&
      sessionStorage.getItem("ContinuousPaymentDirectDebitSortCodePart3") !== null) {
      return sessionStorage.getItem("ContinuousPaymentDirectDebitSortCodePart1") +
        sessionStorage.getItem("ContinuousPaymentDirectDebitSortCodePart2") +
        sessionStorage.getItem("ContinuousPaymentDirectDebitSortCodePart3");
    }
    return null;
  }

  function checkAiComplete() {
    if (sessionStorage.getItem("AI_sentBuffer") === "[]" || redirectTick >= maxRedirectWait) {
      clearInterval(redirectInterval);
      if (isPaymentSuccessful) {
        bgl.common.utilities.redirectToUrl(componentConfig.PaymentSuccessfulUrl);
      } else {
        bgl.common.utilities.redirectToUrl(sessionStorage.getItem("PaymentProcessedRedirectUrl"));
      }
    }
    redirectTick += 1;
  }

  function getCommitCardData() {
    var model = {};
    model.TodaysPaymentCard = getTodaysPaymentCardData();
    model.PaymentDeclinedUrl = componentConfig.PaymentDeclinedUrl;
    model.ComponentConfiguration = componentConfig;
    return model;
  }

  function getTodaysPaymentSessionData() {
    return {
      "Name": sessionStorage.getItem("AnnualSelectedPersonIdText"),
      "CardCaptureUri": sessionStorage.getItem("AnnualCardCaptureUri"),
      "SecureSessionId": sessionStorage.getItem("AnnualSecureSessionId"),
      "SelectedPersonId": sessionStorage.getItem("AnnualSelectedPersonIdVal"),
      "ThreeDSecureUri": sessionStorage.getItem("AnnualThreeDSecureUri")
    };
  }

  function getContinuousPaymentCardSecureSessionData() {
    return {
      "Name": sessionStorage.getItem("ContinuousPaymentCardSelectedPersonIdText"),
      "CardCaptureUri": sessionStorage.getItem("ContinuousPaymentCardCaptureUri"),
      "SecureSessionId": sessionStorage.getItem("ContinuousPaymentCardSecureSessionId"),
      "SelectedPersonId": sessionStorage.getItem("ContinuousPaymentCardSelectedPersonIdVal"),
      "ThreeDSecureUri": sessionStorage.getItem("ContinuousPaymentCardThreeDSecureUri")
    };
  }

  function dataLayerPushPaymentSuccessful() {
    if (typeof dataLayer !== "undefined") {
      dataLayer.push({
        'event': "TEVirtualPageview",
        'virtualPageURL': "Payment/Successful",
        'virtualPageTitle': "Payment Successful"
      });
    }
  }

  function checkSortcodeValidation(element) {
    removeApiValidationErrorMessage(element);
    validateIndividualSortCodeInput(element);
    clearValidationErrorStylingWhenAllSortCodeInputsAreValid();
  }

  function removeApiValidationErrorMessage(element) {
    if ($(element).hasClass("form-row__input--sort-code") && !$("#sortcode-api-validation-message").hasClass("hide")) {
      RemoveSortCodeErrorStyle();
    }
  }

  function validateIndividualSortCodeInput(element) {
    if ($(element).valid()) {
      RemoveIndividualSortCodeErrorStyle(element);
    } else {
      AddIndividualSortCodeErrorStyle(element);
    }
    $("#form-row-sortcode1").removeClass("error");
  }

  function clearValidationErrorStylingWhenAllSortCodeInputsAreValid() {
    var allSortCodeInputsValid = checkAllSortCodeInputsAreValid();
    if (allSortCodeInputsValid) {
      $("#SortCodeLabel").removeClass("sort-code__label--error");
      $("#sortcode-validation-text").addClass("hide");
    }
  }

  function checkAllSortCodeInputsAreValid() {
    var allSortCodeInputsValid = true;

    $(".form-row__input--sort-code").each(function () {
      if ($(this).hasClass("sort-code__input--error")) {
        allSortCodeInputsValid = false;
      }
    });

    return allSortCodeInputsValid;
  }

  function sortCodeKeyUpEvent(event, currentSortCodePart, nextSortCodePart) {
    var TAB_KEYCODE = 9;
    var SHIFT_KEYCODE = 16;
    var LEFT_ARROW_KEYCODE = 37;
    var UP_ARROW_KEYCODE = 38;
    var RIGHT_ARROW_KEYCODE = 39;
    var DOWN_ARROW_KEYCODE = 40;

    var currentLength = $(currentSortCodePart).val().length;
    if (currentLength === 2
      && event.which !== TAB_KEYCODE
      && event.which !== SHIFT_KEYCODE
      && event.which !== LEFT_ARROW_KEYCODE
      && event.which !== UP_ARROW_KEYCODE
      && event.which !== RIGHT_ARROW_KEYCODE
      && event.which !== DOWN_ARROW_KEYCODE) {
      $(nextSortCodePart).focus().select();
    }
  }

  function sortCodeAutoTab() {
      var container1 = document.getElementById("SortCodePart1");
      var container2 = document.getElementById("SortCodePart2");

      container1.onkeyup = function (e) {
          sortCodeKeyUpEvent(e, "#SortCodePart1", "#SortCodePart2")
      };

      container2.onkeyup = function (e) {
          sortCodeKeyUpEvent(e, "#SortCodePart2", "#SortCodePart3")
      };
  }

  function waitForSlidePanelToLoad() {
    window.setTimeout(function () {
      if ($("#SlidePanel").hasClass("active")) {
        var directDebitDetails = $("#direct-debit-info").html();
        $("#SlidePanelContent").html(directDebitDetails);
      } else {
        waitForSlidePanelToLoad();
      }
    },
    100);
  }

  function validateBankAccountAndSortCode() {
    $("#direct-debit-sort-code").val($("#SortCodePart1").val() + $("#SortCodePart2").val() + $("#SortCodePart3").val());
    var dataToPost = {
      __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
      sortcode: $("#direct-debit-sort-code").val(),
      accountnumber: $("#direct-debit-account-number").val()
    };

    $.ajax({
      url: componentConfig.ApplicationPath + "TakePaymentComponent/" + "PostAccountNumberAndSortCode",
      type: "POST",
      cache: false,
      dataType: "json",
      data: dataToPost,
      success: function (result) {
        if (result.isSuccess === false) {
          window.location.href = result.errorRedirectUrl;
        }
        else if (result.Result === "Validated") {
          RemoveSortCodeErrorStyle();
          RemoveAccountErrorStyle();
          commitPaymentAndBasket(true);
        }
        else if (result.Result === "sortCode invalid") {
          AddSortCodeErrorStyle();
        }
        else if (result.Result === "accountNumber invalid") {
          AddAccountErrorStyle();
        }
      }
    });
  }

  function RemoveSortCodeErrorStyle() {
    $("#sortcode-api-validation-message").addClass("hide");
    $("#SortCodeLabel").removeClass("sort-code__label--error");
    $("#sortcode-api-validation-message").removeClass("sort-code__span--error");
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").removeClass("sort-code__input--error");
    cleanAriaDescribedBy("#SortCodePart1");
  }

  function AddSortCodeErrorStyle() {
    $("#sortcode-api-validation-message").removeClass("hide");
    $("#SortCodeLabel").addClass("sort-code__label--error");
    $("#sortcode-api-validation-message").addClass("sort-code__span--error");
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").addClass("sort-code__input--error");
  }

  function AddAccountErrorStyle() {
    $("#account-number-validation-message").removeClass("hide");
    $("#AccountNumberLabel").addClass("sort-code__label--error");
    $("#account-number-validation-message").addClass("sort-code__span--error");
    $("#direct-debit-account-number").addClass("sort-code__input--error");
  }

  function RemoveAccountErrorStyle() {
    $("#account-number-validation-message").addClass("hide");
    $("#AccountNumberLabel").removeClass("sort-code__label--error");
    $("#account-number-validation-message").removeClass("sort-code__span--error");
    $("#direct-debit-account-number").removeClass("sort-code__input--error");
    cleanAriaDescribedBy("#direct-debit-account-number");
  }

  function cleanAriaDescribedBy(element) {
    if ($(element).attr("aria-describedby")) {
      $(element).removeAttr("aria-describedby");
    }
  }

  function RemoveIndividualSortCodeErrorStyle(element) {
    $(element).removeClass("sort-code__input--error");
  }

  function AddIndividualSortCodeErrorStyle(element) {
    $("#SortCodeLabel").addClass("sort-code__label--error");
    $("#sortcode-validation-text").removeClass("hide");
    $(element).addClass("sort-code__input--error");
  }

  function checkAccountNumberValidation(element) {
    var isValid = false;

    if ($(element).hasClass("form-row__input account-number sort-code__input--error")) {
      $("#account-number-validation-message").addClass("hide");
    }

    if ($(element).hasClass("form-row__input account-number")) {
      isValid = $("#direct-debit-account-number").valid();
    }

    if (isValid) {
      RemoveAccountErrorStyle();
    }
  }

  return {
    init: initComponent,
    load: loadComponent
  };
})();

if (typeof module !== "undefined") {
  module.exports = bgl.components.takepayment.annualsummary;
}