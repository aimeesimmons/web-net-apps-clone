﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.additionaldetails = bgl.components.additionaldetails || {};

bgl.components.additionaldetails.common = (function () {
    var configuration;
    var form;
    var monthValidation, yearValidation;
    var monthDropdown;
    var yearDropdown;
    var fullYearMonthOptions;
    var currentYearMonthOptions;
    var currentMonth;
    var sectionLabel;
    var errorClass = 'error';

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            bgl.components.additionaldetails.common.init(componentConfiguration);
            bgl.components.additionaldetails.vehicleusage.init(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#' + componentConfiguration.Id + "-form-additional-details");
        monthValidation = $('#Month-Section');
        yearValidation = $('#Year-Section');
        monthDropdown = form.find('#Month');
        yearDropdown = form.find('#Year');
        sectionLabel = $("#Additional-Input-Purchase-Section");

        bgl.common.validator.init(form, {
            onfocusout: resetValidation,
            onclick: resetValidation
        });

        subscribeToEvents();
        almostThereButtonSubmit();

        if (yearDropdown.length > 0) {
            var currentDate = new Date();
            currentMonth = currentDate.getMonth() + 1;
            fullYearMonthOptions = monthDropdown.find('option');
            currentYearMonthOptions = fullYearMonthOptions.slice(0, currentMonth + 1);
            updateMonthDropdown();
        }
    }

    function resetValidation(element) {
        bgl.common.validator.resetValidationState($(element));
        purchaseSectionValidationMessage();
    }

    function almostThereButtonSubmit() {
        $("#almost-there-button").on("click",
            function() {
                form.submit();
            });
    }

    function subscribeToEvents() {
        form.off('submit').on('submit',
            function (event) {
                event.preventDefault();

                updateRegistrationValue($("#Additional-Input-Registration"));

                bgl.common.validator.validate(configuration, $(this));

                purchaseSectionValidationMessage();
            });

        $("#Additional-Input-Registration").off("input").on("input",
            function () {

                var start = this.selectionStart;
                var end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });

        $("#Additional-BackButton").off("click").on("click",
            function () {
                var options = {
                    callback: function () {
                        window.location = configuration.PreviousPageUrl;
                    }
                };
                bgl.common.pubsub.emit('PollBasket', options);
            });

        yearDropdown.on("change", updateMonthDropdown);

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function purchaseSectionValidationMessage() {
        if (monthValidation.hasClass(errorClass) && yearValidation.hasClass(errorClass)) {
            sectionLabel.addClass(errorClass);
        } else {
            sectionLabel.removeClass(errorClass);
        }
    }

    function updateRegistrationValue(inputField) {
        if (inputField.length !== 0) {
            inputField.val(inputField.val().replace(/\s+/gi, ""));
        }
    }

    function onSuccess(configuration, data) {
        bgl.common.pubsub.emit('PollBasket',
            {
                callback: function () {
                    bgl.common.utilities.redirectToUrl(data.redirectUrl);
                },
                hideNotification: true
            });
    }

    function onError(configuration) {
        var params = {
            callback: bgl.common.overlay.hide,
            spinnerName: "addonSpinner"
        };

        bgl.common.pubsub.emit("PollBasket", params);
        bgl.components.additionaldetails.common.init(configuration);
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $(yearDropdown).find("option:eq(1)").val();

        if (yearDropdown.val() == currentYear) {
            months.html(currentYearMonthOptions);
        }
        else {
            months.html(fullYearMonthOptions);
        }

        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.additionaldetails.vehicleusage = (function () {

    function initComponent() {
        toggleHelpText($("[data-usagebtn]:checked").data("usagebtn"));

        $("[data-usagebtn]").on("change", function () {
            toggleHelpText($(this).data("usagebtn"));
        });
    }

    function toggleHelpText(selectedItemCode) {
        $("[data-usagehelptext]").addClass("hide");

        if (selectedItemCode) {
            $("[data-usagehelptext=" + selectedItemCode + "]").removeClass("hide");
        }
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.additionaldetails.common;
}
