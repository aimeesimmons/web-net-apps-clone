﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.pagereloader = bgl.components.pagereloader || {};

bgl.components.pagereloader.documentloader = (function () {

    var applicationPath = '';

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {

        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        applicationPath = componentConfiguration.ApplicationPath;
        showSpinner();
        pollDocument();
    }

    function showSpinner() {
        var spinnerHtml = bgl.common.spinner.getSpinnerBody("Working on it",
            "We won't be a moment");
        $('#spinner').html(spinnerHtml);
    }
    function showSpinnerDownloadCompletedMsg() {
       var downloadCompletedHtml = "<main>" +
                "<div class=\"page-section\">" +
                "<div class=\"page-section--inner\">" +
                "<div class=\"spinner\">" +
                "<div class=\"spinner__tick\"><\/div>" +
                "<h2 class=\"spinner__heading\"><span>Document downloaded<\/span><\/h2>" +
                "<p class=\"spinner__text\"><span>Please close this tab to continue your journey<\/span><\/p>" +
                "<\/div>" +
                "<\/div>" +
                "<\/div>" +
                "<\/main>";

            $('#spinner').html(downloadCompletedHtml);
    }


    function pollDocument() {
        var interval = 1000;
        var timeoutDuration = 120000;
        var pollUrl = 'PageReloaderComponent/IsDocumentReady';
        var tickCount = new Date().getTime();
        var timeout = tickCount + timeoutDuration;
        poll(timeout, interval, pollUrl);
    }

    function poll(timeout, interval, pollUrl) { 
        var token = $('input[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: applicationPath + pollUrl,
            type: 'Post',
            cache: false,
            dataType: 'json',
            data: { 
                __RequestVerificationToken: token,
                url: $('#documentUrl').val()
            },
            success: function (response) {
                if (response.isDocumentReady === false) {
                    if (timeout >= new Date().getTime()) {
                        setTimeout(function () {
                            poll(timeout, interval, pollUrl);
                        }, interval);
                    }
                    else {
                        goToErrorPage(applicationPath);
                    }
                } else if (response.isDocumentReady === true) {
                    DownloadDocument();
                    showSpinnerDownloadCompletedMsg();
                   
                } else {
                    goToErrorPage(applicationPath);
                }
            },
            error: function () {
                goToErrorPage(applicationPath);
            }
        });
    }
    function goToErrorPage() {
        window.location = applicationPath + 'Error';
    }

    function DownloadDocument() {
        $('#__DocumentLoaderForm').submit();
    }
    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.pagereloader;
}