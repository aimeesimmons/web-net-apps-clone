﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.annualmileage = (function() {
    var form;
    var configuration;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
		configuration = componentConfiguration;

		bgl.common.loader.load(componentConfiguration,
			componentUrl,
			containerElementId,
			function () {
				initComponent(componentConfiguration);
			});
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#' + configuration.Id + '__annual-mileage-form');
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init(form);
        subscribeToEvents(configuration);
    }

    function subscribeToEvents(configuration) {

        var numberBox = $("#" + configuration.Id + "__annual-mileage");
        var continueButtonLink = $("#ContinueButton");

		calculateAverageMiles(numberBox.val());

		numberBox.off("keydown").on("keydown",
			function (e) {
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
					(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
					(e.keyCode >= 35 && e.keyCode <= 40)) {
					return;
				}
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ||
					$(this).val().length === 5 ||
					(e.keyCode === 96 && $(this).val().length === 0)) {
					e.preventDefault();
				}
			});

		numberBox.on("keyup",
			function () {
				var val = $(this).val();
				while (val.substring(0, 1) === '0') {
					val = val.substring(1);
				}
                $(this).val(val);
			});

		numberBox.off("input").on("input",
			function (e) {
				e.preventDefault();

				var value = $(e.target).val();

                calculateAverageMiles(value);
            });

        continueButtonLink.off("click").on("click",
            function (e) {
                e.preventDefault();
                if (bgl.common.validator.isFormValid(form)) {
                    bgl.common.validator.submitForm(form, configuration);
                }
            });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
                });
    }

    function calculateAverageMiles(value) {
		var label = $("#" + configuration.Id + "__annual-mileage-label");
		var labelTemplateValue = $("#labelTemplate").val();

		if (value !== undefined && value !== "") {
			var averageMiles = Math.round(value / 52, 0);

			if (averageMiles === 0) {
				label.hide();
			} else {
				label.show();
				var text = $.parseHTML(labelTemplateValue.replace("{mileage}", averageMiles));
				label.html(text);
			}
		}
    }

    function onSuccess(componentConfiguration) {
        location.href = componentConfiguration.NextStepUrl;
    }
    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess
    };
})();