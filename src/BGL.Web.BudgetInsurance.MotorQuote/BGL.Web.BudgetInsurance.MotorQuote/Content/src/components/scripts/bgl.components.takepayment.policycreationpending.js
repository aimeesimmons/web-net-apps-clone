﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.policycreationpending = (function () {

    function initComponent(componentConfiguration) {
        getPolicyNumber();
    }

    function getPolicyNumber() {
        var policyNumber = sessionStorage.getItem("PaymentProcessedPolicyId");
        $("#policy-number").text(policyNumber);
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.policycreationpending;
}
