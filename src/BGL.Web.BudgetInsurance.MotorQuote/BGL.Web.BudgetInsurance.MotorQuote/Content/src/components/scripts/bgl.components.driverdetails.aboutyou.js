﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.aboutyou = (function () {
    var form;
    var configuration;
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {

            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#about-you-form');
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init(form);
        bgl.common.date.initDateFormat($('#DateOfBirth'));
        form.off('submit')
            .on('submit',
            function (event) {
                event.preventDefault();
                if (bgl.common.validator.isFormValid(form)) {
                    bgl.common.validator.submitForm(form, configuration);
                }
                
            });
    }

    function onSuccess(configuration, data) {
        location.href = configuration.NextStepUrl;
    }

    function onError() {
        bgl.components.driverdetails.aboutyou.init(configuration);
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();