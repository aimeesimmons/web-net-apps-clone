﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.addpaymentmethodconfirmation = (function () {
    var componentConfig;
    var requestVerificationToken;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        componentConfiguration.CardSessionData = getTodaysPaymentSessionData();
        componentConfiguration.ContinuousPaymentCardSessionData = getContinuousPaymentCardSecureSessionData();

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();
        componentConfig = componentConfiguration;
        bgl.common.datastore.setupReadAndWrite();
        if (window.sessionStorage.getItem("ActiveTab") === "BankAccount") {
            populateData();
        } else {
            var cardHolderName = sessionStorage.getItem("AddPaymentDirectDebitSelectedPersonIdText");
            $('#Account-Holder-Name strong').text(cardHolderName);
        }

        $("#Confirm-Direct-Debit-Details").off().on("click",
            function () {
                commitDirectDebitPayment();
            });
        $("#Confirm-Credit-Card-Details").off().on("click",
            function () {
                commitCreditCardPayment();
            });

        $("#Edit-Direct-Debit-Details,#Back-To-Edit-Direct-Debit-Details").on("click",
            function () {
                backToAddPaymentMethod();
            });
        bgl.common.pubsub.on('session:end', function () {
            cleanUpSessionStorage();
        });
    }

    function cleanUpSessionStorage() {
        var keysToRemove = ["AddPaymentDirectDebitAccountNumber",
            "AddPaymentDirectDebitSortCodePart1",
            "AddPaymentDirectDebitSortCodePart2",
            "AddPaymentDirectDebitSortCodePart3",
            "AddPaymentDirectDebitAuthorise",
            "OneOffPaymentSelectedPersonIdText",
            "OneOffPaymentCardCaptureUri",
            "OneOffPaymentSecureSessionId",
            "OneOffPaymentSelectedPersonIdVal",
            "OneOffPaymentSelectedPersonId"
        ];

        keysToRemove.forEach(function (key) {
            window.sessionStorage.removeItem(key);
        });
    }

    function populateData() {
        var sortCode = buildSortCode();
        var accountNumber = sessionStorage.getItem('AddPaymentDirectDebitAccountNumber');
        var visibleAccountNumber = "**** " + accountNumber.substr(4);
        var accountHolderName = sessionStorage.getItem('AddPaymentDirectDebitSelectedPersonIdText');

        $('#Sort-Code strong').text(sortCode);
        $('#Account-Number').text(visibleAccountNumber);
        $('#Account-Holder-Name strong').text(accountHolderName);
    }

    function commitDirectDebitPayment() {
        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            addDirectDebitViewModel: getCommitDirectDebitData()
        };

        $.ajax({
            url: componentConfig.ApplicationPath + "TakePaymentComponent/CommitNewDirectDebitPaymentMethod",
            type: "POST",
            cache: false,
            dataType: "json",
            data: dataToPost,
            success: function (result) {
                cleanUpSessionStorage();
                if (result.isSuccess === false) {
                    bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
                    return false;
                }
                logPaymentMethodChange('Direct Debit');
                bgl.common.pubsub.emit('PollBasket',
                    {
                        hideNotification: true,
                        callback: function() {
                            bgl.components.takepayment.addpaymentmethodconfirmation.commitBasketCallback(result.accountId);
                        }
                    });
                return true;
            }
        });
    }

    function commitCreditCardPayment() {
        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            commitCreditCardViewModel: getCommitCreditCardData()
        };

        $.ajax({
            url: componentConfig.ApplicationPath + "TakePaymentComponent/CommitNewCreditCardPaymentMethod",
            type: "POST",
            cache: false,
            dataType: "json",
            data: dataToPost,
            success: function (result) {
                if (result.isSuccess === false) {
                    bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
                    return false;
                }
                logPaymentMethodChange('Card');
                bgl.common.pubsub.emit('PollBasket',
                    {
                        hideNotification: true,
                        
                        callback: function () {
                            bgl.components.takepayment.addpaymentmethodconfirmation.commitBasketCallback(result.accountId);
                        }
                    });
                return true;
            }
        });
    }

    function commitBasketCallback(accountId) {
        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            nominatedAccountId: accountId
        };

        $.ajax({
            url: componentConfig.ApplicationPath + "TakePaymentComponent/CommitBasketUpdatedPaymentMethod",
            type: "POST",
            cache: false,
            dataType: "json",
            data: dataToPost,
            success: function(result) {
                if (result.isSuccess === false) {
                    window.location.href = result.errorRedirectUrl;
                    return false;
                }
                if (result.viewOptions === "PaymentStatus") {
                    window.location.href = componentConfig.PaymentStatusUrl;
                } else if (result.viewOptions === "PaymentMethodAllDone") {
                    window.location.href = componentConfig.PaymentMethodAllDoneUrl;
                }
                return true;
            }
        });
    }

    function logPaymentMethodChange(newPaymentMethod) {
        sessionStorage.setItem('PaymentUpdated', newPaymentMethod);
    }

    function backToAddPaymentMethod() {
        bgl.common.utilities.redirectToUrl(componentConfig.RedirectUrl);
    }

    function getCommitDirectDebitData() {
        var model = {};
        var commitDirectDebitData = {};
        
        commitDirectDebitData.SelectedPersonIdVal = sessionStorage.getItem("AddPaymentDirectDebitSelectedPersonIdVal");
        commitDirectDebitData.AccountNumber = sessionStorage.getItem("AddPaymentDirectDebitAccountNumber");
        commitDirectDebitData.SortCode = buildSortCode().replace(/ /g, "");

        model.ComponentConfiguration = componentConfig;
        model.CommitDirectDebitData = commitDirectDebitData;
        
        return model;
    }

    function getCommitCreditCardData() {
        var model = {};

        model.SecureSessionId = sessionStorage.getItem("AddPaymentMethodSecureSessionId");
        model.SelectedPersonIdText = sessionStorage.getItem("AddPaymentDirectDebitSelectedPersonIdText");
        model.SelectedPersonIdVal = sessionStorage.getItem("AddPaymentCreditCardSelectedPersonId");
        return model;
    }

    function buildSortCode() {
        if (sessionStorage.getItem("AddPaymentDirectDebitSortCodePart1") !== null && 
            sessionStorage.getItem("AddPaymentDirectDebitSortCodePart2") !== null && 
            sessionStorage.getItem("AddPaymentDirectDebitSortCodePart3") !== null) {
            return sessionStorage.getItem("AddPaymentDirectDebitSortCodePart1") + " " +
                sessionStorage.getItem("AddPaymentDirectDebitSortCodePart2") + " " +
                sessionStorage.getItem("AddPaymentDirectDebitSortCodePart3");
        }
        return "";
    }

    function getTodaysPaymentSessionData() {
        return {
            "Name": sessionStorage.getItem("AddPaymentSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("AddPaymentMethodCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("AddPaymentMethodSecureSessionId")

        };
    }

    function getContinuousPaymentCardSecureSessionData() {
        return {
            "Name": sessionStorage.getItem("ContinuousPaymentCardSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("AddPaymentMethodCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("AddPaymentMethodSecureSessionId")
        };
    }

    return {
        init: initComponent,
        load: loadComponent,
        commitBasketCallback: commitBasketCallback
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.addpaymentmethodconfirmation;
}