﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.optionalextras = (function () {

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent();
        });
    }

    function initComponent() {
        bgl.common.accordions.init();
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.optionalextras;
}
