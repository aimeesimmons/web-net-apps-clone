﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.houston = (function () {
    var applicationPath = "/";
    var houstonButtonState = "";
    var brand = "";
    var livePersonBrand = "";
    var journey = "";
    var journeySection = "";
    var notificationTimeout;
    var configuration;
    var autoCleared = true;
    var defaultNotificationSettings = {
        autoClose: true,
        timeout: 10000
    };

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function getPage() {
        var pathname = location.pathname.split('/');
        return pathname[pathname.length - 1];
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        brand = configuration.Brand;
        livePersonBrand = configuration.LivePersonBrand;
        journey = configuration.Journey;
        journeySection = configuration.JourneySection;
        setState('loading');
        eventListener();
        $(document).ready(function() {
            var contextData = getContextData();

            if (contextData.VaContext != undefined && contextData.VaContext.ApplicationPath != undefined) {
                applicationPath = contextData.VaContext.ApplicationPath;
            }
            
            if (configuration.IsVaEnabled) {
                window.VAUserInitials = contextData.Initials;
                try {
                    var contextItems = contextData.VaContext !== null ? contextData.VaContext : {
                        Brand: brand,
                        BGLActualPageName: "Sales_TE" +
                            location.pathname.substr(location.pathname.lastIndexOf('/') + 1).toLowerCase(),
                        InvocationPoint: configuration.InvocationPoint,
                        WebSessionID: "Unknown",
                        QuoteNo: "N/A",
                        PolicyNo: "Unknown",
                        ArrayOfProducts: [
                            {
                                BGLProduct: configuration.Product,
                                BGLSeqNumber: "Unknown",
                                CoverLevel: "Unknown",
                                ProductStatus: "Unknown",
                                ProductVariant: "N/A"
                            }
                        ]
                    };
                    var livePersonContextItems = contextData.LivePersonContext !== null ? contextData.LivePersonContext : {
                        ActionName: getPage(),
                        Ctype: livePersonBrand + ": " + journey + ": " + journeySection,
                        CustomerName: sessionStorage.getItem("CustomerFirstName"),
                        Section: [livePersonBrand, journey, journeySection, getPage()],
                        SessionId: "N/A",
                        VaStatus: 7
                    };
                    bgl.components.houston.buildLivePersonEngagementAttributes(livePersonContextItems);
                    window.NinaVars.forceReprocessContextItems = false;
                    window.NinaVars.BGLContextItems = JSON.stringify(contextItems);
                    window.NinaVars.BGLBrand = contextItems.Brand;
                    $('#nina-block .nw_Input .nw_StatusMessage').remove();
                    cookiePolicyCheck(contextItems);
                } catch (err) {
                    if (window.NinaVars.preprod === true) {
                        console.warn(err.message);
                    }
                }
            }

            if ($('#EnhancedNotification').length) {
                if (sessionStorage.getItem("notification") !== null) {
                    bgl.components.houston.showEnhancedNotification(contextData);
                }
            } else {
                bgl.components.houston.showNotificationFromLocalStore(contextData);
            }
        });

        $('#houstonButton').on(whichAnimationEvent(), handleLoad);

        $('#houstonButton').on('click', function () {
            if (configuration.IsVaEnabled) {
                setInvocationPoint('#houstonButton');
                if ($(this).hasClass("ready")) {
                    window.NinaVars.forceReprocessContextItems = true;
                }
                clickHandler();
                var attributeVal = $('#houstonButton').attr('aria-label');
                if (attributeVal === 'Close Virtual Assistant' && configuration.IsVapEnabled) {
                    setInvocationForVap();
                }
            }
            else {
                bgl.common.pubsub.emit('showContactInformationNotification');
            }
        });

        $(document.body).on('click', '.notification-button, #notificationButton', function () {
            $('.notification-container').hide();
            autoCleared = false;
            resetFocus();
        });

        $(document.body).on('click', ".va-invocation",
            function (event) {
                $('.notification-container').hide();
                event.preventDefault();
                if (configuration.IsVaEnabled) {
                    setInvocationPoint(this);
                    window.NinaVars.forceReprocessContextItems = true;
                    setState('ready');
                    clickHandler();
                    if (configuration.IsVapEnabled){
                        setInvocationForVap(); 
                    }
                } else {
                    bgl.common.pubsub.emit('showContactInformationNotification');
                }
            });
    }


    function setInvocationForVap() {
        vapExchangeCompleteEventListener();
        var tokens = bgl.security.authentication.getAccessAndIdentityTokens();
        if (Object.keys(tokens).length === 0 || tokens === undefined) {
            var redirectUrl = encodeURIComponent(configuration.AuthRedirectUrl);
            var authUrl =
                bgl.security.authentication.getAuthenticationRequestUrl(configuration.ClientId,
                    redirectUrl,
                    configuration.AuthorizationEndpoint, configuration.Tid);
            var body = $(document.body);
            var div = $("<div />");
            var iframe = $("<iframe />")
                .attr({ src: authUrl, id: "VapHiddenIframe" })
                .addClass("hide");
            div.append(iframe);
            body.append(div);
        } else {
            window.NinaVars.vapAccessToken = tokens.accessToken;
            window.NinaVars.vapIdentityToken = tokens.identityToken;
        }
    }

    function vapExchangeCompleteEventListener() {
        bgl.common.pubsub.on('exchangeComplete',
            function () {
               var tokens = bgl.security.authentication.getAccessAndIdentityTokens();
                window.NinaVars.vapAccessToken = tokens.accessToken;
                window.NinaVars.vapIdentityToken = tokens.identityToken;
                $("#VapHiddenIframe").remove();
            });
    }

    function getFormattedNotification(url) {
        $.get(url,
            function (data) {
                if (data.errorRedirectUrl) {
                    bgl.common.utilities.redirectToUrl(data.errorRedirectUrl);
                } else {
                    bgl.components.houston.showFormattedNotification(data, { autoClose: false });
                }
            });
    }

    function eventListener() {
        bgl.common.pubsub.on('showNotification', function (data) {
            bgl.components.houston.showNotification(data);
        });

        bgl.common.pubsub.on('showContactDataNotification', function () {
            getFormattedNotification(configuration.ContactDataUrl);
        });

        bgl.common.pubsub.on('showContactInformationNotification', function () {
            getFormattedNotification(configuration.ContactInformationUrl);
        });

        bgl.common.pubsub.on('refreshHoustonContext', function (callback) {
            refreshContext(callback);
        });

        bgl.common.pubsub.on('PaymentComplete', function () {
            clearSession();
        });

        bgl.common.pubsub.on('CancelMTA', function (data) {
            $("#houstonConfirmation").removeClass("hide");

            $("#houstonConfirmation #continueButton").off().on("click",
                function () {
                    $("#houstonConfirmation").addClass("hide");
                });

            $("#houstonConfirmation #abandonButton").focus();
            $("#houstonConfirmation #abandonButton").off().on("click",
                function () {
                    deleteBasket(data ? data.redirectUrl : null);
                });
        });
    }

    function setInvocationPoint(element) {
        var invocationPoint = $(element).attr('data-va-invocation-point');
        try {
            var contextItems = getContextData().VaContext;
            if (contextItems === null) {
                contextItems = {
                    Brand: brand,
                    BGLActualPageName: "Sales_TE" +
                        location.pathname.substr(location.pathname.lastIndexOf('/') + 1).toLowerCase(),
                    InvocationPoint: configuration.InvocationPoint,
                    WebSessionID: "Unknown",
                    QuoteNo: "N/A",
                    PolicyNo: "Unknown",
                    ArrayOfProducts: [
                        {
                            BGLProduct: configuration.Product,
                            BGLSeqNumber: "Unknown",
                            CoverLevel: "Unknown",
                            ProductStatus: "Unknown",
                            ProductVariant: "N/A"
                        }
                    ]
                };
            }
            contextItems.InvocationPoint = invocationPoint;
            window.NinaVars.BGLContextItems = JSON.stringify(contextItems);
        } catch (err) {
            if (window.NinaVars.preprod === true) {
                console.warn(err.message);
            }
        }
    }

    function stopNotificationTimeout() {
        if (notificationTimeout !== undefined) {
            clearTimeout(notificationTimeout);
        }
    }

    function showNotification(message, notificationContext) {
        stopNotificationTimeout();

        if (message) {
            if (notificationContext) {
                if (typeof message === "object") {
                    if (notificationContext.IsMonthly) {
                        message = message.monthly;
                    } else {
                        message = message.annual;
                    }
                }
            }

            var context = getContextData();
            var notificationMessage = buildNotificationMessage(message, context);

            $('.notification-message').text(notificationMessage);
            showNotificationTimeout();
        }
    }

    function showFormattedNotification(html, notificationSettings) {
        stopNotificationTimeout();
        if (html) {
            $('.notification-message').html(html);
            showNotificationTimeout(notificationSettings);
        }
    }

    function showNotificationTimeout(notificationSettings) {
        var settings = $.extend({}, defaultNotificationSettings, notificationSettings);
        autoCleared = true;
        $('.notification-container').show(0).focus();
        $('.notification-container').attr('aria-live', 'polite');
        $('.notification-button').focus();
        if (settings.autoClose) {
            notificationTimeout = setTimeout(function () {
                $('.notification-container').hide(0);
                setFocusToElement();
            }, settings.timeout);
        }
    }

    function showNotificationFromLocalStore(contextData) {
        var notification = getNotificationFromLocalStore();
        if (notification) {
            showNotification(notification.msg, contextData.NotificationContext);
            sessionStorage.removeItem("notification");
        }
    }

    function showEnhancedNotification(contextData) {
        stopNotificationTimeout();

        if ($('#SaveEnhancedProduct').val() == 'True') {
            displayEnhancedNotification('#SaveEnhanced', '#SpendEnhanced');
        } else if ($('#SpendEnhancedProduct').val() == 'True') {
            displayEnhancedNotification('#SpendEnhanced', '#SaveEnhanced');
        } else {
            bgl.components.houston.showNotificationFromLocalStore(contextData);
        }
    }

    function setupEnhancedNotificationHtml() {
        var enhancedMessageHtml = $('#EnhancedNotification').html();
        $('.notification--enhanced').html(enhancedMessageHtml);
        $('#EnhancedNotification').remove();
    }

    function displayEnhancedNotification(shownMessageContainerId, hiddenMessageContainerId) {
        setupEnhancedNotificationHtml();
        sessionStorage.removeItem("notification");

        $(shownMessageContainerId).removeClass('hide');
        $(hiddenMessageContainerId).removeClass('hide').addClass('hide');

        $('.notification-container').show(0);
        $('.notification-container').attr('aria-live', 'polite');
        $('#ViewEnhancedProduct').focus();
    }

    function buildLivePersonEngagementAttributes(livePersonContext) {
        if (livePersonContext !== null) {
            livePersonTag.buildAttributes(livePersonContext);
        }
    }

    function getNotificationFromLocalStore() {
        return JSON.parse(sessionStorage.getItem("notification"));
    }

    function buildNotificationMessage(msg, contextData) {
        var contextDataReplacer = createContextDataReplacer(contextData);
        msg = replaceAll(msg, contextDataReplacer);
        return msg;
    }

    function createContextDataReplacer(contextData) {
        var contextDataReplacer = [
            {
                search: '##PRICE##',
                replacement: contextData.NotificationContext.Price
            },
            {
                search: '##FIRSTNAME##',
                replacement: toTitleCase(contextData.NotificationContext.FirstName)
            }, {
                search: '##EMAIL##',
                replacement: contextData.NotificationContext.Email
            },
            {
                search: '##UNDERWRITER##',
                replacement: contextData.NotificationContext.Underwriter
            },
            {
                search: '##DEPOSIT##',
                replacement: contextData.NotificationContext.Deposit
            }
        ];
        return contextDataReplacer;
    }

    function toTitleCase(str) {
        try {
            var splitName = str.split('-');
            var titleCaseName = "";
            for (i = 0; i < splitName.length; i++) {
                titleCaseName =
                    titleCaseName + splitName[i].charAt(0).toUpperCase() + splitName[i].substr(1).toLowerCase() + "-";
            }
            return titleCaseName.substring(0, titleCaseName.length - 1);
        }
        catch (ex) {
            return str;
        }
    }

    function replaceAll(input, contextDataReplacer) {
        for (var i = 0; i < contextDataReplacer.length; i++) {
            input = input.replace(new RegExp(contextDataReplacer[i].search, 'g'), contextDataReplacer[i].replacement);
        }
        return input;
    }

    function getContextData() {
        var context = $('#houstoncontextdata').attr('data-context');
        if (context) {
            return JSON.parse(context);
        }

        return {};
    }

    function handleLoad(event) {
        $(event.target).off(event.type, handleLoad);
        setState('ready');
    }

    function setState(newState) {

        var houstonButton = $('#houstonButton');
        switch (newState) {
            case 'loading':
                houstonButtonState = 'loading';
                houstonButton.removeClass('ready va typing').addClass('loading');
                break;

            case 'typing':
                houstonButtonState = 'typing';
                houstonButton.removeClass('loading ready va').addClass('typing');
                break;

            case 'ready':
                houstonButtonState = 'ready';
                houstonButton.removeClass('loading typing va').addClass('ready');
                houstonButton.attr('aria-label', 'Open Virtual Assistant');
                break;

            case 'va':
                houstonButtonState = 'va';
                houstonButton.removeClass('loading ready typing').addClass('va');
                houstonButton.attr('aria-label', 'Close Virtual Assistant');
                break;

            default:
                break;
        }

    }

    function clickHandler() {

        switch (houstonButtonState) {
            case 'loading':
                break;

            case 'typing':
                break;

            case 'ready':
                handleReadyStateClick();
                break;

            case 'va':
                setState('ready');
                $('#nina-block').removeClass('is-visible');
                $('.houston-container').removeClass('ground-control');
                break;

            default:
                break;
        }
    }

    function handleReadyStateClick() {
        var conversationLog = $('#nw_ConversationText');
        var lastAgentMessage = $('#nw_ConversationText .nw_AgentSays:last');
        var userMessages = $('#nw_ConversationText .nw_UserSays');
        var messagesToAnnounce;

        setState('va');
        $('#nina-block').addClass('is-visible');
        $('.houston-container').addClass('ground-control');
        $('#nw_UserInputField').focus();

        if (userMessages.length === 0) {
            messagesToAnnounce = conversationLog.children();
        } else {
            var previousMessagesAnnouncement = $('#nw_previousMessagesAnnouncement').length ?
                $('#nw_previousMessagesAnnouncement') :
                $('<span id=\"nw_previousMessagesAnnouncement\" class=\"visually-hidden\">Last message: </span>');
            previousMessagesAnnouncement.detach();
            messagesToAnnounce = lastAgentMessage.prev().nextAll();
            previousMessagesAnnouncement.prependTo(messagesToAnnounce);
        }

        if (messagesToAnnounce.length) {
            messagesToAnnounce.detach();
            setTimeout(
                function () {
                    messagesToAnnounce.appendTo(conversationLog);
                },
                200);
        }
    }

    function whichAnimationEvent() {
        var typeAnimations;
        var element = document.createElement("fakeelement");

        var animations = {
            "animation": "animationend",
            "OAnimation": "oAnimationEnd",
            "MozAnimation": "animationend",
            "WebkitAnimation": "webkitAnimationEnd"
        };

        for (typeAnimations in animations) {
            if (element.style[typeAnimations] !== undefined) {
                return animations[typeAnimations];
            }
        }
    }

    function setFocusToElement() {
        var notificationButtonIsFocused = $('.notification-button').is(':focus');

        if (autoCleared && notificationButtonIsFocused) {
            resetFocus();
        }
    }

    function resetFocus() {
        bgl.common.focusHolder.setFocusToElement();
        bgl.common.focusHolder.removeFocused();
    }

    function cookiePolicyCheck(contextItems) {
        if (contextItems.InvocationPoint !== "SSC_TE" && document.cookie.indexOf('bgl-cookie-notified') === -1) {
            setCookieForCookiePolicy();
        }
    }

    function setCookieForCookiePolicy() {
        var domain = getRootDomain();
        var expirationDate = new Date();
        expirationDate.setDate(expirationDate.getDate() + 180);
        expirationDate = expirationDate.toUTCString();

        document.cookie = 'bgl-cookie-notified=1;expires=' + expirationDate + ';domain=' + domain + ';SameSite=strict;';
    }

    function getRootDomain() {
        var acceptableTlds = [".com", ".co.uk", ".io", ".net"];
        var tld = "";
        var hostname = window.location.hostname.toString();

        acceptableTlds.forEach(function (element) {
            if (hostname.indexOf(element) > -1) {
                tld = element;
                hostname = hostname.replace(tld, "");
            }
        });

        var parts = hostname.split(".");

        return parts[parts.length - 1] + tld;
    }

    function refreshContext(callback) {
        var url = applicationPath + "HoustonComponent/GetContext";

        $.ajax({
            url: url,
            type: "GET",
            data: configuration,
            success: function (data) {
                $("#houstoncontextdata").attr('data-context', JSON.stringify(data));

                if (typeof callback === "function") {
                    callback();
                }
            }
        });

    }

    function deleteBasket(redirectUrl) {
        var token = $('input[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: applicationPath + "HoustonComponent/DeleteBasket",
            type: 'POST',
            data: {
                __RequestVerificationToken: token
            },
            success: function (data) {
                if (data.isSuccess) {
                    clearSession();

                    if (redirectUrl) {
                        sessionStorage.removeItem("MTAStartPage");
                        window.location = redirectUrl;
                    } else {
                        $("#houstonConfirmation button").removeAttr("tabindex");
                        $("#houstonConfirmation").addClass("hide");
                        bgl.common.pubsub.emit("CancelMTAComplete");
                    }
                } else {
                    window.location = applicationPath + 'Error';
                }
            },
            error: function () {
                window.location = applicationPath + 'Error';
            }
        });
    }

    function clearSession() {
        sessionStorage.removeItem("cancellationReasons");
        sessionStorage.removeItem("cancellationReasonsOther");
        sessionStorage.removeItem("cancellationWhen");
        sessionStorage.removeItem("cancellationDate");
        sessionStorage.removeItem("cancellationBackWhen");
        sessionStorage.removeItem("cancellationBackDatePicker");
    }
 
    return {
        showNotification: showNotification,
        showFormattedNotification: showFormattedNotification,
        showNotificationFromLocalStore: showNotificationFromLocalStore,
        showEnhancedNotification: showEnhancedNotification,
        setInvocationForVap: setInvocationForVap,
        buildLivePersonEngagementAttributes: buildLivePersonEngagementAttributes,
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.houston;
}
