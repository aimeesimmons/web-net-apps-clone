﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.coverbubblesrenewal = (function () {

    function initComponent() {

        $(document).ready(function () {
            if ($("[data-renewals-included] button").length < 4) {
                $("[data-see-more]").toggleClass("hide")
            }
            $("[data-renewals-included] button").slice(4).toggleClass("hide")
            $("[data-see-more]").click(function () {
                $("[data-renewals-included] button").slice(4).toggleClass("hide");
                $("[data-renewals-included] button")[4].focus();
                $(this).text(function (i, text) {
                    return text === "See more" ? "Show less" : "See more";
                })
            });
        });
    }

    return {
        init: initComponent
    };
})();