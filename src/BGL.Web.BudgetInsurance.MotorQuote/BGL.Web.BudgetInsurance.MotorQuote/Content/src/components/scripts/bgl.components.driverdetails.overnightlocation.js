﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var overnightParking = "OvernightParkingCode";

bgl.components.driverdetails.overnightparking = (function () {
    var doneButton,
        radioButtons,
        configuration,
        containerId,
        BACK_URL_STACK_KEY = "backUrlStack",
        form;

    function initComponent(componentConfiguration, containerRef) {
        configuration = componentConfiguration
        containerId = containerRef;

        form = $("[data-component-id='" + configuration.Id + "'] form");
        doneButton = $('#vehicle-overnight_done-button');
        radioButtons = $("input[type='radio']");
        
        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        initBackButtonLink();
        subscribeToDoneEvents();
        subscribeToRadioButtonEvents();

        bgl.common.datastore.extendCollectedData(form, { OvernightParkingCode: $('input[name=OvernightParkingCode]:checked').val() });
    }

    function initBackButtonLink() {
        $('.back-button-link')
            .off('click')
            .on('click',
                function (e) {
                    e.preventDefault();
                    $(this).hide();
                    var url = getBackButtonUrl();
                    loadView(url);
                });
    }

    function loadView(url) {
        var data = bgl.common.datastore.getData("Address");

        data.ComponentConfiguration = configuration;


        bgl.common.loader.load(data,
            url,
            containerId,
            function () {
                bgl.components.editaddress.init(configuration, false);   
            });
    }

    function pushBackButtonUrl(url) {
        var stack = JSON.parse(sessionStorage.getItem(BACK_URL_STACK_KEY)) || [];

        stack.push(url);

        sessionStorage.setItem(BACK_URL_STACK_KEY, JSON.stringify(stack));
    }

    function getBackButtonUrl() {
        var stack = JSON.parse(sessionStorage.getItem(BACK_URL_STACK_KEY));
        var url = stack.pop();

        sessionStorage.setItem(BACK_URL_STACK_KEY, JSON.stringify(stack));

        return url;
    }


    function subscribeToRadioButtonEvents() {
        var hasValue = radioButtons.is(':checked');

        if (hasValue) {
            doneButton.removeClass("hide");
        } else {
            doneButton.addClass("hide");
        }

        $.each(radioButtons,
            function(key, item) {
                var button = $(item);

                button.on("change",
                    function (event) {
                        event.preventDefault();
                        if (!hasValue) {
                            form.submit();
                        }
                    });
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToDoneEvents() {

        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();
                if (bgl.common.validator.isFormValid($(this))) {
                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(event.target)) === true);
                    bgl.common.validator.submitForm($(this), configuration);
                }
            });
    }

    function onSuccess(configuration, response) {

        bgl.common.datastore.extendCollectedData(form, { isFormDirty: response.hasChanges, HasChanges: response.hasChanges});
        pushBackButtonUrl(response.backUrl);
        bgl.common.loader.load(configuration, response.nextUrl, containerId, function (e) {
            bgl.components.driverdetails.annualmileage.init(configuration, containerId);
        });
    }

    function onError() {
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
        bgl.components.vehicledetails.overnightparking.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();