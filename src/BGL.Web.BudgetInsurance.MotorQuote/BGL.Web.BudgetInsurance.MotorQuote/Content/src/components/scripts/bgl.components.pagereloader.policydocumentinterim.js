﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.pagereloader = bgl.components.pagereloader || {};

bgl.components.pagereloader.policydocumentinterim = (function () {

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        var form = $("#downloadDocumentForm");
        var userAgent = navigator.userAgent.toLowerCase();
        var isAndroid = userAgent.indexOf("android") > -1;

        if (isAndroid) {
            $('#androidMessage').show();
        }
        else {
            showSpinner();
            var timeout = componentConfiguration.TimeoutMilliseconds;
            var errorUrl = componentConfiguration.ErrorRedirectUrl;

            form.submit(function() {
                setTimeout(
                    function() {
                        bgl.common.utilities.redirectToUrl(errorUrl);
                    },
                    timeout);
            });
        }

        form.submit();
    }

    function showSpinner() {
        var spinnerHtml = bgl.common.spinner.getSpinnerBody("Working on it",
            "We won't be a moment");
        $('#spinner').html(spinnerHtml);
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.pagereloader;
}