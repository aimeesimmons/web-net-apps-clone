﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.ncdyears = (function () {
    var configuration,
        form;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form, false);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {

        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', continueHandler)
                .on('click', function (event) {
                    $(this).focusout();
                    form.change();
                    continueHandler(event);
                });
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
            continueButton.off("click").on("click", continueHandler);
        }

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

        var ncdYearsSessionData = JSON.parse(sessionStorage.getItem("NcdYears"));
        var isVan = ncdYearsSessionData["IsVan"];

        if (isVan === "True") {
            $('#noclaimsdiscountyearsbackbtn').attr('href', configuration.CarriageOfDangerousGoodsUrl);
        }
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;
        if (code === 13 || code === 32) {
            $(this).click();
        }
    }

    function continueHandler(event, element) {
        event.preventDefault();
        var submitButton = form.find(':submit');
        submitButton.attr('disabled', 'disabled');

        if (bgl.common.validator.isFormValid(form)) {
            $("#HasChanges").val(bgl.common.datastore.isDataChanged(form) === true);
            bgl.common.validator.submitForm(form, configuration);
        } else {
            submitButton.removeAttr('disabled');
        }
    }

    function onSuccess(configuration, data) {
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

        if (data.isNeedToDeleteNcdSource === true && configuration.DataStoreKeyForNcdSourcePage) {
            bgl.common.datastore.deleteFields(configuration.DataStoreKeyForNcdSourcePage, ["NcdSourceCode"]);
        }

        bgl.common.utilities.redirectToUrl(data.nextPageUrl);
    }

    function onError() {
        bgl.components.coverdetails.ncdyears.init(configuration);
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();