﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.yearsbusinessestablished = (function () {
    var configuration,
        container,
        form;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration, containerElementId);
            });
    }

    function initComponent(componentConfiguration, containerId) {
        configuration = componentConfiguration;
        container = containerId;
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.off("click").on("click", continueHandler);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    setDataHasChangedFlagForQuestionSet();

                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

        if (!isMultiStepEdit()) {
            form.find('#yearsBusinessEstablishedBackBtn').off('click').one('click',
                function (event) {
                    event.preventDefault();

                    var vehicleUseComponentActionUrl = $(this).attr('href');
                    var model = bgl.common.datastore.getData(form);
                    model.ComponentConfiguration = configuration;

                    bgl.components.vehicleusage.load(model, vehicleUseComponentActionUrl, container);
                });
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        form.submit();
    }

    function isMultiStepEdit() {
        var coverDetailsCommon = bgl.components.coverdetails.common;
        return configuration.JourneyType === coverDetailsCommon.journeyTypes.multiStepEdit;
    }

    function onSuccess(result) {
        if (isMultiStepEdit()) {
            location.href = configuration.NextStepUrl;
        } else {
            var dangerousGoodsComponentActionUrl = $('#continueButton').data('action-url');

            bgl.components.coverdetails.carriageofdangerousgoods.load(configuration, dangerousGoodsComponentActionUrl, container);
        }
    }

    function onError() {
        initComponent(configuration);
    }

    function setDataHasChangedFlagForQuestionSet() {
        var vehicleUseSessionData = JSON.parse(sessionStorage.getItem("CoverDetailsVehicleUse"));
        var dataHasChanged = vehicleUseSessionData["DataHasChanged"] === "True";

        if (dataHasChanged || bgl.common.datastore.isDataChanged(form)) {
            vehicleUseSessionData.DataHasChanged = "True";
            sessionStorage.setItem("CoverDetailsVehicleUse", JSON.stringify(vehicleUseSessionData));
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();