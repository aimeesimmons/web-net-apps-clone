﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequotepaymentswitchformwithsmallannualbutton = (function () {
    var configuration, paymentType, form, monthlyAnnualRadioButtons;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#payment-type-switch-form');
        monthlyAnnualRadioButtons = $('input[type=radio].payment-option__radio');

        preventFormSubmission(form);
        setPaymentTypeWhenPaymentTypeChanges(monthlyAnnualRadioButtons);
        postPaymentTypeWhenPaymentTypeChanges(monthlyAnnualRadioButtons);
        changeToAnnualWhenAnnualButtonClicked();
        changeToMonthlyWhenMonthlyButtonClicked();
        setUpPubSubSubscription();
    }

    function preventFormSubmission(form) {
        form.on("submit", function (event) {
            event.preventDefault();
        });
    }

    function setPaymentTypeWhenPaymentTypeChanges(radioButtons) {
        radioButtons.on('change', function() {
                setPaymentOption($(this));
            });
    }

    function setPaymentOption(selected) {
        paymentType = selected.data('paySchedule');
    }

    function postPaymentTypeWhenPaymentTypeChanges(radioButtons) {
        radioButtons.on('change', function () {
            bgl.common.validator.submitForm(form, configuration);
        });
    }

    function changeToAnnualWhenAnnualButtonClicked() {
        $("#switch-to-annual-button").on("click",
            function () {
                $("#paymentAnnualRadio").click();
            });
    }

    function changeToMonthlyWhenMonthlyButtonClicked() {
        $("#switch-to-monthly-button").on("click",
            function () {
                $("#paymentMonthlyRadio").click();
            });
    }

    function setUpPubSubSubscription()
    {
        bgl.common.pubsub.on('switchPaymentType', toggleMonthlyAnnual);
    }

    function toggleMonthlyAnnual(isMonthly) {
        if (isMonthly) {
            $("#payment-options-container").addClass("hide");
            $("#switch-to-annual-button").removeClass("hide");
            $("#switch-to-monthly-cta").addClass("hide");
            $("#switch-to-annual-cta").removeClass("hide");
        } else  {
            $("#payment-options-container").removeClass("hide");
            $("#switch-to-annual-button").addClass("hide");
            $("#switch-to-monthly-cta").removeClass("hide");
            $("#switch-to-annual-cta").addClass("hide");
        }
    }

    function onSuccess() {
        publishSwitchPaymentTypeEvent();
        showHoustonNotification();
    }

    function publishSwitchPaymentTypeEvent() {
        var isMonthly = paymentType === "monthly";
        bgl.common.pubsub.emit("switchPaymentType", isMonthly);
    }

    function showHoustonNotification() {
        var notification = $("#" + paymentType + "-welcomeQuote-notification-message").val();

        bgl.common.pubsub.emit("showNotification", notification);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequotepaymentswitchformwithsmallannualbutton;
}