﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymentsummary = (function () {
    var componentConfig;
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function() {
                initComponent();
            });
    }

    function initComponent(componentConfiguration) {
        bgl.common.validator.enableMaxLength();

        componentConfig = componentConfiguration;

        bgl.common.validator.init($("#PaymentSummaryForm"), {
            onfocusout: function (element) {
                $(element).valid();
                if ($('.form-row__validation-text').hasClass('field-validation-error')) {
                    $('#Email').focus();
                }
            },
            hasSubmit: false
        });

        bgl.components.takepayment.documentloader.init(componentConfig);

        $("#PaymentSummaryForm").submit(
          function(e) {
            e.preventDefault();

            if (!$(this).valid()) {
                return;
            }
            if (typeof bgl.common.pubsub !== 'undefined') {
              bgl.common.pubsub.emit('fakeFormSubmit', $(this).attr('id') || $(this).attr('action') || 'unknown');
            }

            $.ajax({
              url: $(this).attr("action"),
              type: $(this).attr("method"),
              data: $(this).serialize(),
              success: function (data) {
                if (data.isSuccess) {
                  bgl.common.utilities.redirectToUrl(data.redirectUrl);
                } else {
                  bgl.common.utilities.redirectToUrl(data.errorRedirectUrl);
                }
              }
            });
          });

        $(".payment-switch-link").on("click",
            function () {
                var paymentSwitchLinkId = $(this).attr("id");
                bgl.common.focusHolder.setFocusedElement($(this).data("focusid"));
                var dataToPost = {
                    __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
                    paymentSwitchId: $(this).attr("data-payment-switch-id")
                };

                $.ajax({
                    url: componentConfig.ApplicationPath + "TakePaymentComponent/PaymentSwitch",
                    type: "POST",
                    cache: false,
                    dataType: "json",
                    data: dataToPost,
                    success: function (result) {
                        if (result.isSuccess === true) {
                            bgl.common.pubsub.emit("switchPaymentType", paymentSwitchLinkId === "monthly-switch-link");
                            if (paymentSwitchLinkId === "monthly-switch-link") {
                                $("#monthly-section").removeClass("hide");
                                $("#annual-section").addClass("hide");
                                $("#IsMonthlyPayment").val("true");
                                bgl.common.pubsub.emit("showNotification", $("#monthly-notification-message").val());
                            } else if (paymentSwitchLinkId === "annual-switch-link") {
                                $("#monthly-section").addClass("hide");
                                $("#annual-section").removeClass("hide");
                                $("#IsMonthlyPayment").val("false");
                                bgl.common.pubsub.emit("showNotification", $("#annual-notification-message").val());
                            }
                        } else {
                            window.location.href = result.errorRedirectUrl;
                            return;
                        }
                    }
                });
            });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentsummary;
}
