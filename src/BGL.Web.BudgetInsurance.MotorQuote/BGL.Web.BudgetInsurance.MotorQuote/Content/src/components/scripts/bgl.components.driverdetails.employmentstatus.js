﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.driverdetails.employmentstatus = (function () {
    var configuration;
    var form;
    var hasPreselectedValues;

    function init(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);
        bgl.common.validator.initAutocompleteSearchInputs(form);

        bgl.common.autocompletesearch.init(3);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

        if (hasPreselectedValues) {
            showContinueButton();
        }

        if ($("#PersonOccupationStep").val() === "PartTime") {

            var person = bgl.common.datastore.getData(form);
            var selectedEmploymentStatus = person.EmploymentStatusId;

            var hasOccupation = $.grep(configuration.StatusCodesToAskOccupation,
                function (value) {
                    return selectedEmploymentStatus.toLowerCase() === value.toLowerCase();
                }).length !== 0;

            if (hasOccupation) {
                $("#" + configuration.Id + "-back-button").attr("href", configuration.PreviousBusinessTypePageUrl);
            }
        }

        if (hasPreselectedValues === false) {
            form.find("input[type=radio]")
                .off('click')
                .on('click', continueHandler);
        }

        if (hasPreselectedValues === true) {
            form.find("input[type=radio]")
                .off('change.occupation')
                .on('change.occupation', function () {
                    var stepType = $("#PersonOccupationStep").val() === "PartTime" ? 1 : 0;
                    resetOccupationInfo(stepType, form);
                });
        }

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                    var elementsArray = $(this).serializeArray();
                    var formData = {};

                    $.map(elementsArray, function (item, i) {
                        formData[item['name']] = item['value'];
                    });

                    var currentData = bgl.common.datastore.getData($(this));

                    currentData = $.extend(currentData, formData);

                    var dataForValidate = $.param(currentData) + '&' + $.param({ ComponentConfiguration: configuration });

                    bgl.common.validator.submitForm($(this), configuration, dataForValidate);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function continueHandler(event) {
        event.preventDefault();

        var controlType = $(event.target).attr('type');
        if (controlType === 'checkbox' || controlType === 'radio') {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function onSuccess(configuration, data) {
        if (data.needDeleteOccupation === true) {
            resetOccupationInfo(data.occupationStep, form);
        }

        if (data.isDataProvided) {
            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });
        }

        if (data.nextDtsPageUrl) {
            location.href = data.nextDtsPageUrl;
        }
    }

    function resetOccupationInfo(stepType, form) {
        switch (stepType) {
        case 1:
            bgl.common.datastore.deleteFields(form,
                [
                    "PartTimeOccupationId", "PartTimeOccupation",
                    "PartTimeBusinessCategoryId", "PartTimeBusinessCategory"
                ]);
            break;
        default:
            bgl.common.datastore.deleteFields(form,
                [
                    "OccupationId", "Occupation",
                    "BusinessCategoryId", "BusinessCategory"
                ]);
            break;
        }
    }

    function onError() {
        bgl.components.driverdetails.employmentstatus.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: init,
        onSuccess: onSuccess,
        onError: onError
    };
})();
