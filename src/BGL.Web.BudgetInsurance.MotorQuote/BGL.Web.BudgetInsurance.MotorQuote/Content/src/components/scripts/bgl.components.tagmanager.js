﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

var digitalData = window.digitalData || {};
digitalData.page_data = digitalData.page_data || {};
digitalData.user_data = digitalData.user_data || {};
digitalData.journey_data = digitalData.journey_data || {};
digitalData.application_data = digitalData.application_data || {};
digitalData.application_data.insurance_data = digitalData.application_data.insurance_data || {};
digitalData.my_account = digitalData.my_account || {};
digitalData.my_account.policy_data = digitalData.my_account.policy_data || {};
digitalData.my_account.policy_data.policy_change = digitalData.my_account.policy_data.policy_change || {};
digitalData.my_account.policy_data.policy_cancellation_questions = digitalData.my_account.policy_data.policy_cancellation_questions || {};

bgl.components.tagmanager = (function () {
  function initComponent() {
    setPageProperties();
    setReferrerProperties();
    setTimestampProperty();
    setPageCategories();
    setJavascriptErrorProperty();
    setUpdatedUserDetails();
    setUpdatedVehicleDetails();
    setUpdatedPaymentDetails();
    setCancellationDetails();
    subscribeToStatusesAndTimingEvents();
    checkStorageAndUpdateApplicationData();
    subscribeToErrorData();
    subscribeToItemsClickedEvent();
    setClickedItemsInDigitalData();
    logClicksForLinksAndButtons();
    subscribeToFormSubmitEvents();
  }

  function subscribeToFormSubmitEvents() {
    var formLoadTime = new Date();
    bgl.common.pubsub.on('formSubmit', function (data) {
      logFormSubmission(data, formLoadTime);
    });
    bgl.common.pubsub.on('fakeFormSubmit', function (data) {
      logFormSubmission(data, formLoadTime);
    });
  }

  function logFormSubmission(data, formLoadTime) {
    var formData = {
      detail: {
        form_title: data,
        form_section: $('h1:visible:first').text().replace(/\s\s+/g, ' ').trim(),
        form_page: digitalData.page_data.page_title,
        page_url: digitalData.page_data.page_url,
        time_to_complete: calculateTimeTaken(formLoadTime, new Date())
      }
    };
    document.dispatchEvent(new CustomEvent('form_completed', formData));
  }

  function setPageProperties() {
    digitalData.page_data.page_url = window.location.href;
    digitalData.page_data.page_title = document.title;
    digitalData.page_data.page_path = window.location.pathname;
  }

  function setReferrerProperties() {
    digitalData.page_data.page_previous_url = document.referrer;
    if (document.referrer.length > 0) {
      var newUrl = new URL(document.referrer);
      digitalData.page_data.page_previous_path = newUrl.pathname;
    }
  }

  function setTimestampProperty() {
    digitalData.page_data.timestamp = (new Date()).toString();
  }

  function setPageCategories() {
    var pathArray = window.location.pathname.split('/');
    pathArray.pop();
    digitalData.page_data.page_categories = pathArray;
  }

  function setJavascriptErrorProperty() {
    digitalData.page_data.javascript_errors = [];
    window.onerror = function(error, url, line) {
      digitalData.page_data.javascript_errors.push('ERR:' + error + ' URL:' + url + ' L:' + line);
    };
  }

  function logClicksForLinksAndButtons() {
    $('i[data-type="policy-overview-slide-panel-navigation"]').click(function (event) {
      var clickedLink = $(event.target).parent().attr('id');
      bgl.common.pubsub.emit('SlidePanelClicked', clickedLink);
    });

    $('a[data-type="policy-navigation"]').click(function (event) {
      var clickedLink = $(event.target).text().trim();
      if (clickedLink === '') {
        clickedLink = $(event.target).parent().text().trim();
      }
      bgl.common.pubsub.emit('NavigationBarClicked', clickedLink);
    });

    $('a[data-testid="footer__contact-us"], a[data-testid="footer__privacy-policy"]').click(function (event) {
      var clickedLink = $(event.target).text().trim();
      bgl.common.pubsub.emit('FooterClicked', clickedLink);
    });

    $('#ChangePaymentMethod, #PaymentSuccessful_ChangePaymentDate').click(function (event) {
      var clickedSlidePanel = $(event.target).attr('id');
      bgl.common.pubsub.emit('SlidePanelClicked', clickedSlidePanel);
    });

    $('i[data-type="slide-panel-navigation"]').on('click',
      function (event) {
        var clickedSlidePanel = $(event.target).parent().attr('data-edit-action-key');
        if (clickedSlidePanel === undefined) {
          clickedSlidePanel = $(event.target).parent().attr('data-cover-key');
          if (clickedSlidePanel === undefined) {
            clickedSlidePanel = $(event.target).parent().attr('data-action');
          }
        }
        bgl.common.pubsub.emit('SlidePanelClicked', clickedSlidePanel);
      });
  }

  function setUpdatedUserDetails() {
    digitalData.user_data.detail_updated = JSON.parse(sessionStorage.getItem('ChangedUserData'));
  }

  function setUpdatedVehicleDetails() {
    digitalData.my_account.policy_data.policy_change = digitalData.my_account.policy_data.policy_change || {};
    digitalData.my_account.policy_data.policy_change.vehicle_details = digitalData.my_account.policy_data.policy_change.vehicle_details || {};
    digitalData.application_data.insurance_data.quote_updated = JSON.parse(sessionStorage.getItem('ChangedVehicleData'));
    getChangedPolicyData();

    if (jQuery.isEmptyObject(digitalData.my_account.policy_data.policy_change.vehicle_details) !== true) {
      digitalData.my_account.policy_data.policy_change.vehicle_changed = true;
    }
  }

  function setUpdatedPaymentDetails() {
    var paymentChange = sessionStorage.getItem('PaymentUpdated');
    if (paymentChange === null) {
      digitalData.my_account.policy_data.payment_method_changes = false;
    } else {
      digitalData.my_account.policy_data.payment_method_changes = true;
      digitalData.my_account.policy_data.payment_method_selected = paymentChange;
    }
  }

  function setCancellationDetails() {
    var cancellationReason = sessionStorage.getItem('TagManager.CancellationReason');
    var cancellationDate = sessionStorage.getItem('TagManager.CancellationDate');
    if (cancellationReason === null && cancellationDate === null) {
      digitalData.my_account.policy_data.policy_cancellation = false;
    } else {
      digitalData.my_account.policy_data.policy_cancellation = true;
      var why = {
        cancellation_question: 'Why',
        cancellation_answer: cancellationReason
      };
      var when = {
        cancellation_question: 'When',
        cancellation_answer: cancellationDate
      };
      digitalData.my_account.policy_data.policy_cancellation_questions = [why, when];
    }
  }

  function subscribeToItemsClickedEvent() {
    bgl.common.pubsub.on('NavigationBarClicked', function (clickedItem) {
      storeClicksInNavigationBarForTagManager(clickedItem);
    });

    bgl.common.pubsub.on('FooterClicked', function (clickedItem) {
      storeClicksInFooterForTagManager(clickedItem);
    });

    bgl.common.pubsub.on('SlidePanelClicked', function (clickedItem) {
      storeClicksOnSlidePanelForTagManager(clickedItem);
    });
  }

  function storeClicksInNavigationBarForTagManager(clickedItem) {
    var data = getPreviouslyStoredClickedItemsInHeader();
    if (clickedItem) {
      data.push(['HeaderItemClicked', clickedItem]);
      setPreviouslyStoredClickedItemsInHeader(data);
    }
  }

  function storeClicksInFooterForTagManager(clickedItem) {
    var data = getPreviouslyStoredClickedItemsInFooter();
    if (clickedItem) {
      data.push(['FooterItemClicked', clickedItem]);
      setPreviouslyStoredClickedItemsInFooter(data);
    }
  }

  function storeClicksOnSlidePanelForTagManager(clickedItem) {
    var data = getPreviouslyStoredClickedSlidePanels();
    if (clickedItem) {
      data.push(['SlidePanelClicked', clickedItem]);
      setPreviouslyStoredClickedSlidePanels(data);
    }
  }

  function setClickedItemsInDigitalData() {
    if (sessionStorage.getItem('NavigationBarClicked') !== null) {
      digitalData.journey_data.megamenu_clicked = true;
      digitalData.journey_data.megamenu_item_clicked =
        JSON.parse(sessionStorage.getItem('NavigationBarClicked'));
    }

    if (sessionStorage.getItem('FooterClicked') !== null) {
      digitalData.journey_data.footer_clicked = true;
      digitalData.journey_data.footer_item_clicked =
        JSON.parse(sessionStorage.getItem('FooterClicked'));
    }

    if (sessionStorage.getItem('SlidePanelClicked') !== null) {
      digitalData.journey_data.page_body_item_clicked =
        JSON.parse(sessionStorage.getItem('SlidePanelClicked'));
    }

    if (sessionStorage.getItem('PaperlessStatus') !== null) {
      digitalData.my_account.policy_data.paperless_status = sessionStorage.getItem('PaperlessStatus');
    }
  }

  function getPreviouslyStoredClickedItemsInHeader() {
    var data = [];

    if (sessionStorage.getItem('NavigationBarClicked') !== null) {
      data = JSON.parse(sessionStorage.getItem('NavigationBarClicked'));
    }

    return data;
  }

  function getPreviouslyStoredClickedItemsInFooter() {
    var data = [];

    if (sessionStorage.getItem('FooterClicked') !== null) {
      data = JSON.parse(sessionStorage.getItem('FooterClicked'));
    }

    return data;
  }

  function getPreviouslyStoredClickedSlidePanels() {
    var data = [];

    if (sessionStorage.getItem('SlidePanelClicked') !== null) {
      data = JSON.parse(sessionStorage.getItem('SlidePanelClicked'));
    }

    return data;
  }

  function setPreviouslyStoredClickedItemsInHeader(data) {
    sessionStorage.setItem('NavigationBarClicked', JSON.stringify(data));
  }

  function setPreviouslyStoredClickedItemsInFooter(data) {
    sessionStorage.setItem('FooterClicked', JSON.stringify(data));
  }

  function setPreviouslyStoredClickedSlidePanels(data) {
    sessionStorage.setItem('SlidePanelClicked', JSON.stringify(data));
  }

  function getChangedPolicyData() {
    var changedVehicleData = JSON.parse(sessionStorage.getItem('ChangedVehicleData'));

    if (changedVehicleData !== null) {
      for (var i = 0; i < changedVehicleData.length; i++) {
        setChangedVehicleDataItems(changedVehicleData[i]);
      }
    }
  }

  function setChangedVehicleDataItems(changedItemArray) {
    if (changedItemArray[0] === 'Manufacturer') {
      digitalData.my_account.policy_data.policy_change.vehicle_details.vehicle_make = changedItemArray[1];
    }

    if (changedItemArray[0] === 'Model') {
      digitalData.my_account.policy_data.policy_change.vehicle_details.vehicle_model = changedItemArray[1];
    }

    if (changedItemArray[0] === 'Age') {
      digitalData.my_account.policy_data.policy_change.vehicle_details.vehicle_age = changedItemArray[1];
    }

    if (changedItemArray[0] === 'VehicleOvernightParking') {
      digitalData.my_account.policy_data.policy_change.vehicle_details.vehicle_overnight_parking = changedItemArray[1];
    }

    if (changedItemArray[0] === 'CustomerVehicleValue') {
      digitalData.my_account.policy_data.policy_change.vehicle_details.vehicle_value = changedItemArray[1];
    }

    if (changedItemArray[0] === 'Age') {
      digitalData.my_account.policy_data.policy_change.vehicle_details.vehicle_age = changedItemArray[1];
    }
  }

  function checkStorageAndUpdateApplicationData() {
    checkForSessionAndClearStorageItems();
    checkForStartedTimeAndUpdateDigitalData();
    checkForQuoteSavedAndUpdateDigitalData();
    checkForQuoteRetrievedAndUpdateDigitalData();
    checkForPaymentSuccessfulAndUpdateDigitalData();
    checkForPaymentUnsuccessfulAndUpdateDigitalData();
    checkForApplicationOrderedTimeAndUpdateDigitalData();
    storeTimeTakenInDigitalData();
    checkForQuotedTimeAndUpdateDigitalData();
    checkForUnableToQuoteAndUpdateDigitalData();
    checkForOrderStartTimeAndUpdateDigitalData();
  }

  function checkForSessionAndClearStorageItems() {
    if (localStorage.getItem('Session') !== digitalData.user_data.user_id) {
      localStorage.setItem('ApplicationOrderedTime', '');
      localStorage.setItem('QuoteRetrieved', '');
      localStorage.setItem('ApplicationOrderStartTime', '');
      localStorage.setItem('ApplicationStartedTime', '');
      localStorage.setItem('QuoteSaved', '');
      localStorage.setItem('PaymentSuccessful', '');
      localStorage.setItem('PaymentUnsuccessful', '');
      localStorage.setItem('ApplicationQuotedTime', '');
      localStorage.setItem('UnableToQuote', '');

      localStorage.setItem('Session', digitalData.user_data.user_id);
    }
  }

  function subscribeToStatusesAndTimingEvents() {
    bgl.common.pubsub.on('ApplicationStarted', function (getAppStartTime) {
      storeApplicationStartedTIme(getAppStartTime);
      checkForStartedTimeAndUpdateDigitalData();
    });

    bgl.common.pubsub.on('ApplicationQuoted', function (getAppQuotedTime) {
      storeApplicationQuotedTimeAndOrderStartTime(getAppQuotedTime);
      checkForQuotedTimeAndUpdateDigitalData();
    });

    bgl.common.pubsub.on('UnableToQuote', function () {
      localStorage.setItem('UnableToQuote', true);
      checkForUnableToQuoteAndUpdateDigitalData();
    });

    bgl.common.pubsub.on('SavedQuote', function () {
      localStorage.setItem('QuoteSaved', true);
      checkForQuoteSavedAndUpdateDigitalData();
    });

    bgl.common.pubsub.on('QuoteRetrieved', function () {
      localStorage.setItem('QuoteRetrieved', true);
      checkForQuoteRetrievedAndUpdateDigitalData();
    });

    bgl.common.pubsub.on('PaymentSuccessful', function (applicationOrderedTime) {
      localStorage.setItem('PaymentSuccessful', true);
      storeApplicationOrderedTime(applicationOrderedTime);
      checkForPaymentSuccessfulAndUpdateDigitalData();
    });

    bgl.common.pubsub.on('PaymentUnsuccessful', function () {
      localStorage.setItem('PaymentUnsuccessful', true);
      checkForPaymentUnsuccessfulAndUpdateDigitalData();
    });
  }

  function checkForStartedTimeAndUpdateDigitalData() {
    if (localStorage.getItem('ApplicationStartedTime') !== '') {
      digitalData.application_data.application_started = true;
    }
  }

  function checkForOrderStartTimeAndUpdateDigitalData() {
    if (localStorage.getItem('ApplicationOrderStartTime') !== '') {
      digitalData.application_data.application_quoted = true;
    }
  }

  function checkForQuoteSavedAndUpdateDigitalData() {
    if (localStorage.getItem('QuoteSaved') !== '') {
      digitalData.application_data.application_saved = true;
    }
  }

  function checkForQuoteRetrievedAndUpdateDigitalData() {
    if (localStorage.getItem('QuoteRetrieved') !== '') {
      digitalData.application_data.retrieved_quote = true;
    }
  }

  function checkForPaymentSuccessfulAndUpdateDigitalData() {
    if (localStorage.getItem('PaymentSuccessful') !== '') {
      digitalData.application_data.application_ordered = true;
      checkForApplicationOrderedTimeAndUpdateDigitalData();
    }
  }

  function checkForPaymentUnsuccessfulAndUpdateDigitalData() {
    if (localStorage.getItem('PaymentUnsuccessful') !== '') {
      digitalData.application_data.application_failed_payment = true;
    }
  }

  function checkForApplicationOrderedTimeAndUpdateDigitalData() {
    if (localStorage.getItem('ApplicationOrderedTime') !== '') {
      var orderStartTime = Date.parse(localStorage.getItem('ApplicationOrderStartTime'));
      var orderEndTime = Date.parse(localStorage.getItem('ApplicationOrderedTime'));

      digitalData.application_data.application_time_to_order = calculateTimeTaken(orderStartTime, orderEndTime);
    }
  }

  function storeTimeTakenInDigitalData() {
    if (localStorage.getItem('ApplicationQuotedTime') !== '' && localStorage.getItem('ApplicationStartedTime') !== '') {
      var quoteStartTime = Date.parse(localStorage.getItem('ApplicationStartedTime'));
      var quoteEndTime = Date.parse(localStorage.getItem('ApplicationQuotedTime'));
      digitalData.application_data.application_time_to_quote = calculateTimeTaken(quoteStartTime, quoteEndTime);
    }
  }

  function checkForQuotedTimeAndUpdateDigitalData() {
    if (localStorage.getItem('ApplicationQuotedTime') !== '') {
      digitalData.application_data.application_quoted = true;
    }
  }

  function checkForUnableToQuoteAndUpdateDigitalData() {
    if (localStorage.getItem('UnableToQuote') !== '' && digitalData.application_data.application_quoted === true) {
      digitalData.application_data.application_failed_order = true;
    } else if (localStorage.getItem('UnableToQuote') !== '' && localStorage.getItem('Session') === digitalData.user_data.user_id) {
      digitalData.application_data.quote_failed = true;
    }
  }

  function storeApplicationStartedTIme(getAppStartTime) {
    if (localStorage.getItem('ApplicationStartedTime') === '') {
      localStorage.setItem('ApplicationStartedTime', getAppStartTime);
    }
  }

  function storeApplicationQuotedTimeAndOrderStartTime(getAppQuotedTime) {
    if (localStorage.getItem('ApplicationQuotedTime') === '') {
      localStorage.setItem('ApplicationQuotedTime', getAppQuotedTime);
      localStorage.setItem('ApplicationOrderStartTime', getAppQuotedTime);
      storeTimeTakenInDigitalData();
    }
  }

  function storeApplicationOrderedTime(applicationOrderedTime) {
    if (localStorage.getItem('PaymentSuccessful') !== '') {
      localStorage.setItem('ApplicationOrderedTime', applicationOrderedTime);
    }
  }

  function calculateTimeTaken(startTime, endTime) {
    var duration = endTime - startTime;

    var seconds = Math.floor((duration / 1000) % 60);
    var minutes = Math.floor((duration / (1000 * 60)) % 60);
    var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? '0' + hours : hours;
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds : seconds;

    var timeTakenToQuote = (hours + ':' + minutes + ':' + seconds);

    return timeTakenToQuote;
  }
	
  function subscribeToErrorData() {
    bgl.common.pubsub.on('formValidationFailed', function (data) {
      document.dispatchEvent(new CustomEvent('form_field_error', {
        detail: {
          form_title: digitalData.page_data.page_path,
          form_section: data.form_section,
          form_page: digitalData.page_data.page_title,
          page_url: digitalData.page_data.page_url,
          error_details: [data.error_details]
        }
      }));
    });
  }

  return {
    init: initComponent
  };
})();

if (typeof module !== 'undefined') {
  module.exports = bgl.components.tagmanager;
}