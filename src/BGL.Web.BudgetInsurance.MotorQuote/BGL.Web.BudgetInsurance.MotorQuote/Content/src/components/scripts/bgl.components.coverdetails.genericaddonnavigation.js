﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.genericaddonnavigation = (function () {
    var configuration,
        selectedValue;

    var sessionStorageKeyName = "CurrentAddon";
    var sessionStorageSelectedAddonKeyName = "SelectedAddon";


    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        displayHeader();
        bgl.components.coverdetails.common.onAddonBackButtonClick();
        sessionStorage.setItem("NextAddon", $('#next-addon').val());

        var nextAddon = sessionStorage.getItem("NextAddon");

        var addonNonSelected = sessionStorage.getItem(("NonSelectedAddon").concat(configuration.SelectedAddon));

        var pageNavigatedBack = sessionStorage.getItem(sessionStorageKeyName.concat(configuration.ContinueUrl));

        if (pageNavigatedBack === configuration.ContinueUrl) {
            $('#continue-button').show();
        }

        if (addonNonSelected !== null) {
            if (addonNonSelected === nextAddon) {
                $('#extras-addon-no').prop('checked', true);
            }
        }

        if ($('.form-row__input--radio').is(':checked')) {
            selectedValue = $('input[name=extras-addon]:checked').val();
        }

        onRadioButtonClick();
        onContinueHandler();
       
    }

    function displayHeader() {
        if ($('#addon-preselected').val() === "False") {
            $('#question-header').removeClass('hide');
        }
    }

    function onRadioButtonClick() {
        $('.addon-radio__wrapper').each(function (index, value) {
            $(value).find('.form-row__input--radio').off('change').on('change',
                function () {
                    selectedValue = $(this).val();

                    if ($(this).attr("id") === "extras-addon-no") {
                        sessionStorage.setItem(("NonSelectedAddon").concat(configuration.SelectedAddon), selectedValue);
                    } else {
                        sessionStorage.setItem(sessionStorageSelectedAddonKeyName.concat(configuration.SelectedAddon), selectedValue);
                    }
                    
                    sessionStorage.setItem(sessionStorageKeyName.concat(configuration.ContinueUrl), configuration.ContinueUrl);

                    if ($('#continue-button').is(":hidden")) {
                        bgl.common.utilities.redirectToUrl(selectedValue ? selectedValue : configuration.NextStepUrl);
                    }
                });
        });

        $('.addon-radio__wrapper')
            .find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function onContinueHandler() {
        $('#continue-button').off('click').on('click',
            function (e) {
                e.preventDefault();

                var nextaddonvalue = $('#next-addon').val();

                var thisAddonSelected =
                    sessionStorage.getItem(("SelectedAddon").concat(configuration.SelectedAddon));

                if (thisAddonSelected) {
                    bgl.common.utilities.redirectToUrl(thisAddonSelected);
                } else {
                    if (nextaddonvalue) {

                        var nextAddonSelected = sessionStorage.getItem(("SelectedAddon").concat(nextaddonvalue));

                        if (nextAddonSelected) {
                            bgl.common.utilities.redirectToUrl(nextAddonSelected);
                        }
                        else {
                            bgl.common.utilities.redirectToUrl(nextaddonvalue);
                        }
                    }
                    else {
                        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
                    }
                }
            });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();