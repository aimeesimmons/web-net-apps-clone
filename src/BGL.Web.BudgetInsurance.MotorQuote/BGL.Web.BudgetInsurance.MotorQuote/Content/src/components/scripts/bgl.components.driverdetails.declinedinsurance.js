﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.driverdetails.declinedinsurance = (function () {
    var form;
    var configuration;

    function ctor(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#declined-insurance');

        bgl.common.validator.init(form);
    }

    function initComponent(componentConfiguration) {
        ctor(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        var person = bgl.common.datastore.getData(form);
        var hasMedicalCondition = person.MedicalCondition.toLowerCase() === "true";

        if (hasMedicalCondition) {
            $("#" + configuration.Id + "-back-button").attr("href", configuration.DvlaNotifiedPageUrl);
        }

        subscribeToEvents();
    }

    function subscribeToEvents() {
        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', continueHandler)
                .on('click', continueHandler);
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        var currentData = bgl.common.datastore.getData($(this));
                        var dataForValidate = $(form).serialize() + '&' + $.param(currentData) + '&' + $.param({ ComponentConfiguration: configuration });

                        bgl.common.validator.submitForm(form, configuration, dataForValidate);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(event.target).prop("checked", true);
            $(event.target).click();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.find("input[type=radio]").off();
        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.needUpdatePerson === true) {

            form.attr("action", data.patchUrl);

            $("#HasChanges").val(bgl.common.datastore.isDataChanged(form) === true);

            var currentData = bgl.common.datastore.getData(form);

            var dataForValidate = $(form).serialize()
                + '&' + $.param(currentData)
                + '&' + $.param({ ComponentConfiguration: configuration, ViewName: "DeclinedInsurance" });

            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.common.validator.submitForm(form, configuration, dataForValidate);
            return;
        }

        if (data.nextDtsPageUrl) {
            location.href = data.nextDtsPageUrl;
        }
    }

    function onError() {
        bgl.components.driverdetails.declinedinsurance.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
