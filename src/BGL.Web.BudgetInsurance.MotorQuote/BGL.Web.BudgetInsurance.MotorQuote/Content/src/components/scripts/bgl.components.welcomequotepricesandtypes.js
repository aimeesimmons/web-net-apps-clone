﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequotepricesandtypes = (function () {

    var configuration;
    var annualData;
    var monthlyData;
    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        annualData = $('#' + configuration.Id + '__annual-data');
        monthlyData = $('#' + configuration.Id + '__monthly-data');

        bgl.common.pubsub.on('switchPaymentType', function (isMonthly) {
            if (isMonthly === true) {
                annualData.addClass('hide');
                monthlyData.removeClass('hide');
            }
            if (isMonthly === false) {
                annualData.removeClass('hide');
                monthlyData.addClass('hide');
            }
        });
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequotepricesandtypes;
}