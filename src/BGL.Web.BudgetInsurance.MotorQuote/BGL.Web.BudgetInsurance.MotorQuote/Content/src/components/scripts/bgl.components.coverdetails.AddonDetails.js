﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.AddonDetails = (function () {
    var configuration,
        form,
    isChoicesMade;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        onYesButtonClick();
        onNoButtonClick();
        onBreakDownTierClick();
        onContinueClick();
        onBackButtonClick();
    }

    function onYesButtonClick() {
        $("#Yes-button").off('click').on('click',
            function () {
                $("#Continue-Button").show();
                bgl.common.setFocusedElement($(this).data("focusid"));
                bgl.common.pubsub.emit("PollBasket");
                bgl.common.pubsub.emit("showNotification");
                isChoicesMade = true;
            });
    }

    function onNoButtonClick() {
        $("#No-button").off('click').on('click',
            function () {
                isChoicesMade = true;
                $("#Continue-Button").show();
                bgl.common.setFocusedElement($(this).data("focusid"));
                bgl.common.pubsub.emit("PollBasket");
                bgl.common.pubsub.emit("showNotification");
            });
    }

    function onContinueClick() {
        $("#Continue-Button").off('click').on('click',
            function () {
                if (!isChoicesMade)
                {
                    $("#validation-message").show();
                }
            });
    }

    function onBackButtonClick() {
        $("#Back-Button").off('click').on('click',
            function () {
                window.history.go(-1);
            });
    }

    function onBreakDownTierClick() {
        $("#BreakDown").off('click').on('click',
            function () {
                bgl.common.pubsub.emit("PollBasket");
                bgl.common.pubsub.emit("showNotification");
            });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();