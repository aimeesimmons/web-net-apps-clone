﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.registeredkeeper = (function () {

    var form,
        configuration,
        container,
        continueButton,
        typeInput,
        triggerPollBasket,
        companyKeeper,
        coverDetailsCommon,
        HIDDEN_CLASS = "hide",
        SPACE_KEYCODE = 32,
        ENTER_KEYCODE = 13;

    function initComponent(componentConfiguration, containerId, callPollBasket) {
        initElements(componentConfiguration, containerId, callPollBasket);

        showCompanyName();

        subscribeForEvents();

        displaySubmitButton(form);
    }

    function loadComponent(config, url, containerId, callPollBasket) {
        bgl.common.loader.load(configuration,
            url,
            container,
            function () {
                initComponent(config, containerId, callPollBasket);
            });
    }

    function onSuccess(configuration, data) {
        if (data.nextUrl) {
            bgl.components.coverdetails.relationshipstatus.load(configuration, data.nextUrl, container);
        } else {
            closeSlidePanelAndTriggerUnderwriting(true);
        }

        clearOtherOptionFields(form);
    }

    function onMultiStepEditSuccess(configuration, data) {
        var url = data.nextUrl || configuration.NextStepUrl;

        bgl.common.utilities.redirectToUrl(url);

        clearOtherOptionFields(form);
    }

    function onError() {
        continueButton.removeAttr('disabled');
        bgl.components.coverdetails.registeredkeeper.init(configuration);
    }

    function clearOtherOptionFields(componentForm) {
        var fieldsToRemove = [
            "Title", "DateOfBirth", "LastName", "HasSameAddress",
            "FirstName", "RelationshipStatusCode", "MaritalStatusCode"
        ];

        bgl.common.datastore.deleteFields(componentForm, fieldsToRemove);
    }

    function redirectIfSessionStorageIsNotFound(componentForm, parentJourneyUrl) {
        var data = bgl.common.datastore.getData(componentForm);

        if (!data || !data["RelationshipStatusCode"]) {
            bgl.common.utilities.redirectToUrl(parentJourneyUrl);
        }
    }

    function initElements(componentConfiguration, containerId, callPollBasket) {
        configuration = componentConfiguration;
        form = $('#' + configuration.Id + '_form-registered-keeper');
        continueButton = form.find("#ContinueButton");
        container = containerId;
        coverDetailsCommon = bgl.components.coverdetails.common;
        typeInput = form.find("#Type");
        companyKeeper = form.find("#company-keeper");
        triggerPollBasket = callPollBasket;
    }

    function subscribeForEvents() {
        bgl.common.validator.init(form);
        bgl.common.validator.enableMaxLength();
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    continueButton.attr('disabled', true);

                    if (!bgl.common.validator.isFormValid(form)) {
                        continueButton.removeAttr('disabled');
                        return;
                    }

                    var isSingleEdit = configuration.JourneyType === coverDetailsCommon.journeyTypes.singleEdit;

                    if (isSingleEdit && !bgl.common.datastore.isDataChanged(form)) {
                        closeSlidePanelAndTriggerUnderwriting(triggerPollBasket);
                        return;
                    }

                    bgl.common.validator.submitForm(form, configuration);
                });

        form.find('input[type="radio"]')
            .off('click')
            .off('keypress')
            .on({
                "keypress": function (e) {
                    if (e.keyCode === ENTER_KEYCODE || e.keyCode === SPACE_KEYCODE) {
                        $(this).click();
                    }
                },
                'click': function (event) {
                    var keeperType = $(this).attr("data-keeper-type");
                    registerKeeperOptionClickHandler(keeperType);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function updateContinueButtonValue() {
        if (configuration.JourneyType !== coverDetailsCommon.journeyTypes.multiStepEdit) {
            continueButton.val("Done");
        }
    }

    function showCompanyName() {
        var data = bgl.common.datastore.getData(form);

        if (data) {
            switch (data.RegisteredKeeperId) {
                case "company":
                    companyKeeper.removeClass(HIDDEN_CLASS);
                    typeInput.val("O");
                    break;
                case "soleTrader":
                    companyKeeper.removeClass(HIDDEN_CLASS);
                    typeInput.val("B");
                    break;
                case "partnership":
                    companyKeeper.removeClass(HIDDEN_CLASS);
                    typeInput.val("A");
                    break;
            }
        }
    }

    function registerKeeperOptionClickHandler(keeperType) {
        var companyName = $("#CompanyName");

        switch (keeperType) {
            case "P":
                companyKeeper.addClass(HIDDEN_CLASS);
                companyName.val("").focusout();
                typeInput.val("P");
                form.change();
                updateContinueButtonValue();
                submitForm(form);
                break;

            case "O":
                companyKeeper.removeClass(HIDDEN_CLASS);
                typeInput.val("O");
                companyName.val("").focusout();
                continueButton.removeClass(HIDDEN_CLASS);
                updateContinueButtonValue();
                break;

            case "B":
                companyKeeper.removeClass(HIDDEN_CLASS);
                typeInput.val("B");
                companyName.val("").focusout();
                continueButton.removeClass(HIDDEN_CLASS);
                updateContinueButtonValue();
                break;

            case "A":
                companyKeeper.removeClass(HIDDEN_CLASS);
                typeInput.val("A");
                companyName.val("").focusout();
                continueButton.removeClass(HIDDEN_CLASS);
                updateContinueButtonValue();
                break;

            case "Other":
                companyKeeper.addClass(HIDDEN_CLASS);
                companyName.val("");
                typeInput.val("Other");
                continueButton.val('Continue');
                submitForm(form);
                break;
        }
    }

    function submitForm(form) {
        var submitButton = form.find(':submit');

        if (submitButton.hasClass(HIDDEN_CLASS)) {
            form.change().submit();
        }
    }

    function displaySubmitButton(form) {
        if (form.find("[type='radio']:checked").length) {
            form.find(':submit').removeClass(HIDDEN_CLASS);
        }
    }

    function updateHeading(form) {
        var data = bgl.common.datastore.getData(form);

        if (data) {
            var heading = form.find('#heading');
            var text = heading.html();

            heading.html(text.replace('{FirstName}', data.FirstName));
        }
    }

    function closeSlidePanelAndTriggerUnderwriting(triggerPollBasket) {
        bgl.common.datastore.removeSessionStorage(form);
        $('[data-dismiss="slide-panel"]').click();

        if (triggerPollBasket) {
            bgl.common.pubsub.emit("PollBasket");
        }
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onMultiStepEditSuccess: onMultiStepEditSuccess,
        onError: onError,
        displaySubmitButton: displaySubmitButton,
        submitForm: submitForm,
        updateHeading: updateHeading,
        clearOtherOptionFields: clearOtherOptionFields,
        redirectIfSessionStorageIsNotFound: redirectIfSessionStorageIsNotFound
    };
})();