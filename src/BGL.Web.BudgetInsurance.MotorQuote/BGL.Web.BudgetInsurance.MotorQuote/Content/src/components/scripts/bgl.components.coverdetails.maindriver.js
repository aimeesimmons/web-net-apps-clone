﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.maindriver = (function () {
    var configuration,
        form;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#main-driver');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', continueHandler)
                .on('click',
                    function(event) {
                        $(this).focusout();
                        submitForm();
                    });
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
            continueButton.off("click").on("click", continueHandler);
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function submitForm() {
        form.submit();
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;
        if (code === 13 || code === 32) {
            $(this).click();
        }
    }

    function onSuccess(componentConfiguration) {
        location.href = componentConfiguration.NextStepUrl;
    }

    function continueHandler(event, element) {
        event.preventDefault();
        if (bgl.common.datastore.isDataChanged(form)) {
            submitForm();
        }
        location.href = configuration.NextStepUrl;
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess
    };
})();