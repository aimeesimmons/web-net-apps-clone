jQuery.support.cors = true;
var livePersonTag = livePersonTag || {};
livePersonTag.initialise = function () {
    window.lpTag = window.lpTag || {};
    if (typeof window.lpTag._tagCount === 'undefined') {
        window.lpTag = {
            site: window.siteId || '',
            section: lpTag.section || '',
            autoStart: lpTag.autoStart === false ? false : true,
            ovr: lpTag.ovr || {},
            _v: '1.6.0',
            _tagCount: 1,
            protocol: 'https:',
            events: {
                bind: function (app, ev, fn) {
                    lpTag.defer(function () {
                        lpTag.events.bind(app, ev, fn);
                    },
                        0);
                },
                trigger: function (app, ev, json) {
                    lpTag.defer(function () {
                        lpTag.events.trigger(app, ev, json);
                    },
                        1);
                }
            },
            defer: function (fn, fnType) {
                if (fnType == 0) {
                    this._defB = this._defB || [];
                    this._defB.push(fn);
                } else if (fnType == 1) {
                    this._defT = this._defT || [];
                    this._defT.push(fn);
                } else {
                    this._defL = this._defL || [];
                    this._defL.push(fn);
                }
            },
            load: function (src, chr, id) {
                var t = this;
                setTimeout(function () {
                    t._load(src, chr, id);
                },
                    0);
            },
            _load: function (src, chr, id) {
                var url = src;
                if (!src) {
                    url = this.protocol +
                        '//' +
                        ((this.ovr && this.ovr.domain) ? this.ovr.domain : 'lptag.liveperson.net') +
                        '/tag/tag.js?site=' +
                        this.site;
                }
                var s = document.createElement('script');
                s.setAttribute('charset', chr ? chr : 'UTF-8');
                if (id) {
                    s.setAttribute('id', id);
                }
                s.setAttribute('src', url);
                document.getElementsByTagName('head').item(0).appendChild(s);
            },
            init: function () {
                this._timing = this._timing || {};
                this._timing.start = (new Date()).getTime();
                var that = this;
                if (window.attachEvent) {
                    window.attachEvent('onload',
                        function () {
                            that._domReady('domReady');
                        });
                } else {
                    window.addEventListener('DOMContentLoaded',
                        function () {
                            that._domReady('contReady');
                        },
                        false);
                    window.addEventListener('load',
                        function () {
                            that._domReady('domReady');
                        },
                        false);
                }
                if (typeof (window._lptStop) == 'undefined') {
                    this.load();
                }
            },
            start: function () {
                this.autoStart = true;
            },
            _domReady: function (n) {
                if (!this.isDom) {
                    this.isDom = true;
                    this.events.trigger('LPT',
                        'DOM_READY',
                        {
                            t: n
                        });
                }
                this._timing[n] = (new Date()).getTime();
            },
            vars: lpTag.vars || [],
            dbs: lpTag.dbs || [],
            ctn: lpTag.ctn || [],
            sdes: lpTag.sdes || [],
            ev: lpTag.ev || []
        };
        lpTag.init();
    } else {
        window.lpTag._tagCount += 1;
    }
}
if (typeof JSON !== "object") {
    JSON = {};
}

(function () {
    "use strict";

    var rx_one = /^[\],:{}\s]*$/;
    var rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
    var rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
    var rx_four = /(?:^|:|,)(?:\s*\[)+/g;
    var rx_escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
    var rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10
            ? "0" + n
            : n;
    }

    function this_value() {
        return this.valueOf();
    }

    if (typeof Date.prototype.toJSON !== "function") {

        Date.prototype.toJSON = function () {

            return isFinite(this.valueOf())
                ? this.getUTCFullYear() + "-" +
                f(this.getUTCMonth() + 1) + "-" +
                f(this.getUTCDate()) + "T" +
                f(this.getUTCHours()) + ":" +
                f(this.getUTCMinutes()) + ":" +
                f(this.getUTCSeconds()) + "Z"
                : null;
        };

        Boolean.prototype.toJSON = this_value;
        Number.prototype.toJSON = this_value;
        String.prototype.toJSON = this_value;
    }

    var gap;
    var indent;
    var meta;
    var rep;


    function quote(string) {

        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.

        rx_escapable.lastIndex = 0;
        return rx_escapable.test(string)
            ? "\"" + string.replace(rx_escapable, function (a) {
                var c = meta[a];
                return typeof c === "string"
                    ? c
                    : "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4);
            }) + "\""
            : "\"" + string + "\"";
    }


    function str(key, holder) {

        // Produce a string from holder[key].

        var i;          // The loop counter.
        var k;          // The member key.
        var v;          // The member value.
        var length;
        var mind = gap;
        var partial;
        var value = holder[key];

        // If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === "object" &&
            typeof value.toJSON === "function") {
            value = value.toJSON(key);
        }

        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.

        if (typeof rep === "function") {
            value = rep.call(holder, key, value);
        }

        // What happens next depends on the value's type.

        switch (typeof value) {
            case "string":
                return quote(value);

            case "number":

                // JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value)
                    ? String(value)
                    : "null";

            case "boolean":
            case "null":

                // If the value is a boolean or null, convert it to a string. Note:
                // typeof null does not produce "null". The case is included here in
                // the remote chance that this gets fixed someday.

                return String(value);

            // If the type is "object", we might be dealing with an object or an array or
            // null.

            case "object":

                // Due to a specification blunder in ECMAScript, typeof null is "object",
                // so watch out for that case.

                if (!value) {
                    return "null";
                }

                // Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

                // Is the value an array?

                if (Object.prototype.toString.apply(value) === "[object Array]") {

                    // The value is an array. Stringify every element. Use null as a placeholder
                    // for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || "null";
                    }

                    // Join all of the elements together, separated with commas, and wrap them in
                    // brackets.

                    v = partial.length === 0
                        ? "[]"
                        : gap
                            ? "[\n" + gap + partial.join(",\n" + gap) + "\n" + mind + "]"
                            : "[" + partial.join(",") + "]";
                    gap = mind;
                    return v;
                }

                // If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === "object") {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        if (typeof rep[i] === "string") {
                            k = rep[i];
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (
                                    gap
                                        ? ": "
                                        : ":"
                                ) + v);
                            }
                        }
                    }
                } else {

                    // Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (
                                    gap
                                        ? ": "
                                        : ":"
                                ) + v);
                            }
                        }
                    }
                }

                // Join all of the member texts together, separated with commas,
                // and wrap them in braces.

                v = partial.length === 0
                    ? "{}"
                    : gap
                        ? "{\n" + gap + partial.join(",\n" + gap) + "\n" + mind + "}"
                        : "{" + partial.join(",") + "}";
                gap = mind;
                return v;
        }
    }

    // If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== "function") {
        meta = {    // table of character substitutions
            "\b": "\\b",
            "\t": "\\t",
            "\n": "\\n",
            "\f": "\\f",
            "\r": "\\r",
            "\"": "\\\"",
            "\\": "\\\\"
        };
        JSON.stringify = function (value, replacer, space) {

            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.

            var i;
            gap = "";
            indent = "";

            // If the space parameter is a number, make an indent string containing that
            // many spaces.

            if (typeof space === "number") {
                for (i = 0; i < space; i += 1) {
                    indent += " ";
                }

                // If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === "string") {
                indent = space;
            }

            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== "function" &&
                (typeof replacer !== "object" ||
                    typeof replacer.length !== "number")) {
                throw new Error("JSON.stringify");
            }

            // Make a fake root object containing our value under the key of "".
            // Return the result of stringifying the value.

            return str("", { "": value });
        };
    }


    // If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== "function") {
        JSON.parse = function (text, reviver) {

            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.

                var k;
                var v;
                var value = holder[key];
                if (value && typeof value === "object") {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            rx_dangerous.lastIndex = 0;
            if (rx_dangerous.test(text)) {
                text = text.replace(rx_dangerous, function (a) {
                    return "\\u" +
                        ("0000" + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

            // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with "()" and "new"
            // because they can cause invocation, and "=" because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.

            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with "@" (a non-JSON character). Second, we
            // replace all simple value tokens with "]" characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or "]" or
            // "," or ":" or "{" or "}". If that is so, then the text is safe for eval.

            if (
                rx_one.test(
                    text
                        .replace(rx_two, "@")
                        .replace(rx_three, "]")
                        .replace(rx_four, "")
                )
            ) {

                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The "{" operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.

                j = eval("(" + text + ")");

                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.

                return (typeof reviver === "function")
                    ? walk({ "": j }, "")
                    : j;
            }

            // If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError("JSON.parse");
        };
    }
}());
/*! modernizr 3.6.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-cors-input-svg-setclasses !*/
!function (e, n, t) { function s(e, n) { return typeof e === n } function a() { var e, n, t, a, o, i, c; for (var f in r) if (r.hasOwnProperty(f)) { if (e = [], n = r[f], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length)) for (t = 0; t < n.options.aliases.length; t++) e.push(n.options.aliases[t].toLowerCase()); for (a = s(n.fn, "function") ? n.fn() : n.fn, o = 0; o < e.length; o++) i = e[o], c = i.split("."), 1 === c.length ? Modernizr[c[0]] = a : (!Modernizr[c[0]] || Modernizr[c[0]] instanceof Boolean || (Modernizr[c[0]] = new Boolean(Modernizr[c[0]])), Modernizr[c[0]][c[1]] = a), l.push((a ? "" : "no-") + c.join("-")) } } function o(e) { var n = f.className, t = Modernizr._config.classPrefix || ""; if (u && (n = n.baseVal), Modernizr._config.enableJSClass) { var s = new RegExp("(^|\\s)" + t + "no-js(\\s|$)"); n = n.replace(s, "$1" + t + "js$2") } Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), u ? f.className.baseVal = n : f.className = n) } function i() { return "function" != typeof n.createElement ? n.createElement(arguments[0]) : u ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments) } var l = [], r = [], c = { _version: "3.6.0", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function (e, n) { var t = this; setTimeout(function () { n(t[e]) }, 0) }, addTest: function (e, n, t) { r.push({ name: e, fn: n, options: t }) }, addAsyncTest: function (e) { r.push({ name: null, fn: e }) } }, Modernizr = function () { }; Modernizr.prototype = c, Modernizr = new Modernizr, Modernizr.addTest("cors", "XMLHttpRequest" in e && "withCredentials" in new XMLHttpRequest); var f = n.documentElement, u = "svg" === f.nodeName.toLowerCase(), p = i("input"), m = "autocomplete autofocus list placeholder max min multiple pattern required step".split(" "), d = {}; Modernizr.input = function (n) { for (var t = 0, s = n.length; s > t; t++) d[n[t]] = !!(n[t] in p); return d.list && (d.list = !(!i("datalist") || !e.HTMLDataListElement)), d }(m), Modernizr.addTest("svg", !!n.createElementNS && !!n.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect), a(), o(l), delete c.addTest, delete c.addAsyncTest; for (var g = 0; g < Modernizr._q.length; g++) Modernizr._q[g](); e.Modernizr = Modernizr }(window, document);
window.Modernizr = window.Modernizr || {};
window.Nina = window.Nina || {};
window.Nina.version = window.Nina.version || {};
window.Nina.$ = jQuery;
window.Nina.helper = window.Nina.helper || {};
window.Nina.config = window.Nina.config || {};
window.Nina.enums = window.Nina.enums || {};
window.Nina.storage = window.Nina.storage || {};
window.Nina.text = window.Nina.text || {};
window.Nina.ws = window.Nina.ws || {};
window.Nina.ui = window.Nina.ui || {};
window.NinaVars = window.NinaVars || {};
window.Nina.storage.sci = '';
/**
 * Number of . composing the TLD (not always 2 as ".something.co.uk" shows it) starting from the end.
 * @type {int}
 * @default 2
 */
window.domainLevel = location.hostname.split('.').length;

window.Nina.version = {
    number: "sdk=3.0.0-201605161647/ui=nina-block-1.0",
    date: "2019-07-16T16:39:05.727Z",
    toString: function () { return "UI Version " + this.number + ", build on " + this.date; }
};

var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
// At least Safari 3+: "[object HTMLElementConstructor]"
var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6

var firefoxVersion, ieVersion, chromeVersion, safariVersion;

if (navigator.userAgent.search("Firefox") >= 0) {
    var firefoxPosition = navigator.userAgent.search("Firefox") + 8;
    firefoxVersion = navigator.userAgent.substring(firefoxPosition);
}

if (navigator.userAgent.search("MSIE") >= 0) {
    var iePosition = navigator.userAgent.search("MSIE") + 5;
    var ieEnd = navigator.userAgent.search("; Windows");
    ieVersion = navigator.userAgent.substring(iePosition, ieEnd);
}
if (navigator.userAgent.search("Chrome") >= 0) {
    var chromePosition = navigator.userAgent.search("Chrome") + 7;
    var chromeEnd = navigator.userAgent.search(" Safari");
    chromeVersion = navigator.userAgent.substring(chromePosition, chromeEnd);
}
if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
    var safariPosition = navigator.userAgent.search("Version") + 8;
    var safariEnd = navigator.userAgent.search(" Safari");
    safariVersion = navigator.userAgent.substring(safariPosition, safariEnd);
}

window.Nina.ui.isIOS = function () {
    "use strict";

    var deviceAgent = navigator.userAgent.toLowerCase();
    return deviceAgent.match(/(iphone|ipod|ipad)/);
};

function checkNinaSessionNeedsClearing() {
    var sessionTerminationPoints = "legal";
    if (sessionTerminationPoints.indexOf(window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1).toLowerCase()) >= 0) {
        window.isNewNinaSessionRequired = true;
        document.cookie = "Nina-nina-block-session=; path=/;";
        document.cookie = "Nina-nina-block-persist=; path=/;";
    } else {
        window.isNewNinaSessionRequired = false;
    }
}
window.Nina.helper.date = (function () {
    "use strict";

    return {
		/**
         * Convert a date object to an ISO 8601 string including the TimeZone indicator
         * @param {Date} d the date to convert to an ISO 8601 string
         * @returns {string} the ISO 8601 representation of the date
         */
        toISOString: function (d) {
            function pad(n) {
                return n < 10 ? '0' + n : n;
            }

            function pad2(n) {
                return n < 100 ? '0' + (n < 10 ? '0' + n : n) : n;
            }

            var base, tzsign, tz, tzhours, tzmins;

            base = d.getFullYear() + '-' +
                pad(d.getMonth() + 1) + '-' +
                pad(d.getDate()) + 'T' +
                pad(d.getHours()) + ':' +
                pad(d.getMinutes()) + ':' +
                pad(d.getSeconds()) + "." +
                pad2(d.getMilliseconds());
            tz = d.getTimezoneOffset();
            tzsign = tz > 0 ? "-" : "+";
            tz = Math.abs(tz);
            tzhours = Math.floor(tz / 60);
            tzmins = tz - tzhours * 60;

            base = base + tzsign + pad(tzhours) + ":" + pad(tzmins);

            return base;
        },
		/**
         * Parses a string containing an ISO 8601 date
         * @param date an ISO 8601 date
         * @returns {Number} the date corresponding to the string
         */
        parse: function (date) {
			/**
             * @preserve
             * Date.parse with progressive enhancement for ISO 8601 <https://github.com/csnover/js-iso8601>
             * 2011 Colin Snover <http://zetafleet.com>
             * Released under MIT license.
             */
            var timestamp, struct, minutesOffset = 0, numericKeys = [1, 4, 5, 6, 7, 10, 11];

            // ES5 15.9.4.2 states that the string should attempt to be parsed as a Date Time String Format string
            // before falling back to any implementation-specific date parsing, so that's what we do, even if native
            // implementations could be faster
            //              1 YYYY                2 MM       3 DD            4 HH     5 mm        6 ss          7 msec        8 Z 9     10 tzHH     11 tzmm
            if ((struct = /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2})?)?)?(?:T(\d{2}):?(\d{2})(?::?(\d{2})(?:[,.](\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::?(\d{2}))?)?)?$/.exec(date))) {
                // avoid NaN timestamps caused by "undefined" values being passed to Date.UTC
                for (var i = 0, k; (k = numericKeys[i]); ++i) {
                    struct[k] = +struct[k] || 0;
                }

                // allow undefined days and months
                struct[2] = (+struct[2] || 1) - 1;
                struct[3] = +struct[3] || 1;

                if (struct[8] !== 'Z' && struct[9] !== undefined) {
                    minutesOffset = struct[10] * 60 + struct[11];

                    if (struct[9] === '+') {
                        minutesOffset = 0 - minutesOffset;
                    }
                }

                timestamp = Date.UTC(struct[1], struct[2], struct[3], struct[4], struct[5] + minutesOffset, struct[6], struct[7]);
            }
            else {
                timestamp = Date.parse ? Date.parse(date) : NaN;
            }
            return timestamp;
        }
    };
}());
window.Nina.helper.newDebug = function (_ui, _cookiesJar) {
    "use strict";

    var me = {};
    me.ui = _ui;
    me.cookiesJar = _cookiesJar;

    var publicMethods = {
        newConversation: function (reload) {
            reload = (undefined !== reload) ? reload : true;
            me.cookiesJar.setSession("sci", "");
            if (reload) {
                location.reload();
            }
        },
        getUIVersion: function () {
            return "sdk=3.0.0-201605161647/ui=nina-block-1.0";
        },
        switchPreprodMode: function (reload) {
            reload = (reload !== "undefined") ? reload : true;
            if (publicMethods.isPreprodMode()) {
                me.cookiesJar.removeSession("preprod");
            } else {
                me.cookiesJar.setSession("preprod", "true");
            }
            if (reload) {
                publicMethods.newConversation();
            }
        },
        isPreprodMode: function () {
            return me.cookiesJar.getSession("preprod") !== null;
        },
        createTicket: function (remark) {
            if (remark && remark !== "") {
                me.ui.sendQuery("", window.Nina.enums.Command.DialogRemark + remark, {});
            }
        },
        getAgentVersion: function () {
            me.ui.sendQuery("", window.Nina.enums.Command.DialogVersion, {});
        },
        displayCookies: function () {
            var cookies = [
                me.cookiesJar.sessionCookieName,
                me.cookiesJar.persistentCookieName,
                me.cookiesJar.postQualifCookieName
            ];

            for (var index = 0, len = cookies.length; index < len; index++) {
                var cookieString = decodeURIComponent(me.cookiesJar.readCookie(cookies[index]));
                console.log(cookies[index] + ": ", cookieString);
            }
        }
    };
    return publicMethods;
};
(function ($) {
    //id itterator if the inputs don't have ids
    if ('object' !== typeof Modernizr.input || Modernizr.input.placeholder) {
        return;
    }
    var phid = 0;
    $.fn.placeholder = function () {
        return this.bind({
            focus: function () {
                $(this).parent().addClass('placeholder-focus');
            }, blur: function () {
                $(this).parent().removeClass('placeholder-focus');
            }, 'keyup input change': function () {
                $(this).parent().toggleClass('placeholder-changed', this.value !== '');
            }
        }).each(function () {
            var $this = $(this);
            //Adds an id to elements if absent
            if (!this.id) this.id = 'ph_' + (phid++);
            //Create input wrapper with label for placeholder. Also sets the for attribute to the id of the input if it exists.
            $('<span class="placeholderWrap"><label for="' + this.id + '">' + $this.attr('placeholder') + '</label></span>')
                .insertAfter($this)
                .append($this);
            //Disables default placeholder
            $this.attr('placeholder', '').keyup();
        });
    };

    //Default plugin invocation
    $(function () {
        $('input[placeholder]').placeholder();
        $('textarea[placeholder]').placeholder();
    });
})(window.Nina.$);
window.Nina.helper.scripts = (function () {
    "use strict";
    var $ = window.Nina.$;

    /**
     * Retrieves the base path of the location the agent files are stored in
     * @param {string} scriptname
     * @returns {string}
     */
    function getScriptPath(scriptname) {
        scriptname = scriptname.toLowerCase();
        var scriptobjects = document.getElementsByTagName('script');
        for (var i = 0; i < scriptobjects.length; i++) {
            if (scriptobjects[i].src && scriptobjects[i].src.toLowerCase().lastIndexOf(scriptname) !== -1) {
                return scriptobjects[i].src.toString().substring(0, scriptobjects[i].src.toLowerCase().lastIndexOf(scriptname));
            }
        }
        return null;
    }

    return {
        getScriptPath: function (scriptName) { return getScriptPath(scriptName); },
        isScriptLoaded: function (scriptName) { return getScriptPath(scriptName) !== null; },
        loadScript: function (scriptName) {
            $.ajax(scriptName, {
                cache: true,
                dataType: "script"
            });
        }
    };
})();

window.Nina.config.newConfig = function (agentConfig) {
    "use strict";

    var defaultConfig = /** @lends window.Nina.config.Config.prototype */ {
        /**
         * The directory where the agent files are stored and the id of the div that will contain the virtual agent
         * It's automatically populated by sprockets.
         * @const
         * @type string
         * */
        agentId: "nina-block",
        /**
         * If the current ui is a popup or not
         * @const
         * @type boolean
         */
        popup: ("true" !== "true") && ("false" !== "true"),
        /**
         * The base directory where the files are hosted
         * @type string
         */
        baseDir: window.Nina.helper.scripts.getScriptPath("scripts/Nina"),
        /**
         * Cookies specific parameters
         * @type window.Nina.config.CookiesParams
         */
        cookies: /** @lends window.Nina.config.CookiesParams */{
            /**
             * The lifetime to assign to the persistent cookie
             * @type {int}
             * @default 30 days
             */
            lifeTimePersist: 30 * 24 * 60 * 60 * 1000,
            /**
             * Should we restrict the cookie to the current TLD rather than the current host
             * @type {boolean}
             * @default true
             */
            restrictDomain: false,

            domainLevel: window.domainLevel,
            /**
             *  Should we restrict the cookie to the current path
             *  @type {boolean}
             *  @default true
             */
            restrictPath: false,
            /**
             * Should we set the cookie as secure
             * @type {boolean}
             * @default true
             */
            secure: true,
            /**
             * Length of the interaction timeout. Used to figure out if a session is still alive
             * @type {int}
             * @default 5 minutes
             */
            timeoutInteraction: 5 * 60 * 1000,
            /**
             * How long before a livechat session times out without polling.
             * @type {int}
             * @default 30s
             */
            timeoutInteractionLiveChat: 30 * 1000,
            /**
             * Lifetime of a postQualif cookie
             * @type {int}
             * @default 7 days
             */
            lifeTimePostQualif: 7 * 24 * 60 * 60 * 1000
        },
        /**
         * Parameters specific to the FirstMessage module
         * @type window.Nina.config.FirstMessageParams
         */
        firstMessage: /** @lends window.Nina.config.FirstMessageParams */{
            /**
             * Message (HTML) asking the user to click to retrieve the transcript of the current chat (when switching from one page to another
             * The link triggering the transcript retrieval must have the class nw_TranscriptLink
             * @type {string}
             */
            transcriptMessage: "<button type=\"button\" class=\"nw_TranscriptLink\">Click here to retrieve the chat transcript.</button>",
            /**
             * Message (HTML) displayed when the user asks to retrieve the chat transcript after the chat session has timed out
             * @type {string}
             */
            sessionExpiredMessage: "I'm sorry, I cannot retrieve the transcript because your session timed out",
            /**
             * Message (HTML) displayed when opening the UI and there is no ongoing session. It can be a string or a function.
             * If it is a function, it will be called and the return value will be used as the welcome message. This allows
             * to have dynamic welcome messages
             * @type {string|function():string}
             */
            welcome: "Hi, I'm your virtual agent. Just type your question below and I'll be glad to help you."
        },
        /**
         * Parameters specific to the various UI modules (ui, displayhistory, displaysingle, bubble)
         * @type window.Nina.config.UIParams
         */
        ui: /** @lends window.Nina.config.UIParams */{
            /**
             * The version of the UI. Injected by Grunt based on package.json and properties.yaml
             * @type {string}
             */
            version: "sdk=3.0.0-201605161647/ui=nina-block-1.0",
            /**
             * The date and time the build of the UI took place. Automatically generated during build in the client's Gruntfile
             * @type {string}
             */
            date: "2019-07-16T16:39:05.727Z",
            /**
             * The type of the UI. Injected by Grunt based on the value present in properties.yaml
             * @type {string}
             */
            uiType: "block",
            /**
             * If enabled, will send the type of the UI to the WebBotHost, which will create a tag ui_type_<uiType> in Analytics
             */
            sendUiType: true,
            /**
             * The maximum length of a user query
             * @type {int}
             * @default 110
             */
            maxInputLength: 110,
            /**
             * The separator added after the name of the emitter of the message in an history UI.
             * @type {string}
             * @default :&nbsp;
             */
            headerSeparator: ":&nbsp;",
            /**
             * Emitter's name when the virtual agent is speaking in an history UI
             * @type {string}
             * @default ""
             */
            headerAgent: "Agent",
            /**
             * Emitter's name for system messages in an history UI
             * @type {string}
             * @default ""
             */
            headerSystem: "",
            /**
             * Emitter's name for error messages in an history UI
             * @type {string}
             * @default ""
             */
            headerError: "",
            /**
             * Emitter's name for user mesages in an history UI
             * @type {string}
             * @default You
             */
            headerUser: "You",
            /**
             * Emitter's name for CSR messages (if we don't know the name of the specific CSR in an history UI
             * @type {string}
             * @default CSR
             */
            defaultCSRName: "CSR",
            /**
             * Text to display as the user friendly query text when sending data from content forms
             * @type {string}
             * @default "Sent form data"
             */
            sendFormDataMessage: "Sent form data",
            /**
             * Text to display in the status message while the virtual agent service is processing the query
             * @type {string}
             * @default Bot is typing ...
             */
            botIsTyping: "Nina is typing ...",
            /**
             * Text to display in the status message while the CSR is writing status is true as returned by the web service
             * @type {string}
             * @default CSR is typing ...
             */
            csrIsTyping: "CSR is typing ...",
            /**
             * Text to display in the status message telling the user how many characters are left before he reaches the
             * maximum. The sequence ##CHARSLEFT## will be replaced by the actual number of characters left.
             * @type {string}
             * @default ##CHARSLEFT## characters left
             */
            charsLeft: "##CHARSLEFT## characters left",
            /**
             * Array of the available text sizes in an history UI. If empty, do not let the user change the text size
             * @type {Array.<int>}
             * @default []
             */
            textSizes: [],
            /**
             * Index of the default text size used. Any change will move from there inside the array
             * @type {int}
             * @default 0
             */
            initTextSizeIndex: 0,
            /**
             * Should the close button of the UI call window.close() as well . Automatically set by build script
             * @type {boolean}
             */
            closeWindow: ("true" !== "true"),
            /**
             * For bubble UIs, defines the maximum height of the bubble
             * @type {int}
             * @default 140
             */
            maxHeightBubble: 140,
            /**
             * For bubble UIs, the height of the middle part of the bubble will be equal to the text height minus this value
             * @type {int}
             * @default 5 px
             */
            deltaMiddleBubble: 5,
            /**
             * Duration of the animation morphing the bubble from one size to another
             * @type {int}
             * @default 300 ms
             */
            speedAnimationBubble: 300,
            /**
             * Shall we empty the input upon form submission
             * @type {boolean}
             * @default true
             */
            emptyInputUponSubmission: true,
            /**
             * Array containing processors from window.Nina.text.Processors
             * @type {array}
             * @default []
             */
            textProcessorsList: [],
            /**
             * If the response contains the additional field "coBrowsingUrl" set from NIQS, automatically load it.
             */
            loadCoBrowsingUrl: true,
            /**
             * If "loadCoBrowsingUrl" is set to true, you can force the URL to open in a new window by also
             * setting "openCoBrowsingUrlInNewWindow" to true.
             * /!\ WARNING: this will do a window.open() when we receive the agent response, and will
             *              generally trigger the popup blocker of the user browser.
             */
            openCoBrowsingUrlInNewWindow: false,
            /**
             * If the UI is identified as a NinaChat UI, it will set "tap" as the datasource when a link is clicked.
             */
            ninaChatUi: false

        },
        uisync: {
            MissingInteractionsText: "You seem to have opened the virtual agent multiple times. Click <button type=\"button\" class=\"nw_TranscriptLink\">here</button> to synchronize your sessions"
        },
        notifications: {
            alwaysShowNotifications: false,
            displayTime: 3000,
            maxLength: 100,
            title: "Virtual Agent",
            icon: ""
        },
        /**
         * Parameters for the dominject module
         * @type window.Nina.config.DOMParams
         */
        dom: /** @lends window.Nina.config.DOMParams */ {
            /**
             * The HTML of the agent to inject into the hosting page
             * Set it to the HTML_AGENT sprocket variable to have the build script replace it with the value contained
             * in the agent.html file
             * @type {string}
             * @const
             */
            agentHTML: "",
            /**
             * The HTML of the minimized version of the agent for popins to inject into the hosting page.
             * Set it to the HTML_AGENT_MIN sprocket variable to have the build script replace it with the value contained
             * in the agent.html file
             * @type {string}
             * @const
             */
            agentMinHTML: ""
        },
        /**
         * Parameters for the popin module
         * @type window.Nina.config.PopinParams
         */
        popin: /** @lends window.Nina.config.PopinParams */ {
            /**
             * Is the popin resizable?
             * @type {boolean}
             * @default true
             */
            resizable: true,
            /**
             * Is the popin draggable?
             * @type {boolean}
             * @default true
             */
            draggable: true,
            /**
             * Should we change the opacity of the popin while moving it? WARNING, this can trigger weird bugs in older versions
             * of IE (6,7,8) where the input text box becomes unresponsive (actually applying CSS filters with a position fixed
             * div seems to be problematic)
             * @type {number|boolean}
             * @default false
             */
            opacity: false,
            /**
             * Minimum width of the popin in pixels
             * @type {int}
             * @default 200
             */
            minWidth: 200,
            /**
             * Minimum height of the popin in pixels
             * @type {int}
             * @default 300
             */
            minHeight: 300,
            /** Maximum width of the popin in pixels
             * @type {int}
             * @default 500
             */
            maxWidth: 500,
            /** Maximum width of the popin in pixels
             * @type {int}
             * @default 600
             */
            maxHeight: 600,
            /**
             * The initial position of the popin when opening and we have no previous position stored.
             * The CSS should still contain some position information
             * It contains 3 properties: "what", "x", "y".
             * "what" indicates where from the screen we position relative to (left/middle/right top/middle/bottom)
             * "x" indicates the x offset from the considered base location
             * "y" indicates the y offset from the considered base location
             * @type {object}
             */
            posPopin: {
                what: "right bottom",
                x: -100,
                y: -100
            },
            /**
             * The position of the minimized popin
             * The CSS should still contain some position information
             * It contains 3 properties: "what", "x", "y".
             * "what" (string) indicates where from the screen we position relative to (left/middle/right top/middle/bottom)
             * "x" (int) indicates the x offset from the considered base location
             * "y" (int) indicates the y offset from the considered base location
             * @type {object}
             */
            posMin: {
                what: "right bottom",
                x: -10,
                y: -10
            },
            /**
             * Do we delete the session when we close the popin?
             * @type {boolean}
             * @default false
             */
            deleteSessionOnClose: false,
            /**
             * Configure if the default collapse/expand actions are animated or not.
             */
            animated: true
        },
        /**
         * Parameters for the beam module
         * @type window.Nina.config.PopinParams
         */
        beam: /** @lends window.Nina.config.PopinParams */ {
            /**
             * Weither the beam UI has a fixed position or stays at the top of the document.
             * @type {boolean}
             */
            fixedMode: true,
            /**
             * If true, the beam UI will be displayed at the bottom of the document.
             * @type {boolean}
             */
            bottom: false
        },
        /**
         * Parameters for the survey module (survey)
         * @type window.Nina.config.SurveyParams
         */
        survey: {
            bannerAnimation: "slide", // slide | fade | show
            bannerFadesAfter: 5,
            verbatimMaxLength: 400
        },
        /**
         * Parameters for the web service modules (jbotservice, jtranscriptservice and jqualification)
         * @type window.Nina.config.WSParams
         */
        ws: /** @lends window.Nina.config.WSParams */{
            /**
             * Set this to true if you want to send requests using JSONP instead of POST.
             */
            useJSONP: false,
            /**
             * The details for the preprod mode
             * It contains 2 properties "base_url" and "debug"
             * "base_url" (string) contains the directory of the webservice (before jbotservice.asmx). Must end with a trailing /
           * "use_smart_router" (boolean) defines if the UI will connect to the smart-router
           * "smart_router_endpoint" The address of the smart-router, e.g.: "http://smartrouter.dev.local:7768/ui/pikachu"
           * "sr_agent_endpoint" The agent namespace on which the UI will talk, e.g.: "agent/White-Rabbit-EnglishUS"
             * "debug" (boolean) defines if we'll call the web service in debug mode (more information returned)
             */
            preprod: {
                base_url: "",
                houstonURL: "https://agent-preprod.nuance-va.com/houston/houston.html",
                houstonURL_IE89: "https://agent-preprod.nuance-va.com/houston/houston_ie89.js",
                use_smart_router: false,
                smart_router_endpoint: "",
                sr_agent_endpoint: "",
                debug: true
            },

            prod: {
                base_url: "",
                houstonURL: "https://agent.nuance-va.com/houston/houston.html",
                houstonURL_IE89: "https://agent.nuance-va.com/houston/houston_ie89.js",
                use_smart_router: false,
                smart_router_endpoint: "",
                sr_agent_endpoint: "",
                debug: false
            },
            apiVersion: 1.1,
            /**
             * # of seconds to wait before considering the service timed out
             * @type {int}
             * @default 10 seconds
             */
            timeoutQuery: 10,
            /**
             * # of seconds before retrying polling the web service when in livechat mode and running into an error
             * @type {int}
             * @default 5 seconds
             */
            livechatRetryDelay: 5,
            /**
             * Maximum # of poll retries in livechat mode after getting an error
             * @type {int}
             */
            livechatMaxErrors: 16,
            /**
             * Message to display after the service timed out or the response is invalid
             * @type {string}
             * @default Error while contacting the service
             */
            errorMessage: "Error while contacting service",
            /**
             * Message to display when the query URL is too long (> 2048) and cannot be sent
             * @type {string}
             * @default Error request URL too long
             */
            urltoolong: "Error request URL too long",
            /**
             * Message to display when too many pending queries have accumulated
             * @type {string}
             * @default Too many pending queries
             */
            tooManyQueries: "Too many pending queries",
            /**
             * Maximum # of pending queries. If the UI tries to send another query, the system will return an error message
             * @type {int}
             * @default 16
             */
            maxQueries: 16,
            /**
             * Should we use the persistent cookie and send its content to the service
             * @type {boolean}
             * @default false
             */
            usePersistent: false,
            /**
             * Map to replace interactions in the Transcript ("" will remove the Q&A).
             * @type {object}
             * @default {}
             *
             */
            transcriptFilter: {
                "##SkipAlyze#": "(Command sent)"
            },
            /**
             * The transcriptFilter above replaces the whole sentence if it contains the filtered string.
             * This filter will replace only the matched part of the sentence.
             * It allows the use of regexs. For each rule, it will do on each human sentence:
             * humanSentence = humanSentence.replace(new RegExp("toReplace", "g"), "replacingString");
             */
            transcriptRegexReplaceFilter: {
                "^Click URL: ": "(Click) ",
                "^Click: ": "(Click) "
            },
            /**
             * Permit to ask the WBH to add the NLE results in the response (if enabled in the agent config).
             */
            nleResults: false
        },
        avatar: {
            init: "",
            loop: "",
            staticImage: "",
            frameRate: 12.0,
            /* To move the clips inside avtLoader */
            clipPosition: [0, 0],
            mapAnim: {},
            /**
             * Should we replace the flash avatar with a static image on mobile devices? (FYI, even when flash is enabled, transparent animations are not supported)
             * @type (boolean)
             * @default false
             */
            staticOnMobileDevices: false
        },
        url: {
            /**
             * Global default that governs whether the UI will follow urls supplied by the agent in the event that the
             * agent doesn't explicitly tell the UI what to do.
             *
             * This property works for 5.x agents only.  For 6.x agents and greater, the "out url" link is always
             * followed.
             */
            honorOutUrl: true,
            additionalTargets: {}
        },
        webtrends: {
            pageDataFeed: null
        }
    };
    return window.Nina.$.extend(true, defaultConfig, agentConfig);
};


window.Nina.enums.Author = {
    CSR: "AuthorCSR",
    Agent: "AuthorAgent",
    User: "AuthorUser",
    System: "AuthorSystem",
    Error: "AuthorError"
};

window.Nina.enums.Command = {
    LivechatPoll: "##LiveChatPoll",
    LivechatEnd: "##SkipAlyze#LiveChatEndConversation",
    DialogFollowUp: "##FollowUp",
    DialogTrackUrl: "##Url#",
    DialogFirstInit: "##FirstInit",
    DialogTimeOut: "##TimeOut",
    DialogRemark: "##Remark#",
    DialogContinue: "##SkipAlyze#Continue",
    DialogFormData: "##SkipAlyze#FormData",
    DialogSkipAlyze: "##SkipAlyze#",
    DialogBack: "##Back",
    DialogVersion: "##Version",
    SendParams: "##SkipAlyze#Params",
    StartSurvey: "##SkipAlyze#StartSurvey",
    AbortSurvey: "##SkipAlyze#AbortSurvey"
};


window.Nina.enums.ResponseCode = {
    Found: "Found",
    Default: "Default",
    Command: "Command",
    EmptyAnswer: "EmptyAnswer",
    IgnoreInit: "IgnoreInit",
    Simple: "Simple",
    InvalidSession: "InvalidSession",
    ErrorRouterException: "ErrorRouterException",
    ErrorMaxSessions: "ErrorMaxSessions",
    ErrorInputTooLong: "ErrorInputTooLong",
    ErrorInvalidContext: "ErrorInvalidContext",
    ErrorAgentException: "ErrorAgentException",
    ErrorNoOutput: "ErrorNoOutput",
    ErrorBack: "ErrorBack",
    ErrorInvalidSession: "ErrorInvalidSession",
    ErrorUnknownCommand: "ErrorUnknownCommand",
    ErrorDisabledCommand: "ErrorDisabledCommand",
    ErrorConcurrentQueries: "ErrorConcurrentQueries",
    ErrorInputEmpty: "ErrorInputEmpty"
};


window.Nina.enums.LivechatStatus = {
    NotStarted: 0,
    Waiting: 1,
    Ongoing: 2,
    Ended: 3
};

window.Nina.enums.serviceReponseNames = {
    talkAgentResponse: "TalkAgentResponse",
    postChatSurveyUrl: "PostChatSurvey",
    SCI: "@SCI",
    IID: "@IID",
    responseCode: "@ResponseCode",
    display: "Display",
    CSRIsWriting: "CSRIsWriting",
    CSRName: "CSRName",
    livechatId: "LiveChatId",
    livechatStatus: "LiveChatStatus",
    livechatConversation: "LivechatConversation",
    source: "Source",
    timetowaitbeforepoll: "TimeToWaitBeforePoll"


};

window.Nina.enums.CSS = {
    inputForm: "nw_Input",
    inputText: "nw_UserInputField",
    inputButton: "nw_UserSubmit",
    inputFocus: "nw_UserInputFocus",
    chat: "nw_Conversation",
    chatText: "nw_ConversationText",
    statusMessage: "nw_StatusMessage",
    lastUserQuery: "nw_LastUserQuery",
    closeButton: "nw_CloseX",
    handleDrag: "nw_HandleDrag",
    minimize: "nw_Minimize",
    maximize: "nw_Maximize",
    transcriptLink: "nw_TranscriptLink",
    authorCSR: "nw_CsrSays",
    authorUser: "nw_UserSays",
    authorSystem: "nw_SystemSays",
    authorAgent: "nw_AgentSays",
    authorError: "nw_ErrorSays"
};

window.Nina.enums.resizeHandles = {
    n: ".nw_HandleN",
    e: ".nw_HandleE",
    s: ".nw_HandleS",
    w: ".nw_HandleW",
    ne: ".nw_HandleNE",
    nw: ".nw_HandleNW",
    se: ".nw_HandleSE",
    sw: ".nw_HandleSW"
};

window.Nina.enums.sessionCookiesProperties = {
    x: "popinX",
    y: "popinY",
    state: "popinState",
    w: "popinW",
    h: "popinH",
    lcstat: "lcstat",
    cont: "cont",
    sci: "sci",
    iid: "iid",
    lastInt: "lastInt"
};

window.Nina.enums.uiEvents = {
    /* popin.js */
    popinShow: "nina-popin-show",
    popinHide: "nina-popin-hide",
    popinMinimize: "nina-popin-minimize",
    /* ui.js */
    uiClose: "nina-ui-close",
    surveyOpened: "nina-survey-opened",
    surveyClosed: "nina-survey-closed",
    surveyQuestionDisplayed: "nina-survey-question-displayed",
    surveyQuestionSent: "nina-survey-question-sent"
};

window.Nina.text.Processors = (function () {
    "use strict";
    var $ = window.Nina.$;

    function ProcessorWhiteSpace(input) {
        if (typeof input !== "string") {
            return "";
        }
        var pattern = /^[ \t]+|[ \t]+$/gi;
        var result = input;
        result = result.replace(pattern, "");
        return result;
    }
    function ProcessorPunctSpace(input) {
        if (typeof input !== "string") {
            return "";
        }
        var pattern = /( +?)(\?|\!|\.|,|:|;)/gi;
        var result = input;
        result = result.replace(pattern, "$2");
        return result;
    }
    function ProcessorBreakLines(input) {
        if (typeof input !== "string") {
            return "";
        }
        /* first converts all types of end of line markers to <br />
		the eliminates any <br /> tags at the start and end of the string
		*/
        var pattern = /((\r|\n)+ ?(\n|\r)+)|(\r)|(\n)/gi;
        var result = input;
        result = result.replace(pattern, "<br />");
        pattern = /^(<\/? ?br>|<br ?\/>)+|(<\/? ?br>|<br ?\/>)+$/gi;
        result = result.replace(pattern, "");
        return result;
    }
    function ProcessorLists(input) {
        if (typeof input !== "string") {
            return "";
        }
		/* first it looks for list patterns through the string (assuming there can be
		more than one list in the text
		then it processes each list candidate and replaces it with its html list
		equivalent.
		*/
        var pattern;
        var result = input;
        var matches = [];
        var processedMatches = [];

        pattern = /(\r|\n|\r\n)?^- ?(.+?)(\r\n|\r|\n)(- ?(.+?)(\r\n|\r|\n|$)){0,}/gim;
        result = input;
        matches = result.match(pattern);
        if (matches) {
            for (var i = 0; i < matches.length; i++) {
                var item = matches[i];
                item = item.replace(/- ?(.+)?/gi, "<li>$1</li>");
                item = item.replace(/(\r\n|\r|\n)/gi, "");
                processedMatches.push(item);
            }
            for (var j = 0; j < matches.length; j++) {
                result = result.replace(matches[j], "<ul>" + processedMatches[j] + "</ul>");
            }
        }
        return result;
    }
    function ProcessorScriptTags(input) {
        if (typeof input !== "string") {
            return "";
        }
        var patternScriptOpen = /<script[^>]*>/gim;
        var patternScriptClose = /<\/script[ ]*>/gim;

        return input.replace(patternScriptOpen, "").replace(patternScriptClose, "");
    }
    function ProcessorEscapeInputText(input) {
        if (typeof input !== "string") {
            return "";
        }

        return input.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    }
    function Capitalize(input) {
        return input.toLowerCase().replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase(); });
    }
    // The following processor is a temporary workaround for 6.0 agents.
    // Links are in the form: <a href="#" target="_blank" data-vtz-browse="http://google.com"...
    // so it's impossible to open them in a new tab using right/wheel click.
    // Until it's fixed in NIQS, we replace "#" with the real link
    function CreateRealLinks(input) {
        var inputAsDom = $('<div>' + input + '</div>');
        inputAsDom.find('a').each(function () {
            if ($(this).attr('data-vtz-browse')) {
                $(this).attr('href', $(this).attr('data-vtz-browse'));
            }
        });
        return inputAsDom.html();
    }
    return {
        getWhiteSpaceProcessor: function () { return ProcessorWhiteSpace; },
        getPunctSpaceProcessor: function () { return ProcessorPunctSpace; },
        getBreakLinesProcessor: function () { return ProcessorBreakLines; },
        getScriptTagsProcessor: function () { return ProcessorScriptTags; },
        getEscapeInputTextProcessor: function () { return ProcessorEscapeInputText; },
        getCapitalizeInputTextProcessor: function () { return Capitalize; },
        getListProcessor: function () { return ProcessorLists; },
        getCreateRealLinksProcessor: function () { return CreateRealLinks; },
        applyProcessors: function (input, processors) {
            if (!processors || processors.length === 0) {
                return input;
            }
            var result = input;
            for (var i = 0; i < processors.length; i++) {
                result = processors[i](result);
            }
            return result;
        }
    };
})();
window.Nina.storage.newCookiesJar = function newCookiesJar(_agentId, _config) {
    "use strict";
    var $ = window.Nina.$;
    /** @private */
    var date = new Date();
    var me = {};
    /** @const **/
    me.lifeTimePersist = getCookie("nina-opt-out") ? "session" : _config.lifeTimePersist;
    /** @const **/
    me.domainLevel = _config.domainLevel;
    /** @const **/
    me.restrictDomain = _config.restrictDomain ? false : getDomain(document.location.hostname);
    /** @const **/
    me.restrictPath = _config.restrictPath ? false : "/";
    /** @const **/
    me.secure = _config.secure && document.location.protocol === "https:";
    /** @const **/
    me.timeoutInteraction = _config.timeoutInteraction;
    /** @const **/
    me.timeoutInteractionLiveChat = _config.timeoutInteractionLiveChat;
    /** @const **/
    me.lifeTimeSession = "session";
    /** @const **/
    me.lifeTimePostQualif = getCookie("nina-opt-out") ? "session" : _config.lifeTimePostQualif;

    /** @const **/
    me.agentId = _agentId;
    /** @const **/
    me.uiID = date.getTime() * Math.random() + date.getTime();
    /** @const **/
    me.preprodCookieName = "preprod";
    /** @const **/
    me.sciCookieName = "sci";
    /** @const **/
    me.uiIDCookieName = "uiID";
    /** @const **/
    me.lastInteractionCookieName = "lastInt";
    /** @const **/
    me.sessionCookieName = "Nina-" + me.agentId + "-session";
    /** @const **/
    me.persistentCookieName = "Nina-" + me.agentId + "-persist";
    /** @const **/
    me.postQualifCookieName = "Nina-" + me.agentId + "-postQualif";

    if (window.NinaVars.hasOwnProperty(me.preprodCookieName)) {
        set(me.sessionCookieName, me.preprodCookieName, window.NinaVars.preprod, me.lifeTimeSession);
    }

    var preprod = get(me.sessionCookieName, me.preprodCookieName);
    if (preprod !== null) {
        window.NinaVars.preprod = window.NinaVars.preprod || preprod;
    }

    // Disabled for now
    // TODO: figure out if overwriting the onbeforeunload callback is ok or not. It can impact other handlers ...
    // and it seems that bind("beforeunload", function() {}) does not work well in all cases
    var uiID = get(me.sessionCookieName, me.uiIDCookieName);
	/*
	if(!uiID || uiID == me.uiID) {
		// We're the only UI, set our own uiID
		set(me.sessionCookieName, me.uiIDCookieName,me.uiID,me.lifeTimeSession);
	}

	window.onbeforeunload = function(e) {
		if(isMainUI()) {
			remove(me.sessionCookieName, me.uiIDCookieName,me.lifeTimeSession);
		}
		return;
	};
    */

    function isMainUI() {
        var uiID = get(me.sessionCookieName, me.uiIDCookieName);
        return !uiID || me.uiID === uiID;
    }

    /**
     * returns the domain name from a host name
     * @param {string} hostname
     * @returns {string}
     */
    function getDomain(hostname) {
        // Regex that will match an IP
        var ipRegex = /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/;
        if (ipRegex.test(hostname)) {
            return false;
        }

        var i = me.domainLevel;
        var idx = hostname.length;
        while (i > 0) {
            idx = hostname.lastIndexOf(".", idx - 1);
            if (idx === -1) {
                return false;
            }
            i--;
        }
        return hostname.substring(idx); // We're supposed to keep the leading . when this is not an FQDN
    }

    /**
     * Retrieve the content of a cookie by name.
     * @param {string} cookieName
     * @returns {string}
     */
    function getCookie(cookieName) {
        /*only cookies for this domain and path will be retrieved */
        var cookieJar = document.cookie.split("; ");
        for (var x = 0; x < cookieJar.length; x++) {
            var split = cookieJar[x].indexOf('=');
            var name = cookieJar[x].substring(0, split);
            if (name === encodeURIComponent(cookieName)) { return decodeURIComponent(cookieJar[x].substring(split + 1)); }
        }
        return null;
    }

    /**
     * Set a cookie to a certain value with a given lifetime, path, domain and secure attribute
     * @param {string} cookieName
     * @param {string} cookieValue
     * @param {(string|number)} lifeTime (in ms)
     * @param {string} path
     * @param {string} domain
     * @param {boolean} isSecure
     * @returns {boolean}
     */
    function setCookie(cookieName, cookieValue, lifeTime, path, domain, isSecure) {
        if (!cookieName) { return false; }
        document.cookie = encodeURIComponent(cookieName) + "=" + encodeURIComponent(cookieValue) +
            (path ? ";path=" + path : "") + (domain ? ";domain=" + domain : "") +
            (isSecure ? ";secure" : "");
        return (typeof getCookie(cookieName) === "string");
    }

    /**
     * Retrieves a property from the JSON obect stored inside the cookie
     * @param {string} cookieName
     * @param {string} name
     * @returns {string}
     */
    function get(cookieName, name) {
        var coVal = getCookie(cookieName);
        var objVal;
        try {
            objVal = JSON.parse(coVal);
        } catch (e) {
            objVal = null;
        }
        if (objVal !== null && objVal.hasOwnProperty(name)) {
            return objVal[name];
        }
        if (name === 'sci') {
            return window.Nina.storage.sci;
        }
        return null;
    }

    /**
     * Sets a property to a certain value in the JSON object stored inside the cookie
     * @param {string} cookieName
     * @param {object} pairs
     * @param {(string|number)} lifetime
     */
    function set(cookieName, pairs, lifetime) {
        var coVal, objVal;
        if (typeof cookieName !== "string" || typeof pairs !== "object") {
            return false;
        }
        coVal = getCookie(cookieName);
        try {
            objVal = JSON.parse(coVal);
        } catch (e) {
            objVal = null;
        }
        if (objVal === null) {
            objVal = {};
        }

        for (var name in pairs) {
            if (pairs.hasOwnProperty(name)) {
                objVal[name] = pairs[name];
                if (name === "sci") {
                    window.Nina.storage.sci = pairs[name];
                }
            }
        }

        coVal = JSON.stringify(objVal);

        // Uses 4000 as the limit because the 4096 limit includes the name of the cookies (sometimes the = sign as well) and the expires & co directives
        // This code is here just to have a coherent return value. Trying to set a cookie with a string that's too large results in the value being discarded
        // but any existing value of the cookie would be left untouched
        if (coVal.length > 4000) {
            return false;
        }

        return setCookie(cookieName, coVal, lifetime, me.restrictPath, me.restrictDomain, me.secure);
    }

    function remove(cookieName, name, lifetime) {
        var coVal = getCookie(cookieName);
        var objVal;
        try {
            objVal = JSON.parse(coVal);
        } catch (e) {
            objVal = null;
        }
        if (objVal === null) {
            return;
        }

        if (objVal.hasOwnProperty(name)) {
            delete objVal[name];
        }
        coVal = JSON.stringify(objVal);
        if (coVal.length > 4000) {
            return false;
        }
        return setCookie(cookieName, coVal, lifetime, me.restrictPath, me.restrictDomain, me.secure);
    }

    /**
     * Invalidates a cookie
     * @name clear
     * @private
     * @param {string} cookieName
     * @returns {boolean}
     */
    function clear(cookieName) {
        return setCookie(cookieName, null, -1, me.restrictPath, me.restrictDomain, me.secure);
    }

    return /** @lends window.Nina.storage.CookiesJar.prototype */ {
        /**
         * Sets the name property of the session cookie to value
         * @param {string} name
         * @param {Object} value
         * @returns {boolean}
         *
         * use: setSession("sci", 123456);
         * use: setSession({'sci': 123456, 'lastInt': 123456789, 'preprod': true});
         */
        setSession: function () {
            var obj = {};
            if (arguments.length === 1 && typeof arguments[0] === "object") {
                obj = arguments[0];
            } else if (arguments.length === 2 && typeof arguments[0] === "string") {
                obj[arguments[0]] = arguments[1];
            } else {
                return false;
            }
            return set(me.sessionCookieName, obj, me.lifeTimeSession);
        },
        /**
         * Gets the value of the name property of the session cookie
         * @param {string} name
         * @returns {Object}
         */
        getSession: function (name) {
            return get(me.sessionCookieName, name);
        },
        /**
         * Deletes a property from the session cookie
         * @param name
         * @returns {boolean}
         */
        removeSession: function (name) {
            return remove(me.sessionCookieName, name, me.lifeTimeSession);
        },
        /**
         * Clears the session cookie
         * @returns {boolean}
         */
        clearSession: function () {
            return clear(me.sessionCookieName);
        },
        /**
         * Sets the name property of the persistent cookie to value
         * @param {string} name
         * @param {Object} value
         * @returns {boolean}
         */
        setPersist: function () {
            var obj = {};
            if (arguments.length === 1 && typeof arguments[0] === "object") {
                obj = arguments[0];
            } else if (arguments.length === 2 && typeof arguments[0] === "string") {
                obj[arguments[0]] = arguments[1];
            } else {
                return false;
            }
            return set(me.persistentCookieName, obj, me.lifeTimePersist);
        },
        /**
         * Gets the value of the name property of the persistent cookie
         * @param {string} name
         * @returns {Object}
         */
        getPersist: function (name) {
            return get(me.persistentCookieName, name);
        },
        /**
         * Set a cookie for postQualification purpose
         * @param name
         * @return {*}
         */
        setPostQualif: function () {
            var sci = this.getSession("sci");
            return setCookie(me.postQualifCookieName, sci, me.lifeTimePostQualif, me.restrictPath, me.restrictDomain, me.secure);
        },
        /**
         * Deletes a property from the session cookie
         * @param name
         * @returns {boolean}
         */
        removePersist: function (name) {
            return remove(me.persistentCookieName, name, me.lifeTimePersist);
        },
        /**
         * Clears the persistent cookie
         * @returns {boolean}
         */
        clearPersist: function () {
            return clear(me.persistentCookieName);
        },
        /**
         * Reads a raw cookie value. Helper function to access other cookies
         * @param {String} name the name of the cookie
         * @returns {string}
         */
        readCookie: function (name) {
            return getCookie(name);
        },
        /**
         * Sets a property to a certain value in the JSON object stored inside the cookie
         * @param {string} cookieName
         * @param {object} pairs
         * @param {(string|number)} lifetime
         */
        setCookie: function (cookieName, pairs, lifetime) {
            return set(cookieName, pairs, lifetime);
        },
        /**
         * Returns whether the current context id is still valid or if it has timed out
         * @returns {boolean}
         */
        isOngoingSession: function () {
            var sci = get(me.sessionCookieName, me.sciCookieName);
            var lastInt = get(me.sessionCookieName, me.lastInteractionCookieName);
            var lcstat = get(me.sessionCookieName, "lcstat");
            var timeout = (lcstat === true) ? me.timeoutInteractionLiveChat : me.timeoutInteraction;

            return !(sci === null || sci === "" || lastInt === null || ((new Date()).getTime() - lastInt) > timeout);
        },
        sessionCookieName: me.sessionCookieName,
        persistentCookieName: me.persistentCookieName,
        postQualifCookieName: me.postQualifCookieName,
        /**
         * Returns whether the current instance of the UI is a main UI (only UI opened or "first" UI opened
         * @function
         * @returns {boolean}
         */
        isMainUI: isMainUI,
        uiID: me.uiID
    };
};

(function (getParams) {
    "use strict";
    var params = {};
    var qs = getParams;
    if (qs.length === 0) {
        return params;
    }
    qs = qs.replace(/\+/g, ' ');
    var args = qs.split('&');
    for (var i = 0; i < args.length; i++) {
        var pair = args[i].split('=');
        var name = decodeURIComponent(pair[0]);
        window.NinaVars[name] = (pair.length === 2) ? decodeURIComponent(pair[1]) : name;
    }
})(document.location.search.substring(1, document.location.search.length));
window.Nina.ws.newQuery = function newQuery(_sci, _iid, _userText, _additionalData) {
    "use strict";
    var me = {};
    me["@SCI"] = _sci;
    me["@IID"] = _iid;
    me["@TimeStamp"] = window.Nina.helper.date.toISOString(new Date());
    me.UserText = _userText;

    for (var k in _additionalData) {
        if (k !== "@xmlns" && k !== "@SCI" && k !== "@IID" && k !== "UserText") {
            me[k] = _additionalData[k];
        }
    }

    return /** @lends window.Nina.ws.Query.prototype */ {
        /**
         * Returns the JSON string representation of the Query object
         * @returns {string}
         */
        toJSON: function () {
            return JSON.stringify(me);
        },
        /**
         * Returns current SCI.
         */
        getSCI: function () {
            return me["@SCI"];
        },
        /**
         * Returns current IID.
         */
        getIID: function () {
            return me["@IID"];
        },
        /**
         * Replaces SCI if the query was queued while waiting for the first answer.
         */
        setSCI: function (_sci) {
            me["@SCI"] = _sci;
        },
        /**
         * Replaces IID if the query was queued while waiting for the first answer.
         */
        setIID: function (_iid) {
            me["@IID"] = _iid;
        }
    };
};

/**
 * Creates a new Interaction from the JON response from the dialog web service
 * @param data {Object} the dialog web service response
 * @returns {window.Nina.ws.Interaction}
 */
window.Nina.ws.newInteraction = function newInteraction(data) {
    "use strict";
    var me = {};
    me.responseCode = "MissingResponseCode";
    me.sci = "";
    me.iid = "";
    me.author = window.Nina.enums.Author.Agent;
    me.text = "";
    me.gui = "";
    me.loop = "";
    me.url = "";
    me.timeout = "";
    me.timestamp = "";
    me.options = [];
    me.rewords = [];
    me.nleResults;
    me.sessionBootstrap;
    me.additionalData = {};
    me.engine = "default";

    me.livechatConversation = false;
    me.livechatStatus = 0;
    me.livechatPollTime = 0;
    me.livechatCSRName = "";
    me.livechatCSRIsWriting = false;
    me.livechatId = "";
    me.livechatInteractionIndex = null;

    var mandatoryFields = 0;

    if (typeof data !== 'object' || !data.hasOwnProperty("TalkAgentResponse")) {
        me.responseCode = "InvalidResponse";
        me.text = "Invalid response from service";
        me.author = window.Nina.enums.Author.Error;
    } else {
        var response = data.TalkAgentResponse;
        for (var k in response) {
            if (response.hasOwnProperty(k)) {
                switch (k) {
                    case "@SCI":
                        me.sci = response[k];
                        mandatoryFields++;
                        break;
                    case "@IID":
                        me.iid = response[k];
                        mandatoryFields++;
                        break;
                    case "@ResponseCode":
                        me.responseCode = response[k];
                        mandatoryFields++;
                        break;
                    case "@TimeStamp":
                        me.timestamp = response[k];
                        break;
                    case "@Version":
                        me.engine = response[k];
                        break;
                    case "Display":
                        processDisplayNode(response[k]);
                        mandatoryFields++;
                        break;
                    case "CSRIsWriting":
                        me.livechatCSRIsWriting = (removeNameSpaceAttributes(response[k]) === "true");
                        break;
                    case "CSRName":
                        me.livechatCSRName = removeNameSpaceAttributes(response[k]);
                        break;
                    case "LiveChatId":
                        me.livechatId = removeNameSpaceAttributes(response[k]);
                        break;
                    case "LiveChatStatus":
                        me.livechatStatus = parseInt(removeNameSpaceAttributes(response[k]), 10);
                        break;
                    case "LivechatConversation":
                        // WBH must see LivechatConversation to be in livechat mode (otherwise regular timeouts apply)
                        me.livechatConversation = (removeNameSpaceAttributes(response[k]) === "true" ||
                            removeNameSpaceAttributes(response[k]) === true);
                        break;
                    case "LivechatInteractionIndex":
                        var index = parseInt(removeNameSpaceAttributes(response[k]), 10);
                        if (!isNaN(index)) {
                            me.livechatInteractionIndex = index;
                        }
                        break;
                    case "Source":
                        processLivechatSource(removeNameSpaceAttributes(response[k]));
                        break;
                    case "TimeToWaitBeforePoll":
                        me.livechatPollTime = parseInt(removeNameSpaceAttributes(response[k]), 10);
                        break;
                    case "TimeOut":
                        me.timeout = parseInt(removeNameSpaceAttributes(response[k]), 10);
                        break;
                    // The serialization use a different mechanism for the 4 following fields, so no namespace to remove.
                    case "OutOptions":
                        me.options = response[k].options;
                        break;
                    case "OutRewords":
                        me.rewords = response[k].rewords;
                        break;
                    case "nleResults":
                        me.nleResults = response.nleResults;
                        break;
                    case "sessionBootstrap":
                        me.sessionBootstrap = response.sessionBootstrap;
                        break;
                    default:
                        processAdditionalNode(k, response[k]);
                }
            }
        }
    }

    if (mandatoryFields < 5) {
        me.author = window.Nina.enums.Author.Error;
    }

    // Replace any occurence of #FLASH_CID# by the actual context id
    me.text = insertContextId(me.text);
    me.url = insertContextId(me.url);

    /**
     * Takes text and replaces any occurences of #FLASH_CID# with the actual interaction context id.
     * @param {string} text
     * @returns {string}
     */
    function insertContextId(text) {
        return text.replace("#FLASH_CID#", me.sci);
    }

    /**
     * Sets the author property based on the livechat source field of the service response
     * @param {string} source
     * @returns {void}
     */
    function processLivechatSource(source) {
        switch (source) {
            case "System":
                me.author = window.Nina.enums.Author.System;
                break;
            case "CSR":
                me.author = window.Nina.enums.Author.CSR;
                break;
            default:
        }
    }

    /**
     * Iterates over the properties of the display node and extract the values of the Loop, OutGui, OutText, TimeOut and OutUrl parameters.
     * @param {Object} display
     * @returns {void}
     */
    function processDisplayNode(display) {
        for (var k in display) {
            if (display.hasOwnProperty(k)) {
                switch (k) {
                    case "Loop":
                        me.loop = removeNameSpaceAttributes(display[k]);
                        break;
                    case "OutGui":
                        me.gui = removeNameSpaceAttributes(display[k]);
                        break;
                    case "OutText":
                        me.text = removeNameSpaceAttributes(display[k]);
                        mandatoryFields++;
                        break;
                    case "AlternateOutText":
                        me.alternateText = removeNameSpaceAttributes(display[k]);
                        break;
                    case "AlternateOutText2":
                        me.alternateText2 = removeNameSpaceAttributes(display[k]);
                        break;
                    case "TimeOut":
                        me.timeout = removeNameSpaceAttributes(display[k]);
                        break;
                    case "OutUrl":
                        me.url = removeNameSpaceAttributes(display[k]);
                        break;
                    default:
                }
            }
        }
    }

    /**
     * Add an additional data field to the response except if it is a namespace attribute
     * @param {string} name
     * @param {Object} object
     * @returns {void}
     */
    function processAdditionalNode(name, object) {
        // Xml namespace stuff, we don't care about
        if (name.indexOf("@xmlns") === 0) {
            return;
        }
        me.additionalData[name] = removeNameSpaceAttributes(object);
    }

    /**
     * Process a response node and remove any namespace attribute
     * @param {Object} node
     * @returns {(Object|string)}
     */
    function removeNameSpaceAttributes(node) {
        // This function looks for any xml namespace related attribute and remove them.
        // Additionally it will convert ##text node to a simple value if no other attribute is left
        if (typeof node === 'object') {
            if (window.Nina.$.isArray(node)) {
                // This is an Array
                window.Nina.$.each(node, function (index, value) {
                    node[index] = removeNameSpaceAttributes(value);
                });
            } else if (node !== null) {
                // This is an associative array
                var hasHHText = false;
                var count = 0;

                for (var k in node) {
                    if (node.hasOwnProperty(k)) {
                        count++;

                        if (k.indexOf("@xmlns") === 0 || k.indexOf("@TempActiv") === 0) {
                            delete node[k];
                            count--;
                        } else if (k.indexOf("#text") === 0) {
                            hasHHText = true;
                        } else {
                            node[k] = removeNameSpaceAttributes(node[k]);
                        }
                    }
                }

                // It's a simple value so let's make it so.
                if (hasHHText && count === 1) {
                    node = node["#text"];
                }

                if (count === 0) {
                    node = "";
                }
            }

        }
        return node;
    }

    return /** @lends window.Nina.ws.Interaction.prototype */ {
        /**
         * Returns the response code of the response.
         * @returns {String}
         */
        getResponseCode: function () { return me.responseCode; },
        /**
         * Returns the context id of the interaction
         * @returns {string}
         */
        getSCI: function () { return me.sci; },
        /**
         * Returns the interlocutor id of the interaction
         * @returns {string}
         */
        getIID: function () { return me.iid; },
        /**
        * Returns the timestamp attribute if present
         * @returns {string}
        */
        getTimeStamp: function () { return me.timestamp; },
        /**
         * Returns the author of the interaction
         * @returns {window.Nina.enums.Author}
         */
        getAuthor: function () { return me.author; },
        /**
         * Returns the HTML text of the interaction
         * @returns {string}
         */
        getResponse: function () { return me.text; },
        /**
         * Returns an alternate version of the response, if any
         * @returns {string}
         */
        getAlternateResponse: function () { return me.alternateText; },
        /**
         * Returns an other alternate version of the response, if any
         * @returns {string}
         */
        getAlternateResponse2: function () { return me.alternateText2; },
        /**
         * Returns the extracted options, without html,
         * when the alternateOutText feature is activated.
         * @returns {array of string}
         */
        getOutOptions: function () { return me.options; },
        /**
         * Returns the extracted rewords, without html,
         * when the alternateOutText feature is activated.
         * @returns {array of string}
         */
        getOutRewords: function () { return me.rewords; },
        /**
         * Returns the NLE results, if any
         * @returns {array of NLE results}
         */
        getNleResults: function () { return me.nleResults; },
        /**
         * Returns the session bootstrap object, if activated
         * @returns {object}
         */
        getSessionBootstrap: function () { return me.sessionBootstrap; },
        /**
        * Returns the OutGui of the interaction
        * @returns {string}
        */
        getGUI: function () { return me.gui; },
        /**
         * Returns the OutUrl of the interaction
         * @returns {string}
         */
        getOutUrl: function () { return me.url; },
        /**
         * Returns the OutUrl metadata contained in the Loopback.OutUrl additional data property
         */
        getOutUrlMetadata: function () {
            // in 6.0 agent, loopback information is not in a Loopback.OutUrl node anymore.
            if (me.engine === 'default') {
                return me.additionalData.hasOwnProperty("Loopback") && me.additionalData.Loopback.hasOwnProperty("OutUrl") ? me.additionalData.Loopback.OutUrl : {};
            } else if (+me.engine >= 6) {
                return me.additionalData.hasOwnProperty("Loopback") ? me.additionalData.Loopback : {};
            }
        },
        /**
         * Returns the Loop field of the interaction
         * @returns {string}
         */
        getLoop: function () { return me.loop; },
        /**
         * Returns the TimeOut of the interaction
         * @returns {string}
         */
        getTimeOut: function () { return me.timeout; },
        /**
         * Returns the additional data (non standard fields) contained in the interaction
         * @returns {string}
         */
        getAdditionalData: function () { return me.additionalData; },
        /**
         * Returns whether the current interaction is part of a livechat conversation or a regular virtual agent chat.
         * @returns {boolean}
         */
        isLivechatConversation: function () { return me.livechatConversation; },
        /**
         * Returns the livechat status of the interaction
         * @returns {number}
         */
        getLivechatStatus: function () { return me.livechatStatus; },
        /**
         * Returns the livechat status of the interaction
         * @returns {number}
         */
        getLivechatPollTime: function () { return me.livechatPollTime; },
        /**
         * Returns the delay before the next livechat poll
         * @returns {number}
         */
        getLivechatCSRName: function () { return me.livechatCSRName; },
        /**
         * Returns whether the livechat CSR is typing
         * @returns {boolean}
         */
        isLivechatCSRWriting: function () { return me.livechatCSRIsWriting; },
        /**
         * Returns the livechat last interaction's index
         * @returns {number}
         */
        getLivechatInteractionIndex: function () { return me.livechatInteractionIndex; },
        /**
         * Returns the livechat CSR's name
         * @returns {string}
         */
        getLivechatId: function () { return me.livechatId; },
        /**
         * Returns whether the user should be allowed to send new queries or not
         * @returns {boolean}
         */
        canSend: function () {
            return !((me.livechatStatus === 1 && me.livechatConversation === true) ||
                (me.livechatStatus === 3 && me.livechatConversation === true));
        }
    };
};

/**
 * Creates a new dialog web service object allowing to send queries / receive responses
 * @param {Object} _config
 * @param {window.Nina.config.Config} _storage
 */
window.Nina.ws.newJBotService = function newJBotService(_config, _storage) {
    "use strict";
    var $ = window.Nina.$,
        io = window.io,
        me = {};

    // Config parameters
    me.timeoutQuery = _config.timeoutQuery;
    me.errorMessage = _config.errorMessage;
    me.urltoolong = _config.urltoolong;
    me.tooManyQueries = _config.tooManyQueries;
    me.newSessionMessage = _config.newSessionMessage;
    me.maxQueries = _config.maxQueries;
    me.usePersistent = _config.usePersistent;
    me.useJSONP = _config.useJSONP;
    me.apiVersion = _config.apiVersion;
    me.nleResults = _config.nleResults;
    if (!Modernizr.cors) { // force JSONP for IE8 & 9
        // The hack described here (http://stackoverflow.com/questions/9160123) is not enough because
        // as explained here (http://blogs.msdn.com/b/ieinternals/archive/2010/05/13/xdomainrequest-restrictions-limitations-and-workarounds.aspx),
        // IE8/9 doesn't send the Content-Type header, so we would need to make
        // server changes for this to work. Thus, falling back to JSONP instead.
        me.useJSONP = true;
    }
    // if( $.isPlainObject(window.NinaVars) && window.NinaVars.hasOwnProperty("preprod")) {
    //     me.dialog_url = _config.preprod.base_url;
    //     me.debug = _config.preprod.debug;
    //     me.use_smart_router = _config.preprod.use_smart_router;
    //     me.smart_router_endpoint = _config.preprod.smart_router_endpoint;
    //     me.sr_agent_endpoint = _config.preprod.sr_agent_endpoint;
    // } else {
    //     me.dialog_url = _config.prod.base_url;
    //     me.debug = _config.prod.debug;
    //     me.use_smart_router = _config.prod.use_smart_router;
    //     me.smart_router_endpoint = _config.prod.smart_router_endpoint;
    //     me.sr_agent_endpoint = _config.prod.sr_agent_endpoint;
    // }
    // me.dialog_url += "jbotservice.asmx/TalkAgent";

    me.storage = _storage;
    me.preQueryProcessors = [];
    me.messageHandlers = [];
    me.errorHandlers = [];
    me.typingHandler = null;
    me.sessionEndingHandlers = [];
    me.queriesQueue = [];
    me.smartrouterMsgQueue = [];
    me.sending = false;
    me.firstQuery = me.storage.getSession("SCI") ? false : true;
    me.errorTimer = null;
    me.timeoutTimer = null;
    me.livechatMode = false;
    me.livechatTimer = null;
    me.livechatErrors = 0;
    me.livechatMaxErrors = _config.livechatMaxErrors;
    me.livechatRetryDelay = _config.livechatRetryDelay;
    me.livechatInteractionIndex = me.storage.getSession("lcindex");
    me.userTyping = false;
    me.connected_to_smartrouter = false;

    me.preprod = $.isPlainObject(window.NinaVars) && window.NinaVars.hasOwnProperty("preprod");
    me.platform = me.preprod ? "preprod" : "prod";
    me.lastRequestFailed = false;

    if (isDualActiveEnabled()) {
        // ACTIVE/ACTIVE MODE
        // we check if there's a stored version in the session cookie
        var as = _storage.getSession('as');
        me.activeSite = doesBaseUrlContain(as) ? as : undefined;
    }

    function setUrls() {
        var platform = _config[me.platform],
            endpoint = "jbotservice.asmx/TalkAgent";

        // If base_url is a string, we're in basic mode. We set the url
        // If base_url is an object, we're in active/active mode.
        // 1. we check if we already know which site to pick
        // 2. if not, we will call houston at the first query, and set the urls again.

        if (isDualActiveEnabled() && doesBaseUrlContain(me.activeSite)) {
            me.dialog_url = platform.base_url[me.activeSite] + endpoint;
        } else if (!isDualActiveEnabled()) {
            me.dialog_url = platform.base_url + endpoint;
        } else {
            me.dialog_url = undefined;
        }

        me.debug = platform.debug;
    }

    setUrls();


    if (me.use_smart_router) {
        var date = new Date();
        var rand = date.getTime() * Math.random() + date.getTime();
        me.uniqueId = rand.toString();

        me.socket = io.connect(me.smart_router_endpoint);
        me.socket.on('whoareyou', function () {
            me.socket.emit('iam', me.uniqueId);
        });
        me.socket.on('hello', function () {
            me.connected_to_smartrouter = true;
            // Treat all the messages we got while we were connecting to the smart-router
            while (me.smartrouterMsgQueue.length > 0) {
                var msg = me.smartrouterMsgQueue.shift();
                msg.ids.ui = me.uniqueId;
                me.socket.emit('talk', msg);
            }
        });
        me.socket.on('talkback', function (data) {
            me.sr_agent_endpoint = data.ids.agent;
            var talkAgentResponse = JSON.parse(data.payload);
            responseHandler(talkAgentResponse);
        });
        me.socket.on('disconnect', function () {
            me.connected_to_smartrouter = false;
        });
    }

    // When we make AJAX calls using JSONP with JQuery, it tacks on the callback as a randomly-generated function
    // name to handle multi-threaded responses (kind of clever).  Unfortunately, this means that it takes up space in
    // the URL.  Space is limited because IE can only handle URLs of 2K in length, so we have to take that into account
    // here.  First we define the length of the JQuery JSONP callback component in the URL.  It seems to come out around
    // 56 characters but to give a spot of head room, I've defined it here to 70.
    me.JQUERY_JSONP_CALLBACK_URL_COMPONENT_LENGTH = 70;

    // Now define the max allowable URL length we can handle by subtracting the JQuery JSONP callback component length
    // from 2K.  We cannot generate URLs longer than this value.
    me.MAX_ALLOWABLE_URL_LENGTH = 2040 - me.JQUERY_JSONP_CALLBACK_URL_COMPONENT_LENGTH;

    /**
     * Builds the URL for the supplied query.
     * @param {window.Nina.ws.Query} query The query of interest.
     * @returns {string} URL
     */
    me.buildQueryURL = function (query) {
        var date = new Date();
        var rand = date.getTime() * Math.random() + date.getTime();
        var apiVersion = me.apiVersion ? "&v=" + me.apiVersion : "";
        return me.dialog_url + "?TalkAgentRequest={\"TalkAgentRequest\":" + encodeURIComponent(query.toJSON()) + "}&rnd=" + rand + apiVersion + "&Callback=?";
    };
    /**
     * Builds the querystring corresponging to the supplied query. It will be put in the Body of a POST request.
     * @param {window.Nina.ws.Query} query The query to be sent.
     * @returns {string} A querystring to be put in the body of the request
     */
    me.getPOSTBody = function (query) {
        var date = new Date();
        var rand = date.getTime() * Math.random() + date.getTime();
        var apiVersion = me.apiVersion ? "&v=" + me.apiVersion : "";
        return "TalkAgentRequest={\"TalkAgentRequest\":" + encodeURIComponent(query.toJSON()) + "}&rnd=" + rand + apiVersion;
    };

    function sessionEndingHandler() {
        me.timeoutTimer = null;

        $.each(me.sessionEndingHandlers, function (index, handler) {
            if ($.isFunction(handler)) {
                handler();
            }
        });
    }

    /**
     * Callback for the dialog web service response
     * @param {Object} data the web service response
     * @returns {void}
     */
    function responseHandler(data) {
        if (me.errorTimer !== null) {
            window.clearTimeout(me.errorTimer);
            me.errorTimer = null;
        }
        var interaction = window.Nina.ws.newInteraction(data);
        var ad = interaction.getAdditionalData();

        //Store sci and iid in cookie or whatever we use to store them.
        me.storage.setSession({
            'sci': interaction.getSCI(),
            'lastInt': +new Date()
        });

        me.storage.setPersist("iid", interaction.getIID());

        // If we do not get a success response code, display an error message and stop the processing here
        var rc = interaction.getResponseCode();
        if (rc !== window.Nina.enums.ResponseCode.Default && rc !== window.Nina.enums.ResponseCode.Found && rc !== window.Nina.enums.ResponseCode.Command && rc !== window.Nina.enums.ResponseCode.EmptyAnswer && rc !== window.Nina.enums.ResponseCode.IgnoreInit) {
            notifyErrorHandlers(rc, interaction.getResponse());
            me.sending = false;
            return;
        }

        //if we receive a value for in the timeoutnode, we start a timer

        var timeoutValue = interaction.getTimeOut();
        if (+timeoutValue !== 0) {
            if (me.timeoutTimer !== null) {
                window.clearTimeout(me.timeoutTimer);
            }
            me.timeoutTimer = window.setTimeout(sessionEndingHandler, timeoutValue * 1000);
        }

        if (ad.hasOwnProperty("Persistent") && me.usePersistent) {
            me.storage.setPersist("persist", ad.Persistent);
        }

        if (ad.hasOwnProperty("ContinueDialog")) {
            me.storage.setSession("cont", ad.ContinueDialog);
        } else {
            me.storage.setSession("cont", null);
        }

        me.livechatMode = interaction.isLivechatConversation();

        // If we're in livechat mode and there is no pending livechat poll timer nor pending query, let's start one
        if (me.livechatMode && interaction.getLivechatPollTime() > 0 && me.livechatTimer === null && me.queriesQueue.length == 0) {
            schedulePoll(interaction.getLivechatPollTime());
        }

        // Store if this is a livechat conversation so we can start back in livechat mode after a page refresh
        me.livechatInteractionIndex = interaction.getLivechatInteractionIndex();
        me.storage.setSession({
            "lcstat": interaction.isLivechatConversation(),
            "lcindex": interaction.getLivechatInteractionIndex()
        });

        // Display CSR is writing notification
        if ($.isFunction(me.typingHandler)) {
            me.typingHandler(interaction.isLivechatCSRWriting());
        }

        // Store the livechat id
        if (interaction.getLivechatId() !== "") {
            me.storage.setPersist("lcid", interaction.getLivechatId());
        }

        // If an additional message handler is available, use it
        $.each(me.messageHandlers, function (index, value) {

            if ($.isFunction(value)) {
                value(interaction);
            }

        });

        me.sending = false;
        processNextQuery();
    }

    /**
     * Schedule a livechat poll in time seconds.
     * @param {int} delay number of seconds to wait
     */
    function schedulePoll(delay) {
        me.livechatTimer = window.setTimeout(livechatPollHandler, delay * 1000);
    }

    /**
     * Called by the timer that triggers livechat polls
     * @returns {void}
     */
    function livechatPollHandler() {
        buildAndSendQuery(window.Nina.enums.Command.LivechatPoll, {});
        me.livechatTimer = null;
    }

    /**
     * Called at the end of a liveChat session to remove every scheduledPoll
     */
    function stopPolling() {
        if (me.livechatTimer) {
            window.clearTimeout(me.livechatTimer);
            me.livechatTimer = null;
        }
    }

    /**
     * Called by the error timer when the web service takes too long to answer
     * @returns {void}
     */
    function errorHandler() {
        window.clearTimeout(me.errorTimer);
        me.errorTimer = null;

        if (!me.lastRequestFailed && isDualActiveEnabled()) {
            me.lastRequestFailed = true;
            me.activeSite = undefined;
            callHoustonThen(function () {
                // we check again
                sendQueryWS(query);
            });
        } else {
            // we stop here and throw an error
            me.lastRequestFailed = false;
            notifyErrorHandlers("ServiceUnavailable", me.errorMessage);
            me.sending = false;
            processNextQuery();
        }
    }

    /**
     * Need to change then name ... it does more than notifying the error handles
     * @param {string} errorCode code representing the error, for web service errors, code returned in the code attribute
     * @param {string} errorText hopefully comprehensive text detailing the error
     */
    function notifyErrorHandlers(errorCode, errorText) {
        // If there is a service error / timeout, and we're in livechat mode, then we'll check if there is a poll waiting
        // and if not we'll schedule one to continue the livechat session
        // We do not do this if we receive a concurrent queries error since it can be a sign that we have multiple UIs
        // open at the same time and we don't want to continue sending polls in that case.
        if (errorCode !== window.Nina.enums.ResponseCode.ErrorConcurrentQueries && me.livechatMode && me.livechatTimer === null) {
            me.livechatErrors++;
            // We set a max number of tries since it's useless to continue hammering the server if it does not work
            if (me.livechatErrors < me.livechatMaxErrors) {
                schedulePoll(me.livechatRetryDelay);
            }
        }

        $.each(me.errorHandlers, function (index, value) {
            if ($.isFunction(value)) {
                value(errorCode, errorText);
            }
        });
    }


    function callHoustonThen(callback) {
        if (typeof callback !== "function") {
            throw new Error('Callback is not a valid function');
        }

        var cfg = _config[me.platform];

        if (me.useJSONP) {
            window.JSONP_Houston_Callback = function (site) {
                setActiveSite(site);
            }

            $.ajax(cfg.houstonURL_IE89, {
                cache: false,
                contentType: 'text/json',
                crossDomain: true,
                dataType: 'jsonp',
                jsonp: false
            });

        } else {
            $.get(cfg.houstonURL, function (site) {
                setActiveSite(site);
            });
        }

        function setActiveSite(site) {
            site = $.trim(site);
            me.activeSite = doesBaseUrlContain(site) ? site : returnFirstSite();
            storeSite(me.activeSite);
            setUrls();
            callback();
            return;
        }
    }

    function isDualActiveEnabled() {
        var sites = _config[me.platform].base_url;
        return typeof sites === 'object' && !$.isEmptyObject(sites);
    }

    function doesBaseUrlContain(site) {
        return _config[me.platform].base_url.hasOwnProperty(site);
    }

    if (_storage.getSession('as')) {
        window.Nina.storage.activeSite = _storage.getSession('as');
    }
    function storeSite(site) {
        _storage.setSession('as', site);
        window.Nina.storage.activeSite = site;
    }

    function returnFirstSite() {
        var sites = _config[me.platform].base_url;
        for (var site in sites) {
            if (typeof sites[site] === 'string' && sites[site].length > 0) {
                return sites[site];
            }
        }
        throw ("No site available");
    }

    me.lastRequestFailed = false;

    /**
     * Sends a query to the web service
     * @param {window.Nina.ws.Query} query
     * @returns {void}
     */
    function sendQueryWS(query) {
        me.sending = true;

        if (!me.useJSONP) {
            $.post(me.dialog_url, me.getPOSTBody(query), responseHandler, "json")
                .fail(errorHandler);
            me.errorTimer = window.setTimeout(errorHandler, me.timeoutQuery * 1000);
        }
        else { // JSONP
            var url = me.buildQueryURL(query);

            // IE cannot open URLs longer than 2083 characters (at most 2048 in path part)
            if (url.length > me.MAX_ALLOWABLE_URL_LENGTH) {
                try {
                    notifyErrorHandlers("URLTooLong", me.urltoolong + ": " + url.length);
                }
                finally {
                    me.sending = false;
                }
            }
            else {
                // With jQuery 1.x the .fail() callback is not called unfortunately. Relying on the errorTimer
                $.getJSON(url, responseHandler).fail(errorHandler);
                me.errorTimer = window.setTimeout(errorHandler, me.timeoutQuery * 1000);
            }
        }
    }


    /**
     * Build a query based on the user text and additional data provided and then queue it for sending
     * @param {string} userText
     * @param {Object} metadata
     * @returns {void}
     */
    function buildAndSendQuery(userText, metadata) {
        if (me.queriesQueue.length >= me.maxQueries) {
            notifyErrorHandlers("TooManyPendingQueries", me.tooManyQueries);
            return;
        }

        var finalMetadata = {};
        if (me.debug) {
            // TODO sbelone 20150318: the parameter to send is actually "debug:true"
            finalMetadata.Debug = {};
        }
        if (me.nleResults) {
            finalMetadata.nleResults = true;
        }

        finalMetadata.uiID = me.storage.uiID;

        // Livechat metadata
        finalMetadata.ClientMetaData = {};
        // Send livechat id if we have one
        var lcid = me.storage.getPersist("lcid");
        if (lcid !== null && lcid !== "") {
            finalMetadata.ClientMetaData.LiveChatId = lcid;
        }
        if (me.livechatInteractionIndex) {
            finalMetadata.lastIndex = me.livechatInteractionIndex;
        }
        // Set if the user is typing
        finalMetadata.VisitorIsTyping = me.userTyping;
        me.userTyping = false;

        // Send persistent node if we have one
        var persist = me.storage.getPersist("persist");
        if (me.usePersistent && persist !== null && persist !== "") {
            finalMetadata.Persistent = persist;
        }

        var sci = me.storage.getSession("sci");
        var iid = me.storage.getPersist("iid");
        sci = sci === null ? "" : sci;
        iid = iid === null ? "" : iid;

        $.extend(true, finalMetadata, metadata);

        // Send datapoints passed by website
        if (typeof window.NinaVars !== "undefined") {

            var processedNinaVars = {};
            $.extend(true, processedNinaVars, window.NinaVars);

            $.each(me.preQueryProcessors, function (index, processor) {
                if ($.isFunction(processor)) {
                    processor(processedNinaVars, userText, finalMetadata, testURLLengthValid);
                }
            });

            finalMetadata.NinaVars = processedNinaVars;
        }

        // NIWENG-300
        finalMetadata.NinaVars = finalMetadata.NinaVars || {};
        if (sci === "") {
            finalMetadata.NinaVars.invocationpoint = location.href;
            me.storage.setSession("invocationpoint", location.href);
        }
        else {
            finalMetadata.NinaVars.invocationpoint = me.storage.getSession("invocationpoint");
        }

        var query = window.Nina.ws.newQuery(sci, iid, userText, finalMetadata);
        if (!me.use_smart_router) {
            me.queriesQueue.push(query);
            processNextQuery();
        }
        else {
            talk(query);
        }
    }

    function talk(query) {
        var msg = {
            ids: { ui: me.uniqueId, agent: me.sr_agent_endpoint },
            metadata: {},
            payload: "{\"TalkAgentRequest\":" + query.toJSON() + "}"
        };
        if (me.connected_to_smartrouter) {
            me.socket.emit('talk', msg);
        }
        else {
            me.smartrouterMsgQueue.push(msg);
        }
    }


    /**
     * Returns true if the URL constructed from the supplied parameters is of a valid length (< 2040 characters) and
     * false if it is too long.
     *
     * @param ninaVars {Object} The Nina vars used to construct the URL.
     * @param userText {string} The text of the user query to be placed in the URL.
     * @param metaData {Object} custom meta data used to construct the URL.
     *
     * @return true if the URL length is valid (< 2040 chars), false if not.
     */
    function testURLLengthValid(ninaVars, userText, metaData) {
        if (!me.useJSONP) {
            return me.dialog_url < me.MAX_ALLOWABLE_URL_LENGTH;
        }

        var sci = me.storage.getSession("sci");
        var iid = me.storage.getPersist("iid");
        sci = sci === null ? "" : sci;
        iid = iid === null ? "" : iid;

        var tempMetaData = {};
        $.extend(true, tempMetaData, metaData);
        tempMetaData.NinaVars = ninaVars;

        var query = window.Nina.ws.newQuery(sci, iid, userText, tempMetaData);
        var url = me.buildQueryURL(query);
        var length = url.length;
        return length < me.MAX_ALLOWABLE_URL_LENGTH - 50;
    }

    /**
     * Processes the Params object and sends as many ##skipAlyze#Params queries as necessary.
     * @param obj {object} The object containing the parameters sent by the webpage in NinaVars
     * @return {void}
     */
    function splitAndSendParams(obj) {
        var params = [];
        if (typeof obj !== "object") {
            return;
        } else {
            $.each(obj, function (name, node) {
                params.push([name, node]);
            });
        }

        // CREATING THE METADATA NODE
        var metadata = {
            VisitorIsTyping: false,
            newParams: {}
        };

        if (me.debug) {
            metadata.Debug = {};
        }

        var lcid = me.storage.getPersist("lcid");
        if (lcid !== null && lcid !== "") {
            metadata.ClientMetaData.LiveChatId = lcid;
        }

        var persist = me.storage.getPersist("persist");
        if (me.usePersistent && persist !== null && persist !== "") {
            metadata.Persistent = persist;
        }

        var empty = true;
        var deleted = 0;
        var tmpMetadata = $.extend(true, {}, metadata);
        tmpMetadata.newParams = {};

        while (params.length > 0) {
            var item = params[0];
            tmpMetadata.newParams[item[0]] = item[1];
            if (testURLLengthValid(window.NinaVars, window.Nina.enums.Command.SendParams, tmpMetadata)) {
                item = params.shift();
                metadata.newParams[item[0]] = item[1];
                empty = false;
            } else {
                if (empty === true) {
                    //it means that this parameter can't even be sent alone. It's just too big.
                    //we delete it
                    params.shift();
                    deleted++;
                } else {
                    //it means that the chunk is ready to be sent
                    buildAndSendParamsQuery(metadata);
                    metadata.newParams = {};
                    tmpMetadata.newParams = {};
                    empty = true;
                }
            }
        }
        if (deleted > 0) {
            window.NinaVars.paramsDeleted = deleted;
        }
        if (empty === false) {
            buildAndSendParamsQuery(metadata);
        }
    }

    /**
    * Build a query only for sending params in several requests
    * @param {Object} metadata
    * @returns {void}
    */
    function buildAndSendParamsQuery(metadata) {
        if (me.queriesQueue.length >= me.maxQueries) {
            notifyErrorHandlers("TooManyPendingQueries", me.tooManyQueries);
            return;
        }

        var finalMetadata = metadata;

        var sci = me.storage.getSession("sci");
        var iid = me.storage.getPersist("iid");
        sci = sci === null ? "" : sci;
        iid = iid === null ? "" : iid;

        var query = window.Nina.ws.newQuery(sci, iid, window.Nina.enums.Command.SendParams, finalMetadata);
        me.queriesQueue.push(query);
        //processNextQuery();
    }



    /**
     * Takes the next query in queue (if any) and send it
     * @returns {void}
     */
    function processNextQuery() {
        if (me.sending || me.queriesQueue.length === 0) {
            return;
        }
        // if we haven't received the first answer, we wait for it before sending a new query
        if (me.storage.getSession('sci') === null && !me.firstQuery) {
            return;
        }
        var query = me.queriesQueue.shift();
        if (query.getSCI() === "" && !me.firstQuery) {
            query.setSCI(me.storage.getSession('sci'));
        }
        if (query.getIID() === "" && !me.firstQuery) {
            query.setIID(me.storage.getPersist('iid'));
        }
        me.firstQuery = false;

        // here we send a request to Houston to select an active site
        // (only if necessary)
        // then we send the `query`

        // Active/Active is enabled and no site has been selected yet
        if (typeof _config[me.platform].base_url === 'object' && me.activeSite === undefined) {
            // we check Houston first
            callHoustonThen(function () {
                sendQueryWS(query);
            });
        } else {
            sendQueryWS(query);
        };
    }

    return /** @lends window.Nina.ws.JBotService.prototype */ {
        /**
         * Queues a query for sending based on the question and any additional data
         * @param {string} userText
         * @param {!Object} additionalData
         * @returns {void}
         */
        sendQuery: function (userText, additionalData) {
            buildAndSendQuery(userText, additionalData);
        },
        /**
         * Queues queries for sending only additional Data
         * @param {object} options object containing additional options such as nodeName and removeNode
         * @returns {void}
         */
        sendParams: function (obj) {
            splitAndSendParams(obj);
        },
        /**
         * Empty Queue. Useful to delete ##SkipAlyze#Params queries before they're sent
         * @returns {void}
         */
        emptyQueue: function () {
            me.queriesQueue = [];
        },
        /**
         * Adds a processor to be called immediately prior to sending a query.  The processor has the signature:
         *
         * void function(ninaVars, userText, metaData, testURLLengthValid)
         *
         * The processor is free to delete or add properties to ninaVars prior to the query being sent.
         * Understand that the operation takes place on a _copy_ of the NinaVars object defined on the web page;
         * the values in the original NinaVars will _not_ be changed.  That means that the processor will be called
         * with the same values as are in the original NinaVars and must remove or add them each time a query is
         * made.
         *
         * The processor is supplied the userText and the metaData to facilitate ninaVar addition or removal
         * decision logic.
         *
         * The processor is supplied a function that will test the length of the URL that would be used to make the
         * query to the agent  The function will return true if the URL length is acceptable or false if not.  This allows
         * processors to update ninaVars and and be confident that the query issued will not result in a "URLTooLong"
         * error. Here's an example of how this can be called:
         *
         * var toBeRemoved = ["wi", "ui"];
         *
         * var index = 0;
         * while (!testURLLengthValid(ninaVars, userText, metaData) && index < toBeRemoved.length)
         * {
         *   delete ninaVars[toBeRemoved[index]];
         *   index++;
         * }
         *
         * Processors are called in the order they are defined.  The state of the supplied ninaVars object will
         * be passed to the next processor, allowing processor chaining.
         *
         * @param {function(window.Nina.ws.Interaction):void} processor
         * @returns {void}
         */
        addPreQueryProcessor: function (processor) {
            me.preQueryProcessors.push(processor);
        },
        /**
         * Adds a new message handler that will be called when receiving a response from the web service
         * @param {function(window.Nina.ws.Interaction):void} messageHandler
         * @returns {void}
         */
        addMessageHandler: function (messageHandler) {
            me.messageHandlers.push(messageHandler);
        },
        /**
         * Add an error handler that will be called when there is an error
         * @param {function(string,string):void} errorHandler
         * @returns {void}
         */
        addErrorHandler: function (errorHandler) {
            me.errorHandlers.push(errorHandler);
        },
        /**
         * Add an error handler that will be called when there is an error
         * @param {function(string,string):void} errorHandler
         * @returns {void}
         */
        addSessionEndingHandler: function (sessionEndingHandler) {
            me.sessionEndingHandlers.push(sessionEndingHandler);
        },
        /**
         * Sets the handler that will be called to update the CSR is typing status
         * @param {function(boolean):void} typingHandler
         * @returns {void}
         */
        setTypingHandler: function (typingHandler) {
            me.typingHandler = typingHandler;
        },
        /**
         * Removes all the message handlers
         * @returns {void}
         */
        clearAdditionalMessageHandler: function () {
            me.messageHandlers = [];
        },
        /**
         * Stop any scheduled poll
         * @returns {void}
         */
        stopPolling: function () {
            stopPolling();
        },
        /**
         * Indicate the user is typing
         * @returns {void}
         */
        setUserIsTyping: function () {
            me.userTyping = true;
        },
        /**
         * Clears the context id which implies the next call will create a new dialog.
         */
        newSession: function () {
            me.storage.setSession("sci", "");
        },

        __me: me
    };
};


/**
 * Creates a new pre-query processor that automatically removes variables from the supplied NinaVars parameter
 * until the URL that will be created to send a query to the agent is of an acceptable length.  This processor achieves
 * this by looping through an array of property paths supplied upon construction, removing each corresponding object
 * from the Nina vars structure until the "testURLLengthFunction" indicates that the URL that will be sent is
 * acceptable.
 *
 * Each element in a property path must be separated with the '.' character.  You may not have property keys with ".".
 * For example:
 *
 * NinaVars["a.b"] = "hello";
 *
 * Will not be picked up by this processor because it will assume "a.b" to be nested sub-objects, as would be created
 * by:
 *
 * NinaVars["a"]["b"] = "hello";
 *
 * Note that it's still possible for this processor to execute successfully but the URL still be too long.  This will
 * happen if you do not supply enough properties to be expunged or the properties do not exist in the NinaVars
 * object.
 *
 * To use:
 *
 * // code to initialize config and cookie jar
 * ...
 * var botService = window.Nina.ws.newJBotService(config.ws, cookieJar);
 * ...
 * botService.addPreQueryProcessor(window.Nina.ws.newPrioritizedNinaVarRemovalPreQueryProcessor([
 *    "a.b.c",
 *    "another.path",
 *    "yet.another.object.path"
 *    ...
 *    ]);
 *
 * @param prioritizedPathList An array of property paths that will be tried in order.  Invalid paths (those that do not
 * correspond to an object property addressable in NinaVars) are ignored.
 *
 * @returns A pre-query processor function suitable for adding to the bot service.
 */
window.Nina.ws.newPrioritizedNinaVarRemovalPreQueryProcessor = function newPrioritizedNinaVarRemovalPreQueryProcessor(prioritizedPathList) {
    "use strict";
    var me = {};
    me.prioritizedPathList = prioritizedPathList;


    /**
     * Returns true if the supplied object has properties, false if not.
     */
    function hasProperties(object) {
        //noinspection LoopStatementThatDoesntLoopJS
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                return true;
            }
        }
        return false;
    }


    return function (ninaVars, userText, metaData, testURLLengthFunction) {

        // Work our way through our ill-fated keys deleting them from ninaVars until our URL is of an acceptable
        // length, as determined by the Nina framework.
        var index = 0;
        while (!testURLLengthFunction(ninaVars, userText, metaData) && index < me.prioritizedPathList.length) {
            var stack = [];
            var currentObject = ninaVars;

            var pathElements = me.prioritizedPathList[index].split(".");
            var i = 0;

            // Work our way down through the property path ("a.b.c.d") and build up a stack containing stuff we
            // might remove.
            while (i < pathElements.length && currentObject.hasOwnProperty(pathElements[i])) {
                stack.push({ object: currentObject, property: pathElements[i] });
                currentObject = currentObject[pathElements[i]];
                i++;
            }

            // Check to see if at least one element in the property path corresponds to an actual property.
            if (stack.length > 0) {
                var node = stack.pop();

                if (stack.length === pathElements.length - 1) {
                    // The full path matched to something.  We get rid of the last property of the last object on
                    // the path.
                    delete node.object[node.property];
                }

                // Now we work our way back up the path, deleting objects that have no content as we go.  Interestingly,
                // if an object has no content we delete it from its _parent_ object.  In other words, we don't want
                // the parent object (the thing that points to it) to be pointing to empty objects.
                while (stack.length > 0 && !hasProperties(node.object)) {
                    node = stack.pop();
                    delete node.object[node.property];
                }
            }

            // On to the next path element.
            index++;
        }
    };
};

window.Nina.ws.newJQualificationService = function (_config, _cookiesJar) {
    "use strict";
    var $ = window.Nina.$,
        me = {};

    me.sending = false;
    me.cookiesJar = _cookiesJar;

    // Config parameters
    if ($.isPlainObject(window.NinaVars) && window.NinaVars.hasOwnProperty("preprod")) {
        me.url = _config.preprod.base_url;
    } else {
        me.url = _config.prod.base_url;
    }
    me.url += "jbotservice.asmx/Qualification";
    me.timeoutQuery = _config.timeoutQuery;
    me.errorMessage = _config.errorMessage;
    me.urltoolong = _config.urltoolong;
    me.useJSONP = _config.useJSONP;
    if (!Modernizr.cors) { // force JSONP for IE8 & 9
        me.useJSONP = true;
    }

    /**
     * Send the qualification info to the webservice
     * @param {object} qualif
     * @returns {boolean}
     */
    function sendQualif(qualif) {
        me.sending = true;
        var date = new Date();
        var rand = date.getTime() * Math.random() + date.getTime();

        if (!me.useJSONP) {
            window.Nina.$.post(me.url, {
                "Infos": JSON.stringify(qualif),
                "Rnd": rand
            });
        }
        else { // JSONP
            // IE cannot open URLs longer than 2083 characters (at most 2048 in path part)
            var targetUrl = me.url + "?Infos=" + encodeURIComponent(JSON.stringify(qualif)) + "&Rnd=" + rand;
            if (targetUrl.length > 2048) {
                return false;
            }

            $.ajax({
                url: me.url,
                dataType: 'script',
                data: {
                    "Infos": JSON.stringify(qualif),
                    "Rnd": rand
                }
            });
        }

        return true;
    }

    return /** @lends window.Nina.ws.JQualificationService.prototype */ {
        /**
         * Send the qualification data
         * @param {string} name
         * @param {string} value
         * @param {Date} date
         * @param {String} sci
         * @returns {boolean}
         */
        sendQualification: function (name, value, datetime, sci) {
            if (sci === undefined || sci === null || sci === "") {
                sci = me.cookiesJar.getSession("sci");
            }

            if (sci === null || sci === "") {
                return false;
            }

            if (typeof datetime !== "Date") {
                datetime = window.Nina.helper.date.toISOString(new Date());
            }
            var infos = {
                Info: [
                    {
                        SCI: sci,
                        Name: name,
                        DateTime: datetime,
                        Value: value
                    }
                ]
            };
            return sendQualif(infos);
        }
    };
};
window.Nina.ws.newTranscript = function (data, filter, regexFilter) {
    "use strict";
    var me = {};
    me.responseCode = "MissingResponseCode";
    me.interactions = [];
    me.filter = {};

    if (typeof data !== 'object' || !data.hasOwnProperty("TranscriptOutput")) {
        me.responseCode = "InvalidResponse";
        me.text = "Invalid response from service";
        me.author = window.Nina.enums.Author.Error;
    } else {
        var response = data.TranscriptOutput;
        if (response.hasOwnProperty("@Code")) {
            me.responseCode = response["@Code"];
        }
        if (response.hasOwnProperty("Interaction")) {
            var inter = window.Nina.$.isArray(response.Interaction) ? response.Interaction : [response.Interaction];
            window.Nina.$.each(inter, function (index, value) {
                var agent = {
                    timestamp: value["@Timestamp"],
                    toDisplay: value.Agent,
                    author: window.Nina.enums.Author.Agent
                };
                var human = {
                    timestamp: value["@Timestamp"],
                    toDisplay: value.Human,
                    author: window.Nina.enums.Author.User
                };

                var filtered = false;
                //We apply filter on "human" interaction
                for (var f in filter) {
                    if (human.toDisplay && human.toDisplay.indexOf(f) > -1) {
                        if (filter[f] === "") {
                            filtered = true;
                        } else {
                            human.toDisplay = filter[f];
                        }
                    }
                }

                for (var rf in regexFilter) {
                    if (human.toDisplay) {
                        human.toDisplay = human.toDisplay.replace(new RegExp(rf, "g"), regexFilter[rf]);
                    }
                }

                //Don't add ##FirstInit
                if (!filtered) {
                    if (!(index === 0 && human.toDisplay === window.Nina.enums.Command.DialogFirstInit)) {
                        me.interactions.push(human);
                    }
                    me.interactions.push(agent);
                }

            });
        }
    }
    return /** @lends window.Nina.ws.Transcript.prototype */ {
        /**
         * Returns the response code from the service
         * @returns {string}
         */
        getResponseCode: function () { return me.responseCode; },
        /**
         * Returns the interaction arrays
         * @returns {Array.<Object>}
         */
        getInteractions: function () { return me.interactions; }
    };
};
/**
 * Create a new JTranscriptService object
 * @param {Object} _config
 * @returns {window.Nina.ws.JTranscriptProxy}
 */
window.Nina.ws.newJTranscriptService = function (_config) {
    "use strict";

    var me = {};
    me.messageHandler = [];
    me.errorHandler = null;
    me.errorTimer = null;
    me.sending = false;
    me.filter = _config.transcriptFilter;
    me.regexFilter = _config.transcriptRegexReplaceFilter || {};
    me.useJSONP = _config.useJSONP;
    if (!Modernizr.cors) { // force JSONP for IE8 & 9
        me.useJSONP = true;
    }

    // Config parameters
    me.timeoutQuery = _config.timeoutQuery;
    me.errorMessage = _config.errorMessage;
    me.urltoolong = _config.urltoolong;

    me.preprod = window.Nina.$.isPlainObject(window.NinaVars) && window.NinaVars.hasOwnProperty("preprod");
    me.platform = me.preprod ? "preprod" : "prod";
    me.lastRequestFailed = false;

    if (isDualActiveEnabled()) {
        // ACTIVE/ACTIVE MODE
        // we check if there's a stored version in the session cookie
        var as = window.Nina.storage.activeSite;
        me.activeSite = doesBaseUrlContain(as) ? as : undefined;
    }

    function setUrls() {
        var platform = _config[me.platform],
            endpoint = "jagentservice.asmx/Transcript";

        // If base_url is a string, we're in basic mode. We set the url
        // If base_url is an object, we're in active/active mode.
        // 1. we check if we already know which site to pick
        // 2. if not, we will call houston at the first query, and set the urls again.

        if (isDualActiveEnabled() && doesBaseUrlContain(me.activeSite)) {
            me.url = platform.base_url[me.activeSite] + endpoint;
        } else if (!isDualActiveEnabled()) {
            me.url = platform.base_url + endpoint;
        }
    }

    setUrls();

    function isDualActiveEnabled() {
        var sites = _config[me.platform].base_url;
        return typeof sites === 'object' && !window.Nina.$.isEmptyObject(sites);
    }

    function doesBaseUrlContain(site) {
        return _config[me.platform].base_url.hasOwnProperty(site);
    }

    me.lastRequestFailed = false;

    /**
     * Called when the web service responds
     * @param {Object} data
     * @returns {void}
     */
    function responseHandler(data) {
        if (me.errorTimer !== null) {
            window.clearTimeout(me.errorTimer);
            me.timer = null;
        }
        var transcript = window.Nina.ws.newTranscript(data, me.filter, me.regexFilter);
        if (transcript.getResponseCode() !== "Success") {
            if (window.Nina.$.isFunction(me.errorHandler)) {
                me.errorHandler(transcript.getResponseCode());
            }
            me.sending = false;
            return;
        }

        window.Nina.$.each(me.messageHandler, function (index, handler) {
            if (window.Nina.$.isFunction(handler)) {
                handler(transcript);
            }
        });
        me.sending = false;
    }

    /**
     * Called when an error occurs
     * @returns {void}
     */
    function errorHandler() {
        window.clearTimeout(me.errorTimer);
        me.errorTimer = null;
        if (window.Nina.$.isFunction(me.errorHandler)) {
            me.errorHandler(me.errorMessage);
        }
        me.sending = false;
    }

    /**
     * Retrieves the transcript for the specified context id
     * @param {string} sci
     * @returns {boolean}
     */
    function retrieveTranscript(sci) {
        me.sending = true;
        var date = new Date();
        var rand = date.getTime() * Math.random() + date.getTime();

        if (!me.useJSONP) {
            window.Nina.$.post(me.url, "SCI=" + sci + "&rnd=" + rand, responseHandler, "json").fail(errorHandler);
        }
        else { // JSONP
            var url = me.url + "?SCI=" + sci + "&rnd=" + rand + "&Callback=?";
            // IE cannot open URLs longer than 2083 characters (at most 2048 in path part)
            if (url.length > 2048) {
                if (window.Nina.$.isFunction(me.errorHandler)) {
                    me.errorHandler(me.urltoolong);
                }
                return false;
            }
            // With jQuery 1.x the .fail() callback is not called unfortunately. Relying on the errorTimer
            window.Nina.$.getJSON(url, responseHandler).fail(errorHandler);
        }

        me.errorTimer = window.setTimeout(errorHandler, me.timeoutQuery * 1000);
        return true;
    }

    return /** @lends window.Nina.ws.JTranscriptService.prototype */ {
        /**
         * Retrieve a transcript for the specified context id
         * @param {string} _sci
         * @returns {boolean}
         */
        getTranscript: function (_sci) {
            // Already querying the service, abort.
            if (me.sending) {
                return false;
            }
            return retrieveTranscript(_sci);
        },
        /**
         * Sets the message handler that will receive the transcript
         * @param {function(window.Nina.ws.Transcript):void} handler
         * @returns {void}
         */
        addMessageHandler: function (handler) {
            me.messageHandler.push(handler);
        },
        /**
         * Sets the error handler that will receive any error message
         * @param {function(string):void} handler
         * @returns {void}
         */
        setErrorHandler: function (handler) {
            me.errorHandler = handler;
        }

    };
};

window.Nina.ui.newDisplayHistory = function (_agentId, _config, _cookiesJar) {
    "use strict";
    var $ = window.Nina.$;
    var me = {};

    me.agentId = _agentId;
    me.cookiesJar = _cookiesJar;
    me.text = {};
    me.text.headerSeparator = _config.headerSeparator;
    me.text.headerAgent = _config.headerAgent;
    me.text.headerSystem = _config.headerSystem;
    me.text.headerError = _config.headerError;
    me.text.headerUser = _config.headerUser;
    me.text.defaultCSRName = _config.defaultCSRName;

    me.textSizes = _config.textSizes;
    me.textSizeIndex = _config.initTextSizeIndex;

    me.text.headerCSR = "";
    me.lastLineAuthor = "";
    me.id = {};
    me.id.chatArea = "#" + _agentId + " .nw_Conversation";
    me.id.chatText = "#" + _agentId + " .nw_Conversation .nw_ConversationText";
    me.id.saveButton = "#" + _agentId + " .nw_Save";
    me.id.printButton = "#" + _agentId + " .nw_Print";
    me.id.textPlusButton = "#" + _agentId + " .nw_TextPlus";
    me.id.textMinusButton = "#" + _agentId + " .nw_TextMinus";
    me.id.textCycleButton = "#" + _agentId + " .nw_TextCycle";

    me.components = {};
    me.components.chatArea = null;
    me.components.chatText = null;
    me.components.saveButton = null;
    me.components.printButton = null;
    me.components.textPlusButton = null;
    me.components.textMinusButton = null;
    me.components.textCycleButton = null;

    me.css = {};
    me.css.authorCSR = "nw_CsrSays";
    me.css.authorUser = "nw_UserSays";
    me.css.authorSystem = "nw_SystemSays";
    me.css.authorAgent = "nw_AgentSays";
    me.css.authorError = "nw_ErrorSays";
    me.css.print = "nw_PrintPage";

    me.firstDisplay = true;

    populateComponents();
    setupHandlers();

    var textSize = parseInt(me.cookiesJar.getPersist("ts"), 10);
    if (!isNaN(textSize)) {
        changeTextSize(textSize);
    }

    /**
     * Sets the various button handlers
     * @returns {void}
     */
    function setupHandlers() {
        me.components.saveButton.click(function () { newHistoryWindow(false); });
        me.components.printButton.click(function () { newHistoryWindow(true); });
        me.components.textMinusButton.click(function () { changeTextSize(me.textSizeIndex - 1); });
        me.components.textPlusButton.click(function () { changeTextSize(me.textSizeIndex + 1); });
        me.components.textCycleButton.click(function () { changeTextSize((me.textSizeIndex + 1) % me.textSizes.length); });
        //TODO: filter the keys that trigger this. space and enter should be enough
        /*me.components.saveButton.keydown(function() { newHistoryWindow(false);});
        me.components.printButton.keydown(function() {newHistoryWindow(true);});
        me.components.textMinusButton.keydown(function() { changeTextSize(me.textSizeIndex -1);});
        me.components.textPlusButton.keydown(function() { changeTextSize(me.textSizeIndex + 1);});*/
    }

    /**
     * Retrieves the components based on their IDs
     * @returns {void}
     */
    function populateComponents() {
        me.components.chatArea = $(me.id.chatArea);
        me.components.chatText = $(me.id.chatText);
        me.components.saveButton = $(me.id.saveButton);
        me.components.printButton = $(me.id.printButton);
        me.components.textPlusButton = $(me.id.textPlusButton);
        me.components.textMinusButton = $(me.id.textMinusButton);
        me.components.textCycleButton = $(me.id.textCycleButton);
    }

    /**
     * Open a new window and fill it with the chat history content
     * @param {boolean} doPrint
     * @returns {void}
     */
    function newHistoryWindow(doPrint) {
        var transcript = getTranscriptHTML();
        var options = "width=500,height=500,menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,location=no,status=no";
        var newWin;

        newWin = window.open("about:blank", "printpopup", options);
        newWin.document.open("text/html", true);
        newWin.document.write(transcript);
        newWin.document.close();

        if (doPrint) {
            newWin.print();
        }
    }

    /**
     * Returns an HTML version of the transcript of a page
     * @returns {string} the HTML of the transcript page
     */
    function getTranscriptHTML() {
        var baseDir = window.Nina.helper.scripts.getScriptPath("scripts/" + me.agentId + ".js");
        var transcriptHTML = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><link rel='stylesheet' type='text/css' media='all' href='" + baseDir + "css/" + me.agentId + ".css' /></head><body><div id='" + me.agentId + "' class='" + me.css.print + "'>" + $(me.id.chatText).html() + "</div></body></html>";

        return transcriptHTML;
    }

    /**
     * Change the text size
     * @param {int} change direction of the change +1/-1
     * @returns {void}
     */
    function changeTextSize(newIndex) {
        if (newIndex < 0 || newIndex >= me.textSizes.length || me.textSizes.length === 0) {
            return;
        }
        me.components.chatText.css("fontSize", me.textSizes[newIndex]);
        me.textSizeIndex = newIndex;

        me.cookiesJar.setPersist("ts", me.textSizeIndex);

        //trigger event that we can listen in agent.js
        $("#" + me.agentId).trigger("changeTextSize", [me.textSizes[newIndex]]);
    }

    /**
     * Append the passed HTML to the chat text area
     * @param {string} line
     * @returns {JQuery}
     */
    function appendChatLine(line) {
        var topPadding = parseInt(me.components.chatArea.css("paddingTop"), 10);
        var curHeight = me.components.chatText.outerHeight() + topPadding;
        var messageHeight;
        var newElement;

        me.components.chatText.append(line);
        newElement = me.components.chatText.children("div").last();

        messageHeight = me.components.chatText.outerHeight() + topPadding - curHeight;

        if (messageHeight > me.components.chatArea.height()) {
            me.components.chatArea.animate({ scrollTop: curHeight }, 500);
        } else {
            me.components.chatArea.animate({ scrollTop: me.components.chatText.prop("scrollHeight") }, 500);
        }
        return newElement;
    }

    /**
     * Append the array of lines to the chat text area
     * @param {Array.<Object>}arrayLines
     * @returns {void}
     */
    function appendChatLines(arrayLines) {
        $.each(arrayLines, function (index, line) {
            var text = $.trim(line.toDisplay);
            if (text && text.length > 0) {
                me.components.chatText.append(getChatLine(line.author, line.toDisplay));
            }
        });
        me.components.chatArea.animate({ scrollTop: me.components.chatText.prop("scrollHeight") }, 500);
    }

    /**
     * Returns the HTML to append to the chat text for a given author + text
     * @param {string} author
     * @param {string} text
     * @returns {string}
     */
    function getChatLine(author, text) {
        var line = "";
        var initialBlock = "<div class=\"nw_initials\" aria-hidden=\"true\">" + (author === window.Nina.enums.Author.User ? window.VAUserInitials : "") + "</div>";
        var cssDialogueLineClass;
        var header = "";
        var baseProcessors = [window.Nina.text.Processors.getScriptTagsProcessor(), window.Nina.text.Processors.getWhiteSpaceProcessor(), window.Nina.text.Processors.getBreakLinesProcessor()];
        var processors = [];

        switch (author) {
            case window.Nina.enums.Author.Agent:
                header = me.text.headerAgent;
                cssDialogueLineClass = me.css.authorAgent;
                // To remove once urls are changed in NIQS
                processors.push(window.Nina.text.Processors.getCreateRealLinksProcessor());
                break;
            case window.Nina.enums.Author.System:
                header = me.text.headerSystem;
                cssDialogueLineClass = me.css.authorSystem;
                break;
            case window.Nina.enums.Author.User:
                header = me.text.headerUser;
                cssDialogueLineClass = me.css.authorUser;
                processors.push(window.Nina.text.Processors.getEscapeInputTextProcessor());
                break;
            case window.Nina.enums.Author.CSR:
                header = me.text.headerCSR;
                cssDialogueLineClass = me.css.authorCSR;
                break;
            case window.Nina.enums.Author.Error:
                header = me.text.headerError;
                cssDialogueLineClass = me.css.authorError;
                break;
            default:
                break;
        }

        text = window.Nina.text.Processors.applyProcessors(text, processors.concat(baseProcessors));
        if (author !== window.Nina.enums.Author.User && me.firstDisplay && window.NinaVars && window.NinaVars.hasOwnProperty("preprod")) {
            me.firstDisplay = false;
            text = "<span class='nw_Preprod'>[PREPROD]</span><br>" + text;
        }

        if (author === me.lastLineAuthor || header === "") {
            line = "<div class=\"" + cssDialogueLineClass + "\"><div class=\"nw_SaysText\">" + text + "</div>" + initialBlock + "</div>";
        } else {
            line = "<div class=\"" + cssDialogueLineClass + "\"><div class=\"nw_SaysText\">" + "<span class=\"nw_Header\">" + header + me.text.headerSeparator + "</span>" + text + "</div>" + initialBlock + "</div>";
        }
        if (author === window.Nina.enums.Author.System) {
            line = "<hr>" + line + "<hr>";
        }
        me.lastLineAuthor = author;
        return line;
    }

    return /** @lends window.Nina.ui.DisplayHistory.prototype */ {
        /**
         * Adds a chat line from a given author
         * @param {string} toDisplay
         * @param {window.Nina.enums.Author} author
         * @returns {JQuery}
         */
        addChatLine: function (toDisplay, author) {
            return appendChatLine(getChatLine(author, toDisplay));
        },
        /**
         * Adds an array of lines to the history
         * @param {Array.<Object>} array
         * @returns {void}
         */
        addChatLines: function (array) {
            appendChatLines(array);
        },
        /**
         * Clears the chat history
         * @returns {void}
         */
        clearChatHistory: function () {
            me.components.chatText.children().remove();
            me.lastLineAuthor = "";
        },
        /**
         * Sets the CSR name for livechat
         * @param {string} name
         * @returns {void}
         */
        setCSRName: function (name) {
            if (name.length > 0) {
                me.text.headerCSR = name;
            } else {
                me.text.headerCSR = me.text.defaultCSRName;
            }

        },
        /**
        * Scroll to the bottom of the history
        */
        scrollToBottom: function () {
            me.components.chatArea.animate({ scrollTop: me.components.chatText.prop("scrollHeight") }, 500);
        },
        scrollToLastAnswer: function () {
            me.components.chatArea.animate({
                scrollTop: $('.' + me.css.authorAgent + ':last').position().top + me.components.chatArea.scrollTop() - 20
            });
        },
        getTranscriptHTML: getTranscriptHTML
    };
};
window.Nina.ui.DOMInject = function (_agentId, _config) {
    "use strict";
    var $ = window.Nina.$;

    var me = {};
    me.agentId = _agentId;

    me.popin = $("#" + me.agentId);
    if (_config.popinHTML !== "") {
        if (me.popin.length === 0) {
            me.popin = $(_config.agentHTML).appendTo('body');
        } else {
            me.popin.replaceWith(_config.agentHTML);
        }
    }

    me.popinMin = $("#" + me.agentId + "-min");
    if (_config.popinMinHTML !== "") {
        if (me.popinMin.length === 0) {
            me.popinMin = $(_config.agentMinHTML).appendTo('body');
        } else {
            me.popinMin.replaceWith(_config.agentMinHTML);
        }
    }

    return $("#" + me.agentId);
};
window.Nina.ui.newFirstMessage = function (_config, _cookiesJar, _ui, _transcript) {
    "use strict";
    var $ = window.Nina.$;
    var me = {};
    me.cookiesJar = _cookiesJar;
    me.ui = _ui;
    me.transcript = _transcript;

    // Config parameters
    me.welcome = _config.welcome;
    me.transcriptMessage = _config.transcriptMessage;
    me.sessionExpiredMessage = _config.sessionExpiredMessage;

    if (me.transcript !== null) {
        me.transcript.addMessageHandler(responseHandler);
        me.transcript.setErrorHandler(errorHandler);
    }
    checkNinaSessionNeedsClearing();
    if (!me.cookiesJar.isOngoingSession() || window.isNewNinaSessionRequired === true) {
        var welcomeSentence = $.isFunction(me.welcome) ? me.welcome() : me.welcome.toString();
        me.cookiesJar.removeSession("sci");
        me.cookiesJar.setSession("lcstat", false);
        if (welcomeSentence !== "") {
            me.ui.addChatLine("<span class='nw_Welcome'>" + welcomeSentence + "</span>", window.Nina.enums.Author.Agent);
        }
    } else {
        if (me.transcriptMessage !== "") {
            var trans = me.ui.addChatLine(me.transcriptMessage, window.Nina.enums.Author.System);
            var link = trans.find(".nw_TranscriptLink");
            link.click(handleGetTranscript);
        }

        var lcstat = me.cookiesJar.getSession("lcstat");
        var send;
        var ad = {};
        if (lcstat === true) {
            send = window.Nina.enums.Command.LivechatPoll;
        } else {
            var continueDialog = me.cookiesJar.getSession("cont");
            if (continueDialog === "1") {
                send = window.Nina.enums.Command.DialogContinue;
            } else {
                send = window.Nina.enums.Command.DialogFollowUp;
                // This will prevent the UI from following a OutUrl that would be returned
                ad = { Loopback: { OutUrl: { Follow: false }, FollowUp: true } };
            }
        }
        me.ui.sendQuery("", send, ad);
    }

    /**
     * Retrieves the transcript for the current session
     * @returns {void}
     */
    function handleGetTranscript() {
        var sci = me.cookiesJar.getSession("sci");
        if (me.cookiesJar.isOngoingSession()) {
            me.transcript.getTranscript(sci);
        } else {
            errorHandler(me.sessionExpiredMessage);
        }

    }

    /**
     * Called by the JTranscriptService once we receive the transcript
     * @param {window.Nina.ws.Transcript} transcript
     * @returns {void}
     */
    function responseHandler(transcript) {
        me.ui.clearChatHistory();
        var interactions = transcript.getInteractions();
        me.ui.addChatLines(interactions);
        //TODO
        //me.components.chatArea.css("scrollTop", me.components.chatText.prop("scrollHeight"));
    }

    /**
     * Called by the JTranscriptService if an error occurs
     * @param {string} errorMessage
     * @returns {void}
     */
    function errorHandler(errorMessage) {
        me.ui.addChatLine(errorMessage, window.Nina.enums.Author.Error);
    }
};
window.Nina.ui.newSurvey = function (_agentId, _config, _botService, _cookiesJar) {
    "use strict";
    var $ = window.Nina.$, modules = {}, uiElements = {};

    modules.config = _config;
    modules.botService = _botService;
    modules.cookiesJar = _cookiesJar;

    uiElements.banner = $('#' + _agentId + ' .nw_SurveyBanner');
    uiElements.bannerClose = $('<span class="close fa fa-close"></span>');
    uiElements.survey = $('#' + _agentId + ' .nw_Survey');
    uiElements.surveyClose = $('<span class="close fa fa-close"></span>');

    function newBanner() {

        var toggle = "toggle",
            openSurvey = false,
            autoHideTimer = null,
            isVisible = false;

        if (modules.config.bannerAnimation === "slide") {
            toggle = "slideToggle";
        } else if (modules.config.bannerAnimation === "fade") {
            toggle = "fadeToggle"
        }

        uiElements.banner.append(uiElements.bannerClose);

        uiElements.banner.on('click', function () {
            if (openSurvey) {
                modules.botService.sendQuery(window.Nina.enums.Command.StartSurvey, {});
                hideBanner();
            }
        });

        uiElements.bannerClose.on('click', function (event) {
            event.stopPropagation();
            hideBanner();
        });

        function showBanner(text, action) {
            if (typeof text === "string") {
                uiElements.banner.find('.text').html(text);
            }
            if (undefined !== typeof action) {
                openSurvey = !!action;
            }
            uiElements.banner[openSurvey ? "removeClass" : "addClass"]('autoHide');
            uiElements.banner[toggle]();
            isVisible = true;
        }

        function hideBanner() {
            if (autoHideTimer) {
                window.clearTimeout(autoHideTimer);
                autoHideTimer = null;
            }

            uiElements.banner[toggle]();
            isVisible = false;
        }

        function setAutoHideBanner(delay) {
            autoHideTimer = window.setTimeout(hideBanner, delay * 1000)
        }

        return {
            show: showBanner,
            hide: hideBanner,
            isVisible: function () { return isVisible; },
            setAutoHide: setAutoHideBanner
        }
    }

    function newSurvey(params) {

        var surveyIsOpened = false, sendEnabled = true;
        params.SurveyTitle = params.SurveyTitle || "Survey";

        uiElements.survey.find('h2').html(params.SurveyTitle);
        uiElements.survey.find('form').on('submit', function (event) {
            event.preventDefault();
            if (sendEnabled) {
                var params = $(this).serializeArray();
                if (params.length > 0) {
                    sendFormData(params);
                    sendEnabled = false;
                }
            }
        });

        uiElements.survey.find(">div").append(uiElements.surveyClose);
        uiElements.surveyClose.on("click", function () {
            if (modules.cookiesJar.isOngoingSession()) {
                modules.botService.sendQuery(window.Nina.enums.Command.AbortSurvey, {});
            }
            uiElements.survey.fadeOut();
            $("#" + _agentId).trigger(window.Nina.enums.uiEvents.surveyClosed);
        });

        uiElements.survey.on('keyup blur', '[name=verbatim]', function () {
            var trimmed, verbatim = $(this);
            if (verbatim.val().length > modules.config.verbatimMaxLength) {
                trimmed = verbatim.val().substring(0, modules.config.verbatimMaxLength);
                verbatim.val(trimmed);
            }
        });

        function displayQuestions(params) {
            sendEnabled = true;
            if ("SurveyQuestions" in params && typeof params.SurveyQuestions === "array") {

                // multiple questions - not implemented yet
                // TODO: implement multiple questions survey

            } else if ("SurveyQuestion" in params) {
                uiElements.question = uiElements.survey.find('.nw_question');
                var choices = params.SurveyQuestionChoices.split("#"),
                    htmlCode = questionFromTemplate({
                        id: params.SurveyQuestionID,
                        title: params.SurveyQuestion || "No title defined",
                        template: params.SurveyQuestionType || "radio",
                        choices: choices || null
                    });
                uiElements.question.html(htmlCode);
                var button = uiElements.survey.find('[type="submit"]');
                if (params.SurveyButton === "") {
                    button.hide();
                } else {
                    button.val(params.SurveyButton || "Next");
                    button.show();
                }


                if (params.SurveyQuestionType === "radioH") {
                    uiElements.question.addClass("nw_survey_horizontal");

                    var columnSize = [, , "half", "third", "fourth", "fifth", "sixth"][choices.length] || "default";
                    uiElements.question.find("label").addClass(columnSize);

                } else {
                    uiElements.question.removeClass("nw_survey_horizontal");
                }

                if (params.SurveyQuestionType === "radioOther") {

                    uiElements.verbatim = uiElements.question.find("textarea");
                    uiElements.verbatim.attr("disabled", "disabled");
                    uiElements.question.find("input[type='radio']").on("change", function (event) {

                        if ($("input[type='radio']:last").is(":checked")) {
                            uiElements.verbatim.removeAttr("disabled").focus();
                        } else {
                            uiElements.verbatim.html("").attr("disabled", "disabled");
                        }
                    });
                }
            }

            if (!surveyIsOpened) {
                uiElements.survey.fadeIn();
                surveyIsOpened = true;
                $("#" + _agentId).trigger(window.Nina.enums.uiEvents.surveyOpened);
            }

            $("#" + _agentId).trigger(window.Nina.enums.uiEvents.surveyQuestionDisplayed, [params]);


        }

        function sendFormData(data) {
            if (data.length < 1) {
                return false;
            } else {
                var command = window.Nina.enums.Command.DialogFormData;
                // command template is ##SkipAlyze#FormData#Question1=2#AnswerText=choice 2#verbatim=Here's some user text

                for (var index = 0, len = data.length; index < len; index++) {
                    var el = data[index],  // { name: "Question1", value: "1#choice 1" }
                        answer = el.value.split("#");

                    if (answer.length === 2) {
                        command += "#" + el.name + "=" + answer[0] + "#" + el.name + "Text=" + answer[1];
                    } else {
                        command += "#" + el.name + "=" + answer[0];
                    }
                }
                if (modules.cookiesJar.isOngoingSession()) {
                    modules.botService.sendQuery(command, {});
                } else {
                    uiElements.survey.fadeOut();
                    $("#" + _agentId).trigger(window.Nina.enums.uiEvents.surveyClosed);
                }

                $("#" + _agentId).trigger(window.Nina.enums.uiEvents.surveyQuestionSent);
                return true;
            }
        }

        function questionFromTemplate(question) {
            var choices, index, id, name, len, html;

            //generate title
            html = ['<h3>' + question.title + '</h3>'];

            if (question.template.indexOf("radio") === 0 && question.choices) { // we accept radio, radioOther, radioH

                choices = question.choices;

                for (index = 0, len = choices.length; index < len; index++) {
                    id = index + 1;
                    name = (question.template === "radioOther" && index === len - 1) ? "other" : choices[index];
                    html.push('<label><input type="radio" name="' + question.id + '" tabindex="' + id + '" value="' + id + '#' + name.replace(/"/g, "'") + '" />' +
                        choices[index] + '</label>');
                }
                if (question.template === "radioOther") {
                    html.push("<textarea name='verbatim' tabindex='20' maxlength='400'></textarea>");
                }
                if (question.template === "radioH") {
                    html.push('<div class="clear"></div>');
                }
            }

            if (question.template === "stars" && question.choices) {

                choices = question.choices;
                html.push("<div class='nw_Stars'>");
                for (index = 0, len = choices.length; index < len; index++) {
                    id = index + 1;
                    name = choices[index];
                    html.push("<div class='nw_Star fa fa-star-o' tabindex='" + id + "' data-id='" + id + "' data-label='" + name + "'></div>");
                }
                html.push("</div><div class='nw_StarsLabel'>&nbsp;</div>");
            }

            return html.join('');
        }

        function hideSurvey() {
            uiElements.survey.fadeOut();
            surveyIsOpened = false;
            $("#" + _agentId).trigger(window.Nina.enums.uiEvents.surveyClosed);
        }

        return {
            displayQuestions: displayQuestions,
            hide: hideSurvey,
            isVisible: function () { return surveyIsOpened; },
            sendFormData: function (params) { return sendFormData(params); },
            sendEnabled: function (value) {
                if (undefined !== value) {
                    sendEnabled = !!value;
                    return true;
                } else {
                    return sendEnabled;
                }
            }
        }
    }

    var banner = newBanner();
    var survey = null;

    modules.botService.addMessageHandler(function surveyMessageHandler(interaction) {
        var ad = interaction.getAdditionalData();

        if ("SurveyBanner" in ad) {
            var openSurvey = !("SurveyEnded" in ad),
                autoHide = !openSurvey && typeof modules.config.bannerFadesAfter === "number";

            window.setTimeout(function () {
                banner.show(ad.SurveyBanner, openSurvey);

                if (autoHide) {
                    banner.setAutoHide(modules.config.bannerFadesAfter);
                }
            }, 1000);

        }
        if ("SurveyQuestion" in ad && !("SurveyEnded" in ad)) { // this means we receive the next question

            if (!survey) {
                survey = newSurvey(ad);
            }

            survey.displayQuestions(ad);
        }
        if ("SurveyEnded" in ad) {
            survey.hide();
        }
    });

    window.Nina.aSurvey = function (data) {
        return newSurvey(data);
    };

    modules.botService.addSessionEndingHandler(function () {
        if (banner.isVisible()) {
            banner.hide();
        }
        if (survey && survey.isVisible()) {
            survey.hide();
        }
    });

    $("#" + _agentId).on(window.Nina.enums.uiEvents.surveyQuestionDisplayed, function (event, question) {
        if (undefined === typeof question || !question.hasOwnProperty("SurveyQuestionType")) {
            return;
        }
        var questionName = question.SurveyQuestionID || "questionName";

        if (question.SurveyQuestionType === "stars") {
            $(".nw_Star").on("mouseover focus", function (event) {
                var id = $(this).data("id"), label = $(this).data("label");
                $.each($(".nw_Star"), function (index, star) {
                    var s = $(star);
                    s[(s.data("id") <= id) ? "addClass" : "removeClass"]("fa-star");
                    s[(s.data("id") <= id) ? "removeClass" : "addClass"]("fa-star-o");
                });
                $(".nw_StarsLabel").html(label);
            });
            $(".nw_Stars").on("mouseout", function (event) {
                $.each($(".nw_Star"), function (index, star) {
                    $(star).addClass("fa-star-o");
                    $(star).removeClass("fa-star");
                });
                $(".nw_StarsLabel").html("&nbsp;");
            });
            $(".nw_Star").on("click", function () {
                var id = $(this).data("id"), label = $(this).data("label");
                $.each($(".nw_Star"), function (index, star) {
                    var s = $(star);
                    s[(s.data("id") <= id) ? "addClass" : "removeClass"]("nw_StarActive");
                });

                survey.sendFormData([{ name: questionName, value: id + "#" + label }]);
            });
            $(".nw_Star").on("keyup", function (event) {
                if (event.which === 13) {
                    $(this).trigger("click");
                }
            });
        }
    });
    $("#" + _agentId).on(window.Nina.enums.uiEvents.surveyQuestionSent, function (event) {
        $(".nw_Star").off("mouseover");
        $(".nw_Stars").off("mouseout");
    });


    return true;
};

window.Nina.ui.newUIHandler = function newUIHandler(_agentId, _config, _jbotservice, _display) {
    "use strict";
    var $ = window.Nina.$;

    var me = {};
    me.jbotservice = _jbotservice;
    me.display = _display;

    // Config parameters
    me.version = _config.version;
    me.uiType = _config.uiType;
    me.sendUiType = _config.sendUiType;
    me.date = _config.date;
    me.maxInputLength = _config.maxInputLength;
    me.closeWindow = _config.closeWindow;
    me.sendFormDataMessage = _config.sendFormDataMessage;
    me.emptyInputUponSubmission = _config.emptyInputUponSubmission;
    me.ninaChatUi = _config.ninaChatUi;
    me.loadCoBrowsingUrl = _config.loadCoBrowsingUrl;
    me.openCoBrowsingUrlInNewWindow = _config.openCoBrowsingUrlInNewWindow;

    me.text = {};
    me.text.botIsTyping = _config.botIsTyping;
    me.text.csrIsTyping = _config.csrIsTyping;
    me.text.charsLeft = _config.charsLeft;
    me.text.charsLeftPlaceHolder = "##CHARSLEFT##";
    me.text.csrName = "";
    me.text.csrNamePlaceHolder = "##CSRNAME##";

    me.postChatSurveyUrl = "";

    me.id = {};
    me.id.inputText = "#" + _agentId + " .nw_UserInputField";
    me.id.inputButton = "#" + _agentId + " .nw_UserSubmit";
    me.id.chatText = "#" + _agentId + " .nw_ConversationText";
    me.id.closeButton = "#" + _agentId + " .nw_CloseX";
    me.id.statusMessage = "#" + _agentId + " .nw_StatusMessage";

    me.components = {};
    me.components.inputText = null;
    me.components.inputButton = null;
    me.components.chatText = null;
    me.components.closeButton = null;
    me.components.statusMessage = null;

    me.css = {};
    me.css.inputFocus = "nw_UserInputFocus";
    me.css.agentWriting = "nw_AgentWriting";
    me.css.noCharLeft = "nw_NoCharsLeft";
    me.css.sendDisabled = "nw_Disabled";

    me.urlHandler = null;
    me.inputHandlers = [];
    me.canSend = true;
    me.isLivechat = false;
    me.isLivechatWaiting = false;
    me.csrTyping = false;
    me.numUserQueries = 0;

    // Setup jbotservice to send us notifications
    me.jbotservice.addMessageHandler(responseHandler);
    me.jbotservice.addErrorHandler(errorHandler);
    me.jbotservice.setTypingHandler(typingHandler);

    //Populate components
    populateComponents();
    //Setup input form
    setupInputText();
    setupHandlers();

    /**
     * Retrieve the components by IDs
     * @returns {void}
     */
    function populateComponents() {
        me.components.inputText = $(me.id.inputText);
        me.components.inputButton = $(me.id.inputButton);
        me.components.chatText = $(me.id.chatText);
        me.components.closeButton = $(me.id.closeButton);
        me.components.statusMessage = $(me.id.statusMessage);
    }

    /**
     * Setup the click handlers
     * @returns {void}
     */
    function setupHandlers() {
        // The 6.x response rendered always returns dialog links with href="#".  5.x does not return the href
        // attribute at all.

        me.components.chatText.on("click", ".nw_AgentSays  a", function (event) {
            event.preventDefault();
            var link = $(this);
            if (link.data("vtz-link-type") === "Web") {
                // This captures 6.x web links:
                webLinkHandler6x(event);
            } else if (link.data("vtz-link-type") === "Dialog") {
                // This captures 5.6 dialog links:
                dialogLinkHandler(event);
            } else if (!!link.attr("href") && !link.attr("vtz-link-type")) {
                // This captures 5.x web links:
                webLinkHandler5x(event);
            } else {
                // This captures 5.6 dialog links:
                dialogLinkHandler(event);
            }
        });
        // Standard form submission capture.
        me.components.chatText.on("submit", ".nw_AgentSays form:first", function (event) {
            event.preventDefault();
            formContentHandler(event);
        });
        me.components.closeButton.click(closeClickHandler);
        me.components.inputButton.click(submitHandler);
        me.components.inputButton.keyup(keyUpHandler);
        me.components.inputText.keyup(keyUpHandler);
    }

    /**
     * Setup the input text box properties
     * @returns {void}
     */
    function setupInputText() {
        // Set-up input text box

        me.components.inputText.prop("maxlength", me.maxInputLength);
        sendEnabled(false);
    }

    /**
     * This function handles the press of the enter key inside the input text area
     * @param event
     * @return {void}
     */
    function keyUpHandler(event) {
        if (event.which === 13) {
            //Enter key, we should remove the carriage return at the end of the string before sending it
            var s = me.components.inputText.val();
            s = s.replace("\r", "").replace("\n", "");
            me.components.inputText.val(s);
            submitUserText();
        } else {
            // Set user is typing
            me.jbotservice.setUserIsTyping();
            //Another key or a copy / paste which could lead to more text than allowed
            if (me.components.inputText.val().length > me.maxInputLength) {
                me.components.inputText.val(me.components.inputText.val().substr(0, me.maxInputLength));
                me.components.inputText.scrollTop(me.components.inputText.prop("scrollHeight"));
            }
            sendEnabled(true);
        }
        updateStatusMessage(0);
    }

    /**
     * This function is called when the CSR is typing in livechat mode
     * @param state
     * @returns {void}
     */
    function typingHandler(state) {
        me.csrTyping = state;
        updateStatusMessage(0);
    }

    /**
     * Function called to update the status message
     * @param toAdd
     * @returns {void}
     */
    function updateStatusMessage(toAdd) {
        var charLeft = me.maxInputLength - me.components.inputText.val().length;
        me.numUserQueries += toAdd;
        me.numUserQueries = (me.numUserQueries < 0) ? 0 : me.numUserQueries; // += 0;

        me.components.statusMessage.removeClass(me.css.agentWriting);
        me.components.statusMessage.removeClass(me.css.noCharLeft);

        if (me.isLivechat) {
            if (me.csrTyping) {
                me.components.statusMessage.addClass(me.css.agentWriting);
                me.components.statusMessage.text(me.text.csrIsTyping.replace(me.text.csrNamePlaceHolder, me.text.csrname));
            } else {
                me.components.statusMessage.text("");
            }
        } else {
            if (me.numUserQueries > 0) {
                me.components.statusMessage.addClass(me.css.agentWriting);
                me.components.statusMessage.html(me.text.botIsTyping);
            } else {
                if (charLeft < 1) {
                    me.components.statusMessage.addClass(me.css.noCharLeft);
                }
                me.components.statusMessage.text(me.text.charsLeft.replace(me.text.charsLeftPlaceHolder, charLeft));
            }
        }
    }

    /**
     * This function is called when there is an error while calling the webservice
     * @param {string} errorCode
     * @param {string} message
     * @returns {void}
     */
    function errorHandler(errorCode, message) {
        me.display.addChatLine(message, window.Nina.enums.Author.Error);
        updateStatusMessage(-1);
        sendEnabled(true);
    }

    /**
     * This function is called when we receive a webservice answer
     * @param {window.Nina.ws.Interaction} interaction
     * @returns {void}
     */
    function responseHandler(interaction) {

        me.isLivechat = interaction.isLivechatConversation();
        me.isLivechatWaiting = interaction.getLivechatStatus() === 1;

        if (interaction.isLivechatConversation()) {
            me.text.csrname = interaction.getLivechatCSRName();
            me.display.setCSRName(me.text.csrname);
        }

        var ad = interaction.getAdditionalData();

        // dirty fix to avoid display ##EmptyAnswer in 6.0
        if ((me.isLivechat && ad.isEmptyAnswer) || /^##EmptyAnswer/.test(interaction.getResponse())) {
            updateStatusMessage(-1);
            sendEnabled(true);
            return;
        }

        if (interaction.getResponse().length > 0 && interaction.getResponseCode() !== window.Nina.enums.ResponseCode.Command) {
            window.NinaVars.forceReprocessContextItems = false;
            me.display.addChatLine(interaction.getResponse(), interaction.getAuthor());
            handleTooltip();
        }

        if (interaction.getOutUrl().length > 0 && $.isFunction(me.urlHandler)) {
            me.urlHandler(true, interaction.getOutUrl(), undefined, interaction.getOutUrlMetadata());
        }

        if (interaction.getAdditionalData().hasOwnProperty("PostChatSurvey")) {
            me.postChatSurveyUrl = interaction.getAdditionalData().PostChatSurvey;
        }

        if (me.loadCoBrowsingUrl && ad.coBrowsingUrl) {
            if (me.openCoBrowsingUrlInNewWindow) {
                window.open(ad.coBrowsingUrl);
            }
            else if (ad.coBrowsingUrl.trim() !== window.location.href) {
                window.location.href = ad.coBrowsingUrl;
            }
        }

        me.canSend = interaction.canSend();
        sendEnabled(true);
        updateStatusMessage(-1);
    }

    // Will look in the agent's answer to see if it contains tooltips, and render them.
    // PS can create custom tooltips in IQStudio by creating an HTML element with custom attributes:
    // This will show a <span class="nw_tooltip" tooltiptitle="Hey" tooltiptext="I'm a custom tooltip!">custom tooltip</span>
    function handleTooltip() {
        var gap = 15;
        $("#" + _agentId).find('.nw_AgentSays.nw_AgentLastAnswer .nw_tooltip').hover(function (event) {
            var toolTipTitle = $(this).attr('tooltiptitle') || "";
            var toolTip = $(this).attr('tooltiptext');
            $('<span class="nw_tooltipBubble"></span>').html('<div class="nw_tooltipTitle">' + toolTipTitle + '</div> ' + toolTip)
                .appendTo('#' + _agentId)
                .css('top', (event.pageY + gap) + 'px')
                .css('left', (event.pageX + gap) + 'px')
                .fadeIn('slow');
        }, function () {
            $('.nw_tooltipBubble').remove();
        }).mousemove(function (event) {
            // If the tooltip go out of the bottom of the page, place it above the mouse instead
            var top = (event.pageY + gap) > ($('body').scrollTop() + $(window).height() - $('.nw_tooltipBubble').outerHeight()) ?
                (event.pageY - gap - $('.nw_tooltipBubble').outerHeight()) : (event.pageY + gap);
            $('.nw_tooltipBubble')
                .css('top', top - $('body').scrollTop() + 'px')
                .css('left', Math.min(event.pageX + gap, $(window).outerWidth() - $('.nw_tooltipBubble').outerWidth()) + 'px');
        });
    }

    /**
     * Allow / Prevent the user from sending any new message
     * @param status
     */
    function sendEnabled(status) {
        if (status && me.canSend && me.components.inputText.val().length > 0) {
            me.components.inputButton.removeClass(me.css.sendDisabled);
            me.components.inputButton.prop("disabled", false);
        } else {
            me.components.inputButton.addClass(me.css.sendDisabled);
            me.components.inputButton.prop("disabled", true);
        }
    }

    /**
     * This function is called when the user clicks on a web link inside the chat text area
     * @param event
     * @returns {void}
     */
    function webLinkHandler(event, urlAttribute) {
        event.preventDefault();

        // Defensive move in case some unruly existing agent UI is calling this method directly; we default to 5x
        // behavior.
        if (!urlAttribute) {
            urlAttribute = "href";
        }

        if ($.isFunction(me.urlHandler)) {
            var url = $(event.currentTarget).attr(urlAttribute);
            var target = $(event.currentTarget).attr("target");
            me.urlHandler(false, url, target, buildMetaDataFromDataAttributes($(event.currentTarget)));
        }
    }

    /**
     * This function is called when the user clicks on a web link inside the chat text area for a 6x agent.
     */
    function webLinkHandler6x(event) {
        webLinkHandler(event, "data-vtz-browse");
    }

    /**
     * This function is called when the user clicks on a web link inside the chat text area for a 5x agent.
     */
    function webLinkHandler5x(event) {
        webLinkHandler(event, "href");
    }

    /**
     * This function is called when the user clicks on a dialog link inside the chat text area
     * @param event
     * @returns {void}
     */
    function dialogLinkHandler(event) {
        event.preventDefault();
        var target = $(event.currentTarget);
        // Use the code attribute if it's there, otherwise fall back to the text
        var toSend = target.attr("code") || target.text();
        var toDisplay = target.text();

        send(window.Nina.enums.Author.User, toDisplay, toSend, buildMetaDataFromDataAttributes(target));
        me.components.inputText.focus();
    }

    /**
     * Builds a meta data element from the data-* attributes held by the supplied element.  Returns an empty meta data
     * object if the supplied element has no data-* attributes.
     * If the UI is a NinaChat UI, we add the "tap" datasource, to identify in Analytics that
     * the user clicked on the webview
     * @param element The element with attributes.
     */
    function buildMetaDataFromDataAttributes(element) {
        // Build up meta data from the data-* attributes held by the links.
        var metaData = {};

        // Have to do this to get the link attributes back because, surprisingly, the JQuery API for doing this is
        // non-existent.
        for (var i = 0; i < element[0].attributes.length; i++) {
            var currentAttribute = element[0].attributes[i];
            if (currentAttribute.nodeName !== "data-vtz-link-type" && currentAttribute.nodeName.indexOf("data-") === 0) {

                // Add an element to our meta data with the "data-" bit stripped off, per the 6.x contract.
                metaData[currentAttribute.nodeName.substring(5)] = currentAttribute.value;
            }
        }

        if (me.ninaChatUi) {
            metaData.ClientMetaData = { dataSource: "tap" };
        }

        return metaData;
    }

    /**
     * This function is called when the user clicks on the close button
     * @param {JQuery.Event} event
     * @returns {void}
     */
    function closeClickHandler(event) {
        event.preventDefault();
        var pendingQuery = false;
        if (me.isLivechat || me.isLivechatWaiting) {
            //me.jbotservice.stopPolling();
            me.jbotservice.sendQuery(window.Nina.enums.Command.LivechatEnd, { Loopback: { "livechatendconversation": true } });
            pendingQuery = true;
        }
        if (me.postChatSurveyUrl.length > 0) {
            if ($.isFunction(me.urlHandler)) {
                me.urlHandler(false, me.postChatSurveyUrl, "PostChatSurvey");
            }
        }
        if (me.closeWindow) {
            window.close();
        }
        $("#" + _agentId).trigger(window.Nina.enums.uiEvents.uiClose, [pendingQuery]);
    }

    /**
     * This function is called when the user submits a form located inside the content window
     * A form must systematically contain a submit button (this is the only way to send the data of the form, the
     * "regular" send button only sends text contained in the "regular" input text box).
     * A given answer can contain multiple forms but only the data from the elements inside the submitted form will be
     * sent.
     * Inside a given form each element must have a distinct non empty name attribute.
     * For textarea and input text boxes, if the name of the form element is "UserText", then the content will be sent
     * as the user input rather than being sent as additional metadata with the query. If there are multiple or no form
     * elements with the name "UserText", then a specific command is sent instead.
     * We support: input text box, textarea, checkboxes, radio buttons, listboxes, password fields, hidden form fields
     * Their value is sent in sub properties of the additional data node "formData". The sub property name is the name
     * of the form field.
     * @param event
     * @returns {void}
     */
    function formContentHandler(event) {
        event.preventDefault();

        var target = $(event.currentTarget); // This is the form that just got submitted
        var input;
        var name;
        var node;

        var userText = "";
        var toSend = "";
        var formData = {};

        // Input text box
        input = target.find("input:text");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");
            if (name === "UserText") {
                userText = node.val();
                toSend = node.val();
            } else {
                formData[name] = node.val();
            }
        });

        // Hidden form field, never use that as a user input, just send the data along
        input = target.find("input:hidden");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");
            formData[name] = node.val();
        });

        // Password field, never use as a user input, just send the data along. That way it won't appear in the transcript
        input = target.find("input:password");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");
            formData[name] = node.val();
        });

        // Checkboxes, never use as user input. Send array of checked boxes
        input = target.find("input:checkbox:checked");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");

            // If it does not exists, create the array that will contain the list of checked elements
            if (!formData.hasOwnProperty(name) || !$.isArray(formData[name])) {
                formData[name] = [];
            }
            formData[name].push(node.val());
        });

        // Radio button, never used as user input. Need to retrieve the text to display in label field
        input = target.find("input:radio:checked");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");
            formData[name] = node.val();
        });

        input = target.find("textarea");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");
            if (name === "UserText") {
                userText = node.val();
                toSend = node.val().replace("\r", " ").replace("\n", " "); // Don't want any carriage return
            } else {
                formData[name] = node.val();
            }
        });

        input = target.find("select");
        input.each(function (index, value) {
            node = $(value);
            name = node.prop("name");
            // For select form elements, val already returns an array of all the selected elements
            formData[name] = node.val();
        });

        if (target.find('[name="UserText"]').length !== 1) {
            toSend = buildFormDataCommand(formData);
            userText = me.sendFormDataMessage;
        }

        //TODO add check on length of userText

        send(window.Nina.enums.Author.User, userText, toSend, { formData: formData });
    }

    function buildFormDataCommand(formData) {
        var toSend = window.Nina.enums.Command.DialogFormData;
        $.each(formData, function (key, value) {
            var toAppend = "#" + key + "=" + value;

            //TODO: check in WBH code if commands must abide to maxInputLength or not

            if (toSend.length + toAppend.length <= me.maxInputLength) {
                toSend += toAppend;
            }
        });
        return toSend;
    }

    /**
     * This function is called when the user clicks on the send button
     * @param {JQuery.event} event
     * @returns {void}
     */
    function submitHandler(event) {
        event.preventDefault();
        submitUserText();
    }

    function submitUserText() {
        if (me.components.inputButton.hasClass(me.css.sendDisabled)) {
            return;
        }
        var userText = me.components.inputText.val();
        send(window.Nina.enums.Author.User, userText, userText, {});
        if (me.emptyInputUponSubmission) {
            me.components.inputText.val("");
        }
    }

    /**
     * This function sends a user query and displays it as well
     * @param {window.Nina.enums.Author} author
     * @param {string} toDisplay the text that should be displayed in the user interface as the user query
     * @param {string} toSend the text that should be sent to the web service as the user query
     * @param {Object} metadata any additional metadata that should be sent in the user query
     * @returns {void}
     */
    function send(author, toDisplay, toSend, metadata) {
        // If one interaction specified to stop sending, do it.
        if (!me.canSend) {
            return;
        }

        // If something to display, display it
        if (toDisplay.length > 0) {
            me.display.addChatLine(toDisplay, author);
        }

        if (toSend.toLowerCase() === window.Nina.enums.Command.DialogVersion.toLowerCase()) {
            me.display.addChatLine("UI Version: " + me.version + "<br />Client Build date: " + me.date, window.Nina.enums.Author.Agent);
        }


        // Only send something if toSend is not empty
        var result = window.Nina.text.Processors.applyProcessors(toSend, [window.Nina.text.Processors.getWhiteSpaceProcessor()]);
        if (result.length === 0) {
            return;
        }
        updateStatusMessage(1);


        // If additional input handler are available, use them
        $.each(me.inputHandlers, function (index, handler) {
            if ($.isFunction(handler)) {
                $.extend(metadata, handler(toDisplay, toSend, metadata));
            }
        });
        if (me.sendUiType) {
            $.extend(true, metadata, { ClientMetaData: { uiType: me.uiType } });
        }

        me.jbotservice.sendQuery(toSend, metadata);
        sendEnabled(false);
    }

    return /** @lends window.Nina.ui.UIHandler.prototype */ {
        /**
         * Sets the URL handler called when a user clicks on a web link
         * @param {function(boolean,string,string,object):void} handler the URL handler
         * @returns {void}
         */
        setURLHandler: function (handler) {
            me.urlHandler = handler;
        },
        /**
         * Sends a query and display it
         * @param {string} toDisplay text to display in the history
         * @param {string} toSend text to send to the web service as user query
         * @param {Object} metadata additional metadata to send along in the user query
         * @returns {void}
         */
        sendQuery: function (toDisplay, toSend, metadata) {
            send(window.Nina.enums.Author.User, toDisplay, toSend, metadata);
        },
        /**
         * Adds a chat line from a given author
         * @param {string} toDisplay the text to display in the user interface
         * @param {window.Nina.enums.Author} author the source of the message
         * @returns {JQuery}
         */
        addChatLine: function (toDisplay, author) {
            return me.display.addChatLine(toDisplay, author);
        },
        /**
         * Adds an array of lines to the history
         * @param {Array.<Object>} array
         * @returns {void}
         */
        addChatLines: function (array) {
            return me.display.addChatLines(array);
        },
        /**
         * Clears the chat history.
         * @returns {void}
         */
        clearChatHistory: function () {
            return me.display.clearChatHistory();
        },
        /**
         * Add an input handler
         * @param {function(string,string,Object):Object} handler
         * @returns {void}
         */
        addInputHandler: function (handler) {
            me.inputHandlers.push(handler);
        },
        setDisplay: function (display) {
            me.display = display;
        },
        getDisplay: function () {
            return me.display;
        }
    };
};

window.Nina.ui.openInParent = function (url) {
    "use strict";
    // TODO sbellone: I've commented "$.browser" to perform jQuery upgrade.
    //                This code seems deprecated (we don't use popup UIs anymore), so we can completely remove it.

    // Bug in IE8 - it does not support changing the opener url when not in the same domain. Other browsers do, so disable this for IE.
    // Cannot disable it only for IE8 because on a lot of site, IE8 is in compatibility view which identifies as IE7...

    // For iOS devices, we should not try to keep the focus in popup mode since the user won't know the parent has changed

    try {
        if (window.Nina.ui.isIOS()) {
            window.open(url, "agentAnswer");
        } else if (/*!window.Nina.$.browser.msie && */window.opener) {
            window.opener.location.href = url;
        } else {
            window.open(url, "agentAnswer");
            window.focus();
        }
    } catch (e) {
        window.open(url, "agentAnswer");
        if (!window.Nina.ui.isIOS()) {
            window.focus();
        }
    }
};

window.Nina.ui.newURLHandler = function (_popup, _config, _ui, _iosBridge) {
    "use strict";
    var me = {};
    // Config parameters
    me.popup = _popup;

    me.ui = _ui;
    me.honorOutUrl = _config.honorOutUrl;
    me.iosBridge = _iosBridge;

    me.targets = {
        "default": function (isOutUrl, url, metaData) {
            if (me.popup) {
                // Open in parent window if it still exists otherwise new window
                window.Nina.ui.openInParent(url);
                trackUrl(url, "default", false, metaData);
            } else {
                // Send tracking (##Url) and once we receive the outUrl, we'll browse (so that we can track the click)
                if (isOutUrl) {
                    if (me.iosBridge) {
                        me.iosBridge.getBridge().callHandler('goto', url);
                    }
                    else {
                        window.location.href = url;
                    }
                } else {
                    trackUrl(url, "default", true, metaData);
                }
            }
        },
        "_blank": function (isOutUrl, url, metaData) {
            if (me.iosBridge) {
                me.iosBridge.getBridge().callHandler('goto', url);
            }
            else {
                window.open(url, "_blank");
            }
            trackUrl(url, "_blank", false, metaData);
        },
        "_self": function (isOutUrl, url, metaData) {
            if (isOutUrl) {
                if (me.iosBridge) {
                    me.iosBridge.getBridge().callHandler('goto', url);
                }
                else {
                    window.location.href = url;
                }
            } else {
                trackUrl(url, "_self", true, metaData);
            }
        },
        "_parent": function (isOutUrl, url, metaData) {
            if (isOutUrl) {
                window.top.location.href = url;
            } else {
                trackUrl(url, "_parent", true, metaData);
            }
        }
    };

    window.Nina.$.extend(me.targets, _config.additionalTargets);
    me.ui.setURLHandler(openURL);

    return {
        /**
         * Open a URL according to the supported targets -> methods
         * @function
         * @param {boolean} isOutUrl
         * @param {string} url
         * @param {string} target
         * @param {object} outUrlMetaData
         * @returns {void}
         */
        URLHandler: openURL,
        /**
         * Track the URL click with the dialog web service
         * @function
         * @param {string} url
         * @params {string} target
         * @params {boolean} followOutUrl
         * @returns {void}
         */
        trackUrl: trackUrl
    };

    /**
     * Track the URL click with the dialog web service
     * @param {string} url
     * @params {string} target
     * @params {boolean} followOutUrl
     * @params metaData The meta data to be sent along with the request or null if none to be sent.
     * @returns {void}
     */
    function trackUrl(url, target, followOutUrl, metaData) {
        metaData = metaData || {};
        metaData.Loopback = {
            OutUrl: {
                Target: target,
                Follow: followOutUrl
            }
        };

        me.ui.sendQuery("", window.Nina.enums.Command.DialogTrackUrl, metaData);
    }

    /**
     * Open a URL according to the supported targets -> methods
     * @param {boolean} isOutUrl
     * @param {string} url
     * @param {string} target
     * @param {object} outUrlMetaData
     * @returns {void}
     */
    function openURL(isOutUrl, url, target, metaData) {
        var follow;

        if (isOutUrl) {
            target = metaData.hasOwnProperty("Target") ? metaData.Target : "";
            if (metaData.hasOwnProperty("Follow")) {
                follow = (metaData.Follow === "true") ? true : false;
            }
            else {
                follow = me.honorOutUrl;
            }

            // bugfix for 6.0 as the new structure is metaData.OutUrl.Target / metaData.OutUrl.Follow
            if (metaData.hasOwnProperty("OutUrl")) {
                target = ("Target" in metaData.OutUrl) ? metaData.OutUrl.Target : "";
                follow = ("Follow" in metaData.OutUrl && metaData.OutUrl.Follow === "true") ? true : false;
            }

            if (!follow) {
                return;
            }
        }

        if (target === undefined || !me.targets.hasOwnProperty(target)) {
            target = "default";
        }

        me.targets[target](isOutUrl, url, metaData);
    }
};
(function () {
    "use strict";
    var c, di, bs, dh, ui, tr, qs, fm, urlh, pi;

    /*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     *                                               *
     *      Assistant configuration                  *
     *                                               *
     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

    var config = Nina.config.newConfig({
        ws: {
            preprod: {
                base_url: "https://agentqa.nina-nuance.com/BGL-Service_UK-EnglishUS-WebBotRouter/",
                debug: false
            },
            prod: {
                base_url: "https://agent.nina-nuance.com/BGL-Service_UK-EnglishUS-WebBotRouter/",
                debug: false
            },
            timeoutQuery: 15,
            errorMessage: "Error while contacting service",
            urltoolong: "Error request URL too long",
            tooManyQueries: "Too many pending queries",
            maxQueries: 16,
            sendReferrer: false
        },
        cookies: {
            timeoutInteraction: 5 * 60 * 1000,
            lifeTimePersist: 30 * 24 * 60 * 60 * 1000,
            secure: true
        },
        firstMessage: {
            transcriptMessage: "<button type=\"button\" class=\"nw_TranscriptLink\"><span class=\"fa fa-bookmark\"></span> Click here for previous conversation</button> ",
            welcome: function () {
                if (NinaVars.welcome) {
                    var obj = NinaVars.welcome;
                    delete NinaVars.welcome;
                    return $.isArray(obj) ? obj[Math.floor(Math.random() * obj.length)] : obj.toString();
                } else {
                    return "Hi, I'm your virtual assistant. I'm here to assist you with any questions you may have. How can I help you today?";
                }
            }
        },
        ui: {
            maxInputLength: 110
        },
        dom: {
            agentHTML: "<div id=\"nina-block\" class=\"nw_Agent\"> <div class=\"nw_agentHeader\"><div class=\"nw_avatar nw_avatar--online\"></div></div> <div class=\"nw_Dialog\"> <div class=\"nw_Conversation\"> <div id=\"nw_ConversationText\" class=\"nw_ConversationText\" role=\"log\" aria-live=\"polite\"></div> </div> <div class=\"nw_Input\" role=\"form\"> <div class=\"nw_DialogForm\"> <div class=\"nw_logoContainer\"> <div class=\"nw_logo\"></div> </div> <div class=\"nw_UserInputContainer\"> <textarea id=\"nw_UserInputField\" class=\"nw_UserInputField\" title=\"Type your question here\" placeholder=\"Type your question here\"></textarea> <label for=\"nw_UserInputField\" class=\"visually-hidden\">Enter your question here</label> <div class=\"nw_StatusMessage\"></div> <button type=\"submit\" aria-label=\"Send Message\" id=\"nw_UserSubmit\" class=\"nw_UserSubmit\"></button> </div> </div> </div> </div> </div>",
            agentMinHTML: ""
        }
    });

    /*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     *                                               *
     *      Modules instanciation                    *
     *                                               *
     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

    c = Nina.storage.newCookiesJar(config.agentId, config.cookies);
    di = Nina.ui.DOMInject(config.agentId, config.dom);
    bs = Nina.ws.newJBotService(config.ws, c);
    dh = Nina.ui.newDisplayHistory(config.agentId, config.ui, c);
    ui = Nina.ui.newUIHandler(config.agentId, config.ui, bs, dh);
    tr = Nina.ws.newJTranscriptService(config.ws);
    qs = Nina.ws.newJQualificationService(config.ws, c);
    fm = Nina.ui.newFirstMessage(config.firstMessage, c, ui, tr);
    urlh = Nina.ui.newURLHandler(config.popup, config.url, ui);


    if ("UICallback" in Nina) Nina.UICallback({ $: $, cookieJar: c, botservice: bs, uihandler: ui });
    Nina.debug = Nina.helper.newDebug(ui, c);

    // Fallback to png if user has IE8
    if (!Modernizr.svg) {
        $('.nw_logoContainer').children().removeClass('nw_logo');
        $('.nw_logoContainer').children().addClass('nw_logoIE');
    }

    bs.addMessageHandler(function (interaction) {
        if ($('.nw_ConversationText .nw_AgentSays').length === 1) {
            getUniqueId();
            if (typeof lpTag !== "undefined") {
                VAInProgress();
            }
        }
    });

    /**********************************************
     *        SURVEY HANDLING                     *
     **********************************************/

    if (Nina.ui.newSurvey) {
        Nina.ui.newSurvey(config.agentId, config.survey, bs, c);

        Nina.setInputMode = function (mode) {
            setInputMode(mode);
        };

        Nina.showSurvey = function () {
            ui.sendQuery('', 'StartSurvey', {});
        };

        $("#" + config.agentId).on(Nina.enums.uiEvents.surveyOpened, function () {
            setInputMode("survey");
        });
        $("#" + config.agentId).on(Nina.enums.uiEvents.surveyClosed, function () {
            setInputMode("dialog");
        });

        $("#" + config.agentId).on(Nina.enums.uiEvents.surveyQuestionDisplayed, function (event, question) {
            updateInputHeight();
        });

        bs.addSessionEndingHandler(function () {
            setInputMode("dialog");
        });
    }

    var inputMode = "dialog";
    function setInputMode(mode) {
        if (undefined === typeof mode || inputMode === mode) {
            return;
        }

        var elements = {
            survey: $('.nw_Survey'),
            dialog: $('.nw_DialogForm')
        };

        // we resize the conversation box
        var h = elements[mode].outerHeight(true);
        $('.nw_Conversation').css({ 'bottom': h + "px" });
        window.setTimeout(function () {
            var scrollH = $('.nw_Conversation').prop('scrollHeight');
            $('.nw_Conversation').scrollTop(scrollH);
        }, 0);

        /* the current input fades out */
        elements[inputMode].fadeOut(200, function () {
            /* we resize the input area */
            $(".nw_Input").animate({
                height: elements[mode].outerHeight(true) //inputHeight[mode]
            }, 200, function () {
                /* then the new input fades in */
                elements[mode].fadeIn(200, function () {
                    /* then we put the focus in the input field */
                    if (mode === "dialog") {
                        window.setTimeout(function () {
                            $(".nw_UserInputField").focus();
                        }, 0);
                    }
                });
            });
        });

        inputMode = mode;
    }

    function updateInputHeight() {
        var elements = {
            survey: $('.nw_Survey'),
            dialog: $('.nw_DialogForm')
        };

        elements[inputMode].hide();
        $(".nw_Input").animate({
            height: elements[inputMode].outerHeight(true) //inputHeight[mode]
        }, 200, function () {
            elements[inputMode].fadeIn(200);
        });
    }

    function getUniqueId() {
        /*only cookies for this domain and path will be retrieved */
        var cookieName = 'Nina-nina-block-session', sci = "Unknown SCI", cookieJar = document.cookie.split("; ");
        for (var x = 0; x < cookieJar.length; x++) {
            var substringSeparator = cookieJar[x].indexOf('='),
                name = cookieJar[x].substring(0, substringSeparator),
                value = cookieJar[x].substring(substringSeparator + 1);
            if (name === encodeURIComponent(cookieName)) {
                try {
                    var sci = JSON.parse(decodeURIComponent(cookieJar[x].substring(split + 1)))["sci"].split("@")[2];
                } catch (err) {
                    console.warn(err.message);
                } finally {
                    console.log("SCI: " + sci);
                    if (typeof getNinaSessionId === "function") {
                        getNinaSessionId(sci);
                    }
                }
            }
        }
    }
})();

/* ninava=>analytics */
window.NinaVars = window.NinaVars || {};
function SabioWebAnalyticsSender(Solution, EventType, Data) {
    if (window.NinaVars.preprod === true) {
        console.log(Solution);
        console.log(EventType);
        console.log(Data);
    }
    if (typeof ga === 'function') {
        ga('send', 'event', Solution, EventType, Data);
    }
}
function SabioWebAnalyticsHandler(EventType, Data) {
    switch (EventType) {
        case "Displayed":
        case "Declined":
        case "Clicked":
        case "Timed Out":
            switch (Data.engagementType) {
                case 0:
                    var EngType = "PeelingCorner";
                    break;
                case 1:
                    var EngType = "ProActiveInvite";
                    break;
                case 2:
                    var EngType = "Toaster";
                    break;
                case 3:
                    var EngType = "SlideOutInvite";
                    break;
                case 4:
                    break;
                case 5:
                    var EngType = "ChatButton";
                    break;
                case 6:
                    var EngType = "ChatButton";
                    break;
                default:
                    break;
            }
            SabioWebAnalyticsSender('Webchat', EngType + EventType + ' State', 'Online');
            SabioWebAnalyticsSender('Webchat', EngType + EventType + ' EngName', Data.engagementName);
            SabioWebAnalyticsSender('Webchat', EngType + EventType + ' EngID', Data.engagementId);
            SabioWebAnalyticsSender('Webchat', EngType + EventType + ' CampID', Data.campaignId);
            break;
        case "EngagementWindow":
            switch (Data) {
                case "minimized":
                    SabioWebAnalyticsSender('Webchat', 'ChatWindowMinimised', "N/A");
                    break;
                case "maximized":
                    SabioWebAnalyticsSender('Webchat', 'ChatWindowMaximised', "N/A");
                    break;
                case "windowClosed":
                    SabioWebAnalyticsSender('Webchat', 'ChatWindowClosed', "N/A");
                    break;
                default:
                    break;
            }
            break;
        case "Conversation":
            switch (Data.state) {
                case "waiting":
                    var ConvStatus = "VisitorWaitingForChat";
                    break;
                case "chatting":
                    var ConvStatus = "'VisitorJoinedWithAgent";
                    break;
                case "interactive":
                    var ConvStatus = "'InteractiveChat";
                    break;
                case "ended":
                    var ConvStatus = "ChatEnded";
                    break;
                default:
                    break;
            }
            switch (Data.state) {
                case "waiting": // Fallthrough.
                case "chatting": // Fallthrough.
                case "interactive": // Fallthrough.
                case "ended":
                    SabioWebAnalyticsSender('Webchat', ConvStatus + ' ConvID', Data.conversationId);
                    SabioWebAnalyticsSender('Webchat', ConvStatus + ' EngName', Data.engagementName);
                    SabioWebAnalyticsSender('Webchat', ConvStatus + ' EngID', Data.engagementId);
                    SabioWebAnalyticsSender('Webchat', ConvStatus + ' CampID', Data.campaignId);
                    break;
                default:
                    break;
            }
            break;
        case "VAEscalate":
            console.log("Web Analytics Event: Visitor was escalated to " + Data.toLowerCase() + " via the Virtual Assistant");
            switch (Data) {
                case "chat":
                    SabioWebAnalyticsSender('VA', 'ChatViaVA SessionID', window.NinaVars.SabioNinaSessionID);
                    break;
                case "voice":
                    SabioWebAnalyticsSender('VA', 'VoiceViaVA SessionID', window.NinaVars.SabioNinaSessionID);
                    break;
                case "email":
                    SabioWebAnalyticsSender('VA', 'EmailViaVA SessionID', window.NinaVars.SabioNinaSessionID);
                    SabioWebAnalyticsSender('VA', 'EmailViaVA Email', window.NinaVars.SabioNinaSessionID);
                    break;
                case "url":
                    SabioWebAnalyticsSender('VA', 'ClickedLinkInVA SessionID', window.NinaVars.SabioNinaSessionID);
                    SabioWebAnalyticsSender('VA', 'ClickedLinkInVA Link', window.NinaVars.SabioNinaSessionID);
                    break;
                default:
            }
            break;
        default:
    }
}
/* ninava=>livechatintegration */
window.NinaVars = window.NinaVars || {};
function getNinaSessionId(sessionId) {
    window.NinaVars.SabioNinaSessionID = sessionId;
    if (window.NinaVars.preprod === true) {
        console.log("Session ID: " + sessionId);
    }
    SabioWebAnalyticsSender('VA', 'VisitorStartedChat SessionID', window.NinaVars.SabioNinaSessionID);
    setTimeout(function () { VAInProgress(); }, 1000);
}
function SetLiveChatAvailability(EngagementID, EngagementState) {
    window.NinaVars.LP_EngagementID = EngagementID;
    window.NinaVars.LP_EngagementType = EngagementState;
}
function getNinaSCI() {
    var cookieName = 'Nina-nina-block-session';
    var cookieJar = document.cookie.split("; ");
    for (var x = 0; x < cookieJar.length; x++) {
        var split = cookieJar[x].indexOf('=');
        var name = cookieJar[x].substring(0, split);
        if (name === encodeURIComponent(cookieName)) {
            try {
                var sci = JSON.parse(decodeURIComponent(cookieJar[x].substring(split + 1)))["sci"];
                return sci;
            } catch (e) {
                return "Unknown SCI";
            }
        }
    }
}
function VAInProgress() {
    if (window.NinaVars.preprod === true) {
        console.log('Invoked VAInProgress');
    }
    lpTag.sdes = lpTag.sdes || [];
    lpTag.sdes.push(
        {
            "type": "service",
            "service": {
                "topic": "VA",
                "status": 1
            }
        }
    );
}
function VAEscalate(contactType) {
    if (window.NinaVars.preprod === true) {
        console.log('Invoked VAEscalate(' + contactType + ')');
    }
    lpTag.sdes = lpTag.sdes || [];
    lpTag.sdes.push({
        "type": "service",
        "service": {
            "topic": "VA",
            "status": 0
        }
    });

    SabioWebAnalyticsHandler("VAEscalate", contactType);

    switch (contactType) {
        case "chat":

            var SearchKeyword = document.querySelector('#nina-block .nw_UserSays').textContent || document.querySelector('#nina-block .nw_UserSays').innerText;
            SearchKeyword = SearchKeyword.substr(5, SearchKeyword.length - 5);
            lpTag.sdes.push(
                {
                    "type": "searchInfo",
                    "keywords": [SearchKeyword]
                }
            );
            lpTag.sdes.push(
                {
                    "type": "personal",
                    "personal": {
                        "company": document.querySelector('#nina-block .nw_ConversationText').innerHTML,
                        "contacts": [
                            {
                                "email": getNinaSCI(),
                                "phone": SearchKeyword
                            }
                        ]
                    }
                }
            );
            ClickChatButton(SearchKeyword);
            break;
        default:
            break;
    }
}
function ClickChatButton(searchKeyword) {
    if (lpTag && lpTag.taglets && lpTag.taglets.rendererStub) {
        var clicked = lpTag.taglets.rendererStub.click(window.NinaVars.LP_EngagementID, { preChatLines: [] });
    }
}
/* ninava=>lpbinders */
window.NinaVars = window.NinaVars || {};
var livePersonTag = livePersonTag || {};
livePersonTag.initialise();
lpTag.events.bind("LP_OFFERS", "OFFER_DISPLAY", function (eventData, eventInfo) {
    if (eventData.engagementType === 5) {
        WebChatDomModified(eventData);
    }
});
lpTag.events.bind("LP_OFFERS", "OFFER_IMPRESSION", function (eventData, eventInfo) {
    switch (eventData.engagementType) {
        case 5:
            SetLiveChatAvailability(eventData.engagementId, eventData.state);
            if (eventData.engagementName.substr(0, 3) == "VA|") {
                var arr = eventData.engagementName.split('|')
                var skillName = arr[1];
                var engId = eventData.engagementId;
                window.NinaVars.chatData.skillName = skillName;
                window.NinaVars.chatData.engId = engId;
                console.log(window.NinaVars.chatData);
            }
            if (eventData.engagementName.substr(0, 3) != "VA|") {
                SabioWebAnalyticsHandler("Displayed", eventData);
            }
            break;
        default:
            SabioWebAnalyticsHandler("Displayed", eventData);
    }
});
lpTag.events.bind("LP_OFFERS", "OFFER_CLICK", function (eventData, eventInfo) {
    if (typeof window.LPButtonClick === "undefined" || window.LPButtonClick === true) {
        SabioWebAnalyticsHandler("Clicked", eventData);
    }
});
lpTag.events.bind("LP_OFFERS", "OFFER_DECLINED", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("Declined", eventData);
});
lpTag.events.bind("LP_OFFERS", "OFFER_TIMEOUT", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("Timed Out", eventData);
});
lpTag.events.bind("lpUnifiedWindow", "maximized", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("EngagementWindow", eventInfo.eventName);
});
lpTag.events.bind("lpUnifiedWindow", "minimized", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("EngagementWindow", eventInfo.eventName);
});
lpTag.events.bind("lpUnifiedWindow", "windowClosed", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("EngagementWindow", eventInfo.eventName);
});
lpTag.events.bind("lpUnifiedWindow", "conversationInfo", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("Conversation", eventData);
});
lpTag.events.bind("lpUnifiedWindow", "windowClosed", function (eventData, eventInfo) {
    SabioWebAnalyticsHandler("EngagementWindow", eventInfo.eventName);
});
lpTag.events.bind("lp_sdes", "VAR_ADDED", function (eventData, eventInfo) {
    switch (eventData.type) {
        case "service":
            if (eventData.service.status === 0) {
                window.LPButtonClick = false;
            }
            break;
        default:
            break;
    }
});
var lpSiteId = "";
if (typeof lpTag !== "undefined") {
    lpSiteId = lpTag.site;
} else {
    lpSiteId = "Unknown";
}
window.NinaVars.chatData = {
    "skillName": "",
    "engId": 0,
    "lpSiteId": lpSiteId
};

/* liveengage engagement attribute builder */
var livePersonTag = livePersonTag || {};
livePersonTag.buildAttributes = function (attributes) {
    if (attributes) {
        window.lpTag = window.lpTag || {};
        livePersonTag.initialise();
        window.lpTag.section = attributes.Section;

        lpTag.events.bind("LP_OFFERS", "OFFER_DISPLAY",
            function (eventData, eventInfo) {
                if (eventData.engagementType === 5) {
                    WebChatDomModified(eventData);
                }
            });
        lpTag.events.bind("lpUnifiedWindow", "conversationInfo", function (eventData, eventInfo) {
            if (eventData.state === "ended") {
                lpTag.sdes.send({
                    "type": "service",
                    "service": {
                        "topic": "VA",
                        "status": 0,
                        "category": attributes.ActionName
                    }
                });
            }
        });
        lpTag.events.bind("lpUnifiedWindow", "windowClosed", function (eventData, eventInfo) {
            lpTag.sdes.send({
                "type": "service",
                "service": {
                    "topic": "VA",
                    "status": 0,
                    "category": attributes.ActionName
                }
            });
        });
        lpTag.sdes.push(
            {
                "type": "ctmrinfo",
                "info": {
                    "ctype": attributes.Ctype,
                    "imei": attributes.SessionId
                }
            }
        );
        lpTag.sdes.push(
            {
                "type": "prodView",
                "products": [
                    {
                        "product": {
                            "name": attributes.PolicyType,
                            "category": attributes.PolicyStatus,
                            "sku": attributes.PolicyId
                        }
                    }
                ]
            }
        );
        lpTag.sdes.push(
            {
                "type": "service",
                "service": {
                    "topic": "VA",
                    "status": attributes.VaStatus,
                    "category": attributes.ActionName
                }
            }
        );
        lpTag.sdes.push({
            "type": "personal",
            "personal": {
                "firstname": attributes.CustomerName
            }
        });
        if (attributes.Leads && attributes.Leads.length > 0) {
            if (!window.sessionStorage.getItem("lp_Leads")) {
                for (var i = 0; i < attributes.Leads.length; i++) {
                    lpTag.sdes.push({
                        "type": "lead",
                        "lead": {
                            "topic": attributes.Leads[i].Topic,
                            "leadId": attributes.Leads[i].QuoteId
                        }
                    });
                }
                window.sessionStorage.setItem("lp_Leads", JSON.stringify(attributes.Leads));
            }
        }
    }
};
livePersonTag.sendPurchaseAttribute = function (policyData, brand, product) {
    window.lpTag = window.lpTag || {};
    lpTag.sdes = lpTag.sdes || [];
    lpTag.sdes.push({
        "type": "purchase",
        "total": 0,
        "orderId": policyData.PolicyId,
        "cart": {
            "products": [
                {
                    "product": {
                        "name": product,
                        "category": brand,
                        "sku": policyData.QuoteId
                    }
                }
            ]
        }
    });
};