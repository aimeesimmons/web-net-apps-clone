﻿var bgl = window.bgl || {};
bgl.test = bgl.test || {};

bgl.test.utilities = (function() {

    var storageTable = {};

    var mockup = (function() {
        return {
            getItem: function(key) {
                return key in storageTable ? storageTable[key] : null;
            },
            setItem: function(key, value) {
                storageTable[key] = value.toString();
            },
            removeItem: function(key) {
                delete storageTable[key];
            },
            clear: function() {
                storageTable = {}
            }
        };
    })();

    function mockSessionStorage() {
        if (window.sessionStorage == undefined) {
            Object.defineProperty(window,
                'sessionStorage',
                { value: mockup, configurable: true, enumerable: true, writable: true });
        }
    }

    return {
        mockSessionStorage: mockSessionStorage
    };

})();