﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Common.BaseApplication.Attributes;

namespace BGL.Web.BudgetInsurance.MotorQuote
{
    [ExcludeFromCodeCoverage]
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new BglHandleErrorAttribute());
            filters.Add(new BackRedirectAttribute());
            filters.Add(new AjaxRequestAttribute());
        }
    }
}
