﻿using System.Linq;
using System.Web;
using BGL.Common.DependencyInjection;
using Owin;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using BGL.Data.ApiHandlers.Interfaces;

namespace BGL.Web.BudgetInsurance.MotorQuote
{
    public partial class Startup
    {
        private readonly Container _container = SimpleInjectorInstaller.Container;

        public void ConfigureRequestAccessor(IAppBuilder app)
        {
            app.Use((context, next) =>
            {
                HttpContextBase httpContext = context.Get<HttpContextBase>(typeof(HttpContextBase).FullName);
                using (AsyncScopedLifestyle.BeginScope(_container))
                {
                    var requestAccessor = _container.GetInstance<IRequestAccessor>();

                    requestAccessor.Headers = httpContext.Request.ServerVariables.AllKeys
                    .ToDictionary(x => x, y => httpContext.Request.ServerVariables[y]);
                }

                return next();
            });
        }
    }
}