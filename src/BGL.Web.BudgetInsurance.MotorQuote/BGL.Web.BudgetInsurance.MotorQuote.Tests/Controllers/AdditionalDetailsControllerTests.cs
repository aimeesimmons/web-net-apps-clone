﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.BudgetInsurance.MotorQuote.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AdditionalDetailsControllerTests
    {
        // Arrange
        private AdditionalDetailsController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new AdditionalDetailsController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }
    }
}
