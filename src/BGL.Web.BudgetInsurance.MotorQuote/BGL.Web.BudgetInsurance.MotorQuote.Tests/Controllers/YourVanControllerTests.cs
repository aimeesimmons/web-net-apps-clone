﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.BudgetInsurance.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class YourVanControllerTests
    {
        private YourVanController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new YourVanController();
        }

        [Test]
        public void VehicleLookup()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.VehicleLookup());
        }

        [Test]
        public void VehicleDetails()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.VehicleDetails());
        }

        [Test]
        public void OvernightParking()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.OvernightParking());
        }
    }
}
