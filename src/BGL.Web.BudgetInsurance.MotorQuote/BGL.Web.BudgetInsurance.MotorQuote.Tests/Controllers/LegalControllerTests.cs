﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.BudgetInsurance.MotorQuote.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LegalControllerTests
    {
        private LegalController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new LegalController();
        }

        [Test]
        public void PrePriceImportantInformation()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.PrePriceImportantInformation());
        }

        [Test]
        public void PostPriceImportantInformation()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.PostPriceImportantInformation());
        }

        [Test]
        public void Direct()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Direct());
        }

        [Test]
        public void AboutUs()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.AboutUs());
        }

        [Test]
        public void DirectPrePriceLegal()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.DirectPrePriceLegal());
        }

        [Test]
        public void QuoteAcceptanceInformation()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.QuoteAcceptanceInformation());
        }
    }
}
