﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.BudgetInsurance.MotorQuote.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PaymentControllerTests
    {
        // Arrange
        private PaymentController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new PaymentController();
        }

        [Test]
        public void PaymentSummary()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.PaymentSummary());
        }

        [Test]
        public void Annual()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Annual());
        }

        [Test]
        public void AnnualSummary()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.AnnualSummary());
        }

        [Test]
        public void MonthlyInstalment()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MonthlyInstalment());
        }

        [Test]
        public void MonthlyInstalmentSummary()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MonthlyInstalmentSummary());
        }

        [Test]
        public void MonthlyDeposit()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MonthlyDeposit());
        }

        [Test]
        public void MonthlyDepositSummary()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MonthlyDepositSummary());
        }

        [Test]
        public void Unsuccessful()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Unsuccessful());
        }

        [Test]
        public void AllDone()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.AllDone());
        }
    }
}
