﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.BudgetInsurance.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ContactDataControllerTests
    {
        private ContactDataController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new ContactDataController();
        }

        [Test]
        public void HoustonContactData()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.HoustonContactData());
        }
    }
}
