﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.BudgetInsurance.MotorQuote.Controllers;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PricePresentationControllerTests
    {
        // Arrange
        private PricePresentationController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new PricePresentationController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }

        [Test]
        public void WhatsIncluded()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.WhatsIncluded());
        }

        [Test]
        public void WhatsIncludedMVT()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.WhatsIncludedMVT());
        }
    }
}
