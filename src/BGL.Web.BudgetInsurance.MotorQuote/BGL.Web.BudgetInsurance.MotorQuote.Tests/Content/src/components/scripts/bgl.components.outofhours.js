﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.outofhours = bgl.components.outofhours || {};

bgl.components.outofhours = (function () {
    var configuration;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        var componentId = configuration.Id.toLowerCase();

        $(document).on("submit", "[data-component-id='" + configuration.Id + "'] form",
            function (event) {
                event.preventDefault();

                bgl.common.validator.init($(this));
                bgl.common.validator.submitForm($(this), configuration);
            });

        $(document).on("input", "textarea[data-id='" + componentId + "-message']", function () {
            $("[data-id='" + componentId + "-charactersRemaining']").html(300 - $(this).val().length);
        });

        $(document).on("click", "a[data-id='" + componentId + "-close']", function () {
            window.close();
        });

        $(document).on("click", "a[data-id='" + componentId + "-cross']", function () {
            window.close();
        });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.outofhours;
}
