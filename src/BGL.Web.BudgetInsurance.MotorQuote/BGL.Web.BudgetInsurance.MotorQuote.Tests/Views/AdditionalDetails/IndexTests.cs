﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.AdditionalDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "AdditionalDetails";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("AdditionalDetails", GetComponent("AdditionalDetails"));
        }

        [Test]
        public void AdditionalDetailsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["AdditionalDetails"]));
        }

        [TestCase("AdditionalDetails", "Id", "\"AdditionalDetails\"")]
        [TestCase("AdditionalDetails", "NextPageUrl", "Url.Action(\"PaymentSummary\", \"Payment\"),")]
        [TestCase("AdditionalDetails", "PreviousPageUrl", "Url.Action(\"Index\", \"Review\"),")]
        [TestCase("AdditionalDetails", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("AdditionalDetails", "ViewOptions", "new ViewOptionsConfiguration")]
        [TestCase("AdditionalDetails", "NcdSourceThisVehicle", "NcdSource.AcceptableProofExperiencedDriver,")]
        [TestCase("AdditionalDetails", "NcdSourcePreviousVehicle", "NcdSource.AcceptableProofExperiencedDriver,")]
        [TestCase("AdditionalDetails", "NcdSourceCompanyCar", "NcdSource.CompanyCarExperiencedDriver")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}