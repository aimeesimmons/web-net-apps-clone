﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.PricePresentation
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "PricePresentation";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("WelcomeQuote", GetComponent("WelcomeQuote"));
            Components.Add("CoverList", GetComponent("CoverList"));
            Components.Add("CoverRange", GetComponent("CoverRange"));
        }

        [Test]
        public void WelcomeQuoteComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["WelcomeQuote"]));
        }

        [Test]
        public void CoverListComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverList"]));
        }

        [Test]
        public void CoverRangeComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverRange"]));
        }

        [TestCase("CoverList", "Id", "\"CoverWidgetCoverLevel\",")]
        [TestCase("CoverRange", "Id", "\"CoverRange\",")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}