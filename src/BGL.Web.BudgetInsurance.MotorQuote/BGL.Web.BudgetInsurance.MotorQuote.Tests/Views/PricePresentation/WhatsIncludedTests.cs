﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.PricePresentation
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class WhatsIncludedTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "PricePresentation";
            ViewName = "WhatsIncluded";
            Document = GetDocument();
            Components.Add("CoverBubbles", GetComponent("CoverBubbles"));
            Components.Add("CoverLevelContent", GetComponent("CoverLevelContent"));
            Components.Add("MotorLegalProtectionContent", 
                GetDocumentPart("bool.TryParse(Html.ComponentRenderTermsAndConditionsNeedToShowLegalProtectionSection(new ComponentTermsAndCondsConfiguration",
                    "}).ToString(), out var needToPresentLegalProtectionWording);"));
        }

        [Test]
        public void CoverBubblesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverBubbles"]));
        }

        [Test]
        public void CoverLevelContentComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverLevelContent"]));
        }

        [Test]
        public void MotorLegalProtectionContentComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["MotorLegalProtectionContent"]));
        }

        [TestCase("CoverBubbles", "Id", "\"WhatsIncluded\",")]
        [TestCase("CoverBubbles", "CoverFeaturePriority", "new Dictionary<string, CoverFeatureItem>{{FeatureServiceConstants.CoverLevel, new CoverFeatureItem { ViewName = \"CoverLevel\" } },{ FeatureServiceConstants.LegalProtection, new CoverFeatureItem { ContentUrl = Url.Action(\"LegalProtection\", \"CoverFeatures\") } },{ FeatureServiceConstants.DriveOtherVehicles, new CoverFeatureItem { ContentUrl = Url.Action(\"DriveOtherCars\", \"CoverFeatures\") } },{ FeatureServiceConstants.CourtesyVehicle, new CoverFeatureItem { ContentUrl = Url.Action(\"CourtesyCar\", \"CoverFeatures\") } },{ FeatureServiceConstants.UninsuredDriverPromise, new CoverFeatureItem { ContentUrl = Url.Action(\"UninsuredDriverPromise\", \"CoverFeatures\") } },{ FeatureServiceConstants.VandalismPromise, new CoverFeatureItem { ContentUrl = Url.Action(\"VandalismPromise\", \"CoverFeatures\") } },{ FeatureServiceConstants.WindscreenCover, new CoverFeatureItem { ContentUrl = Url.Action(\"WindscreenCover\", \"CoverFeatures\") } },{ FeatureServiceConstants.DrivingAbroad, new CoverFeatureItem { ContentUrl = Url.Action(\"DrivingAbroad\", \"CoverFeatures\") } },{ FeatureServiceConstants.PersonalBelongings, new CoverFeatureItem { ContentUrl = Url.Action(\"PersonalBelongings\", \"CoverFeatures\") } },{ FeatureServiceConstants.ChildSeatCover, new CoverFeatureItem { ContentUrl = Url.Action(\"ChildSeatCover\", \"CoverFeatures\") } },{ FeatureServiceConstants.EmergencyTransport, new CoverFeatureItem { ContentUrl = Url.Action(\"EmergencyTransport\", \"CoverFeatures\") } },{ FeatureServiceConstants.MedicalExpenses, new CoverFeatureItem { ContentUrl = Url.Action(\"MedicalExpenses\", \"CoverFeatures\") } },{ FeatureServiceConstants.PersonalAccidentBenefits, new CoverFeatureItem { ContentUrl = Url.Action(\"PersonalAccidentBenefits\", \"CoverFeatures\") } },{ FeatureServiceConstants.ReplacementLocks, new CoverFeatureItem { ContentUrl = Url.Action(\"ReplacementLocks\", \"CoverFeatures\") } },{ FeatureServiceConstants.AudioEquipment, new CoverFeatureItem { ContentUrl = Url.Action(\"AudioEquipment\", \"CoverFeatures\") } }},")]
        [TestCase("CoverBubbles", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("CoverLevelContent", "Id", "\"CoverLevelContent\",")]
        [TestCase("CoverLevelContent", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("MotorLegalProtectionContent", "Id", "\"SalesTermsAndConditionsLegalProtection\",")]
        [TestCase("MotorLegalProtectionContent", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}