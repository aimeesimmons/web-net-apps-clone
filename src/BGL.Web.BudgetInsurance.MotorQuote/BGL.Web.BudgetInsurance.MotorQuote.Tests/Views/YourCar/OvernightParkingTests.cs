﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourCar
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class OvernightParkingTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCar";
            ViewName = "OvernightParking";
            Document = GetDocument();
            Components.Add("OvernightParking", GetComponent("OvernightParking"));
        }

        [Test]
        public void OvernightParkingComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["OvernightParking"]));
        }

        [TestCase("OvernightParking", "Id", "\"OvernightParking\"")]
        [TestCase("OvernightParking", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("OvernightParking", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("OvernightParking", "DataStoreKey", "\"YourCarParking\",")]
        [TestCase("OvernightParking", "NextStepUrl", "Url.Action(\"Index\", \"YourCover\"),")]
        [TestCase("OvernightParking", "PreviousStepUrl", "$\"{Url.Action(\"VehicleDetails\", \"YourCar\")}#value\"")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}