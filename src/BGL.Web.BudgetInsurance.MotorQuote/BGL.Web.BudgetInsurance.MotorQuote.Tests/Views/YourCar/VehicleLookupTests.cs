﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourCar
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class VehicleLookupTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCar";
            ViewName = "VehicleLookup";
            Document = GetDocument();
            Components.Add("VehicleSearching", GetComponent("VehicleSearching"));
        }

        [Test]
        public void VehicleSearchingComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["VehicleSearching"]));
        }

        [TestCase("VehicleSearching", "Id", "\"VehicleDetails\",")]
        [TestCase("VehicleSearching", "ManufactureListInUpperCase", "new List<string>\r\n            {\r\n                \"BMW\", \"SEAT\", \"MAN\"\r\n            },")]
        [TestCase("VehicleSearching", "ManufacturerKeys", "new List<string>\r\n            {\r\n                \"FO\",\r\n                \"VA\",\r\n                \"VW\",\r\n                \"PE\",\r\n                \"NI\",\r\n                \"BM\",\r\n                \"R1\",\r\n                \"CI\",\r\n                \"TO\",\r\n                \"AU\",\r\n                \"ME\",\r\n                \"HO\",\r\n                \"FI\",\r\n            },")]
        [TestCase("VehicleSearching", "WrongVehicleTypeForVanInsValidationErrorMessage", "BrandConstants.WrongVehicleTypeForVanInsValidationErrorMessage,")]
        [TestCase("VehicleSearching", "VanErrorMessageUrl", "BrandConstants.VanErrorMessageUrl,")]
        [TestCase("VehicleSearching", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("VehicleSearching", "SummaryButtonText", "\"Continue\",")]
        [TestCase("VehicleSearching", "ModificationButtonText", "\"Continue\",")]
        [TestCase("VehicleSearching", "DataStoreKey", "\"YourCar\"")]
        [TestCase("VehicleSearching", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("VehicleSearching", "NextStepUrl", "Url.Action(\"VehicleDetails\", \"YourCar\"),")]
        [TestCase("VehicleSearching", "PreviousStepUrl", "Url.Action(\"AdditionalDrivers\", \"YourDetails\"),")]
        [TestCase("VehicleSearching", "ComponentUrl", "Url.Action(\"VehicleDetails\", \"YourCar\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}