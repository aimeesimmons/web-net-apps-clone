﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.OtherDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PaymentsRegularityTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "OtherDetails";
            ViewName = "PaymentsRegularity";
            Document = GetDocument();
            Components.Add("PaymentsRegularity", GetComponent("PaymentsRegularity"));
        }

        [Test]
        public void PaymentsRegularityComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["PaymentsRegularity"]));
        }

        [TestCase("PaymentsRegularity", "Id", "\"PaymentsRegularity\"")]
        [TestCase("PaymentsRegularity", "NextPageUrl", "Url.Action(\"AndFinally\", \"OtherDetails\"),")]
        [TestCase("PaymentsRegularity", "DataStoreKey", "\"OtherDetails\",")]
        [TestCase("PaymentsRegularity", "PreviousPageUrl", "Url.Action(\"OtherVehicles\", \"YourCover\"),")]
        [TestCase("PaymentsRegularity", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}