﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class OtherVehiclesTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "OtherVehicles";
            Document = GetDocument();
            Components.Add("CoverDetailsOtherVehicles", GetComponent("CoverDetailsOtherVehicles"));
        }

        [Test]
        public void CoverDetailsOtherVehiclesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsOtherVehicles"]));
        }

        [TestCase("CoverDetailsOtherVehicles", "Id", "\"OtherVehicles\",")]
        [TestCase("CoverDetailsOtherVehicles", "DataStoreKey", "\"OtherVehicles\",")]
        [TestCase("CoverDetailsOtherVehicles", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsOtherVehicles", "PreviousStepUrl", "Url.Action(\"NcdSource\", \"YourCover\"),")]
        [TestCase("CoverDetailsOtherVehicles", "BackButtonUrl", "Url.Action(\"NcdYears\", \"YourCover\"),")]
        [TestCase("CoverDetailsOtherVehicles", "NextStepUrl", "Url.Action(\"PaymentsRegularity\", \"OtherDetails\")")]
        [TestCase("CoverDetailsOtherVehicles", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}