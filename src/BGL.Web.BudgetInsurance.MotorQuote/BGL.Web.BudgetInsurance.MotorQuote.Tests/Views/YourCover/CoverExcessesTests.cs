﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class CoverExcessesTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "CoverExcesses";
            Document = GetDocument();
            Components.Add("CoverExcess", GetComponent("CoverExcess"));
        }

        [Test]
        public void CoverExcessComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverExcess"]));
        }

        [TestCase("CoverExcess", "Id", "\"CoverExcess\",")]
        [TestCase("CoverExcess", "DataStoreKey", "\"CoverDetailsExcesses\",")]
        [TestCase("CoverExcess", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverExcess", "NextStepUrl", "Url.Action(\"CoverStartDate\", \"YourCover\"),")]
        [TestCase("CoverExcess", "PreviousStepUrl", "Url.Action(\"CoverLevel\", \"YourCover\"),")]
        [TestCase("CoverExcess", "BackButtonUrl", "Url.Action(\"CoverLevel\", \"YourCover\"),")]
        [TestCase("CoverExcess", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}