﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LayoutNoSessionTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_LayoutNoSession";
            Document = GetDocument();
            Components.Add("GoogleTagManager", GetComponent("GoogleTagManager"));
            Components.Add("GoogleTagManagerDataLayer", GetComponent("GoogleTagManagerDataLayer"));
            Components.Add("GoogleTagManagerTagManager", GetComponent("GoogleTagManagerTagManager"));
            Components.Add("GoogleTagManagerLaunchScript", GetComponent("GoogleTagManagerLaunchScript"));
        }

        [Test]
        public void GoogleTagManagerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManager"]));
        }

        [Test]
        public void GoogleTagManagerDataLayerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerDataLayer"]));
        }

        [Test]
        public void GoogleTagManagerTagManagerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerTagManager"]));
        }

        [Test]
        public void GoogleTagManagerLaunchScriptRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerLaunchScript"]));
        }

        [TestCase("GoogleTagManager", "Id", "\"GoogleTagManager\",")]
        [TestCase("GoogleTagManager", "GtmConfiguration", "config.GetGtmConfiguration(),")]
        [TestCase("GoogleTagManager", "IgnoreSource", "true,")]
        [TestCase("GoogleTagManager", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("GoogleTagManagerDataLayer", "QuoteTier", "\"Standard\"")]
        [TestCase("GoogleTagManagerTagManager", "TagManagerScriptUrl", "ConfigurationManager.AppSettings[\"TagManagerScriptUrl\"]")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}