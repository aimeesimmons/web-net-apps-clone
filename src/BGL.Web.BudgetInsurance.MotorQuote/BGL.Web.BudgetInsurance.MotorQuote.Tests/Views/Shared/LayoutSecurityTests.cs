﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LayoutSecurityTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_LayoutSecurity";
            Document = GetDocument();
            Components.Add("HoustonConfiguration", GetDocumentPart("var componentHoustonConfiguration = new ComponentHoustonConfiguration", "};"));
            Components.Add("GoogleTagManager", GetComponent("GoogleTagManager"));
            Components.Add("Houston", GetDocumentPart("@Html.ComponentRenderHouston", "(componentHoustonConfiguration)"));
            Components.Add("GoogleTagManagerDataLayer", GetComponent("GoogleTagManagerDataLayer"));
            Components.Add("GoogleTagManagerTagManager", GetComponent("GoogleTagManagerTagManager"));
            Components.Add("GoogleTagManagerLaunchScript", GetComponent("GoogleTagManagerLaunchScript"));
        }

        [Test]
        public void HoustonConfigurationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["HoustonConfiguration"]));
        }

        [Test]
        public void GoogleTagManagerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManager"]));
        }

        [Test]
        public void HoustonComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Houston"]));
        }

        [Test]
        public void GoogleTagManagerDataLayerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerDataLayer"]));
        }

        [Test]
        public void GoogleTagManagerTagManagerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerTagManager"]));
        }

        [Test]
        public void GoogleTagManagerLaunchScriptRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerLaunchScript"]));
        }

        [TestCase("HoustonConfiguration", "Id", "\"Houston\",")]
        [TestCase("HoustonConfiguration", "Brand", "vaConfig.BrandName,")]
        [TestCase("HoustonConfiguration", "Application", "vaConfig.Application,")]
        [TestCase("HoustonConfiguration", "JourneySection", "string.Empty,")]
        [TestCase("HoustonConfiguration", "Product", "product,")]
        [TestCase("HoustonConfiguration", "Journey", "isVan ? \"Van\" : \"Car\",")]
        [TestCase("HoustonConfiguration", "ContactDataUrl", "Url.Action(\"HoustonContactData\", \"ContactData\"),")]
        [TestCase("HoustonConfiguration", "InvocationPoint", "vaConfig.DefaultInvocationPoint,")]
        [TestCase("HoustonConfiguration", "PageNamePrefix", "\"Sales_TE_\",")]
        [TestCase("HoustonConfiguration", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("HoustonConfiguration", "IsPreQuote", "false,")]
        [TestCase("HoustonConfiguration", "OnError", "Request?.Url == null || Request.Url.AbsolutePath.ToLower().Contains(\"error\"),")]
        [TestCase("GoogleTagManager", "Id", "\"GoogleTagManager\",")]
        [TestCase("GoogleTagManager", "GtmConfiguration", "config.GetGtmConfiguration(),")]
        [TestCase("GoogleTagManager", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("GoogleTagManager", "IgnoreSource", "true")]
        [TestCase("GoogleTagManagerDataLayer", "QuoteTier", "\"Standard\"")]
        [TestCase("GoogleTagManagerTagManager", "TagManagerScriptUrl", "ConfigurationManager.AppSettings[\"TagManagerScriptUrl\"]")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}