﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LayoutDirectToSiteTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_LayoutDirectToSite";
            Document = GetDocument();
            Components.Add("HoustonConfiguration", GetDocumentPart("var componentHoustonConfiguration = new ComponentHoustonConfiguration", "};"));
            Components.Add("GoogleTagManager", GetComponent("GoogleTagManager"));
            Components.Add("Navigation", GetComponent("Navigation"));
            Components.Add("PageReloader", GetComponent("PageReloader"));
            Components.Add("Houston", GetDocumentPart("@Html.ComponentRenderHouston", "(componentHoustonConfiguration)"));
            Components.Add("GoogleTagManagerDataLayer", GetComponent("GoogleTagManagerDataLayer"));
            Components.Add("GoogleTagManagerTagManager", GetComponent("GoogleTagManagerTagManager"));
            Components.Add("GoogleTagManagerLaunchScript", GetComponent("GoogleTagManagerLaunchScript"));
        }

        [Test]
        public void HoustonConfigurationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["HoustonConfiguration"]));
        }

        [Test]
        public void GoogleTagManagerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManager"]));
        }

        [Test]
        public void NavigationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Navigation"]));
        }

        [Test]
        public void PageReloaderComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["PageReloader"]));
        }

        [Test]
        public void HoustonComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Houston"]));
        }

        [Test]
        public void GoogleTagManagerDataLayerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerDataLayer"]));
        }

        [Test]
        public void GoogleTagManagerTagManagerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerTagManager"]));
        }

        [Test]
        public void GoogleTagManagerLaunchScriptRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerLaunchScript"]));
        }

        [TestCase("HoustonConfiguration", "Id", "\"Houston\",")]
        [TestCase("HoustonConfiguration", "Brand", "vaConfig.BrandName,")]
        [TestCase("HoustonConfiguration", "Application", "vaConfig.Application,")]
        [TestCase("HoustonConfiguration", "JourneySection", "\"DTS\",")]
        [TestCase("HoustonConfiguration", "Product", "product,")]
        [TestCase("HoustonConfiguration", "Journey", "isVan ? \"Van\" : \"Car\",")]
        [TestCase("HoustonConfiguration", "ContactDataUrl", "Url.Action(\"HoustonContactData\", \"ContactData\"),")]
        [TestCase("HoustonConfiguration", "InvocationPoint", "vaConfig.DefaultInvocationPoint,")]
        [TestCase("HoustonConfiguration", "PageNamePrefix", "\"Sales_TE_\",")]
        [TestCase("HoustonConfiguration", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("HoustonConfiguration", "IsPreQuote", "preQuote,")]
        [TestCase("HoustonConfiguration", "OnError", "Request?.Url == null || Request.Url.AbsolutePath.ToLower().Contains(\"error\"),")]
        [TestCase("GoogleTagManager", "Id", "\"GoogleTagManager\",")]
        [TestCase("GoogleTagManager", "GtmConfiguration", "config.GetGtmConfiguration(),")]
        [TestCase("GoogleTagManager", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("GoogleTagManager", "IgnoreSource", "true")]
        [TestCase("Navigation", "Id", "\"NavigationDirectToSite\",")]
        [TestCase("Navigation", "MenuKeyPointsConfig", "new Dictionary<string, MenuItemConfig>\r\n                            {\r\n                                { \"Your details\", new MenuItemConfig\r\n                                {\r\n                                    ParentAction = Url.Action(\"Index\", \"YourDetails\"),\r\n                                    ChildActions = new List<string>\r\n                                    {\r\n                                        Url.Action(\"MaritalStatus\", \"YourDetails\"),\r\n                                        Url.Action(\"Address\", \"YourDetails\"),\r\n                                        Url.Action(\"HomeOwner\", \"YourDetails\"),\r\n                                        Url.Action(\"UkResident\", \"YourDetails\"),\r\n                                        Url.Action(\"EmploymentStatus\", \"YourDetails\"),\r\n                                        Url.Action(\"EmploymentOccupation\", \"YourDetails\"),\r\n                                        Url.Action(\"EmploymentBusinessType\", \"YourDetails\"),\r\n                                        Url.Action(\"PartTimeOccupation\", \"YourDetails\"),\r\n                                        Url.Action(\"PartTimeOccupationType\", \"YourDetails\"),\r\n                                        Url.Action(\"PartTimeBusinessType\", \"YourDetails\"),\r\n                                        Url.Action(\"LicenceType\", \"YourDetails\"),\r\n                                        Url.Action(\"LicenceDate\", \"YourDetails\"),\r\n                                        Url.Action(\"LicenceNumber\", \"YourDetails\"),\r\n                                        Url.Action(\"MedicalCondition\", \"YourDetails\"),\r\n                                        Url.Action(\"MedicalConditionDvlaNotified\", \"YourDetails\"),\r\n                                        Url.Action(\"DeclinedInsurance\", \"YourDetails\"),\r\n                                        Url.Action(\"MotorClaims\", \"YourDetails\"),\r\n                                        Url.Action(\"MotorConvictions\", \"YourDetails\"),\r\n                                        Url.Action(\"CriminalConvictions\", \"YourDetails\"),\r\n                                        Url.Action(\"AdditionalDrivers\", \"YourDetails\")\r\n                                    }\r\n                                }},\r\n                                { \"Your \" + product.ToLower(), new MenuItemConfig\r\n                                {\r\n                                    ParentAction = Url.Action(\"VehicleLookup\", controller),\r\n                                    ChildActions = new List<string>\r\n                                    {\r\n                                        Url.Action(\"VehicleDetails\", controller),\r\n                                        Url.Action(\"OvernightParking\", controller)\r\n                                    }\r\n                                }},\r\n                                { \"Your cover\", new MenuItemConfig\r\n                                {\r\n                                    ParentAction = Url.Action(\"Index\", \"YourCover\"),\r\n                                    ChildActions = new List<string>\r\n                                    {\r\n                                        Url.Action(\"RegisteredKeeperRelationship\", \"YourCover\"),\r\n                                        Url.Action(\"AboutRegisteredKeeper\", \"YourCover\"),\r\n                                        Url.Action(\"RegisteredKeeperMaritalStatus\", \"YourCover\"),\r\n                                        Url.Action(\"RegisteredKeeperSameAddress\", \"YourCover\"),\r\n                                        Url.Action(\"MainDriver\", \"YourCover\"),\r\n                                        Url.Action(\"CoverLevel\", \"YourCover\"),\r\n                                        Url.Action(\"CoverExcesses\", \"YourCover\"),\r\n                                        Url.Action(\"CoverStartDate\", \"YourCover\"),\r\n                                        Url.Action(\"AnnualMileage\", \"YourCover\"),\r\n                                        Url.Action(\"VehicleUse\", \"YourCover\"),\r\n                                        Url.Action(\"NcdYears\", \"YourCover\"),\r\n                                        Url.Action(\"NcdSource\", \"YourCover\"),\r\n                                        Url.Action(\"OtherVehicles\", \"YourCover\"),\r\n                                        Url.Action(\"PaymentsRegularity\", \"OtherDetails\"),\r\n                                        Url.Action(\"AndFinally\", \"OtherDetails\")\r\n                                    }\r\n                                }},\r\n                                {  \"Quote\", new MenuItemConfig() }},")]
        [TestCase("Navigation", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("PageReloader", "Id", "\"PageReloader\",")]
        [TestCase("PageReloader", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("GoogleTagManagerDataLayer", "QuoteTier", "\"Standard\"")]
        [TestCase("GoogleTagManagerTagManager", "TagManagerScriptUrl", "ConfigurationManager.AppSettings[\"TagManagerScriptUrl\"]")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}