﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PaymentSuccessfulTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "PaymentSuccessful";
            Document = GetDocument();
            Components.Add("TakePaymentPaymentSuccessful", GetComponent("TakePaymentPaymentSuccessful"));
        }

        [Test]
        public void TakePaymentPaymentSuccessfulComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentPaymentSuccessful"]));
        }

        [TestCase("TakePaymentPaymentSuccessful", "Id", "\"PaymentSuccessful\"")]
        [TestCase("TakePaymentPaymentSuccessful", "PolicyCreationPendingUrl", "Url.Action(\"PolicyCreationPending\", \"Payment\"),")]
        [TestCase("TakePaymentPaymentSuccessful", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}