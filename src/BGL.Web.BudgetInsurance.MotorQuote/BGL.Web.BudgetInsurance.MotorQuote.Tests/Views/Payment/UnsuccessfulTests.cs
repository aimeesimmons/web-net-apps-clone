﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class UnsuccessfulTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "Unsuccessful";
            Document = GetDocument();
            Components.Add("TakePaymentPaymentDeclined", GetComponent("TakePaymentPaymentDeclined"));
        }

        [Test]
        public void TakePaymentPaymentDeclinedComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentPaymentDeclined"]));
        }

        [TestCase("TakePaymentPaymentDeclined", "Id", "\"PaymentDeclined\"")]
        [TestCase("TakePaymentPaymentDeclined", "PaymentSummaryUrl", "Url.Action(\"PaymentSummary\", \"Payment\"),")]
        [TestCase("TakePaymentPaymentDeclined", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}