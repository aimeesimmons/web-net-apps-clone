﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class CriminalConvictionsTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "CriminalConvictions";
            Document = GetDocument();
            Components.Add("CriminalConvictions", GetComponent("CriminalConvictions"));
        }

        [Test]
        public void CriminalConvictionsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CriminalConvictions"]));
        }

        [TestCase("CriminalConvictions", "Id", "\"CriminalConvictions\",")]
        [TestCase("CriminalConvictions", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("CriminalConvictions", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CriminalConvictions", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CriminalConvictions", "NextStepUrl", "Url.Action(\"AdditionalDrivers\", \"YourDetails\"),")]
        [TestCase("CriminalConvictions", "PreviousStepUrl", "Url.Action(\"MotorConvictions\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}