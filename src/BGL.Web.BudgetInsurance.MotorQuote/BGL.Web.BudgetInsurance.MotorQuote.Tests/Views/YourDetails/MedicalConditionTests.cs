﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MedicalConditionTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "MedicalCondition";
            Document = GetDocument();
            Components.Add("DriverDetailsMedicalCondition", GetComponent("DriverDetailsMedicalCondition"));
        }

        [Test]
        public void DriverDetailsMedicalConditionComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsMedicalCondition"]));
        }

        [TestCase("DriverDetailsMedicalCondition", "Id", "\"MedicalCondition\",")]
        [TestCase("DriverDetailsMedicalCondition", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsMedicalCondition", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsMedicalCondition", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsMedicalCondition", "NextStepUrl", "Url.Action(\"DeclinedInsurance\", \"YourDetails\"),")]
        [TestCase("DriverDetailsMedicalCondition", "DvlaNotifiedPageUrl", "Url.Action(\"MedicalConditionDvlaNotified\", \"YourDetails\"),")]
        [TestCase("DriverDetailsMedicalCondition", "LicenceDatePageUrl", "Url.Action(\"LicenceDate\", \"YourDetails\"),")]
        [TestCase("DriverDetailsMedicalCondition", "PreviousStepUrl", "Url.Action(\"LicenceNumber\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}