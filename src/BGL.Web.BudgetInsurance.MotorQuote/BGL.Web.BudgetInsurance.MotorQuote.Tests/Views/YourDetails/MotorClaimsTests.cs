﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MotorClaimsTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "MotorClaims";
            Document = GetDocument();
            Components.Add("MotorClaims", GetComponent("MotorClaims"));
        }

        [Test]
        public void MotorClaimsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["MotorClaims"]));
        }

        [TestCase("MotorClaims", "Id", "\"MotorClaims\",")]
        [TestCase("MotorClaims", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("MotorClaims", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("MotorClaims", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("MotorClaims", "NextStepUrl", "Url.Action(\"MotorConvictions\", \"YourDetails\"),")]
        [TestCase("MotorClaims", "PreviousStepUrl", "Url.Action(\"DeclinedInsurance\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}