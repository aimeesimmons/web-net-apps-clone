﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MotorConvictionsTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "MotorConvictions";
            Document = GetDocument();
            Components.Add("MotorConvictions", GetComponent("MotorConvictions"));
        }

        [Test]
        public void MotorConvictionsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["MotorConvictions"]));
        }

        [TestCase("MotorConvictions", "Id", "\"MotorConvictions\",")]
        [TestCase("MotorConvictions", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("MotorConvictions", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("MotorConvictions", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("MotorConvictions", "NextStepUrl", "Url.Action(\"CriminalConvictions\", \"YourDetails\"),")]
        [TestCase("MotorConvictions", "PreviousStepUrl", "Url.Action(\"MotorClaims\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}