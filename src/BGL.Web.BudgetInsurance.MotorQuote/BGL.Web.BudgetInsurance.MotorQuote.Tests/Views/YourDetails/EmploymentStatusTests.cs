﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class EmploymentStatusTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "EmploymentStatus";
            Document = GetDocument();
            Components.Add("DriverDetailsEmploymentStatus", GetComponent("DriverDetailsEmploymentStatus"));
        }

        [Test]
        public void DriverDetailsEmploymentStatusComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsEmploymentStatus"]));
        }

        [TestCase("DriverDetailsEmploymentStatus", "Id", "\"EmploymentStatus\",")]
        [TestCase("DriverDetailsEmploymentStatus", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsEmploymentStatus", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsEmploymentStatus", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsEmploymentStatus", "NextStepUrl", "Url.Action(\"PartTimeOccupation\", \"YourDetails\"),")]
        [TestCase("DriverDetailsEmploymentStatus", "PreviousStepUrl", "Url.Action(\"UkResident\", \"YourDetails\")")]
        [TestCase("DriverDetailsEmploymentStatus", "OccupationPageUrl", "Url.Action(\"EmploymentOccupation\", \"YourDetails\"),")]
        [TestCase("DriverDetailsEmploymentStatus", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}