﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PartTimeOccupationTypeTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "PartTimeOccupationType";
            Document = GetDocument();
            Components.Add("DriverDetailsPartTimeOccupationType", GetComponent("DriverDetailsPartTimeOccupationType"));
        }

        [Test]
        public void DriverDetailsPartTimeOccupationTypeComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsPartTimeOccupationType"]));
        }

        [TestCase("DriverDetailsPartTimeOccupationType", "Id", "\"PartTimeOccupation\",")]
        [TestCase("DriverDetailsPartTimeOccupationType", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsPartTimeOccupationType", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsPartTimeOccupationType", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsPartTimeOccupationType", "NextStepUrl", "Url.Action(\"PartTimeBusinessType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsPartTimeOccupationType", "PreviousStepUrl", "Url.Action(\"PartTimeOccupation\", \"YourDetails\")")]
        [TestCase("DriverDetailsPartTimeOccupationType", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}