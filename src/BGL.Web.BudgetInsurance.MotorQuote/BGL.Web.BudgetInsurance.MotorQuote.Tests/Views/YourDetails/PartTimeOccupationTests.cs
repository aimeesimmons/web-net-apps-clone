﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PartTimeOccupationTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "PartTimeOccupation";
            Document = GetDocument();
            Components.Add("DriverDetailsPartTimeOccupation", GetComponent("DriverDetailsPartTimeOccupation"));
        }

        [Test]
        public void DriverDetailsPartTimeOccupationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsPartTimeOccupation"]));
        }

        [TestCase("DriverDetailsPartTimeOccupation", "Id", "\"PartTimeOccupation\",")]
        [TestCase("DriverDetailsPartTimeOccupation", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsPartTimeOccupation", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsPartTimeOccupation", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsPartTimeOccupation", "NextStepUrl", "Url.Action(\"LicenceType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsPartTimeOccupation", "PreviousStepUrl", "Url.Action(\"EmploymentStatus\", \"YourDetails\")")]
        [TestCase("DriverDetailsPartTimeOccupation", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        [TestCase("DriverDetailsPartTimeOccupation", "PreviousBusinessTypePageUrl", "Url.Action(\"EmploymentBusinessType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsPartTimeOccupation", "OccupationPageUrl", "Url.Action(\"PartTimeOccupationType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsPartTimeOccupation", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}