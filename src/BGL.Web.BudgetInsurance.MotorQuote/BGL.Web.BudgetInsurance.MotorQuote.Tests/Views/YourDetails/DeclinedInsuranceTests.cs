﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class DeclinedInsuranceTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "DeclinedInsurance";
            Document = GetDocument();
            Components.Add("DriverDetailsDeclinedInsurance", GetComponent("DriverDetailsDeclinedInsurance"));
        }

        [Test]
        public void DriverDetailsDeclinedInsuranceComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsDeclinedInsurance"]));
        }

        [TestCase("DriverDetailsDeclinedInsurance", "Id", "\"DeclinedInsurance\",")]
        [TestCase("DriverDetailsDeclinedInsurance", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsDeclinedInsurance", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsDeclinedInsurance", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsDeclinedInsurance", "NextStepUrl", "Url.Action(\"MotorClaims\", \"YourDetails\"),")]
        [TestCase("DriverDetailsDeclinedInsurance", "DvlaNotifiedPageUrl", "Url.Action(\"MedicalConditionDvlaNotified\", \"YourDetails\"),")]
        [TestCase("DriverDetailsDeclinedInsurance", "PreviousStepUrl", "Url.Action(\"MedicalCondition\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}