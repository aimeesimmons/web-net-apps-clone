﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AdditionalDriversTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "AdditionalDrivers";
            Document = GetDocument();
            Components.Add("DriverDetailsAnimatedDriverBubbles", GetComponent("DriverDetailsAnimatedDriverBubbles"));
            Components.Add("EmploymentStatusCodes", GetDocumentPart("var employmentStatusCodes", " = new List<string>\r\n    {\r\n        \"E\",\r\n        \"S\",\r\n        \"C\",\r\n        \"D\",\r\n        \"T\",\r\n        \"V\"\r\n    };"));
        }

        [Test]
        public void DriverDetailsAnimatedDriverBubblesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsAnimatedDriverBubbles"]));
        }

        [Test]
        public void EmploymentStatusCodesRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["EmploymentStatusCodes"]));
        }

        [TestCase("DriverDetailsAnimatedDriverBubbles", "Id", "\"AdditionalDriver\",")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "DataStoreKey", "\"AdditionalDriver\",")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "PreviousStepUrl", "Url.Action(\"CriminalConvictions\", \"YourDetails\"),")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "NextStepUrl", "Url.Action(\"VehicleLookup\", controller)")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "HasPageRefresh", "true,")]
        [TestCase("DriverDetailsAnimatedDriverBubbles", "StatusCodesToAskOccupation", "employmentStatusCodes")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}