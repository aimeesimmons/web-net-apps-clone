﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class UkResidentTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "UkResident";
            Document = GetDocument();
            Components.Add("DriverDetailsUkResident", GetComponent("DriverDetailsUkResident"));
        }

        [Test]
        public void DriverDetailsUkResidentComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsUkResident"]));
        }

        [TestCase("DriverDetailsUkResident", "Id", "\"UkResident\",")]
        [TestCase("DriverDetailsUkResident", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsUkResident", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsUkResident", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsUkResident", "NextStepUrl", "Url.Action(\"EmploymentStatus\", \"YourDetails\"),")]
        [TestCase("DriverDetailsUkResident", "PreviousStepUrl", "Url.Action(\"HomeOwner\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}