﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.BudgetInsurance.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LicenceTypeTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "LicenceType";
            Document = GetDocument();
            Components.Add("DriverDetailsTypeofLicence", GetComponent("DriverDetailsTypeofLicence"));
        }

        [Test]
        public void DriverDetailsTypeofLicenceComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsTypeofLicence"]));
        }

        [TestCase("DriverDetailsTypeofLicence", "Id", "\"DriverLicence\",")]
        [TestCase("DriverDetailsTypeofLicence", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsTypeofLicence", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsTypeofLicence", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsTypeofLicence", "PreviousBusinessTypePageUrl", "Url.Action(\"PartTimeBusinessType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsTypeofLicence", "NextStepUrl", "Url.Action(\"LicenceDate\", \"YourDetails\"),")]
        [TestCase("DriverDetailsTypeofLicence", "PreviousStepUrl", "Url.Action(\"PartTimeOccupation\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}