using BGL.Web.PostOffice.MotorQuote;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace BGL.Web.PostOffice.MotorQuote
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureRequestAccessor(app);
            ConfigureAuth(app);
        }
    }
}