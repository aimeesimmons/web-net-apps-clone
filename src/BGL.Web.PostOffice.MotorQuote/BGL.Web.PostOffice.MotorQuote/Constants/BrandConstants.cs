namespace BGL.Web.PostOffice.MotorQuote.Constants
{
    public class BrandConstants
    {
        public const string VanErrorMessageUrl = "https://postoffice.insure-systems.co.uk/Van/PO01/NLE";

        public const string CarErrorMessageUrl = "https://postoffice.insure-systems.co.uk/Car/PO01/NLE";

        public const string WrongVehicleTypeForVanInsValidationErrorMessage = "We have matched your vehicle to a van.Please click &lt;a href=\"{0}\"&gt;here&lt;/a&gt; to get a van insurance quote.";

        public const string WrongVehicleTypeForCarInsValidationErrorMessage = "We have matched your vehicle to a car. Please click &lt;a href=\"{0}\"&gt;here&lt;/a&gt; to get a car insurance quote.";

        public const string HoustonContactCentrePhoneNumber = "0330 018 0348";

        public const string ContactCentrePhoneNumber = "0330 018 3770";

        public const string ExistingCustomerPhoneNumber = "0345 073 1002";
    }
}