﻿(function() {
    bgl.components.houston.init({
        "Brand": "PostOffice",
        "LivePersonBrand": "Post Office",
        "Journey": "SSC",
        "JourneySection": "",
        "InvocationPoint": "SSC_TE",
        "PageNamePrefix": "SSC_TE_",
        "VirtualAssistantChatOnly": false,
        "ContactDataUrl": "#",
        "IsPreQuote": false,
        "Id": "HoustonDefault",
        "IsCritical": false,
        "IsVaEnabled": true,
        "ErrorConfiguration": {
            "Header": null,
            "ErrorMessage":
                null
        },
        "ApplicationPath": null
    });
})();
