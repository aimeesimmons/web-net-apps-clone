﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.breakdownassistancedetailview = (function () {
    var configuration, currentTierButton;
    var resetFocusAttributeName = 'data-reset-addon-focus';
    var form;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                init(componentConfiguration);
            });
    }

    function init(componentConfiguration) {
        form = $('#' + componentConfiguration.Id + '__form');
        configuration = componentConfiguration;
        if (configuration.DetailUrl != null) {
            sessionStorage.setItem(configuration.SelectedAddon, configuration.DetailUrl);
        }
        storeBreakdownAddonSelectedStatus();
        subscribeForEvents();
        changeNextButtonText();
        shouldRadioBeSelectedOnLoad();
        onRadioButtonClick();
        onNextButtonClick();
        bgl.components.coverdetails.common.onAddonBackButtonClick();
        $(document).scrollTop(0);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function storeBreakdownAddonSelectedStatus() {

        if ($("#breakdown-table").length > 0) {
            var isBreakdownTableHeaderActive = $('.breakdown-table__header-item.active');
            if (isBreakdownTableHeaderActive.length > 0) {
                sessionStorage.setItem("Breakdown-Selected", true);
            } else {
                sessionStorage.setItem("Breakdown-Selected", false);
            }
        }
    }

    function selectBreakdownTier(currentTier) {
        var breakdownButtons = $('.breakdown-table__button');
        var allTiers = $('[data-tier]');
        var queryFull = "[data-tier='" + currentTier.attr('data-tier') + "']";
        var allCurrentTier = $(queryFull);
        currentTierButton = $(".breakdown-table__button" + queryFull);

        if ($(currentTierButton).hasClass('active')) {
            $(currentTierButton).html('Select');
            $(allCurrentTier).removeClass('active');
        } else {
            $(breakdownButtons).html('Select');
            $(currentTierButton).html('Selecting');
            $(allTiers).removeClass("active")
                .addClass('ui-state-disabled');
            $(allCurrentTier).addClass("active");
        }
        storeBreakdownAddonSelectedStatus();
    }

    function subscribeForEvents() {

        $('[data-tier]')
            .off('click')
            .on('click',
                function (e) {
                    e.stopImmediatePropagation();
                    var $this = $(this);
                    if ($this.hasClass('active') && configuration.IsTiersDeselectionForbidden) {
                        return;
                    }

                    $this.attr(resetFocusAttributeName, '');

                    selectBreakdownTier($this);
                    bgl.common.focusHolder.setFocusedElement($(this).data("focusid"));

                    $('#Code').val($this.data("cover-level-code"));

                    var form = $('#' + configuration.Id + '__form');

                    bgl.common.validator.submitForm(form, configuration);
                    setLinksDisabled(true);
                }
            );
    }

    function changeNextButtonText() {
        var nextaddonvalue = $("#next-addon").val();
        var isCoverMandatory = $("#mandatory-cover").val().toLowerCase() === "true";
        $('#next-button-span').text(nextaddonvalue || isCoverMandatory ? "Optional Extras" : "Review details");
    }

    function onNextButtonClick() {
        $("#next-extras-button").on("click",
            function (e) {
                e.preventDefault();

                var nextaddonvalue = $("#next-addon").val();

                if (!(configuration.IsSinglePriceBreakdown && $('.form-row__input--radio:checked').length === 0)) {
                    if (nextaddonvalue) {
                        var nextaddonNonSelected = sessionStorage.getItem(("NonSelectedAddon").concat(nextaddonvalue));

                        var nextAddonSelected = sessionStorage.getItem(("SelectedAddon").concat(nextaddonvalue));

                        if (nextaddonNonSelected || nextAddonSelected) {
                            if (nextAddonSelected) {
                                window.location.href = nextAddonSelected;
                            } else {
                                window.location.href = nextaddonvalue;
                            }
                        } else {
                            var nextaddonDetailUrl = sessionStorage.getItem(nextaddonvalue);
                            if (nextaddonDetailUrl) {
                                window.location.href = nextaddonDetailUrl;
                            } else {
                                window.location.href = nextaddonvalue;
                            }
                        }
                    } else {
                        window.location.href = configuration.NextStepUrl;
                    }
                } else {
                    displayValidationMessage();
                }
            });
    }

    function successCallback(houstonMessage) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: houstonMessage
        });
        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
        updateButtonText();
    }

    function updateButtonText() {
        currentTierButton = currentTierButton || $();

        if ($('[data-tier].active').length) {
            currentTierButton.html("Selected");
        }
    }

    function onSuccess(configuration, data) {
        var message = data.houstonMessage || data.message;
        if (message) {
            var params = {
                callback: function () {
                    successCallback(message);
                },
                spinnerName: "addonSpinner"
            };

            bgl.common.pubsub.emit("PollBasket", params);

        } else {
            bgl.common.pubsub.emit("PollBasket");
        }

        setLinksDisabled(false);
    }

    function setLinksDisabled(disabled) {
        var elements = $('button, a, select, input, label, [data-tier], .shopping-basket, #Houston');
        elements.prop("disabled", disabled);

        if (disabled) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        }
    }

    function onRadioButtonClick() {
        $(".form-row__input--radio").off("change").on("change",
            function () {
                if ($('#button-panel').hasClass('error-text__heading')) {
                    removeValidationMessage();
                }
                bgl.common.focusHolder.setFocusedElement($(this).data("focusid"));
                var id = $(this).prop('id');
                var isAdded = sessionStorage.getItem("Breakdown-Selected");
                if (id === "Breakdown-No") {
                    if (isAdded === "true") {
                        submitForm();
                    }
                    sessionStorage.setItem("Breakdown-Selected", false);
                } else if (id === "Breakdown-Yes") {
                    if (isAdded === "false" || isAdded === null) {
                        submitForm();
                    }
                    sessionStorage.setItem("Breakdown-Selected", true);
                }
            });
    }

    function shouldRadioBeSelectedOnLoad() {
        if ($('.form-row__input--radio').is(':checked')) {
            var selectedValue = $('input[name=IsAddonSelected]:checked').val();
            var isValueSetForBreakdown = sessionStorage.getItem("Breakdown-Selected");
            if (selectedValue === "No" && isValueSetForBreakdown === null) {
                $('.form-row__input--radio').prop('checked', false);
            }
            if (selectedValue === "Yes") {
                sessionStorage.setItem("Breakdown-Selected", true);
            }
        }
    }

    function removeValidationMessage() {
        $('#addon-questions').removeClass('addon-question-error');
        $('#button-panel').removeClass('error-text__heading');
        $('.form-row__validation-text').hide();
    }

    function displayValidationMessage() {
        $('#addon-questions').addClass('addon-question-error');
        $('#button-panel').addClass('error-text__heading');
        $('.form-row__validation-text').show();
    }

    function submitForm() {
        form.attr('action', $('#UpdateGenericAddonUrl').val());
        if (bgl.common.validator.isFormValid(form)) {
            bgl.common.validator.submitForm(form, configuration);
        }
        setLinksDisabled(true);
    }

    function onError() {
        setLinksDisabled(false);
        init(configuration);
    }

    return {
        onSuccess: onSuccess,
        init: init,
        load: loadComponent,
        setLinksDisabled: setLinksDisabled,
        onError: onError
    };
})();