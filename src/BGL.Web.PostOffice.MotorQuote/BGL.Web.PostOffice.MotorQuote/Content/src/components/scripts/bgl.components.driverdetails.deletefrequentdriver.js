﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.deletefrequentdriver = (function() {
    var configuration;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        subscribeDeleteFrequentDriver();
    }
    function subscribeDeleteFrequentDriver() {
        $(document).off('click', 'button[data-delete-driver]')
            .on('click', 'button[data-delete-driver]', function (event) {
                event.preventDefault();
                var $this = $(this);
                var url = $this.data('delete-url');
                var driverId = $this.data('driverId');
                var disabled = $this.data("disabled");
                var requestVerificationToken = $("input[name='__RequestVerificationToken']").val();

                if (!disabled){
                    $this.attr("data-disabled", "true");
                    deleteFrequentDriver(url, driverId, requestVerificationToken);
                }
            });
    }

    function deleteFrequentDriver(url, driverId, requestVerificationToken) {
        var data = {
            driverId: driverId,
            __RequestVerificationToken: requestVerificationToken
        }

        $.post(url, data, function (data) {
            if (!data.isSuccess && data.errorRedirectUrl) {
                bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                return;
            }

            if (configuration.IsService) {
                if (configuration.IsInitialChange) {
                    sessionStorage.setItem('MTAStartPage', window.location.href);
                }

                bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                return;
            } else {
                $('[data-dismiss="slide-panel"]').click();
            }
            
            bgl.common.utilities.setUrlHashValue('main');
            bgl.common.pubsub.emit('PollBasket');
        });
    }

    return {
        init: initComponent
    }
})();