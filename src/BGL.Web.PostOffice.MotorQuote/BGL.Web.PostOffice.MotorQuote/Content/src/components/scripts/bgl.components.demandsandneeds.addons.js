﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.demandsandneeds = bgl.components.demandsandneeds || {};

bgl.components.demandsandneeds.addons = (function ($) {
    var configuration,
        validateOnEveryInput = false,
        applicationPath = '',
        patchedYes = [],
        sessionStorageData,
        basketUpdatedNotification,
        priceProtectionAddonName = "Price Protection Cover";

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        applicationPath = componentConfiguration.ApplicationPath;
        preventDefaultFormBehaviour();
        setEventBindings();
        populatePatchedYesList();
        dataStoreInit();
        matchInputsWithSessionStorage();
        logClicksForLinksAndButtons();
    }

    function setEventBindings() {
        setAccessibilityEventBindings();
        setBreakdownTableEventBindings();
        setInterestedEventBindings();
        setYesButtonEventBindings();
        setNextAddonEventBindings();
        setNoButtonEventBindings();
        setDisabledFormEventBindings();
        setContinueButtonEventBindings();
        setNotInterestEventBindings();
        setCoverLevelChangeEventBindings();
        onInterestedButtonClick();
        setPncdEventBindings();
        setAccordionEventBindings();
    }

    function preventDefaultFormBehaviour() {
        var addons = getAllAddons();

        addons.submit(function (e) {
            e.preventDefault();
        });
    }

    function setAccessibilityEventBindings() {
        generateClickOnKeyPress();
    }

    function setInterestedEventBindings() {
        $('.addon-interested')
            .on('click', function () {

                var addon = getAddon($(this));
                var addonId = addon.attr('id');
                sessionStorage.setItem(addonId + '-interested', addonId + '-interested');
                sessionStorage.removeItem(addonId + '-not-interested');
                if (addon.attr('id') === 'Breakdown') {
                    uncheckBreakdownTable(addon);
                    hideBasket(addon);

                }

                setFocusToHeader(addon);
                revealFbels(addon);
                revealBasket(addon);

                if (validateOnEveryInput) {
                    validateAddons();
                }

                hideDemandsAndNeedsSection(addon);
            }
            );
    }

    function setNotInterestEventBindings() {
        $('.addon-not-interested')
            .on('click',
                function () {
                    var addon = getAddon($(this));
                    var addonId = addon.attr('id');
                    sessionStorage.setItem(addonId + '-not-interested', addonId + '-not-interested');
                    sessionStorage.removeItem(addonId + '-interested');
                    setAriaSelectedToTrue(addon);
                });
    }

    function setYesButtonEventBindings() {
        $('.addon-yes').on('click', function (e) {
            setNavigationAndUpdateElementsDisabled();
            e.stopPropagation();
            var addon = getAddon($(this));
            var addonId = addon.attr('id');
            sessionStorage.setItem(addonId + '-yes', addonId + '-yes');
            sessionStorage.removeItem(addonId + '-no');
            patchedYes.push(addonId);
            updateAddOn(addon);
            var fullAddonName = addonId.replace(/([A-Z])/g, ' $1').trim();
            storeAddonBehaviourForTagManager(fullAddonName, 'Yes');
        });
    }

    function storeAddonBehaviourForTagManager(fullAddonName, addonSelected) {
        var data = getPreviouslyStoredAddonBehaviourData();

        data.push([fullAddonName, addonSelected]);

        setPreviouslyStoredAddonBehaviourData(data);
    }

    function getPreviouslyStoredAddonBehaviourData() {
        var data = [];

        if (sessionStorage.getItem('ChangedVehicleData') !== null) {
            data = JSON.parse(sessionStorage.getItem('ChangedVehicleData'));
        }

        return data;
    }

    function setPreviouslyStoredAddonBehaviourData(data) {
        sessionStorage.setItem('ChangedVehicleData', JSON.stringify(data));
    }

    function setNoButtonEventBindings() {

        var addons = getAllAddons();

        addons.each(function () {
            var addon = $(this);
            var addonNoButton = addon.find('.addon-no');

            addonNoButton.on('click', function (e) {
                setNavigationAndUpdateElementsDisabled();
                e.stopPropagation();
                var addon = getAddon($(this));
                var addonId = addon.attr('id');
                var fullAddonName = addonId.replace(/([A-Z])/g, ' $1').trim();


                sessionStorage.setItem(addonId + '-no', addonId + '-no');
                sessionStorage.removeItem(addonId + '-yes');
                storeAddonBehaviourForTagManager(fullAddonName, 'No');
                var wasPatchedYes = patchedYes.indexOf(addonId) >= 0;

                if (wasPatchedYes) {
                    removeFromPatchedYesList(addonId);
                    updateAddOn(addon);
                } else {
                    setNavigationAndUpdateElementsEnabled();
                }

            });
        });
    }

    function setNextAddonEventBindings() {
        $('.addon-not-interested, .addon-yes, .addon-no').on('click', function (e) {
            if ($(this).is(':checked')) {
                var currentAddon = getAddon($(this));
                var nextAddon = getNextAddon(currentAddon);

                setTabIndexZeroOnDemandsAndNeedsButtons(nextAddon);
                hideValidationError(currentAddon);
                hideValidationErrorMessage(currentAddon);
                revealNextAddon(currentAddon);
            }

            if (validateOnEveryInput) {
                validateAddons();
            }
        });
    }

    function onInterestedButtonClick() {
        $('.addon-interested').on('click',
            function (e) {
                if ($(this).is(':checked')) {
                    var currentAddon = getAddon($(this));
                    setAriaSelectedToFalse(currentAddon);
                }
            });
    }

    function setAriaSelectedToTrue(currentAddon) {
        var currentAddonNotInterestedLabel = getNotInterestedLabel(currentAddon);
        var currentAddonInterestedLabel = getInterestedLabel(currentAddon);
        currentAddonNotInterestedLabel.attr('aria-selected', true);
        currentAddonInterestedLabel.attr('aria-selected', false);
    }

    function setAriaSelectedToFalse(currentAddon) {
        var currentAddonNotInterestedLabel = getNotInterestedLabel(currentAddon);
        var currentAddonInterestedLabel = getInterestedLabel(currentAddon);
        currentAddonNotInterestedLabel.attr('aria-selected', false);
        currentAddonInterestedLabel.attr('aria-selected', true);
    }

    function getNotInterestedLabel(currentAddon) {
        var currentAddonNotInterestedInput = currentAddon.find('.addon-not-interested');
        var notInterestedInputId = currentAddonNotInterestedInput.attr('id');
        var currentAddonNotInterestedLabel = $("label[for='" + notInterestedInputId + "']");
        return currentAddonNotInterestedLabel;
    }

    function getInterestedLabel(currentAddon) {
        var currentAddonInterestedInput = currentAddon.find('.addon-interested');
        var interestedInputId = currentAddonInterestedInput.attr('id');
        var currentAddonInterestedLabel = $("label[for='" + interestedInputId + "']");
        return currentAddonInterestedLabel;
    }

    function setCoverLevelChangeEventBindings() {
        var grcCoverLevelCount = $('#grc-coverlevels-count').val();
        var isGrcMultiTier = grcCoverLevelCount !== undefined && grcCoverLevelCount !== '1';
        if (isGrcMultiTier) {
            onGrcGrvCoverLevelChange();
        }

        var toolCoverLevelCount = $('#toolscover-coverlevels-count').val();
        var isToolMultiTier = toolCoverLevelCount !== undefined && toolCoverLevelCount !== '1';
        if (isToolMultiTier) {
            onToolsCoverCoverLevelChange();
        }
    }

    function onGrcGrvCoverLevelChange() {
        $('#SelectedCoverLevelCode').off('change').on('change',
            function () {
                updateCoverLevel('GuaranteedReplacementCar-SelectedCover', $(this));
                displaySelectedCoverLevelPrice($(this));
                toggleMultiTierContent($(this));
            });
    }

    function onToolsCoverCoverLevelChange() {
        $('#SelectedToolsCoverCoverLevelCode').off('change').on('change',
            function () {
                updateCoverLevel('ToolsCover-SelectedCover', $(this));
                displaySelectedCoverLevelPrice($(this));
            });
    }

    function updateCoverLevel(sessionStorageKey, data) {
        var coverLevelCode = $(data).val();
        sessionStorage.setItem(sessionStorageKey, coverLevelCode);
        var addon = getAddon($(data));
        var addonId = addon.attr('id');
        var isInPatchedYesList = patchedYes.indexOf(addonId) >= 0;
        if (isInPatchedYesList) {
            updateAddOn(addon);
        }
    }

    function displaySelectedCoverLevelPrice(data) {
        var coverLevelCodes = ('#' + $(data).attr('id') + ' option');
        var selectedValue = $(data).val();

        $(coverLevelCodes).each(function () {
            $('#' + $(this).val()).addClass('hide');
        });
        $('#' + selectedValue).removeClass('hide');
    }

    function toggleMultiTierContent(data) {
        coverLevelCode = $(data).val();
        hideAllMultiTierContent();
        showSelectedMultiTierContent(coverLevelCode);
    }

    function hideAllMultiTierContent() {
        $('.multi-tier-content').addClass('hide');
    }

    function showSelectedMultiTierContent(coverLevelCode) {
        $('#multi-tier-content-' + coverLevelCode).removeClass('hide');
    }

    function setDisabledFormEventBindings() {
        $('form.addon').on('click',
            function () {
                addErrorStateOnDisabledAddon($(this));
            });
    }

    function setBreakdownTableEventBindings() {
        $('[data-tier]').on('click', function (e) {
            setNavigationAndUpdateElementsDisabled();
            e.stopPropagation();
            var tableElement = $(this);
            var isActive = tableElement.hasClass('active');

            if (isActive) {
                return;
            }

            var addon = getAddon(tableElement);
            var code = getBreakdownCode(tableElement);
            var activeTierNumber = getTierNumber(tableElement);
            setCodeValue(addon, code);
            selectTier(addon, activeTierNumber);
            hideValidationError(addon);
            hideValidationErrorMessage(addon);
            revealNextAddon(addon);

        });
    }

    function setContinueButtonEventBindings() {
        $('#continue-button').on('click',
            function () {
                var isValid = validateAddons();

                if (isValid) {
                    bgl.components.pagereloader.poller.reloadPage(null, false, configuration.NextPageUrl);
                }
            });

    }

    function getAllAddons() {
        return $('form.addon');
    }

    function getAddon(element) {
        return element.closest('form');
    }

    function getNextAddon(addOn) {
        return addOn.closest('li').next().find('form.addon');
    }

    function setFocusToHeader(addon) {
        addon.find('.addon-primary__title__text').focus();
    }

    function revealFbels(addon) {
        addon.find('.addon-main__content').removeClass('hide');
    }

    function revealBasket(addon) {
        addon.find('.addon-cta__basket').removeClass('hide');
    }

    function hideDemandsAndNeedsSection(addon) {
        addon.find('.demands-and-needs').addClass('hide');
    }

    function hideBasket(addon) {
        var basket = addon.find('.addon-secondary');
        basket.addClass('hide');
    }

    function revealNextAddon(currentAddon) {
        var nextAddon = getNextAddon(currentAddon);
        nextAddon.find('.addon-content').removeClass('disabled');
        nextAddon.find('.addon-not-interested').prop('disabled', false);
        nextAddon.find('.addon-interested').prop('disabled', false);
        nextAddon.find('.addon-content').attr('aria-hidden', false);
    }

    function uncheckBreakdownTable(addon) {
        var tableInputs = addon.find('table').find('input');
        tableInputs.prop('checked', false);
    }

    function populatePatchedYesList() {
        var yesInputs = $('input.addon-yes');

        yesInputs.each(function () {
            var input = $(this);
            var addon = getAddon($(this));
            var addonId = addon.attr('id');
            var isChecked = input.is(':checked') || input.attr('checked') === 'checked';
            var isInPatchedYesList = patchedYes.indexOf(addonId) >= 0;

            if (isChecked && !isInPatchedYesList) {
                patchedYes.push(addonId);
            }
        });
    }

    function removeFromPatchedYesList(addonId) {

        var position = patchedYes.indexOf(addonId);

        patchedYes.splice(position, position + 1);
    }

    function updateAddOn(addon) {
      bgl.common.overlay.show();
      if (typeof bgl.common.pubsub !== 'undefined') {
        bgl.common.pubsub.emit('fakeFormSubmit', addon.attr('id') || addon.attr('action') || 'unknown');
      }
      setTieredDropdownElementEnabled();
      var dataToPost = $(addon).serialize();
      setTieredDropdownElementDisabled();

      $.ajax({
        url: addon.attr('action'),
        type: addon.attr('method'),
        data: dataToPost,
        success: function (data, textStatus, jqXhr) {
          updateAddonAjaxCallSuccessful(data, textStatus, jqXhr, dataToPost);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          updateAddonAjaxCallError(jqXHR, textStatus, errorThrown, dataToPost, null);
        }
      });
    }

    function updateAddonAjaxCallError(jqXhr, textStatus, errorThrown, dataPosted, responseData) {
        if (appInsights !== undefined) {
            appInsights.trackEvent('DemandsAndNeeds bgl.components.demandsandneeds.addons updateAddonError called', {
                "jqXHR": $(jqXhr).serialize(),
                "status": textStatus,
                "errorThrown": $(errorThrown).serialize(),
                "dataPosted": dataPosted,
                "responseData": $(responseData).serialize()
            });
            appInsights.flush();
        }

        setNavigationAndUpdateElementsEnabled();
        bgl.common.utilities.errorRedirect(configuration.ErrorRedirectUrl);
    }

    function updateAddonAjaxCallSuccessful(responseData, textStatus, jqXhr, dataPosted) {
        if (!responseData.isSuccess) {
            updateAddonAjaxCallError(jqXhr, textStatus, null, dataPosted, responseData);
        } else {
            basketUpdatedNotification = responseData.houstonMessage;
            pollBasket();
        }
    }

    function pollBasket() {
        var params = {
            callback: function () { pollBasketCallback(); },
            spinnerName: 'addonSpinner'
        };
        bgl.common.pubsub.emit('PollBasket', params);
    }

    function pollBasketCallback() {
        var callback = function () { updateBasketCallback(); }
        bgl.common.pubsub.emit('UpdateBasket', callback());
    }

    function updateBasketCallback() {
        var callback = function () { refreshHoustonContextCallback(); }
        bgl.common.pubsub.emit('refreshHoustonContext', callback);
    }

    function refreshHoustonContextCallback() {
        var isAnnual = $('#demands-and-needs-isAnnual').text().toLowerCase();
        var message;

        if (isAnnual === 'true') {
            message = basketUpdatedNotification.annual;
        } else {
            message = basketUpdatedNotification.monthly;
        }

        bgl.common.overlay.hide();
        setNavigationAndUpdateElementsEnabled();
        bgl.common.pubsub.emit('showNotification', message);
        basketUpdatedNotification = null;

        var isPriceProtectOffered = configuration.AddonOrder.includes(priceProtectionAddonName);
        if (isPriceProtectOffered && !$("#PriceProtectionCover").length && patchedYes.includes("NoClaimsDiscountProtection")) {
            getPriceProtectionAddon();
        }
    }

    function setNavigationAndUpdateElementsEnabled() {
        setElementsDisabledProperty(false);
    }

    function setNavigationAndUpdateElementsDisabled() {
        setElementsDisabledProperty(true);
    }

    function setElementsDisabledProperty(disabled) {
        var elements = $('button, a, select, label, [data-tier], .shopping-basket, #Houston').not('.tooltip');
        setSpecifiedElementsDisabledProperty(elements, disabled);
    }

    function setTieredDropdownElementEnabled() {
        setSelectElementsDisabledProperty(false);
    }

    function setTieredDropdownElementDisabled() {
        setSelectElementsDisabledProperty(true);
    }

    function setSelectElementsDisabledProperty(disabled) {
        var elements = $('select');
        setSpecifiedElementsDisabledProperty(elements, disabled);
    }

    function setSpecifiedElementsDisabledProperty(elements, disabled) {
        elements.prop('disabled', disabled);

        if (disabled === true) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        };
    }

    function validateAddons() {
        var addons = getAllAddons();

        validateOnEveryInput = true;
        var allAddonsAreValid = true;
        var firstInvalidAddon;

        addons.each(function () {
            var addon = $(this);
            var isFormValid = addon.valid();
            var addonContent = this.querySelectorAll("[data-type='addon-content']");

            if (isFormValid) {
                hideValidationError(addon);
            } else {
                if (!addonContent[0].classList.contains("disabled")) {
                    allAddonsAreValid = false;
                    showValidationErrorStyling(addon);

                    if (typeof firstInvalidAddon === 'undefined') {
                        firstInvalidAddon = addon;
                    }
                }
            }
        });

        if (!allAddonsAreValid) {
            showValidationErrorMessage(firstInvalidAddon);
            publishAddonErrors(firstInvalidAddon);
        }

        return allAddonsAreValid;
    }

    function publishAddonErrors(formContainingInvalidInputs) {
        var data = {
            form_section: formContainingInvalidInputs.attr('id'),
            error_details: []
        }

        formContainingInvalidInputs.find('input.error, input.input-validation-error').each(function () {
            var errorDetail = {
                error_field: getFieldsetLegendText(this),
                error_message: getFormAlertText(formContainingInvalidInputs)
            };
            data.error_details.push(errorDetail);
        });

        data.error_details = deduplicateErrorDetails(data.error_details);
        bgl.common.pubsub.emit('formValidationFailed', data);
    }

    function deduplicateErrorDetails(errorDetails) {
        var uniqueErrorDetails = [];
        return errorDetails.filter(function (errorDetail) {
            return isErrorDetailInArray(uniqueErrorDetails, errorDetail) ? false : uniqueErrorDetails.push(errorDetail);
        });
    }

    function isErrorDetailInArray(uniqueErrorDetails, newErrorDetail) {
        for (var counter = 0; counter < uniqueErrorDetails.length; counter++) {
            var errorDetail = uniqueErrorDetails[counter];
            if (errorDetail.error_field === newErrorDetail.error_field &&
                errorDetail.error_message === newErrorDetail.error_message) {
                return true;
            }
        }

        return false;
    }

    function getFieldsetLegendText(invalidFormElement) {
        var legendText = getFieldsetLegendTextForMostAddons(invalidFormElement);
        if (legendText.length > 0) {
            return legendText;
        } else {
            return getFieldsetLegendTextForBreakdownAddon(invalidFormElement);
        }
    }

    function getFieldsetLegendTextForMostAddons(invalidFormElement) {
        return $(invalidFormElement).parent().find('legend').text();
    }

    function getFieldsetLegendTextForBreakdownAddon(invalidFormElement) {
        return $(invalidFormElement).closest('fieldset').find('legend').text();
    }

    function getFormAlertText(formContainingInvalidElement) {
        return $(formContainingInvalidElement).find('div.alert').text();
    }

    function showValidationErrorStyling(addon) {
        addon.addClass('error');
        addon.find('label.error').remove();
    }

    function hideValidationError(addon) {
        addon.removeClass('error');
    }

    function showValidationErrorMessage(addonWithError) {
        hideAllValidationErrorAlerts();
        showValidationErrorAlert(addonWithError);
    }

    function hideAllValidationErrorAlerts() {
        var allAddOns = getAllAddons();

        allAddOns.each(function () {
            var addon = $(this);
            hideValidationErrorMessage(addon);
        });
    }

    function showValidationErrorAlert(addon) {
        var errorMessage = generateValidationErrorMessage(addon);
        var alert = addon.find('.alert');

        alert.find('.alert__heading').text(errorMessage);

        var currentAddonNotInterestedLabel = getNotInterestedLabel(addon);

        alert.removeClass('hide');
        scrollToElement(addon);
        currentAddonNotInterestedLabel.focus();
    }

    function generateValidationErrorMessage(addonWithError) {
        var addonName = getAddonName(addonWithError);

        var ctaBasketShowing = !addonWithError.find('.addon-cta__basket').hasClass('hide');
        if (ctaBasketShowing) {
            return 'Please select if you would like to add ' + addonName + ' to your policy';
        } else {
            return 'Please select if you are interested in ' + addonName;
        }
    }

    function getAddonName(addon) {
        return addon.data('addon-name');
    }

    function hideValidationErrorMessage(addon) {
        addon.find('.alert').addClass('hide');
    }

    function addErrorStateOnDisabledAddon(addon) {
        var isAddOnDisabled = addon.find($('.addon-content')).hasClass('disabled');
        if (isAddOnDisabled) {
            validateAddons();
        }
    }

    function scrollToElement(element) {
        $('html, body').animate({ scrollTop: element.offset().top - 110 }, 'slow');
    }

    function getBreakdownCode(tableElement) {
        return tableElement.data('cover-level-code');
    }

    function setCodeValue(addon, code) {
        var codeInput = addon.find($('input[name="Code"]'));
        codeInput.val(code);
    }

    function getTierNumber(tableElement) {
        return tableElement.data('tier');
    }

    function selectTier(breakdownAddon, activeTierNumber) {
        makeTierActive(breakdownAddon, activeTierNumber);
        updateTierButtons(breakdownAddon, activeTierNumber);
        checkSelectedTiersRadioButton(breakdownAddon, activeTierNumber);
    }

    function makeTierActive(breakdownAddon, tierNumber) {
        var allTiersButtons = breakdownAddon.find('.addon-yes, .addon-no');
        breakdownAddon.find('.active').removeClass('active');
        allTiersButtons.prop('checked', false);

        var targetTier = getTierElements(breakdownAddon, tierNumber);

        if (tierNumber > 0) {
            targetTier.closest('th').addClass('active');
            breakdownAddon.find('tbody tr').each(function (i, el) {
                $(this).find('td').eq(tierNumber - 1).addClass('active');
            });
        } else {
            var notRequiredButton = breakdownAddon.find('.not-required-button');
            notRequiredButton.addClass('active');
            $('#Breakdown-no').prop('checked', true);
        }
    }

    function getTierElements(breakdownAddon, tierNumber) {
        return breakdownAddon.find("[data-Tier='" + tierNumber + "']");
    }

    function updateTierButtons(breakdownAddon, selectedTierNumber) {
        var breakdownButtons = breakdownAddon.find('.breakdown-table__button:not(.not-required-button)');
        breakdownButtons.html('Select');

        var selectedTierButton = breakdownAddon.find(".breakdown-table__button[data-tier='" + selectedTierNumber + "']:not(.not-required-button)");
        selectedTierButton.html('Selected');
    }

    function checkSelectedTiersRadioButton(breakdownAddon, selectedTierNumber) {
        var tierElements = getTierElements(breakdownAddon, selectedTierNumber);
        var tierInput = tierElements.find('.form-row__input--radio');
        tierInput.prop('checked', true);
    }

    function generateClickOnKeyPress() {
        $('label, #continue-button').on('keyup', function (event) {
            var SPACE_KEYCODE = 32;
            var ENTER_KEYCODE = 13;
            var code = event.charCode || event.keyCode;

            if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                if ($(this).attr('data-tier')) {
                    $(this).click();
                }

                var forAttr = $(event.target).attr("for");

                if (forAttr !== null) {
                    $("#" + forAttr).click();
                } else {
                    $(this).click();
                }
            }
        });
    }

    function setTabIndexZeroOnDemandsAndNeedsButtons(addOn) {
        var labels = addOn.find('.demands-and-needs__label');
        labels.attr('tabindex', 0);
    }

    function matchInputsWithSessionStorage() {
        var allAddons = getAllAddons();
        allAddons.each(function () {
            var addon = $(this);
            var addonId = addon.attr('id');
            var sessionKeys = ['-not-interested', '-interested', '-no'];
            sessionKeys.forEach(function (key) {
                var session = sessionStorage.getItem(addonId + key);
                var wasPatchedYes = patchedYes.indexOf(addonId) >= 0;
                if (session != null && !wasPatchedYes) {
                    addon.find('.addon-content').removeClass('disabled');
                    addon.find('.addon-content').attr('aria-hidden', false);
                    var button = addon.find('#' + session);
                    button.click();
                }
            });
        });
    }

    function setAccordionEventBindings() {
        var accordionToggleButtons = getAllAccordionToggleButtons();
        accordionToggleButtons.on('click', function () {
            var accordion = getAccordion($(this));
            toggleAccordionContent(accordion);
        });
    }

    function getAllAccordionToggleButtons() {
        return $('.accordion__heading-button');
    }

    function getAccordion(accordionElement) {
        return accordionElement.closest('.accordion');
    }

    function getAccordionContent(accordion) {
        return accordion.find('.accordion__body-wrapper');
    }

    function getAccordionToggleButton(accordion) {
        return accordion.find('.accordion__heading-button');
    }

    function toggleAccordionContent(accordion) {
        if (isAccordionCollapsed(accordion)) {
            expandAccordion(accordion);
        } else {
            collapseAccordion(accordion);
        }
    }

    function isAccordionCollapsed(accordion) {
        return getAccordionContent(accordion).hasClass('hide');
    }

    function expandAccordion(accordion) {
        getAccordionContent(accordion).removeClass('hide');
        getAccordionToggleButton(accordion).attr('aria-expanded', 'true');
    }

    function collapseAccordion(accordion) {
        getAccordionContent(accordion).addClass('hide');
        getAccordionToggleButton(accordion).attr('aria-expanded', 'false');
    }

    function setPncdEventBindings() {
        var pncdAddButton = $('#NoClaimsDiscountProtection-yes-label');
        var pncdRemoveButton = $('#NoClaimsDiscountProtection-no-label');

        pncdAddButton.on('click', function () {
            togglePncdContent(true);
        });

        pncdRemoveButton.on('click', function () {
            setDataStoreWasPncdSelectedAtAggregatorFalse();
            togglePncdContent(false);
            showPncdYesNoBasket();
            removePriceProtect();
        });
    }

    function togglePncdContent(isPncdSelected) {
        if (isPncdSelected) {
            hidePncdNotSelectedContent();
            showPncdSelectedContent();
        } else {
            hidePncdSelectedContent();
            showPncdNotSelectedContent();
        }
    }

    function showPncdSelectedContent() {
        showPncdSelectedHeading();
        showPncdSelectedFbels();
    }

    function hidePncdSelectedContent() {
        hidePncdSelectedHeading();
        hidePncdSelectedFbels();
    }

    function showPncdNotSelectedContent() {
        showPncdNotSelectedHeading();
        showPncdNotSelectedFbels();
    }

    function hidePncdNotSelectedContent() {
        hidePncdNotSelectedHeading();
        hidePncdNotSelectedFbels();
    }

    function showPncdSelectedHeading() {
        $('#NoClaimsDiscountProtection-heading-selected').removeClass('hide');
    }

    function hidePncdSelectedHeading() {
        $('#NoClaimsDiscountProtection-heading-selected').addClass('hide');
    }

    function showPncdNotSelectedHeading() {
        $('#NoClaimsDiscountProtection-heading-not-selected').removeClass('hide');
    }

    function hidePncdNotSelectedHeading() {
        $('#NoClaimsDiscountProtection-heading-not-selected').addClass('hide');
    }

    function showPncdSelectedFbels() {
        $('#NoClaimsDiscountProtection-fbels-selected').removeClass('hide');
    }

    function hidePncdSelectedFbels() {
        $('#NoClaimsDiscountProtection-fbels-selected').addClass('hide');
    }

    function showPncdNotSelectedFbels() {
        $('#NoClaimsDiscountProtection-fbels-not-selected').removeClass('hide');
    }

    function hidePncdNotSelectedFbels() {
        $('#NoClaimsDiscountProtection-fbels-not-selected').addClass('hide');
    }

    function showPncdYesNoBasket() {
        changePncdBasketButtonsToYesNoVariation();
        showPncdBasketPrice();
        changePncdDidYouKnowTextToYesNoVariation();
    }

    function changePncdBasketButtonsToYesNoVariation() {
        var addButton = $('#NoClaimsDiscountProtection-yes-label');
        var removeButton = $('#NoClaimsDiscountProtection-no-label');

        addButton.text('Yes');
        removeButton.text('No');
        addButton.removeClass('full-width');
        removeButton.removeClass('full-width');
    }

    function showPncdBasketPrice() {
        var price = $('#NoClaimsDiscountProtection-basket-price');
        price.removeClass('hide');
    }

    function changePncdDidYouKnowTextToYesNoVariation() {
        var addRemoveDidYouKnowText = $('#NoClaimsDiscountProtection-DidYouKnow-AddRemove');
        var yesNoDidYouKnowText = $('#NoClaimsDiscountProtection-DidYouKnow-YesNo');
        addRemoveDidYouKnowText.addClass('hide');
        yesNoDidYouKnowText.removeClass('hide');
    }

    function dataStoreInit() {
        var dataLoadedFromSessionStorage = sessionStorage.getItem('DemandsAndNeeds');

        if (dataLoadedFromSessionStorage != null) {
            sessionStorageData = JSON.parse(dataLoadedFromSessionStorage);
        } else {
            sessionStorageData = { wasPncdSelectedAtAggregator: wasPncdSelectedAtAggregator() };
            writeToDataStore();
        }

        if (!sessionStorageData.wasPncdSelectedAtAggregator) {
            showPncdYesNoBasket();
        }
    }

    function wasPncdSelectedAtAggregator() {
        var result = false;

        var addRemoveVariationShown = !$('#NoClaimsDiscountProtection-DidYouKnow-AddRemove').hasClass('hide');
        if (addRemoveVariationShown) {
            result = true;
        }

        return result;
    }

    function setDataStoreWasPncdSelectedAtAggregatorFalse() {
        sessionStorageData.wasPncdSelectedAtAggregator = false;
        writeToDataStore();
    }

    function writeToDataStore() {
        var data = JSON.stringify(sessionStorageData);
        sessionStorage.setItem('DemandsAndNeeds', data);
    }

    function logClicksForLinksAndButtons() {
        $("button, a, label[role='button'], label[role='tab'], td.breakdown-table__body-item, th.breakdown-table__header-item").click(function () {
            var timeNow = new Date().toLocaleTimeString();
            var id = this.id;
            if (id === '') {
                id = $(this).attr('data-testid');
            }
            if (id === undefined) {
                id = $(this).text();
            }
            if (window.appInsights !== undefined) {
                var appInsights = window.appInsights;
                appInsights.trackEvent(id + ' clicked - ' + timeNow);
                appInsights.flush();
            }
        });
    }

    function getPriceProtectionAddon() {
        var priceProtectAddonConfiguration = configuration.AddonConfigurations.find(isPriceProtectConfiguration);
        var dataToPost = {
            configuration: configuration,
            addonName: priceProtectAddonConfiguration.AddOnService
        };

        $.ajax({
            url: configuration.ApplicationPath + "DemandsAndNeedsComponent/GetSingleAddon",
            type: "POST",
            data: JSON.stringify(dataToPost),
            contentType: 'application/json',
            success: function (data, textStatus, jqXhr, errorThrown) {
                if (data.isSuccess === false) {
                    return;
                }

                displayPriceProtectionAddon(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                getAddonAjaxCallError(jqXhr, textStatus, errorThrown, dataToPost, null);
            }
        });
    }

    function isPriceProtectConfiguration(addonConfiguration) {
        return addonConfiguration.AddOnService === priceProtectionAddonName;
    }

    function displayPriceProtectionAddon(content) {
        var viewContent = $("<li>" + content + "</li>");
        $("ul.addons").append(viewContent);

        var priceProtectionAddon = $("#PriceProtectionCover");
        if (shouldAddonBeDisabled(priceProtectionAddon)) {
            var priceProtectAddonContentElement = $('#PriceProtectionCover .addon-content');
            setSpecifiedElementsDisabledProperty(priceProtectAddonContentElement, true);
        }

        setEventBindings();
    }

    function removePriceProtect() {
        $('#PriceProtectionCover').closest("li").remove();
    }

    function getAddonAjaxCallError(jqXhr, textStatus, errorThrown, dataPosted, responseData) {
        if (appInsights !== undefined) {
            appInsights.trackEvent('DemandsAndNeeds bgl.components.demandsandneeds.addons getAddonError called', {
                "jqXHR": $(jqXhr).serialize(),
                "status": textStatus,
                "errorThrown": $(errorThrown).serialize(),
                "dataPosted": dataPosted,
                "responseData": $(responseData).serialize()
            });
            appInsights.flush();
        }

        setNavigationAndUpdateElementsEnabled();
        bgl.common.utilities.errorRedirect(configuration.ErrorRedirectUrl);
    }

    function shouldAddonBeDisabled(currentAddon) {
        var allAddons = getAllAddons();
        var currentAddonIndex = allAddons.index(currentAddon);
        var previousAddon = $(allAddons[currentAddonIndex - 1]);

        if (previousAddon.find(".addon-content").hasClass("disabled") || previousAddon.hasClass("error")) {
            return true;
        }

        if (!previousAddon.find("input").hasClass("completed")) {
            return true;
        }

        return false;
    }

    return {
        init: initComponent,
        load: loadComponent,
        publishAddonErrors: publishAddonErrors,
        refreshHoustonContextCallback: refreshHoustonContextCallback
    };
})($);