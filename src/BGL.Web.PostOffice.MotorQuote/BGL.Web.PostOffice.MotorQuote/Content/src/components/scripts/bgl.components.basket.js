﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.basket = (function () {

    var configuration;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {

        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        $('#shopping-basket').on('click', function () {
            waitForSlidePanelToLoad();
        });
        eventListener();
        sessionStorage.setItem("QuoteId", $("#quote-id-in-basket").text());
    }

    function eventListener() {
        bgl.common.pubsub.on('switchPaymentType', function (isMonthly) {
            switchPaymentType(isMonthly);
        });

        subscribeUpdateBasket();
    }

    function registerSaveQuoteEvent() {
        $('#SlidePanel').off().on('click', '#saveQuote', function (event) {
            bgl.common.pubsub.emit("showNotification", $("#basket-notification-message").val());
            bgl.common.pubsub.emit("SavedQuote");
        });
    }

    function waitForSlidePanelToLoad() {
       
        window.setTimeout(function () {
            if ($('#SlidePanel').hasClass('active')) {
                registerSaveQuoteEvent();
                var basketDetails = $('#basket-details').html();
                $('#SlidePanelContent').html(basketDetails);
            } else {
                waitForSlidePanelToLoad();
            }
        }, 100);
    }

    function switchPaymentType(isMonthly) {
        if (isMonthly === true) {
            $('#monthly-subtext').removeClass('hide');
            $('#monthly').removeClass('hide');
            $('#price-monthly').removeClass('hide');
            $('#annual').addClass('hide');
            $('#price-annually').addClass('hide');
        }
        if (isMonthly === false) {
            $('#monthly-subtext').addClass('hide');
            $('#monthly').addClass('hide');
            $('#price-monthly').addClass('hide');
            $('#annual').removeClass('hide');
            $('#price-annually').removeClass('hide');
        }
    }

    function subscribeUpdateBasket() {
        bgl.common.pubsub.off("UpdateBasket", updateBasket);
        bgl.common.pubsub.on('UpdateBasket', updateBasket);
    }

    function updateBasket(callback) {
        bgl.common.loader.loadAndReplace(configuration, configuration.ComponentUrl, configuration.Id, function () {
            initComponent(configuration);

            if (typeof callback === "function") {
                var options = getUpdateBasketOptions();

                callback(options);
            }
        });
    }

    function getUpdateBasketOptions() {
        var component = $('#' + configuration.Id);
        var isMonthly = component.find("#IsMonthly").val() === "True";
        var monthlyPrice = component.find("#MonthlyPrice").val();
        var annualPrice = component.find("#AnnualPrice").val();
        var depositAmount = component.find("#DepositAmount").val();

        return {
            isMonthly: isMonthly,
            monthlyPrice: monthlyPrice,
            annualPrice: annualPrice,
            depositAmount: depositAmount
        };
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.basket;
}
