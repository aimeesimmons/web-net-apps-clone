﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.relationshipstatus = (function () {
    var form,
        configuration,
        container,
        registerKeeper,
        SPACE_KEYCODE = 32,
        ENTER_KEYCODE = 13;

    function initComponent(componentConfiguration, containerId) {
        initElements(componentConfiguration, containerId);

        subscribeForEvents();

        registerKeeper.displaySubmitButton(form);
    }

    function loadComponent(config, url, containerId) {
        bgl.common.loader.load(config,
            url,
            containerId,
            function () {
                initComponent(config, containerId);
            });
    }

    function initElements(componentConfiguration, containerId) {
        configuration = componentConfiguration;
        form = $("#relationship-status-form");
        container = containerId;
        registerKeeper = bgl.components.coverdetails.registeredkeeper;
    }

    function subscribeForEvents() {

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();

        form.find('input[type="radio"]')
            .off('click')
            .off('keypress')
            .on({
                "keypress": function (e) {
                    if (e.keyCode === ENTER_KEYCODE || e.keyCode === SPACE_KEYCODE) {
                        $(this).click();
                    }
                },
                'click': function (event) {
                    registerKeeper.submitForm(form);
                }
            });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {

                        bgl.common.validator.submitForm(form, configuration);
                    }
                });

        form.find('.back-button-link').off('click').one('click',
            function (event) {
                event.preventDefault();

                var url = $(this).attr('href');

                bgl.components.coverdetails.registeredkeeper.load(configuration, url, container);
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function onMultiStepEditSuccess(configuration, data) {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onSuccess(configuration, data) {
        bgl.components.coverdetails.aboutyou.load(configuration, data.nextUrl, container);
    }

    function onError() {
        initComponent(configuration, container);
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onMultiStepEditSuccess: onMultiStepEditSuccess,
        onError: onError
    };
})();