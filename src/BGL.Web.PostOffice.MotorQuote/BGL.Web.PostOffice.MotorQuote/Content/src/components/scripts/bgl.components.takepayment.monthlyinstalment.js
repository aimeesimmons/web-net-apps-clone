﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.monthlyinstalment = (function () {
  var componentConfig;
  function loadComponent(componentConfiguration, componentUrl, containerElementId, callback) {
    componentConfiguration.CardSessionData = getSecureSessionData();
    componentConfiguration.CreditInformationUrl = sessionStorage.getItem("credit-information");
    componentConfig = componentConfiguration;
    bgl.common.loader.load(componentConfiguration,
      componentUrl,
      containerElementId,
      function () {
        initComponent(componentConfiguration);
        if (typeof callback === 'function') {
          callback();
        }
      });
  }

  function initComponent(componentConfiguration) {
    sortCodeAutoTab();
    initDirectDebit();
    initCommon(componentConfiguration);

    $("#Card-Capture-Iframe").on("load", function () {
      hideElementsWhen3DSecureIsDisplayed();
    });

    $("#direct-debit-details").on("click", function () {
      waitForSlidePanelToLoad();
    });

    $("#importantInfo-payments-instalments, #importantInfo-bottom, #intermediary-services-contract-summary-slider-link, #intermediary-services-contract-summary-slider-link-read-confirm").off("click").on("click", function (event) {
      event.preventDefault();
      var link = $(this);
      bgl.common.loader.load({}, link.data("action-url"), link.data("container"));
    });
  }

  function waitForSlidePanelToLoad() {
    window.setTimeout(function () {
      if ($("#SlidePanel").hasClass("active")) {
        var directDebitDetails = $("#direct-debit-info").html();
        $("#SlidePanelContent").html(directDebitDetails);
      } else {
        waitForSlidePanelToLoad();
      }
    }, 100);
  }

  function initCommon(componentConfiguration) {
    var accessible = componentConfiguration.ViewOptions.MonthlyInstalment === 2;
    if (accessible) {
      setupAriaLabeltoDates();
      postInstalmentDate();
    }
    setupDatePicker();
    bgl.common.datastore.setupReadAndWrite();
    loadIframe();
    forceMonthlyPaymentMethodStoreAndSetPaymentDate();
  }

  function loadIframe() {
    var cardSessionData = getSecureSessionData();
    bgl.components.takepayment.cardcaptureiframe.loadIframe(
      'Card-Capture-Iframe',
      cardSessionData.CardCaptureUri,
      cardSessionData.SecureSessionId,
      cardSessionData.ThreeDSecureUri);
  }

  function initDirectDebit() {
    bgl.common.validator.enableMaxLength();
    bgl.common.validator.init($("#Direct-Debit-Form"),
      {
        onfocusout: function (element) {
          checkSortcodeValidation(element);
          checkAccountNumberValidation(element);
        },
        hasSubmit: false
      });

    $("#Direct-Debit-Authorise-Row label").on("click",
      function () {
        $("#" + $(this).attr("for")).click().focusout();
      });

    $("#Direct-Debit-Continue-Button").on("click",
      function () {
        var formIsValid = $("#Direct-Debit-Form").valid();
        if (formIsValid) {
          validateBankAccountAndSortCode();
        } else {
          checkSortcodeValidation($("#SortCodePart1"));
          getValidationMessages();
          scrollToFirstFieldWithErrorAndFocus();
        }

        return formIsValid;
      });
  }

  function getValidationMessages() {
    if ($(".form-row").hasClass("error")) {
      var data = {
        form_section: getFormSectionName($(this)),
        error_details: []
      }

      $('.error').find('.form-row__validation-text').each(function () {
        addError(data, $(this).attr('data-valmsg-for'), $(this).text().trim());
      });

      var sortcodeValiationElement = $("#sortcode-validation-text");
      if (sortcodeValiationElement.length == 1 && !sortcodeValiationElement.hasClass("hide")) {
        addError(data, $("#form-row-sortcode1 fieldset legend").text().trim(), $("#sortcode-validation-text").text().trim());
      }

      var sortcodeApiValidationElement = $("#sortcode-api-validation-message");
      if (sortcodeApiValidationElement.length == 1 && !sortcodeApiValidationElement.hasClass("hide")) {
        addError(data, $("#form-row-sortcode1 fieldset legend").text().trim(), $("#sortcode-api-validation-message").text().trim());
      }

      bgl.common.pubsub.emit('formValidationFailed', data);
    }
  }

  function addError(data, field, message) {
    var errorDetail = {
      error_field: field,
      error_message: message
    };
    data.error_details.push(errorDetail);
  }

  function getFormSectionName() {
    var formRow = '.form-row__validation-text';
    if ($(formRow).parents().find('.step-form__section-title').text().length === 0) {
      return $(formRow).closest('form').find('h1').text();
    } else {
      return $(formRow).closest('form').find('.step-form__section-title').text();
    }
  }

  function scrollToFirstFieldWithErrorAndFocus() {
    var firstRequiredField = $('.error,.sort-code__input--error').first();
    $('html,body').animate({ scrollTop: firstRequiredField.offset().top }, 'slow');

    if (firstRequiredField.length) {
      var invalidInputElem = firstRequiredField.find("input, select, textarea").addBack("input, select, textarea")[0];
      if (invalidInputElem !== undefined) {
        invalidInputElem.focus();
      }
    }
  }

  function scrollToCardPayment() {
    var requiredField = $('.card-payment');
    $('html,body').animate({ scrollTop: requiredField.offset().top }, 0);
  }

  function hideElementsWhen3DSecureIsDisplayed() {
    if ($("#Card-Capture-Iframe").contents().find("#continue").length === 0) {
      $("#Cardholder-Name-Form-Row").hide();
      $("#Monthly-Card-Payment-Date-Form-Row").hide();
      $("#Monthly-Payment-DD-Label").hide();
      $("#Monthly-Payment-Card-Label").hide();
      $("#Monthly-Payment-DD").hide();
      $("#Monthly-Payment-Card").hide();
      scrollToCardPayment();
      return;
    }

    var timeoutId = setTimeout(function () {
      hideElementsWhen3DSecureIsDisplayed();
    }, 1000);

    sessionStorage.setItem("3dSecureTimeoutId", timeoutId);
  }

  function checkAccountNumberValidation(element) {
    var isValid = false;

    if ($(element).hasClass("form-row__input account-number sort-code__input--error")) {
      $("#account-number-validation-message").addClass("hide");
    }
    if ($(element).hasClass("form-row__input account-number")) {
      isValid = $("#direct-debit-account-number").valid();
    }
    if (isValid) {
      RemoveAccountErrorStyle();
    }
  }

  function checkSortcodeValidation(element) {
    removeApiValidationErrorMessage(element);
    validateIndividualSortCodeInput(element);
    clearValidationErrorStylingWhenAllSortCodeInputsAreValid();
  }

  function removeApiValidationErrorMessage(element) {
    if ($(element).hasClass("form-row__input--sort-code") && !$("#sortcode-api-validation-message").hasClass("hide")) {
      RemoveSortCodeErrorStyle();
    }
  }

  function validateIndividualSortCodeInput(element) {
    if ($(element).valid()) {
      RemoveIndividualSortCodeErrorStyle(element);
    } else {
      AddIndividualSortCodeErrorStyle(element);
    }
    $("#form-row-sortcode1").removeClass("error");
  }

  function clearValidationErrorStylingWhenAllSortCodeInputsAreValid() {
    var allSortCodeInputsValid = checkAllSortCodeInputsAreValid();
    if (allSortCodeInputsValid) {
      $("#SortCodeLabel").removeClass("sort-code__label--error");
      $("#sortcode-validation-text").addClass("hide");
    }
  }

  function checkAllSortCodeInputsAreValid() {
    var allSortCodeInputsValid = true;

    $(".form-row__input--sort-code").each(function () {
      if ($(this).hasClass("sort-code__input--error")) {
        allSortCodeInputsValid = false;
      }
    });

    return allSortCodeInputsValid;
  }

  function setupAriaLabeltoDates() {
    var selectList = document.getElementById('Direct-Debit-Payment-Date-Label');
    var selectCreditCardList = document.getElementById('Credit-Card-Payment-Date-Label');
    selectList.setAttribute('aria-label', "Please Select preferred first installment date from given list");
    selectCreditCardList.setAttribute('aria-label', "Please Select preferred first installment date from given list");  
  }

  function postInstalmentDate() {     
    $(document).ready(function () {
      $(document).on("change", "#Direct-Debit-Payment-Date-List", (function () {
        var nominatedDate = $(this).val();
        postChangePaymentDateToApi(nominatedDate);
      }));

      $(document).on("change", "#Credit-Card-Payment-Date-List", (function () {
        var nominatedDate = $(this).val();
        postChangePaymentDateToApi(nominatedDate);
      }));
    });
  }

  function setupDatePicker() {
    $.extend($.datepicker, {
      _doKeyDown: function (event) {
        if (event.shiftKey) {
          if (event.keyCode === 9) {
            $.datepicker._hideDatepicker();
          }
        } else {
          event.preventDefault();
          dayTripper();
          $(".ui-datepicker-calendar").keyup(function (event) {
            if (event.keyCode === 13) {
              $.datepicker._hideDatepicker();
              $("#Direct-Debit-Selected-Person-Id").focus();
            }
          });
        }
      }
    });

    $(".form-row__input--datepicker").datepicker({
      selectOtherMonths: true,
      minDate: $("#FirstPaymentDate").val(),
      maxDate: $("#MaxPaymentDate").val(),
      dateFormat: "dd-mm-yy",
      showOtherMonths: false,
      dayNamesShort: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      beforeShow: function (input, inst) {
        $(inst.dpDiv).addClass('monthly-payment-date-widget');
      },
      onSelect: function () {
        var nominatedDate = $(this).val();
        postChangePaymentDateToApi(nominatedDate);
      }
    });

    $("Direct-Debit-Payment-Date").attr('aria-describedby', 'Date Input dd-mm-yy');
  }

  function positionDateCursorToFirstSelectableDate() {
    var cursorStart = getFirstSelectablePositionForMonth();
    $(".ui-datepicker-calendar tr:nth-child(" + cursorStart.rowStart + ") td:nth-child(" + cursorStart.columnStart + ")").find('a').focus();
  }

  function getFirstSelectablePositionForMonth() {
    var rowStart = 0;
    var columnStart = 0;
    var foundColumnStart = false;

    $(".ui-datepicker-calendar tr").each(function (rowIndex) {
      var $tds = $(this).find('td');
      $($tds).each(function (index) {
        var value = $(this).text();
        if ($.isNumeric(value) && (!$(this).hasClass('ui-datepicker-unselectable'))) {
          columnStart = index + 1;
          foundColumnStart = true;
          return false;
        }
      });
      if (foundColumnStart) {
        rowStart = rowIndex;
        return false;
      }
    });
    return obj = {
      rowStart: rowStart,
      columnStart: columnStart
    };
  }

  function sortCodeKeyUpEvent(event, currentSortCodePart, nextSortCodePart) {
    var TAB_KEYCODE = 9;
    var SHIFT_KEYCODE = 16;
    var LEFT_ARROW_KEYCODE = 37;
    var UP_ARROW_KEYCODE = 38;
    var RIGHT_ARROW_KEYCODE = 39;
    var DOWN_ARROW_KEYCODE = 40;

    var currentLength = $(currentSortCodePart).val().length;
    if (currentLength === 2
      && event.which !== TAB_KEYCODE
      && event.which !== SHIFT_KEYCODE
      && event.which !== LEFT_ARROW_KEYCODE
      && event.which !== UP_ARROW_KEYCODE
      && event.which !== RIGHT_ARROW_KEYCODE
      && event.which !== DOWN_ARROW_KEYCODE) {
      $(nextSortCodePart).focus().select();
    }
  }

  function sortCodeAutoTab() {
    var container1 = document.getElementById("SortCodePart1");
    var container2 = document.getElementById("SortCodePart2");

    container1.onkeyup = function (e) {
      sortCodeKeyUpEvent(e, "#SortCodePart1", "#SortCodePart2");
    };

    container2.onkeyup = function (e) {
      sortCodeKeyUpEvent(e, "#SortCodePart2", "#SortCodePart3");
    };
  }

  function postChangePaymentDateToApi(nominatedDate) {
    var dataToPost = {
      __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
      nominatedDate: nominatedDate
    };

    $.ajax({
      url: componentConfig.ApplicationPath + "TakePaymentComponent/ChangePaymentDate",
      type: "POST",
      cache: false,
      dataType: "json",
      data: dataToPost,
      success: function (result) {
        if (result.isSuccess === false) {
          window.location.href = result.errorRedirectUrl;
          return true;
        }
        return true;
      }
    });
  }

  function forceMonthlyPaymentMethodStoreAndSetPaymentDate() {
    $(".monthly-radio__label").on("click",
      function () {
        $("#" + $(this).attr("for")).click().focusout();
        $("#Direct-Debit-Payment-Date, #Credit-Card-Payment-Date")
          .val(sessionStorage.getItem("MonthlyInstalmentPaymentDate"));
      });
  }


  function getSecureSessionData() {
    return {
      "Name": sessionStorage.getItem("MonthlyInstalmentCardSelectedPersonIdText"),
      "CardCaptureUri": sessionStorage.getItem("MonthlyInstalmentCardCaptureUri"),
      "SecureSessionId": sessionStorage.getItem("MonthlyInstalmentSecureSessionId"),
      "SelectedPersonId": sessionStorage.getItem("MonthlyInstalmentCardSelectedPersonIdVal"),
      "ThreeDSecureUri": sessionStorage.getItem("MonthlyInstalmentThreeDSecureUri")
    };
  }

  function validateBankAccountAndSortCode() {
    $("#direct-debit-sort-code").val($("#SortCodePart1").val() + $("#SortCodePart2").val() + $("#SortCodePart3").val());
    var dataToPost = {
      __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
      sortcode: $("#direct-debit-sort-code").val(),
      accountnumber: $("#direct-debit-account-number").val()
    };
      
    $.ajax({
      url: componentConfig.ApplicationPath + "TakePaymentComponent/" + "PostAccountNumberAndSortCode",
      type: "POST",
      cache: false,
      dataType: "json",
      data: dataToPost,
      success: function (result) {
        if (result.isSuccess === false) {
          window.location.href = result.errorRedirectUrl;
          return true;
        }
        else if (result.Result === "Validated") {
          if (typeof bgl.common.pubsub !== 'undefined') {
            bgl.common.pubsub.emit('fakeFormSubmit', $('#Direct-Debit-Form').attr('id') || $('#Direct-Debit-Form').attr('action') || 'unknown');
          }
          RemoveSortCodeErrorStyle();
          RemoveAccountErrorStyle();
          bgl.common.utilities.redirectToUrl(componentConfig.MonthlyDepositUrl);
          return true;
        }
        else if (result.Result === "sortCode invalid") {
          AddSortCodeErrorStyle();
          scrollToFirstFieldWithErrorAndFocus();
          return false;
        }
        else if (result.Result === "accountNumber invalid") {
          AddAccountErrorStyle();
          scrollToFirstFieldWithErrorAndFocus();
          return false;
        }
        return false;
      }
    });
  }

  function RemoveSortCodeErrorStyle() {
    $("#sortcode-api-validation-message").addClass("hide");
    $("#SortCodeLabel").removeClass("sort-code__label--error");
    $("#sortcode-api-validation-message").removeClass("sort-code__span--error");
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").removeClass("sort-code__input--error");
    $("#sort-code-input-1, #sort-code-input-2, #sort-code-input-3").removeClass("error");
    cleanAriaDescribedBy("#sort-code-input-1");
  }

  function AddSortCodeErrorStyle() {
    $("#sortcode-api-validation-message").removeClass("hide");
    $("#SortCodeLabel").addClass("sort-code__label--error");
    $("#sortcode-api-validation-message").addClass("sort-code__span--error");
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").addClass("sort-code__input--error");
    $("#sort-code-input-1, #sort-code-input-2, #sort-code-input-3").addClass("error");
        
    cleanAriaDescribedBy($("#SortCodePart1,#SortCodePart2,#SortCodePart3"));
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").attr("aria-describedby", "sortcode-api-validation-message");
  }

  function RemoveIndividualSortCodeErrorStyle(element) {
    $(element).removeClass("sort-code__input--error");
    cleanAriaDescribedBy(element);
  }

  function AddIndividualSortCodeErrorStyle(element) {
    $("#SortCodeLabel").addClass("sort-code__label--error");
    $("#sortcode-validation-text").removeClass("hide");
    $(element).addClass("sort-code__input--error");

    cleanAriaDescribedBy(element);

    if ($(element).hasClass("form-row__input account-number sort-code__input--error")) {
      element.setAttribute("aria-describedby", "direct-debit-account-number-error");
    }
    else if ($(element).hasClass("form-row__input form-row__input--radio form-row__input--hidden sort-code__input--error")) {
      element.setAttribute("aria-describedby", "DirectDebitData.CanAuthorise-error");
    }
    else {
      $(element).attr("aria-describedby", "sortcode-validation-text");
    }
  }

  function AddAccountErrorStyle() {
    $("#account-number-validation-message").removeClass("hide");
    $("#AccountNumberLabel").addClass("sort-code__label--error");
    $("#account-number-validation-message").addClass("sort-code__span--error");
    $("#direct-debit-account-number").addClass("sort-code__input--error");
    $("#account-number-input").addClass("error");
        
    cleanAriaDescribedBy($("#account-number-input"));
    $("#direct-debit-account-number").attr("aria-describedby", "account-number-validation-message");
  }

  function RemoveAccountErrorStyle() {
    $("#account-number-validation-message").addClass("hide");
    $("#AccountNumberLabel").removeClass("sort-code__label--error");
    $("#account-number-validation-message").removeClass("sort-code__span--error");
    $("#direct-debit-account-number").removeClass("sort-code__input--error");
    $("#account-number-input").removeClass("error");
    cleanAriaDescribedBy("#account-number-input");
  }

  function cleanAriaDescribedBy(element) {
    if ($(element).attr("aria-describedby")) {
      $(element).removeAttr("aria-describedby");
    }
  }

  function dayTripper() {
    setTimeout(function () {
      var today = $('.ui-datepicker-today a')[0];

      if (!today) {
        today = $('.ui-state-active')[0] ||
          $('.ui-state-default')[0];
      }
      $("main").attr('id', 'dp-container');
      $("#dp-container").attr('aria-hidden', 'true');
      $("#skipnav").attr('aria-hidden', 'true');

      $(".ui-datepicker-current").hide();

      today.focus();
      datePickHandler();
      $(document).on('click', '#ui-datepicker-div .ui-datepicker-close', function () {
        closeCalendar();
      });
    }, 0);
  }

  function datePickHandler() {
    var activeDate;
    var container = document.getElementById('ui-datepicker-div');
    var input = document.getElementById('Direct-Debit-Payment-Date');

    if (!container || !input) {
      return;
    }

    $(container).find('table').first().attr('role', 'grid');

    container.setAttribute('role', 'application');
    container.setAttribute('aria-label', 'Calendar view date-picker');
    var prev = $('.ui-datepicker-prev', container)[0],
      next = $('.ui-datepicker-next', container)[0];


    next.href = 'javascript:void(0)';
    prev.href = 'javascript:void(0)';

    next.setAttribute('role', 'button');
    next.removeAttribute('title');
    prev.setAttribute('role', 'button');
    prev.removeAttribute('title');

    appendOffscreenMonthText(next);
    appendOffscreenMonthText(prev);

    $(next).on('click', handleNextClicks);
    $(prev).on('click', handlePrevClicks);

    monthDayYearText();

    $(container).on('keydown', function calendarKeyboardListener(keyVent) {
      var which = keyVent.which;
      var target = keyVent.target;
      var dateCurrent = getCurrentDate(container);

      if (!dateCurrent) {
        dateCurrent = $('a.ui-state-default')[0];
        setHighlightState(dateCurrent, container);
      }

      if (27 === which) {
        keyVent.stopPropagation();
        return closeCalendar();
      } else if (which === 9 && keyVent.shiftKey) {
        keyVent.preventDefault();
        if ($(target).hasClass('ui-state-default')) {
          $('.ui-datepicker-prev')[0].focus();
        } else if ($(target).hasClass('ui-datepicker-prev')) {
          $('.ui-datepicker-next')[0].focus();
        } else if ($(target).hasClass('ui-datepicker-next')) {
          positionDateCursorToFirstSelectableDate();
        }
      } else if (which === 9) { // TAB
        keyVent.preventDefault();
        if ($(target).hasClass('ui-datepicker-prev')) {
          positionDateCursorToFirstSelectableDate();

        } else if ($(target).hasClass('ui-state-default')) {
          $('.ui-datepicker-next')[0].focus();
        } else if ($(target).hasClass('ui-datepicker-next')) {
          $('.ui-datepicker-prev')[0].focus();
        }
      } else if (which === 37) {
        if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
          keyVent.preventDefault();
          previousDay(target);
        }
      } else if (which === 39) {
        if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
          keyVent.preventDefault();
          nextDay(target);
        }
      } else if (which === 38) {
        if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
          keyVent.preventDefault();
          upHandler(target, container, prev);
        }
      } else if (which === 40) {
        if (!$(target).hasClass('ui-datepicker-close') && $(target).hasClass('ui-state-default')) {
          keyVent.preventDefault();
          downHandler(target, container, next);
        }
      } else if (which === 13) {
        if ($(target).hasClass('ui-state-default')) {
          setTimeout(function () {
            closeCalendar();
          }, 100);
        } else if ($(target).hasClass('ui-datepicker-prev')) {
          handlePrevClicks();
        } else if ($(target).hasClass('ui-datepicker-next')) {
          handleNextClicks();
        }
      } else if (32 === which) {
        if ($(target).hasClass('ui-datepicker-prev') || $(target).hasClass('ui-datepicker-next')) {
          target.click();
        }
      } else if (33 === which) {
        moveOneMonth(target, 'prev');
      } else if (34 === which) {
        moveOneMonth(target, 'next');
      } else if (36 === which) {
        var firstOfMonth = $(target).closest('tbody').find('.ui-state-default')[0];
        if (firstOfMonth) {
          firstOfMonth.focus();
          setHighlightState(firstOfMonth, $('#ui-datepicker-div')[0]);
        }
      } else if (35 === which) {
        var $daysOfMonth = $(target).closest('tbody').find('.ui-state-default');
        var lastDay = $daysOfMonth[$daysOfMonth.length - 1];
        if (lastDay) {
          lastDay.focus();
          setHighlightState(lastDay, $('#ui-datepicker-div')[0]);
        }
      }
      $(".ui-datepicker-current").hide();
    });
  }

  function closeCalendar() {
    var container = $('#ui-datepicker-div');
    $(container).off('keydown');
    var input = $('#datepicker')[0];
    $(input).datepicker('hide');

    input.focus();
  }

  function removeAria() {
    $("#dp-container").removeAttr('aria-hidden');
    $("#skipnav").removeAttr('aria-hidden');
  }

  function moveOneMonth(currentDate, dir) {
    var button = (dir === 'next')
      ? $('.ui-datepicker-next')[0]
      : $('.ui-datepicker-prev')[0];

    if (!button) {
      return;
    }

    var ENABLED_SELECTOR = '#ui-datepicker-div tbody td:not(.ui-state-disabled)';
    var $currentCells = $(ENABLED_SELECTOR);
    var currentIdx = $.inArray(currentDate.parentNode, $currentCells);

    button.click();
    setTimeout(function () {
      updateHeaderElements();

      var $newCells = $(ENABLED_SELECTOR);
      var newTd = $newCells[currentIdx];
      var newAnchor = newTd && $(newTd).find('a')[0];

      while (!newAnchor) {
        currentIdx--;
        newTd = $newCells[currentIdx];
        newAnchor = newTd && $(newTd).find('a')[0];
      }

      setHighlightState(newAnchor, $('#ui-datepicker-div')[0]);
      newAnchor.focus();
    }, 0);
  }

  function handleNextClicks() {
    setTimeout(function () {
      updateHeaderElements();
      prepHighlightState();
      $('.ui-datepicker-next').focus();
      $(".ui-datepicker-current").hide();
    }, 0);
  }

  function handlePrevClicks() {
    setTimeout(function () {
      updateHeaderElements();
      prepHighlightState();
      $('.ui-datepicker-prev').focus();
      $(".ui-datepicker-current").hide();
    }, 0);
  }

  function previousDay(dateLink) {
    var container = document.getElementById('ui-datepicker-div');
    if (!dateLink) {
      return;
    }
    var td = $(dateLink).closest('td');
    if (!td) {
      return;
    }

    var prevTd = $(td).prev(),
      prevDateLink = $('a.ui-state-default', prevTd)[0];

    if (prevTd && prevDateLink) {
      setHighlightState(prevDateLink, container);
      prevDateLink.focus();
    } else {
      handlePrevious(dateLink);
    }
  }


  function handlePrevious(target) {
    var container = document.getElementById('ui-datepicker-div');
    if (!target) {
      return;
    }
    var currentRow = $(target).closest('tr');
    if (!currentRow) {
      return;
    }
    var previousRow = $(currentRow).prev();

    if (!previousRow || previousRow.length === 0) {
      previousMonth();
    } else {
      var prevRowDates = $('td a.ui-state-default', previousRow);
      var prevRowDate = prevRowDates[prevRowDates.length - 1];

      if (prevRowDate) {
        setTimeout(function () {
          setHighlightState(prevRowDate, container);
          prevRowDate.focus();
        }, 0);
      }
    }
  }

  function previousMonth() {
    var prevLink = $('.ui-datepicker-prev')[0];
    var container = document.getElementById('ui-datepicker-div');
    prevLink.click();
    setTimeout(function () {
      var trs = $('tr', container),
        lastRowTdLinks = $('td a.ui-state-default', trs[trs.length - 1]),
        lastDate = lastRowTdLinks[lastRowTdLinks.length - 1];

      updateHeaderElements();
      setHighlightState(lastDate, container);
      lastDate.focus();
    }, 0);
  }

  function nextDay(dateLink) {
    var container = document.getElementById('ui-datepicker-div');
    if (!dateLink) {
      return;
    }
    var td = $(dateLink).closest('td');
    if (!td) {
      return;
    }
    var nextTd = $(td).next(),
      nextDateLink = $('a.ui-state-default', nextTd)[0];

    if (nextTd && nextDateLink) {
      setHighlightState(nextDateLink, container);
      nextDateLink.focus();
    } else {
      handleNext(dateLink);
    }
  }

  function handleNext(target) {
    var container = document.getElementById('ui-datepicker-div');
    if (!target) {
      return;
    }
    var currentRow = $(target).closest('tr'),
       nextRow = $(currentRow).next();

    if (!nextRow || nextRow.length === 0) {
       nextMonth();
    } else {
      var nextRowFirstDate = $('a.ui-state-default', nextRow)[0];
      if (nextRowFirstDate) {
        setHighlightState(nextRowFirstDate, container);
        nextRowFirstDate.focus();
      }
    }
  }

  function nextMonth() {
    nextMon = $('.ui-datepicker-next')[0];
    var container = document.getElementById('ui-datepicker-div');
    nextMon.click();
    setTimeout(function () {
      updateHeaderElements();
      var firstDate = $('a.ui-state-default', container)[0];
      setHighlightState(firstDate, container);
      firstDate.focus();
    }, 0);
  }

  function upHandler(target, cont, prevLink) {
    prevLink = $('.ui-datepicker-prev')[0];
    var rowContext = $(target).closest('tr');
    if (!rowContext) {
       return;
    }
    var rowTds = $('td', rowContext),
      rowLinks = $('a.ui-state-default', rowContext),
      targetIndex = $.inArray(target, rowLinks),
      prevRow = $(rowContext).prev(),
      prevRowTds = $('td', prevRow),
      parallel = prevRowTds[targetIndex],
      linkCheck = $('a.ui-state-default', parallel)[0];

    if (prevRow && parallel && linkCheck) {
      setHighlightState(linkCheck, cont);
      linkCheck.focus();
    } else {
      prevLink.click();
      setTimeout(function () {
        updateHeaderElements();
        var newRows = $('tr', cont),
          lastRow = newRows[newRows.length - 1],
          lastRowTds = $('td', lastRow),
          tdParallelIndex = $.inArray(target.parentNode, rowTds),
          newParallel = lastRowTds[tdParallelIndex],
          newCheck = $('a.ui-state-default', newParallel)[0];

        if (lastRow && newParallel && newCheck) {
          setHighlightState(newCheck, cont);
          newCheck.focus();
        } else {
          var secondLastRow = newRows[newRows.length - 2],
            secondTds = $('td', secondLastRow),
            targetTd = secondTds[tdParallelIndex],
            linkCheck = $('a.ui-state-default', targetTd)[0];

          if (linkCheck) {
            setHighlightState(linkCheck, cont);
            linkCheck.focus();
          }
        }
      }, 0);
    }
  }

  function downHandler(target, cont, nextLink) {
    nextLink = $('.ui-datepicker-next')[0];
    var targetRow = $(target).closest('tr');
    if (!targetRow) {
      return;
    }
    var targetCells = $('td', targetRow),
      cellIndex = $.inArray(target.parentNode, targetCells),
      nextRow = $(targetRow).next(),
      nextRowCells = $('td', nextRow),
      nextWeekTd = nextRowCells[cellIndex],
      nextWeekCheck = $('a.ui-state-default', nextWeekTd)[0];

    if (nextRow && nextWeekTd && nextWeekCheck) {
      setHighlightState(nextWeekCheck, cont);
      nextWeekCheck.focus();
    } else {
      nextLink.click();

      setTimeout(function () {
        updateHeaderElements();

        var nextMonthTrs = $('tbody tr', cont),
          firstTds = $('td', nextMonthTrs[0]),
          firstParallel = firstTds[cellIndex],
          firstCheck = $('a.ui-state-default', firstParallel)[0];

        if (firstParallel && firstCheck) {
          setHighlightState(firstCheck, cont);
          firstCheck.focus();
        } else {
          var secondRow = nextMonthTrs[1],
            secondTds = $('td', secondRow),
            secondRowTd = secondTds[cellIndex],
            secondCheck = $('a.ui-state-default', secondRowTd)[0];

          if (secondRow && secondCheck) {
            setHighlightState(secondCheck, cont);
            secondCheck.focus();
          }
        }
      }, 0);
    }
  }

  function monthDayYearText() {
    var cleanUps = $('.amaze-date');
    $(cleanUps).each(function (clean) {
      clean.parentNode.removeChild(clean);
    });

    var datePickDiv = document.getElementById('ui-datepicker-div');
    if (!datePickDiv) {
        return;
    }

    var dates = $('a.ui-state-default', datePickDiv);
    $(dates).attr('role', 'button').on('keydown', function (e) {
      if (e.which === 32) {
        e.preventDefault();
        e.target.click();
        setTimeout(function () {
            closeCalendar();
        }, 100);
      }
    });
    $(dates).each(function (index, date) {
      var currentRow = $(date).closest('tr'),
        currentTds = $('td', currentRow),
        currentIndex = $.inArray(date.parentNode, currentTds),
        headThs = $('thead tr th', datePickDiv),
        dayIndex = headThs[currentIndex],
        daySpan = $('span', dayIndex)[0],
        monthName = $('.ui-datepicker-month', datePickDiv)[0].innerHTML,
        year = $('.ui-datepicker-year', datePickDiv)[0].innerHTML,
        number = date.innerHTML;

      if (!daySpan || !monthName || !number || !year) {
        return;
      }

      var dateText = date.innerHTML + ' ' + monthName + ' ' + year + ' ' + daySpan.title;
      date.setAttribute('aria-label', dateText);
    });
  }

  function updateHeaderElements() {
    var context = document.getElementById('ui-datepicker-div');
    if (!context) {
      return;
    }

    $(context).find('table').first().attr('role', 'grid');

    prev = $('.ui-datepicker-prev', context)[0];
    next = $('.ui-datepicker-next', context)[0];

    next.href = 'javascript:void(0)';
    prev.href = 'javascript:void(0)';

    next.setAttribute('role', 'button');
    prev.setAttribute('role', 'button');
    appendOffscreenMonthText(next);
    appendOffscreenMonthText(prev);

    $(next).on('click', handleNextClicks);
    $(prev).on('click', handlePrevClicks);

    monthDayYearText();
  }


  function prepHighlightState() {
    var highlight;
    var cage = document.getElementById('ui-datepicker-div');
    highlight = $('.ui-state-highlight', cage)[0] ||
       $('.ui-state-default', cage)[0];
    if (highlight && cage) {
       setHighlightState(highlight, cage);
    }
  }

  function setHighlightState(newHighlight, container) {
    var prevHighlight = getCurrentDate(container);
    $(prevHighlight).removeClass('ui-state-highlight');
    $(newHighlight).addClass('ui-state-highlight');
  }


  function getCurrentDate(container) {
    var currentDate = $('.ui-state-highlight', container)[0];
    return currentDate;
  }

  function appendOffscreenMonthText(button) {
    var buttonText;
    var isNext = $(button).hasClass('ui-datepicker-next');
    var months = [
      'january',
      'february',
      'march',
      'april',
      'may',
      'june',
      'july',
      'august',
      'september',
      'october',
      'november',
      'december'
    ];

    var currentMonth = $('.ui-datepicker-title .ui-datepicker-month').text().toLowerCase();
    var monthIndex = $.inArray(currentMonth.toLowerCase(), months);
    var currentYear = $('.ui-datepicker-title .ui-datepicker-year').text().toLowerCase();
    var adjacentIndex = (isNext) ? monthIndex + 1 : monthIndex - 1;

    if (isNext && currentMonth === 'december') {
      currentYear = parseInt(currentYear, 10) + 1;
      adjacentIndex = 0;
    } else if (!isNext && currentMonth === 'january') {
      currentYear = parseInt(currentYear, 10) - 1;
      adjacentIndex = months.length - 1;
    }

    buttonText = (isNext)
      ? 'Next Month, ' + firstToCap(months[adjacentIndex]) + ' ' + currentYear
      : 'Previous Month, ' + firstToCap(months[adjacentIndex]) + ' ' + currentYear;

    $(button).find('.ui-icon').html(buttonText);
  }

  function firstToCap(s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  return {
    init: initComponent,
    load: loadComponent,
    getValidationMessages: getValidationMessages
  };
})();