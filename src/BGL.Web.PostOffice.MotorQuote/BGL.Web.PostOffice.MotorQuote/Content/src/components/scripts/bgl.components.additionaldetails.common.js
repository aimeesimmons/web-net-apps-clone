﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.additionaldetails = bgl.components.additionaldetails || {};

bgl.components.additionaldetails.common = (function () {

    var configuration;
    var form;
    var monthValidation, yearValidation;
    var sectionLabel;
    var errorClass = 'error';

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            bgl.components.additionaldetails.common.init(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#' + componentConfiguration.Id + "-form-additional-details");
        monthValidation = $('#Month-Section');
        yearValidation = $('#Year-Section');
        sectionLabel = $("#Additional-Input-Purchase-Section");

        bgl.common.validator.init(form, {
            onfocusout: resetValidation,
            onclick: resetValidation
        });

        subscribeToEvents();
        almostThereButtonSubmit();
    }

    function resetValidation(element) {
        bgl.common.validator.resetValidationState($(element));
        purchaseSectionValidationMessage();
    }

    function almostThereButtonSubmit() {
        $("#almost-there-button").on("click",
            function() {
                form.submit();
            });
    }

    function subscribeToEvents() {
        form.off('submit').on('submit',
            function (event) {
                event.preventDefault();

                updateRegistrationValue($("#Additional-Input-Registration"));

                bgl.common.validator.validate(configuration, $(this));

                purchaseSectionValidationMessage();
            });

        $("#Additional-Input-Registration").off("input").on("input",
            function () {

                var start = this.selectionStart;
                var end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });

        $("#Additional-BackButton").off("click").on("click",
            function () {
                var options = {
                    callback: function () {
                        window.location = configuration.PreviousPageUrl;
                    }
                };
                bgl.common.pubsub.emit('PollBasket', options);
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function purchaseSectionValidationMessage() {
        if (monthValidation.hasClass(errorClass) && yearValidation.hasClass(errorClass)) {
            sectionLabel.addClass(errorClass);
        } else {
            sectionLabel.removeClass(errorClass);
        }
    }

    function updateRegistrationValue(inputField) {
        if (inputField.length !== 0) {
            inputField.val(inputField.val().replace(/\s+/gi, ""));
        }
    }

    function onSuccess(configuration, data) {
        bgl.common.pubsub.emit('PollBasket',
            {
                callback: function () {
                    bgl.common.utilities.redirectToUrl(data.redirectUrl);
                },
                hideNotification: true
            });
    }

    function onError(configuration) {
        var params = {
            callback: bgl.common.overlay.hide,
            spinnerName: "addonSpinner"
        };

        bgl.common.pubsub.emit("PollBasket", params);
        bgl.components.additionaldetails.common.init(configuration);
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.additionaldetails.common;
}
