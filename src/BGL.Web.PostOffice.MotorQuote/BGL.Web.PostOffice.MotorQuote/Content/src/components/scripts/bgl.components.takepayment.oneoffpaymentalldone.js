﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.oneoffpaymentalldone = (function () {

    function initComponent() {
        clearOneOffPaymentSessionData();
        updateSessionDataForCancellationTracking();
        bgl.common.pubsub.emit('PaymentComplete');
        bgl.components.takepayment.cancellations.init();
    }

    function clearOneOffPaymentSessionData() {
        sessionStorage.removeItem("OneOffPaymentSelectedPersonIdText");
        sessionStorage.removeItem("OneOffPaymentCardCaptureUri");
        sessionStorage.removeItem("OneOffPaymentSecureSessionId");
        sessionStorage.removeItem("OneOffPaymentSelectedPersonIdVal");
        sessionStorage.removeItem("OneOffPaymentSelectedPersonId");
    }

    function updateSessionDataForCancellationTracking() {
        sessionStorage.setItem('TagManager.CancellationReason', sessionStorage.getItem('Cancellation.Reason'));
        sessionStorage.removeItem('Cancellation.Reason');
        sessionStorage.setItem('TagManager.CancellationDate', sessionStorage.getItem('Cancellation.Date'));
        sessionStorage.removeItem('Cancellation.Date');
    }

    return {
        init: initComponent
    };
})();