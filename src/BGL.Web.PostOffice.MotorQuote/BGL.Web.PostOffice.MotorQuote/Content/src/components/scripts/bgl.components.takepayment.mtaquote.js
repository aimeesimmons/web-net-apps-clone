﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.mtaquote = (function () {

    function initComponent() {
        $('#toggle-payment-breakdown-table').on('click', function (event) {
            event.preventDefault();
            if ($('[data-payment-breakdown-table]').hasClass('hide')) {
                $('[data-payment-breakdown-table]').attr('class', 'flex');
                $(this).text('Hide new monthly payment plan');
            } else {
                $('[data-payment-breakdown-table]').attr('class', 'hide');
                $(this).text('View new monthly payment plan');
            }
        });
    }

    return {
        init: initComponent
    };
})();