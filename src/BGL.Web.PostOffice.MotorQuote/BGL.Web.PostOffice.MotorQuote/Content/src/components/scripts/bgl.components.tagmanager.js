﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

var digitalData = window.digitalData || {};
digitalData.page_data = digitalData.page_data || {};
digitalData.user_data = digitalData.user_data || {};
digitalData.application_data = digitalData.application_data || {};
digitalData.application_data.insurance_data = digitalData.application_data.insurance_data || {};


bgl.components.tagmanager = (function () {

    function initComponent() {
        setPageProperties();
        setReferrerProperties();
        setTimestampProperty();
        setPageCategories();
        setJavascriptErrorProperty();
        setUpdatedUserDetails();
        setUpdatedVehicleDetails();
        subscribeToStatusesAndTimingEvents();
        checkStorageAndUpdateApplicationData();
        subscribeToErrorData();
    }

    function setPageProperties() {
        digitalData.page_data.page_url = window.location.href;
        digitalData.page_data.page_title = document.title;
        digitalData.page_data.page_path = window.location.pathname;
    }

    function setReferrerProperties() {
        digitalData.page_data.page_previous_url = document.referrer;
        if (document.referrer.length > 0) {
            var newUrl = new URL(document.referrer);
            digitalData.page_data.page_previous_path = newUrl.pathname;
        }
    }

    function setTimestampProperty() {
        digitalData.page_data.timestamp = (new Date()).toString();
    }

    function setPageCategories() {
        var pathArray = window.location.pathname.split('/');
        pathArray.pop();
        digitalData.page_data.page_categories = pathArray;
    }

    function setJavascriptErrorProperty() {
        digitalData.page_data.javascript_errors = [];
        window.onerror = function(error, url, line) {
            digitalData.page_data.javascript_errors.push('ERR:' + error + ' URL:' + url + ' L:' + line);
        };
    }

    function setUpdatedUserDetails() {
        digitalData.user_data.detail_updated = JSON.parse(sessionStorage.getItem("ChangedUserData"));
    }

    function setUpdatedVehicleDetails() {
        digitalData.application_data.insurance_data.quote_updated = JSON.parse(sessionStorage.getItem("ChangedVehicleData"));
    }

    function checkStorageAndUpdateApplicationData() {
        checkForSessionAndClearStorageItems();
        checkForStartedTimeAndUpdateDigitalData();
        checkForQuoteSavedAndUpdateDigitalData();
        checkForQuoteRetrievedAndUpdateDigitalData();
        checkForPaymentSuccessfulAndUpdateDigitalData();
        checkForPaymentUnsuccessfulAndUpdateDigitalData();
        checkForApplicationOrderedTimeAndUpdateDigitalData();
        storeTimeTakenInDigitalData();
        checkForQuotedTimeAndUpdateDigitalData();
        checkForUnableToQuoteAndUpdateDigitalData();
        checkForOrderStartTimeAndUpdateDigitalData();
    }

    function checkForSessionAndClearStorageItems() {
        if (localStorage.getItem("Session") !== digitalData.user_data.user_id) {
            localStorage.setItem("ApplicationOrderedTime", "");
            localStorage.setItem("QuoteRetrieved", "");
            localStorage.setItem("ApplicationOrderStartTime", "");
            localStorage.setItem("ApplicationStartedTime", "");
            localStorage.setItem("QuoteSaved", "");
            localStorage.setItem("PaymentSuccessful", "");
            localStorage.setItem("PaymentUnsuccessful", "");
            localStorage.setItem("ApplicationQuotedTime", "");
            localStorage.setItem("UnableToQuote", "");

            localStorage.setItem("Session", digitalData.user_data.user_id);
        }
    }

    function subscribeToStatusesAndTimingEvents() {
        bgl.common.pubsub.on("ApplicationStarted", function (getAppStartTime) {
            storeApplicationStartedTIme(getAppStartTime);
            checkForStartedTimeAndUpdateDigitalData();
        });

        bgl.common.pubsub.on("ApplicationQuoted", function (getAppQuotedTime) {
            storeApplicationQuotedTimeAndOrderStartTime(getAppQuotedTime);
            checkForQuotedTimeAndUpdateDigitalData();
        });

        bgl.common.pubsub.on("UnableToQuote", function () {
            localStorage.setItem("UnableToQuote", true);
            checkForUnableToQuoteAndUpdateDigitalData();
        });

        bgl.common.pubsub.on("SavedQuote", function () {
            localStorage.setItem("QuoteSaved", true);
            checkForQuoteSavedAndUpdateDigitalData();
        });

        bgl.common.pubsub.on("QuoteRetrieved", function () {
            localStorage.setItem("QuoteRetrieved", true);
            checkForQuoteRetrievedAndUpdateDigitalData();
        });

        bgl.common.pubsub.on("PaymentSuccessful", function (applicationOrderedTime) {
            localStorage.setItem("PaymentSuccessful", true);
            storeApplicationOrderedTime(applicationOrderedTime);
            checkForPaymentSuccessfulAndUpdateDigitalData();
        });

        bgl.common.pubsub.on("PaymentUnsuccessful", function () {
            localStorage.setItem("PaymentUnsuccessful", true);
            checkForPaymentUnsuccessfulAndUpdateDigitalData();
        });
    }

    function checkForStartedTimeAndUpdateDigitalData() {
        if (localStorage.getItem("ApplicationStartedTime") !== "") {
            digitalData.application_data.application_started = true;
        }
    }

    function checkForOrderStartTimeAndUpdateDigitalData() {
        if (localStorage.getItem("ApplicationOrderStartTime") !== "") {
            digitalData.application_data.application_quoted = true;
        }
    }

    function checkForQuoteSavedAndUpdateDigitalData() {
        if (localStorage.getItem("QuoteSaved") !== "") {
            digitalData.application_data.application_saved = true;
        }
    }

    function checkForQuoteRetrievedAndUpdateDigitalData() {
        if (localStorage.getItem("QuoteRetrieved") !== "") {
            digitalData.application_data.retrieved_quote = true;
        }
    }

    function checkForPaymentSuccessfulAndUpdateDigitalData() {
        if (localStorage.getItem("PaymentSuccessful") !== "") {
            digitalData.application_data.application_ordered = true;
            checkForApplicationOrderedTimeAndUpdateDigitalData();
        }
    }

    function checkForPaymentUnsuccessfulAndUpdateDigitalData() {
        if (localStorage.getItem("PaymentUnsuccessful") !== "") {
            digitalData.application_data.application_failed_payment = true;
        }
    }

    function checkForApplicationOrderedTimeAndUpdateDigitalData() {
        if (localStorage.getItem("ApplicationOrderedTime") !== "") {
            var orderStartTime = Date.parse(localStorage.getItem("ApplicationOrderStartTime"));
            var orderEndTime = Date.parse(localStorage.getItem("ApplicationOrderedTime"));

            digitalData.application_data.application_time_to_order = calculateTimeTaken(orderStartTime, orderEndTime);
        }
    }

    function storeTimeTakenInDigitalData() {
        if (localStorage.getItem("ApplicationQuotedTime") !== "" && localStorage.getItem("ApplicationStartedTime") !== "") {
            var quoteStartTime = Date.parse(localStorage.getItem("ApplicationStartedTime"));
            var quoteEndTime = Date.parse(localStorage.getItem("ApplicationQuotedTime"));
            digitalData.application_data.application_time_to_quote = calculateTimeTaken(quoteStartTime, quoteEndTime);
        }
    }

    function checkForQuotedTimeAndUpdateDigitalData() {
        if (localStorage.getItem("ApplicationQuotedTime") !== "") {
            digitalData.application_data.application_quoted = true;
        }
    }

    function checkForUnableToQuoteAndUpdateDigitalData() {
        if (localStorage.getItem("UnableToQuote") !== "" && digitalData.application_data.application_quoted === true) {
            digitalData.application_data.application_failed_order = true;
        } else if (localStorage.getItem("UnableToQuote") !== "" && localStorage.getItem("Session") === digitalData.user_data.user_id) {
            digitalData.application_data.quote_failed = true;
        }
    }

    function storeApplicationStartedTIme(getAppStartTime) {
        if (localStorage.getItem("ApplicationStartedTime") === "") {
            localStorage.setItem("ApplicationStartedTime", getAppStartTime);
        }
    }

    function storeApplicationQuotedTimeAndOrderStartTime(getAppQuotedTime) {
        if (localStorage.getItem("ApplicationQuotedTime") === "") {
            localStorage.setItem("ApplicationQuotedTime", getAppQuotedTime);
            localStorage.setItem("ApplicationOrderStartTime", getAppQuotedTime);
            storeTimeTakenInDigitalData();
        }
    }

    function storeApplicationOrderedTime(applicationOrderedTime) {
        if (localStorage.getItem("PaymentSuccessful") !== "") {
            localStorage.setItem("ApplicationOrderedTime", applicationOrderedTime);
        }
    }

    function calculateTimeTaken(startTime, endTime) {
        var duration = endTime - startTime;

        var seconds = Math.floor((duration / 1000) % 60);
        var minutes = Math.floor((duration / (1000 * 60)) % 60);
        var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        var timeTakenToQuote = (hours + ":" + minutes + ":" + seconds);

        return timeTakenToQuote;
    }
	
    function subscribeToErrorData() {
        bgl.common.pubsub.on('formValidationFailed', function (data) {
            document.dispatchEvent(new CustomEvent("form_field_error", {
                detail: {
                    form_title: digitalData.page_data.page_path,
                    form_section: data.form_section,
                    form_page: digitalData.page_data.page_title,
                    page_url: digitalData.page_data.page_url,
                    error_details: [data.error_details]
                }
            }));
        });
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.tagmanager;
}