﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.marketingpreferences = (function () {
    var component;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        component = $("#" + componentConfiguration.Id);

        component.find("input[type='checkbox']").on("change", submitComponentForm);

        component.find("a[data-id=optOutAll]").on("click", function (event) {
            event.preventDefault();

            submitOptOutAll();
        });

        $('.contact-tiles__tile').off('keypress').on('keypress',
            function (event) {

                var code = event.charCode || event.keyCode;

                if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                    event.preventDefault();
                    $(this).click();
                    $(this).focus();
                }
            });
    }

    function getData() {
        var token = component.find("input[name='__RequestVerificationToken']").val();
        var radioButtonList = [];

        component.find(".contact-tiles__input[type='checkbox']").each(function () {
            var element = $(this);
            radioButtonList.push({
                Id: element.data('preference-id'),
                IsContactTileSelected: element.is(':checked')
            });
        });

        return {
            __RequestVerificationToken: token,
            variant: component.find("[data-id=variant]").val(),
            MarketingPreferences: radioButtonList
        };
    }

  function submitComponentForm() {
    var form = component.find('form');
    if (typeof bgl.common.pubsub !== 'undefined') {
      bgl.common.pubsub.emit('fakeFormSubmit', form.attr('id') || form.attr('action') || 'unknown');
    }
    $.ajax({
      url: $(form).attr('action'),
      type: $(form).attr('method'),
      data: getData(),
      success: function (data) {
        if (!data.isSuccess) {
            bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
        }
      }
    });
  }

  function submitOptOutAll() {
    var form = component.find('form');
    if (typeof bgl.common.pubsub !== 'undefined') {
      bgl.common.pubsub.emit('fakeFormSubmit', form.attr('id') || form.attr('action') || 'unknown');
    }
    var token = component.find('input[name="__RequestVerificationToken"]').val();

    $.ajax({
      url: $(form).attr('action'),
      type: $(form).attr('method'),
      data: { __RequestVerificationToken: token },
      success: function(data) {
        if (data.isSuccess) {
          $('[data-id=optOutAllConfirmation]').removeClass('hide');
        } else {
          bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
        }
      }
    });
  }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.marketingpreferences;
}