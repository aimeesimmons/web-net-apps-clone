﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

var vehicleBodyTypeCode = "VehicleBodyTypeCode";

bgl.components.vehicledetails.vehiclebodytype = (function () {
    var doneButton,
        radioButtons,
        configuration,
        form,
        containerId;

    function loadComponent(model, componentUrl, containerElementId) {

        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.vehiclebodytype.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $("#vehicle_edit_body-type_form");
        doneButton = $('#vehicle-body_type_done-button');
        radioButtons = $("input[type='radio']");
        
        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);
        subscribeToDoneEvents();
        subscribeToRadioButtonEvents();
        subscribeToBackButtonEvents();
    }

    function subscribeToRadioButtonEvents() {
        var hasValue = radioButtons.is(':checked');

        if (!hasValue) {
            doneButton.addClass("hide");
        } else {
            doneButton.removeClass("hide");
        }

        $.each(radioButtons,
            function(key, item) {
                var button = $(item);

                button.on("change",
                    function (event) {
                        event.preventDefault();
                        if (!hasValue) {
                            form.submit();
                        }
                    });
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToDoneEvents() {

        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanged").val(bgl.common.datastore.isDataChanged($(this)));

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });
    }

    function subscribeToBackButtonEvents() {
        $("#back-button").off("click").on("click",
            function (event) {
                event.preventDefault();

                var link = $(event.currentTarget);

                if (configuration.JourneyType.toString() === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
                    bgl.common.utilities.redirectToUrl(link.data("action-url"));
                } else {
                    var containerSelector = link.data("container");

                    var onEventName = link.data("init-event");
                    var callback = eval(onEventName);

                    var model = bgl.common.datastore.getData(form);
                    model.ComponentConfiguration = configuration;

                    if (typeof callback === "function") {
                        bgl.common.loader.load(model,
                            link.data("action-url"),
                            containerSelector,
                            function () {
                                callback(configuration);
                            });
                    }
                }
            });
    }

    function onSuccess(configuration, data) {

        if (data.journeyType.toString() === bgl.components.journeyTypesVehicleDetails.LookupEdit) {
            var model = bgl.common.datastore.getData(form);
            model.ComponentConfiguration = configuration;

            bgl.components.vehicledetails.numberofseats.load(model,
                data.componentUrl,
                containerId);
        } else if (data.journeyType.toString() === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
            bgl.common.utilities.redirectToUrl(data.componentUrl);
        }
        else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit("PollBasket");
            }
            bgl.common.datastore.removeSessionStorage(form);
        }
    }


    function onError() {
        bgl.components.vehicledetails.vehiclebodytype.init(configuration);
    }

    return {
        load: loadComponent,
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();