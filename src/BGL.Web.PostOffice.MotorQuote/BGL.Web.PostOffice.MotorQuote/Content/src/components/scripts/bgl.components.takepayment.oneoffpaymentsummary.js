﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.oneoffpaymentsummary = (function () {
    var componentConfig;
    var requestVerificationToken;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        componentConfiguration.CardSessionData = getSecureSessionData();
        componentConfig = componentConfiguration;
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        var payNowButton = $("#One-Off-Pay-Now-Button");
        var cancellationConfirmationCheckbox = $("#cancellation-confirmation-checkbox");

        bgl.components.takepayment.cancellations.init(componentConfiguration);

        payNowButton.on("click",
            function () {
                commitOneOffPayment();
            });
        cancellationConfirmationCheckbox.on("click",
            function() {
                var isChecked = cancellationConfirmationCheckbox.is(":checked");

                if (isChecked) {
                    payNowButton.removeAttr("disabled");
                } else {
                    payNowButton.attr("disabled", "disabled");
                }
            });
    }

    function getSecureSessionData() {
        return {
            "Name": sessionStorage.getItem("OneOffPaymentSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("OneOffPaymentCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("OneOffPaymentSecureSessionId")
        };
    }

    function commitOneOffPayment() {
        requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            commitData: {
                ComponentConfiguration: componentConfig,
                CommitCardData: {
                    SecureSessionId: sessionStorage.getItem("OneOffPaymentSecureSessionId"),
                    SelectedPersonIdVal: sessionStorage.getItem("OneOffPaymentSelectedPersonIdVal"),
                    SelectedPersonIdText: sessionStorage.getItem("OneOffPaymentSelectedPersonIdText")
                }
            }
        };

        $("body").html(bgl.common.spinner.getSpinnerBody("Processing your payment!",
            "Please do not hit the back button or refresh the page"));

        var urlPrefix = componentConfig.ApplicationPath !== null ? componentConfig.ApplicationPath : "/";

        $.ajax({
            url: urlPrefix + "TakePaymentComponent/CommitOneOffPayment",
            type: "POST",
            cache: false,
            dataType: "json",
            data: dataToPost,
            success: function (result) {
                if (result.isSuccess === false) {
                    bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
                    return;
                }
                bgl.common.utilities.redirectToUrl(result.RedirectUrl);
            }
        });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();