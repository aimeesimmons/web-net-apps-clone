﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.sameaddress = (function () {
    var form,
        configuration,
        container,
        registerKeeper,
        continueButton,
        SPACE_KEYCODE = 32,
        ENTER_KEYCODE = 13;

    function initComponent(componentConfiguration, containerId) {
        initElements(componentConfiguration, containerId);

        registerKeeper.redirectIfSessionStorageIsNotFound(form, componentConfiguration.ParentJourneyUrl);

        subscribeForEvents();

        registerKeeper.updateHeading(form);

        registerKeeper.displaySubmitButton(form);
    }

    function loadComponent(config, url, containerId) {
        bgl.common.loader.load(config,
            url,
            containerId,
            function () {
                initComponent(config, containerId);
            });
    }

    function onMultiStepEditSuccess(configuration, data) {
        registerKeeper.clearOtherOptionFields(form);

        var registeredKeeperData = { RegisteredKeeperId: data.registeredKeeperId };

        bgl.common.datastore.extendCollectedData(form, registeredKeeperData);
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onSuccess(configuration, data) {
        registerKeeper.clearOtherOptionFields(form);
        bgl.components.coverdetails.registeredkeeper.load(configuration, data.nextUrl, container, true);
    }

    function onError() {
        initComponent(configuration);
    }

    function initElements(componentConfiguration, containerId) {
        configuration = componentConfiguration;
        form = $("#same-address-form");
        continueButton = $("#continueButton");
        container = containerId;
        registerKeeper = bgl.components.coverdetails.registeredkeeper;
    }

    function subscribeForEvents() {
        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();

        $('input[type="radio"]')
            .off('click')
            .off('keypress')
            .on({
                "keypress": function (e) {
                    if (e.keyCode === ENTER_KEYCODE || e.keyCode === SPACE_KEYCODE) {
                        $(this).click();
                    }
                },
                'click': function (event) {
                    registerKeeper.submitForm(form);
                }
            });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    continueButton.attr("disabled", true);
                    if (bgl.common.validator.isFormValid(form)) {

                        var hasOwnData = form.data('hasOwnData');

                        bgl.common.validator.submitForm(form, configuration, hasOwnData ? getStoredData() : null);
                    } else {
                        continueButton.attr("disabled", false);
                    }
                });

        form.find('.back-button-link').off('click').one('click',
            function (event) {
                event.preventDefault();

                var url = $(this).attr('href');

                bgl.components.coverdetails.maritalstatus.load(configuration, url, container);
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function getStoredData() {
        var data = getDataToPost();
        var formData = form.serialize();

        return formData + "&" + $.param(data);
    }

    function getDataToPost() {
        var data = bgl.common.datastore.getData(form);
        var config = { ComponentConfiguration: configuration };

        $.extend(data, config);

        return data;
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onMultiStepEditSuccess: onMultiStepEditSuccess,
        onError: onError
    };
})();