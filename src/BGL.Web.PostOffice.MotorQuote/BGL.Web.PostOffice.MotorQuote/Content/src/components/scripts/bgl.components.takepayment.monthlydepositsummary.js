﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.monthlydepositsummary = (function () {
    var componentConfig;
    var redirectInterval;
    var redirectTick = 0;
    var maxRedirectWait = 25;
    var requestVerificationToken;
    var isPaymentSuccessful = false;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        componentConfiguration.CardSessionData = getSecureSessionData();
        componentConfiguration.CreditInformationUrl = sessionStorage.getItem("credit-information");
        componentConfig = componentConfiguration;
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent();
            });
    }

    function initComponent() {
        setupTimeline();
        setupImportantInformation();

        $("#Monthly-Pay-Now-Button").on("click",
            function () {
                if (!$("#Read-Confirm-Checkbox").prop("checked")) {
                    $("#Read-Confirm-Container").addClass("error");
                } else {
                    commitInstalmentAndDepositPaymentsAndBasket();
                }
            });

        $("#Read-Confirm-Checkbox").on("click",
            function () {
                if ($("#Read-Confirm-Checkbox").prop("checked")) {
                    $("#Read-Confirm-Container").removeClass("error");
                }
            });
    }

    function setupTimeline() {

        $(".schedule-bar__item").each(function(i, el) {
            var itemWidth = $(el).data("width");
            $(el).css("width", itemWidth + "%");
        });

        $(".schedule-bar__item.active").prevAll(".schedule-bar__item").each(function(i, el) {
            $(el).addClass("active");
        });
    }

    function getSecureSessionData() {
        if (sessionStorage.getItem("UseMonthlyInstalmentCard") === "true") {
            return {
                "Name": sessionStorage.getItem("MonthlyInstalmentCardSelectedPersonIdText"),
                "CardCaptureUri": sessionStorage.getItem("MonthlyInstalmentCardCaptureUri"),
                "SecureSessionId": sessionStorage.getItem("MonthlyInstalmentSecureSessionId")
            };
        }
        return {
            "Name": sessionStorage.getItem("MonthlyDepositSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("MonthlyDepositCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("MonthlyDepositSecureSessionId")
        };
    }

  function commitInstalmentAndDepositPaymentsAndBasket() {
    requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

    var dataToPost = {
      __RequestVerificationToken: requestVerificationToken,
      commitData: getCommitCardData()
    };

    $("body").html(bgl.common.spinner.getSpinnerBody("Processing your payment!",
      "Please do not hit the back button or refresh the page"));

    var urlPrefix = componentConfig.ApplicationPath !== null ? componentConfig.ApplicationPath : "/";

    $.ajax({
      url: urlPrefix + "TakePaymentComponent/CommitMultiplePaymentBasket",
      type: "POST",
      cache: false,
      dataType: "json",
      data: dataToPost,
      success: function (result) {
        if (result.isSuccess === false) {
          window.location.href = result.errorRedirectUrl;
          return;
        }
        if (result.PaymentSuccessful === true) {
          if (typeof bgl.common.pubsub !== 'undefined') {
            bgl.common.pubsub.emit('fakeFormSubmit', 'PaymentSuccessful');
          }
          dataLayerPushPaymentSuccessful();
          if (appInsights !== undefined) {
            appInsights.trackEvent("PaymentSuccessful", { "PolicyId": result.PolicyId });
            appInsights.flush();
          }
          isPaymentSuccessful = true;
        }
        sessionStorage.setItem("PaymentProcessedRedirectUrl", result.RedirectUrl);
        if (result.PolicyId) {
          var stringValue = result.PolicyId + "";
          var length = stringValue.indexOf("-");
          length = length > 0 ? length : stringValue.length;
          sessionStorage.setItem("PaymentProcessedPolicyId", stringValue.substring(0, length));
        }
      },
      complete: function () {
        redirectInterval = setInterval(checkAiComplete, 200);
      }
    });
  }

    function checkAiComplete() {
        if (sessionStorage.getItem("AI_sentBuffer") === "[]" || redirectTick >= maxRedirectWait) {
            clearInterval(redirectInterval);
            if (isPaymentSuccessful) {
                window.location.href = componentConfig.PaymentSuccessfulUrl;
            } else {
                window.location.href = sessionStorage.getItem("PaymentProcessedRedirectUrl");
            }
        }
        redirectTick += 1;
    }

    function getCommitCardData() {
        var model = {};

        model.TodaysPaymentCard = getDepositCardData();
        model.ContinuousPaymentCard = getInstalmentCardData();
        model.ContinuousPaymentDirectDebit = getInstalmentDirectDebitData();

        model.PaymentType = sessionStorage.getItem("MonthlyInstalmentPaymentType");

        model.UseContinuousPaymentCardForTodaysPayment = sessionStorage.getItem("UseMonthlyInstalmentCard");

        model.PaymentDeclinedUrl = componentConfig.PaymentDeclinedUrl;

        model.ComponentConfiguration = componentConfig;

        return model;
    }

    function getDepositCardData() {
        var model = {};
        model.SelectedPersonIdText = sessionStorage.getItem("MonthlyDepositSelectedPersonIdText");
        model.CardCaptureUri = sessionStorage.getItem("MonthlyDepositCardCaptureUri");
        model.SecureSessionId = sessionStorage.getItem("MonthlyDepositSecureSessionId");
        model.SelectedPersonIdVal = sessionStorage.getItem("MonthlyDepositSelectedPersonIdVal");
        return model;
    }

    function getInstalmentCardData() {
        var model = {};
        model.SelectedPersonIdText = sessionStorage.getItem("MonthlyInstalmentCardSelectedPersonIdText");
        model.CardCaptureUri = sessionStorage.getItem("MonthlyInstalmentCardCaptureUri");
        model.SecureSessionId = sessionStorage.getItem("MonthlyInstalmentSecureSessionId");
        model.SelectedPersonIdVal = sessionStorage.getItem("MonthlyInstalmentCardSelectedPersonIdVal");
        return model;
    }

    function getInstalmentDirectDebitData() {
        var model = {};
        model.SelectedPersonIdText = sessionStorage.getItem("MonthlyInstalmentDirectDebitSelectedPersonIdText");
        model.SelectedPersonIdVal = sessionStorage.getItem("MonthlyInstalmentDirectDebitSelectedPersonIdVal");
        model.AccountNumber = sessionStorage.getItem("MonthlyInstalmentDirectDebitAccountNumber");
        model.SortCode = buildSortCode();
        return model;
    }

    function buildSortCode() {
        if (sessionStorage.getItem("MonthlyInstalmentDirectDebitSortCodePart1") !== null &&
            sessionStorage.getItem("MonthlyInstalmentDirectDebitSortCodePart2") !== null &&
            sessionStorage.getItem("MonthlyInstalmentDirectDebitSortCodePart3") !== null) {
            return sessionStorage.getItem("MonthlyInstalmentDirectDebitSortCodePart1") +
                sessionStorage.getItem("MonthlyInstalmentDirectDebitSortCodePart2") +
                sessionStorage.getItem("MonthlyInstalmentDirectDebitSortCodePart3");
        }
        return null;
    }

    function dataLayerPushPaymentSuccessful() {
        if (typeof dataLayer !== "undefined") {
            dataLayer.push({
                'event': "TEVirtualPageview",
                'virtualPageURL': "Payment/Successful",
                'virtualPageTitle': "Payment Successful"
            });
        }
    }

    function setupImportantInformation() {
        $("#moreInfo-payments").off("click").on("click",
            function (event) {
                event.preventDefault();
                var link = $(this);
                bgl.common.loader.load({}, link.data("action-url"), link.data("container"));
            });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();