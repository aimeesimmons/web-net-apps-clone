﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.footer = (function () {
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {

        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            bgl.components.footer.init();
        });
    }

    function initComponent() {
        eventListener();
    }

    function eventListener() {
        bgl.common.pubsub.on('switchPaymentType', function (isMonthly) {
            switchPaymentType(isMonthly);
        });
    }

    function switchPaymentType(isMonthly) {
        var monthlyLink = $('#monthly-link');

        if (isMonthly) {
            monthlyLink.removeClass('hide');
        } else {
            monthlyLink.addClass('hide');
        }
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.footer;
}
