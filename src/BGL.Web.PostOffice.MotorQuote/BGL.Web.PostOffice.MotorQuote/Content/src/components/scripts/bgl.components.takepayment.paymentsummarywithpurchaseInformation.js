﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymentsummarywithpurchaseinformation = (function () {
    var configuration;
    var paymentType;
    var switchForm;

    function initComponent(componentConfiguration) {
        switchForm = $('#PaymentSummaryPaymentSwitchForm');
        configuration = componentConfiguration;
        var monthlyAnnualRadioButtons = $('input[type=radio].payment-option__radio');

        preventFormSubmission(switchForm);
        setPaymentTypeWhenPaymentTypeChanges(monthlyAnnualRadioButtons);
        postPaymentTypeWhenPaymentTypeChanges(monthlyAnnualRadioButtons);
        changeToAnnualWhenAnnualButtonClicked();
        changeToMonthlyWhenMonthlyButtonClicked();

        setupPaymentSummaryForm(configuration);
    }

    function setupPaymentSummaryForm(configuration) {
        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init($("#PaymentSummaryForm"), {
            onfocusout: function (element) {
                $(element).valid();
            },
            hasSubmit: false
        });
        bgl.components.takepayment.documentloader.init(configuration);
        $("#PaymentSummaryForm").submit(
            function (e) {
                e.preventDefault();

                if (!$(this).valid()) {
                    return;
                }

                $.ajax({
                    url: $(this).attr("action"),
                    type: $(this).attr("method"),
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.isSuccess) {
                            window.location = data.redirectUrl;
                        } else {
                            window.location = data.errorRedirectUrl;
                        }
                    }
                });
            });
    }

    function preventFormSubmission(form) {
        form.on("submit", function (event) {
            event.preventDefault();
        });
    }

    function changeToAnnualWhenAnnualButtonClicked() {
        $("#annual-price-label").on("click",
            function () {
                $('#monthly-price-label').removeClass('payment-switch__label--selected');
                $("#annual-price").click();
                $("#annual-price-label").addClass('payment-switch__label--selected');
            });
    }

    function changeToMonthlyWhenMonthlyButtonClicked() {
        $("#monthly-price-label").on("click",
            function () {
                $('#annual-price-label').removeClass('payment-switch__label--selected');
                $("#monthly-price").click();
                $("#monthly-price-label").addClass('payment-switch__label--selected');
            });
    }

    function setPaymentTypeWhenPaymentTypeChanges(radioButtons) {
        radioButtons.on('change', function () {
            setPaymentOption($(this));
        });
    }

    function setPaymentOption(selected) {
        paymentType = selected.data('paySchedule');
    }

    function postPaymentTypeWhenPaymentTypeChanges(radioButtons) {
        radioButtons.on('change', function () {
            bgl.common.validator.submitForm(switchForm, configuration);
        });
    }

    function onSuccess() {
        var isMonthly = paymentType === "monthly";
        bgl.common.pubsub.emit("switchPaymentType", isMonthly);
        switchCostBreakdownContent(isMonthly);
        switchPolicyFeaturesContent(isMonthly);
        showHoustonNotification();
    }

    function switchCostBreakdownContent(isMonthly) {
        if (isMonthly) {
            $(".cost-breakdown__annual").addClass("hide");
            $(".cost-breakdown__monthly").removeClass("hide");
            $("#IsMonthlyPayment").val("true");
            // bgl.common.pubsub.emit("showNotification", $("#monthly-notification-message").val());
        } else {
            $(".cost-breakdown__monthly").addClass("hide");
            $(".cost-breakdown__annual").removeClass("hide");
            $("#IsMonthlyPayment").val("false");
            // bgl.common.pubsub.emit("showNotification", $("#annual-notification-message").val());
        }
    }

    function switchPolicyFeaturesContent(isMonthly) {
        if (isMonthly) {
            $(".payment-policy-features-annual").addClass("hide");
            $(".payment-policy-features-monthly").removeClass("hide");
        } else {
            $(".payment-policy-features-monthly").addClass("hide");
            $(".payment-policy-features-annual").removeClass("hide");
        }
    }

    function showHoustonNotification() {
        var notification = $("#" + paymentType + "-paymentSummary-notification-message").val();

        bgl.common.pubsub.emit("showNotification", notification);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.takepayment.paymentsummarywithpurchaseinformation;
}