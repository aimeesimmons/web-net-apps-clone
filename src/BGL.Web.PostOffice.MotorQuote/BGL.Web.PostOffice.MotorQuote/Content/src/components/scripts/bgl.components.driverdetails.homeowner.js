﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.driverdetails.homeowner = (function () {
  var form,
    configuration,
    continueButton,
    homeOwnerYes,
    homeOwnerNo;

  function initComponent(componentConfiguration) {
    configuration = componentConfiguration;
    form = $('#home-owner');
    continueButton = form.find("[type='submit']");
    homeOwnerYes = form.find('#home-owner-yes');
    homeOwnerNo = form.find('#home-owner-no');

    bgl.common.validator.init(form);
    bgl.common.datastore.init(form, false, true);
    bgl.common.datastore.setupReadAndWrite();

    if (!configuration.MarketingQuestion) {
      subscribeForRadioButtonsClick();
    } else {
      subscribeForRadioButtonsClickWithHomeInsuranceDueOptions();
    }

    form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
  }

  function generateClickOnKeypress(event) {
    var code = event.charCode || event.keyCode;

    if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
      $(event.target).prop("checked", "checked");
      $(event.target).click();
    }
  }

  function continueHandler(event) {
    if (typeof bgl.common.pubsub !== 'undefined') {
      bgl.common.pubsub.emit('fakeFormSubmit', form.attr('id') || form.attr('action') || 'unknown');
    }
    event.preventDefault();

    if ($(event.target).is(':checked')) {
      $(event.target).trigger('change');
    }

    bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
  }

  function subscribeForRadioButtonsClick() {
    if (form.find("input[type=radio]:checked").length === 0) {
      form.find("input[type=radio]")
        .off('keypress', generateClickOnKeypress)
        .on('keypress', generateClickOnKeypress)
        .off('click', continueHandler)
        .on('click', continueHandler);
    } else {
      var continueButton = $(form.find("[type='submit']"));
      continueButton.removeClass("hide");
      continueButton.off("click").on("click", continueHandler);
    }
  }

  function showDropDown() {
    form.find('#home-insurance-due-dropdowns').removeClass("hide");
    showContinue();
  }

  function showContinue() {
    continueButton.removeClass('hide');
    continueButton.off("click").on("click", continueHandlerWithHomeInsuranceDueRemoval);
    homeOwnerNo.off('click', continueHandlerWithHomeInsuranceDueRemoval).on('click', hideDropdown);
  }

  function hideDropdown() {
    form.find('#home-insurance-due-dropdowns').addClass("hide");
  }

  function continueHandlerWithHomeInsuranceDueRemoval(event) {
    event.preventDefault();

    if ($(event.target).is(":checked")) {
        $(event.target).trigger("change");
    }

    continueButton.attr('disabled', 'disabled');
 
    if (homeOwnerNo.prop('checked')) {
      $("#Home-Insurance-Due-Month").val("");
    }

    if ($("#SelectedResponse").val() !== $("#Home-Insurance-Due-Month").val()) {
      bgl.common.validator.submitForm(form, configuration);
    } else {
      continueButton.removeAttr('disabled');
      bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }
  }

  function subscribeForRadioButtonsClickWithHomeInsuranceDueOptions() {
    homeOwnerYes.off('click').on('click', showDropDown);

    if (form.find("input[type=radio]:checked").length === 0) {
      form.find("input[type=radio]")
        .off('keypress', generateClickOnKeypress)
        .on('keypress', generateClickOnKeypress);

      homeOwnerNo.off('click').on('click', continueHandlerWithHomeInsuranceDueRemoval);
    } else if (homeOwnerNo.prop('checked')) {
      showContinue();
    } else {
      showDropDown();
    }
  }

  function onSuccess() {
    if (homeOwnerNo.prop('checked')) {
      bgl.common.datastore.deleteFields(form, ['HomeInsuranceDueQuestion.SelectedResponseText', 'HomeInsuranceDueQuestion.SelectedResponseVal', 'HomeInsuranceDueQuestion.SelectedResponse']);
    }
    bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
  }

  function onError() {
    bgl.components.driverdetails.homeowner.init(configuration);
  }

  return {
    init: initComponent,
    onSuccess: onSuccess,
    onError: onError
  };
})();
