﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.additionaldetails = bgl.components.additionaldetails || {};

bgl.components.additionaldetails.andfinally = (function () {
    var form;
    var configuration;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#and-finally-form');

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeForEvents();
    }

    function subscribeForEvents() {
        form.off('submit')
            .on('submit',
                function(event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid(form)) {
                        var data = bgl.common.datastore.getData(form) || {};

                        data.__RequestVerificationToken = $('input[name="__RequestVerificationToken"]').val();
                        data.ComponentConfiguration = configuration;

                        bgl.common.validator.submitForm(form, configuration, data);
                    }
                });
    }

    function onSuccess() {
        var options = {
            callback: function () {
                window.location.href = configuration.NextPageUrl;
            },
            hideNotification: true
        };

        sessionStorage.clear();
        bgl.common.pubsub.emit('PollBasket', options);
    }

    function onError() {
        bgl.components.additionaldetails.andfinally.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();