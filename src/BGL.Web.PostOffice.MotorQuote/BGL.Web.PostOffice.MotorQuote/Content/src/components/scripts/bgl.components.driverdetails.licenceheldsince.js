﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.licenceheldsince = (function () {
    var form;
    var configuration;
    var monthDropdown;
    var yearDropdown;
    var fullYearMonthOptions;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#licence-held-since');

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeOnEvents();

        updateMonthDropdown();
    }

    function subscribeOnEvents() {
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        var currentData = bgl.common.datastore.getData($(this));
                        var dataForValidate = $(form).serialize() + '&' + $.param(currentData) + '&' + $.param({ ComponentConfiguration: configuration });

                        bgl.common.validator.submitForm($(this), configuration, dataForValidate);
                    }
                });

        monthDropdown = form.find('#Since-Month');
        yearDropdown = form.find('#Since-Year');

        fullYearMonthOptions = monthDropdown.find('option');

        yearDropdown.on("change", updateMonthDropdown);
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();

        var currentYear = $('#CurrentYear').val() * 1;
        var currentMonth = $('#CurrentMonth').val() * 1;

        var selectedYear = yearDropdown.val() * 1;

        var personBirthYear = $("#PersonBirthYear").val() * 1;
        var personBirthMonth = $('#PersonBirthMonth').val() * 1;

        var adulthood = personBirthYear + 17;

        if (selectedYear === currentYear) {
            months.html(fullYearMonthOptions.slice(0, currentMonth + 1));
        } else if (selectedYear === adulthood) {
            // merge months after birth month and please select...
            var dropdownOptions = fullYearMonthOptions.slice(personBirthMonth);
            months.html($.merge([fullYearMonthOptions[0]], dropdownOptions));
        } else {
            months.html(fullYearMonthOptions);
        }
        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function toDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    function onSuccess(configuration, data) {

        if (data.needUpdatePerson === true) {

            form.attr("action", data.patchUrl);

            $("#HasChanges").val(bgl.common.datastore.isDataChanged(form) === true);

            var currentData = bgl.common.datastore.getData(form);

            var dataForValidate = $(form).serialize()
                + '&' + $.param(currentData)
                + '&' + $.param({ ComponentConfiguration: configuration, ViewName: "LicenceHeldSince" });

            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.common.validator.submitForm(form, configuration, dataForValidate);
            return;
        }

        location.href = data.nextDtsPageUrl;
    }

    function onError() {
        bgl.components.driverdetails.licenceheldsince.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
