﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.aboutyou = (function () {
    var form,
        configuration,
        registerKeeper,
        container;

    function initComponent(componentConfiguration, containerId) {
        initElements(componentConfiguration, containerId);

        registerKeeper.redirectIfSessionStorageIsNotFound(form, componentConfiguration.ParentJourneyUrl);

        subscribeForEvents();

        updateHeading();

        registerKeeper.displaySubmitButton(form);
    }

    function loadComponent(config, url, containerId) {
        bgl.common.loader.load(config,
            url,
            containerId,
            function () {
                initComponent(config, containerId);
            });
    }

    function onSuccess(configuration, data) {
        bgl.components.coverdetails.maritalstatus.load(configuration, data.nextUrl, container);
    }

    function onMultiStepEditSuccess() {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onError() {
        initComponent(configuration, container);
    }

    function initElements(componentConfiguration, containerId) {
        configuration = componentConfiguration;
        form = $("#about-you-form");
        container = containerId;

        registerKeeper = bgl.components.coverdetails.registeredkeeper;
    }

    function subscribeForEvents() {
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);
        bgl.common.validator.enableMaxLength();
        bgl.common.date.initDateFormat($('#DateOfBirth'));

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
                });

        form.find('.back-button-link').off('click').one('click',
            function (event) {
                event.preventDefault();

                var url = $(this).attr('href');

                bgl.components.coverdetails.relationshipstatus.load(configuration, url, container);
            });
    }

    function updateHeading() {
        var data = bgl.common.datastore.getData(form);

        if (data) {
            var heading = getHeading(data.RelationshipStatusCode);
            var header = form.find('#heading');

            header.html(header.html().replace("{FirstName}", heading));
        }
    }

    function getHeading(code) {
        switch (code) {
            case "J":
            case "W":
                return "partner";
            case "S":
                return "spouse";
        }

        return "registered keeper";
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onMultiStepEditSuccess: onMultiStepEditSuccess,
        onError: onError
    };
})();