﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

var selectedModifications = 'SelectedModifications';

bgl.components.journeyTypesVehicleDetails = {
    SingleEdit: "0",
    LookupEdit: "1",
    MultiStepEdit: "2"
}

bgl.components.vehicledetails.purchasedate = (function () {
    var purchaseDateDropdowns,
        yesCheckButton,
        year,
        month,
        configuration,
        containerId,
        HIDE_CLASS = "hide",
        form;
    var hasPreselectedValues;
    var monthDropdown;
    var yearDropdown;
    var fullYearMonthOptions;
    var currentYearMonthOptions;

    function loadComponent(model, componentUrl, containerElementId) {
        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId, function () {
                bgl.components.vehicledetails.purchasedate.init(configuration);
            });
    }

    function init(componentConfiguration) {
        configuration = componentConfiguration;

        initializeVariables(componentConfiguration);
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        var isSingleJourneyType = form.find('#JourneyType').val() === bgl.components.journeyTypesVehicleDetails.SingleEdit;

        if (isSingleJourneyType) {
            hasPreselectedValues = true;
            bgl.common.datastore.init(form);
        } else {
            bgl.common.datastore.init(form, false, true);
            bgl.common.datastore.setupReadAndWrite();

            var hasValue = $("#HasValue").val() === "True";

            hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0 || hasValue;
        }

        subscribeForEvents();
    }

    function subscribeForEvents() {

        if (hasPreselectedValues === true) {
            showSubmitButton();

            if (form.find("input[type=radio]:checked").length !== 0) {
                var selectedButton = $(form.find("input[type=radio]:checked")[0]);
                switch (selectedButton.val().toLowerCase()) {
                    case "true":
                        if (purchaseDateDropdowns.hasClass(HIDE_CLASS)) {
                            purchaseDateDropdowns.removeClass(HIDE_CLASS);
                        }
                        break;
                }
            }           
        }

        $("#vehicle-purchased-yes").on("click",
            function () {
                $(this).prop("checked", true);
                $(this).trigger("change");

                hasPreselectedValues = true;
                showSubmitButton();
                if (purchaseDateDropdowns.hasClass(HIDE_CLASS)) {
                    purchaseDateDropdowns.removeClass(HIDE_CLASS);
                }
            });

        $("#vehicle-purchased-no").on("click",
            function () {
                $(this).prop("checked", true);
                $(this).trigger("change");

                if (hasPreselectedValues === false) {
                    form.submit();
                    return;
                }

                if (!purchaseDateDropdowns.hasClass(HIDE_CLASS)) {
                    purchaseDateDropdowns.addClass(HIDE_CLASS);
                }
            });

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {
                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));
                    storeVehicleChangesForTagManager();
                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        function storeVehicleChangesForTagManager() {
            var data = bgl.components.vehicledetails.common.getPreviouslyStoredChangedVehicleData();
            var vehicleDetails = getCurrentVehicleChanges();
            var changesInSessionStorage = sessionStorage.getItem("VehicleLookupJourney");
            var currentVehicleChanges = JSON.parse(changesInSessionStorage);

            if (vehicleDetails !== null) {
                data.push(["Manufacturer", currentVehicleChanges.Manufacturer]);
                data.push(["Model", currentVehicleChanges.Model]);
                data.push(["Age", getVehicleAge(currentVehicleChanges.Registration)]);
                bgl.components.vehicledetails.common.setPreviouslyStoredChangedVehicleData(data);
            }
        }

        function getCurrentVehicleChanges() {

            var changesInSessionStorage = sessionStorage.getItem("VehicleLookupJourney");

            if (changesInSessionStorage !== null) {
                var currentVehicleChanges = JSON.parse(changesInSessionStorage);

                return {
                    Manufacturer: currentVehicleChanges.Manufacturer,
                    Model: currentVehicleChanges.Model,
                    Age: getVehicleAge(currentVehicleChanges.Registration)
                };
            }
            else {
                return null;
            }
        }

        function getVehicleAge(registration) {
            if (registration) {
                var year = registration.substring(0, 4);
                var age = new Date().getFullYear() - parseInt(year);
                return age;
            }
            else {
                return 0;
            }
        }

        if (form.find('#JourneyType').val() === bgl.components.journeyTypesVehicleDetails.LookupEdit) {
            $("a[data-step-type='previous-step']").off("click").one("click",
                function(event) {
                    event.preventDefault();

                    $(this).hide();

                    var backUrl = $(this).attr("href");

                    var vehicleObject = bgl.common.datastore.getData(form);

                    var model = {
                        ComponentConfiguration: configuration,
                        Modifications: vehicleObject[selectedModifications] || [],
                        Model: vehicleObject.Model,
                        Manufacturer: vehicleObject.Manufacturer,
                        VehicleId: vehicleObject.VehicleId,
                        JourneyType: form.find('#JourneyType').val()
                    };

                    if (configuration.IsService) {
                        bgl.components.vehicledetails.importantinformation.load(model, backUrl, containerId);
                    } else {
                        bgl.components.vehicledetails.modifications.load(model, backUrl, containerId);
                    }                    
                });
        }

        var currentMonth = $('#CurrentMonth').val() - 0;

        monthDropdown = form.find('#Purchased-Month');
        yearDropdown = form.find('#Purchased-Year');

        fullYearMonthOptions = monthDropdown.find('option');
        currentYearMonthOptions = fullYearMonthOptions.slice(0, currentMonth + 1);

        updateMonthDropdown();

        yearDropdown.on("change", updateMonthDropdown);
    }

    function showSubmitButton() {
        var submitButton = form.find("button[type='submit']");
        submitButton.removeClass(HIDE_CLASS);
    }

    function initializeVariables(componentConfiguration) {
        purchaseDateDropdowns = $(".purchase-date-dropdowns");
        configuration = componentConfiguration;
        month = getMonth();
        year = getYear();
        yesCheckButton = getYesCheckButtonState();
    }

    function onSuccess(configuration, data) {
        if (data.content && data.content.JourneyType && data.content.JourneyType.toString() !== bgl.components.journeyTypesVehicleDetails.SingleEdit) {

            if (data.content.IsVehiclePurchased === false) {
                bgl.common.datastore.deleteFields(data.content.DataStoreKey,
                    ["Month", "MonthVal", "MonthText", "Year", "YearVal", "YearText"]);
            }

            if (data.content.HasValue === true) {
                bgl.components.vehicledetails.lookup.postVehicle(form, data.nextStepUrl);
            } else {
                if (data.content.JourneyType.toString() === bgl.components.journeyTypesVehicleDetails.LookupEdit) {
                    var model = bgl.common.datastore.getData(form);
                    model.ComponentConfiguration = configuration;

                    bgl.components.vehicledetails.value.load(data.content,
                        data.nextStepUrl,
                        containerId);
                } else {
                    bgl.common.utilities.redirectToUrl(data.nextStepUrl);
                }
            }
        } 
        else {
            if (data.hasChanges === true) {
                bgl.common.pubsub.emit("PollBasket");
            }
            bgl.common.datastore.removeSessionStorage(form);
        }
    }

    function onError(configuration) {
        bgl.components.vehicledetails.purchasedate.init(configuration);
    }

    function updateMonthDropdown() {
        var months = monthDropdown;
        var selectedMonthValue = months.val();
        var currentYear = $(yearDropdown).find("option:eq(1)").val();

        switch (yearDropdown.val()) {
        case currentYear:
            months.html(currentYearMonthOptions);
            break;
        default:
            months.html(fullYearMonthOptions);
            break;
        };

        months.val(selectedMonthValue);

        // select first value if value is not selected
        if (!months.val()) {
            months.find('option:eq(0)').prop('selected', true);
        }
    }

    function getMonth() {
        return $('#Month').val();
    }

    function getYear() {
        return $('#Year').val();
    }

    function getYesCheckButtonState() {
        return $('#Vehicle-Purchased-Yes').prop('checked');
    }

    return {
        load: loadComponent,
        init: init,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.vehicledetails;
}
