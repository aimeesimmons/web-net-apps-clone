﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

var selectedModifications = 'SelectedModifications';

bgl.components.journeyTypesVehicleDetails = {
  SingleEdit: "0",
  LookupEdit: "1",
  MultiStepEdit: "2"
};

bgl.components.vehicledetails.modifications = (function () {

  var containerId;
  var configuration;
  var modificationCheckButton,
    isChanged = false;
  var form;

  function loadComponent(model, componentUrl, containerElementId) {
    containerId = containerElementId;
    configuration = model.ComponentConfiguration;

    bgl.common.loader.load(model,
      componentUrl,
      containerElementId,
      initComponent);
  }

  function initComponent(componentConfiguration) {
    if (configuration == undefined) {
      configuration = componentConfiguration;
    }

    unSubscribeToEventDismissVehicleModificationsPanel();

    initializeVariables();
    subscribeToEvents();

    initializeLookupEdit();
  }

  function initializeVariables() {
    form = $("#vehicle-modifications-form");

    modificationCheckButton = getModificationButtonCheckedState();
  }

  function subscribeToEvents() {
    subscribeToEventShowModification();

    subscribeToEventAddModification();

    subscribeToEventRemoveAllModifications();

    subscribeToEventDeleteModification();

    if (form.find("#JourneyType").val() !== bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
      subscribeBackButton();
      subscribeToEventDismissVehicleModificationsPanel();
    }
  }

  function initializeLookupEdit() {
    if ($('#JourneyType').val() !== bgl.components.journeyTypesVehicleDetails.SingleEdit) {
      bgl.common.datastore.init(form, false, true);

      bgl.common.datastore.setupReadAndWrite();

      if (getModificationCount() !== 0) {
        $('#Modification-Yes').prop('checked', true);
      } else {
        var vehicleObject = bgl.common.datastore.getData(form);

        if (vehicleObject != null && vehicleObject['IsModified'] === 'No') {
          $('#Modification-No').prop('checked', true);
          $('#vehicle-modification_continue-button').removeClass('hide');
        }
      }
    }
  }

  function getModificationCount() {
    return $('#Vehicle-Modification-List .step-form__summary-item').length;
  }

  function subscribeToEventDismissVehicleModificationsPanel() {
    $(document).on('click.modifications', '[data-dismiss="slide-panel"]', function () {
      unSubscribeToEventDismissVehicleModificationsPanel();
      dismissSlidePanelHandler(isChanged);
    });

    $(document).on('keyup.modifications', function (event) {
      if (event.key === "Escape") {
        unSubscribeToEventDismissVehicleModificationsPanel();
        dismissSlidePanelHandler(isChanged);
      }
    });

    $('.overlay').on('click.modifications', function () {
      unSubscribeToEventDismissVehicleModificationsPanel();
      dismissSlidePanelHandler(isChanged);
    });
  }

  function unSubscribeToEventDismissVehicleModificationsPanel() {
    $(document).off('click.modifications', '[data-dismiss="slide-panel"]');
    $(document).off('keyup.modifications');
    $('.overlay').off('click.modifications');
  }

  function dismissSlidePanelHandler(flag) {
    if (modificationCheckButton !== getModificationButtonCheckedState() || flag) {

      var journeyType = $("input[name='JourneyType']").val();

      if (journeyType === bgl.components.journeyTypesVehicleDetails.SingleEdit) {
        bgl.common.utilities.startUnderwritingPolling();
      }

      if (journeyType === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
        bgl.common.utilities.refreshPage();
      }
    }
  }

  function subscribeBackButton() {
    $('.vehicle-modification-back-button')
      .off('click')
      .on('click',
        function (event) {

          event.preventDefault();

          $(this).hide();

          var link = $(event.currentTarget);
          var productType = link.data("product-type");

          var model = bgl.common.datastore.getData(form);
          var url = $(this).attr('href');

          model.ComponentConfiguration = configuration;

          if (productType === "van") {
            bgl.components.vehicledetails.numberofseats.load(model,
              url,
              containerId);
          } else {
            bgl.components.vehicledetails.summary.load(model, url, containerId);
          }
        });
  }

  function subscribeToEventShowModification() {
    var radioButtons = $("input[type='radio']");

    $.each(radioButtons,
      function (key, item) {
        var button = $(item);

        button.on("change",
          function (event) {
            event.preventDefault();

            var listContainer = $("#Vehicle-Modification-List");
            switch ($(this).val()) {
              case "Yes":
                var items = $(".step-form__summary-item");
                if (items.length === 0) {
                  loadAddModification($(event.target).data("url"));
                } else {
                  listContainer.show();
                }
                break;
              case "No":
                listContainer.hide();

                var continueButton = $("#vehicle-modification_continue-button");
                if (continueButton.hasClass('hide')) {
                  continueButton.click();
                }
                break;
            }
          });
      });

    form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
  }

  function subscribeToEventAddModification() {

    var addModificationButton = $('#add-vehicle-modifications');

    addModificationButton.off('click').on('click',
      function (event) {

        event.preventDefault();

        var object = $(event.target);

        loadAddModification(object.data('url'));
      });
  }

  function subscribeToEventRemoveAllModifications() {

    var removeAllButton = $("#remove-all-vehicle-modifications");
    var noButton = $("#Modification-No");

    removeAllButton.off('click');
    removeAllButton.on('click',
      function (event) {

        event.preventDefault();

        noButton.click();
      });


    var doneButton = $('button[data-remove-all-url]');

    var clickHandler;

    var journeyType = $("input[name='JourneyType']").val();

    if (journeyType === bgl.components.journeyTypesVehicleDetails.SingleEdit) {
      clickHandler = function (event) {
        event.preventDefault();

        unSubscribeToEventDismissVehicleModificationsPanel();

        var isModified = $(this).data("ismodified").toString().toLowerCase() === "true";
        var isNoButtonChecked = noButton.is(':checked');

        if (isModified && isNoButtonChecked) {

          var url = $(this).data("remove-all-url");
          var data = {
            __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
            vehicleId: $('#VehicleId').val()
          };

          $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (data) {
              if (!data.isSuccess && data.errorRedirectUrl !== null) {
                bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
              } else if (data.isSuccess) {
                bgl.common.utilities.startUnderwritingPolling();
              }
            }
          });

        } else if (modificationCheckButton !== getModificationButtonCheckedState() || isChanged) {
          bgl.common.utilities.startUnderwritingPolling();
        }

        isChanged = false;
      };
    } else {
      clickHandler = function (event) {
        event.preventDefault();

        var vehicleObject = bgl.common.datastore.getData(form);

        if ($('#Modification-No').is(':checked')) {
          // remove modifications from session storage
          var key = form.data("store-key");
          delete vehicleObject[selectedModifications];
          sessionStorage.setItem(key, JSON.stringify(vehicleObject));
        }

        if (journeyType === bgl.components.journeyTypesVehicleDetails.LookupEdit) {

          var url = $(this).data("next-step-url");
          var vehicleId = $("#VehicleId").val();
          var registration = vehicleObject.Registration;
          var hasValue = vehicleObject.HasValue;

          var model = {
            ComponentConfiguration: configuration,
            HasValue: hasValue,
            VehicleId: vehicleId,
            Registration: registration,
            JourneyType: journeyType
          };

          bgl.components.vehicledetails.purchasedate.load(model, url, containerId);
        } else {
          if (bgl.common.pubsub !== 'undefined') {
            bgl.common.pubsub.emit('fakeFormSubmit', form.attr('id') || form.attr('action') || 'unknown');
          }
          bgl.common.utilities.redirectToUrl(configuration.ComponentUrl + "#purchasedate");
        }
      };
    }

    doneButton.one("click", clickHandler);
  }

  function subscribeToEventDeleteModification() {
    var deleteModificationButtons = $("[data-role='delete']");

    $.each(deleteModificationButtons,
      function (key, item) {
        $(item).off("click").on("click",
          function (event) {
            event.preventDefault();

            var target = $(event.target);

            var object = target.is("a, button") ? target : target.parent();

            var journeyType = $("input[name='JourneyType']").val();
            var modifications = {};

            if (journeyType !== bgl.components.journeyTypesVehicleDetails.SingleEdit) {
              var key = $('#vehicle-modifications-form').data('store-key');
              var vehicleObject = JSON.parse(sessionStorage.getItem(key)) || {};
              modifications = vehicleObject[selectedModifications] || [];

              var modificationCode = object.data('modification-code');
              var modificationTypeCode = object.data('modification-type-code');

              modifications = modifications.filter(function (modification) {
                return modification.SelectedModificationCode !== modificationCode &&
                  modification.SelectedModificationTypeCode !== modificationTypeCode;
              });

              if (modifications.length === 0) {
                vehicleObject["IsModified"] = "No";
              }

              vehicleObject[selectedModifications] = modifications;
              sessionStorage.setItem(key, JSON.stringify(vehicleObject));

              isChanged = true;

              if (journeyType === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
                bgl.common.utilities.refreshPage();
                return;
              }

              bgl.components.vehicledetails.modifications.load(generateInputModel(configuration),
                $('#VehicleModificationsUrl').val(),
                containerId);

            } else {
              var token = $("input[name='__RequestVerificationToken']").val();
              var patchUrl = object.data('url');
              var vehicleId = $("#VehicleId").val();

              var dataToPost = {
                __RequestVerificationToken: token,
                vehicleId: vehicleId,
                modificationId: object.data("value"),
                journeyType: journeyType,
                modifications: modifications
              };

              var model = generateInputModel(configuration);

              $.ajax({
                url: patchUrl,
                type: "POST",
                data: dataToPost,
                success: function (data, textStatus, request) {
                  if (request.getResponseHeader('Content-Type') ===
                    'application/json; charset=utf-8') {
                    if (!data.isSuccess) {
                      bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                    } else {
                      isChanged = true;
                      bgl.components.vehicledetails.modifications.load(model,
                        data.redirectUrl,
                        containerId);
                    }
                  }
                }
              });
            }
          });
      });
  }

  function generateInputModel(configuration) {
    var model = {
      ComponentConfiguration: configuration
    };

    model.VehicleId = $("#VehicleId").val();
    model.JourneyType = $("#JourneyType").val();

    if (model.JourneyType !== bgl.components.journeyTypesVehicleDetails.SingleEdit) {
      var vehicleObject = bgl.common.datastore.getData(form);
      model.Modifications = vehicleObject[selectedModifications] || [];
      model.Model = vehicleObject.Model;
      model.Manufacturer = vehicleObject.Manufacturer;
    } else {
      model.EditActionKey = bgl.components.vehicledetails.list.vehicleDetailsKeys.vehicleModifications;
    }

    return model;
  }

  function loadAddModification(url) {
    isChanged = true;

    $("#add-vehicle-modifications-div").click();

    var currentContainerId = containerId;

    var journeyType = $("#JourneyType").val();

    if (journeyType.toString() === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
      currentContainerId = "SlidePanelContent";
      subscribeToEventDismissVehicleModificationsPanel();
    }

    bgl.components.vehicledetails.addmodification.load({
      ComponentConfiguration: configuration,
      VehicleId: $("#VehicleId").val(),
      JourneyType: journeyType
    },
      url,
      currentContainerId);
  }

  function getModificationButtonCheckedState() {
    return $('#Modification-Yes').prop('checked');
  }

  return {
    load: loadComponent,
    init: initComponent
  };
})();

bgl.components.vehicledetails.addmodification = (function () {
  var componentModel,
    componentId,
    configuration,
    form;

  function loadComponent(model, componentUrl, containerElementId) {

    componentModel = model;
    componentId = containerElementId;
    configuration = model.ComponentConfiguration;

    bgl.common.loader.load(model,
      componentUrl,
      containerElementId,
      function () {
        bgl.components.vehicledetails.addmodification.init(configuration);
      });
  }

  function initComponent(componentConfiguration) {

    form = $("#" + componentConfiguration.Id + "_form-add-modification");
    bgl.common.validator.init(form);
    subscribeToEvents();
  }

  function subscribeToEvents() {
    subscribeModificationTypeButtons();
    subscribeBreadcrumbButton();
    subscribeSaveButton();
    subscribeBackButton();
  }

  function subscribeModificationTypeButtons() {
    var modificationButtons = $('[data-button-name="modificationTypeCode"]');
    $.each(modificationButtons,
      function (key, button) {
        $(button).off('click');
        $(button).on('click',
          function (event) {
            event.preventDefault();
            if (typeof bgl.common.pubsub !== 'undefined') {
              bgl.common.pubsub.emit('fakeFormSubmit', this.parentElement.parentElement.getElementsByTagName('h2')[0].innerText);
            }
            
            var object = $(event.target)[0];
            componentModel.SelectedModificationTypeCode = $(object).data('value');
            componentModel.HasValue = $("input[name='HasValue']").val();
            componentModel.JourneyType = $("input[name='JourneyType']").val();

            bgl.components.vehicledetails.addmodification.load(componentModel, $(object).data('url'), componentId);
          });
      });
  }

  function subscribeBreadcrumbButton() {
    var breadcrumbButtons = $('#back-to-modification-type');

    if (breadcrumbButtons !== undefined && breadcrumbButtons !== null) {

      breadcrumbButtons.off('click');
      breadcrumbButtons.on('click',
        function (event) {

          event.preventDefault();

          var object = $(event.target)[0];

          loadModificationTypeView($(object).data('url'));
        });
    }
  }

  function subscribeSaveButton() {
    var saveButton = $('#save-vehicle-modification');

    if (saveButton !== undefined && saveButton !== null) {
      saveButton.off('click');
      saveButton.on('click',
        function (event) {

          event.preventDefault();
          saveButton.attr('disabled', 'disabled');

          if (!bgl.common.validator.isFormValid(form)) {

            saveButton.removeAttr('disabled');

          } else {
            var checkedButton = $("#" + componentId).find("input[type='radio']:checked");

            if (checkedButton.length === 1) {

              componentModel.SelectedModificationTypeDescription = $("#back-to-modification-type").text();

              componentModel.SelectedModificationCode = checkedButton.val();

              var journeyType = $('#JourneyType').val();

              if (journeyType !== bgl.components.journeyTypesVehicleDetails.SingleEdit) {
                var key = $('#DataStoreKey').val();
                var vehicleObject = bgl.common.datastore.getData(key) || {};
                var modifications = vehicleObject[selectedModifications] || [];
                var hasDuplicate = false;

                modifications.forEach(function (mod) {
                  if (mod.SelectedModificationCode === componentModel.SelectedModificationCode &&
                    mod.SelectedModificationTypeCode ===
                    componentModel.SelectedModificationTypeCode) {
                    hasDuplicate = true;
                    return;
                  }
                });

                if (hasDuplicate) {
                  $('#modification-list, #save-vehicle-modification').addClass('hide');
                  $('#modification-error').removeClass('hide');

                  return;
                }

                modifications.push({
                  SelectedModificationCode: componentModel.SelectedModificationCode,
                  SelectedModificationCodeDescription: checkedButton.data('description').trim(),
                  SelectedModificationTypeCode: componentModel.SelectedModificationTypeCode,
                  SelectedModificationTypeDescription: componentModel
                    .SelectedModificationTypeDescription.trim()
                });
                vehicleObject[selectedModifications] = modifications;
                sessionStorage.setItem(key, JSON.stringify(vehicleObject));

                if (journeyType === bgl.components.journeyTypesVehicleDetails.LookupEdit) {
                  loadModificationView($('#VehicleModificationsUrl').val());
                } else {
                  $("[data-dismiss='slide-panel']").click();
                  bgl.common.utilities.refreshPage();
                }
              } else {

                var token = $("input[name='__RequestVerificationToken']").val();

                var dataToPost = {
                  __RequestVerificationToken: token,
                  model: componentModel
                };

                bgl.common.validator.submitForm(form, configuration, dataToPost);

              }
            }
          }
        });
    }
  }

  function subscribeBackButton() {
    var backButton = $("[data-step-type='previous-step']");

    if (backButton.length !== 0) {
      backButton.off("click").on("click",
        function (event) {

          $(this).hide();

          var url = $(event.target).data('url');

          switch ($(event.target).data("back-option")) {
            case "modificationType":
              loadModificationTypeView(url);
              break;
            case "modification":
              loadModificationView(url);
              break;
          }
        });
    }
  }

  function loadModificationTypeView(url) {
    componentModel.SelectedModificationTypeCode = null;
    componentModel.SelectedModificationCode = null;

    bgl.components.vehicledetails.addmodification.load(componentModel, url, componentId);
  }

  function loadModificationView(url) {

    var model = {
      ComponentConfiguration: componentModel.ComponentConfiguration
    };

    model.VehicleId = $("#VehicleId").val();
    model.JourneyType = $("input[name='JourneyType']").val();
    model.EditActionKey = bgl.components.vehicledetails.list.vehicleDetailsKeys.vehicleModifications;

    if (model.JourneyType === bgl.components.journeyTypesVehicleDetails.LookupEdit) {
      var key = $('#DataStoreKey').val();
      var vehicleObject = JSON.parse(sessionStorage.getItem(key)) || {};
      model.Modifications = vehicleObject[selectedModifications] || [];
      model.Model = vehicleObject.Model;
      model.Manufacturer = vehicleObject.Manufacturer;
    }

    if (model.JourneyType !== bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {
      bgl.components.vehicledetails.modifications.load(model,
        url,
        componentId);
    } else {
      $("[data-dismiss='slide-panel']").click();
      bgl.common.utilities.refreshPage();
    }
  }

  function onSuccess(configuration, data) {
    loadModificationView(data.redirectUrl);
  }

  function onError() {
    bgl.components.vehicledetails.addmodification.init(configuration);
  }

  return {
    load: loadComponent,
    init: initComponent,
    onSuccess: onSuccess,
    onError: onError
  };
})();

if (typeof module !== 'undefined') {
  module.exports = bgl.components.vehicledetails;
}