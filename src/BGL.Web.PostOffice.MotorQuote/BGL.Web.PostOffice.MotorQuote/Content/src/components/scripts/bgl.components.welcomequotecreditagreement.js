﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequotecreditagreement = (function () {
    
    function initComponent() {
        bgl.common.pubsub.on('switchPaymentType', toggleCreditAgreementText);
    }

    function toggleCreditAgreementText(isMonthly) {
        if (isMonthly) {
            $('[data-subtext="credit-agreement-text"], [data-subtext="isf-wording-monthly"]').removeClass('hide');
            $('[data-subtext="isf-wording-annual"]').addClass('hide');
            $(".payment-annual-information__promote-monthly").addClass("hide");
        } else {
            $('[data-subtext="credit-agreement-text"], [data-subtext="isf-wording-monthly"]').addClass('hide');
            $('[data-subtext="isf-wording-annual"]').removeClass('hide');
            $(".payment-annual-information__promote-monthly").removeClass("hide");
        }
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequotecreditagreement;
}