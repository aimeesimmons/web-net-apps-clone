﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var slidePanelContainer = 'SlidePanelContent';

bgl.components.driverdetails.driverbubbles = (function () {

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            bgl.components.driverdetails.common.init(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {

        subscribeAddDriverButton(componentConfiguration);
        subscribeMainDriver(componentConfiguration);
        subscribeNamedDrivers(componentConfiguration);

        var tabIndex = localStorage.getItem("driverDetailsTabIndex");

        if (tabIndex !== null) {
            $('#tab-' + tabIndex).click();
            localStorage.removeItem("driverDetailsTabIndex");
        }

        bgl.components.driverdetails.scrolltoelement.init();
    }

    function subscribeAddDriverButton(componentConfiguration) {
        $('#AddDriver')
            .off('click')
            .on('click', function (event) {
                event.preventDefault();

                var requestBodyData = {
                    ComponentConfiguration: componentConfiguration,
                    JourneyType: 1
                };

                var actionUrl = $(this).data("action-url");

                bgl.common.loader.load(requestBodyData,
                    actionUrl,
                    slidePanelContainer,
                    function () {
                        bgl.components.additionaldriver.init(componentConfiguration);
                    });
            });
    }

   
    function subscribeMainDriver(componentConfiguration) {
        subscribeDriver(componentConfiguration, 'ViewMainDriver', 0);
    }

    function subscribeNamedDrivers(componentConfiguration) {
        subscribeDriver(componentConfiguration, 'ViewNamedDrivers', 1);
    }

    function subscribeDriver(componentConfiguration, tabName, tabIndex) {
        $('#' + tabName)
            .off('keypress', generateClickOnKeypress)
            .on('keypress', generateClickOnKeypress)
            .off('click')
            .on('click',
                function (event) {
                    event.preventDefault();

                    if (componentConfiguration && componentConfiguration.RedirectUrl && componentConfiguration.RedirectUrl.length > 0) {
                        localStorage.setItem("driverDetailsTabIndex", tabIndex);
                        window.location = componentConfiguration.RedirectUrl;
                    } else {

                        if (document.getElementById("DriverDetails")) {
                            openAccordion($("#DriverDetails"));
                        }

                        $('#tab-' + tabIndex).click();
                        $('#tab-' + tabIndex).focus();
                    }
                });
    }
    
    function openAccordion(element) {

        var driverHeaderAccordionButton = $(element).find('.accordion__heading-button');
        var isExpanded = $(driverHeaderAccordionButton).attr("aria-expanded");

        if (isExpanded.toLowerCase() === "false") {
            $(driverHeaderAccordionButton).click();
        }

        bgl.components.driverdetails.scrolltoelement.scrollTo(element, 400);

    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;
        if (code === 13 || code === 32) {
            $(this).click();
        }
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.driverdetails.common;
}
