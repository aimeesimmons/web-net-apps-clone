﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.documentloader = (function () {
    var configuration;
    function initComponent(config) {
        configuration = config;
        $(document).ready(function() {
            getDocumentUrl();
        });
    }

    function getDocumentUrl() {
        $.get(configuration.ApplicationPath + "TakePaymentComponent/DocumentLoader",
            { name: "credit-information"},
            function(result) {
                if (result.url) {
                    window.sessionStorage.setItem("credit-information", result.url);
                }
            });
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.documentloader;
}
