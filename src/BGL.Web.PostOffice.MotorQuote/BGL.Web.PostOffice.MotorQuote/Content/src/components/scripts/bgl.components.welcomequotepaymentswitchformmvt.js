﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequotepaymentswitchformmvt = (function () {
    var configuration;
    var paymentType;
    var form;

    function initComponent(componentConfiguration) {
        form = $('#PaymentSummaryMVTForm');
        configuration = componentConfiguration;
        var monthlyAnnualRadioButtons = $('input[type=radio].payment-option__radio');

        preventFormSubmission(form);
        setPaymentTypeWhenPaymentTypeChanges(monthlyAnnualRadioButtons);
        postPaymentTypeWhenPaymentTypeChanges(monthlyAnnualRadioButtons);
        changeToAnnualWhenAnnualButtonClicked();
        changeToMonthlyWhenMonthlyButtonClicked();
    }

    function preventFormSubmission(form) {
        form.on("submit", function (event) {
            event.preventDefault();
        });
    }

    function changeToAnnualWhenAnnualButtonClicked() {
        $("#annual-price-label").on("click",
            function () {
                $('#monthly-price-label').removeClass('payment-switch__label--selected');
                $("#annual-price").click();
                $("#annual-price-label").addClass('payment-switch__label--selected');
            });
    }

    function changeToMonthlyWhenMonthlyButtonClicked() {
        $("#monthly-price-label").on("click",
            function () {
                $('#annual-price-label').removeClass('payment-switch__label--selected');
                $("#monthly-price").click();
                $("#monthly-price-label").addClass('payment-switch__label--selected');
            });
    }

    function setPaymentTypeWhenPaymentTypeChanges(radioButtons) {
        radioButtons.on('change', function () {
            setPaymentOption($(this));
        });
    }

    function setPaymentOption(selected) {
        paymentType = selected.data('paySchedule');
    }

    function postPaymentTypeWhenPaymentTypeChanges(radioButtons) {
        radioButtons.on('change', function () {
            bgl.common.validator.submitForm(form, configuration);
        });
    }

    function onSuccess() {
        var isMonthly = paymentType === "monthly";
        bgl.common.pubsub.emit("switchPaymentType", isMonthly);
        showHoustonNotification();
    }

    function showHoustonNotification() {
        var notification = $("#" + paymentType + "-welcomeQuote-notification-message").val();

        bgl.common.pubsub.emit("showNotification", notification);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequotepaymentswitchform;
}