﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.addpaymentmethod = (function () {
  var componentConfiguration = {};

  function loadComponent(componentConfig, componentUrl, containerElementId) {
    componentConfig.CardSessionData = getTodaysPaymentSessionData();
    componentConfig.ContinuousPaymentCardSessionData = getContinuousPaymentCardSecureSessionData();

    bgl.common.loader.load(componentConfig,
      componentUrl,
      containerElementId,
      function () {
        initComponent(componentConfig);
      });
  }

  function initComponent(componentConfig) {
    componentConfiguration = componentConfig;
    sortCodeAutoTab();
    initDirectDebit();
    initCreditCard();
    onCreditCardTabClick();
    onDirectDebitTabClick();
    bgl.common.datastore.setupReadAndWrite();
    loadIframe();

    window.sessionStorage.setItem("ActiveTab", "BankAccount");

    bgl.common.pubsub.on('session:end', function () {
      var keysToRemove = ["AddPaymentDirectDebitAccountNumber",
        "AddPaymentDirectDebitSortCodePart1",
        "AddPaymentDirectDebitSortCodePart2",
        "AddPaymentDirectDebitSortCodePart3",
        "AddPaymentDirectDebitAuthorise",
        "OneOffPaymentSelectedPersonIdText",
        "OneOffPaymentCardCaptureUri",
        "OneOffPaymentSecureSessionId",
        "OneOffPaymentSelectedPersonIdVal",
        "OneOffPaymentSelectedPersonId"
      ];

      keysToRemove.forEach(function (key) {
        window.sessionStorage.removeItem(key);
      });
    });
  }

  function initCardCapture() {
    sessionStorage.removeItem("AddPaymentMethodCardCaptureUri");
    sessionStorage.removeItem("AddPaymentMethodSecureSessionId");
    sessionStorage.removeItem("AddPaymentMethodThreeDSecureUri");

    bgl.components.takepayment.cardcaptureiframe.clearIframe();

    componentConfiguration.CardSessionData = { "SelectedPersonId": $("#Credit-Card-Selected-Person-Id").val() };

    var dataToPost = {
      __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
      configuration: componentConfiguration
    };

    $.ajax({
      url: componentConfiguration.ApplicationPath + "TakePaymentComponent/InitialiseAddPaymentMethodCardCapture",
      type: "POST",
      data: dataToPost,
      success: function (data) {
        sessionStorage.setItem("AddPaymentMethodCardCaptureUri", data.cardCaptureUri);
        sessionStorage.setItem("AddPaymentMethodSecureSessionId", data.secureSessionId);
        sessionStorage.setItem("AddPaymentMethodThreeDSecureUri", data.threeDSecureUri);

        loadIframe();
      }
    });
  }

  function loadIframe() {
    var cardSessionData = getTodaysPaymentSessionData();

    bgl.components.takepayment.cardcaptureiframe.loadIframe(
      'Card-Capture-Iframe',
      cardSessionData.CardCaptureUri,
      cardSessionData.SecureSessionId,
      cardSessionData.ThreeDSecureUri);
  }

  function onCreditCardTabClick() {
    $("#credit-card").on("click", function () {
      $("#section2").removeClass("hide")
        .addClass("active")
        .attr("display", "none");

      $("#section1").removeClass("active")
        .addClass("hide");

      $('#Credit-Card-Tab-Button').addClass("active");
      $('#Direct-Debit-Tab-Button').removeClass("active");
      window.sessionStorage.setItem("ActiveTab", "Card");
    });
  }

  function onDirectDebitTabClick() {
    $("#direct-debit").on("click", function () {
      $("#section2").removeClass("active")
        .addClass("hide")
        .attr("display", "block");

      $("#section1").removeClass("hide")
        .addClass("active");

      $('#Credit-Card-Tab-Button').removeClass("active");
      $('#Direct-Debit-Tab-Button').addClass("active");
      window.sessionStorage.setItem("ActiveTab", "BankAccount");
    });
  }

  function initCreditCard() {
    $("#Credit-Card-Selected-Person-Id").change(
      function () {
        initCardCapture();
      });
  }

  function initDirectDebit() {
    bgl.common.validator.enableMaxLength();

    bgl.common.validator.init($("#Direct-Debit-Form"),
      {
        onfocusout: function (element) {
            checkSortcodeValidation(element);
            checkAccountNumberValidation(element);
        },
        hasSubmit: false
      });

    $("#Direct-Debit-Authorise-Row label").on("click",
      function () {
        $("#" + $(this).attr("for")).click().focusout();
      });

    $("#Direct-Debit-Continue-Button").off().on("click",
      function () {
        var isValid = true;
        if (!$("#Direct-Debit-Form").valid()) {
          checkSortcodeValidation($("#SortCodePart1"));
          isValid = false;
        }
        if (isValid) {
          validateBankAccountAndSortCode();
        }

        if (!isValid) {
          getValidationMessages();
          scrollToFirstFieldWithErrorAndFocus();
        }
        return isValid;
      });

    $("#Direct-Debit-Cancel-Button").off().on("click",
      function () {
        window.location.href = componentConfiguration.ApplicationPath + "Payments";
      });
  }

  function checkAccountNumberValidation(element) {
    var isValid = false;

    if ($(element).hasClass("form-row__input account-number sort-code__input--error")) {
      $("#account-number-validation-message").addClass("hide");
    }
    if ($(element).hasClass("form-row__input account-number")) {
      isValid = $("#direct-debit-account-number").valid();
    }
    if (isValid) {
      removeAccountErrorStyle();
    }
  }

  function checkSortcodeValidation(element) {
    removeApiValidationErrorMessage(element);
    validateIndividualSortCodeInput(element);
    clearValidationErrorStylingWhenAllSortCodeInputsAreValid();
  }

  function removeApiValidationErrorMessage(element) {
    if ($(element).hasClass("form-row__input--sort-code") && !$("#sortcode-api-validation-message").hasClass("hide")) {
      removeSortCodeErrorStyle();
    }
  }

  function validateIndividualSortCodeInput(element) {
    if ($(element).valid()) {
      removeIndividualSortCodeErrorStyle(element);
    } else {
      addIndividualSortCodeErrorStyle(element);
    }
    $("#form-row-sortcode1").removeClass("error");
  }

  function clearValidationErrorStylingWhenAllSortCodeInputsAreValid() {
    var allSortCodeInputsValid = checkAllSortCodeInputsAreValid();

    if (allSortCodeInputsValid) {
      $("#SortCodeLabel").removeClass("sort-code__label--error");
      $("#sortcode-validation-text").addClass("hide");
    }
  }

  function checkAllSortCodeInputsAreValid() {
    var allSortCodeInputsValid = true;

    $(".form-row__input--sort-code").each(function () {
      if ($(this).hasClass("sort-code__input--error")) {
        allSortCodeInputsValid = false;
      }
    });

    return allSortCodeInputsValid;
  }

  function sortCodeKeyUpEvent(event, currentSortCodePart, nextSortCodePart) {
    var TAB_KEYCODE = 9;
    var SHIFT_KEYCODE = 16;
    var LEFT_ARROW_KEYCODE = 37;
    var UP_ARROW_KEYCODE = 38;
    var RIGHT_ARROW_KEYCODE = 39;
    var DOWN_ARROW_KEYCODE = 40;

    var currentLength = $(currentSortCodePart).val().length;
    if (currentLength === 2
      && event.which !== TAB_KEYCODE
      && event.which !== SHIFT_KEYCODE
      && event.which !== LEFT_ARROW_KEYCODE
      && event.which !== UP_ARROW_KEYCODE
      && event.which !== RIGHT_ARROW_KEYCODE
      && event.which !== DOWN_ARROW_KEYCODE) {
      $(nextSortCodePart).focus().select();
    }
  }

  function sortCodeAutoTab() {
    var container1 = document.getElementById("SortCodePart1");
    var container2 = document.getElementById("SortCodePart2");

    container1.onkeyup = function (e) {
      sortCodeKeyUpEvent(e, "#SortCodePart1", "#SortCodePart2");
    };

    container2.onkeyup = function (e) {
      sortCodeKeyUpEvent(e, "#SortCodePart2", "#SortCodePart3");
    };
  }

  function validateBankAccountAndSortCode() {
    var dataToPost = {
      __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
      sortcode: $("#SortCodePart1").val() + $("#SortCodePart2").val() + $("#SortCodePart3").val(),
      accountnumber: $("#direct-debit-account-number").val()
    };
    $.ajax({
      url: componentConfiguration.ApplicationPath + "TakePaymentComponent/" + "PostAccountNumberAndSortCode",
      type: "POST",
      cache: false,
      dataType: "json",
      data: dataToPost,
      success: function (result) {
        if (result.isSuccess === false) {
          bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
          return true;
        }
        else if (result.Result === "Validated") {
          if (typeof bgl.common.pubsub !== 'undefined') {
            bgl.common.pubsub.emit('fakeFormSubmit', $('#Direct-Debit-Form').attr('id') || $('#Direct-Debit-Form').attr('action') || 'unknown');
          }
          removeSortCodeErrorStyle();
          removeAccountErrorStyle();
          loadAddPaymentConfirmation();
          return true;
        }
        else if (result.Result === "sortCode invalid") {
          addSortCodeErrorStyle();
          getInvalidAccountNumberOrSortCodeMessages();
          return false;
        }
        else if (result.Result === "accountNumber invalid") {
          addAccountErrorStyle();
          getInvalidAccountNumberOrSortCodeMessages();
          return false;
        }
        getInvalidAccountNumberOrSortCodeMessages();
        return false;
      }
    });
  }

  function loadAddPaymentConfirmation() {
    bgl.common.utilities.redirectToUrl(componentConfiguration.RedirectUrl);
  }

  function removeSortCodeErrorStyle() {
    $("#sortcode-api-validation-message").addClass("hide");
    $("#SortCodeLabel").removeClass("sort-code__label--error");
    $("#sortcode-api-validation-message").removeClass("sort-code__span--error");
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").removeClass("sort-code__input--error");
    cleanAriaDescribedBy("#SortCodePart1,#SortCodePart2,#SortCodePart3");
  }

  function addSortCodeErrorStyle() {
    $("#sortcode-api-validation-message").removeClass("hide");
    $("#SortCodeLabel").addClass("sort-code__label--error");
    $("#sortcode-api-validation-message").addClass("sort-code__span--error");
    $("#SortCodePart1,#SortCodePart2,#SortCodePart3").addClass("sort-code__input--error")
      .attr("aria-describedby", "sortcode-api-validation-message");
  }

  function removeIndividualSortCodeErrorStyle(element) {
    $(element).removeClass("sort-code__input--error");
    cleanAriaDescribedBy(element);
  }

  function addIndividualSortCodeErrorStyle(element) {
    $("#SortCodeLabel").addClass("sort-code__label--error");
    $("#sortcode-validation-text").removeClass("hide");
    $(element).addClass("sort-code__input--error")
      .attr("aria-describedby", "sortcode-validation-text");
  }

  function addAccountErrorStyle() {
    $("#account-number-validation-message").removeClass("hide");
    $("#account-number-validation-message").addClass("sort-code__span--error");
    $("#direct-debit-account-number").addClass("sort-code__input--error");
  }

  function removeAccountErrorStyle() {
    $("#account-number-validation-message").addClass("hide");
    $("#account-number-validation-message").removeClass("sort-code__span--error");
    $("#direct-debit-account-number").removeClass("sort-code__input--error");
    $("#direct-debit-account-number").removeAttr("aria-describedby");
    cleanAriaDescribedBy("#direct-debit-account-number");
  }

  function cleanAriaDescribedBy(element) {
    if ($(element).attr("aria-describedby")) {
      $(element).removeAttr("aria-describedby");
    }
  }

  function getTodaysPaymentSessionData() {
    return {
      "Name": sessionStorage.getItem("AddPaymentSelectedPersonIdText"),
      "CardCaptureUri": sessionStorage.getItem("AddPaymentMethodCardCaptureUri"),
      "SecureSessionId": sessionStorage.getItem("AddPaymentMethodSecureSessionId"),
      "SelectedPersonId": sessionStorage.getItem("AddPaymentSelectedPersonIdVal"),
      "ThreeDSecureUri": sessionStorage.getItem("AddPaymentMethodThreeDSecureUri")
    };
  }

  function getContinuousPaymentCardSecureSessionData() {
    return {
      "Name": sessionStorage.getItem("ContinuousPaymentCardSelectedPersonIdText"),
      "CardCaptureUri": sessionStorage.getItem("AddPaymentMethodCardCaptureUri"),
      "SecureSessionId": sessionStorage.getItem("AddPaymentMethodSecureSessionId"),
      "SelectedPersonId": sessionStorage.getItem("AddPaymentSelectedPersonIdVal"),
      "ThreeDSecureUri": sessionStorage.getItem("AddPaymentMethodThreeDSecureUri")
    };
  }

  function scrollToFirstFieldWithErrorAndFocus() {
    var firstRequiredField = $('.error,.sort-code__input--error').first();
    $('html,body').animate({ scrollTop: firstRequiredField.offset().top }, 'slow');

    if (firstRequiredField.length) {
      var invalidInputElem = firstRequiredField.find("input, select, textarea").addBack("input, select, textarea")[0];

      if (invalidInputElem !== undefined) {
        invalidInputElem.focus();
      }
    }
  }

  function getValidationMessages() {
    if ($(".form-row").hasClass("error")) {
      var data = {
        form_section: $('h1').text().trim(),
        error_details: []
      }

      $('.error').find('.form-row__validation-text').each(function () {
        addError(data, $(this).attr('data-valmsg-for'), $(this).text().trim());
      });

      var sortcodeValidationElement = $("#sortcode-validation-text");
      if (sortcodeValidationElement.length == 1 && !sortcodeValidationElement.hasClass("hide")) {
        addError(data, $("#SortCodeLabel").text().trim(), $("#sortcode-validation-text").text().trim());
      }

      var sortcodeApiValidationElement = $("#sortcode-api-validation-message");
      if (sortcodeApiValidationElement.length == 1 && !sortcodeApiValidationElement.hasClass("hide")) {
        addError(data, $("#SortCodeLabel").text().trim(), $("#sortcode-api-validation-message").text().trim());
      }

      bgl.common.pubsub.emit('formValidationFailed', data);
    }
  }

  function getInvalidAccountNumberOrSortCodeMessages() {
    var data = { error_details: [] }

    var accountNumberValidationElement = $("#account-number-validation-message");
    if (accountNumberValidationElement.length == 1 && !accountNumberValidationElement.hasClass("hide")) {
      addError(data, $("label[for='direct-debit-account-number']").text().trim(), $("#account-number-validation-message").text().trim());
    }

    var sortcodeApiValidationElement = $("#sortcode-api-validation-message");
    if (sortcodeApiValidationElement.length == 1 && !sortcodeApiValidationElement.hasClass("hide")) {
      addError(data, $("#SortCodeLabel").text().trim(), $("#sortcode-api-validation-message").text().trim());
    }

    if (data.error_details.length > 0) {
      data.form_section = $('h1').text().trim();
      bgl.common.pubsub.emit('formValidationFailed', data);
    }
  }

  function addError(data, field, message) {
    var errorDetail = {
      error_field: field,
      error_message: message
    };
    data.error_details.push(errorDetail);
  }

  return {
    init: initComponent,
    load: loadComponent,
    getValidationMessages: getValidationMessages,
    getInvalidAccountNumberOrSortCodeMessages: getInvalidAccountNumberOrSortCodeMessages
  };
})();

if (typeof module !== "undefined") {
  module.exports = bgl.components.takepayment.addpaymentmethod;
}