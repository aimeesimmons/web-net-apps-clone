﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.maritalstatus = (function () {
    var form,
        configuration,
        container,
        registerKeeper,
        SPACE_KEYCODE = 32,
        ENTER_KEYCODE = 13;

    function initComponent(componentConfiguration, containerId) {
        initElements(componentConfiguration, containerId);

        registerKeeper.redirectIfSessionStorageIsNotFound(form, componentConfiguration.ParentJourneyUrl);

        subscribeForEvents();

        registerKeeper.updateHeading(form);

        registerKeeper.displaySubmitButton(form);
    }

    function loadComponent(config, url, containerId) {
        bgl.common.loader.load(config,
            url,
            containerId,
            function () {
                initComponent(config, containerId);
            });
    }

    function onMultiStepEditSuccess(configuration, data) {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onSuccess(configuration, data) {
        bgl.components.coverdetails.sameaddress.load(configuration, data.nextUrl, container);
    }

    function onError() {
        initComponent(configuration, container);
    }

    function initElements(componentConfiguration, containerId) {
        configuration = componentConfiguration;
        container = containerId;
        form = $("#marital-status-form");
        registerKeeper = bgl.components.coverdetails.registeredkeeper;
    }

    function subscribeForEvents() {

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
                });

        $('[id$=marital-status]')
            .off('click')
            .off('keypress')
            .on({
                "keypress": function (e) {
                    if (e.keyCode === ENTER_KEYCODE || e.keyCode === SPACE_KEYCODE) {
                        $(this).click();
                    }
                },
                'click': function (event) {
                    registerKeeper.submitForm(form);
                }
            });

        form.find('.back-button-link').off('click').one('click',
            function (event) {
                event.preventDefault();

                var url = $(this).attr('href');

                bgl.components.coverdetails.aboutyou.load(configuration, url, container);
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onMultiStepEditSuccess: onMultiStepEditSuccess,
        onError: onError
    };
})();