﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.priceprotectdetailview = (function () {
    var configuration,
        form;

    function initComponent(componentConfiguration) {
        form = $("#update-price-protection-form");
        configuration = componentConfiguration;
        sessionStorage.setItem("priceProtectionAvailable", true);
        shouldRadioBeSelectedOnLoad();
        shouldContinueButtonTextUpdate();
        onRadioButtonClick();
        onNextButtonClick();
        bgl.components.coverdetails.common.onAddonBackButtonClick();
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
                });
        
    }

    function shouldContinueButtonTextUpdate() {
        if ($('.form-row__input--radio').is(':checked')) {
            updateSkipButtonToNextOptionalExtras();
        }
    }

    function shouldRadioBeSelectedOnLoad() {
        if ($('.form-row__input--radio').is(':checked')) {
            var selectedValue = $('input[name=IsAddonSelected]:checked').val();
            var isValueSetForPriceProtect = sessionStorage.getItem("PriceProtect-Selected");
            if (selectedValue === "No" && isValueSetForPriceProtect === null) {
                $('.form-row__input--radio').prop('checked', false);
            }
            if (selectedValue === "Yes") {
                sessionStorage.setItem("PriceProtect-Selected", true);
            } else
            {
                sessionStorage.setItem("PriceProtect-Selected", false);
            }
        }
    }

    function onNextButtonClick() {
        $("#next-button").on("click",
            function (e) {
                e.preventDefault();
                var selectedValue = $('input[name=IsAddonSelected]:checked').val();
                window.location.href = (selectedValue !== undefined && selectedValue === "Yes") ? configuration.ContinueUrl : configuration.NextStepUrl;
            });
    }

    function onRadioButtonClick() {
        $(".form-row__input--radio").off("change").on("change",
            function () {
                updateSkipButtonToNextOptionalExtras();
                var id = $(this).prop('id');
                var isPriceProtectAdded = sessionStorage.getItem("PriceProtect-Selected");
                if (id === "price-protect-no") {
                    if (isPriceProtectAdded === "true") {
                        submitForm();
                    }
                    sessionStorage.setItem("PriceProtect-Selected", false);
                } else if (id === "price-protect-yes") {
                    if (isPriceProtectAdded === "false" || isPriceProtectAdded === null) {
                        submitForm();
                    }
                    sessionStorage.setItem("PriceProtect-Selected", true);
                }
            });
    }

    function submitForm() {
        form.submit();
        setLinksDisabled(true);
    }

    function successCallback(message) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: message
        });

        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        var message = data.houstonMessage || data.message;
        if (message) {
            var params = {
                callback: function () {
                    successCallback(message);
                },
                spinnerName: "addonSpinner"
            };

            bgl.common.pubsub.emit("PollBasket", params);

        } else {
            bgl.common.pubsub.emit("PollBasket");
        }
        setLinksDisabled(false);
    }

    function setLinksDisabled(disabled) {
        var elements = $('button, a, select, input, label, [data-tier], .shopping-basket, #Houston');
        elements.prop("disabled", disabled);

        if (disabled) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        }
    }

    function updateSkipButtonToNextOptionalExtras() {
        $("#next-button").html('<strong>Next: </strong>Optional Extra');
    }
    return {
        onSuccess: onSuccess,
        init: initComponent
    };
})();