using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.PostOffice.MotorQuote.Controllers
{
    public class ExtrasController : SecuredController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}