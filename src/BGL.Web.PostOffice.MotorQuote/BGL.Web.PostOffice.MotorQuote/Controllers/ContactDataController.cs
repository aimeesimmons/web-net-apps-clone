using System.Web.Mvc;

namespace BGL.Web.PostOffice.MotorQuote.Controllers
{
    public class ContactDataController : Controller
    {
        public ActionResult HoustonContactData()
        {
            return PartialView("_HoustonContactData");
        }
    }
}