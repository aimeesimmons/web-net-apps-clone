using System.Web.Mvc;

namespace BGL.Web.PostOffice.MotorQuote.Controllers
{
    public class CustomerMatchController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/ExistingCustomer/Index.cshtml");
        }

        public ActionResult RetrieveQuoteExistingCustomer()
        {
            return View("~/Views/ExistingCustomer/RetrieveQuoteExistingCustomer.cshtml");
        }
    }
}