using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.PostOffice.MotorQuote.Controllers
{
    public class CoverFeaturesController : SecuredController
    {
        public ActionResult LegalProtection()
        {
            return PartialView();
        }

        public ActionResult LegalProtectionWithBothVariants()
        {
            return PartialView();
        }

        public ActionResult DriveOtherCars()
        {
            return PartialView();
        }

        public ActionResult CourtesyCar()
        {
            return PartialView();
        }

        public ActionResult UninsuredDriverPromise()
        {
            return PartialView();
        }

        public ActionResult VandalismPromise()
        {
            return PartialView();
        }

        public ActionResult WindscreenCover()
        {
            return PartialView();
        }

        public ActionResult DrivingAbroad()
        {
            return PartialView();
        }

        public ActionResult PersonalBelongings()
        {
            return PartialView();
        }

        public ActionResult PersonalBelongingsPremier()
        {
            return PartialView();
        }

        public ActionResult ChildSeatCover()
        {
            return PartialView();
        }

        public ActionResult EmergencyTransport()
        {
            return PartialView();
        }

        public ActionResult MedicalExpenses()
        {
            return PartialView();
        }

        public ActionResult PersonalAccidentBenefits()
        {
            return PartialView();
        }

        public ActionResult ReplacementLocks()
        {
            return PartialView();
        }

        public ActionResult AudioEquipment()
        {
            return PartialView();
        }

        public ActionResult BreakdownCover()
        {
            return PartialView();
        }

        public ActionResult BreakdownCoverWithBothVariants()
        {
            return PartialView();
        }

        public ActionResult GuaranteedReplacementVehicle()
        {
            return PartialView();
        }

        public ActionResult GuaranteedReplacementVehicleWithBothVariants()
        {
            return PartialView();
        }

        public ActionResult KeyCover()
        {
            return PartialView();
        }

        public ActionResult KeyCoverWithBothVariants()
        {
            return PartialView();
        }
    }
}