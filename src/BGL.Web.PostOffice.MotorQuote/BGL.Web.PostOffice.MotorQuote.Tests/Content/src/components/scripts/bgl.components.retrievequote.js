﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.retrievequote = (function () {
    var configuration;

    function initComponent(componentConfiguration) {

        configuration = componentConfiguration;

        var form = $('#retrieve-quote-form');
        bgl.common.validator.init(form);
        bgl.common.date.initDateFormat($('#DateOfBirth'));

        $('#DontKnowQuoteButton').off('click').on('click',
            function (event) {
                event.preventDefault();
                bgl.common.pubsub.emit('showContactDataNotification');
            });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    var errorMessageBox = $('.error-messagebox');
                    var form = $(this);

                    if (errorMessageBox) {
                        errorMessageBox.hide();
                    }

                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.components.recaptcha.populateToken(function () {
                            bgl.common.validator.submitForm(form, configuration);
                        });
                    }
                });
    }

    function onSuccess(configuration, data) {
        if (data.redirectUrl) {
            bgl.common.pubsub.emit('PollBasket',
            {
                callback: function () {
                    bgl.common.utilities.redirectToUrl(data.redirectUrl);
                },
                hideNotification: true
            });
        }
    }

    function onError() {
        bgl.components.retrievequote.init(configuration);
        bgl.components.recaptcha.addHiddenInputToForm();
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };

})();