using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.PostOffice.MotorQuote.Controllers;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ReviewControllerTests
    {
        // Arrange
        private ReviewController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new ReviewController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }
    }
}