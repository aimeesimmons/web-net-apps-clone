using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.PostOffice.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class YourDetailsControllerTests
    {
        // Arrange
        private YourDetailsController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new YourDetailsController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }

        [Test]
        public void MaritalStatus()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MaritalStatus());
        }

        [Test]
        public void Address()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Address());
        }

        [Test]
        public void HomeOwner()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.HomeOwner());
        }

        [Test]
        public void InformationSource()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.InformationSource());
        }

        [Test]
        public void UkResident()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.UkResident());
        }

        [Test]
        public void EmploymentStatus()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.EmploymentStatus());
        }

        [Test]
        public void EmploymentOccupation()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.EmploymentOccupation());
        }

        [Test]
        public void EmploymentBusinessType()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.EmploymentBusinessType());
        }

        [Test]
        public void PartTimeOccupation()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.PartTimeOccupation());
        }

        [Test]
        public void PartTimeOccupationType()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.PartTimeOccupationType());
        }

        [Test]
        public void PartTimeBusinessType()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.PartTimeBusinessType());
        }

        [Test]
        public void LicenceType()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.LicenceType());
        }

        [Test]
        public void LicenceDate()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.LicenceDate());
        }

        [Test]
        public void LicenceNumber()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.LicenceNumber());
        }

        [Test]
        public void MedicalCondition()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MedicalCondition());
        }

        [Test]
        public void MedicalConditionDvlaNotified()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MedicalConditionDvlaNotified());
        }

        [Test]
        public void DeclinedInsurance()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.DeclinedInsurance());
        }

        [Test]
        public void MotorClaims()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MotorClaims());
        }

        [Test]
        public void MotorConvictions()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MotorConvictions());
        }

        [Test]
        public void CriminalConvictions()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.CriminalConvictions());
        }

        [Test]
        public void AdditionalDriver()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.AdditionalDrivers());
        }
    }
}