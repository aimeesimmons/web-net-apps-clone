using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.PostOffice.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ContactDataControllerTests
    {
        private ContactDataController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new ContactDataController();
        }

        [Test]
        public void HoustonContactData()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.HoustonContactData());
        }
    }
}