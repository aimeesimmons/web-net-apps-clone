using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.PostOffice.MotorQuote.Controllers;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class CoverFeaturesControllerTests
    {
        private CoverFeaturesController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new CoverFeaturesController();
        }

        [Test]
        public void LegalProtection()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.LegalProtection());
        }

        [Test]
        public void LegalProtectionWithBothVariants()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.LegalProtectionWithBothVariants());
        }

        [Test]
        public void DriveOtherCars()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.DriveOtherCars());
        }

        [Test]
        public void CourtesyCar()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.CourtesyCar());
        }

        [Test]
        public void UninsuredDriverPromise()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.UninsuredDriverPromise());
        }

        [Test]
        public void VandalismPromise()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.VandalismPromise());
        }

        [Test]
        public void WindscreenCover()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.WindscreenCover());
        }

        [Test]
        public void DrivingAbroad()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.DrivingAbroad());
        }

        [Test]
        public void PersonalBelongings()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.PersonalBelongings());
        }

        [Test]
        public void ChildSeatCover()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.ChildSeatCover());
        }

        [Test]
        public void EmergencyTransport()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.EmergencyTransport());
        }

        [Test]
        public void MedicalExpenses()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.MedicalExpenses());
        }

        [Test]
        public void PersonalAccidentBenefits()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.PersonalAccidentBenefits());
        }

        [Test]
        public void ReplacementLocks()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.ReplacementLocks());
        }

        [Test]
        public void AudioEquipment()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.AudioEquipment());
        }

        [Test]
        public void BreakdownCover()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.BreakdownCover());
        }

        [Test]
        public void BreakdownCoverWithBothVariants()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.BreakdownCoverWithBothVariants());
        }

        [Test]
        public void GuaranteedReplacementVehicle()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.GuaranteedReplacementVehicle());
        }

        [Test]
        public void GuaranteedReplacementVehicleWithBothVariants()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.GuaranteedReplacementVehicleWithBothVariants());
        }

        [Test]
        public void KeyCover()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.KeyCover());
        }

        [Test]
        public void KeyCoverWithBothVariants()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.KeyCoverWithBothVariants());
        }
    }
}