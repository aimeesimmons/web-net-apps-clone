﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.PricePresentation
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "PricePresentation";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("WelcomeQuoteConfiguration", GetDocumentPart("new ComponentWelcomeQuoteConfiguration", "};"));
            Components.Add("СoverFeaturePriorityForPremier", GetDocumentPart("(hasPremier && isPremierSelected)", "};"));
            Components.Add("CoverListWidgetTicks", GetComponent("CoverListWidgetTicks"));
            Components.Add("CoverExcess", GetComponent("CoverExcess"));
        }

        [Test]
        public void WelcomeQuoteConfigurationRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["WelcomeQuoteConfiguration"]));
        }

        [Test]
        public void СoverFeaturePriorityForPremierRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["СoverFeaturePriorityForPremier"]));
        }

        [Test]
        public void CoverListWidgetTicksComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverListWidgetTicks"]));
        }

        [Test]
        public void CoverExcessComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverExcess"]));
        }

        [TestCase("WelcomeQuoteConfiguration", "Id", "\"SalesWelcomeQuote\",")]
        [TestCase("WelcomeQuoteConfiguration", "ManufactureListInUpperCase", "new List<string> { \"BMW\", \"MG\", \"SEAT\" },")]
        [TestCase("WelcomeQuoteConfiguration", "AvivaCreditPhoneNumber", "\"0344 412 2122\",")]
        [TestCase("WelcomeQuoteConfiguration", "ViewOptions", "new WelcomeQuoteViewOptionsConfiguration \r\n { \r\n WelcomeQuoteAmountRadioLabel = WelcomeQuoteAmountRadioLabel.WithoutCar },")]
        [TestCase("WelcomeQuoteConfiguration", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("СoverFeaturePriorityForPremier", "coverFeaturePriority", "new Dictionary<string, CoverFeatureItem>\r\n            {\r\n                {\r\n                    FeatureServiceConstants.UninsuredDriverPromise, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"UninsuredDriverPromise\", \"CoverFeatures\"),\r\n                        Description = \"Cover if you are hit by an uninsured motorist\",\r\n                        IsPremier = true,\r\n                        HeaderText = \"Premier\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.VandalismPromise, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"VandalismPromise\", \"CoverFeatures\"),\r\n                        Description = \"Cover if your car suffers damage caused by vandalism\",\r\n                        IsPremier = true,\r\n                        HeaderText = \"Premier\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.ChildSeatCover, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"ChildSeatCover\", \"CoverFeatures\"),\r\n                        Description = \"Good news for the younger members of the family\",\r\n                        IsPremier = true,\r\n                        HeaderText = \"Premier\"\r\n\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.TransferHome, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"EmergencyTransport\", \"CoverFeatures\"),\r\n                        Description = \"Help for you/your passengers if you have an accident\",\r\n                        IsPremier = true,\r\n                        HeaderText = \"Premier\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.WindscreenCover, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"WindscreenCover\", \"CoverFeatures\"),\r\n                        Description = \"Covering the cost of repairing or replacing your glass\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.DrivingAbroad, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"DrivingAbroad\", \"CoverFeatures\"),\r\n                        Description = \"Great news - you can take your car abroad\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.PersonalBelongings, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(personalBelongingsAction, \"CoverFeatures\"),\r\n                        Description = \"We will cover your personal belongings too\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.AudioEquipment, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"AudioEquipment\", \"CoverFeatures\"),\r\n                        Description = \"Cover for your audio, navigation and entertainment equipment\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.MedicalExpenses, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"MedicalExpenses\", \"CoverFeatures\"),\r\n                        Description = \"Cover up to \\u00A3200 if you need medical help\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.ReplacementLocks, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"ReplacementLocks\", \"CoverFeatures\"),\r\n                        Description = \"Cover for you car door and /or boot locks\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.DriveOtherVehicles, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"DriveOtherCars\", \"CoverFeatures\"),\r\n                        Description = \"We may be able to cover you to drive other cars\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.PersonalAccidentBenefits, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"PersonalAccidentBenefits\", \"CoverFeatures\"),\r\n                        Description = \"A range of injuries covered for you/partner\"\r\n                    }\r\n                },\r\n                {\r\n                    FeatureServiceConstants.CourtesyVehicle, new CoverFeatureItem\r\n                    {\r\n                        ContentUrl = Url.Action(\"CourtesyCar\", \"CoverFeatures\"),\r\n                        Description = \"Provides you with a car, subject to availability\", HasUpdate = true\r\n                    }\r\n                }\r\n            };")]
        [TestCase("CoverListWidgetTicks", "Id", "\"CoverList\",")]
        [TestCase("CoverListWidgetTicks", "CoverFeaturePriority", "new Dictionary<string, CoverFeatureItem>\r\n{\r\n{ FeatureServiceConstants.NcdProtection, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName } },\r\n{ FeatureServiceConstants.LegalProtection, new CoverFeatureItem { ContentUrl = Url.Action(\"LegalProtection\", \"CoverFeatures\") } },\r\n{ FeatureServiceConstants.GuaranteedReplacementVehicle, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName  } },\r\n{ FeatureServiceConstants.Breakdown, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName  } },\r\n{ FeatureServiceConstants.PriceProtection, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName } },\r\n{ FeatureServiceConstants.KeyCover, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName } },\r\n{ FeatureServiceConstants.PersonalAccidentCover, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName } }\r\n},")]
        [TestCase("CoverListWidgetTicks", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("CoverExcess", "Id", "\"CoverExcessBar\",")]
        [TestCase("CoverExcess", "ExcessConfiguration", "new ExcessConfiguration\r\n{\r\nVoluntaryExcessAvailableAmounts = new List<int> { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 }\r\n},")]
        [TestCase("CoverExcess", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}