using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MedicalConditionDvlaNotifiedTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "MedicalConditionDvlaNotified";
            Document = GetDocument();
            Components.Add("DriverDetailsMedicalConditionDvlaNotified", GetComponent("DriverDetailsMedicalConditionDvlaNotified"));
        }

        [Test]
        public void DriverDetailsMedicalConditionComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsMedicalConditionDvlaNotified"]));
        }

        [TestCase("DriverDetailsMedicalConditionDvlaNotified", "Id", "\"MedicalCondition\",")]
        [TestCase("DriverDetailsMedicalConditionDvlaNotified", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsMedicalConditionDvlaNotified", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsMedicalConditionDvlaNotified", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsMedicalConditionDvlaNotified", "NextStepUrl", "Url.Action(\"DeclinedInsurance\", \"YourDetails\"),")]
        [TestCase("DriverDetailsMedicalConditionDvlaNotified", "PreviousStepUrl", "Url.Action(\"MedicalCondition\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}