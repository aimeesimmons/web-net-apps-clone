using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AddressTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "Address";
            Document = GetDocument();
            Components.Add("DriverDetailsAddress", GetComponent("DriverDetailsAddress"));
        }

        [Test]
        public void DriverDetailsAddressComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsAddress"]));
        }

        [TestCase("DriverDetailsAddress", "Id", "\"Address\",")]
        [TestCase("DriverDetailsAddress", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsAddress", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsAddress", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsAddress", "NextStepUrl", "Url.Action(\"HomeOwner\", \"YourDetails\"),")]
        [TestCase("DriverDetailsAddress", "PreviousStepUrl", "Url.Action(\"MaritalStatus\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}