using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LicenceNumberTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "LicenceNumber";
            Document = GetDocument();
            Components.Add("DriverDetailsDrivingLicenceNumber", GetComponent("DriverDetailsDrivingLicenceNumber"));
        }

        [Test]
        public void DriverDetailsDrivingLicenceNumberComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsDrivingLicenceNumber"]));
        }

        [TestCase("DriverDetailsDrivingLicenceNumber", "Id", "\"DriverLicence\",")]
        [TestCase("DriverDetailsDrivingLicenceNumber", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsDrivingLicenceNumber", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsDrivingLicenceNumber", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsDrivingLicenceNumber", "NextStepUrl", "Url.Action(\"MedicalCondition\", \"YourDetails\"),")]
        [TestCase("DriverDetailsDrivingLicenceNumber", "PreviousStepUrl", "Url.Action(\"LicenceDate\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}