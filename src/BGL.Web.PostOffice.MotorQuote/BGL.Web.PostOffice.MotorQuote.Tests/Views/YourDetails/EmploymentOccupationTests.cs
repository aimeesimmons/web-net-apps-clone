using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class EmploymentOccupationTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "EmploymentOccupation";
            Document = GetDocument();
            Components.Add("DriverDetailsOccupation", GetComponent("DriverDetailsOccupation"));
        }

        [Test]
        public void DriverDetailsOccupationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsOccupation"]));
        }

        [TestCase("DriverDetailsOccupation", "Id", "\"EmploymentStatus\",")]
        [TestCase("DriverDetailsOccupation", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsOccupation", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsOccupation", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsOccupation", "NextStepUrl", "Url.Action(\"EmploymentBusinessType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsOccupation", "PreviousStepUrl", "Url.Action(\"EmploymentStatus\", \"YourDetails\")")]
        [TestCase("DriverDetailsOccupation", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}