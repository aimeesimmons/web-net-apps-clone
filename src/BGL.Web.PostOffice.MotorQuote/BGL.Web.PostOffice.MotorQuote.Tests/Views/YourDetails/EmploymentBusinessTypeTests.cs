using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class EmploymentBusinessTypeTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "EmploymentBusinessType";
            Document = GetDocument();
            Components.Add("DriverDetailsBusinessType", GetComponent("DriverDetailsBusinessType"));
        }

        [Test]
        public void DriverDetailsBusinessTypeComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsBusinessType"]));
        }

        [TestCase("DriverDetailsBusinessType", "Id", "\"EmploymentStatus\",")]
        [TestCase("DriverDetailsBusinessType", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsBusinessType", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsBusinessType", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsBusinessType", "NextStepUrl", "Url.Action(\"PartTimeOccupation\", \"YourDetails\"),")]
        [TestCase("DriverDetailsBusinessType", "PreviousStepUrl", "Url.Action(\"EmploymentOccupation\", \"YourDetails\")")]
        [TestCase("DriverDetailsBusinessType", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}