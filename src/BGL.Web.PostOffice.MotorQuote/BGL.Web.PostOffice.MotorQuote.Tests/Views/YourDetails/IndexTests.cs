using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("DriverDetailsAboutYou", GetComponent("DriverDetailsAboutYou"));
        }

        [Test]
        public void DriverDetailsAboutYouComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsAboutYou"]));
        }

        [TestCase("DriverDetailsAboutYou", "Id", "\"AboutYou\",")]
        [TestCase("DriverDetailsAboutYou", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsAboutYou", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsAboutYou", "NextStepUrl", "Url.Action(\"MaritalStatus\", \"YourDetails\"),")]
        [TestCase("DriverDetailsAboutYou", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}