using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class HomeOwnerTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "HomeOwner";
            Document = GetDocument();
            Components.Add("DriverDetailsHomeOwner", GetComponent("DriverDetailsHomeOwner"));
        }

        [Test]
        public void DriverDetailsHomeOwnerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsHomeOwner"]));
        }

        [TestCase("DriverDetailsHomeOwner", "Id", "\"HomeOwner\",")]
        [TestCase("DriverDetailsHomeOwner", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsHomeOwner", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsHomeOwner", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsHomeOwner", "NextStepUrl", "Url.Action(\"InformationSource\", \"YourDetails\"),")]
        [TestCase("DriverDetailsHomeOwner", "PreviousStepUrl", "Url.Action(\"ConfirmedAddress\", \"YourDetails\")")]
        [TestCase("DriverDetailsHomeOwner", "MarketingQuestion", "\"Home insurance due? (Optional)\"")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}