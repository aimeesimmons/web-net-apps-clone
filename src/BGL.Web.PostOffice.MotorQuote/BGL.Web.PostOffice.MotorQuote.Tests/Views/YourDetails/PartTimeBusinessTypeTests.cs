using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PartTimeBusinessTypeTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "PartTimeBusinessType";
            Document = GetDocument();
            Components.Add("DriverDetailsPartTimeBusinessType", GetComponent("DriverDetailsPartTimeBusinessType"));
        }

        [Test]
        public void DriverDetailsPartTimeBusinessTypeComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsPartTimeBusinessType"]));
        }

        [TestCase("DriverDetailsPartTimeBusinessType", "Id", "\"PartTimeOccupation\",")]
        [TestCase("DriverDetailsPartTimeBusinessType", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsPartTimeBusinessType", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsPartTimeBusinessType", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsPartTimeBusinessType", "NextStepUrl", "Url.Action(\"LicenceType\", \"YourDetails\"),")]
        [TestCase("DriverDetailsPartTimeBusinessType", "PreviousStepUrl", "Url.Action(\"PartTimeOccupationType\", \"YourDetails\")")]
        [TestCase("DriverDetailsPartTimeBusinessType", "StatusCodesToAskOccupation", "new List<string>\r\n            {\r\n                \"E\",\r\n                \"S\",\r\n                \"C\",\r\n                \"D\",\r\n                \"T\",\r\n                \"V\"\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}