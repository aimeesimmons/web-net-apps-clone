using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourVan
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class VehicleDetailsTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourVan";
            ViewName = "VehicleDetails";
            Document = GetDocument();
            Components.Add("VehicleLookup", GetComponent("VehicleLookup"));
        }

        [Test]
        public void VehicleLookupComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["VehicleLookup"]));
        }

        [TestCase("VehicleLookup", "Id", "\"VehicleDetails\",")]
        [TestCase("VehicleLookup", "ManufactureListInUpperCase", "new List<string>\r\n            {\r\n                \"BMW\", \"SEAT\", \"MAN\"\r\n            },")]
        [TestCase("VehicleLookup", "ManufacturerKeys", "new List<string>\r\n            {\r\n                \"FO\",\r\n                \"VA\",\r\n                \"VW\",\r\n                \"PE\",\r\n                \"NI\",\r\n                \"BM\",\r\n                \"R1\",\r\n                \"CI\",\r\n                \"TO\",\r\n                \"AU\",\r\n                \"ME\",\r\n                \"HO\",\r\n                \"FI\",\r\n            },")]
        [TestCase("VehicleLookup", "WrongVehicleTypeForVanInsValidationErrorMessage", "BrandConstants.WrongVehicleTypeForVanInsValidationErrorMessage,")]
        [TestCase("VehicleLookup", "VanErrorMessageUrl", "BrandConstants.VanErrorMessageUrl,")]
        [TestCase("VehicleLookup", "WrongVehicleTypeForCarInsValidationErrorMessage", "BrandConstants.WrongVehicleTypeForCarInsValidationErrorMessage,")]
        [TestCase("VehicleLookup", "CarErrorMessageUrl", "BrandConstants.CarErrorMessageUrl,")]
        [TestCase("VehicleLookup", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("VehicleLookup", "SummaryButtonText", "\"Continue\",")]
        [TestCase("VehicleLookup", "ModificationButtonText", "\"Continue\",")]
        [TestCase("VehicleLookup", "DataStoreKey", "\"YourVan\"")]
        [TestCase("VehicleLookup", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("VehicleLookup", "NextStepUrl", "Url.Action(\"OvernightParking\", \"YourVan\"),")]
        [TestCase("VehicleLookup", "PreviousStepUrl", "Url.Action(\"VehicleLookup\", \"YourVan\"),")]
        [TestCase("VehicleLookup", "ComponentUrl", "Url.Action(\"VehicleDetails\", \"YourVan\"),")]
        [TestCase("VehicleLookup", "SearchSummaryUrl", "Url.Action(\"SearchSummary\", \"VehicleDetailsComponentSearching\"),")]
        [TestCase("VehicleLookup", "ModificationsUrl", "Url.Action(\"Modifications\", \"VehicleDetailsComponentModifications\"),")]
        [TestCase("VehicleLookup", "PurchasedDateUrl", "Url.Action(\"PurchaseDate\", \"VehicleDetailsComponentPurchaseDate\"),")]
        [TestCase("VehicleLookup", "EditValueUrl", "Url.Action(\"EditVehicleValue\", \"VehicleDetailsComponentValue\"),")]
        [TestCase("VehicleLookup", "VehicleBodyTypeUrl", "Url.Action(\"VehicleBodyType\", \"VehicleDetailsComponentVehicleBodyType\"),")]
        [TestCase("VehicleLookup", "NumberOfSeatsUrl", "Url.Action(\"NumberOfSeats\", \"VehicleDetailsComponentNumberOfSeats\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}