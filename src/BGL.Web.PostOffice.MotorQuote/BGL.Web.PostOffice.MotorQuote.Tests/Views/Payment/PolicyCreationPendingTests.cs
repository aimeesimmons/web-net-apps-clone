using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PolicyCreationPendingTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "PolicyCreationPending";
            Document = GetDocument();
            Components.Add("TakePaymentPolicyCreationPending", GetComponent("TakePaymentPolicyCreationPending"));
        }

        [Test]
        public void TakePaymentPaymentSummaryComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentPolicyCreationPending"]));
        }

        [TestCase("TakePaymentPolicyCreationPending", "Id", "\"PolicyCreationPending\"")]
        [TestCase("TakePaymentPolicyCreationPending", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}