using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AllDoneTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "AllDone";
            Document = GetDocument();
            Components.Add("TakePaymentAllDone", GetComponent("TakePaymentAllDone"));
        }

        [Test]
        public void TakePaymentAllDoneComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentAllDone"]));
        }

        [TestCase("TakePaymentAllDone", "Id", "\"AllDone\"")]
        [TestCase("TakePaymentAllDone", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}