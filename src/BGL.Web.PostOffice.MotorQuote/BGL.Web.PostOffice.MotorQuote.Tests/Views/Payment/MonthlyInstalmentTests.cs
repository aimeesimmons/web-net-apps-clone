using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MonthlyInstalmentTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "MonthlyInstalment";
            Document = GetDocument();
            Components.Add("TakePaymentMonthlyInstalmentAsync", GetComponent("TakePaymentMonthlyInstalmentAsync"));
        }

        [Test]
        public void TakePaymentMonthlyInstalmentAsyncComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentMonthlyInstalmentAsync"]));
        }

        [TestCase("TakePaymentMonthlyInstalmentAsync", "Id", "\"MonthlyInstalment\"")]
        [TestCase("TakePaymentMonthlyInstalmentAsync", "PaymentSummaryUrl", "Url.Action(\"PaymentSummary\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyInstalmentAsync", "MonthlyDepositUrl", "Url.Action(\"MonthlyDeposit\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyInstalmentAsync", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("TakePaymentMonthlyInstalmentAsync", "ImportantInformationUrl", "Url.Action(\"PostPriceImportantInformation\", \"Legal\"),")]
        [TestCase("TakePaymentMonthlyInstalmentAsync", "BislBrandName", "\"BUDGET\"")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}