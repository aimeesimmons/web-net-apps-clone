using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MonthlyInstalmentSummaryTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "MonthlyInstalmentSummary";
            Document = GetDocument();
            Components.Add("TakePaymentMonthlyInstalmentSummaryAsync", GetComponent("TakePaymentMonthlyInstalmentSummaryAsync"));
        }

        [Test]
        public void TakePaymentMonthlyInstalmentSummaryAsyncComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentMonthlyInstalmentSummaryAsync"]));
        }

        [TestCase("TakePaymentMonthlyInstalmentSummaryAsync", "Id", "\"MonthlyInstalmentSummary\"")]
        [TestCase("TakePaymentMonthlyInstalmentSummaryAsync", "MonthlyDepositUrl", "Url.Action(\"MonthlyDeposit\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyInstalmentSummaryAsync", "MonthlyInstalmentUrl", "Url.Action(\"MonthlyInstalment\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyInstalmentSummaryAsync", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("TakePaymentMonthlyInstalmentSummaryAsync", "ImportantInformationUrl", "Url.Action(\"PostPriceImportantInformation\", \"Legal\"),")]
        [TestCase("TakePaymentMonthlyInstalmentSummaryAsync", "BislBrandName", "\"BUDGET\"")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}