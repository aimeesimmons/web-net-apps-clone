using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class CoverStartDateTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "CoverStartDate";
            Document = GetDocument();
            Components.Add("CoverRangeDatePicker", GetComponent("CoverRangeDatePicker"));
        }

        [Test]
        public void CoverRangeDatePickerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverRangeDatePicker"]));
        }

        [TestCase("CoverRangeDatePicker", "Id", "\"DatePickerDefault\",")]
        [TestCase("CoverRangeDatePicker", "JourneyType", "JourneyType.MultiStepEdit,")]
        [TestCase("CoverRangeDatePicker", "BackButtonUrl", "Url.Action(\"CoverLevel\", \"YourCover\"),")]
        [TestCase("CoverRangeDatePicker", "CoverExcessesBackUrl", "Url.Action(\"CoverExcesses\", \"YourCover\"),")]
        [TestCase("CoverRangeDatePicker", "NextStepUrl", "Url.Action(\"AnnualMileage\", \"YourCover\"),")]
        [TestCase("CoverRangeDatePicker", "DataStoreKey", "\"CoverDetailsCoverLevel\",")]
        [TestCase("CoverRangeDatePicker", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}