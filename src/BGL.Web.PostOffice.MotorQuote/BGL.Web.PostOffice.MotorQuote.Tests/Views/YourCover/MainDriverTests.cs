using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MainDriverTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "MainDriver";
            Document = GetDocument();
            Components.Add("MainDriver", GetComponent("MainDriver"));
        }

        [Test]
        public void MainDriverComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["MainDriver"]));
        }

        [TestCase("MainDriver", "Id", "\"MainDriver\",")]
        [TestCase("MainDriver", "DataStoreKey", "\"CoverDetailsMainDriver\",")]
        [TestCase("MainDriver", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("MainDriver", "NextStepUrl", "Url.Action(\"CoverLevel\", \"YourCover\")")]
        [TestCase("MainDriver", "BackButtonUrl", "Url.Action(\"Index\", \"YourCover\"),")]
        [TestCase("MainDriver", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}