using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("CoverDetailsRegisteredKeeper", GetComponent("CoverDetailsRegisteredKeeper"));
        }

        [Test]
        public void CoverDetailsRegisteredKeeperComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsRegisteredKeeper"]));
        }

        [TestCase("CoverDetailsRegisteredKeeper", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetailsRegisteredKeeper", "ErrorConfiguration", "new ComponentErrorConfiguration(),")]
        [TestCase("CoverDetailsRegisteredKeeper", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsRegisteredKeeper", "DataStoreKey", "\"CoverDetails\",")]
        [TestCase("CoverDetailsRegisteredKeeper", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsRegisteredKeeper", "RelationshipPageUrl", "Url.Action(\"RegisteredKeeperRelationship\", \"YourCover\"),")]
        [TestCase("CoverDetailsRegisteredKeeper", "PreviousStepUrl", "Url.Action(\"OvernightParking\", controller),")]
        [TestCase("CoverDetailsRegisteredKeeper", "NextStepUrl", "Url.Action(\"MainDriver\", \"YourCover\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}