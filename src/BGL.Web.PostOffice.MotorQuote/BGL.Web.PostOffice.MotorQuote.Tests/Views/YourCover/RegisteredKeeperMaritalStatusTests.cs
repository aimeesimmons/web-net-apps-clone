using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class RegisteredKeeperMaritalStatusTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "RegisteredKeeperMaritalStatus";
            Document = GetDocument();
            Components.Add("CoverDetailsMaritalStatus", GetComponent("CoverDetailsMaritalStatus"));
        }

        [Test]
        public void CoverDetailsMaritalStatusComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsMaritalStatus"]));
        }

        [TestCase("CoverDetailsMaritalStatus", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetailsMaritalStatus", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsMaritalStatus", "DataStoreKey", "\"CoverDetails\",")]
        [TestCase("CoverDetailsMaritalStatus", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsMaritalStatus", "MainDriverPageUrl", "Url.Action(\"MainDriver\", \"YourCover\"),")]
        [TestCase("CoverDetailsMaritalStatus", "PreviousStepUrl", "Url.Action(\"AboutRegisteredKeeper\", \"YourCover\"),")]
        [TestCase("CoverDetailsMaritalStatus", "NextStepUrl", "Url.Action(\"RegisteredKeeperSameAddress\", \"YourCover\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}