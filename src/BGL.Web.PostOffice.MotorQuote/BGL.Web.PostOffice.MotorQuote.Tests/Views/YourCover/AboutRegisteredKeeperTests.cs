using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AboutRegisteredKeeperTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "AboutRegisteredKeeper";
            Document = GetDocument();
            Components.Add("CoverDetailsAboutYou", GetComponent("CoverDetailsAboutYou"));
        }

        [Test]
        public void CoverDetailsAboutYouComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsAboutYou"]));
        }

        [TestCase("CoverDetailsAboutYou", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetailsAboutYou", "ErrorConfiguration", "new ComponentErrorConfiguration(),")]
        [TestCase("CoverDetailsAboutYou", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsAboutYou", "DataStoreKey", "\"CoverDetails\",")]
        [TestCase("CoverDetailsAboutYou", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsAboutYou", "MainDriverPageUrl", "Url.Action(\"MainDriver\", \"YourCover\"),")]
        [TestCase("CoverDetailsAboutYou", "PreviousStepUrl", "Url.Action(\"RegisteredKeeperRelationship\", \"YourCover\"),")]
        [TestCase("CoverDetailsAboutYou", "NextStepUrl", "Url.Action(\"RegisteredKeeperMaritalStatus\", \"YourCover\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}