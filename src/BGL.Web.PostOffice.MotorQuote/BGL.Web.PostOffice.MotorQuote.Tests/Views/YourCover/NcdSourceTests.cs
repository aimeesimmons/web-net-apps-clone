using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class NcdSourceTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "NcdSource";
            Document = GetDocument();
            Components.Add("CoverDetailsNcdSource", GetComponent("CoverDetailsNcdSource"));
        }

        [Test]
        public void CoverDetailsNcdSourceComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsNcdSource"]));
        }

        [TestCase("CoverDetailsNcdSource", "Id", "\"NcdSource\",")]
        [TestCase("CoverDetailsNcdSource", "DataStoreKey", "\"NcdSource\",")]
        [TestCase("CoverDetailsNcdSource", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsNcdSource", "NextStepUrl", "Url.Action(\"OtherVehicles\", \"YourCover\")")]
        [TestCase("CoverDetailsNcdSource", "PreviousStepUrl", "Url.Action(\"NcdYears\", \"YourCover\"),")]
        [TestCase("CoverDetailsNcdSource", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsNcdSource", "ViewOptions", "new ViewOptionsConfiguration\r\n            {\r\n                NcdSourceCompanyCar = NcdSource.CompanyCarExperiencedDriver\r\n            }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}