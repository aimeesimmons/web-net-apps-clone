using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class VehicleUseTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "VehicleUse";
            Document = GetDocument();
            Components.Add("VehicleUsage", GetComponent("VehicleUsage"));
        }

        [Test]
        public void VehicleUsageComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["VehicleUsage"]));
        }

        [TestCase("VehicleUsage", "Id", "\"VehicleUse\",")]
        [TestCase("VehicleUsage", "DataStoreKey", "\"CoverDetailsVehicleUse\",")]
        [TestCase("VehicleUsage", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("VehicleUsage", "NextStepUrl", "Url.Action(\"NcdYears\", \"YourCover\")")]
        [TestCase("VehicleUsage", "CoverLevelUrl", "Url.Action(\"CoverStartDate\", \"YourCover\"),")]
        [TestCase("VehicleUsage", "BackButtonUrl", "Url.Action(\"AnnualMileage\", \"YourCover\"),")]
        [TestCase("VehicleUsage", "PreviousStepUrl", "Url.Action(\"CoverExcesses\", \"YourCover\"),")]
        [TestCase("VehicleUsage", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}