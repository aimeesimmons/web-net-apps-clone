using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class RegisteredKeeperRelationshipTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "RegisteredKeeperRelationship";
            Document = GetDocument();
            Components.Add("CoverDetailsRelationshipStatus", GetComponent("CoverDetailsRelationshipStatus"));
        }

        [Test]
        public void CoverDetailsRelationshipStatusComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsRelationshipStatus"]));
        }

        [TestCase("CoverDetailsRelationshipStatus", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetailsRelationshipStatus", "ErrorConfiguration", "new ComponentErrorConfiguration(),")]
        [TestCase("CoverDetailsRelationshipStatus", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsRelationshipStatus", "DataStoreKey", "\"CoverDetails\",")]
        [TestCase("CoverDetailsRelationshipStatus", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsRelationshipStatus", "MainDriverPageUrl", "Url.Action(\"MainDriver\", \"YourCover\"),")]
        [TestCase("CoverDetailsRelationshipStatus", "PreviousStepUrl", "Url.Action(\"Index\", \"YourCover\"),")]
        [TestCase("CoverDetailsRelationshipStatus", "NextStepUrl", "Url.Action(\"AboutRegisteredKeeper\", \"YourCover\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}