using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class RegisteredKeeperSameAddressTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "RegisteredKeeperSameAddress";
            Document = GetDocument();
            Components.Add("CoverDetailsSameAddress", GetComponent("CoverDetailsSameAddress"));
        }

        [Test]
        public void CoverDetailsSameAddressComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsSameAddress"]));
        }

        [TestCase("CoverDetailsSameAddress", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetailsSameAddress", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsSameAddress", "DataStoreKey", "\"CoverDetails\",")]
        [TestCase("CoverDetailsSameAddress", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsSameAddress", "MainDriverPageUrl", "Url.Action(\"maindriver\", \"YourCover\"),")]
        [TestCase("CoverDetailsSameAddress", "PreviousStepUrl", "Url.Action(\"RegisteredKeeperMaritalStatus\", \"YourCover\"),")]
        [TestCase("CoverDetailsSameAddress", "NextStepUrl", "Url.Action(\"Index\", \"YourCover\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}