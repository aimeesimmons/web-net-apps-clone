using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Extras
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Extras";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("DemandsAndNeedsSinglePageAddOnView", GetComponent("DemandsAndNeedsSinglePageAddOnView", true));
        }

        [Test]
        public void ComponentRenderDemandsAndNeedsSinglePageAddOnViewRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DemandsAndNeedsSinglePageAddOnView"]));
        }

        [TestCase("DemandsAndNeedsSinglePageAddOnView", "Id", "\"DemandsAndNeeds\"")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "NextPageUrl", "Url.Action(\"Index\", \"Review\"),")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "PreviousPageUrl", "Url.Action(\"Index\", \"PricePresentation\")")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "ErrorRedirectUrl", "ConfigurationManager.AppSettings[\"ErrorRedirectUrl\"]")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "AddonOrder", "AddonOrder(isVan)")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "AddonConfigurations", "AddOnConfigurations(isVan)")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue, "DemandsAndNeedsConfiguration", true);
        }
    }
}