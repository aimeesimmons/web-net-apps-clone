using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Review
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Review";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("VehicleDetails", GetComponent("VehicleDetails"));
            Components.Add("CoverOverview", GetComponent("CoverOverview"));
            Components.Add("CoverRange", GetComponent("CoverRange"));
            Components.Add("DriverDetailsDriverBubbles", GetComponent("DriverDetailsDriverBubbles"));
            Components.Add("VehicleDetailsList", GetComponent("VehicleDetailsList"));
            Components.Add("DriverDetails", GetComponent("DriverDetails"));
            Components.Add("CoverDetails", GetComponent("CoverDetails"));
            Components.Add("OptionalExtras", GetComponent("OptionalExtras"));
            Components.Add("MarketingPreferences", GetComponent("MarketingPreferences"));
            Components.Add("AddonsBack", GetDocumentPart(" Html.ComponentRenderAddonsBack", "})"));
        }

        [Test]
        public void VehicleDetailsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["VehicleDetails"]));
        }

        [Test]
        public void CoverOverviewComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverOverview"]));
        }

        [Test]
        public void CoverRangeComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverRange"]));
        }

        [Test]
        public void DriverDetailsDriverBubblesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsDriverBubbles"]));
        }

        [Test]
        public void VehicleDetailsListComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["VehicleDetailsList"]));
        }

        [Test]
        public void DriverDetailsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetails"]));
        }

        [Test]
        public void CoverDetailsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetails"]));
        }

        [Test]
        public void OptionalExtrasComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["OptionalExtras"]));
        }

        [Test]
        public void MarketingPreferencesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["MarketingPreferences"]));
        }

        [TestCase("VehicleDetails", "Id", "\"VehicleLookup\",")]
        [TestCase("VehicleDetails", "ManufactureListInUpperCase", "new List<string> {\"BMW\", \"MG\", \"SEAT\"},")]
        [TestCase("VehicleDetails", "ManufacturerKeys", "new List<string>\r\n                                    {\r\n                                        \"FO\",\r\n                                        \"VA\",\r\n                                        \"VW\",\r\n                                        \"PE\",\r\n                                        \"NI\",\r\n                                        \"BM\",\r\n                                        \"R1\",\r\n                                        \"CI\",\r\n                                        \"TO\",\r\n                                        \"AU\",\r\n                                        \"ME\",\r\n                                        \"HO\",\r\n                                        \"FI\"\r\n                                    },")]
        [TestCase("VehicleDetails", "WrongVehicleTypeForVanInsValidationErrorMessage", "BrandConstants.WrongVehicleTypeForVanInsValidationErrorMessage,")]
        [TestCase("VehicleDetails", "VanErrorMessageUrl", "BrandConstants.VanErrorMessageUrl,")]
        [TestCase("VehicleDetails", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("VehicleDetails", "ModificationItemsLimit", "10,")]
        [TestCase("VehicleDetails", "SummaryButtonText", "\"Continue\",")]
        [TestCase("VehicleDetails", "ModificationButtonText", "\"Continue\"")]
        [TestCase("CoverOverview", "Id", "\"CoverOverview\",")]
        [TestCase("CoverOverview", "ExcessConfiguration", "new ExcessConfiguration\r\n                                    {\r\n                                        VoluntaryExcessAvailableAmounts = new List<int> {0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500}\r\n                                    },")]
        [TestCase("CoverOverview", "TenantConfiguration", "config.GetTenantConfiguration()")]
        [TestCase("CoverRange", "Id", "\"OverviewCoverRangeStartDate\",")]
        [TestCase("CoverRange", "ViewType", "CoverRangeViewType.StartDate,")]
        [TestCase("CoverRange", "TenantConfiguration", "config.GetTenantConfiguration()")]
        [TestCase("DriverDetailsDriverBubbles", "Id", "\"DriverBubbles\",")]
        [TestCase("DriverDetailsDriverBubbles", "JourneyType", "DriverDetailsJourneyTypes.AdditionalDriver,")]
        [TestCase("DriverDetailsDriverBubbles", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("DriverDetailsDriverBubbles", "StatusCodesToAskOccupation", "employmentStatusCodes")]
        [TestCase("VehicleDetailsList", "Id", "\"VehicleDetails\",")]
        [TestCase("VehicleDetailsList", "ManufactureListInUpperCase", "new List<string> {\"BMW\", \"MG\", \"SEAT\"},")]
        [TestCase("VehicleDetailsList", "ManufacturerKeys", "new List<string>\r\n                {\r\n                    \"FO\",\r\n                    \"VA\",\r\n                    \"VW\",\r\n                    \"PE\",\r\n                    \"NI\",\r\n                    \"BM\",\r\n                    \"R1\",\r\n                    \"CI\",\r\n                    \"TO\",\r\n                    \"AU\",\r\n                    \"ME\",\r\n                    \"HO\",\r\n                    \"FI\"\r\n                },")]
        [TestCase("VehicleDetailsList", "WrongVehicleTypeForVanInsValidationErrorMessage", "BrandConstants.WrongVehicleTypeForVanInsValidationErrorMessage,")]
        [TestCase("VehicleDetailsList", "VanErrorMessageUrl", "BrandConstants.VanErrorMessageUrl,")]
        [TestCase("VehicleDetailsList", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("VehicleDetailsList", "ModificationItemsLimit", "10,")]
        [TestCase("VehicleDetailsList", "SummaryButtonText", "\"Continue\",")]
        [TestCase("VehicleDetailsList", "ModificationButtonText", "\"Done\"")]
        [TestCase("DriverDetails", "Id", "\"DriverBubbles\",")]
        [TestCase("DriverDetails", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("DriverDetails", "StatusCodesToAskOccupation", "employmentStatusCodes")]
        [TestCase("CoverDetails", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetails", "DataStoreKey", "\"CoverDetails\"")]
        [TestCase("CoverDetails", "ExcessConfiguration", "new ExcessConfiguration\r\n                            {\r\n                                VoluntaryExcessAvailableAmounts = new List<int> { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 }\r\n                            },")]
        [TestCase("CoverDetails", "TenantConfiguration", "config.GetTenantConfiguration()")]
        [TestCase("OptionalExtras", "Id", "\"OptionalExtras\",")]
        [TestCase("OptionalExtras", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("MarketingPreferences", "Id", "\"SalesMarketingPreferences\",")]
        [TestCase("MarketingPreferences", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}