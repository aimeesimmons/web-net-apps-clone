using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class FooterTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_Footer";
            Document = GetDocument();
            Components.Add("FooterDocumentLinks", GetComponent("FooterDocumentLinks"));
        }

        [Test]
        public void FooterDocumentLinksComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["FooterDocumentLinks"]));
        }

        [TestCase("FooterDocumentLinks", "Id", "\"Footer\",")]
        [TestCase("FooterDocumentLinks", "TenantConfiguration", "tenantConfiguration")]
       public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}