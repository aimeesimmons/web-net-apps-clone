using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LayoutBasicTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_LayoutBasic";
            Document = GetDocument();
            Components.Add("GoogleTagManagerDataLayer", GetComponent("GoogleTagManagerDataLayer"));
            Components.Add("GoogleTagManagerTagManager", GetComponent("GoogleTagManagerTagManager"));
            Components.Add("GoogleTagManagerLaunchScript", GetComponent("GoogleTagManagerLaunchScript"));
        }

        [Test]
        public void GoogleTagManagerDataLayerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerDataLayer"]));
        }

        [Test]
        public void GoogleTagManagerTagManagerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerTagManager"]));
        }

        [Test]
        public void GoogleTagManagerLaunchScriptRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerLaunchScript"]));
        }

        [TestCase("GoogleTagManagerDataLayer", "QuoteTier", "\"Standard\"")]
        [TestCase("GoogleTagManagerTagManager", "TagManagerScriptUrl", "ConfigurationManager.AppSettings[\"TagManagerScriptUrl\"]")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}