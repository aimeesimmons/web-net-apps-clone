using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.PostOffice.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LayoutTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_Layout";
            Document = GetDocument();
            Components.Add("HoustonConfiguration", GetDocumentPart("var componentHoustonConfiguration = new ComponentHoustonConfiguration", "};"));
            Components.Add("GoogleTagManager", GetComponent("GoogleTagManager"));
            Components.Add("Navigation", GetComponent("Navigation"));
            Components.Add("Basket", GetComponent("Basket"));
            Components.Add("PageReloader", GetComponent("PageReloader"));
            Components.Add("Houston", GetDocumentPart("@Html.ComponentRenderHouston", "(componentHoustonConfiguration)"));
            Components.Add("GoogleTagManagerDataLayer", GetComponent("GoogleTagManagerDataLayer"));
            Components.Add("GoogleTagManagerTagManager", GetComponent("GoogleTagManagerTagManager"));
            Components.Add("GoogleTagManagerLaunchScript", GetComponent("GoogleTagManagerLaunchScript"));
        }

        [Test]
        public void HoustonConfigurationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["HoustonConfiguration"]));
        }

        [Test]
        public void GoogleTagManagerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManager"]));
        }

        [Test]
        public void NavigationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Navigation"]));
        }

        [Test]
        public void BasketComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Basket"]));
        }

        [Test]
        public void PageReloaderComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["PageReloader"]));
        }

        [Test]
        public void HoustonComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Houston"]));
        }

        [Test]
        public void GoogleTagManagerDataLayerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerDataLayer"]));
        }

        [Test]
        public void GoogleTagManagerTagManagerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerTagManager"]));
        }

        [Test]
        public void GoogleTagManagerLaunchScriptRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerLaunchScript"]));
        }

        [TestCase("HoustonConfiguration", "Id", "\"Houston\",")]
        [TestCase("HoustonConfiguration", "Brand", "vaConfig.BrandName,")]
        [TestCase("HoustonConfiguration", "Application", "vaConfig.Application,")]
        [TestCase("HoustonConfiguration", "JourneySection", "\"Purchase\",")]
        [TestCase("HoustonConfiguration", "Product", "product,")]
        [TestCase("HoustonConfiguration", "Journey", "isVan ? \"Van\" : \"Car\",")]
        [TestCase("HoustonConfiguration", "ContactDataUrl", "Url.Action(\"HoustonContactData\", \"ContactData\"),")]
        [TestCase("HoustonConfiguration", "InvocationPoint", "vaConfig.DefaultInvocationPoint,")]
        [TestCase("HoustonConfiguration", "PageNamePrefix", "\"Sales_TE_\",")]
        [TestCase("HoustonConfiguration", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("HoustonConfiguration", "IsPreQuote", "preQuote,")]
        [TestCase("HoustonConfiguration", "OnError", "Request?.Url == null || Request.Url.AbsolutePath.ToLower().Contains(\"error\"),")]
        [TestCase("GoogleTagManager", "Id", "\"GoogleTagManager\",")]
        [TestCase("GoogleTagManager", "AddOnsList", "new List<string>{\"Legal Protection\",\"No claims discount protection\",\"Price Protection Cover\",\"Guaranteed Replacement Car\",\"RAC Breakdown Assistance\",\"Keycare\",\"Personal Accident Cover\"},")]
        [TestCase("GoogleTagManager", "MarketingSourceSetupList", "new List<string> { \"comparethemarket\", \"gocompare\", \"moneysupermarket\", \"confusedcom\", \"quotezonecouk\" },")]
        [TestCase("GoogleTagManager", "GtmConfiguration", "config.GetGtmConfiguration(),")]
        [TestCase("GoogleTagManager", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("Navigation", "Id", "\"NavigationDefault\",")]
        [TestCase("Navigation", "MenuKeyPointsConfig", "new Dictionary<string, MenuItemConfig> \r\n { \r\n { \"Quote\", new MenuItemConfig  \r\n { \r\n ParentAction = Url.Action(\"Index\", \"PricePresentation\") \r\n }}, \r\n { \"Extras\", new MenuItemConfig \r\n { \r\n ParentAction = Url.Action(\"Index\", \"Extras\") \r\n }}, \r\n { \"Review\", new MenuItemConfig  \r\n { \r\n ParentAction = Url.Action(\"Index\", \"Review\"), \r\n ChildActions = new List<string> \r\n { \r\n Url.Action(\"Index\", \"AdditionalDetails\") \r\n } \r\n }}, \r\n { \"Pay\", new MenuItemConfig \r\n { \r\n ParentAction = Url.Action(\"PaymentSummary\", \"Payment\"), \r\n ChildActions = new List<string> \r\n { \r\n Url.Action(\"Annual\", \"Payment\"), \r\n Url.Action(\"AnnualSummary\", \"Payment\"), \r\n Url.Action(\"MonthlyInstalment\", \"Payment\"), \r\n Url.Action(\"MonthlyInstalmentSummary\", \"Payment\"), \r\n Url.Action(\"Unsuccessful\", \"Payment\") \r\n } \r\n }}, \r\n { \"Done\", new MenuItemConfig { ParentAction = Url.Action(\"AllDone\", \"Payment\") }} \r\n }")]
        [TestCase("Navigation", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("Basket", "Id", "\"Basket\",")]
        [TestCase("Basket", "CoverListPriority", "new List<string>{FeatureServiceConstants.CoverLevel,FeatureServiceConstants.NcdProtection,FeatureServiceConstants.PriceProtection,FeatureServiceConstants.LegalProtection,FeatureServiceConstants.GuaranteedReplacementVehicle,FeatureServiceConstants.Breakdown,FeatureServiceConstants.ToolsCover,FeatureServiceConstants.KeyCover,FeatureServiceConstants.PersonalAccidentCover},")]
        [TestCase("Basket", "AvivaCreditPhoneNumber", "\"0344 412 2122\",")]
        [TestCase("Basket", "ComponentUrl", "Url.Action(\"Basket\", \"BasketComponent\"),")]
        [TestCase("Basket", "TenantConfiguration", "config.GetTenantConfiguration()")]
        [TestCase("PageReloader", "Id", "\"PageReloader\",")]
        [TestCase("PageReloader", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("GoogleTagManagerDataLayer", "QuoteTier", "\"Standard\"")]
        [TestCase("GoogleTagManagerTagManager", "TagManagerScriptUrl", "ConfigurationManager.AppSettings[\"TagManagerScriptUrl\"]")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}