using BGL.Web.Halifax.MotorQuote;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace BGL.Web.Halifax.MotorQuote
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureRequestAccessor(app);
            ConfigureAuth(app);
        }
    }
}