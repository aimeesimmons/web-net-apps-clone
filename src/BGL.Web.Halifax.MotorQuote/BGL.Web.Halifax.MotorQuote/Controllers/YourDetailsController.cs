using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class YourDetailsController : SecuredController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AboutYou()
        {
            return View();
        }

        public ActionResult MaritalStatus()
        {
            return View();
        }

        public ActionResult Address()
        {
            return View();
        }

        public ActionResult ConfirmedAddress()
        {
            return View();
        }

        public ActionResult HomeOwner()
        {
            return View();
        }

        public ActionResult UkResident()
        {
            return View();
        }

        public ActionResult EmploymentStatus()
        {
            return View();
        }

        public ActionResult EmploymentOccupation()
        {
            return View();
        }

        public ActionResult EmploymentBusinessType()
        {
            return View();
        }

        public ActionResult PartTimeOccupation()
        {
            return View();
        }

        public ActionResult PartTimeOccupationType()
        {
            return View();
        }

        public ActionResult PartTimeBusinessType()
        {
            return View();
        }

        public ActionResult LicenceType()
        {
            return View();
        }

        public ActionResult LicenceDate()
        {
            return View();
        }

        public ActionResult LicenceNumber()
        {
            return View();
        }

        public ActionResult MedicalCondition()
        {
            return View();
        }

        public ActionResult MedicalConditionDvlaNotified()
        {
            return View();
        }

        public ActionResult DeclinedInsurance()
        {
            return View();
        }

        public ActionResult MotorClaims()
        {
            return View();
        }

        public ActionResult MotorConvictions()
        {
            return View();
        }

        public ActionResult CriminalConvictions()
        {
            return View();
        }

        public ActionResult AdditionalDrivers()
        {
            return View();
        }
    }
}