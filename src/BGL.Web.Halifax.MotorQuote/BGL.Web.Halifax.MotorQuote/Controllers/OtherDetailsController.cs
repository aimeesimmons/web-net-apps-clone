using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class OtherDetailsController : SecuredController
    {
        public ActionResult PaymentsRegularity()
        {
            return View();
        }

        public ActionResult PaymentsSchedule()
        {
            return View();
        }

        public ActionResult AndFinally()
        {
            return View();
        }
    }
}