using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class AdditionalDetailsController : SecuredController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AlmostThere()
        {
            return View();
        }
    }
}