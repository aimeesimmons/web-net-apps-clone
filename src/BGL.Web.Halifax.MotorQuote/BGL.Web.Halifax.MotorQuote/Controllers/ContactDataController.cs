﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class ContactDataController : Controller
    {
        public ActionResult HoustonContactData()
        {
            return PartialView("_HoustonContactData");
        }
    }
}