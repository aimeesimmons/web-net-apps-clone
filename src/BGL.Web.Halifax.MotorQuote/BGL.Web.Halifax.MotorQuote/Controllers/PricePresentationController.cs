using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class PricePresentationController : SecuredController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Breakdown()
        {
            return View();
        }
    }
}