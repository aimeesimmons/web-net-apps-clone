﻿using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class CoverFeaturesController : SecuredController
    {
        public ActionResult DriveOtherCars()
        {
            return PartialView();
        }

        public ActionResult CourtesyCar()
        {
            return PartialView();
        }

        public ActionResult WindscreenCover()
        {
            return PartialView();
        }

        public ActionResult DrivingAbroad()
        {
            return PartialView();
        }

        public ActionResult PersonalBelongings()
        {
            return PartialView();
        }

        public ActionResult MedicalExpenses()
        {
            return PartialView();
        }

        public ActionResult PersonalAccidentBenefits()
        {
            return PartialView();
        }

        public ActionResult ReplacementLocks()
        {
            return PartialView();
        }

        public ActionResult AudioEquipment()
        {
            return PartialView();
        }
    }
}