using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class PaymentController : SecuredController
    {
        public ActionResult PaymentSummary()
        {
            return View();
        }

        public ActionResult Annual()
        {
            return View();
        }

        public ActionResult AnnualSummary()
        {
            return View();
        }

        public ActionResult MonthlyInstalment()
        {
            return View();
        }

        public ActionResult MonthlyInstalmentSummary()
        {
            return View();
        }

        public ActionResult MonthlyDeposit()
        {
            return View();
        }

        public ActionResult MonthlyDepositSummary()
        {
            return View();
        }

        public ActionResult AllDone()
        {
            return View();
        }

        public ActionResult Unsuccessful()
        {
            return View();
        }

        public ActionResult PaymentSuccessful()
        {
            return View();
        }

        public ActionResult PolicyCreationPending()
        {
            return View();
        }
    }
}