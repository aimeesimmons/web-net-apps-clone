using System.Web.Mvc;
using BGL.Security.Web.Authentication.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Controllers
{
    public class DocumentDownloadController : SecuredController
    {
        public ActionResult Document()
        {
            return View();
        }
    }
}