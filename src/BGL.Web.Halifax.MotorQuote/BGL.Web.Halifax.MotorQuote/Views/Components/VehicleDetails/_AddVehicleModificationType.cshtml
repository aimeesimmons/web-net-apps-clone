﻿@using System.Web.Mvc.Html
@using BGL.Components.VehicleDetails.Enums
@model BGL.Components.VehicleDetails.Models.AddModificationViewModel

@{
    var componentId = Model.ComponentConfiguration.Id;
    var url = Url.Action("AddModification", "VehicleDetailsComponentModifications");
}

<div class="narrow">
    <div class="step-form">

        <h1 class="step-form__section-title">About your @Model.VehicleType</h1>
        <h2 class="vehicle-modification__builder-header">What type of modifications does your @Model.VehicleType have?</h2>

        @Html.HiddenFor(m => m.VehicleId)
        @Html.Hidden("JourneyType", (int)Model.JourneyType)
        @Html.HiddenFor(m => m.DataStoreKey)

        <div class="step-form__button-wrapper">
            @foreach (var modification in Model.List)
            {
                <button type="button" class="button--radio vehicle-lookup__button step-form__button vehicle-builder-button" data-button-name="modificationTypeCode" data-url="@url" data-value="@modification.Code" data-testid="@(componentId)__modification-type">@modification.Description</button>
            }
        </div>

        <div class="page-section--inner">
            <div class="button-container">
                <a class="button--text button--large button--back button--left vehicle-builder-back-button" data-step-type="previous-step"
                        data-url="@Model.BackButtonUrl" data-back-option="modification" href="#" data-testid="@(componentId)__step-back-button">
                    Back
                </a>
            </div>
        </div>
    </div>
</div>
