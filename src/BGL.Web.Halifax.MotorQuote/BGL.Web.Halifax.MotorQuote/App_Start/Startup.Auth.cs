using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace BGL.Web.Halifax.MotorQuote
{
    [ExcludeFromCodeCoverage]
    public partial class Startup
    {
        public const string XmlHttpRequest = "XMLHttpRequest";
        public const string XRequestedWith = "X-Requested-With";

        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            var cookieProvider = new CookieAuthenticationProvider
            {
                OnApplyRedirect = context =>
                {
                    if (!IsAjaxRequest(context.Request))
                    {
                        var isErrorPage = context.Request.Uri.AbsolutePath.Contains(ConfigurationManager.AppSettings["ErrorRedirectUrl"]);

                        if (!isErrorPage)
                        {
                            context.Response.Redirect(ConfigurationManager.AppSettings["SessionTimeoutRedirectUrl"]);
                        }
                    }
                }
            };

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                CookieName = "ApplicationCookie",
                LoginPath = new PathString("/Error"),
                SlidingExpiration = true,
                Provider = cookieProvider,
                ExpireTimeSpan = TimeSpan.FromMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["SessionExpiryInMinute"]))
            });
        }

        private static bool IsAjaxRequest(IOwinRequest request)
        {
            var query = request.Query;
            if (query != null && string.Equals(query[XRequestedWith], XmlHttpRequest))
            {
                return true;
            }

            var headers = request.Headers;
            return headers != null && string.Equals(headers[XRequestedWith], XmlHttpRequest);
        }
    }
}