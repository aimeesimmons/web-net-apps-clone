using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Web.Optimization;
using BGL.Web.Halifax.MotorQuote.App_Start;

namespace BGL.Web.Halifax.MotorQuote
{
    [ExcludeFromCodeCoverage]
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                "~/Content/dist/scripts/jquery/jquery.js",
                "~/Content/dist/scripts/jquery-ui/jquery-ui.js",
                "~/Content/dist/scripts/jquery-validate/jquery.validate*",
                "~/Content/dist/scripts/modernizr/modernizr-{version}.js"));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/dist/css/brand.css", new CssUrlRewritter()));
            
            if (bool.TryParse(ConfigurationManager.AppSettings["BundlingEnabled"], out var bundlingEnabled) && bundlingEnabled)
            {
                bundles.Add(new ScriptBundle("~/bundles/bgl")
                    .Include("~/Content/dist/scripts/bgl/bgl.common.js")
                    .Include("~/Content/dist/scripts/bgl.components.*"));
            }

            EnableMinification();
        }

        private static void EnableMinification()
        {
            if (bool.TryParse(ConfigurationManager.AppSettings["MinificationEnabled"], out var minificationEnabled) && minificationEnabled)
            {
                BundleTable.EnableOptimizations = true;
            }
        }
    }
}