﻿var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var del = require('del');
var merge = require('gulp-merge-json');

var paths = {
    src: {
        common_css: './Content/src/common/sass/**/*.+(scss|sass)',
        common_scripts: './Content/src/common/scripts/**/*.js',
        common_font_icons: './Content/src/common/fonts/icons/*',
        brand_fonts: './Content/src/brand/fonts/**/*',
        brand_images: './Content/src/brand/images/**/*.+(png|svg|jpg|gif|ico)',
        brand_sass: './Content/src/brand/sass/**/*.+(scss|sass)',
        components_scripts: './Content/src/components/scripts/**/*.js',
        components_api: './Api/**/*.json',
        ping: './content/src/brand/ping/*.js'
        },
    dest: {
        css: './Content/dist/css',
        fonts: './Content/dist/fonts',
        font_icons: './Content/dist/fonts/icons',
        images: './Content/dist/images',
        root_dir: './Content/dist/',
        scripts: './Content/dist/scripts',
        ping: './content/dist/ping'
    }
};

function clean() {
	return del([paths.dest.root_dir], {
		force: true
	});
}

function images() {
	return gulp.src([paths.src.brand_images])
		.pipe(gulp.dest(paths.dest.images));
}

function fonts() {
	return gulp.src([paths.src.brand_fonts])
		.pipe(gulp.dest(paths.dest.fonts));
}

function fontIcons() {
    return gulp.src([paths.src.common_font_icons])
        .pipe(gulp.dest(paths.dest.font_icons));
}

function scripts() {
	return gulp.src([
			paths.src.common_scripts,
			paths.src.components_scripts
		])
		.pipe(gulp.dest(paths.dest.scripts));
}

function ping() {
    return gulp.src([paths.src.ping])
        .pipe(gulp.dest(paths.dest.ping));
}

function apis() {
    return gulp.src([paths.src.components_api])
		.pipe(merge({
			fileName: 'api.json',
			edit: (parsedJson, file) => {
                Object.keys(parsedJson).forEach((value, idx) => {
                    if (parsedJson[value].hasOwnProperty('testcase')) {
                        delete parsedJson[value].testcase;
                    }
                });
				return parsedJson;
			}
		})).pipe(gulp.dest('./'));
}

function compileSass() {
	return gulp.src(paths.src.brand_sass)
		.pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(paths.dest.css));
}

exports.build = gulp.series(clean, images, fonts, fontIcons, scripts, apis, compileSass, ping);