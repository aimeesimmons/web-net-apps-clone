﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.additionaldetails = bgl.components.additionaldetails || {};

bgl.components.additionaldetails.paymentsregularity = (function () {
  function initComponent() {
    var formId = 'payments-regularity-form';
    var form = $('#' + formId);
    var radioButtons = $('#payments-preference-monthly,#payments-preference-annually');
    var continueButton = $('#payments-regularity-continue-button');

    bgl.common.datastore.init(form, false, true);
    bgl.common.datastore.setupReadAndWrite();

    var redirectForwardSelector;
    if (radioButtons.is(':checked')) {
      continueButton.removeClass('hide');
      redirectForwardSelector = continueButton;
    } else {
      redirectForwardSelector = radioButtons;
    }

    redirectForwardSelector.on('click', function () {
      if (bgl.common.pubsub !== 'undefined') {
        bgl.common.pubsub.emit('fakeFormSubmit', formId);
      }
      bgl.common.utilities.redirectToUrl(continueButton.data('next-step-url'));
    });

    form.find('label').on('keyup', bgl.common.keyboardAccessibility.generateClickOnKeypress);
  }

  return {
    init: initComponent
  };
})();