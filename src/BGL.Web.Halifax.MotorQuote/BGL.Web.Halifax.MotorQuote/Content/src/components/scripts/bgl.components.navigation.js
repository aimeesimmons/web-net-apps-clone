﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.navigation = (function () {

    var configuration;

    var pagesWithAsyncComponents = [];

    var pagesWithoutBackAction = [];

    var pathName;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(configuration, componentUrl, containerElementId, function () {
            bgl.components.navigation.init(configuration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
    }

    function initBackButtonFromOopsPage(listOfPagesWithoutBackAction, listOfPagesWithAsyncComponents) {

        pagesWithAsyncComponents = listOfPagesWithAsyncComponents || [];

        pagesWithoutBackAction = listOfPagesWithoutBackAction || [];

        var location = bgl.common.utilities.getWindowLocation();

        pathName = location.pathname;

        var result = isPagesWithoutBackAction(pathName);

        if (result === true) {
            sessionStorage.setItem("hideBackButton", true);
        }

        $(document).ready(function () {
            sessionStorage.setItem("hideBackButton", isPagesWithoutBackAction(pathName));

            var index = jQuery.inArray(pathName, pagesWithAsyncComponents);

            if ((index > -1) === false) {
                sessionStorage.setItem("loaded_page", pathName);
            }
        });
    }

    function showBackButtonOnOopsPage(listOfPagesWithoutBackAction, failedAction) {

        pagesWithoutBackAction = listOfPagesWithoutBackAction || [];

        pathName = decodeURIComponent(failedAction);

        $(document).ready(function () {
            var backLink = $("a[data-role='back-button-from-error']");

            var hideBackButtonValue = sessionStorage.getItem("hideBackButton");

            var hideBackButton = hideBackButtonValue === null || hideBackButtonValue === "true" || isPagesWithoutBackAction(pathName);

            if (hideBackButton) {
                backLink.hide();
            } else {
                var pageAction = sessionStorage.getItem("loaded_page");
                backLink.attr("href", pageAction);
            }
        });
    }

    function isPagesWithoutBackAction(pathName) {
        var index = pagesWithoutBackAction.length > 0 ? jQuery.inArray(pathName, pagesWithoutBackAction) : -1;

        return (index > -1) === true;
    }

    return {
        init: initComponent,
        load: loadComponent,
        initBackButtonFromOopsPage: initBackButtonFromOopsPage,
        showBackButtonOnOopsPage: showBackButtonOnOopsPage
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.navigation;
}