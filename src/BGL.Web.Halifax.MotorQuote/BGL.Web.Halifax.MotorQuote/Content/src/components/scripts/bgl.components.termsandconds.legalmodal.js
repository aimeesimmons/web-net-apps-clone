﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.termsandconds = bgl.components.termsandconds || {};

bgl.components.termsandconds.legalmodal = (function () {

    var modal, overlay, isRead, modalContent;

    function initComponent(componentConfiguration) {
        modal = $('#legal-modal');
        modalContent = $('#legal-model-content');
        overlay = $('#overlay');
        isRead = false;
        setUpEventBindings();
        if (!termsAndConditionsHaveBeenAccepted()) {
            showModal();
        }
    }

    function setUpEventBindings() {
        accordionEventBindings();
        dismissalEventBindings();
        scrollEventBindings();
    }

    function scrollEventBindings() {
        var modalContent = modal.find(".modal-body");

        modalContent.on('scroll',
            function () {
                if (isElementScrolledToBottom($(this))) {
                    isRead = true;
                    enableContinueButton();
                    hideErrorMessage();
                }
            });
    }

    function isElementScrolledToBottom(element) {
        var heightOfTheScrollingContent = element[0].scrollHeight;
        var heightOfTheElementContainingScrollingContent = element.innerHeight();
        var amountToScroll = heightOfTheScrollingContent - heightOfTheElementContainingScrollingContent;
        var amountCurrentlyScrolled = element.scrollTop();
        
        var distanceFromBottom = amountToScroll - amountCurrentlyScrolled;

        return distanceFromBottom < 25;
    }

    function accordionEventBindings() {
        var accordions = $(".legal-accordion__heading-button");

        accordions.on("click", function () {
            toggleAccordion($(this));
        });
    }

    function toggleAccordion(accordionButton) {
        var accordionSection = accordionButton.closest(".accordion__section");
        var accordionContent = accordionSection.find(".accordion__body-wrapper");

        if (accordionContent.hasClass("hide")) {
            accordionContent.removeClass("hide");
        } else {
            accordionContent.addClass("hide");
        }
    }

    function dismissalEventBindings() {
        var continueButton = $("#close-modal");

        $(continueButton).on("click",
            function () {
                dismissOrShowError();
            });

        $("body").on("click",
            function (event) {
                var wasModalClicked = $(event.target).closest("#legal-model-content").is(modalContent);

                if (!wasModalClicked) {
                    dismissOrShowError();
                }
            });
    }

    function dismissOrShowError() {
        if (isRead) {
            addTermsAndConditionsAcceptedInSessionStorage();
            dismissModal();
        } else {
            showErrorMessage();
        }
    }

    function showErrorMessage() {
        var modalFooter = $(".modal-footer");
        var errorText = modalFooter.find("p.error");
        modalFooter.addClass("modal-footer--error");
        errorText.addClass("show");
    }

    function hideErrorMessage() {
        var modalFooter = $(".modal-footer");
        var errorText = modalFooter.find("p.error");
        errorText.removeClass("show");
        modalFooter.removeClass("modal-footer--error");
    }

    function enableContinueButton() {
        var closeButton = $("#close-modal");
        closeButton.removeClass("button--disabled");
        closeButton.addClass("button--primary");
    }

    function showModal() {
        overlay.addClass("show");
        overlay.removeClass("hide");
        modal.addClass("show");
        disableScrolling();
    }

    function dismissModal() {
        modal.fadeOut(800);
        overlay.fadeOut(800);
        modal.removeClass("show");
        overlay.removeClass("show");
        enableScrolling();
    }

    function disableScrolling() {
        $('body').addClass('modal-open');
    }

    function enableScrolling() {
        $('body').removeClass('modal-open');
    }

    function addTermsAndConditionsAcceptedInSessionStorage() {
        sessionStorage.setItem("T&CAccepted", true);
    }

    function termsAndConditionsHaveBeenAccepted() {
        var sessionStorageValue = sessionStorage.getItem("T&CAccepted");

        return sessionStorageValue == "true";
    }

    return {
        init: initComponent
    };

})();

