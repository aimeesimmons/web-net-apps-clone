﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.mergedpaymentsummary = (function () {
    var componentConfig;

    function initComponent(componentConfiguration) {
        componentConfig = componentConfiguration;

        $(".payment-switch-link").off('click').on("click",
            function () {
                clearTimeout(sessionStorage.getItem("3dSecureTimeoutId"));

                var paymentSwitchLinkId = $(this).attr("id");
                bgl.common.focusHolder.setFocusedElement($(this).data("focusid"));

                var dataToPost = {
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name="__RequestVerificationToken"]').val(),
                    paymentSwitchId: $(this).data("payment-switch-id")
                };
                var url = $(this).data('url');
                var paymentContentUrl = $(this).data('payment-content-url');

                $.ajax({
                    url: url,
                    type: "POST",
                    cache: false,
                    dataType: "json",
                    data: dataToPost,
                    success: function (result) {
                        if (result.isSuccess === true) {
                            $("#merged-payment-content").empty();
                            var isMonthly = paymentSwitchLinkId === "monthly-switch-link";

                            $("#IsMonthlyPayment").val(isMonthly);
                            if (isMonthly) {
                                bgl.components.takepayment.monthlyinstalment.load(componentConfig,
                                    paymentContentUrl,
                                    "merged-payment-content",
                                    function () {
                                        $("#monthly-section").removeClass("hide");
                                        $("#annual-section").addClass("hide");

                                        bgl.common.pubsub.emit("showNotification", $("#monthly-notification-message").val());
                                    });

                            } else {
                                bgl.components.takepayment.annual.load(componentConfig,
                                    paymentContentUrl,
                                    "merged-payment-content",
                                    function () {
                                        $("#monthly-section").addClass("hide");
                                        $("#annual-section").removeClass("hide");

                                        bgl.common.pubsub.emit("showNotification", $("#annual-notification-message").val());
                                    });
                            }

                            bgl.common.pubsub.emit("switchPaymentType", isMonthly);
                        } else {
                            window.location.href = result.errorRedirectUrl;
                        }
                    }
                });
            });
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.mergedpaymentsummary;
}
