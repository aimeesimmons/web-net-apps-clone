﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.pagereloader = bgl.components.pagereloader || {};
bgl.components.pagereloader.deletebasket = (function () {
    var applicationPath = '';
    function initComponent(componentConfiguration) {
        applicationPath = componentConfiguration.ApplicationPath;
        showSpinner();
        deleteBasket();
    }
    function showSpinner() {
        var spinnerHtml = bgl.common.spinner.getSpinnerBody("Working on it",
            "We won't be a moment");
        $('#spinner').html(spinnerHtml);
    }
    function deleteBasket() {
        var token = $('input[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: applicationPath + "PageReloaderComponent/DeleteBasket",
            type: 'POST',
            data: {
                __RequestVerificationToken: token
            },
            success: function () {
                goToSuccessPage();
            },
            error: function () {
                goToErrorPage();
            }
        });
    }
    function goToSuccessPage() {
        var attemptToContinue = getUrlParameter("startAgain");
        if (attemptToContinue && attemptToContinue.toLocaleLowerCase().trim() === "true") {
            var startPage = sessionStorage.getItem("MTAStartPage");
            if (startPage) {
                window.location = startPage;
                return;
            }
        }
        window.location = applicationPath + 'PolicyOverview/Vehicle';
    }
    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    function goToErrorPage() {
        window.location = applicationPath + 'Error';
    }
    return {
        init: initComponent
    };
})();
if (typeof module !== 'undefined') {
    module.exports = bgl.components.pagereloader;
}