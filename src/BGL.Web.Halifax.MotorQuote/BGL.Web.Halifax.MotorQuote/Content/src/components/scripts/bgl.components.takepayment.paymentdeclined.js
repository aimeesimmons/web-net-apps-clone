﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymentdeclined = (function () {

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent();
            });
    }

    function initComponent() {
        clearSession();
    }

    function clearSession() {
        clearAnnualSessionData();
        clearMonthlyDepositSessionData();
        clearMonthlyInstalmentSessionData();
        clearOneOffPaymentSessionData();
    }

    function clearAnnualSessionData() {
        sessionStorage.removeItem("AnnualSelectedPersonIdText");
        sessionStorage.removeItem("AnnualCardCaptureUri");
        sessionStorage.removeItem("AnnualSecureSessionId");
        sessionStorage.removeItem("AnnualSelectedPersonIdVal");
    }

    function clearMonthlyDepositSessionData() {
        sessionStorage.removeItem("MonthlyDepositSelectedPersonIdText");
        sessionStorage.removeItem("MonthlyDepositCardCaptureUri");
        sessionStorage.removeItem("MonthlyDepositSecureSessionId");
        sessionStorage.removeItem("MonthlyDepositSelectedPersonIdVal");
    }

    function clearMonthlyInstalmentSessionData() {
        sessionStorage.removeItem("MonthlyInstalmentCardSelectedPersonIdText");
        sessionStorage.removeItem("MonthlyInstalmentCardCaptureUri");
        sessionStorage.removeItem("MonthlyInstalmentSecureSessionId");
        sessionStorage.removeItem("MonthlyInstalmentCardSelectedPersonIdVal");
    }

    function clearOneOffPaymentSessionData() {
        sessionStorage.removeItem("OneOffPaymentSelectedPersonIdText");
        sessionStorage.removeItem("OneOffPaymentCardCaptureUri");
        sessionStorage.removeItem("OneOffPaymentSecureSessionId");
        sessionStorage.removeItem("OneOffPaymentSelectedPersonIdVal");
    }
  
    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentdeclined;
}
