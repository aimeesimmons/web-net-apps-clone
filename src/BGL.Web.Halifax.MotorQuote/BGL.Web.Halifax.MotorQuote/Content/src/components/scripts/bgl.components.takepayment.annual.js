﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.annual = (function () {
    var componentConfiguration;
    var hideElementsWhen3DSecureIsDisplayedTimeout;

    function loadComponent(componentConfiguration, componentUrl, containerElementId, callback) {
        componentConfiguration.CardSessionData = getSecureSessionData();
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);

                if (typeof callback === 'function') {
                    callback();
                }
            });
    }

    function initComponent(configuration) {
        componentConfiguration = configuration;

        bgl.common.datastore.setupReadAndWrite();

        loadIframe();

        setVisibilityOfCardholderNameOther();

        bgl.common.validator.enableMaxLength();

        $("#AnnualPaymentForm").off("submit").on("submit",
            function (e) {
                e.preventDefault();
            });

        bgl.common.validator.init($("#AnnualPaymentForm"),
            {
                onkeyup: function (element) {
                    ValidateElementAndToggleIframe(element);
                },
                hasSubmit: false
            });

        $("#Card-Capture-Iframe").on("load",
            function () {              
                hideElementsWhen3DSecureIsDisplayed();
            });

        $("#PersonListData_SelectedPersonId").change(
            function () {
                if (IsOtherSelected()) {
                    $("#CardholderNameRow").removeClass("hide");
                    $("#Card-Capture-Iframe").addClass("hide");
                    sessionStorage.setItem("AnnualSelectedPersonIdText", "");
                    $("#CardholderName").val("");
                    $("#CardholderName").focus();
                } else {
                    $("#CardholderNameRow").addClass("hide");
                    $("#Card-Capture-Iframe").removeClass("hide");
                }

                initCardCapture();
            });

        if (IsOtherSelected()) {
            ValidateElementAndToggleIframe($("#AnnualPaymentForm"));
        }
    }

    function initCardCapture() {
        sessionStorage.removeItem("AnnualCardCaptureUri");
        sessionStorage.removeItem("AnnualSecureSessionId");
        sessionStorage.removeItem("AnnualThreeDSecureUri");

        if (hideElementsWhen3DSecureIsDisplayedTimeout) {
            clearTimeout(hideElementsWhen3DSecureIsDisplayedTimeout);
        }

        bgl.components.takepayment.cardcaptureiframe.clearIframe();

        componentConfiguration.CardSessionData = { "SelectedPersonId": $("#PersonListData_SelectedPersonId").val() };

        var dataToPost = {
            __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
            configuration: componentConfiguration
        };

        $.ajax({
            url: componentConfiguration.ApplicationPath + "TakePaymentComponent/InitialiseAnnualCardCapture",
            type: "POST",
            data: dataToPost,
            success: function (data) {
                sessionStorage.setItem("AnnualCardCaptureUri", data.cardCaptureUri);
                sessionStorage.setItem("AnnualSecureSessionId", data.secureSessionId);
                sessionStorage.setItem("AnnualThreeDSecureUri", data.threeDSecureUri);

                loadIframe();
            }
        });
    }

    function loadIframe() {
        var cardSessionData = getSecureSessionData();
        bgl.components.takepayment.cardcaptureiframe.loadIframe(
            'Card-Capture-Iframe',
            cardSessionData.CardCaptureUri,
            cardSessionData.SecureSessionId,
            cardSessionData.ThreeDSecureUri);
    }

    function ValidateElementAndToggleIframe(element) {
        var valid = $(element).valid();
        if (valid === true) {
            $("#Card-Capture-Iframe").removeClass("hide");
        } else {
            $("#Card-Capture-Iframe").addClass("hide");
        }
    }

    function IsOtherSelected() {
        return $("#PersonListData_SelectedPersonId").find(":selected").text() === "Other";
    }

    function getSecureSessionData() {
        return {
            "Name": sessionStorage.getItem("AnnualSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("AnnualCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("AnnualSecureSessionId"),
            "SelectedPersonId": sessionStorage.getItem("AnnualSelectedPersonIdVal"),
            "ThreeDSecureUri": sessionStorage.getItem("AnnualThreeDSecureUri")
        };
    }

    function setVisibilityOfCardholderNameOther() {
        if ($("#PersonListData_SelectedPersonId").find(":selected").text() === "Other") {
            $("#CardholderNameRow").removeClass("hide");
            $("#CardholderName").focus();
        }
    }

    function hideElementsWhen3DSecureIsDisplayed() {
        if ($("#Card-Capture-Iframe").contents().find("#continue").length === 0) {
            $("#AnnualCardholderNameFormRow").hide();
            $("#CardholderNameRow").hide();
            scrollToCardPayment();
            return;
        }

        hideElementsWhen3DSecureIsDisplayedTimeout = setTimeout(function () {
            hideElementsWhen3DSecureIsDisplayed();
        }, 1000);
    }

    function scrollToCardPayment() {
        var requiredField = $('.card-payment');
        $('html,body').animate({ scrollTop: requiredField.offset().top }, 0);
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();