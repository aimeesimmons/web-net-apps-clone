﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

bgl.components.journeyTypesVehicleDetails = {
    SingleEdit: "0",
    LookupEdit: "1",
    MultiStepEdit: "2"
};

bgl.components.vehicledetails.common = (function () {

    function getPreviouslyStoredChangedVehicleData() {
        var data = [];

        if (sessionStorage.getItem("ChangedVehicleData") !== null) {
            data = JSON.parse(sessionStorage.getItem("ChangedVehicleData"));
        }

        return data;
    }

    function setPreviouslyStoredChangedVehicleData(data) {
        sessionStorage.setItem("ChangedVehicleData", JSON.stringify(data));
    }

    return {
        getPreviouslyStoredChangedVehicleData: getPreviouslyStoredChangedVehicleData,
        setPreviouslyStoredChangedVehicleData: setPreviouslyStoredChangedVehicleData
    };
})();

bgl.components.vehicledetails.list = (function () {

    var configuration,
        ENTER_KEYCODE = 13,
        SPACE_KEYCODE = 32,
        containerId;

    var vehicleDetailsKeys = {
        vehicleModifications: "add-vehicle-modifications",
        overnightParking: "overnightParking",
        dateOfPurchase: "purchaseDate",
        assumedData: "assumedData",
        vehicleSpecification: "vehicleSpecification",
        registrationNumber: "registrationNumber",
        vanBodyType: "vanBodyType",
        numberOfSeats: "numberOfSeats",
        vehicleValue: "value"
    };

    function initComponent(componentConfiguration, containerElementId) {
        configuration = componentConfiguration;
        containerId = containerElementId;

        var editButton = $(".vehicle-details_edit-button");
        var yesButton = $("#Modification-Yes");
        var noButton = $("#Modification-No");
        var section = $("#Vehicle-Modification-List");

        bgl.common.accordions.init();

        editButton.off()
            .on({
                'keyup': function (event) {

                    var code = event.charCode || event.keyCode;
                    var link = $(this);

                    if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                        link.click();
                    }
                },
                'click': function (event) {
                    event.preventDefault();

                    var link = $(this);
                    var containerSelector = link.data("container");
                    var editActionKey = link.data("edit-action-key");
                    var actionUrl = link.data("action-url");
                    var vehicleId = link.data("vehicle-id");
                    var vehicleType = link.data("vehicle-type");
                    var storeKey = link.data("store-key");
                    var data = {
                        ComponentConfiguration: configuration,
                        EditActionKey: editActionKey,
                        VehicleId: vehicleId,
                        VehicleType: vehicleType
                    };

                    loadComponentByActionKey(data, containerSelector, actionUrl, storeKey);
                }
            });

        noButton.on('click', function () {
            section.hide();
        });

        yesButton.on('click', function () {
            section.show();
        });

        var triggerKey = 'VehicleDetails__VehicleDetails__ClickEditVehicle';
        var trigger = sessionStorage.getItem(triggerKey);
        if (trigger === 'true') {
            sessionStorage.removeItem(triggerKey);
            $('#' + configuration.Id + '_vehicleSpecification__edit-button').click();
        }
    }

    function loadComponentByActionKey(data, selector, actionUrl, storeKey) {

        switch (data.EditActionKey) {
            case vehicleDetailsKeys.vehicleSpecification:
                sessionStorage.removeItem(storeKey);

                bgl.components.vehicledetails.searching.load(data,
                    actionUrl,
                    selector
                );
                break;

            case vehicleDetailsKeys.dateOfPurchase:
                bgl.components.vehicledetails.purchasedate.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.vehicleModifications:
                bgl.components.vehicledetails.modifications.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.assumedData:
                bgl.components.vehicledetails.editassumeddata.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.overnightParking:
                bgl.components.overnightparking.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.registrationNumber:
                bgl.components.vehicledetails.registrationnumber.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.vanBodyType:
                bgl.components.vehicledetails.vehiclebodytype.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.numberOfSeats:
                bgl.components.vehicledetails.numberofseats.load(
                    data,
                    actionUrl,
                    selector);
                break;

            case vehicleDetailsKeys.vehicleValue:
                bgl.components.vehicledetails.value.load(
                    data,
                    actionUrl,
                    selector);
                break;

            default:
                $("#" + selector).empty();
                break;
        }
    }

    return {
        init: initComponent,
        vehicleDetailsKeys: vehicleDetailsKeys
    };
})();

bgl.components.overnightparking = (function () {

    var configuration;
    var form;

    function loadComponent(model, componentUrl, containerElementId) {
        configuration = model.ComponentConfiguration;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.overnightparking.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {
        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));

                    storeVehicleChangesForTagManager();

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function storeVehicleChangesForTagManager() {
        var data = bgl.components.vehicledetails.common.getPreviouslyStoredChangedVehicleData();
        var vehicleOvernightParking = getSelectedOvernightParkingValue();

        if (vehicleOvernightParking) {
            data.push(["VehicleOvernightParking", vehicleOvernightParking]);
            bgl.components.vehicledetails.common.setPreviouslyStoredChangedVehicleData(data);
        }
    }

    function getSelectedOvernightParkingValue() {
        var checkedOvernightParkingRadioButtonId = $('input[name=OvernightParkingCode]:checked').attr('id');
        var valueForCheckedOvernightParkingRadioButton = $(`label[for="${checkedOvernightParkingRadioButtonId}"]`).html();

        return valueForCheckedOvernightParkingRadioButton;
    }

    function onSuccess(configuration, data) {
        bgl.common.datastore.removeSessionStorage(form);

        if (configuration && configuration.IsService) {
            if (configuration.IsInitialChange) {
                if (data.hasChanges === true) {
                    sessionStorage.setItem("MTAStartPage", window.location.href);
                    bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                } else {
                    bgl.common.contentpanel.hideSlidePanel();
                }
            } else {
                bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
            }
        } else {

            if (data.hasChanges === true) {
                bgl.common.pubsub.emit("PollBasket");
            }

            $('[data-dismiss="slide-panel"]').click();
        }
    }

    function onError(configuration) {
        bgl.components.overnightparking.init(configuration);
    }

    return {
        load: loadComponent,
        init: initComponent,
        onSuccess: onSuccess,
        storeVehicleChangesForTagManager: storeVehicleChangesForTagManager,
        getSelectedOvernightParkingValue: getSelectedOvernightParkingValue,
        onError: onError
    };
})();

bgl.components.vehicledetails.registrationnumber = (function () {

    var configuration;
    var form;
    var containerId;

    function loadComponent(model, componentUrl, containerElementId) {
        configuration = model.ComponentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.registrationnumber.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();

        subscribeToEvents();
    }

    function subscribeToEvents() {
        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                var inputField = $("#registration-number");
                inputField.val(inputField.val().replace(/\s+/gi, ""));

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        $("a[data-load-change-vehicle]").one("click",
            function (event) {

                event.preventDefault();

                var link = $(event.currentTarget);
                var containerSelector = link.data("container");

                var onEventName = link.data("init-event");
                var callback = eval(onEventName);
                var vehicleId = link.data("vehicle-id") || bgl.common.datastore.getData(form).VehicleId;

                var model = {
                    ComponentConfiguration: configuration,
                    vehicleId: vehicleId
                };

                if (typeof callback === "function") {
                    bgl.common.loader.load(model,
                        link.data("action-url"),
                        containerSelector,
                        function () {
                            callback(configuration);
                        });
                }
            });

        $("#registration-number").on("input",
            function () {

                var start = this.selectionStart;
                var end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });
    }

    function onSuccess(configuration, data) {
        var model = data.content;
        model.ComponentConfiguration = configuration;

        bgl.components.vehicledetails.registrationnumberinformation.load(model,
            data.componentUrl,
            containerId);
    }

    function onError(configuration) {
        bgl.components.vehicledetails.registrationnumber.init(configuration);
    }

    return {
        load: loadComponent,
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.vehicledetails.registrationnumberinformation = (function () {

    var configuration;
    var form;

    function loadComponent(model, componentUrl, containerElementId) {
        configuration = model.ComponentConfiguration;

        bgl.common.loader.load(model,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.vehicledetails.registrationnumberinformation.init(configuration);
            });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.validator.init(form);

        bgl.common.datastore.init(form);

        subscribeToEvents();
    }

    function subscribeToEvents() {
        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                if (!$("#agreement-checkbox").prop("checked")) {
                    $("#agreement-container").addClass("error");
                } else {
                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));

                        bgl.common.validator.submitForm($(this), configuration);
                    }
                }
            });

        $("a[data-step-type='previous-step']").off("click").one("click",
            function (event) {

                event.preventDefault();

                var link = $(event.currentTarget);
                var containerSelector = link.data("container");
                var url = link.data("action-url");

                var data = bgl.common.datastore.getData(form);

                var model = {
                    ComponentConfiguration: configuration,
                    VehicleId: link.data("vehicle-id"),
                    RegistrationNumber: data.RegistrationNumber
                };

                bgl.components.vehicledetails.registrationnumber.load(model, url, containerSelector);
            });
    }

    function onSuccess(configuration, data) {
        bgl.common.datastore.removeSessionStorage(form);

        if (configuration.IsInitialChange) {
            if (data.hasChanges === true) {
                sessionStorage.setItem("MTAStartPage", window.location.href);
                bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
            } else {
                $('[data-dismiss="slide-panel"]').click();
            }
        } else {
            bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
        }
    }

    function onError(configuration) {
        bgl.components.vehicledetails.registrationnumberinformation.init(configuration);
    }

    return {
        load: loadComponent,
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.vehicledetails;
}
