﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.pagereloader = bgl.components.pagereloader || {};

bgl.components.pagereloader.loader = (function () {

    var applicationPath = '';

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {

        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        applicationPath = componentConfiguration.ApplicationPath;
        eventListener();
    }

    function eventListener() {
        bgl.common.pubsub.on('PollBasket', function (options) {
            pollBasket(options);
        });
    }

    function pollBasket(options) {

        var interval = 500;
        var timeoutDuration = 30000;
        var pollUrl = 'PageReloaderComponent/IsUnderwritingComplete';
        var tickCount = new Date().getTime();
        var timeout = tickCount + timeoutDuration;
        bgl.components.pagereloader.poller.poll(timeout, interval, false, pollUrl, applicationPath, options);
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

bgl.components.pagereloader.poller = (function () {

    var notificationObj = {
        cat: "underwriting",
        msg: {
            annual:
                "OK ##FIRSTNAME##, your new price is £##PRICE##. The underwriter for your policy is ##UNDERWRITER##.",
            monthly:
                "OK ##FIRSTNAME##, your new price is £##PRICE## a month with a deposit of £##DEPOSIT##. The underwriter for your policy is ##UNDERWRITER##."
        }
    };

    function poll(timeout, interval, isSpinnerVisible, pollUrl, applicationPath, options) {
        options = options || {};

        if (options.hideSpinnerOnInitialAttempt) {
            options.hideSpinnerOnInitialAttempt = false;
        } else {
            bgl.components.pagereloader.poller.showSpinner(isSpinnerVisible, options.spinnerName);
            isSpinnerVisible = true;
        }

        $.ajax({
            url: applicationPath + pollUrl,
            type: 'Get',
            cache: false,
            dataType: 'json',
            success: function (response) {
                if (response.IsSuccess === true) {
                    if (response.statusCode === 202) {
                        if (timeout >= new Date().getTime()) {
                            setTimeout(function () {
                                poll(timeout, interval, isSpinnerVisible, pollUrl, applicationPath, options);
                            }, interval);
                        }
                        else {
                            bgl.components.pagereloader.poller.goToUnderwritingTimeOutPage(applicationPath);
                        }
                    } else if (response.statusCode === 400) {
                        removeNotificationFromSessionStorage();
                        bgl.components.pagereloader.poller.goToUnableToQuoteOnlinePage(applicationPath);
                    }
                    else {
                        if (options.hideNotification) {
                            removeNotificationFromSessionStorage();
                        }
                        bgl.components.pagereloader.cleanaddons.clearSessionStorage();
                        bgl.components.pagereloader.poller.reloadPage(options.callback, options.hideNotification, options.redirectUrl);
                    }
                } else {
                    bgl.components.pagereloader.poller.goToErrorPage(applicationPath);
                }
            },
            error: function () {
                bgl.components.pagereloader.poller.goToErrorPage(applicationPath);
            }
        });
    }

    function showSpinner(isSpinnerVisible, spinnerName) {

        if (isSpinnerVisible === false) {

            switch (spinnerName) {
                case "addonSpinner":
                    bgl.common.overlay.show();
                    break;
                case "serviceSpinner":
                    var spinnerHtml = bgl.common.spinner.getSpinnerBody("Working on it",
                        "We won't be a moment");
                    $('body').html('<div class="spinner-container"></div>');
                    $('.spinner-container').html(spinnerHtml);
                    break;

                default:
                    {
                        var spinnerHtml = bgl.common.spinner.getSpinnerBody("Working on it",
                            "We won't be a moment");
                        $('body').html(spinnerHtml);
                    }
                    break;
            }
        }
    }

    function goToErrorPage(applicationPath) {
        window.location = applicationPath + 'Error';
    }

    function goToUnderwritingTimeOutPage(applicationPath) {
        window.location = applicationPath + 'Error/UnderwritingTimeOut';
    }

    function goToUnableToQuoteOnlinePage(applicationPath) {
        window.location = applicationPath + 'Error/UnableToQuoteOnline';
    }

    function reloadPage(callback, hideNotification, redirectUrl) {
        if (typeof callback === "function") {
            callback();
            return;
        }

        if (!hideNotification) {
            window.sessionStorage.setItem("notification", JSON.stringify(notificationObj));
        }

        if (redirectUrl) {
            window.location = redirectUrl
        } else {
            window.location.reload(true);
        }
    }

    function removeNotificationFromSessionStorage() {
        sessionStorage.removeItem("notification");
    }

    return {
        poll: poll,
        showSpinner: showSpinner,
        goToErrorPage: goToErrorPage,
        goToUnderwritingTimeOutPage: goToUnderwritingTimeOutPage,
        goToUnableToQuoteOnlinePage: goToUnableToQuoteOnlinePage,
        reloadPage: reloadPage
    };
})();

bgl.components.pagereloader.cleanaddons = (function () {
    var isClearSessionStorageForAddons = false;

    function init() {
        isClearSessionStorageForAddons = true;
    }

    function clearSessionStorage() {
        if (isClearSessionStorageForAddons) {
            sessionStorage.removeItem('CoverDetailsPncdAddonIsYesClicked');
            sessionStorage.removeItem('CoverDetailsPriceProtectionAddonIsYesClicked');
            sessionStorage.removeItem('AddOn-Key-Care-Cover-Choice');
            sessionStorage.removeItem('AddOn-Personal-Accident-Cover');
            sessionStorage.removeItem('DaNAddons');

            sessionStorage.setItem('ReviewIsRedirectRequiredToPncdAddon', 'true');
        }
    }

    return {
        init: init,
        clearSessionStorage: clearSessionStorage
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.pagereloader.loader;
}