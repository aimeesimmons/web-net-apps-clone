﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequote = (function () {

    var configuration;
    var welcomeQuoteSchedule;
    var form;

    function initComponent(componentConfiguration) {
        form = $('#WelcomeQuoteForm');
        configuration = componentConfiguration;

        bgl.common.datastore.init(form);

        form.find("[data-pay-schedule]").off('change').on('change', selectPaymentOption);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (!bgl.common.datastore.isDataChanged(form)) {
                        return;
                    }

                    bgl.common.validator.submitForm(form, configuration);
                });
    }

    function selectPaymentOption() {
        var selected = $(this);
        welcomeQuoteSchedule = selected.data('pay-schedule');

        if (welcomeQuoteSchedule === "monthly") {
            $('#annual-price-label').removeClass('payment-switch__label--selected');
            $('[data-subtext="credit-agreement-text"], [data-subtext="isf-wording-monthly"]').removeClass('hide');
            $('[data-subtext="isf-wording-annual"]').addClass('hide');
        }
        else if (welcomeQuoteSchedule === "annually") {
            $('#monthly-price-label').removeClass('payment-switch__label--selected');
            $('[data-subtext="credit-agreement-text"], [data-subtext="isf-wording-monthly"]').addClass('hide');
            $('[data-subtext="isf-wording-annual"]').removeClass('hide');
        }

        $('#PaymentSwitchId').val(selected.data('paymentMethodId'));

        var label = $("label[for='" + selected.attr("id") + "']");
        label.addClass('payment-switch__label--selected');

        form.submit();
    }

    function onSuccess() {
        bgl.common.focusHolder.setFocusedElement('quote_selector_option');
        bgl.common.datastore.init(form);
        bgl.common.pubsub.emit("switchPaymentType", welcomeQuoteSchedule === "monthly");

        if (welcomeQuoteSchedule === "monthly") {
            bgl.common.pubsub.emit("showNotification", $("#monthly-welcomeQuote-notification-message").val());
        } else if (welcomeQuoteSchedule === "annually") {
            bgl.common.pubsub.emit("showNotification", $("#annual-welcomeQuote-notification-message").val());
        }
    }

    return {
        init: initComponent,
        onSuccess: onSuccess
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequote;
}