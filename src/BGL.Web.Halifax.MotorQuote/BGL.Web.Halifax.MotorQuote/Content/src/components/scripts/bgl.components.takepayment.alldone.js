﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.alldone = (function () {


    function initComponent(policyFieldId) {
        var policyId = sessionStorage.getItem("PaymentProcessedPolicyId");

        $("#" + policyFieldId).text(policyId);

        clearSomeKeysFromSessionStorage();



        sessionStorage.removeItem('PaymentProcessedPolicyId');
    }

    function clearSomeKeysFromSessionStorage() {
        var keysToRemove = [
            '3dSecureTimeoutId',
            'ActiveTab',
            'AddPaymentCardCaptureUri',
            'AddPaymentCreditCardSelectedPersonId',
            'AddPaymentCreditCardSelectedPersonIdText',
            'AddPaymentCreditCardSelectedPersonIdVal',
            'AddPaymentDirectDebitAccountNumber',
            'AddPaymentDirectDebitSelectedPersonId',
            'AddPaymentDirectDebitSelectedPersonIdText',
            'AddPaymentDirectDebitSelectedPersonIdVal',
            'AddPaymentDirectDebitSortCodePart1',
            'AddPaymentDirectDebitSortCodePart2',
            'AddPaymentDirectDebitSortCodePart3',
            'AddPaymentDirectDebitAuthorise',
            'AddPaymentMethodCardCaptureUri',
            'AddPaymentMethodSecureSessionId',
            'AddPaymentSecureSessionId',
            'AnnualCardCaptureUri',
            'AnnualSecureSessionId',
            'AnnualSelectedPersonIdText',
            'ContinuousPaymentCardCaptureUri',
            'ContinuousPaymentCardEdit',
            'ContinuousPaymentCardSecureSessionId',
            'credit-information',
            'MonthlyDepositSelectedPersonIdText',
            'MTAStartPage',
            'OneOffPaymentSelectedPersonIdText',
            'OneOffPaymentCardCaptureUri',
            'OneOffPaymentSecureSessionId',
            'OneOffPaymentSelectedPersonIdVal',
            'OneOffPaymentSelectedPersonId',
            'PaymentProcessedPolicyId',
            'PaymentProcessedRedirectUrl',
            'UseMonthlyInstalmentCard'
        ];

        keysToRemove.forEach(function (key) {
            window.sessionStorage.removeItem(key);
        });
    }

    return {
        init: initComponent
    };
})();