﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};


bgl.components.takepayment.updatepaymentmethodcomplete = (function () {
    
    var configuration;
    var slidePanelContainer = 'SlidePanelContent';
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {

            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        onBackButtonClick();
        onClickDoneButton();
        onClickConfirmPaymentMethod();
    }

    function onBackButtonClick() {
        $('#BackPaymentMethod').on('click', function () {
            var linkId = this.id;
            var linkButton = $("#" + linkId);
            var actionUrl = linkButton.data("action-url");
            var dataActionUrl = actionUrl;
            if (configuration.LayoutOption === "PageAsSlider") {
                 dataActionUrl = "/TakePaymentComponent/AddPaymentMethodConfirmation";
            } 
                var containerSelector = slidePanelContainer;
                var requestBodyData = { componentConfiguration: configuration };
                bgl.common.loader.load(requestBodyData,
                    dataActionUrl,
                    containerSelector,
                    function () {
                        if (configuration.LayoutOption === "PageAsSlider") {
                            bgl.components.takepayment.addpaymentmethodconfirmation.init(configuration);
                        } else {
                            bgl.components.takepayment.paymentmethods.init(configuration);
                        }
                    });
        });
    }

    function onClickConfirmPaymentMethod() {
        $('#ConfirmPaymentMethod').on('click', function () {
            window.location.href = configuration.PaymentMethodAllDoneUrl;
        });
    }

    function onClickDoneButton() {
        $("#Done-button").on('click', function () {
            clearSomeKeysFromSessionStorage(); 
            window.location.href = configuration.RedirectUrl;
        });
    }

    function clearSomeKeysFromSessionStorage() {
        var keysToRemove = [
            '3dSecureTimeoutId',
            'ActiveTab',
            'AddPaymentCardCaptureUri',
            'AddPaymentCreditCardSelectedPersonId',
            'AddPaymentCreditCardSelectedPersonIdText',
            'AddPaymentCreditCardSelectedPersonIdVal',
            'AddPaymentDirectDebitAccountNumber',
            'AddPaymentDirectDebitSelectedPersonId',
            'AddPaymentDirectDebitSelectedPersonIdText',
            'AddPaymentDirectDebitSelectedPersonIdVal',
            'AddPaymentDirectDebitSortCodePart1',
            'AddPaymentDirectDebitSortCodePart2',
            'AddPaymentDirectDebitSortCodePart3',
            'AddPaymentDirectDebitAuthorise',
            'AddPaymentMethodCardCaptureUri',
            'AddPaymentMethodSecureSessionId',
            'AddPaymentSecureSessionId',
            'AnnualCardCaptureUri',
            'AnnualSecureSessionId',
            'AnnualSelectedPersonIdText',
            'ContinuousPaymentCardCaptureUri',
            'ContinuousPaymentCardEdit',
            'ContinuousPaymentCardSecureSessionId',
            'credit-information',
            'MonthlyDepositSelectedPersonIdText',
            'MTAStartPage',
            'OneOffPaymentSelectedPersonIdText',
            'OneOffPaymentCardCaptureUri',
            'OneOffPaymentSecureSessionId',
            'OneOffPaymentSelectedPersonIdVal',
            'OneOffPaymentSelectedPersonId',
            'PaymentProcessedPolicyId',
            'PaymentProcessedRedirectUrl',
            'UseMonthlyInstalmentCard'
        ];

        keysToRemove.forEach(function (key) {
            window.sessionStorage.removeItem(key);
        });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentstatus;
}
