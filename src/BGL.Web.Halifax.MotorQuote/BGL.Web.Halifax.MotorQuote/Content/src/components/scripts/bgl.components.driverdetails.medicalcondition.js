﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var SPACE_KEYCODE = 32;
var ENTER_KEYCODE = 13;

bgl.components.driverdetails.medicalcondition = (function () {
    var form,
        configuration;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        initialiseVariables();

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeForEvents();
    }

    function initialiseVariables() {
        form = $('#medical-condition');

        bgl.common.validator.init(form, configuration);
    }

    function subscribeForEvents() {
        var person = bgl.common.datastore.getData(form);
        var isLicenceInternational = person.Licence === "I";

        if (isLicenceInternational) {
            $("#" + configuration.Id + "-back-button").attr("href", configuration.LicenceDatePageUrl);
        }

        if (form.find("input[type=radio]:checked").length === 0) {
            form.find("input[type=radio]")
                .off('keypress', generateClickOnKeypress)
                .on('keypress', generateClickOnKeypress)
                .off('click', continueHandler)
                .on('click', continueHandler);
        } else {
            var continueButton = $(form.find("[type='submit']"));
            continueButton.removeClass("hide");
        }

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (bgl.common.validator.isFormValid($(this))) {

                        $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                        bgl.common.validator.submitForm(form, configuration);
                    }
                });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function generateClickOnKeypress(event) {
        var code = event.charCode || event.keyCode;

        if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
            $(event.target).prop("checked", "checked");
            $(event.target).click();
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.find("input[type=radio]").off();
        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.hasDvlaNotified === false) {
            bgl.common.datastore.deleteFields(form, ["MedicalConditionDvlaNotified"]);
        }

        if (data.nextDtsPageUrl) {
            location.href = data.nextDtsPageUrl;
        }
    }

    function onError() {
        bgl.components.driverdetails.medicalcondition.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
