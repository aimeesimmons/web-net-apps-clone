﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.monthlyinstalmentsummary = (function() {

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        componentConfiguration.CardSessionData = getSecureSessionData();
        componentConfiguration.CreditInformationUrl = sessionStorage.getItem("credit-information");
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function() {
                initComponent();
            });
    }

    function initComponent() {
        bgl.common.datastore.setupReadAndWrite();

        $("#importantInfo-payments-instalment-summary, #importantInfo-bottom, #intermediary-services-contract-summary-slider-link").off("click").on("click", function (event) {
            event.preventDefault();
            var link = $(this);
            bgl.common.loader.load({}, link.data("action-url"), link.data("container"));
        });
    }

    function getSecureSessionData() {
        return {
            "Name": sessionStorage.getItem("MonthlyInstalmentCardSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("MonthlyInstalmentCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("MonthlyInstalmentSecureSessionId")
        };
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();