﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.scrolltoelement = (function () {

    function init() {
        subscribeScrollLink();
    }

    function subscribeScrollLink() {
        $('.scroll-to-link')
            .off('click')
            .on('click', function (e) {

                e.preventDefault();

                var $this = $(this);
                var time = parseInt($this.data('scrollTime'));
                var elementToScroll = $this.attr('href');
                var accordion = $this.data('accordion');

                if (accordion) {
                    openAccordion(accordion);
                }

                scrollTo(elementToScroll, time);
            });
    }

    function openAccordion(element) {
        var driverHeaderAccordionButton = $(element).find('.accordion__heading-button');
        var isExpanded = $(driverHeaderAccordionButton).attr("aria-expanded");

        if (isExpanded.toLowerCase() === "false") {
            $(driverHeaderAccordionButton).click();
        }
    }

    function scrollTo(element, time) {
        $('html,body').animate({
            scrollTop: $(element).offset().top
        }, time);
    }

    return {
        init: init,
        scrollTo: scrollTo
    };
})();