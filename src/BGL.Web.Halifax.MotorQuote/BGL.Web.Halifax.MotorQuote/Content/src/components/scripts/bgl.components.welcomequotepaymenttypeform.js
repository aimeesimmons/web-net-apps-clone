﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.welcomequotepaymenttypeform = (function () {
    var configuration;
    var welcomeQuoteSchedule;
    var form;

    function initComponent(componentConfiguration) {
        form = $('#PaymentTypeForm');
        configuration = componentConfiguration;

        $('.payment-options input[type=radio]').off('change')
            .on('change',
                function (event) {
                    event.preventDefault();

                    selectPaymentOption($(this));
                    bgl.common.validator.submitForm(form, configuration);
                });
    }

    function selectPaymentOption(selected) {
        welcomeQuoteSchedule = selected.data('paySchedule');

        if (welcomeQuoteSchedule === "monthly") {
            $('[data-subtext="credit-agreement-text"], [data-subtext="isf-wording-monthly"]').removeClass('hide');
            $('[data-subtext="isf-wording-annual"]').addClass('hide');
        }
        else if (welcomeQuoteSchedule === "annually") {
            $('[data-subtext="credit-agreement-text"], [data-subtext="isf-wording-monthly"]').addClass('hide');
            $('[data-subtext="isf-wording-annual"]').removeClass('hide');
        }
    }

    function onSuccess() {
        bgl.common.focusHolder.setFocusedElement('quote_selector_option');
        var isMonthly = welcomeQuoteSchedule === "monthly";
        bgl.common.pubsub.emit("switchPaymentType", isMonthly);

        if (welcomeQuoteSchedule) {
            var notification = $("#" + welcomeQuoteSchedule + "-welcomeQuote-notification-message").val();

            bgl.common.pubsub.emit("showNotification", notification);
        }
    }

    return {
        init: initComponent,
        onSuccess: onSuccess
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.welcomequotepaymenttypeform;
}