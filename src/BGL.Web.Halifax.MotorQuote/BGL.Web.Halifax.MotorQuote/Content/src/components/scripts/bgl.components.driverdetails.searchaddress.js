﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.searchaddress = (function() {
    var configuration,
        containerId,
        form,
        BACK_URL_STACK_KEY = "backUrlStack";

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;
        containerId = document.getElementById(containerElementId).parentElement.getAttribute("id");

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                bgl.components.driverdetails.searchaddress.init(configuration);
                setSessionStorageItems();
                sessionStorage.removeItem(BACK_URL_STACK_KEY);
            }
        );
    }

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        containerId = document.getElementById(componentConfiguration.Id).parentElement.getAttribute("id");
        form = $('form');
        if (form.attr("data-step") === "confirm") {
            bgl.common.datastore.setupReadAndWrite();
        } else {
            bgl.common.datastore.init(form);
            bgl.common.datastore.setupReadAndWrite();
        }

        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init(form);

        initAjaxLink();
        initBackButtonLink();
        initPostcodeFieldTrim();

        setElementFocusIfIsOnPage('Line1'); // Manual entry
        setElementFocusIfIsOnPage('Home'); // Search entry

        form.off('submit')
            .on('submit',
                function(event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {

                        var quoteId = $("#QuoteId").val();

                        if (form.attr("data-step") === "confirm" && quoteId !== "") {
                            bgl.common.validator.submitForm(form, configuration);
                        } else if (form.attr("data-step") === "confirm" && quoteId === "") {
                            var dataToPost = form.serialize();
                            $.ajax({
                                url: configuration.ApplicationPath + "Account/" + "NewLeadEntry",
                                type: "POST",
                                cache: false,
                                dataType: "json",
                                data: dataToPost,
                                success: function (data) {
                                    if (data.isSuccess === false) {
                                        bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
                                        return;
                                    }
                                    sessionStorage.setItem("CustomerFirstName", data.customerFirstName);
                                    sessionStorage.setItem("CustomerAccountId", data.customerAccountId);
                                    bgl.common.utilities.redirectToUrl(data.redirectUrl);
                                }
                            });
                        } else {
                            bgl.common.validator.submitForm(form, configuration);
                        }
                    }
                });
    }

    function setSessionStorageItems() {
        var addressId = $('[name="AddressId"]').val();
        var postcode = $('[name="PostCode"]').val();
        var home = $('[name="Home"]').val();

        sessionStorage.setItem("addressId", addressId);
        sessionStorage.setItem("postCode", postcode);
        sessionStorage.setItem("home", home);
    }

    function loadView(url) {
        var sessionStorageData = JSON.parse(sessionStorage.getItem(configuration.DataStoreKey));

        var data = {
            ComponentConfiguration: configuration,
            AddressId: sessionStorageData.AddressId
        };

        if (url.indexOf('SearchAddress') !== -1) {
            sessionStorage.removeItem(BACK_URL_STACK_KEY);
            data = configuration;
        } else if (url.indexOf('SelectAddress') !== -1) {
            data.PostCode = sessionStorageData.PostCode;
            data.Home = sessionStorageData.Home;
        }

        bgl.common.loader.load(data,
            url,
            containerId,
            function () {
                initComponent(configuration);
            });
    }

    function initAjaxLink() {
        $('.ajax-address-link')
            .off('click')
            .on('click',
                function (e) {
                    e.preventDefault();
                    var url = $(this).attr('href');
                    var backButtonUrl = $(this).data('back-url');
                    pushBackButtonUrl(backButtonUrl);
                    loadView(url);
                });
    }

    function initBackButtonLink() {
        $('.back-button-link')
            .off('click')
            .on('click',
                function (e) {
                    e.preventDefault();
                    var url = getBackButtonUrlFromStack();
                    if (url) {
                        $(this).hide();
                        loadView(url);
                    } else {
                        location.href = configuration.PreviousStepUrl;
                    }
                });
    }

    function initPostcodeFieldTrim() {
        $('#PostCode')
            .on('blur',
                function (e) {
                    $(this).val($(this).val().trim());
                });
    }

    function setElementFocusIfIsOnPage(elementId) {
        var element = document.getElementById(elementId);

        if (element != null) {
            $(`#${elementId}`).focus().select();
        }
    }

    function onSuccess(configuration, data) {
        pushBackButtonUrl(data.backButtonUrl);
        bgl.common.loader.load(data.content, data.url, containerId, function () {
            bgl.components.driverdetails.searchaddress.init(configuration);
        });
    }

    function onSuccessConfirmation(configuration, data) {
        location.href = configuration.NextStepUrl;
    }

    function onError() {
        bgl.components.driverdetails.searchaddress.init(configuration);
    }

    function getBackButtonUrlFromStack() {
        var stack = JSON.parse(sessionStorage.getItem(BACK_URL_STACK_KEY));
        if (stack) {
            var url = stack.pop();
            sessionStorage.setItem(BACK_URL_STACK_KEY, JSON.stringify(stack));
            return url;
        }
    }

    function pushBackButtonUrl(url) {
        var stack = JSON.parse(sessionStorage.getItem(BACK_URL_STACK_KEY)) || [];
        stack.push(url);
        sessionStorage.setItem(BACK_URL_STACK_KEY, JSON.stringify(stack));
    }
    
    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onSuccessConfirmation: onSuccessConfirmation,
        onError: onError
    };
})();
