﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.informationsource = (function () {
    var form,
        configuration,
        informationSourceSelectedResponse,
        brandCodeNames = [
            "BRANCH",
            "KIOSK",
            "PROMO",
            "COLLEA"
        ];

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#' + configuration.Id).find("#information-source-form");
        informationSourceSelectedResponse = form.find('#InformationSourceQuestion_SelectedResponse');

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);
        subscribeForEvents();

        toggleBrandCodeSection(informationSourceSelectedResponse.val());
    }

    function subscribeForEvents() {
        informationSourceSelectedResponse.on('change',
            function () {
                var currentValue = $(this).val();
                toggleBrandCodeSection(currentValue);
            });

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (!bgl.common.validator.isFormValid(form))
                        return;

                    if (!bgl.common.datastore.isDataChanged(form)) {
                        onSuccess();
                        return;
                    };

                    bgl.common.validator.submitForm(form, configuration);
                });
    }

    function toggleBrandCodeSection(currentValue) {
        var brandCodeSection = $('#Branch-Code-Section');

        for (var i = 0; i < brandCodeNames.length; i++) {
            if (endsWith(currentValue, brandCodeNames[i])) {
                brandCodeSection.removeClass('hide');
                return;
            }
        }

        brandCodeSection.addClass('hide');
        form.find("#BranchCodeQuestion_SelectedResponse").val("").trigger('focusout');
    }

    function endsWith(originalString, searchString) {
        var position = originalString.length;
        position -= searchString.length;

        var lastIndex = originalString.indexOf(searchString, position);

        return lastIndex !== -1 && lastIndex === position;
    }

    function onSuccess() {
        bgl.common.datastore.removeSessionStorage(form);
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onError() {
        initComponent(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

