﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

var selectedModifications = 'SelectedModifications';

bgl.components.journeyTypesVehicleDetails = {
    SingleEdit: "0",
    LookupEdit: "1",
    MultiStepEdit: "2"
}

bgl.components.vehicledetails.lookup = (function () {

    var configuration, containerId;
    var globalContainerId;

    function initComponent(componentConfiguration, containerElementId) {

        configuration = componentConfiguration;
        containerId = containerElementId;

        var editButton = $("#vehicle-lookup_edit-button");

        editButton.on("click",
            function (event) {
                var link = $(event.currentTarget);
                var containerSelector = link.data("container");
                var storeKey = link.data("store-key");

                sessionStorage.removeItem(storeKey);

                bgl.components.vehicledetails.searching.load({ ComponentConfiguration: configuration, VehicleId: link.data("vehicle-id") },
                    link.data("action-url"),
                    containerSelector);

                event.preventDefault();
            });
    }

    function initPage(componentConfiguration, pageContainerId) {
        if (configuration === undefined) {
            configuration = componentConfiguration;
        }

        if (pageContainerId !== undefined) {
            globalContainerId = pageContainerId;
        }

        $(window).off('hashchange').on('hashchange', loadVehicleContent);
        $(document).ready(loadVehicleContent);
    }

    function loadVehicleContent() {
        var hash = bgl.common.utilities.getUrlHashValue() || "";

        var vehicleObject = bgl.common.datastore.getData(configuration.DataStoreKey) || {};

        vehicleObject.ComponentConfiguration = configuration;
        vehicleObject.DataStoreKey = configuration.DataStoreKey;
        vehicleObject.JourneyType = configuration.JourneyType;

        if (hash.toLowerCase() === "value" && vehicleObject.HasValue && vehicleObject.HasValue.toLowerCase() === "true") {
            bgl.common.utilities.setUrlHashValue("purchasedate");
            return;
        }

        switch (hash.toLowerCase()) {
        case "purchasedate":
            bgl.components.vehicledetails.purchasedate.load(vehicleObject,
                configuration.PurchasedDateUrl,
                globalContainerId);
                break;
        case "numberofseats":
            bgl.components.vehicledetails.numberofseats.load(vehicleObject,
                configuration.NumberOfSeatsUrl,
                globalContainerId);
                break;
        case "vehiclebodytype":
            bgl.components.vehicledetails.vehiclebodytype.load(vehicleObject,
                configuration.VehicleBodyTypeUrl,
                globalContainerId);
            break;
        case "modifications":
            vehicleObject.Modifications = vehicleObject[selectedModifications] || [];

            bgl.components.vehicledetails.modifications.load(vehicleObject,
                configuration.ModificationsUrl,
                globalContainerId);
            break;
        case "value":
            bgl.components.vehicledetails.value.load(vehicleObject,
                configuration.EditValueUrl,
                globalContainerId);
                break;
        case "main-content":
                scrollToMainContentOfPage();
                break;
        default:
            bgl.components.vehicledetails.summary.load(vehicleObject,
                configuration.SearchSummaryUrl,
                globalContainerId);
            break;
        }
    }

    function postVehicle(form, url) {

        var data = getData(form);

        data.HasChanges = data.isFormDirty === true;
        
        $.post(url, data, function (response) {
            if (!response.isSuccess && response.errorRedirectUrl) {
                bgl.common.utilities.errorRedirect(response.errorRedirectUrl);
            } else if (response.isSuccess) {
                if (response.journeyType.toString() === bgl.components.journeyTypesVehicleDetails.MultiStepEdit) {

                    if (response.hasRegistrationNumber === false) {
                        bgl.common.datastore.extendCollectedData(configuration.DataStoreKey, { VehicleRegistrationNumber: "" });
                    }

                    bgl.common.datastore.extendCollectedData(configuration.DataStoreKey, { isFormDirty: false });
                    bgl.common.utilities.redirectToUrl(response.redirectUrl);
                    return;
                }

                bgl.common.datastore.removeSessionStorage(form);

                if (configuration && configuration.IsService) {
                    if (configuration.IsInitialChange) {
                        if (data.HasChanges === true) {
                            sessionStorage.setItem("MTAStartPage", window.location.href);
                            bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                        } else {
                            $('[data-dismiss="slide-panel"]').click();
                        }
                    } else {
                        bgl.common.utilities.redirectToUrl(configuration.AdditionalChangesUrl);
                    }
                } else {
                    bgl.common.pubsub.emit("PollBasket");
                    $('[data-dismiss="slide-panel"]').click();
                }
            }
        });
    }

    function getData(form) {
        var token = form.find('input[name="__RequestVerificationToken"]').val();
        var data = bgl.common.datastore.getData(form) || {};

        data.__RequestVerificationToken = token;
        data.ComponentConfiguration = configuration;

        return data;
    }

    function scrollToMainContentOfPage() {
        var requiredField = $('#main-content');
        $('html,body').animate({ scrollTop: requiredField.offset().top }, 0);
    }

    return {
        init: initComponent,
        initPage: initPage,
        postVehicle: postVehicle
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.vehicledetails;
}
