﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

var slidePanelContainer = 'SlidePanelContent';

bgl.components.takepayment.paymentschedule = (function () {

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        var configuration = componentConfiguration;

        $('#ChangePaymentMethod').on('click', function () {
            var linkId = this.id;
            var linkButton = $("#" + linkId);
            var actionUrl = linkButton.data("action-url");
            var containerSelector = slidePanelContainer;
            var requestBodyData = { componentConfiguration: configuration };
            bgl.common.loader.load(requestBodyData,
                actionUrl,
                containerSelector,
                function () {                  
                    bgl.components.takepayment.paymentmethods.init(componentConfiguration);
                });
        });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentschedule;
}
