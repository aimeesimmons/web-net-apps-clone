﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.mtatakepayment = (function () {
    var configuration;
    var takePaymentForm;
    var additionalDetailsForm;
    var monthDropdown;
    var yearDropdown;
    var fullYearMonthOptions;
    var currentYearMonthOptions;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        takePaymentForm = $("[data-component-id='" + configuration.Id + "'] form#" + configuration.Id + "_mta-take-payment");
        additionalDetailsForm = $("[data-component-id='" + configuration.Id + "'] form#" + configuration.Id + "_mta-additional-details");

        if (takePaymentForm.length > 0) {
            bgl.common.validator.init(takePaymentForm);
        } else {
            bgl.common.validator.init(additionalDetailsForm);

            var currentMonth = $('#CurrentMonth').val() - 0;

            monthDropdown = additionalDetailsForm.find('#Purchased-Month');
            yearDropdown = additionalDetailsForm.find('#Purchased-Year');
            fullYearMonthOptions = monthDropdown.find('option');
            currentYearMonthOptions = fullYearMonthOptions.slice(0, currentMonth + 1);
        }

        subscribeToEvents();
    }

    function subscribeToEvents() {
        takePaymentForm.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                if (!$("#agreement-checkbox").prop("checked")) {
                    $("#agreement-container").addClass("error");
                    scrollToFirstFieldWithErrorAndFocus();
                } else {
                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        additionalDetailsForm.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                var regField = additionalDetailsForm.find("#registration-number");
                if (regField.length > 0) {
                    regField.val(regField.val().replace(/\s+/gi, ""));
                }

                if (bgl.common.validator.isFormValid($(this))) {
                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

        $("[data-cancel]").on("click",
            function () {
                var startPage = sessionStorage.getItem("MTAStartPage");
                if (!startPage) {
                    startPage = configuration.MTACancelUrl;
                }

                sessionStorage.removeItem("MTAStartPage");

                bgl.common.utilities.redirectToUrl(startPage);
            });

        if (additionalDetailsForm.length > 0) {
            additionalDetailsForm.find("#Purchased-Year").on("change", function () {
                var months = monthDropdown;
                var selectedMonthValue = months.val();
                var currentYear = $(yearDropdown).find("option:eq(1)").val();

                switch (yearDropdown.val()) {
                    case currentYear:
                        months.html(currentYearMonthOptions);
                        break;
                    default:
                        months.html(fullYearMonthOptions);
                        break;
                };

                months.val(selectedMonthValue);

                // select first value if value is not selected
                if (!months.val()) {
                    months.find('option:eq(0)').prop('selected', true);
                }

                additionalDetailsForm.find("#registration-number").on("input",
                    function () {

                        var start = this.selectionStart;
                        var end = this.selectionEnd;

                        this.value = this.value.toUpperCase();

                        this.setSelectionRange(start, end);
                    });
            });
        }
    }

    function scrollToFirstFieldWithErrorAndFocus() {
        var firstRequiredField = $('.error').first();
        $('html,body').animate({ scrollTop: firstRequiredField.offset().top }, 'slow');

        if (firstRequiredField.length) {
            var invalidInputElem = firstRequiredField.find("input, select, textarea").addBack("input, select, textarea")[0];

            if (invalidInputElem !== undefined) {
                invalidInputElem.focus();
            }
        }
    }

    function onSuccess(configuration, data) {
        if (data.redirectUrl) {
            bgl.common.utilities.redirectToUrl(data.redirectUrl);
        } else {
            bgl.common.utilities.redirectToUrl(configuration.PaymentAllDoneUrl);
        }
    }

    function onError(configuration) {
        bgl.common.utilities.redirectToUrl(configuration.PaymentDeclinedUrl);
    }

    function onAdditionalDetailsSuccess() {
        var options = {
            hideNotification: true
        }
        bgl.common.pubsub.emit('PollBasket', options);
    }

    function onAdditionalDetailsError() {
        initComponent(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError,
        onAdditionalDetailsSuccess: onAdditionalDetailsSuccess,
        onAdditionalDetailsError: onAdditionalDetailsError
    };
})();