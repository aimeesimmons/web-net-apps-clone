﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.othervehicles = (function () {
    var form;
    var configuration;
    var continueButton;

    function initComponent(componentConfiguration) {
        var isNcdQuestionRemoved = componentConfiguration.IsNcdQuestionRemoved;

        configuration = componentConfiguration;
        form = $('#' + configuration.Id + '__other-vehicles-form');
        continueButton = $('#Continue-Button');

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);

        if (!isNcdQuestionRemoved) {
            subscribeForEvents();
            initElements();
        } else {
            initWithoutNcdQuestion();
        }

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function initWithoutNcdQuestion() {
        showContinue();

        form.find('#Vehicle-Use-Yes, #Vehicle-Use-No').on('click',
            function (e) {
                if (continueButton.hasClass('hide')) {
                    form.change().submit();
                }
            });

        subscribeForSubmit();
    }

    function initElements() {
        form.find('#Vehicle-Use-Yes, #Other-Vehicle-Yes').each(function (index, element) {
            var sections = $(element).data('sections');

            if ($(element).prop('checked')) {
                $(sections).removeClass('hide');
            }
        });

        showContinue();
    }

    function showContinue() {
        if (form.find("input[type='radio']:checked").length) {
            continueButton.removeClass('hide');
        }
    }

    function subscribeForEvents() {
        form.find('#Vehicle-Use-Yes, #Other-Vehicle-Yes').on('click',
            function (e) {
                var sections = $(this).data('sections');

                $(sections).removeClass('hide');
            });

        form.find('#Vehicle-Use-No, #Other-Vehicle-No').on('click',
            function (e) {
                var sections = $(this).data('sections');

                $(sections).addClass('hide');

                deleteFields(sections);

                if (continueButton.hasClass('hide')) {
                    form.change().submit();
                }
            });

        subscribeForSubmit();
    }

    function subscribeForSubmit() {
        form.off('submit').on('submit',
            function (event) {
                event.preventDefault();

                if (bgl.common.validator.isFormValid(form)) {

                    if (bgl.common.datastore.isDataChanged(form)) {
                        var data = getStoredData();

                        bgl.common.validator.submitForm(form, configuration, data);

                    } else {
                        onSuccess();
                    }
                }
            });
    }

    function deleteFields(sections) {
        var elements = $(sections).removeClass('error').find('[data-store]');
        var dropdownKey;

        var fieldsToDelete = $.map(elements,
            function (element) {

                var elementName = $(element).attr('name');

                if ($(element).is('select')) {
                    $(element).val("");
                    dropdownKey = elementName;
                } else if ($(element).is('input')) {
                    $(element).prop('checked', false);
                }

                return elementName;
            });

        if (dropdownKey) {
            fieldsToDelete.push(dropdownKey + "Text", dropdownKey + "Val");
        }

        bgl.common.datastore.deleteFields(form, fieldsToDelete);
    }

    function getStoredData() {
        var data = bgl.common.datastore.getData(form) || {};
        var formData = form.serialize();

        return formData + "&" + $.param(data);
    }

    function onSuccess() {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onError() {
        initComponent(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();