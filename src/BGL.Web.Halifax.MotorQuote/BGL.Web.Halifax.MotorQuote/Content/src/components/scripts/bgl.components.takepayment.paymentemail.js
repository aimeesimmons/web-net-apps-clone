﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymentemail = (function () {
    var configuration;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        var form = $('#PaymentEmailForm');

        bgl.common.validator.init(form);
        bgl.common.datastore.init(form);

        form.off("submit").on('submit',
            function(e) {
                e.preventDefault();

                if (!bgl.common.validator.isFormValid(form)) {
                    return;
                }

                if (!bgl.common.datastore.isDataChanged(form)) {
                    onSuccess(configuration);
                    return;
                }

                bgl.common.validator.submitForm(form, configuration);
            });
    }

    function onSuccess(componentConfiguration, data) {
        bgl.common.utilities.redirectToUrl(componentConfiguration.NextStepUrl);
    }

    function onError(componentConfiguration) {
        initComponent(componentConfiguration);  
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentsummary;
}