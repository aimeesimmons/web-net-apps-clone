﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.coverlevel = (function () {

    var configuration,form;
    var selectedCoverFlag;
    var sessionData;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        
        initValidation();

        toggleCoverLevelText();

        $("#BtnSubmit").off("click").on("click",
            function () {
                    sessionData = bgl.common.datastore.getData(configuration.DataStoreKey);
                    if (!sessionData.selectedCoverFlag && !sessionData.CoverLevelCode) {
                        $("#validation-message").show();
                        return false;
                    } else {
                        submitForm();
                    }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);

    }

    function toggleCoverLevelText() {
        $(".cover-level").off("click").on("click",
            function () {
                $("#validation-message").hide();
                $(".cover-level").attr('checked', 'checked');
                selectedCoverFlag = $(this).data('coverlevel');
                $("#feature-header-cover-level").text(selectedCoverFlag);
                $("#feature-question-box-cover-level").text(selectedCoverFlag);
                sessionData = JSON.parse(sessionStorage.getItem(configuration.DataStoreKey));
                sessionData.selectedCoverFlag = selectedCoverFlag;
                sessionStorage.setItem(configuration.DataStoreKey, JSON.stringify(sessionData));
            });  
    }

    function initValidation() {
        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.init(form);

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();

                bgl.common.validator.validate(configuration, $(this));
            });

       }

    function submitForm() {
        form.submit();
    }

    function onSuccess() {
        if (sessionData.selectedCoverFlag === "Comprehensive") {
            window.location.href = configuration.NextStepUrl;
        } else {
            window.location.href = configuration.CoverLevelUrl;
        }
    }

    return {
        init: initComponent,
        onSuccess: onSuccess
    };
})();