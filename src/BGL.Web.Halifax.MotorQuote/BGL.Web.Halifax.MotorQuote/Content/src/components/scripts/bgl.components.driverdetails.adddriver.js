﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

var slidePanelContainer = 'SlidePanelContent';

bgl.components.driverdetails.adddriver = (function () {

    function initComponent(componentConfiguration) {
        $('[data-add-driver-button]')
            .on('click', function () {
                var requestBodyData = {
                    ComponentConfiguration: componentConfiguration,
                    JourneyType: 1
                };

                var actionUrl = $(this).data("action-url");

                bgl.common.loader.load(requestBodyData,
                    actionUrl,
                    slidePanelContainer,
                    function () {
                        bgl.components.additionaldriver.init(componentConfiguration);
                    });
            });
    }

    return {
        init: initComponent
    };
})();