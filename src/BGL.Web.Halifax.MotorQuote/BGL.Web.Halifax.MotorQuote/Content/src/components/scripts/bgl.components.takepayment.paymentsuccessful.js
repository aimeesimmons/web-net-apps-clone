﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymentsuccessful = (function () {
    var componentConfig;
    var requestVerificationToken;

    function initComponent(componentConfiguration) {
        componentConfig = componentConfiguration;
        requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

        $("body").html(bgl.common.spinner.getSpinnerBody("Creating your policy",
            "Please do not hit the back button or refresh the page"));

        var redirectUrl = sessionStorage.getItem("PaymentProcessedRedirectUrl");
        var policyId = sessionStorage.getItem("PaymentProcessedPolicyId");
        sessionStorage.clear();
        sessionStorage.setItem("PaymentProcessedPolicyId", policyId);
        emitGTMUpdate(policyId);
        waitForPolicyToBeCreated(policyId, redirectUrl);
    }

    function emitGTMUpdate(policyId) {
        bgl.common.pubsub.emit('UpdateGTMPolicyId', policyId);
    }

    function waitForPolicyToBeCreated(policyId, redirectUrl) {
        var interval = 500;
        var timeoutDuration = 30000;
        var pollUrl = 'TakePaymentComponent/IsPolicyCreated';
        var tickCount = new Date().getTime();
        var timeout = tickCount + timeoutDuration;
        var applicationPath = componentConfig.ApplicationPath;
        poll(timeout, interval, pollUrl, applicationPath, policyId, redirectUrl);
    }

    function poll(timeout, interval, pollUrl, applicationPath, policyId, redirectUrl) {
        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            PolicyId: policyId
        };

        $.ajax({
            url: applicationPath + pollUrl,
            type: 'Post',
            cache: false,
            data: dataToPost,
            dataType: 'json',
            success: function (response) {
                if (response.Status === "Processing") {
                    if (timeout >= new Date().getTime()) {
                        setTimeout(function () {
                            poll(timeout, interval, pollUrl, applicationPath, policyId, redirectUrl);
                        },
                            interval);
                    }
                    else {
                        navigateWhenReadyTo(componentConfig.PolicyCreationPendingUrl);
                    }
                }
                else if (response.Status === "Created") {
                    navigateWhenReadyTo(redirectUrl);
                }
                else {
                    navigateWhenReadyTo(response.ErrorRedirectUrl);
                }
            }
        });
    }

    function navigateWhenReadyTo(nextPage) {
        var twoSeconds = 2000;
        navigateTo(nextPage, theTimeRightNow() + twoSeconds);
    }

    function navigateTo(nextPage, timeoutTime) {
        if (hasTimeoutOccured(timeoutTime) || hasTagProcessingCompleted()) {
            window.location.href = nextPage;
        }
        else {
            setTimeout(navigateTo, 100, nextPage, timeoutTime);
        }
    }

    function theTimeRightNow() {
        return new Date().getTime();
    }

    function hasTimeoutOccured(timeoutTime) {
        return timeoutTime <= theTimeRightNow();
    }

    function hasTagProcessingCompleted() {
        return tagProcessingDone;
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentsuccessful;
}