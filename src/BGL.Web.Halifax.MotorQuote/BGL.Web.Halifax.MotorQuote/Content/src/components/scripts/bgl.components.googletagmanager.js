﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.googletagmanager = (function () {
    var gtmContainerId;
    var gtmApplicationPath;

    function initComponent(containerId, applicationPath) {
        gtmContainerId = containerId;
        gtmApplicationPath = applicationPath;

        if (gtmContainerId) {
            // gtmStart(window, document, 'script', 'dataLayer', gtmContainerId);
            gtmStartWithOneTrust(window, document, 'script', 'dataLayer', gtmContainerId, 'cdn.cookielaw.org', 'Optanon');
        }

        UpdateGTMPolicyId();
    }

    function gtmStartWithOneTrust(w, d, s, l, g, o1, o2) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); 
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l !== 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + g + dl;
        for (var i = 0; i < d.getElementsByTagName(s).length; i++) {
	        if (i > 0) f = d.getElementsByTagName(s)[i];
	        if ((f.outerHTML.indexOf(o1) === -1) && (f.outerHTML.indexOf(o2) === -1)) {
		        break;
	        }
        }
        f.parentNode.insertBefore(j, f);
    }

    function gtmStart(w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    }

    function UpdateGTMPolicyId() {
        bgl.common.pubsub.on('UpdateGTMPolicyId', function (policyId) {
            dataLayer.push({ 'policyNumber': policyId });

            var url = gtmApplicationPath + "GoogleTagManagerComponent/GetECommerceDataLayer?policyId=" + policyId;

            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    var eCommmerceData = JSON.parse(data)
                    dataLayer.push(eCommmerceData)
                }
            });
        });
    }

    return {
        init: initComponent
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.googletagmanager;
}