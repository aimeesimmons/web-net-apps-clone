﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.annualmileage = (function () {
    var form;
    var configuration;
    var backUrlStackKey = "backUrlStack";
    var containerId;
   
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;
        containerId = containerElementId;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration, containerElementId ) {
        configuration = componentConfiguration;
        containerId = containerElementId;

        form = $('#' + configuration.Id + '__annual-mileage-form');
        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();
        bgl.common.validator.enableMaxLength();
        bgl.common.validator.init(form);
        bgl.common.datastore.extendCollectedData(form, { AnnualMileage: $('#' + configuration.Id + '__annual-mileage').val() });

        subscribeToEvents(configuration);
        initBackButtonLink();
    }

    function subscribeToEvents(configuration) {

        var numberBox = $("#" + configuration.Id + "__annual-mileage");
       
        calculateAverageMiles(numberBox.val());

        numberBox.off("keydown").on("keydown",
            function (e) {
                // 46 - DELETE
                // 8 - BACKSPACE
                // 9 - TAB
                // 27 - ESCAPE
                // 13 - ENTER
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }
                //Check to see if the shift key, or any other alpha character has been hit.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ||
                    $(this).val().length === 5 ||
                    (e.keyCode === 96 && $(this).val().length === 0)) {
                    e.preventDefault();
                }
            });

        numberBox.on("keyup",
            function () {
                var val = $(this).val();
                while (val.substring(0, 1) === '0') {
                    val = val.substring(1);
                }
                $(this).val(val);
            });

        numberBox.off("input").on("input",
            function (e) {
                e.preventDefault();

                var value = $(e.target).val();

                calculateAverageMiles(value);
            });

        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();
                if (bgl.common.validator.isFormValid($(this))) {
                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(event.target)) === true);
                    bgl.common.validator.submitForm($(this), configuration);
                }
            });

    }

    function initBackButtonLink() {
        $('.back-button-link')
            .off('click')
            .on('click',
                function (e) {
                    e.preventDefault();
                    $(this).hide();
                    var url = getBackButtonUrl();
                    loadView(url);
                });
    }

    function getBackButtonUrl() {
        var stack = JSON.parse(sessionStorage.getItem(backUrlStackKey));
        var url = stack.pop();

        sessionStorage.setItem(backUrlStackKey, JSON.stringify(stack));

        return url;
    }

    function loadView(url) {
        bgl.common.loader.load(configuration,
            url,
            containerId,
            function () {
                bgl.components.driverdetails.overnightparking.init(configuration, containerId);
            });
    }

    function calculateAverageMiles(value) {
        var label = $("#" + configuration.Id + "__annual-mileage-label");
        var labelTemplateValue = $("#labelTemplate").text();

        if (value !== undefined && value !== "") {
            var averageMiles = Math.round(value / 52, 0);

            if (averageMiles === 0) {
                label.hide();
            } else {
                label.show();
                var text = $.parseHTML(labelTemplateValue.replace("{mileage}", averageMiles));
                label.html(text);
            }
        }
    }

    function onSuccess(configuration, responseData) {
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: responseData.hasChanges, HasChanges: responseData.hasChanges });
        bgl.components.editaddress.postAddressChange(configuration, responseData.redirectUrl, getData(form));
    }

    function getData(form) {
        var token = form.find('input[name="__RequestVerificationToken"]').val();
        var data = bgl.common.datastore.getData(form) || {};

        data.Address = bgl.common.datastore.getData('Address');
        data.AnnualMileage = bgl.common.datastore.getData('AnnualMileage');
        data.VehicleOvernightParking = bgl.common.datastore.getData('OvernightParking');
        data.__RequestVerificationToken = token;
        data.ComponentConfiguration = configuration;

        return data;
    }

    function error(data) {
    }

    return {
        init: initComponent,
        load: loadComponent,
        onSuccess: onSuccess,
        onError: error
    };
})();