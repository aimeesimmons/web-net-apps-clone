﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.pncd = (function () {
    var configuration,
        form;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        configuration = componentConfiguration;

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(componentConfiguration) {
        form = $("#pncd-add-on");
        configuration = componentConfiguration;
        shouldRadioBeSelectedOnLoad();
        shouldContinueButtonTextUpdate();
        onRadioButtonClick();
        onNextButtonClick();
        onBackButtonClick();
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
            });

        $('#what-happens-link').off('click').on('click', function (event) {
            event.preventDefault();
            $(this.hash).toggleClass('hide');
        });
    }

    function shouldContinueButtonTextUpdate() {
        if ($('.form-row__input--radio').is(':checked')) {
            updateSkipButtonToNextOptionalExtras();
        }
    }

    function shouldRadioBeSelectedOnLoad() {
        if ($('.form-row__input--radio').is(':checked')) {
            var selectedValue = $('input[name=IsAddonSelected]:checked').val();
            var isValueSetForPncd = sessionStorage.getItem("Pncd-Selected");
            if (selectedValue === "No" && isValueSetForPncd === null) {
                $('.form-row__input--radio').prop('checked', false);
            }
            if (selectedValue === "Yes") {
                sessionStorage.setItem("Pncd-Selected", true);
            }
        }
    }

    function onNextButtonClick() {
        $("#next-button").on("click",
            function (e) {
                e.preventDefault();
                var selectedValue = $('input[name=IsAddonSelected]:checked').val();
                window.location.href = (selectedValue !== undefined && selectedValue === "Yes") ? configuration.ContinueUrl : configuration.NextStepUrl;
            });
    }

    function onRadioButtonClick() {
        $(".form-row__input--radio").off("change").on("change",
            function () {
                updateSkipButtonToNextOptionalExtras();
                var id = $(this).prop('id');
                var isPncdAdded = sessionStorage.getItem("Pncd-Selected");
                if (id === "pncd-no") {
                    if (isPncdAdded === "true") {
                        submitForm();
                    }
                    sessionStorage.setItem("Pncd-Selected", false);
                } else if (id === "pncd-yes") {
                    if (isPncdAdded === "false" || isPncdAdded === null) {
                        submitForm();
                    }
                    sessionStorage.setItem("Pncd-Selected", true);
                }
            });
    }

    function submitForm() {
        form.submit();
        setLinksDisabled(true);
    }

    function onBackButtonClick() {
        $('#back-button').off('click').on('click', function () {
            navigateTo(configuration.PreviousStepUrl);
        });
    }
    function navigateTo(url) {
        window.location = url;
    }
    function successCallback(message) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: message
        });

        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        if (data.message) {
            var params = {
                callback: function () {
                    successCallback(data.message);
                },
                spinnerName: "addonSpinner"
            };

            bgl.common.pubsub.emit("PollBasket", params);

        } else {
            bgl.common.pubsub.emit("PollBasket");
        }
        setLinksDisabled(false);
    }

    function setLinksDisabled(disabled) {
        var elements = $('button, a, select, input, label, [data-tier], .shopping-basket, #Houston');
        elements.prop("disabled", disabled);

        if (disabled) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        }
    }

    function updateSkipButtonToNextOptionalExtras() {
        $("#next-button").html('<strong>Next: </strong>Optional Extra');
    }
    return {
        onSuccess: onSuccess,
        init: initComponent,
        load: loadComponent
    };
})();