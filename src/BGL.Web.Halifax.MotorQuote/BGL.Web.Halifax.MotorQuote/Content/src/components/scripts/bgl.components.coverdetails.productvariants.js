﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.productvariants = (function () {
    var configuration,
        table,
        messageBox;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        table = $('#prod-var-table');
        messageBox = $('#prod-var-message');
        showMessageItem();
        onSelected();
        initHelperLink();
        eventListener();

        if (configuration.RowBuilder === 1) {
            toggleApr();
            toggleAddonLinks();
        }
    }

    function initHelperLink() {
        var links = $('#' + configuration.Id).find('.tooltip');
        links.off('keyup');
        links.off('click');
        links.on({
            'keyup': function (event) {
                var code = event.charCode || event.keyCode;
                var link = $(this);

                if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
                    link.click();
                }
            },
            'click': function (event) {
                event.preventDefault();
                var link = $(this);
                bgl.components.coverdetails.common.load(configuration, link.data("content-url"), link.data("container"));
            }
        });
    }

    function eventListener() {
        bgl.common.pubsub.on('switchPaymentType',
            function(isMonthly) {
                switchPaymentType(isMonthly);
            });
    }

    function switchPaymentType(isMonthly) {
        if (isMonthly === true) {
            $('[data-payment-type="Monthly"]').removeClass('hide');
            $('[data-payment-type="Annual"]').addClass('hide');
        }

        if (isMonthly === false) {
            $('[data-payment-type="Monthly"]').addClass('hide');
            $('[data-payment-type="Annual"]').removeClass('hide');
        }

        toggleApr();

        if (configuration.RowBuilder === 1) {
            toggleAddonLinks();
        }
    }

    function showMessageItem() {
        var tier = table.find(".active:button").data('tier');
        messageBox.find("[data-tier=" + tier + "]").removeClass('hide');
    }

    function onSelected() {
        table.find("button").off("click").on("click",
            function (e) {
                e.stopPropagation();

                if (!$(this).hasClass('active')) {

                    var tier = $(this).data('tier');

                    setButtonText('Select');
                    showMessageBox(tier);
                    table.find(".active").removeClass('active').addClass('ui-state-disabled');
                    table.find("[data-tier=" + tier + "]").addClass('active');
                    $(this).text('Selecting');

                    $('#VariantCode').val($(this).data('variant-code'));
                    $('#Description').val($(this).data('variant-description'));
                    var form = $('#' + configuration.Id + '__form');

                    bgl.common.validator.submitForm(form, configuration);
                    setLinksDisabled(true);
                }
            });
    }

    function setButtonText(text) {
        table.find(".active:button").text(text);
    }

    function successCallback(houstonMessage) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: houstonMessage
        });
        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
        setButtonText('Selected');
    }

    function onSuccess(configuration, data) {
        if (data.houstonMessage) {
            var params = {
                callback: function () {
                    successCallback(data.houstonMessage);
                },
                spinnerName: "addonSpinner"
            };

            bgl.common.pubsub.emit("PollBasket", params);

        } else {
            bgl.common.pubsub.emit("PollBasket");
        }
        setLinksDisabled(false);
    }

    function showMessageBox(tier) {
        messageBox.find("div:not(.hide)").addClass('hide');
        messageBox.find("[data-tier=" + tier + "]").removeClass('hide');
    }

    function setLinksDisabled(disabled) {
        var elements = $('button, a, select, input, label, [data-tier], .shopping-basket, #Houston').not('.tooltip');
        elements.prop("disabled", disabled);

        if (disabled === true) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        };
    }

    function toggleApr() {
        var isMonthly = $('[data-payment-type="Annual"]').hasClass('hide');

        if (isMonthly) {
            $('#apr-table-row').removeClass("hide");
        } else {
            $('#apr-table-row').addClass("hide");
        }
    }
    
    function toggleAddonLinks() {
        var isPremier = $('[data-tier="2"]').hasClass('active');
        var addonLinks = $(".tooltip");

        addonLinks.each(function (i, element) {
            var coverKey = $(element).attr("data-cover-key");

            if (isPremier) {
                var contentUrl = configuration.CoverFeaturePriority[coverKey].ContentUrlPremier;
            } else {
                var contentUrl = configuration.CoverFeaturePriority[coverKey].ContentUrl;
            }

            $(element).attr("data-content-url", contentUrl);
        });
    }

    function onError() {
        setLinksDisabled(false);
        bgl.components.coverdetails.productvariants.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();