﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymentmethods = (function () {

    var configuration;
    var requestVerificationToken;
    var newPaymentMethodType;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration, componentUrl, containerElementId, function () {
            initComponent(componentConfiguration);
        });
    }

    function initComponent(componentConfiguration) {
        requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();
        configuration = componentConfiguration;
        onRadioButtonChange();
        onUpdatingPaymentMethod();
        onClickAddNewPaymentMethod();
    }

    function onUpdatingPaymentMethod() {
        $('#updatePaymentMethod').on('click', function () {
            var linkId = this.id;
            var linkButton = $("#" + linkId);
            var actionUrl = linkButton.data("action-url");

            commitAndUpdatePaymentMethod(actionUrl);
        });
    }

    function commitAndUpdatePaymentMethod(actionUrl) {
        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            nominatedAccountId: $('[data-type=payment-methods]:checked').val()
        };
        newPaymentMethodType = $('[data-type=payment-methods]:checked').attr('data-payment-method-type');
        
        $.ajax({
            url: actionUrl,
            type: "POST",
            cache: false,
            dataType: "json",
            data: dataToPost,
            success: function (result) {
                if (!result.isSuccess) {
                    bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
                } else {
                    logPaymentMethodChange();
                    if (result.viewOptions === "PaymentStatus") {
                        window.location.href = configuration.ApplicationPath + configuration.PaymentStatusUrl;
                    } else if (result.viewOptions === "PaymentMethodAllDone") {
                        window.location.href = configuration.ApplicationPath + configuration.PaymentMethodAllDoneUrl;
                    }
                }
            }
        });
    }

    function logPaymentMethodChange() {
        sessionStorage.setItem('PaymentUpdated', newPaymentMethodType === 'Bank Account' ? 'Direct Debit': 'Card');
    }

    function onClickAddNewPaymentMethod() {
        $('#addNewPaymentMethod').on('click',
            function() {
                $(document.body).removeClass('slide-panel-open');
            });
    }

    function onRadioButtonChange() {
        $('[data-type=payment-methods]').each(function (index, value) {
            $(value).off('change').on('change',
                function () {
                    if ($('#updatePaymentMethod').hasClass('hide')) {
                        $('#updatePaymentMethod').removeClass('hide');
                    }
                });
        });
    };

    function onError() {
    }

    return {
        init: initComponent,
        load: loadComponent,
        onError: onError,
        newPaymentMethodType: newPaymentMethodType
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymentmethods;
}
