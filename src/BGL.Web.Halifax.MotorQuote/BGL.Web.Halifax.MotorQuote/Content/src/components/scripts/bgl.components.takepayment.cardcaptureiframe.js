﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.cardcaptureiframe = (function () {

    function loadIframe(iframeName, cardCaptureUri, secureSessionId, threeDSecureUri) {
        if ($('#Card-Capture-Iframe').length) {
            $('<form name="iFrameSecureSessionForm" id="iFrameSecureSessionForm" target="' + iframeName + '" method="post" action="' + cardCaptureUri + '">' +
                '<input type="hidden" name="Id" id="Id" value="' + secureSessionId + '" />' +
                '<input type="hidden" name="ThreeDSecureUri" id="ThreeDSecureUri" value="' + threeDSecureUri + '" />' +
                '</form>')
                .appendTo('body')
                .submit()
                .remove();
        }
    }

    function clearIframe() {
        $('#Card-Capture-Iframe').contents().find('body').empty();
    }

    return {
        loadIframe: loadIframe,
        clearIframe: clearIframe
    };
})();