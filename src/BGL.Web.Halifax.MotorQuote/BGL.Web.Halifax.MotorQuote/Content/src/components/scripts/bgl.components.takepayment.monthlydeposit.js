﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.monthlydeposit = (function () {
    var componentConfiguration;
    var hideElementsWhen3DSecureIsDisplayedTimeout;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        componentConfiguration.CardSessionData = getSecureSessionData();
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function() {
                initComponent(componentConfiguration);
            });
    }

    function initComponent(configuration) {
        componentConfiguration = configuration;
        bgl.common.datastore.setupReadAndWrite();

        loadIframe();

        setVisibilityOfCardholderNameOther();

        setCardInformation();

        bgl.common.validator.enableMaxLength();

        bgl.common.validator.init($("#MonthlyDepositPaymentForm"),
            {
                onkeyup: function(element) {
	                ValidateElementAndToggleIframe(element);
                },
                hasSubmit: false
            });

        $("#PersonListData_SelectedPersonId").change(
            function() {
                if (IsOtherSelected()) {
                    $("#CardholderNameRow").removeClass("hide");
                    $("#Card-Capture-Iframe").addClass("hide");
                    sessionStorage.setItem("MonthlyDepositSelectedPersonIdText", "");
                    $("#CardholderName").val("");
                    $("#CardholderName").focus();
                } else {
                    $("#CardholderNameRow").addClass("hide");
                    $("#Card-Capture-Iframe").removeClass("hide");
                }

                initCardCapture();
            });

        if (IsOtherSelected()) {
	        ValidateElementAndToggleIframe($("#MonthlyDepositPaymentForm"));
        }

        setupHowToPayButtons();

        $("#Card-Capture-Iframe").on("load", function () {
            hideElementsWhen3DSecureIsDisplayed();
        });
    }


    function initCardCapture() {
        sessionStorage.removeItem("MonthlyDepositCardCaptureUri");
        sessionStorage.removeItem("MonthlyDepositSecureSessionId");
        sessionStorage.removeItem("MonthlyDepositThreeDSecureUri");

        if (hideElementsWhen3DSecureIsDisplayedTimeout) {
            clearTimeout(hideElementsWhen3DSecureIsDisplayedTimeout);
        }

        bgl.components.takepayment.cardcaptureiframe.clearIframe();

        componentConfiguration.CardSessionData = { "SelectedPersonId": $("#PersonListData_SelectedPersonId").val() };

        var dataToPost = {
            __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(),
            configuration: componentConfiguration
        };

        $.ajax({
            url: componentConfiguration.ApplicationPath + "TakePaymentComponent/InitialiseMonthlyCardCapture",
            type: "POST",
            data: dataToPost,
            success: function (data) {
                sessionStorage.setItem("MonthlyDepositCardCaptureUri", data.cardCaptureUri);
                sessionStorage.setItem("MonthlyDepositSecureSessionId", data.secureSessionId);
                sessionStorage.setItem("MonthlyDepositThreeDSecureUri", data.threeDSecureUri);

                loadIframe();
            }
        });       
               
    }

    function loadIframe() {
        var cardSessionData = getSecureSessionData();
        bgl.components.takepayment.cardcaptureiframe.loadIframe(
            'Card-Capture-Iframe',
            cardSessionData.CardCaptureUri,
            cardSessionData.SecureSessionId,
            cardSessionData.ThreeDSecureUri);
    }

    function ValidateElementAndToggleIframe(element) {
	    var valid = $(element).valid();
	    if (valid === true) {
            $("#Card-Capture-Iframe").removeClass("hide");
	    } else {
		    $("#Card-Capture-Iframe").addClass("hide");
	    }
    }

    function IsOtherSelected() {
	    return $("#PersonListData_SelectedPersonId").find(":selected").text() === "Other";
    }

    function hideElementsWhen3DSecureIsDisplayed() {
        if ($("#Card-Capture-Iframe").contents().find("#continue").length === 0) {
            $("#Cardholder-Name-Form-Row").hide();
            $("#CardholderNameRow").hide();
            $("#today-payment-section").hide();
            scrollToCardPayment();
            return;
        }

        hideElementsWhen3DSecureIsDisplayedTimeout = setTimeout(function () {
            hideElementsWhen3DSecureIsDisplayed();
        }, 1000);
    }

    function scrollToCardPayment() {
        var requiredField = $('.card-payment');
        $('html,body').animate({ scrollTop: requiredField.offset().top }, 0);
    }

    function setupHowToPayButtons() {
        setUseMonthlyInstalmentCard("false");

        $("#deposit-new-card").on("click",
            function () {
                $("#deposit-iframe-section").removeClass("hide");
                if (IsOtherSelected()) {
	                ValidateElementAndToggleIframe($("#MonthlyDepositPaymentForm"));
                }
            });

        $("#deposit-instalment-card, #deposit-card-icons-instalment-card").on("click",
            function () {
                setUseMonthlyInstalmentCard("true");
                var actionUrl = $(this).data('action-url');
                window.location.href = actionUrl;
            });

        addCardTypeClassToCardBox();

        addCardTypeForCardIconsClassToCardBox();

        if (instalmentsPaidByDirectDebit()) {
            $("#today-payment-section, #deposit-new-card").addClass("hide");
            $("#deposit-iframe-section").removeClass("hide");
        }
    }

    function setUseMonthlyInstalmentCard(value) {
        sessionStorage.setItem("UseMonthlyInstalmentCard", value);
    }

    function addCardTypeClassToCardBox() {
        $("#deposit-instalment-card").addClass("payment-card--" + sessionStorage.getItem("MonthlyInstalmentCardType"));
    }

    function addCardTypeForCardIconsClassToCardBox() {
        $("#deposit-card-icons-instalment-card").find(".payment-card__type").addClass("payment-card-new--" + sessionStorage.getItem("MonthlyInstalmentCardType"));
    }

    function instalmentsPaidByDirectDebit() {
        return sessionStorage.getItem("MonthlyInstalmentPaymentType") === "DirectDebit";
    }

    function getSecureSessionData() {
        return {
            "Name": sessionStorage.getItem("MonthlyDepositSelectedPersonIdText"),
            "CardCaptureUri": sessionStorage.getItem("MonthlyDepositCardCaptureUri"),
            "SecureSessionId": sessionStorage.getItem("MonthlyDepositSecureSessionId"),
            "SelectedPersonId": sessionStorage.getItem("MonthlyDepositSelectedPersonIdVal"),
            "ThreeDSecureUri": sessionStorage.getItem("MonthlyDepositThreeDSecureUri")
        };
    }

    function setVisibilityOfCardholderNameOther() {
        if ($("#PersonListData_SelectedPersonId").find(":selected").text() === "Other") {
            $("#CardholderNameRow").removeClass("hide");
            $("#CardholderName").focus();
        }
    }

    function setCardInformation() {
        $('#card-info-date').text("Expires: " + sessionStorage.getItem('MonthlyInstalmentCardExpiry'));
        $('#card-info-name').text(sessionStorage.getItem("MonthlyDepositSelectedPersonIdText"));
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();