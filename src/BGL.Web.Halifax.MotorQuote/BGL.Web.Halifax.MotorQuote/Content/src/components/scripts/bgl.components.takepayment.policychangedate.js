﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.policychangedate = (function () {
    var configuration;

    function init(componentConfiguration) {
        configuration = componentConfiguration;

        $('#' + configuration.Id + '_ChangePaymentDate').on('click', function (event) {
            event.preventDefault();

            var container = this.dataset.container;
            var url = this.dataset.actionUrl;

            bgl.common.loader.load(configuration,
                url,
                container,
                function () {
                    bgl.components.takepayment.policychangedate.initSlidePanel();
                });
        });

        var triggerKey = 'TakePayment__PolicyChangeDate__ClickChangePaymentDate';
        var trigger = sessionStorage.getItem(triggerKey);
        if (trigger === 'true') {
            sessionStorage.removeItem(triggerKey);
            $('#' + configuration.Id + '_ChangePaymentDate').click();
        }
    }

    function initSlidePanel() {
        var datePicker = document.body.querySelector('[data-date-picker]');
        var minDate = datePicker.dataset.earliestDate;
        var maxDate = datePicker.dataset.latestDate;
        var defaultDate = datePicker.dataset.selectedDate;

        $(datePicker).datepicker({
            selectOtherMonths: false,
            dateFormat: "dd-mm-yy",
            minDate: minDate,
            maxDate: maxDate,
            defaultDate: defaultDate,
            showOtherMonths: false,
            onChangeMonthYear: function (year, month, inst) {
                setTimeout(function () {
                    $('.ui-datepicker-prev:not(.ui-state-disabled), .ui-datepicker-next:not(.ui-state-disabled)').attr("href", "#");
                    $(".ui-datepicker-calendar tbody a").first().focus();
                    $(".ui-datepicker-calendar tbody a").each(function () {
                        $(this).attr("aria-label", toOrdinalString(parseInt(this.text)) + " " + getMonth(month));
                    });
                });
            },
            onSelect: function (date, inst) {
                inst.inline = false;

                var nominatedDate = this.value;
                var nominatedDateField = document.getElementById('NominatedDate');
                nominatedDateField.value = nominatedDate;

                document.body.querySelector('[data-dispaly-selected-date]')
                    .textContent = nominatedDate.substr(0, 2);
                document.body.querySelector('[data-day-of-month-display]')
                    .textContent = toOrdinalString(parseInt(nominatedDate.substr(0, 2)));

                if (nominatedDateField.value !== document.getElementById('OriginalNominatedDate').value) {
                    document.getElementById('SameDateValidationError')
                        .classList.remove("error");
                    $(".ui-datepicker-calendar .ui-datepicker-current-day a").removeAttr("aria-describedby");
                    document.body.querySelector('[data-change-date-next]').disabled = false;
                }

                $(".ui-datepicker-calendar .ui-datepicker-current-day a").removeAttr("aria-current");
                $(".ui-datepicker-calendar .ui-datepicker-current-day").removeClass("ui-datepicker-current-day").children().removeClass("ui-state-active");
                $(".ui-datepicker-calendar tbody a").each(function () {
                    if ($(this).text() == inst.selectedDay) {
                        $(this).addClass("ui-state-active");
                        $(this).parent().addClass("ui-datepicker-current-day");
                        $(this).attr("aria-current", "date");
                    }
                });
            }
        });

        $(".ui-datepicker-calendar tbody a").each(function () {
            $(this).attr("aria-label", toOrdinalString(parseInt(this.text)) + " " + getMonth(defaultDate.substring(3, 5)));
        });
        $(".ui-datepicker-calendar .ui-datepicker-current-day a").attr("aria-current", "date");
        $('.ui-datepicker-prev:not(.ui-state-disabled), .ui-datepicker-next:not(.ui-state-disabled)').attr("href", "#");

        document.body.querySelector('[data-change-date-next]')
            .addEventListener('click', function () {
                var nominatedDate = document.getElementById('NominatedDate').value;
                var originalNominatedDate = document.getElementById('OriginalNominatedDate').value;
                if (nominatedDate === originalNominatedDate) {
                    this.disabled = true;
                    document.getElementById('SameDateValidationError')
                        .classList.add("error");
                    $(".ui-datepicker-calendar .ui-datepicker-current-day a")
                        .attr("aria-describedby", "SameDateValidationErrorMessage")
                        .first().focus();
                } else {
                    document.body
                        .querySelector('[data-change-date-calendar-section]')
                        .classList.add("hide");
                    document.body
                        .querySelector('[data-change-date-review-section]')
                        .classList.remove("hide");
                }
            });

        var backElements = document.body.querySelectorAll('[data-change-date-back]');

        for (var i = 0, backElement; backElement = backElements[i]; i++) {
            backElement.addEventListener('click',
                function () {
                    document.body
                        .querySelector('[data-change-date-review-section]')
                        .classList.add("hide");
                    document.body
                        .querySelector('[data-change-date-calendar-section]')
                        .classList.remove("hide");
                });
        }

        var form = document.getElementById(configuration.Id + "_PolicyChangeDateForm");
        var $form = $(form);
        bgl.common.datastore.init($form);
        bgl.common.validator.init($form);

        form.addEventListener("submit", function (event) {
            event.preventDefault();
            if (bgl.common.datastore.isDataChanged($form)) {
                bgl.common.validator.submitForm($form, configuration);
            } else {
                bgl.common.datastore.removeSessionStorage($form);
                onSuccess();
            }
        });
    }
    function onSuccess(config,data) {
        if (data) {
            if (data.UpdatePaymentDateState === 2) {
                document.body
                    .querySelector('[data-invalid-date-review-section]')
                    .classList.remove("hide");
                return;
            }
        }
        document.body
            .querySelector('[data-change-date-review-section]')
            .classList.add("hide");
        document.body
            .querySelector('[data-change-date-done-section]')
            .classList.remove("hide");

        var dismissElements = document.body.querySelectorAll('[data-dismiss="slide-panel"]');

        for (var i = 0, dismissElement; dismissElement = dismissElements[i]; i++) {
            dismissElement.addEventListener('click', refreshPage);
        }
    }

    function refreshPage() {
        window.location.reload();
    }

    function toOrdinalString(n) {
        return n + (["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th");
    }

    function getMonth(intMonth) {
        return [
            "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ][parseInt(intMonth) - 1];
    }

    return {
        init: init,
        initSlidePanel: initSlidePanel,
        onSuccess: onSuccess
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.takepayment.policychangedate;
}