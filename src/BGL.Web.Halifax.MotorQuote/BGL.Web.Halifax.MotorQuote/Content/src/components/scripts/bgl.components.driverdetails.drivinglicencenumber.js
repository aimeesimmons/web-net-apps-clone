﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.driverdetails = bgl.components.driverdetails || {};

bgl.components.driverdetails.drivinglicencenumber = (function () {

    var configuration, form;
    var hasPreselectedValues;

    function initComponent(componentConfiguration) {

        initObject(componentConfiguration);

        bgl.common.datastore.init(form, false, true);
    }

    function initObject(componentConfiguration) {
        configuration = componentConfiguration;

        form = $("[data-component-id='" + configuration.Id + "'] form");

        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        bgl.common.validator.init(form);

        initRadioButtons();

        form.off("submit").on("submit",
            function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)) === true);

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });
    }

    function initRadioButtons() {
        hasPreselectedValues = form.find("input[type=radio]:checked").length !== 0;

        if (hasPreselectedValues === true) {
            showContinueButton();
            setDrivingLicenceRowVisibility(form.find("input[type='radio']:checked").val().toLowerCase() === "true");
        }

        $('#has-driving-licence-number-yes').on("click",
            function () {
                setDrivingLicenceRowVisibility(true);

                hasPreselectedValues = true;
                showContinueButton();
            });

        $('#has-driving-licence-number-no').on("click",
            function (event) {
                setDrivingLicenceRowVisibility(false);

                if (hasPreselectedValues === false) {
                    $(event.target).prop("checked", "checked");
                    continueHandler(event);
                }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function showContinueButton() {
        var continueButton = $(form.find("[type='submit']"));
        continueButton.removeClass("hide");
    }

    function setDrivingLicenceRowVisibility(needToShow) {
        var drivingLicenceRow = form.find("#driving-licence-number-row");

        if (needToShow === true) {
            drivingLicenceRow.removeClass('hide');
        } else {
            drivingLicenceRow.addClass('hide');
        }
    }

    function continueHandler(event) {
        event.preventDefault();

        if ($(event.target).is(":checked")) {
            $(event.target).trigger("change");
        }

        form.submit();
    }

    function onSuccess(configuration, data) {
        if (data.hasDrivingLicenceNumber === false) {
            bgl.common.datastore.deleteFields(form, ["DrivingLicenceNumber"]);
        }

        if (data.needUpdatePerson === true) {

            form.attr("action", data.patchUrl);

            $("#HasChanges").val(bgl.common.datastore.isDataChanged(form) === true);

            var currentData = bgl.common.datastore.getData(form);

            var dataForValidate = $(form).serialize()
                + '&' + $.param(currentData)
                + '&' + $.param({ ComponentConfiguration: configuration, ViewName: "DrivingLicenceNumber" });

            bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

            bgl.common.validator.submitForm(form, configuration, dataForValidate);
            return;
        }

        location.href = data.nextDtsPageUrl;
    }

    function onError() {
        bgl.components.driverdetails.drivinglicencenumber.init(configuration);

        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();
