﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.refundsummary = (function () {
    var componentConfig;
    var requestVerificationToken;

    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        componentConfig = componentConfiguration;

        bgl.components.takepayment.cancellations.init(componentConfiguration);

        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function () {
                initComponent();
            });
    }

    function initComponent() {
        var confirmButton = $("[data-refund-confirm-button]");
        var cancellationConfirmationCheckbox = $("#cancellation-confirmation-checkbox");

        confirmButton.on("click",
            function () {
                commitRefund();
            });

        cancellationConfirmationCheckbox.on("click",
            function() {
                var isChecked = cancellationConfirmationCheckbox.is(":checked");

                if (isChecked) {
                    confirmButton.removeAttr("disabled");
                } else {
                    confirmButton.attr("disabled", "disabled");
                }
            });
    }

    function commitRefund() {
        requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

        var dataToPost = {
            __RequestVerificationToken: requestVerificationToken,
            configuration: componentConfig
        };

        $("body").html(bgl.common.spinner.getSpinnerBody("Processing your refund!",
            "Please do not hit the back button or refresh the page"));

        var urlPrefix = componentConfig.ApplicationPath !== null ? componentConfig.ApplicationPath : "/";

        $.ajax({
            url: urlPrefix + "TakePaymentComponent/CommitRefund",
            type: "POST",
            cache: false,
            dataType: "json",
            data: dataToPost,
            success: function (result) {
                if (result.isSuccess === false) {
                    bgl.common.utilities.redirectToUrl(result.errorRedirectUrl);
                    return;
                }
                bgl.common.utilities.redirectToUrl(result.RedirectUrl);
            }
        });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();