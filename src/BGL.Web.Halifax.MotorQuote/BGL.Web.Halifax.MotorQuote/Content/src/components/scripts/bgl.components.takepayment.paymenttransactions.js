﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.paymenttransactions = (function () {
    var componentConfig;
    function loadComponent(componentConfiguration, componentUrl, containerElementId) {
        bgl.common.loader.load(componentConfiguration,
            componentUrl,
            containerElementId,
            function() {
                initComponent();
            });
    }

    function initComponent(componentConfiguration) {
        componentConfig = componentConfiguration;

        $("[data-content='tab-button']").each(function () {
            $(this).click(
                function () {
                    if ($(this.parentNode).hasClass("active"))
                        return;

                    $("[data-content='tab-button']").each(function() {
                        this.parentNode.classList.toggle("active");
                        $(this.parentNode).hasClass("active") ? $(this).attr("aria-selected", "true") : $(this).attr("aria-selected", "false");
                    });
                    $("[data-content='tab-payment-activity']").each(function() {
                        this.classList.toggle("active");
                    });
                }
            );
        });

        $("[data-action='toggle-transactions']").click(
            function (e) {
                $("[data-content='all-transactions']").each(function() {
                    this.classList.toggle("hide");
                });
                e.target.innerHTML === "View all transactions"
                    ? e.target.innerHTML = "Hide transactions"
                    : e.target.innerHTML = "View all transactions";
            });
    }

    return {
        init: initComponent,
        load: loadComponent
    };
})();

if (typeof module !== "undefined") {
    module.exports = bgl.components.takepayment.paymenttransactions;
}
