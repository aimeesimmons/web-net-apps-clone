var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.additionaldetails = bgl.components.additionaldetails || {};

bgl.components.additionaldetails.paymentsregularityhiddenemailtel = (function () {
    var configuration, form;

    function initComponent(componentConfiguration) {
        setCommonVariables(componentConfiguration);
        updateHiddenInputsWithSessionStorageValues(configuration.SecondaryDataStoreKey);
        setUpDataStore();
        setUpKeyboardNavigationForAccessibility();
        setUpFormValidation(form);
        setUpFormSubmissionEvents();
    }

    function setCommonVariables(componentConfiguration) {
        configuration = componentConfiguration;
        form = $('#payments-regularity-form');
    }

    function updateHiddenInputsWithSessionStorageValues(dataStoreKey) {
        var sessionStorageData = getSessionStorageData(dataStoreKey);
        var hiddenInputs = getHiddenInputs();
        updateInputValues(hiddenInputs, sessionStorageData);
    }

    function getSessionStorageData(key) {
        return JSON.parse(sessionStorage.getItem(key));
    }

    function getHiddenInputs() {
        return $(".hidden-input");
    }

    function updateInputValues(inputs, data) {
        inputs.each(function () {
            var input = $(this);
            var key = getDataStoreAttribute(input);
            var newInputValue = data[key];
            updateInputValue(input, newInputValue);
        });
    }

    function getDataStoreAttribute(element) {
        return element.data("store");
    }

    function updateInputValue(inputElement, value) {
        inputElement.val(value);
    }

    function setUpDataStore() {
        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();
    }

    function setUpKeyboardNavigationForAccessibility() {
        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function setUpFormValidation(form) {
        bgl.common.validator.init(form);
    }

    function setUpFormSubmissionEvents() {
        form.off('submit').on('submit', function (event) {
            event.preventDefault();
            logToAppInsights({ message: "Form submission requested" });
            if (bgl.common.validator.isFormValid(form)) {
                logToAppInsights({ message: "Form validation passed" });
                var data = getDataForFormSubmission();
                logToAppInsights({ message: "Submitting form" });
                bgl.common.validator.submitForm(form, configuration, data);
            } else {
                logToAppInsights({ message: "Form validation failed" });
                logToAppInsights({ message: "Form not submitted" });
            }
        });
    }

    function getDataForFormSubmission() {
        var data = bgl.common.datastore.getData(form);
        data.__RequestVerificationToken = getRequestVerificationTokenValue();
        data.ComponentConfiguration = configuration;

        return data;
    }

    function getRequestVerificationTokenValue() {
        return $('input[name="__RequestVerificationToken"]').val();
    }

    function onSuccess() {
        logToAppInsights({ message: "Form submission successful" });
        var options = {
            callback: function () {
                bgl.common.utilities.redirectToUrl(configuration.NextPageUrl);
            },
            hideNotification: true
        };

        sessionStorage.clear();
        bgl.common.pubsub.emit('PollBasket', options);
    }

    function onError() {
        bgl.components.additionaldetails.paymentsregularityhiddenemailtel.init(configuration);
        logToAppInsights({ message: "Form submission error" });
    }

    function logToAppInsights(props) {
        if (window.appInsights !== undefined) {
            var appInsights = window.appInsights;
            console.log(`track event -> ${JSON.stringify(props)}`);
            appInsights.trackEvent("paymentsSchedule MVT Submission Event", { properties: JSON.stringify(props) });
            appInsights.flush();
        }
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();