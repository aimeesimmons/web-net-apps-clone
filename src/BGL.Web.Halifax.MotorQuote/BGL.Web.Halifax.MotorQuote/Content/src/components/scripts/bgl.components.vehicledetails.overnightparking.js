﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.vehicledetails = bgl.components.vehicledetails || {};

var overnightParking = "OvernightParkingCode";

bgl.components.vehicledetails.overnightparking = (function () {
    var doneButton,
        radioButtons,
        configuration,
        form;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;
        form = $("[data-component-id='" + configuration.Id + "'] form");
        doneButton = $('#vehicle-overnight_done-button');
        radioButtons = $("input[type='radio']");
        
        bgl.common.datastore.init(form, false, true);
        bgl.common.datastore.setupReadAndWrite();

        subscribeToDoneEvents();
        subscribeToRadioButtonEvents();
    }

    function subscribeToRadioButtonEvents() {
        var hasValue = radioButtons.is(':checked');

        if (!hasValue) {
            doneButton.addClass("hide");
        } else {
            doneButton.removeClass("hide");
        }

        $.each(radioButtons,
            function(key, item) {
                var button = $(item);

                button.on("change",
                    function (event) {
                        event.preventDefault();
                        if (!hasValue) {
                            form.submit();
                        }
                    });
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function subscribeToDoneEvents() {

        form.off("submit").on("submit",
            function (event) {
                event.preventDefault();

                if (bgl.common.validator.isFormValid($(this))) {

                    $("#HasChanges").val(bgl.common.datastore.isDataChanged($(this)));

                    bgl.common.validator.submitForm($(this), configuration);
                }
            });
    }

    function onSuccess() {
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: false });

        goToNextStep();
    }

    function goToNextStep() {
        bgl.common.utilities.redirectToUrl(configuration.NextStepUrl);
    }

    function onError() {
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });

        bgl.components.vehicledetails.overnightparking.init(configuration);
    }

    return {
        init: initComponent,
        onSuccess: onSuccess,
        onError: onError
    };
})();