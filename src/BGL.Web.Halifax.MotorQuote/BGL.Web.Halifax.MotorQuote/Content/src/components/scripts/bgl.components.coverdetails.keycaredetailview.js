﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.coverdetails = bgl.components.coverdetails || {};

bgl.components.coverdetails.keycaredetailview = (function () {
    var configuration,
        form;

    function initComponent(componentConfiguration) {
        form = $("#keycare-form");
        configuration = componentConfiguration;
        if (configuration.DetailUrl != null) {
            sessionStorage.setItem(configuration.SelectedAddon, configuration.DetailUrl);
        }
        changeNextButtonText();
        shouldRadioBeSelectedOnLoad();
        onRadioButtonClick();
        onNextButtonClick();
        bgl.components.coverdetails.common.onAddonBackButtonClick();
        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();
                    if (bgl.common.validator.isFormValid(form)) {
                        bgl.common.validator.submitForm(form, configuration);
                    }
            });

        form.find("label").on("keyup", bgl.common.keyboardAccessibility.generateClickOnKeypress);
    }

    function changeNextButtonText() {
        var nextaddonvalue = $("#next-addon").val();
        $('#next-button-span').text(nextaddonvalue ? "Optional Extras" : "Review details");
    }

    function shouldRadioBeSelectedOnLoad() {
        if ($('.form-row__input--radio').is(':checked')) {
            var selectedValue = $('input[name=IsAddonSelected]:checked').val();
            var isValueSetForKeycare = sessionStorage.getItem("Keycare-Selected");
            if (selectedValue === "No" && isValueSetForKeycare === null) {
                $('.form-row__input--radio').prop('checked', false);
            }
            if (selectedValue === "Yes") {
                sessionStorage.setItem("Keycare-Selected", true);
            }
        }
    }

    function onNextButtonClick() {
        $("#next-button").on("click",
            function (e) {
                e.preventDefault();
                var nextaddonvalue = $("#next-addon").val();
                if ($('.form-row__input--radio').is(':checked')) {
                    if (nextaddonvalue) {
                        var nextaddonNonSelected = sessionStorage.getItem(("NonSelectedAddon").concat(nextaddonvalue));
                        var nextAddonSelected = sessionStorage.getItem(("SelectedAddon").concat(nextaddonvalue));
                        if (nextaddonNonSelected || nextAddonSelected) {
                            if (nextAddonSelected) {
                                window.location.href = nextAddonSelected;
                            } else {
                                window.location.href = nextaddonvalue;
                            }
                        }
                        else {
                            var nextaddonDetailUrl = sessionStorage.getItem(nextaddonvalue);
                            if (nextaddonDetailUrl) {
                                window.location.href = nextaddonDetailUrl;
                            } else {
                                window.location.href = nextaddonvalue;
                            }
                        }
                    } else {
                        window.location.href = configuration.NextStepUrl;
                    }
                } else {
                    displayValidationMessage();
                }
            });
    }

    function onRadioButtonClick() {
        $(".form-row__input--radio").off("change").on("change",
            function () {
                if ($('#button-panel').hasClass('error-text__heading')) {
                    removeValidationMessage();
                }
                bgl.common.focusHolder.setFocusedElement($(this).data("focusid"));
                var id = $(this).prop('id');
                var isKeycareAdded = sessionStorage.getItem("Keycare-Selected");
                if (id === "keycare-no") {
                    if (isKeycareAdded === "true") {
                        submitForm();
                    }
                    sessionStorage.setItem("Keycare-Selected", false);
                } else if (id === "keycare-yes") {
                    if (isKeycareAdded === "false" || isKeycareAdded === null) {
                        submitForm();
                    }
                    sessionStorage.setItem("Keycare-Selected", true);
                }
            });
    }

    function removeValidationMessage() {
        $('#addon-questions').removeClass('addon-question-error');
        $('#button-panel').removeClass('error-text__heading');
        $('.form-row__validation-text').hide();
    }

    function displayValidationMessage() {
        $('#addon-questions').addClass('addon-question-error');
        $('#button-panel').addClass('error-text__heading');
        $('.form-row__validation-text').show();
    }

    function submitForm() {
        form.submit();
        setLinksDisabled(true);
    }

    function successCallback(message) {

        var notification = JSON.stringify({
            cat: "pricechange",
            msg: message
        });

        sessionStorage.setItem("notification", notification);
        window.location.reload(true);
    }

    function onSuccess(configuration, data) {
        var message = data.houstonMessage || data.message;
        if (message) {
            var params = {
                callback: function () {
                    successCallback(message);
                },
                spinnerName: "addonSpinner"
            };
            bgl.common.pubsub.emit("PollBasket", params);
        } else {
            bgl.common.pubsub.emit("PollBasket");
        }
        setLinksDisabled(false);
    }

    function setLinksDisabled(disabled) {
        var elements = $('button, a, select, input, label, [data-tier], .shopping-basket, #Houston');
        elements.prop("disabled", disabled);

        if (disabled) {
            elements.addClass('disabled');
        } else {
            elements.removeClass('disabled');
        }
    }

    function onError() {
        setLinksDisabled(false);
        init(configuration);
    }

    return {
        onSuccess: onSuccess,
        init: initComponent,
        setLinksDisabled: setLinksDisabled,
        onError: onError
    };
})();