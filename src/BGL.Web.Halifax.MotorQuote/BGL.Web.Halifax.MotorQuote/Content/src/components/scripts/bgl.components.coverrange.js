﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

var bglDatePickerViewOption = 0;

bgl.components.coverrange = (function () {
    var configuration;

    function init(componentConfiguration) {
        configuration = componentConfiguration;
        if (configuration.JourneyType === "MultiStepEdit") {
            if (configuration.ViewOptions.PartialDatePicker === bglDatePickerViewOption) {
                bgl.components.bgldatepicker.init(configuration);
            } else {
                bgl.components.datepicker.init(configuration);
            }
        } else {
            initEvents();
        }
    }

    function initEvents() {
        $('.cover-range__button, .start-date_edit-button, .cover-range-widget__button')
            .off()
            .on('click',
                function (e) {
                    e.preventDefault();

                    var $this = $(this);
                    var container = $this.data('container');
                    var url = $this.data('url');

                    bgl.common.loader.load(configuration,
                        url,
                        container,
                        function () {
                            if (configuration.ViewOptions.PartialDatePicker === 0) {
                                bgl.components.bgldatepicker.init(configuration);
                            } else {
                                bgl.components.datepicker.init(configuration);
                            }
                        });
                });
    }
    return {
        init: init
    };
})();

bgl.components.policycoverrange = (function () {

    function init() {
        var progress = $(".cover-range__bar").data("bar-progress") + "%";
        $(".cover-range__bar").css("width", progress);
    }

    return {
        init: init
    };
})();


bgl.components.datepicker = (function () {
    var months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ];

    var datepicker;
    var selectedDate;
    var configuration;
    var form;
    var today;
    var dateFormat = "dd/mm/yy";

    function init(componentConfiguration) {
        configuration = componentConfiguration;
        form = $("#" + configuration.Id + "_form-cover-range");
        bgl.common.datastore.init(form);

        bgl.common.validator.init(form);

        today = configuration.DateToday ? new Date(configuration.DateToday) : new Date();
        var tomorrow = new Date(today.getTime());
        tomorrow.setDate(tomorrow.getDate() + 1);

        $('.date-picker__sub-button--today').on('click',
            function (e) {
                e.preventDefault();
                datepicker.datepicker("setDate", today);
                updateSelectedDate(getFormattedValue(today), configuration.IsService);
            });

        $('.date-picker__sub-button--tomorrow').on('click',
            function (e) {
                e.preventDefault();
                datepicker.datepicker("setDate", tomorrow);
                updateSelectedDate(getFormattedValue(tomorrow), configuration.IsService);
            });

        datepicker = $("#datepicker");
        selectedDate = new Date(datepicker.data("selected-date"));
        var earliestDate = new Date(datepicker.data("earliest-date"));
        var latestDate = new Date(datepicker.data("latest-date"));

        datepicker.datepicker({
            selectOtherMonths: false,
            minDate: earliestDate,
            maxDate: latestDate,
            defaultDate: selectedDate,
            showOtherMonths: false,
            dateFormat: dateFormat,
            onChangeMonthYear: function (year, month, inst) {
                setTimeout(function () {
                    $('.ui-datepicker-prev:not(.ui-state-disabled), .ui-datepicker-next:not(.ui-state-disabled)').attr("href", "#");
                    $(".ui-datepicker-calendar tbody a").first().focus();
                    $(".ui-datepicker-calendar tbody a").each(function () {
                        $(this).attr("aria-label", toOrdinalString(parseInt(this.text)) + " " + getMonthString(month - 1));
                    });
                });
            },
            onSelect: function (dateText, inst) {
                inst.inline = false;
                updateSelectedDate(dateText, configuration.IsService);
            }
        });

        $("#" + configuration.Id + "_form-cover-range button").prop('disabled', false);

        $(".ui-datepicker-calendar tbody a").each(function () {
            $(this).attr("aria-label", toOrdinalString(parseInt(this.text)) + " " + getMonthString(selectedDate.getMonth()));
        });
        $('.ui-datepicker-prev:not(.ui-state-disabled), .ui-datepicker-next:not(.ui-state-disabled)').attr("href", "#");

        updateSelectedDate(getFormattedValue(selectedDate), configuration.IsService);
        setBackButtonUrl(configuration);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (!bgl.common.datastore.isDataChanged(form)) {
                        if (configuration.JourneyType === "MultiStepEdit") {
                            onSuccess();
                        } else {
                            $('[data-dismiss="slide-panel"]').click();
                            bgl.common.datastore.removeSessionStorage(form);
                            return;
                        }
                    }
                    bgl.common.validator.submitForm(form, configuration);
                });
    }

    function getMonthString(num) {
        var month = months[num];
        if (month) return month;

        return "Invalid month";
    }

    function updateSelectedDate(dateText, isService) {
        $('#SelectedEffectiveDate').val(dateText + " 00:00:00");

        var dateTime = $.datepicker.parseDate(dateFormat, dateText);
        var isFuture = dateTime > today;
        var text = "";

        if (isService === true) {
            text = isFuture ? "Cover will start at 00:01 hours" : "Cover will start with immediate effect";

            var selectedEffectiveDateMessage = isFuture ? "Your changes are effective from " + dateText + " 00:01 hours" : "Your changes are effective immediately";
            sessionStorage.setItem("selectedEffectiveDateMessage", selectedEffectiveDateMessage);

        } else {
            text = isFuture ? "Cover will start at 00:01 hours" : "Cover will start as soon as you purchase your policy";
        }

        $('#datePickerSubText').html(text);
        $('.date-picker__selected-day').html(dateTime.getDate());
        $('.date-picker__selected-month').html(getMonthString(dateTime.getMonth()) + " " + dateTime.getFullYear());

        $(".ui-datepicker-calendar .ui-datepicker-current-day a").removeAttr("aria-current");
        $(".ui-datepicker-calendar .ui-datepicker-current-day").removeClass("ui-datepicker-current-day").children().removeClass("ui-state-active");
        $(".ui-datepicker-calendar tbody a").each(function () {
            if ($(this).text() == dateTime.getDate()) {
                $(this).addClass("ui-state-active");
                $(this).parent().addClass("ui-datepicker-current-day");
                $(this).attr("aria-current", "date");
            }
        });
    }

    function getFormattedValue(date) {
        return $.datepicker.formatDate(dateFormat, date);
    }

    function toOrdinalString(n) {
        return n + (["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th");
    }

    function setBackButtonUrl(configuration) {
        if (configuration.JourneyType === "MultiStepEdit") {
            var sessionData = JSON.parse(sessionStorage.getItem(configuration.DataStoreKey));
            if (sessionData != null && sessionData.selectedCoverFlag === "Comprehensive") {
                $('#back-button-link').attr("href", configuration.CoverExcessesBackUrl);
            }
        }
    }

    function onSuccess() {
        if (configuration.JourneyType === "MultiStepEdit") {
            if (configuration.IsService) {
                var options = {
                    redirectUrl: configuration.NextStepUrl,
                    hideNotification: true,
                    spinnerName: "serviceSpinner"
                }
                bgl.common.pubsub.emit('PollBasket', options);
            } else {
                window.location.href = configuration.NextStepUrl;
            }
        } else {
            $('[data-dismiss="slide-panel"]').click();
            bgl.common.pubsub.emit('PollBasket');
        }
        bgl.common.datastore.removeSessionStorage(form);
    }

    function onError(configuration) {
        bgl.components.datepicker.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: init,
        onSuccess: onSuccess,
        onError: onError
    };
})();

bgl.components.bgldatepicker = (function () {
    var configuration;
    var today;
    var tomorrow;
    var calendarSection;
    var form;
    var dateFormat = "dd/mm/yy";
    var calendarTabsCount;

    function init(componentConfiguration) {
        configuration = componentConfiguration;
        form = $("#" + configuration.Id + "_form-cover-range");

        calendarSection = $("#calendar-dates");
        calendarTabsCount = $('.calendar-v2-month-year').length;

        bgl.common.datastore.init(form);
        bgl.common.validator.init(form);

        form.off('submit')
            .on('submit',
                function (event) {
                    event.preventDefault();

                    if (!bgl.common.datastore.isDataChanged(form)) {
                        if (configuration.JourneyType === "MultiStepEdit") {
                            onSuccess();
                        } else {
                            $('[data-dismiss="slide-panel"]').click();
                            bgl.common.datastore.removeSessionStorage(form);
                            return;
                        }
                    }
                    bgl.common.validator.submitForm(form, configuration);
                });

        var selectedDate = new Date($(".date-picker-v2").data("selected-date"));
        selectDate($("[data-date='" + getFormattedValue(selectedDate) + "']")[0]);

        today = configuration.DateToday ? new Date(configuration.DateToday) : new Date();
        tomorrow = new Date(today.getTime());
        tomorrow.setDate(tomorrow.getDate() + 1);

        $('.date-picker-v2__sub-button--today').on('click', function (e) {
            e.preventDefault();
            showMonthByIndex(0);
            selectDate($("[data-date='" + getFormattedValue(today) + "']")[0]);
        });

        $('.date-picker-v2__sub-button--tomorrow').on('click', function (e) {
            e.preventDefault();
            var monthIndex = tomorrow.getDate() === 1 ? 1 : 0;
            showMonthByIndex(monthIndex);
            selectDate($("[data-date='" + getFormattedValue(tomorrow) + "']")[0]);
        });

        updateSelectedDate(getFormattedValue(selectedDate), configuration.IsService);

        calendarSection.on("click", onClick);
        calendarSection.on("keydown", onKeydown);

        subscribeHeaderNavigationButtons();
        setBackButtonUrl(configuration);
    }

    function getMonthString(num) {
        var months = [
            "January", "February", "March", "April",
            "May", "June", "July", "August",
            "September", "October", "November", "December"
        ];

        var month = months[num];
        if (month) return month;

        return "Invalid month";
    }

    function updateSelectedDate(dateText, isService) {
        $('#SelectedEffectiveDate').val(dateText + " 00:00:00");

        var dateTime = $.datepicker.parseDate(dateFormat, dateText);
        var isFuture = dateTime > today;
        var text = "";

        if (isService === true) {
            text = isFuture ? "Cover will start at 00:01 hours" : "Cover will start with immediate effect";

            var selectedEffectiveDateMessage = isFuture ? "Your changes are effective from " + dateText + " 00:01 hours" : "Your changes are effective immediately";
            sessionStorage.setItem("selectedEffectiveDateMessage", selectedEffectiveDateMessage);

        } else {
            text = isFuture ? "Cover will start at 00:01 hours" : "Cover will start as soon as you purchase your policy";
        }

        $('#datePickerSubText').html(text);
        $('.date-picker-v2__selected-day').html(dateTime.getDate());
        $('.date-picker-v2__selected-month').html(getMonthString(dateTime.getMonth()) + " " + dateTime.getFullYear());
    }

    function getFormattedValue(date) {
        return $.datepicker.formatDate(dateFormat, date);
    }

    function setBackButtonUrl(configuration) {
        if (configuration.JourneyType === "MultiStepEdit") {
            var sessionData = JSON.parse(sessionStorage.getItem(configuration.DataStoreKey));
            if (sessionData != null && sessionData.selectedCoverFlag === "Comprehensive") {
                $('#back-button-link').attr("href", configuration.CoverExcessesBackUrl);
            }
        }
    }

    function selectDate(date) {
        var selection = calendarSection.find('[aria-current="date"]');

        if (selection) {
            selection.removeAttr("aria-current");
            selection.removeClass("calendar-v2-item--active");
            selection.attr("tabindex", "-1");
        }

        var selectedDate = $(date);
        selectedDate.attr("aria-current", "date");
        selectedDate.addClass("calendar-v2-item--active");
        selectedDate.removeAttr("tabindex");
        var selected = selectedDate.data("date");

        if (selected) {
            updateSelectedDate(selected, configuration.IsService);
        }
    }

    function onKeydown(event) {
        var parent;
        var next;
        var row;
        var index;

        var target = event.target;
        var key = event.key.replace("Arrow", "");

        if ($(target).hasClass("calendar-v2-item") && key.match(/Up|Down|Left|Right|Home|End|PageUp|PageDown/)) {
            switch (key) {
                case "Right":
                    if (target === getLastDate()) {
                        if (!showNextMonth()) return;
                        next = getFirstDate();
                    } else {
                        next = target.nextElementSibling || target.parentElement.nextElementSibling.firstElementChild;
                    }

                    break;

                case "Left":
                    if (target === getFirstDate()) {
                        if (!showPreviousMonth()) return;
                        next = getLastDate();
                    } else {
                        next = target.previousElementSibling || target.parentElement.previousElementSibling.lastElementChild;
                    }

                    break;

                case "Up":
                    if (target === getFirstDate()) {
                        showPreviousMonth();
                        next = getLastDate();
                    } else {
                        parent = target.parentElement;
                        index = getIndexInCollection(parent.children, target);
                        row = parent.previousElementSibling;

                        if (row) {
                            next = row.children.item(index);
                        }
                    }

                    break;

                case "Down":
                    if (target === getLastDate()) {
                        showNextMonth();
                        next = getFirstDate();
                    } else {
                        parent = target.parentElement;
                        index = getIndexInCollection(parent.children, target);
                        row = parent.nextElementSibling;

                        if (row) {
                            next = row.children.item(index);
                        }
                    }

                    break;

                case "Home":
                    next = getFirstDate();
                    break;

                case "End":
                    next = getLastDate();
                    break;

                case "PageUp":
                case "PageDown":
                    if (key === "PageUp") {
                        showPreviousMonth();
                        next = getFirstDate();
                    } else {
                        showNextMonth();
                        next = getLastDate();
                    }

                    break;
            }

            event.preventDefault();

            if (next) {
                $(next).focus();
            }
        }
    }

    function getIndexInCollection(collection, element) {
        for (var i = 0; i < collection.length; i++) {
            if (collection[i] === element) {
                return i;
            }
        }

        return -1;
    }

    function getFirstDate() {
        return $('.calendar-month-dates').not('.hide').find('button').first()[0];
    }

    function getLastDate() {
        return $('.calendar-month-dates').not('.hide').find('button').last()[0];
    }

    function showMonthByIndex(index) {
        var monthsSections = $('[data-month-index]');
        monthsSections.not('.hide').addClass('hide');

        $("[data-month-index='" + index + "']").removeClass('hide');
        subscribeHeaderNavigationButtons();
    }

    function showPreviousMonth() {
        var currentMonth = $('[data-month-index]').not('.hide').data('month-index');
        if (currentMonth === 0) {
            return false;
        }

        --currentMonth;
        showMonthByIndex(currentMonth);
        return true;
    }

    function showNextMonth() {
        var currentMonth = $('[data-month-index]').not('.hide').data('month-index');
        if (currentMonth + 1 >= calendarTabsCount) {
            return false;
        }

        ++currentMonth;
        showMonthByIndex(currentMonth);
        return true;
    }

    function subscribeHeaderNavigationButtons() {
        var currentMonth = $('[data-month-index]').not('.hide');

        var previousButton = currentMonth.find('.previous').not('.disabled');
        previousButton.off('click')
            .on('click',
                function (event) {
                    event.preventDefault();
                    if (!showPreviousMonth()) {
                        return;
                    }
                    var next = getLastDate();
                    $(next).focus();
                });

        var nextButton = currentMonth.find('.next').not('.disabled');
        nextButton.off('click')
            .on('click',
                function (event) {
                    event.preventDefault();
                    if (!showNextMonth()) {
                        return;
                    }
                    var next = getFirstDate();
                    $(next).focus();
                });
    }

    function onClick(event) {
        var target = event.target;

        if ($(target).hasClass("calendar-v2-item--unavailable")) {
            return;
        } else {
            selectDate(target);
        }
    }

    function onSuccess() {
        if (configuration.JourneyType === "MultiStepEdit") {
            if (configuration.IsService) {
                var options = {
                    redirectUrl: configuration.NextStepUrl,
                    hideNotification: true,
                    spinnerName: "serviceSpinner"
                };
                bgl.common.pubsub.emit('PollBasket', options);
            } else {
                window.location.href = configuration.NextStepUrl;
            }
        } else {
            $('[data-dismiss="slide-panel"]').click();
            bgl.common.pubsub.emit('PollBasket');
        }
        bgl.common.datastore.removeSessionStorage(form);
    }

    function onError(configuration) {
        bgl.components.bgldatepicker.init(configuration);
        bgl.common.datastore.extendCollectedData(form, { isFormDirty: true });
    }

    return {
        init: init,
        onSuccess: onSuccess,
        onError: onError
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.coverrange;
}