﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};
bgl.components.takepayment = bgl.components.takepayment || {};

bgl.components.takepayment.cancellations = (function () {

    function init(componentConfiguration) {
        if (componentConfiguration) {
            if (componentConfiguration.MTACancelUrl) {
                sessionStorage.setItem("MTAStartPage", componentConfiguration.MTACancelUrl);
            }
        }
    }

    return {
        init: init
    };
})();