var bgl = window.bgl || {};
bgl.common = bgl.common || {};

bgl.common.pubsub = (function () {
  var events = {};

  function on(eventName, fn) {
    events[eventName] = events[eventName] || [];
    events[eventName].push(fn);
  }

  function off(eventName, fn) {
    if (events[eventName]) {
      for (var i = 0; i < events[eventName].length; i++) {
        if (events[eventName][i] === fn) {
          events[eventName].splice(i, 1);
          break;
        }
      }
    }
  }

  function emit(eventName, data) {
    if (events[eventName]) {
      events[eventName].forEach(function (fn) {
        fn(data);
      });
    }
  }

  function getEventCount(eventName) {
    return events[eventName].length;
  }

  function clearEvents() {
    events = {};
  }

  return {
    on: on,
    off: off,
    emit: emit,
    clear: clearEvents,
    getEventCount: getEventCount,
  };
})();

bgl.common.loader = (function () {
  function makeAjaxCall(options) {
    $.ajax({
      url: options.componentUrl,
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(options.componentConfiguration),
      success: function (data, textStatus, request) {
        if (
          request.getResponseHeader("Content-Type") ===
          "application/json; charset=utf-8"
        ) {
          if (data.isSuccess && data.redirectUrl) {
            if (typeof options.beforePageRedirectCallback === "function") {
              options.beforePageRedirectCallback();
            }

            bgl.common.utilities.redirectToUrl(data.redirectUrl);
          } else if (!data.isSuccess) {
            bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
          }
        } else {
          var htmlChangingFunction = options.htmlChangingFunction;

          htmlChangingFunction.call($("#" + options.containerElementId), data);

          if (typeof options.callback === "function") {
            options.callback();
          }
        }
      },
      error: function (error) {
        console.log(error);
      },
    });
  }

  function logToGA(componentConfiguration, componentUrl) {
    var subCall;
    var sliderGTMPageURL;
    var sliderGTMPageTitle;

    if ($(componentConfiguration)[0].CurrentStep) {
      subCall = "/" + $(componentConfiguration)[0].CurrentStep;
    } else if ($(componentConfiguration)[0].currentStep) {
      subCall = "/" + $(componentConfiguration)[0].currentStep;
    } else if ($(componentConfiguration)[0].EditActionKey) {
      subCall = "/" + $(componentConfiguration)[0].EditActionKey;
    } else {
      subCall = "";
    }

    if (componentUrl && window.dataLayer) {
      sliderGTMPageURL =
        window.location.pathname.replace(/\/$/, "") +
        "/" +
        componentUrl.split("/").pop() +
        subCall;
      sliderGTMPageTitle = componentUrl.split("/").pop() + subCall;

      dataLayer.push({
        event: "MAVirtualPageView",
        virtualPageURL: sliderGTMPageURL,
        virtualPageTitle: sliderGTMPageTitle,
      });
    }
  }

  function load(
    componentConfiguration,
    componentUrl,
    containerElementId,
    callback,
    beforePageRedirectCallback
  ) {
    var options = {
      componentConfiguration: componentConfiguration,
      componentUrl: componentUrl,
      containerElementId: containerElementId,
      callback: callback,
      htmlChangingFunction: $.fn.html,
      beforePageRedirectCallback: beforePageRedirectCallback,
    };

    logToGA(componentConfiguration, componentUrl);
    makeAjaxCall(options);
  }

  function loadAndReplace(
    componentConfiguration,
    componentUrl,
    containerElementId,
    callback,
    beforePageRedirectCallback
  ) {
    var options = {
      componentConfiguration: componentConfiguration,
      componentUrl: componentUrl,
      containerElementId: containerElementId,
      callback: callback,
      htmlChangingFunction: $.fn.replaceWith,
      beforePageRedirectCallback: beforePageRedirectCallback,
    };

    makeAjaxCall(options);
  }

  return {
    load: load,
    loadAndReplace: loadAndReplace,
  };
})();

bgl.common.modal = (function () {
  // Public Functions
  function show(el, event) {
    el.addClass("show");
    el.attr("aria-hidden", false);
    $(document.body).addClass("modal-open");
    showBackdrop();
    event.stopPropagation();
  }

  function hide(el) {
    el.removeClass("show");
    el.attr("aria-hidden", true);
    $(document.body).removeClass("modal-open");
    $(".modal").off("click");
    removeBackdrop();
  }

  function showBackdrop() {
    var backdrop = document.createElement("div");
    backdrop.className = "modal-backdrop fade show";
    $(backdrop).appendTo(document.body);

    $(".modal").on("click", function (e) {
      if (e.target !== e.currentTarget) {
        return;
      }
      hide($(".modal"), e);
    });
  }

  function removeBackdrop() {
    $(".modal-backdrop").remove();
    $(".modal").off("click");
  }

  function loadModalContent(url, $popupContainer, popupConfiguration) {
    var requestData = "";
    if (popupConfiguration) {
      requestData = JSON.stringify(popupConfiguration);
    }

    $.ajax({
      url: url,
      type: "POST",
      contentType: "application/json",
      data: requestData,
      success: function (data) {
        $popupContainer.html(data);
      },
      error: function (error) {
        console.error(error);
      },
    });
  }

  // Event Handlers
  $(document).on("click", '[data-toggle="modal"]', function (event) {
    event.preventDefault();
    var url = $(this).data("url");
    var popupConfiguration = $(this).data("configuration");

    bgl.common.utilities.invoke(
      $(this).data("togglePreModal"),
      $(this).data("togglePreModalParams")
    );

    var $relatedTarget = $($(this).data("target"));
    show($relatedTarget, event);

    if (url) {
      loadModalContent(
        url,
        $relatedTarget.find(".modal-body"),
        popupConfiguration
      );
    }
  });

  $(document).on("click", '[data-dismiss="modal"]', function (event) {
    event.preventDefault();
    var $relatedTarget = $(this).closest(".modal");
    hide($relatedTarget, event);
  });
})();

bgl.common.validator = (function () {
  var validator,
    options = {
      errorClass: "error",
      hasSubmit: true,
    };

  var enterKeyCode = 13;
  var escKeyCode = 27;
  var tabKeyCode = 9;
  var backspaceKeyCode = 8;
  var deleteKeyCode = 46;
  var endKeyCode = 35;
  var downArrowKeyCode = 40;
  var numpadZeroKeyCode = 96;
  var numpadNineKeyCode = 105;
  var zeroKeyCode = 48;
  var nineKeyCode = 57;
  var letterAInLowerKeyCode = 65;

  function initFormValidator(form, optionsParameters) {
    form = form || {};
    optionsParameters = optionsParameters || {};

    $.extend(options, optionsParameters);

    // if jQuery validation is switched on
    if ($.validator) {
      setupCustomValidationRules(form);

      $.validator.unobtrusive.parse(form);

      validator = $(form).validate();

      if (validator) {
        validator.settings.onsubmit = options.onsubmit || false;
        validator.settings.onkeyup = options.onkeyup || false;
        validator.settings.onfocusout = options.onfocusout || false;
        validator.settings.onfocusin = options.onfocusin || false;
        validator.settings.onclick = options.onclick || false;
        validator.settings.focusInvalid = options.focusInvalid || false;
        validator.settings.cleanUp = options.cleanUp || false;
        validator.settings.errorClass = options.errorClass || "error";
        validator.settings.highlight =
          options.highlight ||
          function (element) {
            highlightElement(element);
          };
        validator.settings.unhighlight =
          options.unhighlight ||
          function (element) {
            unhighlighElement(element);
          };
        validator.settings.ignore =
          options.ignore !== undefined
            ? options.ignore
            : validator.settings.ignore;
      }
    }

    return validator;
  }

  function setupCustomValidationRules(form) {
    if (form.find("[data-val-requirefromgroup]")) {
      setRequireAtLeastOneOfGroupValidation();
    }

    if (form.find("[data-val-dateformat]")) {
      setValidationOfDateToFormat();
    }

    if (form.find("[data-val-requiredif]")) {
      setRequiredIfValidation();
    }
  }

  function validateForm(configuration, form) {
    var isValidForm = isFormValid(form);

    // client side validation is passed or skipped if jQuery validation is switched off
    if (isValidForm) {
      submitComponentForm(form, configuration);
    }

    return isValidForm;
  }

  function isFormValid(form) {
    var isValidForm = true;
    validator = getFormValidator(form);

    // try to validate the form using jQuery validation
    if (validator) {
      isValidForm = $(form).valid() && options.hasSubmit;
    }

    if (!isValidForm) {
        scrollToTheFirstInvalidInput();
        getValidationMessages();
    }

    // client side validation is passed or skipped if jQuery validation is switched off
    return isValidForm;
  }

    function getValidationMessages() {
        if ($(".form-row").hasClass("error")) {
            var data = {
                form_section: getFormSectionName($(this)),
                error_details: []
            }

            $('.error').find('.form-row__validation-text').each(function () {
                var error_detail = {
                    error_field: $(this).attr('data-valmsg-for'),
                    error_message: $(this).text()
                };

                data.error_details.push(error_detail);

            });

            bgl.common.pubsub.emit('formValidationFailed', data);
        }
    }

    function getFormSectionName() {
        var formRow = '.form-row__validation-text';

        if ($(formRow).parents().find('.step-form__section-title').text().length === 0) {
            return $(formRow).closest('form').find('h1').text();
        } else {
            return $(formRow).closest('form').find('.step-form__section-title').text();
        }
    }

  function enableMaxLength() {
    $("input[data-val-length-max]").each(function (index, element) {
      var maxLength = $(element).data("val-length-max");

      $(element).attr("maxlength", maxLength);
    });
  }

  function getFormValidator(form) {
    form = form || {};

    if (validator) {
      return validator;
    }

    if ($.validator) {
      $.validator.unobtrusive.parse(form);

      validator = $(form).validate({
        ignore: ".hide",
      });

      $.extend(options, validator.settings);
    }

    return validator;
  }

  function highlightElement(element) {
    element = element || $();
    var parentRow = $(element).closest("div.form-row") || $();

    if (parentRow.length > 0) {
      $(parentRow).addClass(options.errorClass);
    } else {
      $(element).addClass(options.errorClass);
    }
  }

  function unhighlighElement(element) {
    element = element || $();
    var parentRow = $(element).closest("div.form-row") || $();

    if (parentRow.length > 0) {
      $(parentRow).removeClass(options.errorClass);
    } else {
      $(element).removeClass(options.errorClass);
    }

    cleanAriaDescribedbyAttribute(element);
  }

  function cleanAriaDescribedbyAttribute(element) {
    if (!$(element).attr("aria-describedby")) {
      return;
    }

    var currentAriaDescribedBy = $(element).attr("aria-describedBy").split(" ");
    var newAriaDescribedBy = [];

    $(currentAriaDescribedBy).each(function (i, elementID) {
      elementID = elementID.trim();
      if ($("#" + elementID).length) {
        newAriaDescribedBy.push(elementID);
      }
    });

    if (newAriaDescribedBy.length) {
      $(element).attr("aria-describedby", newAriaDescribedBy.join(" "));
    } else {
      $(element).removeAttr("aria-describedby");
    }
  }

  function submitComponentForm(form, configuration, requestBodyData) {
    var disabled = $("form [data-disable-submit][disabled]").length > 0;

    if (!disabled) {
      $("form [data-disable-submit]").attr("disabled", "disabled");

      form = form || $();

      bgl.common.pubsub.emit('formSubmit', form.attr('id') || form.attr('action') || 'unknown');

      var dataToPost =
        requestBodyData ||
        $(form).serialize() +
          "&" +
          $.param({ ComponentConfiguration: configuration });

      $.ajax({
        url: $(form).attr("action"),
        type: $(form).attr("method"),
        data: dataToPost,
        success: function (data) {
          onSuccess(form, configuration, data);
        },
        error: function (error) {
          onError(form, configuration, error.responseText);
        },
      });
    }
  }

  function onSuccess(form, configuration, data) {
    if (data !== undefined && data !== null) {
      if (data.isSuccess === true) {
        callFormEvent($(form), "onSuccess", configuration, data);
      } else {
        if (data.errorRedirectUrl !== undefined) {
          bgl.common.utilities.errorRedirect(data.errorRedirectUrl);
        } else {
          onError(form, configuration, data);
        }
      }
    }
  }

  function onError(form, configuration, error) {
    form = form || $();

    var $error = $(error);
    var $component = $('div[data-component-id="' + configuration.Id + '"]');

    if ($error.length && $component.length) {
      $component.replaceWith($error);
    }

    var $invalidForm = $error.find("#" + form.attr("id"));
    var invalidInputs = {};

    $error.find("input, select").each(function (index, element) {
      unhighlighElement($(element));
      var inputName = $(element).attr("name");
      var message = $('[data-valmsg-for="' + inputName + '"]').text();

      if (message !== "") {
        var htmlMessage = $.parseHTML(message);

        $('[data-valmsg-for="' + inputName + '"]').html(htmlMessage);
        invalidInputs[inputName] = htmlMessage;
      }
    });

    callFormEvent($invalidForm, "onError", configuration, error);

    validator = getFormValidator($invalidForm);

    // if jQuery validation is switched on
    if (validator) {
      validator.showErrors(invalidInputs);
    } else {
      // highlight errors if jQuery validation is switched off
      showErrors(invalidInputs);
    }

    scrollToTheFirstInvalidInput();
  }

  function callFormEvent(form, event, configuration, data) {
    var onEventName = form.data(event);
    var callback = eval(onEventName);

    if (typeof callback === "function") {
      callback(configuration, data);
    }
  }

  function showErrors(invalidInputs) {
    $.each(invalidInputs, function (key) {
      highlightElement($("[name='" + key + "']"));
    });
  }

  function addRequireAtLeastOneOfGroupMethod(value, element, params) {
    var controlsWithValue = $(
      'input[data-val-requirefromgroup-selector="' + params["selector"] + '"]'
    ).filter(function () {
      var controlType = $(this).attr("type");

      return controlType === "checkbox" || controlType === "radio"
        ? $(this).prop("checked") === true
        : $(this).val() !== "";
    });

    return controlsWithValue.length >= params["number"];
  }

  function setRequireAtLeastOneOfGroupValidation() {
    $.validator.addMethod(
      "require_from_group",
      addRequireAtLeastOneOfGroupMethod,
      ""
    );

    $.validator.unobtrusive.adapters.add(
      "requirefromgroup",
      ["number", "selector"],
      function (options) {
        options.rules["require_from_group"] = {
          number: options.params.number,
          selector: options.params.selector,
        };
        options.messages["require_from_group"] = options.message;
      }
    );
  }

  function addRequiredIfValidationMethod(value, element, parameters) {
    var id =
      "[data-dependent-property='" + parameters["dependentproperty"] + "']";

    // get the target value (as a string,
    // as that's what actual value will be)
    var targetValue = parameters["targetvalue"];
    targetValue = (targetValue === null ? "" : targetValue).toString();

    // get the actual value of the target control
    // note - this probably needs to cater for more
    // control types, e.g. radios
    var control = $(id);
    var controlType = control.attr("type");
    var actualValue =
      controlType === "checkbox" || controlType === "radio"
        ? control.filter(":checked").val()
        : control.val();

    actualValue =
      actualValue !== undefined && actualValue !== null
        ? actualValue.toLowerCase()
        : actualValue;

    // if the condition is true, reuse the existing
    // required field validator functionality
    if (
      $.trim(targetValue.toLowerCase()) === $.trim(actualValue) ||
      ($.trim(targetValue) === "*" && $.trim(actualValue) !== "")
    )
      return $.validator.methods.required.call(
        this,
        value,
        element,
        parameters
      );

    return true;
  }

  function setRequiredIfValidation() {
    $.validator.addMethod("requiredif", addRequiredIfValidationMethod);

    $.validator.unobtrusive.adapters.add(
      "requiredif",
      ["dependentproperty", "targetvalue"],
      function (options) {
        options.rules["requiredif"] = {
          dependentproperty: options.params["dependentproperty"],
          targetvalue: options.params["targetvalue"],
        };
        options.messages["requiredif"] = options.message;
      }
    );
  }

  function setValidationOfDateToFormat() {
    $.validator.addMethod("dateformat", validateDateFormat);

    $.validator.unobtrusive.adapters.add(
      "dateformat",
      ["regex", "separator"],
      function (options) {
        options.rules["dateformat"] = {
          regex: options.params["regex"],
          separator: options.params["separator"],
        };
        options.messages["dateformat"] = options.message;
      }
    );
  }

  function validateDateFormat(value, element, parameters) {
    var separator = parameters["separator"];

    var regex = new RegExp(parameters["regex"]);

    if (regex.test(value)) {
      var adata = value.split(separator);

      var day = parseInt(adata[0], 10);
      var month = parseInt(adata[1], 10) - 1;
      var year = parseInt(adata[2], 10);

      var xdata = new Date(year, month, day);

      if (
        xdata.getFullYear() === year &&
        xdata.getMonth() === month &&
        xdata.getDate() === day
      ) {
        return true;
      }
    }

    return false;
  }

  function initAutocompleteSearchInputs(form) {
    var autocompleteSearchInputs = form.find("[autocompletesearch]");

    $.each(autocompleteSearchInputs, function (index, element) {
      var input = $(element);

      $.validator.addMethod(
        input.attr("name"),
        validateAutocompleteSearchInputs,
        input.data("val-required")
      );
    });
  }

  function resetValidationState(input) {
    unhighlighElement(input);
  }

  function validateAutocompleteSearchInputs(value, element) {
    var hiddenItemInputId = $(element).attr("id") + "Id";
    var valueId = $("#" + hiddenItemInputId).val();

    return valueId.length !== 0;
  }

  function scrollToTheFirstInvalidInput() {
    var headerHeight;
    var invalidInput = $(".error, .error-messagebox").first();

    if (invalidInput.length) {
      var header;
      var selector;
      var inputTopPosition = invalidInput.offset().top;

      var slidePanelId = bgl.common.contentpanel.getSlidePanelId();
      var isInputInSlidePanel = invalidInput.closest("#" + slidePanelId).length;

      if (isInputInSlidePanel) {
        header = $("header.panel-header");
        selector = $("#" + slidePanelId);

        var slidePanelContentTopPosition = $(
          "#" + bgl.common.contentpanel.getSlidePanelContentId()
        ).offset().top;
        inputTopPosition -= slidePanelContentTopPosition;
      } else {
        header = $("header.page-header, header.page-headerv2");
        selector = $(window);
      }

      headerHeight = header.height() || 0;

      if (header.css("position") === "fixed") {
        headerHeight *= -1;
      }

      selector.scrollTop(inputTopPosition + headerHeight);
      var invalidInputElem = invalidInput.find("input, select, textarea")[0];
      if (invalidInputElem !== undefined) {
        invalidInputElem.focus();
      }
    }
  }

  function initNumberInputField(component, maxLength) {
    var isZeroAvailable = component.data("val-range-min") === 0;

    $(component)
      .off("keydown")
      .on("keydown", function (e) {
        if (
          $.inArray(e.keyCode, [
            deleteKeyCode,
            backspaceKeyCode,
            tabKeyCode,
            escKeyCode,
            enterKeyCode,
          ]) !== -1 ||
          (e.keyCode === letterAInLowerKeyCode &&
            (e.ctrlKey === true || e.metaKey === true)) ||
          (e.keyCode >= endKeyCode && e.keyCode <= downArrowKeyCode)
        ) {
          return;
        }
        if (
          ((e.shiftKey || e.keyCode < zeroKeyCode || e.keyCode > nineKeyCode) &&
            (e.keyCode < numpadZeroKeyCode || e.keyCode > numpadNineKeyCode)) ||
          $(this).val().length === maxLength ||
          (!isZeroAvailable &&
            e.keyCode === numpadZeroKeyCode &&
            $(this).val().length === 0)
        ) {
          e.preventDefault();
        }
      });

    $(component)
      .off("keyup")
      .on("keyup", function () {
        var val = $(this).val();
        while (val.substring(0, 1) === "0") {
          if (isZeroAvailable && val.length === 1) {
            break;
          }
          val = val.substring(1);
        }
        $(this).val(val);
      });
  }

  function validatePage(buttonId) {
    $("#" + buttonId)
      .off("click")
      .on("click", function (e) {
        e.preventDefault();

        var components = $("[data-component-state='partialerror']");

        if (components !== undefined && components.length > 0) {
          $.each(components, function (index, value) {
            highlightElement($(value));
          });

          scrollToTheFirstInvalidInput();
        } else {
          bgl.common.utilities.redirectToUrl($(this).attr("href"));
        }
      });
  }

  return {
    init: initFormValidator,
    validate: validateForm,
    isFormValid: isFormValid,
    getValidationMessages: getValidationMessages,
    enableMaxLength: enableMaxLength,
    initAutocompleteSearchInputs: initAutocompleteSearchInputs,
    resetValidationState: resetValidationState,
    submitForm: submitComponentForm,
    initNumberInputField: initNumberInputField,
    validatePage: validatePage,
  };
})();

bgl.common.utilities = (function () {
  var globalIdentifier = this;

  function invoke(funcName, funcParams) {
    if (funcName !== undefined && funcName !== null && funcName !== "") {
      var params =
        funcParams !== undefined && funcParams !== null
          ? funcParams.split(",")
          : [];

      var object = funcName.split(".");
      var fn = globalIdentifier;

      for (var i = 0, len = object.length; i < len && fn; i++) {
        fn = fn[object[i]];
      }

      if (typeof fn === "function") {
        fn.apply(null, params);
      } else {
        console.error(funcName + " is not a function.");
      }
    }
  }

  function setIdentifier(indentifier) {
    if (indentifier !== undefined && indentifier !== null) {
      globalIdentifier = indentifier;
    }
  }

  function startUnderwritingPolling() {
    bgl.common.pubsub.emit("PollBasket");
  }

  function getWindowLocation() {
    return window.location;
  }

  function getUrlHashValue() {
    return window.location.hash.substring(1);
  }

  function setUrlHashValue(hash) {
    window.location.hash = hash;
  }

  function errorRedirect(errorRedirectUrl) {
    errorRedirectUrl = errorRedirectUrl.replace(/\?returnUrl=.*$/i, "");
    bgl.common.utilities.redirectToUrl(errorRedirectUrl);
  }

  function redirectToUrl(url) {
    window.location.href = url;
  }

  function refreshPage() {
    window.location.reload(true);
  }

  function toBase64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  function fromBase64(str) {
    return decodeURIComponent(escape(window.atob(str)));
    }

    function getUrlQueryString(parameterName) {
      parameterName = parameterName.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      var regex = new RegExp('[\\?&]' + parameterName + '=([^&#]*)');
      var results = regex.exec(location.search);
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

  return {
    invoke: invoke,
    setIdentifier: setIdentifier,
    startUnderwritingPolling: startUnderwritingPolling,
    getWindowLocation: getWindowLocation,
    getUrlHashValue: getUrlHashValue,
    setUrlHashValue: setUrlHashValue,
    redirectToUrl: redirectToUrl,
    errorRedirect: errorRedirect,
    refreshPage: refreshPage,
    toBase64: toBase64,
    fromBase64: fromBase64,
    getUrlQueryString: getUrlQueryString
  };
})();

bgl.common.cookie = (function () {
  function getCookie(name) {
    var matches = document.cookie.match(
      new RegExp(
        "(?:^|; )" +
          name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
          "=([^;]*)"
      )
    );

    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires === "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }

    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];

      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie + "; path=/";
  }

  function getUrlForCookieAsync(componentUrl) {
    var regex = /\/((\w+)|(\w+\/))$/gi;

    var url = componentUrl.replace(regex, "/UpdateCookie");

    return url;
  }

  function setCookieAsync(
    cookieKey,
    cookieValue,
    updateCookieUrl,
    onSuccess,
    onFailure
  ) {
    $.ajax({
      url: updateCookieUrl,
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({ cookieKey: cookieKey, cookieValue: cookieValue }),
      success: function (data) {
        if (data && data.isSuccess) {
          if (typeof onSuccess === "function") {
            onSuccess();
          }
        }
      },
      error: function (error) {
        if (typeof onFailure === "function") {
          onFailure();
        }
        console.log(error);
      },
    });
  }

  function deleteCookie(name) {
    deleteCookieByNameAndPath(name, "/");
  }

  function deleteCookieByNameAndPath(name, path) {
    setCookie(name, "", { expires: -1, path: path });
  }

  return {
    getCookie: getCookie,
    setCookie: setCookie,
    deleteCookie: deleteCookie,
    deleteCookieByNameAndPath: deleteCookieByNameAndPath,
    setCookieAsync: setCookieAsync,
    getUrlForCookieAsync: getUrlForCookieAsync,
  };
})();

bgl.common.carousel = (function () {
  var carousel, tabs, slides, options;

  function createCarouselBlock() {
    carousel = options.element;
    slides = carousel.find(".carousel__slide");
    tabs = carousel.find(".carousel__navigation-tab");

    var ctrls = $("<ul>")
      .addClass("carousel__controls")
      .html(
        '<li class="carousel__controls-section carousel__controls-section--left">' +
          '<button type="button" class="carousel__button carousel__button--left">' +
          '<img class="carousel__button-image" src="' +
          options.basePath +
          'icon-arrow-left.svg" alt="Previous Item"></button></li>' +
          '<li class="carousel__controls-section carousel__controls-section--right">' +
          '<button type="button" class="carousel__button carousel__button--right">' +
          '<img class="carousel__button-image" src="' +
          options.basePath +
          'icon-arrow-right.svg" alt="Next Item"></button></li>'
      );

    carousel.append(ctrls);
  }

  function changeSlide(direction) {
    slides = carousel.find(".carousel__slide");

    var slidesCount = slides.length,
      activeSlideNumber = parseInt(
        carousel.find(".carousel__slide.active").data("slide")
      );

    if (direction === "left") {
      if (activeSlideNumber > 1) {
        activeSlideNumber--;
      } else if (activeSlideNumber === 1) {
        activeSlideNumber = slidesCount;
      }
    } else if (direction === "right") {
      if (activeSlideNumber < slidesCount) {
        activeSlideNumber++;
      } else if (activeSlideNumber === slidesCount) {
        activeSlideNumber = 1;
      }
    }

    var tab = carousel.find(
      ".carousel__navigation-tab[data-slide='" + activeSlideNumber + "']"
    );
    var slide = carousel.find(
      ".carousel__slide[data-slide='" + activeSlideNumber + "']"
    );

    $.merge(tabs, slides).removeClass("active");
    $.merge(tab, slide).addClass("active");
  }

  function carouselEvents() {
    carousel.find(".carousel__button--left").on("click", function () {
      changeSlide("left");
    });

    carousel.find(".carousel__button--right").on("click", function () {
      changeSlide("right");
    });
  }

  function carouselTabsSetup() {
    $.each(tabs, function () {
      var tab = $(this);
      var slide = $(".carousel__slide[data-slide='" + tab.data("slide") + "']");

      tab.on("click", function () {
        $.merge(tabs, slides).removeClass("active");
        $.merge(tab, slide).addClass("active");
      });
    });
  }

  function carouselTouchScrollSetup() {
    carousel.on("touchstart", function (e) {
      var swipe = e.originalEvent.touches,
        start = swipe[0].pageX,
        distance = 0;

      $(this)
        .on("touchmove", function (e) {
          var contact = e.originalEvent.touches,
            end = contact[0].pageX;

          distance = end - start;
        })
        .one("touchend", function () {
          if (distance < -30) {
            changeSlide("right");
          }
          if (distance > 30) {
            changeSlide("left");
          }
          $(this).off("touchmove touchend");
        });
    });
  }

  function init(opt) {
    options = opt;
    createCarouselBlock();
    carouselEvents();
    carouselTabsSetup();
    carouselTouchScrollSetup();
  }

  return {
    init: init,
  };
})();

bgl.common.spinner = (function () {
  function getSpinnerBody(titleText, bodyText) {
    var htmlEncodedTitle = $("<span>").text(titleText).html();
    var htmlEncodedBody = $("<span>").text(bodyText).html();

    return (
      "<main>" +
      '<div class="page-section">' +
      '<div class="page-section--inner">' +
      '<div class="spinner">' +
      '<div class="spinner__ring"></div>' +
      '<h2 class="spinner__heading">' +
      htmlEncodedTitle +
      "</h2>" +
      '<p class="spinner__text">' +
      htmlEncodedBody +
      "</p>" +
      '<div class="spinner__divider"></div>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</main>"
    );
  }

  return {
    getSpinnerBody: getSpinnerBody,
  };
})();

bgl.common.accordions = (function () {
  var $accordionButtons;

  function getAccordionBody($element) {
    return $element
      .closest(".accordion__section")
      .find(".accordion__body-wrapper");
  }

  function hideAccordionsOnLoad() {
    $accordionButtons = $(".accordion__heading-button");
    $.each($accordionButtons, function () {
      var $accordionButton = $(this);
      var isVisible = isAccordionExpanded($accordionButton);

      if (isVisible === "false") {
        var $accordionBody = getAccordionBody($accordionButton);

        $accordionBody.hide();
      }

      $accordionButton.attr("aria-expanded", isVisible);
    });
  }

  function subscribeForEvents() {
    var accordions = document.querySelectorAll(".accordion__heading-button");

    for (var i = 0; i < accordions.length; i++) {
      if (!accordions[i].onclick) {
        accordions[i].onclick = accordionClickHandler;
      }
    }
  }

  function accordionClickHandler() {
    var $accordionButton = $(this);
    var $accordionBody = getAccordionBody($accordionButton);
    var isExpanded = isAccordionExpanded($accordionButton) === "true";
    if ($accordionButton.data("accordion-key")) {
        sessionStorage.setItem($accordionButton.data("accordion-key"), !isExpanded);
    }

    $accordionButton.attr("aria-expanded", !isExpanded);

    isExpanded ? $accordionBody.hide() : $accordionBody.show();
  }

  function isAccordionExpanded($accordionButton) {
    return (
        ($accordionButton.data("accordion-key") && sessionStorage.getItem($accordionButton.data("accordion-key")))
            || $accordionButton.attr("aria-expanded")
    );
  }

  function init() {
    hideAccordionsOnLoad();
    subscribeForEvents();
  }

  return {
    init: init,
  };
})();

bgl.common.contentpanel = (function () {
  var slidePanelContentObserver = new MutationObserver(setFocus);
  var slidePanelElement;

  function show(el, event) {
    subscribeToEvents();

    el.removeClass("hide-panel").addClass("active");

    $(document.body).addClass("slide-panel-open");
      showBackdrop(el.attr("data-confirmation"));

    var slidePanelContent = document.getElementById(getSlidePanelContentId());
    if (slidePanelContent) {
      slidePanelContentObserver.observe(slidePanelContent, {
        childList: true,
        subtree: true,
      });
    }
    slidePanelElement = $("#" + getSlidePanelId());

    setFocus();

    if (event) {
      event.stopPropagation();
    }
  }

  function subscribeToEvents() {
    bgl.common.pubsub.on("CancelMTAComplete", function () {
      if (sessionStorage.getItem("reload") === null) {
        $("#" + getSlidePanelId()).removeAttr("data-confirmation");
        hide($("#SlidePanel"));
      } else {
        sessionStorage.removeItem("reload");
        window.location.reload();
      }
    });
  }

  function hide(el, event) {
    el.removeClass("active").addClass("hide-panel");
    $(document.body).removeClass("slide-panel-open");
    $("#" + getSlidePanelContentId()).empty();
    slidePanelContentObserver.disconnect();

    if (event) {
      event.stopPropagation();
    }

    removeBackdrop();
    setTimeout(bgl.common.focusHolder.setFocusToElementById, 100);
  }

  function setFocus() {
    if (slidePanelElement.find(":focus").first().length === 0) {
      var autoFocusElement = slidePanelElement.find("[autofocus]").first();
      if (autoFocusElement.length) {
        autoFocusElement.focus();
      } else {
        $("#SlidePanel").focus();
      }
    }
  }

  function showBackdrop(confirmation) {
    if ($("#overlay-pane").length > 0) {
      return;
    }

      $("<div>").attr('id', 'overlay-pane').addClass("overlay show").appendTo(document.body);

    if ($("#mta-always-open-slider")[0]) {
      $("#overlay-pane").on("click", function () {
        var startPage = sessionStorage.getItem("MTAStartPage");
        var options = {
          redirectUrl: startPage
        }
        bgl.common.pubsub.emit('CancelMTA', options);
      });
    } else if (confirmation) {
      $("#overlay-pane").on("click", function () {
        bgl.common.pubsub.emit("CancelMTA");
      });
    } else {
      $("#overlay-pane").on("click", function (e) {
        if (e.target !== e.currentTarget) {
          return;
        }

        hide($("#SlidePanel"), e);
      });
    }
  }

  function removeBackdrop() {
    $("#overlay-pane").remove();
  }

  function hideSlidePanel(event) {
    if (event) {
      event.preventDefault();
    }

    var $relatedTarget = $("#SlidePanel");
    hide($relatedTarget, event);
    $relatedTarget.off();
  }

  $(document).on("click", '[data-toggle="slide-panel"]', function (event) {
    event.preventDefault();
    bgl.common.focusHolder.setFocusedElementId($(this));
    var $relatedTarget = $($(this).data("target"));
    $relatedTarget.find(".slide-panel-content").empty();

    if ($(this).data("confirmation")) {
      $relatedTarget.attr("data-confirmation", true);
    }

    show($relatedTarget, event);
  });

  $(document).on("click", '[data-dismiss="slide-panel"]', function (event) {
    if ($("#" + getSlidePanelId()).attr("data-confirmation") == "true") {
      bgl.common.pubsub.emit("CancelMTA");
      return;
    }
    hideSlidePanel(event);
  });

  $(document).on("keyup", function (event) {
    if (event.key === "Escape" && ($(".slide-panel").hasClass("active") || $("#mta-always-open-slider")[0])) {
      if ($("#mta-always-open-slider")[0]) {
        var startPage = sessionStorage.getItem("MTAStartPage");
        var options = {
          redirectUrl: startPage
        }
        bgl.common.pubsub.emit('CancelMTA', options);
        return;
      } else if ($("#" + getSlidePanelId()).attr("data-confirmation") == "true") {
        bgl.common.pubsub.emit("CancelMTA");
        return;
      }
      hideSlidePanel(event);
    }
  });

  $(document).on("keydown", function (event) {
    var tabKeyCode = 9;

    if ($(".slide-panel").hasClass("active")) {
      if (event.which === tabKeyCode || event.keyCode === tabKeyCode) {
        var firstTabbable = getFirstTabbableElement($(".slide-panel.active"));
        var lastTabbable = getLastTabbableElement($(".slide-panel.active"));
        var houstonButton = $("#houstonButton");

        if (!event.shiftKey) {
          if (
            lastTabbable.is(":focus") ||
            focusIsWithinLastRadioGroup(lastTabbable)
          ) {
            event.preventDefault();

            if (houstonButton.length) {
              if (houstonButton.hasClass("ready")) {
                houstonButton.focus();
              } else {
                $("#nina-block :focusable").first().focus();
              }
            } else {
              firstTabbable.focus();
            }
          } else if (houstonButton.is(":focus")) {
            event.preventDefault();
            firstTabbable.focus();
          }
        } else if (event.shiftKey) {
          if (firstTabbable.is(":focus")) {
            event.preventDefault();

            if (houstonButton.length) {
              houstonButton.focus();
            } else {
              lastTabbable.focus();
            }
          } else if (
            houstonButton.is(":focus") &&
            !houstonButton.hasClass("va")
          ) {
            event.preventDefault();
            lastTabbable.focus();
          } else if ($("#nina-block").find(":focus").length !== 0) {
            event.preventDefault();
            lastTabbable.focus();
          }
        }
      }
    }
  });

  function focusIsWithinLastRadioGroup(lastTabbable) {
    return (
      $(":focus").is('[type = "radio"]') &&
      lastTabbable.is('[type = "radio"]') &&
      $(":focus").attr("name") === lastTabbable.attr("name")
    );
  }

  function getSlidePanelId() {
    return "SlidePanel";
  }

  function getSlidePanelContentId() {
    return "SlidePanelContent";
  }

  function getFirstTabbableElement(el) {
    return el.find(":tabbable").first();
  }

  function getLastTabbableElement(el) {
    return el.find(":tabbable").last();
  }

  function openPanelByAction(linkId, url, options, containerId) {
    options = options || {};

    containerId =
      containerId || bgl.common.contentpanel.getSlidePanelContentId();

    $(document).ready(function () {
      $("#" + linkId)
        .off("click")
        .on("click", function (event) {
          event.preventDefault();

          bgl.common.loader.load(options, url, containerId);
        });
    });
  }

  function openPanelByClick(linkId) {
    var link = $("#" + linkId);

    openPanelByAction(
      linkId,
      link.data("action-url"),
      null,
      link.data("container")
    );
  }

  function loadAndDisplaySidePanel(options, url, callback) {
    bgl.common.loader.load(options, url, getSlidePanelContentId(), callback);
    show($("#" + getSlidePanelId()));
  }

  return {
    getSlidePanelId: getSlidePanelId,
    getSlidePanelContentId: getSlidePanelContentId,
    getFirstTabbableElement: getFirstTabbableElement,
    getLastTabbableElement: getLastTabbableElement,
    openPanelByAction: openPanelByAction,
    openPanelByClick: openPanelByClick,
    loadAndDisplaySidePanel: loadAndDisplaySidePanel,
    hideSlidePanel: hideSlidePanel,
  };
})();

bgl.common.autocompletesearch = (function () {
  var autocompleteMinLength;
  var TAB_KEYCODE = 9;
  var ENTER_KEYCODE = 13;
  var ARROW_UP_KEYCODE = 38;
  var ARROW_DOWN_KEYCODE = 40;
  var activeClass = "ui-menu-item-active";

  function init(symbolsCountToTriggerSearch) {
    autocompleteMinLength = symbolsCountToTriggerSearch || 1;

    var inputs = $(document.body).find("[autocompletesearch]");

    $.each(inputs, function (index, element) {
      var input = $(element);
      prepareDOM(input);

      input.on("keyup", inputKeyUpHandler).autocomplete({
        minLength: symbolsCountToTriggerSearch,
        appendTo: "#" + input.parent().attr("id"),
        source: function (request, response) {
          makeAjaxRequest(input, response);
        },
        response: function (event, ui) {
          var len = ui.content.length;
          $("#span-autocomplete").html(
            len +
              " results are available, use up and down arrow keys to navigate."
          );
        },
        focus: function (event, ui) {
          selectOption(input, ui.item);

          unhighlight($(this));
          highlightActiveElement($(this));

          event.preventDefault();
        },
        select: function (event, ui) {
          selectOption(input, ui.item);

          unhighlight($(this));

          event.preventDefault();
        },
      });

      overrideItemsDrawing(input);
    });
  }

  function highlightActiveElement(input) {
    var activeItem = input.data("ui-autocomplete").menu.active;
    activeItem.addClass(activeClass);
  }

  function unhighlight(input) {
    input.parent().find("ul li").removeClass(activeClass);
  }

  function prepareDOM(input) {
    var inputId = input.attr("id");
    var parentElement = input.parent();
    // Add an identifier for the parent element, if necessary, to know for what block we need to draw list
    // We will have DOM structure:
    // <div>
    //   <input />
    //   <ul></ul>
    // </div>
    var span = $("<span />")
      .attr({ role: "status", "aria-live": "polite", id: "span-autocomplete" })
      .addClass("visually-hidden");
    if (!parentElement.attr("id")) {
      parentElement.attr("id", inputId + "ParentElement");
    }
    parentElement.append(span);
  }

  function inputKeyUpHandler(event) {
    var code = event.charCode || event.keyCode;

    var codesForOptionSelecting = [
      ENTER_KEYCODE,
      ARROW_UP_KEYCODE,
      ARROW_DOWN_KEYCODE,
      TAB_KEYCODE,
    ];

    if (code && codesForOptionSelecting.indexOf(code) !== -1) {
      event.preventDefault();

      // Set value in autocomplete input control and item id into hidden field for it
      var selectedItem = $(this).autocomplete("instance").selectedItem;
      if (selectedItem) {
        selectOption($(this), selectedItem);
      }
    } else {
      // Reset selected item id if we have this field on DOM
      var hiddenFieldForItemId = getHiddenFieldForItemId($(this));
      if (hiddenFieldForItemId.length) {
        hiddenFieldForItemId.val("");
      }

      if ($(this).val().length >= autocompleteMinLength) {
        // Remove validation message for input
        bgl.common.validator.resetValidationState($(this));
      }
    }
  }

  function getHiddenFieldForItemId(input) {
    return $("#" + input.attr("id") + "Id");
  }

  function selectOption(input, item) {
    input.val(item.label);

    var hiddenFieldForItemId = getHiddenFieldForItemId(input);
    if (hiddenFieldForItemId.length) {
      hiddenFieldForItemId.val(item.value);
      hiddenFieldForItemId.trigger("focusout");
    }
  }

  function overrideItemsDrawing(input) {
    input.data("ui-autocomplete")._renderItem = function (ul, item) {
      ul.attr("role", "listbox");
      return $('<li role="option">').append(item.label).appendTo(ul);
    };
  }

  function makeAjaxRequest(input, response) {
    $.ajax({
      url: input.attr("autocompletesearch"),
      data: { enteredData: input.val().trim() },
      type: "GET",
      success: function (data) {
        if (data.IsSuccess) {
          response(
            $.map(data.SearchOptionItems, function (item) {
              return { label: item.Value, value: item.Key };
            })
          );
        } else {
          bgl.common.utilities.errorRedirect(data.ErrorRedirectUrl);
        }
      },
    });
  }

  return {
    init: init,
  };
})();

bgl.common.datastore = (function () {
  var dataStoreAttributeName = "data-store";
  var dropdownTextSuffix = "Text";
  var dropdownValueSuffix = "Val";

  function setupReadAndWrite() {
    setupRead();
    setupWrite();
  }

  function setupRead() {
    $("[data-store]").each(function () {
      var element = $(this);
      var dataStoreItem = readDataStoreValue(element);
      if (element.is("input, textarea")) {
        if (dataStoreItem) {
          if (isRadioButton(element)) {
            if (element[0].defaultValue === dataStoreItem) {
              element.prop("checked", true);
            }
          } else {
            element.val(dataStoreItem);
            if (!isDatePicker(element)) {
              element.focus().focusout();
            }
          }
        } else if (isDatePicker(element)) {
          element.datepicker("setDate", "0");
          writeDataStoreValue(element);
        }
      } else if (element.is("select")) {
        setDropDownToStoredValue(element);
      } else if (element.is("span")) {
        element.html(dataStoreItem);
      } else if (element.is("strong")) {
        element.html(dataStoreItem);
      }
    });
  }

  function setupWrite() {
    $("select[data-store]").change(function () {
      writeDataStoreDropDownValue($(this));
    });

    $("input[data-store]").on("focusout", function () {
      writeDataStoreValue($(this));
    });
    $("input[data-store-keyup]").on("keyup", function () {
      writeDataStoreValue($(this));
    });
    $("input[data-store][type=text], textarea[data-store]").on(
      "keyup",
      function () {
        writeDataStoreValue($(this));
      }
    );
    $("input[data-store][type=number]").on("change", function () {
      writeDataStoreValue($(this));
    });

    $("input[data-store][type=number]").on("keyup", function () {
      writeDataStoreValue($(this));
    });

    $("input[data-store][type=tel]").on("change", function () {
      writeDataStoreValue($(this));
    });

    $("input[data-store][type=tel]").on("keyup", function () {
      writeDataStoreValue($(this));
    });
    $("input[data-store][type=radio]").on("change", function () {
      $(this).focusout();
    });

    $("input[data-store][type=hidden]").each(function () {
      writeDataStoreValue($(this));
    });
  }

  function readDataStoreValue($element, suffix) {
    var journeyKey = getJourneyKey($element);

    var elementValue;
    var keyOfValue = $element.attr(dataStoreAttributeName);

    if (suffix) {
      keyOfValue += suffix;
    }

    if (journeyKey) {
      var journeyData = JSON.parse(sessionStorage.getItem(journeyKey)) || {};
      elementValue = journeyData[keyOfValue];
    } else {
      elementValue = sessionStorage.getItem(keyOfValue);
    }

    return elementValue;
  }

  function writeDataStoreValue($element) {
    var journeyKey = getJourneyKey($element);
    var key = $element.attr(dataStoreAttributeName);
    var elementValue = $element.val();

    if (journeyKey) {
      var journeyData = JSON.parse(sessionStorage.getItem(journeyKey)) || {};

      journeyData[key] = elementValue;
      sessionStorage.setItem(journeyKey, JSON.stringify(journeyData));
    } else {
      sessionStorage.setItem(key, elementValue);
    }
  }

  function writeDataStoreDropDownValue($dropdown) {
    var journeyKey = getJourneyKey($dropdown);
    var key = $dropdown.attr(dataStoreAttributeName);
    var textKey = $dropdown.attr(dataStoreAttributeName) + dropdownTextSuffix;
    var valueKey = $dropdown.attr(dataStoreAttributeName) + dropdownValueSuffix;

    var dropdownText = $dropdown.find(":selected").text();
    var dropdownValue = $dropdown.find(":selected").val();

    if (journeyKey) {
      var journeyData = JSON.parse(sessionStorage.getItem(journeyKey)) || {};

      journeyData[textKey] = dropdownText;
      journeyData[valueKey] = dropdownValue;
      journeyData[key] = dropdownValue;
      sessionStorage.setItem(journeyKey, JSON.stringify(journeyData));
    } else {
      sessionStorage.setItem(textKey, dropdownText);
      sessionStorage.setItem(valueKey, dropdownValue);
      sessionStorage.setItem(key, dropdownValue);
    }
  }

  function getJourneyKey($element) {
    return $element.closest("[data-store-key]").attr("data-store-key");
  }

  function isDatePicker(element) {
    return element.hasClass("form-row__input--datepicker");
  }

  function isRadioButton(element) {
    return element.is("[type=radio]");
  }

  function setDropDownToStoredValue($dropdown) {
    var dataStoreVal = readDataStoreValue($dropdown, dropdownValueSuffix);

    if (dataStoreVal) {
      $dropdown.val(dataStoreVal);
    } else {
      writeDataStoreDropDownValue($dropdown);
    }
  }

  function getFormKey(selector) {
    if (typeof selector === "object" && selector !== null) {
      return selector.data("store-key");
    }

    return selector;
  }

  function init(form, needToResetDirtyFlag, mergeFormsByKey) {
    if (needToResetDirtyFlag === undefined || needToResetDirtyFlag === true) {
      resetDirtyFlag(form);
    }

    var key = getFormKey(form);

    if (key !== undefined && key !== null) {
      if (mergeFormsByKey) {
        var collectedData = JSON.parse(sessionStorage.getItem(key));
        if (!collectedData) {
          collectedData = { isFormDirty: false };
          sessionStorage.setItem(key, JSON.stringify(collectedData));
        }
      } else {
        sessionStorage.setItem(key, buildJson(form));
      }

      subscribeForChange(form);
    }
  }

  function removeSessionStorage(form) {
    var key = getFormKey(form);
    sessionStorage.removeItem(key);
  }

  function getModelFromSessionStorage(key) {
    return JSON.parse(sessionStorage.getItem(key));
  }

  function buildJson(form, isDirty) {
    var key = getFormKey(form);
    var data = getModelFromSessionStorage(key) || {};

    if (data.hasOwnProperty("isFormDirty") !== true) {
      data.isFormDirty = false;
    }

    if (isDirty !== undefined) {
      data.isFormDirty = isDirty;
    }

    expandDataWithHiddenFields(form, data);

    return JSON.stringify(data);
  }

  function isHiddenFieldsChanged(sessionStorageData, form) {
    var formHiddenData = {};
    expandDataWithHiddenFields(form, formHiddenData);

    for (var key in formHiddenData) {
      if (formHiddenData.hasOwnProperty(key)) {
        if (formHiddenData[key] !== sessionStorageData[key]) {
          return true;
        }
      }
    }
    return false;
  }

  function expandDataWithHiddenFields(form, formData) {
    form
      .find("input[type=hidden]")
      .not('input[name="__RequestVerificationToken"]')
      .not("input[data-store-skip]")
      .each(function () {
        var input = $(this);
        formData[input.attr("name")] = input.val();
      });
  }

  function isDataChanged(form) {
    var key = getFormKey(form);

    if (key === undefined || key === null) {
      return false;
    }

    var data = getModelFromSessionStorage(key);

    return data.isFormDirty || isHiddenFieldsChanged(data, form);
  }

  function subscribeForChange(form) {
    var key = getFormKey(form);

    form.off("change").on("change", function () {
      sessionStorage.setItem(key, buildJson(form, true));
    });
  }

  function resetDirtyFlag(form) {
    var key = getFormKey(form);

    if (key !== undefined && key !== null) {
      sessionStorage.setItem(key, buildJson(form, false));
    }
  }

  function extendCollectedData(selector, data) {
    var key = getFormKey(selector);

    if (key) {
      var formData = JSON.parse(sessionStorage.getItem(key));
      formData = $.extend(formData, data);
      sessionStorage.setItem(key, JSON.stringify(formData));
    }
  }

  function deleteFields(selector, fieldNameList) {
    var key = getFormKey(selector);

    if (key) {
      var formData = JSON.parse(sessionStorage.getItem(key));
      if (formData) {
        $.each(fieldNameList, function (key, value) {
          delete formData[value];
        });
      }
      sessionStorage.setItem(key, JSON.stringify(formData));
    }
  }

  function getData(selector) {
    var key = getFormKey(selector);

    if (key !== undefined && key !== null) {
      return getModelFromSessionStorage(key);
    }

    return {};
  }

  return {
    setupRead: setupRead,
    setupWrite: setupWrite,
    setupReadAndWrite: setupReadAndWrite,
    init: init,
    removeSessionStorage: removeSessionStorage,
    isDataChanged: isDataChanged,
    resetDirtyFlag: resetDirtyFlag,
    extendCollectedData: extendCollectedData,
    deleteFields: deleteFields,
    getData: getData,
  };
})();

bgl.common.overlay = (function () {
  var overlayId = "overlay";
  function show() {
    if ($(document.body).find(`#${overlayId}`).length == 0) {
      var overlay = getOverlay();
      $(document.body).append(overlay);
    }
  }

  function hide() {
    $(document.body).find(`#${overlayId}`).remove();
  }

  function getOverlay() {
    return (
      `<div id="${overlayId}" class="addonOverlay show">` +
      '<div class="spinner-wrapper">' +
      '<div class="spinner">' +
      '<div class="addon-spinner"></div>' +
      '</div></div></div>'
    );
  }

  return {
    show: show,
    hide: hide,
    getOverlay: getOverlay,
  };
})();

bgl.common.focusHolder = (function () {
  var focusKey = "focusedElement";

  function setFocusedElement(focusedElement) {
    sessionStorage.setItem(focusKey, focusedElement);
  }

  function setFocusedElementId(focusedElement) {
    var id = $(focusedElement).prop("id");

    sessionStorage.setItem(focusKey, id);
  }

  function setFocusToElement(focusOveride) {
    var lastFocusedOn;

    if (focusOveride) {
      $(focusOveride).focus();
    } else {
      lastFocusedOn = sessionStorage.getItem(focusKey);
      if (lastFocusedOn !== null && lastFocusedOn !== "undefined") {
        $('[data-focusid="' + lastFocusedOn + '"]').focus();
        removeFocused();
      } else {
        $("#header-focus").focus();
      }
    }
  }

  function setFocusToElementById() {
    var lastFocusedOn = sessionStorage.getItem(focusKey);

    if (lastFocusedOn) {
      $("#" + lastFocusedOn).focus();
      removeFocused();
    }
  }

  function removeFocused() {
    sessionStorage.removeItem(focusKey);
  }

  return {
    setFocusedElement: setFocusedElement,
    setFocusToElement: setFocusToElement,
    setFocusToElementById: setFocusToElementById,
    setFocusedElementId: setFocusedElementId,
    removeFocused: removeFocused,
  };
})();

bgl.common.tabulation = (function () {
  $(document).keydown(function (e) {
    if (e.shiftKey && e.which === 9) {
      $(":focusable").focus(function () {
        var elem = $(this);
        var headerHeight = $("#fixed-header").height();

        if (isElementUnderHeader(elem, headerHeight)) {
          var scrollY = elem.offset().top - headerHeight * 2;

          $(window).scrollTop(scrollY);
        }
      });
    }
  });

  function isElementUnderHeader(element, headerHeight) {
    var headerTop = $(window).scrollTop() + headerHeight;
    var elemTop = element.offset().top - headerHeight;

    return elemTop <= headerTop;
  }
})();

bgl.common.helptext = (function () {
  $(document).on("click", "[data-open-close-help-text]", function (event) {
    event.preventDefault();
    $(this).toggleClass("active");
    $(".help-text").fadeToggle();
  });
})();

bgl.common.date = (function () {
  var BglDateFormat = function (value) {
    this.formattedDate = value;
  };

  BglDateFormat.prototype.removeAllCharactersApartFromDigitsAndForwardSlashes = function () {
    this.formattedDate = this.formattedDate.replace(/[^\d\/]/g, "");
    return this;
  };

  BglDateFormat.prototype.addFowardSlashBetweenDayAndMonth = function () {
    this.formattedDate = this.formattedDate.replace(
      /^(\d\d)(\d{1,2})$/g,
      "$1/$2"
    );
    return this;
  };

  BglDateFormat.prototype.appendSingleDigitDayWithZero = function () {
    this.formattedDate = this.formattedDate.replace(/^(\d)\/$/g, "0$1/");
    return this;
  };

  BglDateFormat.prototype.appendSingleDigitMonthWithZero = function () {
    this.formattedDate = this.formattedDate.replace(
      /^(\d\d)\/(\d)\/$/g,
      "$1/0$2/"
    );
    return this;
  };

  BglDateFormat.prototype.addForwardSlashBetweenMonthAndYear = function () {
    this.formattedDate = this.formattedDate.replace(
      /^(\d\d\/\d\d)(\d+)$/g,
      "$1/$2"
    );
    return this;
  };

  BglDateFormat.prototype.preventMoreThanFourYearDigits = function () {
    this.formattedDate = this.formattedDate.replace(
      /^(\d\d\/\d\d\/\d\d\d\d)(\d+)$/g,
      "$1"
    );
    return this;
  };

  BglDateFormat.prototype.generateFormattedDate = function () {
    this.removeAllCharactersApartFromDigitsAndForwardSlashes()
      .addFowardSlashBetweenDayAndMonth()
      .appendSingleDigitDayWithZero()
      .appendSingleDigitMonthWithZero()
      .addForwardSlashBetweenMonthAndYear()
      .preventMoreThanFourYearDigits();
    return this.formattedDate;
  };

  function initDateFormat($input) {
    $input.on("keyup", function () {
      var date = new BglDateFormat($(this).val());
      $(this).val(date.generateFormattedDate());
    });
  }

  return {
    initDateFormat: initDateFormat,
  };
})();

bgl.common.customselect = (function () {
  var DOWN_ARROW_KEY_CODE = 40;
  var UP_ARROW_KEY_CODE = 38;
  var TAB_KEY_CODE = 9;

  function toggleOptions(control, callback) {
    var container = control.find("[data-custom-select-container]");

    if (container.hasClass("active")) {
      closeOptions(control, callback);
    } else {
      openOptions(control, callback);
    }
  }

  function openOptions(control, callback) {
    var container = control.find("[data-custom-select-container]");

    if (!container.hasClass("active")) {
      container.addClass("active");
      container.attr("aria-expanded", true);

      var selected = getSelectedItem(control);

      if (selected) {
        selected.focus();
      } else {
        getItems(control)[0].focus();
      }

      typeof callback === "function" && callback();
    }
  }

  function closeOptions(control, callback) {
    var container = control.find("[data-custom-select-container]");

    if (container.hasClass("active")) {
      control.find("[data-custom-select-button]").focus();

      container.removeClass("active");
      container.attr("aria-expanded", false);

      typeof callback === "function" && callback();
    }
  }

  function navigate(event, control, closeCallback) {
    var activeElement = $(event.target);

    switch (event.keyCode) {
      case DOWN_ARROW_KEY_CODE:
        event.preventDefault();
        moveToNextItem(activeElement);
        return;

      case UP_ARROW_KEY_CODE:
        event.preventDefault();
        moveToPrevItem(activeElement);
        return;

      case TAB_KEY_CODE:
        event.preventDefault();
        closeOptions(control, closeCallback);

        if (!event.shiftKey) {
          focusNextElement(control);
        }
        return;

      default:
        return;
    }
  }

  function moveToNextItem(activeElement) {
    var listItems = activeElement
      .siblings("[data-custom-select-item]")
      .addBack();
    var itemIndex = listItems.index(activeElement);

    if (itemIndex !== listItems.length - 1) {
      listItems[itemIndex + 1].focus();
    }
  }

  function moveToPrevItem(activeElement) {
    var listItems = activeElement
      .siblings("[data-custom-select-item]")
      .addBack();
    var itemIndex = listItems.index(activeElement);

    if (itemIndex > 0) {
      listItems[itemIndex - 1].focus();
    }
  }

  function focusNextElement(control) {
    var focusableElements = $(":tabbable");
    var button = control.find("[data-custom-select-button]");
    var index = focusableElements.index(button);
    index = index += 1;

    if (index < focusableElements.length) {
      focusableElements[index].focus();
    } else {
      focusableElements[0].focus();
    }
  }

  function selectItem(activeElement, callback) {
    var listItems = activeElement.siblings("[data-custom-select-item]");
    var list = activeElement.closest("[data-custom-select-list]");

    list.attr("aria-activedescendant", activeElement.attr("id"));

    listItems.each(function (i, el) {
      $(el).removeClass("selected");
      $(el).removeAttr("aria-selected");
    });

    activeElement.addClass("selected");
    activeElement.attr("aria-selected", true);

    typeof callback === "function" && callback();
  }

  function getSelectedItem(control) {
    return control.find("[data-custom-select-item].selected");
  }

  function getButton(control) {
    return control.find("[data-custom-select-button]");
  }

  function getContainer(control) {
    return control.find("[data-custom-select-container]");
  }

  function getItems(control) {
    return control.find("[data-custom-select-item]");
  }

  function isOpen(control) {
    var modal = control.find("[data-custom-select-container]");
    return modal.hasClass("active");
  }

  return {
    toggleOptions: toggleOptions,
    openOptions: openOptions,
    closeOptions: closeOptions,
    navigate: navigate,
    selectItem: selectItem,
    getSelectedItem: getSelectedItem,
    getButton: getButton,
    getContainer: getContainer,
    getItems: getItems,
    isOpen: isOpen,
  };
})();

bgl.common.tabs = (function () {
  $(document).on("click", "[data-tab] button", function (event) {
    event.stopImmediatePropagation();
    selectTab($(this));
    this.scrollIntoView({
      behavior: "smooth",
      block: "nearest",
      inline: "start",
    });
    event.preventDefault();
  });

  $(document).on("click", "#left-arrow", function (event) {
    event.stopImmediatePropagation();

    var tabItems = $(".tabs-nav__item");
    for (var i = 0; i < tabItems.length; i++) {
      if (tabItems[i].classList.contains("active")) {
        var tabItem = $("#tab-" + (i - 1));
        selectTab(tabItem);
        tabItem[0].scrollIntoView({
          behavior: "smooth",
          block: "nearest",
          inline: "start",
        });
        break;
      }
    }
  });

  $(document).on("click", "#right-arrow", function (event) {
    event.stopImmediatePropagation();

    var tabItems = $(".tabs-nav__item");
    for (var i = 0; i < tabItems.length; i++) {
      if (tabItems[i].classList.contains("active")) {
        var tabItem = $("#tab-" + (i + 1));
        selectTab(tabItem);
        tabItem[0].scrollIntoView({
          behavior: "smooth",
          block: "nearest",
          inline: "start",
        });
        break;
      }
    }
  });

  function showHideArrows() {
    var navTabs = $(".tabs-nav__item");
    var leftArrow = $("#left-arrow");
    var rightArrow = $("#right-arrow");
    if (navTabs[0].classList.contains("active")) {
      leftArrow.addClass("hide");
      leftArrow.attr("aria-hidden", "true");
      rightArrow.removeClass("hide");
      rightArrow.attr("aria-hidden", "false");
    } else if (navTabs[navTabs.length - 1].classList.contains("active")) {
      rightArrow.addClass("hide");
      rightArrow.attr("aria-hidden", "true");
      leftArrow.removeClass("hide");
      leftArrow.attr("aria-hidden", "false");
    } else {
      rightArrow.removeClass("hide");
      rightArrow.attr("aria-hidden", "false");
      leftArrow.removeClass("hide");
      leftArrow.attr("aria-hidden", "false");
    }
  }

  function selectTab(element) {
    var tabContainer = element.closest("[data-tab-container]");
    var selectedTab = element.parent("[data-tab]");
    var selectedTabContent = tabContainer.find(
      "[data-tab-content=" + selectedTab.data("tab") + "]"
    );
    var tabs = tabContainer.find("[data-tab]");
    var tabButtons = tabContainer.find("[data-tab] button");
    var tabContent = tabContainer.find("[data-tab-content]");

    tabs.removeClass("active");
    tabButtons.attr("aria-selected", "false");
    tabContent.removeClass("active").attr("aria-hidden", "true");

    selectedTab.addClass("active");
    element.attr("aria-selected", "true");
    selectedTabContent.addClass("active").attr("aria-hidden", "false");

    showHideArrows();
  }
})();

bgl.common.keyboardAccessibility = (function () {
  var SPACE_KEYCODE = 32;
  var ENTER_KEYCODE = 13;

  function generateClickOnKeypress(event) {
    var code = event.charCode || event.keyCode;

    if (code === ENTER_KEYCODE || code === SPACE_KEYCODE) {
      var forAttr = $(event.target).attr("for");

      if (forAttr !== null) {
        $("#" + forAttr).click();
      } else {
        $(this).click();
      }
    }
  }

  return {
    generateClickOnKeypress: generateClickOnKeypress,
  };
})();

bgl.common.timeoutCounter = (function () {
  var timer;
  var timeoutDurationMinutes;
  var minutes;
  var sessionTimeoutRedirectUrl;

  function init(timeoutDurationMins, sessionTimeoutRedirect) {
    timeoutDurationMinutes = timeoutDurationMins;
    sessionTimeoutRedirectUrl = sessionTimeoutRedirect;

    $(document).ajaxSuccess(function (data, props, xhr) {
      resetCounter();
    });

    $(document).ajaxError(function (xhr, props) {
      if (props.status === 401) {
        location.href = sessionTimeoutRedirectUrl;
      }
    });

    startCounter();
  }

  function decrementCounter() {
    if (minutes === 0) {
      onCounterEnd();
      return;
    }
    minutes--;
  }

  function startCounter() {
    clearInterval(timer);
    setCookie();
    minutes = timeoutDurationMinutes;
    decrementCounter();
    timer = 0;
    timer = setInterval(decrementCounter, 60 * 1000);
  }

  function resetCounter() {
    startCounter();
  }

  function stopCounter() {
    clearInterval(timer);
  }

  function onCounterEnd() {
    if (!document.cookie.includes("sessionAlive")) {
      stopCounter();
      bgl.common.utilities.redirectToUrl(sessionTimeoutRedirectUrl);
    }
  }

  function setCookie() {
    var ttl = timeoutDurationMinutes * 60;
    document.cookie =
      "sessionAlive=true; max-age=" + ttl + "; path=/; secure; samesite=lax";
  }

  return {
    init: init,
  };
})();
