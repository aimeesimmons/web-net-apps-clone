﻿$(function () {
    var thisScript = document.querySelector('script[data-ai]');
    var s = thisScript.getAttribute('data-ai-s');
    var i = thisScript.getAttribute('data-ai-i');
    var u = thisScript.getAttribute('data-ai-u');
    var o = thisScript.getAttribute('data-ai-o');

    var appInsights = window.appInsights || function (a) {
        function b(a) {
            c[a] = function () {
                var b = arguments;
                c.queue.push(function () { c[a].apply(c, b); });
            };
        }
        var c = { config: a }, d = document, e = window; setTimeout(function () {
            var b = d.createElement("script");
            b.src = a.url || "https://az416426.vo.msecnd.net/scripts/a/ai.0.js", d.getElementsByTagName("script")[0]
                .parentNode.appendChild(b);
        });
        try { c.cookie = d.cookie } catch (a) { } c.queue = [];
        for (var f = ["Event", "Exception", "Metric", "PageView", "Trace", "Dependency"]; f.length;)b("track" + f.pop()); if (b("setAuthenticatedUserContext"), b("clearAuthenticatedUserContext"), b("startTrackEvent"), b("stopTrackEvent"), b("startTrackPage"), b("stopTrackPage"), b("flush"), !a.disableExceptionTracking) {
        f = "onerror", b("_" + f); var g = e[f];
            e[f] = function (a, b, d, e, h) {
                var i = g && g(a, b, d, e, h);
                return !0 !== i && c["_" + f](a, b, d, e, h), i;
            };
        }
        return c;
    }({
        instrumentationKey: i
    });

    window.appInsights = appInsights, appInsights.queue && 0 === appInsights.queue.length;

    var telemetryInitialiser = function () {
        appInsights.context.addTelemetryInitializer(function (envelope) {
            envelope.tags["ai.session.id"] = s;
        });
        appInsights.context.operation.id = o;
        appInsights.setAuthenticatedUserContext(u);
        appInsights.trackPageView();
    };

    if (appInsights.queue !== undefined) {
        appInsights.queue.push(telemetryInitialiser);
    } else {
        telemetryInitialiser();
    }

});