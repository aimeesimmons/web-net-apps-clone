﻿window.siteId = '31631473';
window.NinaVars = window.NinaVars || {};
window.NinaVars.isSelfService = true;
window.NinaVars.isSales = false;
window.NinaVars.preprod = true;
window.livepersonImgPath = "https://lp.sabio.co.uk/bis";

function gtmStart(w, d, s, l, g) {
	w[l] = w[l] || []; w[l].push({
		'gtm.start':
			new Date().getTime(), event: 'gtm.js'
	});
	var f = d.getElementsByTagName(s)[0],
		j = d.createElement(s),
		dl = l !== 'dataLayer' ? '&l=' + l : '';
	j.async = true;
	j.src = 'https://www.googletagmanager.com/gtm.js?id=' + g + dl;
	for (var i = 0; i < d.getElementsByTagName(s).length; i++) {
		if (i > 0) f = d.getElementsByTagName(s)[i];
		if ((f.outerHTML.indexOf("cdn.cookielaw.org") === -1) && (f.outerHTML.indexOf("Optanon") === -1)) {
			break;
		}
	}
	f.parentNode.insertBefore(j, f);
}

var firstScript = document.getElementsByTagName('script')[0];
var oneTrustScript1 = document.createElement('script');
oneTrustScript1.type = 'text/javascript';
oneTrustScript1.src = "https://cdn.cookielaw.org/consent/de25bbce-6e2c-401e-98a1-c3fe93e24150/OtAutoBlock.js";
firstScript.parentNode.insertBefore(oneTrustScript1, firstScript);

var oneTrustScript2 = document.createElement('script');
oneTrustScript2.type = 'text/javascript';
oneTrustScript2.src = "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js";
oneTrustScript2.charset = "UTF-8";
oneTrustScript2.setAttribute("data-domain-script", "de25bbce-6e2c-401e-98a1-c3fe93e24150");
firstScript.parentNode.insertBefore(oneTrustScript2, firstScript);

var oneTrustScript3 = document.createElement('script');
oneTrustScript3.type = 'text/javascript';
oneTrustScript3.text = "function OptanonWrapper() { }";
firstScript.parentNode.insertBefore(oneTrustScript3, firstScript);

var maskCode = 'HX01';

var optrial = optrial || {};
optrial.channel = '';
optrial.aggregator = '';
optrial.affinitycode = maskCode;
optrial.productclass = '';

var dataLayer = window.dataLayer || [];
dataLayer.push({
    "channel": null,
    "maskCode": maskCode,
    "sessionId": null,
    "product": null,
    "aggregatorName": null,
    "pQuoteNumber": null
});

(function () { gtmStart(window, document, 'script', 'dataLayer', 'GTM-5G3LQS8'); })();
(function () { gtmStart(window, document, 'script', 'dataLayer', 'GTM-W7ZPDPP'); })();

var tagProcessingDone = false;
function setTagProcessingDone() {
    tagProcessingDone = true;
}

var digitalData = { "page_data": {} };
$.getScript('https://assets.adobedtm.com/5b4eb01e0d29/1728f971d27d/launch-f81d526d4c69-development.min.js', function()
{
    digitalData.page_data.page_url = window.location.href;
    digitalData.page_data.page_title = document.title;
    digitalData.page_data.page_path = window.location.pathname;
    _satellite.pageBottom();
});