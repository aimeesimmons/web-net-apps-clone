﻿namespace BGL.Web.Halifax.MotorQuote.Constants
{
    public class BrandConstants
    {
        public const string ContactPhoneNumber = "0330 018 6312";

        public const string ExistingCustomerPhoneNumber = "0344 209 0471";

        public const string AcceptanceInformationPhoneNumber = "0330 0189 539";
        
        public const string WrongVehicleTypeForVanInsValidationErrorMessage = "We have matched your registration number to a van. Please enter a car registration number to get a car insurance quote.";
    }
}