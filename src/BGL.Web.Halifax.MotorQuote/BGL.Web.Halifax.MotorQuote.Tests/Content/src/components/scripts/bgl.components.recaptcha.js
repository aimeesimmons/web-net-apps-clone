﻿var bgl = window.bgl || {};
bgl.components = bgl.components || {};

bgl.components.recaptcha = (function () {
    var configuration, tokenInput;

    function initComponent(componentConfiguration) {
        configuration = componentConfiguration;

        addHiddenInputToForm();
        addScriptToHead();
    }

    function addHiddenInputToForm() {
        var form = document.querySelector(configuration.formCssSelector);

        tokenInput = document.createElement('input');
        tokenInput.type = 'hidden';
        tokenInput.name = 'recaptchaToken';

        form.appendChild(tokenInput);
    }

    function addScriptToHead() {
        var scripts = [].slice.call(document.querySelectorAll('head script'));
        var indexOfExistingScript = scripts.map(function(x) {
            return x.src;
        }).indexOf("https://www.google.com/recaptcha/api.js?render=siteKey");

        if (indexOfExistingScript > -1) {
            return;
        }

        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://www.google.com/recaptcha/api.js?render=' + configuration.siteKey;
        head.appendChild(script);
    }

    function populateToken(callback) {
        grecaptcha.execute(configuration.siteKey, { action: configuration.Id })
            .then(function (token) {
                tokenInput.value = token;
                
                if (callback) {
                    callback(token);
                }
            });
    }

    return {
        init: initComponent,
        populateToken: populateToken,
        addHiddenInputToForm: addHiddenInputToForm
    };
})();

if (typeof module !== 'undefined') {
    module.exports = bgl.components.recaptcha;
}