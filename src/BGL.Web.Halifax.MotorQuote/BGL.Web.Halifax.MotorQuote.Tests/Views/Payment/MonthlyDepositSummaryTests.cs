﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MonthlyDepositSummaryTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "MonthlyDepositSummary";
            Document = GetDocument();
            Components.Add("TakePaymentMonthlyDepositSummaryAsync", GetComponent("TakePaymentMonthlyDepositSummaryAsync"));
        }

        [Test]
        public void TakePaymentMonthlyDepositSummaryAsyncComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentMonthlyDepositSummaryAsync"]));
        }

        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "Id", "\"MonthlyDepositSummary\"")]
        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "MonthlyDepositUrl", "Url.Action(\"MonthlyDeposit\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "PaymentDeclinedUrl", "Url.Action(\"Unsuccessful\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "PaymentSuccessfulUrl", "Url.Action(\"PaymentSuccessful\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "ImportantInformationUrl", "Url.Action(\"PostPriceImportantInformation\", \"Legal\"),")]
        [TestCase("TakePaymentMonthlyDepositSummaryAsync", "UseServiceRedirector", "useServiceRedirector")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}