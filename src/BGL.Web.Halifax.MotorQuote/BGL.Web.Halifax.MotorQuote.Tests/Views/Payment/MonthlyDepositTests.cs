﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MonthlyDepositTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "MonthlyDeposit";
            Document = GetDocument();
            Components.Add("TakePaymentMonthlyDepositAsync", GetComponent("TakePaymentMonthlyDepositAsync"));
        }

        [Test]
        public void TakePaymentMonthlyDepositAsyncComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentMonthlyDepositAsync"]));
        }

        [TestCase("TakePaymentMonthlyDepositAsync", "Id", "\"MonthlyDeposit\"")]
        [TestCase("TakePaymentMonthlyDepositAsync", "MonthlyDepositSummaryUrl", "Url.Action(\"MonthlyDepositSummary\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyDepositAsync", "MonthlyInstalmentUrl", "Url.Action(\"MonthlyInstalment\", \"Payment\"),")]
        [TestCase("TakePaymentMonthlyDepositAsync", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("TakePaymentMonthlyDepositAsync", "ImportantInformationUrl", "Url.Action(\"PostPriceImportantInformation\", \"Legal\"),")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}