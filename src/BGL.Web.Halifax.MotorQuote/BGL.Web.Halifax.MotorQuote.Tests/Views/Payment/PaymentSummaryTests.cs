﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PaymentSummaryTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "PaymentSummary";
            Document = GetDocument();
            Components.Add("TakePaymentPaymentSummary", GetComponent("TakePaymentPaymentSummary"));
        }

        [Test]
        public void TakePaymentPaymentSummaryComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentPaymentSummary"]));
        }

        [TestCase("TakePaymentPaymentSummary", "Id", "\"PaymentSummary\"")]
        [TestCase("TakePaymentPaymentSummary", "AnnualUrl", "Url.Action(\"Annual\", \"Payment\"),")]
        [TestCase("TakePaymentPaymentSummary", "MonthlyInstalmentUrl", "Url.Action(\"MonthlyInstalment\", \"Payment\"),")]
        [TestCase("TakePaymentPaymentSummary", "PaymentSummaryBackButtonUrl", "Url.Action(\"Index\", \"Review\"),")]
        [TestCase("TakePaymentPaymentSummary", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}