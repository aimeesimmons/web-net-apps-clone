﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AnnualSummaryTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "AnnualSummary";
            Document = GetDocument();
            Components.Add("TakePaymentAnnualSummaryAsync", GetComponent("TakePaymentAnnualSummaryAsync"));
        }

        [Test]
        public void TakePaymentAnnualSummaryAsyncComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentAnnualSummaryAsync"]));
        }

        [TestCase("TakePaymentAnnualSummaryAsync", "Id", "\"AnnualSummary\"")]
        [TestCase("TakePaymentAnnualSummaryAsync", "PaymentDeclinedUrl", "Url.Action(\"Unsuccessful\", \"Payment\"),")]
        [TestCase("TakePaymentAnnualSummaryAsync", "PaymentSuccessfulUrl", "Url.Action(\"PaymentSuccessful\", \"Payment\"),")]
        [TestCase("TakePaymentAnnualSummaryAsync", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("TakePaymentAnnualSummaryAsync", "ImportantInformationUrl", "Url.Action(\"PostPriceImportantInformation\", \"Legal\"),")]
        [TestCase("TakePaymentAnnualSummaryAsync", "BislBrandName", "\"HALIFAX\"")]
        [TestCase("TakePaymentAnnualSummaryAsync", "UseServiceRedirector", "useServiceRedirector")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}