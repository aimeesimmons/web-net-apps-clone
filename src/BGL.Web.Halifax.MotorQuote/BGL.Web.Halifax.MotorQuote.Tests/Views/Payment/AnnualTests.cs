﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Payment
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AnnualTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Payment";
            ViewName = "Annual";
            Document = GetDocument();
            Components.Add("TakePaymentAnnualAsync", GetComponent("TakePaymentAnnualAsync"));
        }

        [Test]
        public void TakePaymentAnnualAsyncComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TakePaymentAnnualAsync"]));
        }

        [TestCase("TakePaymentAnnualAsync", "Id", "\"Annual\"")]
        [TestCase("TakePaymentAnnualAsync", "TenantConfiguration", "config.GetTenantConfiguration(),")]
        [TestCase("TakePaymentAnnualAsync", "BislBrandName", "\"HALIFAX\"")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}