﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.OtherDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AndFinallyTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "OtherDetails";
            ViewName = "AndFinally";
            Document = GetDocument();
            Components.Add("AndFinally", GetComponent("AndFinally"));
        }

        [Test]
        public void AndFinallyComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["AndFinally"]));
        }

        [TestCase("AndFinally", "Id", "\"AndFinally\"")]
        [TestCase("AndFinally", "NextPageUrl", "Url.Action(\"DirectPrePriceLegal\", \"Legal\"),")]
        [TestCase("AndFinally", "DataStoreKey", "\"OtherDetails\",")]
        [TestCase("AndFinally", "PreviousPageUrl", "Url.Action(\"PaymentsRegularity\", \"OtherDetails\"),")]
        [TestCase("AndFinally", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}