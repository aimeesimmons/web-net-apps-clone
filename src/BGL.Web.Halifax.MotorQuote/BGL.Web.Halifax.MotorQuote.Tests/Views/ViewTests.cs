﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views
{
    public class ViewTests
    {
        private readonly string _solutionName = "BGL.Web.Halifax.MotorQuote";

        public string Document { get; set; }

        public string ControllerName { get; set; }

        public string ViewName { get; set; }

        public Dictionary<string, string> Components { get; set; } = new Dictionary<string, string>();
        
        internal string GetDocument()
        {
            return File.ReadAllText(
                Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory,
                    $"..\\..\\..\\..\\{_solutionName}\\{_solutionName}\\Views\\{ControllerName}\\{ViewName}.cshtml"));
        }

        internal string GetComponent(string componentType, bool consumesConfigurationAsVariable = false)
        {
            return consumesConfigurationAsVariable
                ? GetDocumentPart($"@Html.ComponentRender{componentType}", ")")
                : GetDocumentPart($"@Html.ComponentRender{componentType}", "})");
        }

        internal string GetConfiguration(string componentType)
        {
            return GetDocumentPart($"var configuration = new Component{componentType}", "};");
        }

        internal void TestComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue, string configurationName = "", bool consumesConfigurationAsVariable = false)
        {
            // Arrange
            var soughtString = CompressString($"{propertyName} = {propertyValue}");
            var configurationValue = consumesConfigurationAsVariable ? CompressString(GetConfiguration(configurationName)) : CompressString(Components[componentName]);
            var errorMessage = consumesConfigurationAsVariable
                ? $"|{soughtString}| not found in |{configurationValue}|"
                : $"|{soughtString}| not found in |{Components[componentName]}|";

            // Act
            var result = configurationValue.Contains(soughtString);

            // Assert
            Assert.True(result, errorMessage);
        }

        internal string GetDocumentPart(string startOfString, string endOfString)
        {
            var startPosition = FindStartPositionOfComponent(startOfString);
            if (startPosition == -1)
            {
                return string.Empty;
            }

            var endPosition = FindEndPositionOfComponent(startPosition, endOfString);
            if (endPosition == -1)
            {
                return string.Empty;
            }

            return Document.Substring(startPosition, endPosition - startPosition);
        }

        private int FindStartPositionOfComponent(string startOfString)
        {
            return Document.IndexOf(startOfString);
        }

        private int FindEndPositionOfComponent(int startPositionOfComponent, string endOfString)
        {
            var endPosition = Document.IndexOf(endOfString, startPositionOfComponent);
            if (endPosition == -1)
            {
                return endPosition;
            }

            return endPosition + endOfString.Length;
        }

        private string CompressString(string inputString)
        {
            Regex quotesBlockMatch = new Regex("\\\"(.*?)\\\"", RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

            var count = 0;
            var list = new List<string>();
            foreach (Match match in quotesBlockMatch.Matches(inputString))
            {
                list.Add(match.Value);
                inputString = inputString.Replace(match.Value, $"_{count++}_");
            }

            inputString = Regex.Replace(inputString, "\\s+|\\t|\\r|\\n", string.Empty);

            count = 0;
            foreach (var item in list)
            {
                inputString = inputString.Replace($"_{count++}_", item);
            }

            return inputString;
        }
    }
}
