﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class RetrieveQuoteFormTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_RetrieveQuoteForm";
            Document = GetDocument();
            Components.Add("Recaptcha", GetComponent("Recaptcha"));
        }

        [Test]
        public void RecaptchaComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Recaptcha"]));
        }

        [TestCase("Recaptcha", "Id", "\"RecaptchaDefault\"")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}