﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Shared
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PageAsSliderLayoutTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Shared";
            ViewName = "_PageAsSliderLayout";
            Document = GetDocument();
            Components.Add("HoustonConfiguration", GetDocumentPart("var componentHoustonConfiguration = new ComponentHoustonConfiguration", "};"));
            Components.Add("GoogleTagManager", GetComponent("GoogleTagManager"));
            Components.Add("PageReloader", GetComponent("PageReloader"));
            Components.Add("Houston", GetDocumentPart("@Html.ComponentRenderHouston", "(componentHoustonConfiguration)"));
            Components.Add("GoogleTagManagerDataLayer", GetComponent("GoogleTagManagerDataLayer"));
            Components.Add("GoogleTagManagerTagManager", GetComponent("GoogleTagManagerTagManager"));
            Components.Add("GoogleTagManagerLaunchScript", GetComponent("GoogleTagManagerLaunchScript"));
        }

        [Test]
        public void HoustonConfigurationComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["HoustonConfiguration"]));
        }

        [Test]
        public void GoogleTagManagerComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManager"]));
        }

        [Test]
        public void PageReloaderComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["PageReloader"]));
        }

        [Test]
        public void HoustonComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["Houston"]));
        }

        [Test]
        public void GoogleTagManagerDataLayerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerDataLayer"]));
        }

        [Test]
        public void GoogleTagManagerTagManagerRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerTagManager"]));
        }

        [Test]
        public void GoogleTagManagerLaunchScriptRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["GoogleTagManagerLaunchScript"]));
        }

        [TestCase("HoustonConfiguration", "Id", "\"Houston\",")]
        [TestCase("HoustonConfiguration", "Brand", "vaConfig.BrandName,")]
        [TestCase("HoustonConfiguration", "Application", "vaConfig.Application,")]
        [TestCase("HoustonConfiguration", "JourneySection", "\"Purchase\",")]
        [TestCase("HoustonConfiguration", "Product", "vaConfig.Product,")]
        [TestCase("HoustonConfiguration", "Journey", "\"Car\",")]
        [TestCase("HoustonConfiguration", "ContactDataUrl", "Url.Action(\"HoustonContactData\", \"ContactData\"),")]
        [TestCase("HoustonConfiguration", "InvocationPoint", "vaConfig.DefaultInvocationPoint,")]
        [TestCase("HoustonConfiguration", "PageNamePrefix", "\"Sales_TE_\",")]
        [TestCase("HoustonConfiguration", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("HoustonConfiguration", "IsPreQuote", "preQuote,")]
        [TestCase("HoustonConfiguration", "OnError", "Request.Url.AbsolutePath.ToLower().Contains(\"error\"),")]
        [TestCase("GoogleTagManager", "Id", "\"GoogleTagManager\",")]
        [TestCase("GoogleTagManager", "AddOnsList", "new List<string> \r\n        {\r\n            \"No claims discount protection\",\r\n            \"Guaranteed Replacement Car\",\r\n            \"RAC Breakdown Assistance\",\r\n            \"Keycare\",\r\n            \"Legal Protection\",\r\n            \"Personal Accident Cover\"\r\n        },")]
        [TestCase("GoogleTagManager", "MarketingSourceSetupList", "new List<string> { \"comparethemarket\", \"gocompare\", \"moneysupermarket\", \"confusedcom\", \"quotezonecouk\" },")]
        [TestCase("GoogleTagManager", "GtmConfiguration", "config.GetGtmConfiguration(),")]
        [TestCase("GoogleTagManager", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("GoogleTagManagerDataLayer", "AddOnsList", "new List<string>\r\n        {\r\n            \"Legal Protection\",\r\n            \"No claims discount protection\",\r\n            \"Price Protection Cover\",\r\n            \"Guaranteed Replacement Car\",\r\n            \"RAC Breakdown Assistance\",\r\n            \"Keycare\",\r\n            \"Personal Accident Cover\"\r\n        }")]
        [TestCase("GoogleTagManagerDataLayer", "QuoteTier", "\"Standard\"")]
        [TestCase("GoogleTagManagerTagManager", "TagManagerScriptUrl", "ConfigurationManager.AppSettings[\"TagManagerScriptUrl\"]")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}