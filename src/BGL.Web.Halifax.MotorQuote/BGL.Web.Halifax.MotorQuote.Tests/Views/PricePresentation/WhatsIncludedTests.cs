﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.PricePresentation
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class WhatsIncludedTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "PricePresentation";
            ViewName = "WhatsIncluded";
            Document = GetDocument();
            Components.Add("CoverBubbles", GetComponent("CoverBubbles"));
            Components.Add("CoverLevelContent", GetComponent("CoverLevelContent"));
        }

        [Test]
        public void CoverBubblesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverBubbles"]));
        }

        [Test]
        public void CoverLevelContentComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverLevelContent"]));
        }

        [TestCase("CoverBubbles", "Id", "\"WhatsIncluded\",")]
        [TestCase("CoverBubbles", "CoverFeaturePriority", "new Dictionary<string, CoverFeatureItem>\r\n                {\r\n                    { FeatureServiceConstants.CoverLevel, new CoverFeatureItem { ViewName = \"CoverLevel\" } },\r\n                    { FeatureServiceConstants.CourtesyVehicle, new CoverFeatureItem { ContentUrl = Url.Action(\"CourtesyCar\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.PersonalAccidentBenefits, new CoverFeatureItem { ContentUrl = Url.Action(\"PersonalAccidentBenefits\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.WindscreenCover, new CoverFeatureItem { ContentUrl = Url.Action(\"WindscreenCover\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.DrivingAbroad, new CoverFeatureItem { ContentUrl = Url.Action(\"DrivingAbroad\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.PersonalBelongings, new CoverFeatureItem { ContentUrl = Url.Action(\"PersonalBelongings\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.AudioEquipment, new CoverFeatureItem { ContentUrl = Url.Action(\"AudioEquipment\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.MedicalExpenses, new CoverFeatureItem { ContentUrl = Url.Action(\"MedicalExpenses\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.ReplacementLocks, new CoverFeatureItem { ContentUrl = Url.Action(\"ReplacementLocks\", \"CoverFeatures\") } },\r\n                    { FeatureServiceConstants.DriveOtherVehicles, new CoverFeatureItem { ContentUrl = Url.Action(\"DriveOtherCars\", \"CoverFeatures\") } }\r\n                },")]
        [TestCase("CoverBubbles", "TenantConfiguration", "config.GetTenantConfiguration()")]
        [TestCase("CoverLevelContent", "Id", "\"CoverLevelContent\",")]
        [TestCase("CoverLevelContent", "TenantConfiguration", "config.GetTenantConfiguration()")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}