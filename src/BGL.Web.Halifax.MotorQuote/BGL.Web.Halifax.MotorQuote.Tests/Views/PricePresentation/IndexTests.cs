﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.PricePresentation
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "PricePresentation";
            ViewName = "Index";
            Document = GetDocument();
            Document = Regex.Replace(Document, "�", string.Empty);
            Components.Add("WelcomeQuoteConfiguration", GetDocumentPart("new ComponentWelcomeQuoteConfiguration", "};"));
            Components.Add("CoverExcess", GetComponent("CoverExcess"));
            Components.Add("СoverFeaturePriority", GetDocumentPart("var coverFeaturePriority = new Dictionary<string, CoverFeatureItem>", "};"));
            Components.Add("CoverListWidgetButtons", GetComponent("CoverListWidgetButtons"));
            Components.Add("FeatureBanner", GetComponent("FeatureBanner"));
        }

        [Test]
        public void WelcomeQuoteConfigurationRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["WelcomeQuoteConfiguration"]));
        }
        
        [Test]
        public void CoverExcessComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverExcess"]));
        }

        [Test]
        public void FeatureBannertRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["FeatureBanner"]));
        }

        [Test]
        public void CoverListWidgetButtonsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverListWidgetButtons"]));
        }

        [TestCase("WelcomeQuoteConfiguration", "Id", "\"SalesWelcomeQuote\",")]
        [TestCase("WelcomeQuoteConfiguration", "ManufactureListInUpperCase", "new List<string> { \"BMW\", \"MG\", \"SEAT\" },")]
        [TestCase("WelcomeQuoteConfiguration", "AvivaCreditPhoneNumber", "BrandConstants.ContactPhoneNumber")]
        [TestCase("WelcomeQuoteConfiguration", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("CoverExcess", "ExcessConfiguration", "new ExcessConfiguration\r\n{\r\nVoluntaryExcessAvailableAmounts = new List<int> { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 }\r\n},")]
        [TestCase("CoverExcess", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("CoverListWidgetButtons", "CoverFeaturePriority", "newDictionary<string, CoverFeatureItem>{" +
        "{ FeatureServiceConstants.NcdProtection, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.LegalProtection, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.GuaranteedReplacementVehicle, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.Breakdown, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.PriceProtection, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.ToolsCover, new CoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.KeyCover, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}," +
        "{ FeatureServiceConstants.PersonalAccidentCover, newCoverFeatureItem { ViewName = FeatureServiceConstants.FeatureUnknownViewName }}")]
        [TestCase("FeatureBanner", "CoverFeaturePriority", "new Dictionary<string, CoverFeatureItem>\r\n                {\r\n                    { FeatureServiceConstants.WindscreenCover, new CoverFeatureItem { ContentUrl = Url.Action(\"WindscreenCover\", \"CoverFeatures\"), Description = \"Covering the cost of repairing or replacing your glass\" }},\r\n                    { FeatureServiceConstants.PersonalBelongings, new CoverFeatureItem { ContentUrl = Url.Action(\"PersonalBelongings\", \"CoverFeatures\"), Description = \"We will cover your personal belongings too\" }},\r\n                    { FeatureServiceConstants.DriveOtherVehicles, new CoverFeatureItem { ContentUrl = Url.Action(\"DriveOtherCars\", \"CoverFeatures\"), Description = \"We may be able to cover you to drive other cars\"}},\r\n                    { FeatureServiceConstants.MedicalExpenses, new CoverFeatureItem { ContentUrl = Url.Action(\"MedicalExpenses\", \"CoverFeatures\"), Description = \"Cover up to \\u00A3200 if you need medical help\" }},\r\n                    { FeatureServiceConstants.AudioEquipment, new CoverFeatureItem { ContentUrl = Url.Action(\"AudioEquipment\", \"CoverFeatures\"), Description = \"Cover for your audio, navigation and entertainment equipment\" }},\r\n                    { FeatureServiceConstants.DrivingAbroad, new CoverFeatureItem { ContentUrl = Url.Action(\"DrivingAbroad\", \"CoverFeatures\"), Description = \"Great news - you can take your car abroad\" }},\r\n                    { FeatureServiceConstants.CourtesyVehicle, new CoverFeatureItem { ContentUrl = Url.Action(\"CourtesyCar\", \"CoverFeatures\"), Description = \"Provides you with a car, subject to availability\"}},\r\n                    { FeatureServiceConstants.PersonalAccidentBenefits, new CoverFeatureItem { ContentUrl = Url.Action(\"PersonalAccidentBenefits\", \"CoverFeatures\"), Description = \"A range of injuries covered for you/partner\"}},\r\n                    { FeatureServiceConstants.ReplacementLocks, new CoverFeatureItem { ContentUrl = Url.Action(\"ReplacementLocks\", \"CoverFeatures\"), Description = \"Cover for your car door and/or boot locks\"}}                \r\n                }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}