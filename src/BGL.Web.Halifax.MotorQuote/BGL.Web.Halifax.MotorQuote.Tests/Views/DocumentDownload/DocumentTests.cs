﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.DocumentDownload
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class DocumentTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "DocumentDownload";
            ViewName = "Document";
            Document = GetDocument();
            Components.Add("DocumentLoader", GetComponent("DocumentLoader"));
        }

        [Test]
        public void DocumentLoaderComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DocumentLoader"]));
        }

        [TestCase("DocumentLoader", "Id", "\"DocumentLoader\"")]
        [TestCase("DocumentLoader", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}