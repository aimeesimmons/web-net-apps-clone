﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class CoverLevelTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "CoverLevel";
            Document = GetDocument();
            Components.Add("CoverLevel", GetComponent("CoverLevel"));
        }

        [Test]
        public void CoverLevelComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverLevel"]));
        }

        [TestCase("CoverLevel", "Id", "\"CoverLevel\",")]
        [TestCase("CoverLevel", "DataStoreKey", "\"CoverDetailsCoverLevel\",")]
        [TestCase("CoverLevel", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverLevel", "NextStepUrl", "Url.Action(\"CoverExcesses\", \"YourCover\"),")]
        [TestCase("CoverLevel", "CoverLevelUrl", "Url.Action(\"CoverStartDate\", \"YourCover\"),")]
        [TestCase("CoverLevel", "BackButtonUrl", "Url.Action(\"MainDriver\", \"YourCover\"),")]
        [TestCase("CoverLevel", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}