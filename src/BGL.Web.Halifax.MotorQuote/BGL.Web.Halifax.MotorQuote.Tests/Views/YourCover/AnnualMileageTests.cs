﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class AnnualMileageTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "AnnualMileage";
            Document = GetDocument();
            Components.Add("CoverDetailsAnnualMileage", GetComponent("CoverDetailsAnnualMileage"));
        }

        [Test]
        public void CoverDetailsAnnualMileageComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["CoverDetailsAnnualMileage"]));
        }

        [TestCase("CoverDetailsAnnualMileage", "Id", "\"CoverDetails\",")]
        [TestCase("CoverDetailsAnnualMileage", "ErrorConfiguration", "new ComponentErrorConfiguration(),")]
        [TestCase("CoverDetailsAnnualMileage", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("CoverDetailsAnnualMileage", "DataStoreKey", "\"CoverDetailsAnnualMileage\",")]
        [TestCase("CoverDetailsAnnualMileage", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("CoverDetailsAnnualMileage", "PreviousStepUrl", "Url.Action(\"CoverStartdate\", \"YourCover\"),")]
        [TestCase("CoverDetailsAnnualMileage", "NextStepUrl", "Url.Action(\"VehicleUse\", \"YourCover\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}