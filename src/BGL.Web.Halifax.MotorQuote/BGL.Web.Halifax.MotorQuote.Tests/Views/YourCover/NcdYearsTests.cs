﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.YourCover
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class NcdYearsTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCover";
            ViewName = "NcdYears";
            Document = GetDocument();
            Components.Add("NcdYears", GetComponent("NcdYears"));
        }

        [Test]
        public void NcdYearsComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["NcdYears"]));
        }

        [TestCase("NcdYears", "Id", "\"NcdYears\",")]
        [TestCase("NcdYears", "DataStoreKey", "\"NcdYears\",")]
        [TestCase("NcdYears", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("NcdYears", "NextStepUrl", "Url.Action(\"OtherVehicles\", \"YourCover\")")]
        [TestCase("NcdYears", "NcdSourceStepUrl", "Url.Action(\"NcdSource\", \"YourCover\"),")]
        [TestCase("NcdYears", "PreviousStepUrl", "Url.Action(\"VehicleUse\", \"YourCover\"),")]
        [TestCase("NcdYears", "DataStoreKeyForNcdSourcePage", "\"NcdSource\",")]
        [TestCase("NcdYears", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}