﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.YourCar
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class VehicleDetailsTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourCar";
            ViewName = "VehicleDetails";
            Document = GetDocument();
            Components.Add("VehicleLookup", GetComponent("VehicleLookup"));
        }

        [Test]
        public void VehicleLookupComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["VehicleLookup"]));
        }

        [TestCase("VehicleLookup", "Id", "\"VehicleDetails\",")]
        [TestCase("VehicleLookup", "ManufactureListInUpperCase", "new List<string>\r\n            {\r\n                \"BMW\", \"SEAT\", \"MAN\"\r\n            },")]
        [TestCase("VehicleLookup", "ManufacturerKeys", "new List<string>\r\n            {\r\n                \"FO\",\r\n                \"VA\",\r\n                \"VW\",\r\n                \"PE\",\r\n                \"NI\",\r\n                \"BM\",\r\n                \"R1\",\r\n                \"CI\",\r\n                \"TO\",\r\n                \"AU\",\r\n                \"ME\",\r\n                \"HO\",\r\n                \"FI\",\r\n            },")]
        [TestCase("VehicleLookup", "WrongVehicleTypeForVanInsValidationErrorMessage", "BrandConstants.WrongVehicleTypeForVanInsValidationErrorMessage,")]
        [TestCase("VehicleLookup", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("VehicleLookup", "SummaryButtonText", "\"Continue\",")]
        [TestCase("VehicleLookup", "ModificationButtonText", "\"Continue\",")]
        [TestCase("VehicleLookup", "DataStoreKey", "\"YourCar\",")]
        [TestCase("VehicleLookup", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("VehicleLookup", "NextStepUrl", "Url.Action(\"OvernightParking\", \"YourCar\"),")]
        [TestCase("VehicleLookup", "PreviousStepUrl", "Url.Action(\"VehicleLookup\", \"YourCar\"),")]
        [TestCase("VehicleLookup", "ComponentUrl", "Url.Action(\"VehicleDetails\", \"YourCar\"),")]
        [TestCase("VehicleLookup", "SearchSummaryUrl", "Url.Action(\"SearchSummary\", \"VehicleDetailsComponentSearching\"),")]
        [TestCase("VehicleLookup", "ModificationsUrl", "Url.Action(\"Modifications\", \"VehicleDetailsComponentModifications\"),")]
        [TestCase("VehicleLookup", "PurchasedDateUrl", "Url.Action(\"PurchaseDate\", \"VehicleDetailsComponentPurchaseDate\"),")]
        [TestCase("VehicleLookup", "EditValueUrl", "Url.Action(\"EditVehicleValue\", \"VehicleDetailsComponentValue\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}