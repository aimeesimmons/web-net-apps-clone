﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class MaritalStatusTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "MaritalStatus";
            Document = GetDocument();
            Components.Add("DriverDetailsMaritalStatus", GetComponent("DriverDetailsMaritalStatus"));
        }

        [Test]
        public void DriverDetailsMaritalStatusComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsMaritalStatus"]));
        }

        [TestCase("DriverDetailsMaritalStatus", "Id", "\"MaritalStatusDefault\",")]
        [TestCase("DriverDetailsMaritalStatus", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsMaritalStatus", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsMaritalStatus", "JourneyType", "JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsMaritalStatus", "NextStepUrl", "Url.Action(\"Address\", \"YourDetails\"),")]
        [TestCase("DriverDetailsMaritalStatus", "PreviousStepUrl", "Url.Action(\"Index\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}