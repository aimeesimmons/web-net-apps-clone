﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.YourDetails
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LicenceDateTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "YourDetails";
            ViewName = "LicenceDate";
            Document = GetDocument();
            Components.Add("DriverDetailsLicenceHeldSince", GetComponent("DriverDetailsLicenceHeldSince"));
        }

        [Test]
        public void DriverDetailsLicenceHeldSinceComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DriverDetailsLicenceHeldSince"]));
        }

        [TestCase("DriverDetailsLicenceHeldSince", "Id", "\"DriverLicence\",")]
        [TestCase("DriverDetailsLicenceHeldSince", "DataStoreKey", "\"YourDetails\",")]
        [TestCase("DriverDetailsLicenceHeldSince", "TenantConfiguration", "tenantConfiguration,")]
        [TestCase("DriverDetailsLicenceHeldSince", "JourneyType", " JourneyTypes.MultiStepEdit,")]
        [TestCase("DriverDetailsLicenceHeldSince", "DrivingLicenceNumberPageUrl", "Url.Action(\"LicenceNumber\", \"YourDetails\"),")]
        [TestCase("DriverDetailsLicenceHeldSince", "NextStepUrl", "Url.Action(\"MedicalCondition\", \"YourDetails\"),")]
        [TestCase("DriverDetailsLicenceHeldSince", "PreviousStepUrl", "Url.Action(\"LicenceType\", \"YourDetails\")")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}