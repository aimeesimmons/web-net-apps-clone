﻿using System.Diagnostics.CodeAnalysis;
using BGL.Web.Halifax.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Extras
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class IndexTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Extras";
            ViewName = "Index";
            Document = GetDocument();
            Components.Add("DemandsAndNeedsSinglePageAddOnView", GetComponent("DemandsAndNeedsSinglePageAddOnView", true));
        }

        [Test]
        public void ComponentRenderDemandsAndNeedsSinglePageAddOnViewRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["DemandsAndNeedsSinglePageAddOnView"]));
        }

        [TestCase("DemandsAndNeedsSinglePageAddOnView", "Id", "\"DemandsAndNeeds\"")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "NextPageUrl", "Url.Action(\"Index\", \"Review\"),")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "PreviousPageUrl", "Url.Action(\"Index\", \"PricePresentation\")")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "ErrorRedirectUrl", "ConfigurationManager.AppSettings[\"ErrorRedirectUrl\"]")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "QuoteHasMarketingSource", "quoteHasMarketingSource")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "AddonOrder", "new List<string>\r\n    {\r\n        DemandsAndNeedsServiceConstants.NoClaimsDiscountProtection,\r\n        DemandsAndNeedsServiceConstants.MotorLegalProtection,\r\n        DemandsAndNeedsServiceConstants.RacBreakdownAssistance,\r\n        DemandsAndNeedsServiceConstants.GuaranteedReplacementCar,\r\n        DemandsAndNeedsServiceConstants.Keycare    }")]
        [TestCase("DemandsAndNeedsSinglePageAddOnView", "AddonConfigurations", "new List<AddOnConfiguration>()\r\n    {\r\n        new AddOnConfiguration\r\n        {\r\n            Id = \"NoClaimsDiscountProtection\",\r\n            AddOnName = \"No Claims Discount Protection\",\r\n            ViewName = \"_NoClaimsDiscountProtectionAddOn\",\r\n            AddOnService = DemandsAndNeedsServiceConstants.NoClaimsDiscountProtection,\r\n            ShowBasket = true,\r\n            BasketButtonType = BasketButtonType.AddRemove,\r\n            DidYouKnowText = \"No claims discount protection allows you to make one or more claims before your number of no claims discount years falls\",\r\n            PncdAverageDiscountTableRowsToInclude = new List<int> {\r\n                4, 5, 6, 7, 8, 9\r\n            },\r\n        },\r\n        new AddOnConfiguration\r\n        {\r\n            Id = \"LegalProtection\",\r\n            AddOnName = \"Motor Legal Protection\",\r\n            ViewName = \"_MotorLegalProtectionAddOnWithThreeIcons\",\r\n            AddOnService = DemandsAndNeedsServiceConstants.MotorLegalProtection,\r\n            DidYouKnowText = \"You'll be covered for up to \\u00A3100,000 of legal costs when you purchase Motor Legal Protection with us.\"\r\n        },\r\n        new AddOnConfiguration\r\n        {\r\n            Id = \"Breakdown\",\r\n            AddOnName = \"RAC Breakdown Cover\",\r\n            ViewName = \"_RacBreakdownCoverAddOnWithThreeIcons\",\r\n            AddOnService = DemandsAndNeedsServiceConstants.RacBreakdownAssistance,\r\n            DidYouKnowText = \"Breakdown cover is provided by\",\r\n            DidYouKnowImage = new ImageConfiguration\r\n            {\r\n                AltText = \"RAC logo\",\r\n                HtmlClass = \"addon-cta__info__rac\",\r\n                SourcePath = \"logo-rac.svg\"\r\n            }\r\n        },\r\n        new AddOnConfiguration\r\n        {\r\n            Id = \"GuaranteedReplacementCar\",\r\n            AddOnName = \"Guaranteed Replacement Car\",\r\n            ViewName = \"_GuaranteedReplacementCarAddOnWithThreeIconsSameContentForEveryTier\",\r\n            AddOnService = DemandsAndNeedsServiceConstants.GuaranteedReplacementCar,\r\n            DidYouKnowText = \"Enterprise Rent-A-Car\\u00AE has been carefully chosen, by us, to provide you with a Guaranteed Replacement Car should the worst happen.\",\r\n            DidYouKnowImage = new ImageConfiguration\r\n            {\r\n                AltText = \"Enterprise Rent-A-Car logo\",\r\n                HtmlClass = \"addon-cta__info__logo\",\r\n                SourcePath = \"icon-singlepage-addons-enterprise.svg\"\r\n            }\r\n        },\r\n        new AddOnConfiguration\r\n        {\r\n            Id = \"Keycare\",\r\n            AddOnName = \"Keycare\",\r\n            ViewName = \"_KeycareAddOnWithThreeIcons\",\r\n            AddOnService = DemandsAndNeedsServiceConstants.Keycare,\r\n            DidYouKnowText = \"We not only cover your car keys but a whole range of keys.\"\r\n        }    }")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue, "DemandsAndNeedsConfiguration", true);
        }
    }
}