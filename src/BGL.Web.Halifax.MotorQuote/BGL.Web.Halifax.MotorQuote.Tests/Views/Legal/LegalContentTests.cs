﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Legal
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class LegalContentTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Legal";
            ViewName = "_LegalContent";
            Document = GetDocument();
            Components.Add("TermsAndConditionsIsAvivaUnderwriter", 
                GetDocumentPart("var hasAvivaUnderwriter = Html.ComponentRenderTermsAndConditionsIsAvivaUnderwriter(new ComponentTermsAndCondsConfiguration",
                    "}).ToString();"));
            Components.Add("TermsAndConditionsAdditionalExcessesStatementsToShow", 
                GetDocumentPart("Html.ComponentRenderTermsAndConditionsAdditionalExcessesStatementsToShow", 
                    "(new ComponentTermsAndCondsConfiguration()).ToString();"));
            Components.Add("MarketingSourceConfig", GetDocumentPart("var marketingSourceConfig = new ComponentTermsAndCondsConfiguration", "};"));
            Components.Add("TermsAndConditionsListOfUnderwritersOnPanel", GetComponent("TermsAndConditionsListOfUnderwritersOnPanel"));
            Components.Add("TermsAndConditionsUnderwriter", GetComponent("TermsAndConditionsUnderwriter"));
            Components.Add("TermsAndConditionsAggregatorLogo", GetComponent("TermsAndConditionsAggregatorLogo"));
            Components.Add("TermsAndConditionsQuoteValidUntilDate", GetComponent("TermsAndConditionsQuoteValidUntilDate"));
            Components.Add("TermsAndConditionsFees", GetComponent("TermsAndConditionsFees"));
        }

        [Test]
        public void TermsAndConditionsIsAvivaUnderwriterComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsIsAvivaUnderwriter"]));
        }

        [Test]
        public void TermsAndConditionsAdditionalExcessesStatementsToShowComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsAdditionalExcessesStatementsToShow"]));
        }

        [Test]
        public void MarketingSourceConfigComponentRendered()
        {
            // assert
            Assert.True(Document.Contains("Html.ComponentRenderTermsAndConditionsHasMarketingSource(marketingSourceConfig).ToString()"));
        }

        [Test]
        public void TermsAndConditionsListOfUnderwritersOnPanelComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsListOfUnderwritersOnPanel"]));
        }

        [Test]
        public void TermsAndConditionsUnderwriterComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsUnderwriter"]));
        }

        [Test]
        public void TermsAndConditionsAggregatorLogoComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsAggregatorLogo"]));
        }
        
        [Test]
        public void TermsAndConditionsQuoteValidUntilDateComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsQuoteValidUntilDate"]));
        }
        
        [Test]
        public void TermsAndConditionsFeesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsFees"]));
        }

        [TestCase("TermsAndConditionsIsAvivaUnderwriter", "Id", "\"SalesTermsAndConditionsWhoWeActFor\"")]
        [TestCase("TermsAndConditionsIsAvivaUnderwriter", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("MarketingSourceConfig", "Id", "\"SalesTermsAndConditionsAccurateData\"")]
        [TestCase("MarketingSourceConfig", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("MarketingSourceConfig", "MarketingSourceSetupList", "new Dictionary<string, MarketingSourceDetails>\r\n        {\r\n            [\"comparethemarket\"] = new MarketingSourceDetails {ImageSource = \"logo-ctm.svg\", ImageAlt = \"Compare the Market logo\", ImageTitle = \"Compare the Market logo\"},\r\n            [\"gocompare\"] = new MarketingSourceDetails {ImageSource = \"logo-gocompare.svg\", ImageAlt = \"Go Compare logo\", ImageTitle = \"Go Compare logo\"},\r\n            [\"moneysupermarket\"] = new MarketingSourceDetails {ImageSource = \"logo-msm.svg\", ImageAlt = \"Money Super Market logo\", ImageTitle = \"Money Super Market logo\"},\r\n            [\"confusedcom\"] = new MarketingSourceDetails {ImageSource = \"logo-confused.png\", ImageAlt = \"Confused.com logo\", ImageTitle = \"Confused.com logo\"},\r\n            [\"quotezonecouk\"] = new MarketingSourceDetails {ImageSource = \"logo-quotezone.svg\", ImageAlt = \"Quotezone logo\", ImageTitle = \"Quotezone logo\"}\r\n        }")]
        [TestCase("TermsAndConditionsListOfUnderwritersOnPanel", "Id", "\"SalesTermsAndConditionsUnderwriters\"")]
        [TestCase("TermsAndConditionsListOfUnderwritersOnPanel", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsUnderwriter", "Id", "\"SalesTermsAndConditionsUnderwriters\"")]
        [TestCase("TermsAndConditionsUnderwriter", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsQuoteValidUntilDate", "Id", "\"SalesTermsAndConditionsPricingInformation\"")]
        [TestCase("TermsAndConditionsQuoteValidUntilDate", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsFees", "Id", "\"SalesTermsAndConditionsFees\"")]
        [TestCase("TermsAndConditionsFees", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}