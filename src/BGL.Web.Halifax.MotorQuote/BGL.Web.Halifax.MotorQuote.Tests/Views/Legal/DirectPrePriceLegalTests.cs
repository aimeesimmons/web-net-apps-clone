﻿using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Views.Legal
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class DirectPrePriceLegalTests : ViewTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ControllerName = "Legal";
            ViewName = "DirectPrePriceLegal";
            Document = GetDocument();
            Components.Add("TermsAndConditionsIsAvivaUnderwriter", 
                GetDocumentPart("var hasAvivaUnderwriter = Html.ComponentRenderTermsAndConditionsIsAvivaUnderwriter(new ComponentTermsAndCondsConfiguratio",
                    "}).ToString();"));
            Components.Add("TermsAndConditionsAdditionalExcessesStatementsToShow", 
                GetDocumentPart("Html.ComponentRenderTermsAndConditionsAdditionalExcessesStatementsToShow", 
                    "(new ComponentTermsAndCondsConfiguration()).ToString();"));
            Components.Add("TermsAndConditionsFirstName", 
                GetDocumentPart("@Html.ComponentRenderTermsAndConditionsFirstName", 
                    "(new ComponentTermsAndCondsConfiguration())"));
            Components.Add("TermsAndConditionsListOfUnderwritersOnPanel", GetComponent("TermsAndConditionsListOfUnderwritersOnPanel"));
            Components.Add("TermsAndConditionsUnderwriter", GetComponent("TermsAndConditionsUnderwriter"));
            Components.Add("TermsAndConditionsQuoteValidUntilDate", GetComponent("TermsAndConditionsQuoteValidUntilDate"));
            Components.Add("TermsAndConditionsFees", GetComponent("TermsAndConditionsFees"));
        }

        [Test]
        public void TermsAndConditionsIsAvivaUnderwriterComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsIsAvivaUnderwriter"]));
        }

        [Test]
        public void TermsAndConditionsAdditionalExcessesStatementsToShowComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsAdditionalExcessesStatementsToShow"]));
        }

        [Test]
        public void TermsAndConditionsFirstNameComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsFirstName"]));
        }

        [Test]
        public void TermsAndConditionsListOfUnderwritersOnPanelComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsListOfUnderwritersOnPanel"]));
        }

        [Test]
        public void TermsAndConditionsUnderwriterComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsUnderwriter"]));
        }

        [Test]
        public void TermsAndConditionsQuoteValidUntilDateComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsQuoteValidUntilDate"]));
        }

        [Test]
        public void TermsAndConditionsFeesComponentRendered()
        {
            // assert
            Assert.False(string.IsNullOrEmpty(Components["TermsAndConditionsFees"]));
        }

        [TestCase("TermsAndConditionsIsAvivaUnderwriter", "Id", "\"SalesTermsAndConditionsWhoWeActFor\"")]
        [TestCase("TermsAndConditionsIsAvivaUnderwriter", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsListOfUnderwritersOnPanel", "Id", "\"SalesTermsAndConditionsUnderwriters\"")]
        [TestCase("TermsAndConditionsListOfUnderwritersOnPanel", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsUnderwriter", "Id", "\"SalesTermsAndConditionsUnderwriters\"")]
        [TestCase("TermsAndConditionsUnderwriter", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsQuoteValidUntilDate", "Id", "\"SalesTermsAndConditionsPricingInformation\"")]
        [TestCase("TermsAndConditionsQuoteValidUntilDate", "TenantConfiguration", "tenantConfiguration")]
        [TestCase("TermsAndConditionsFees", "Id", "\"SalesTermsAndConditionsFees\"")]
        [TestCase("TermsAndConditionsFees", "TenantConfiguration", "tenantConfiguration")]
        public void ComponentHasPropertySetToValue(string componentName, string propertyName, string propertyValue)
        {
            TestComponentHasPropertySetToValue(componentName, propertyName, propertyValue);
        }
    }
}