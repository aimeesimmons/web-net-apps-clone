using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.Halifax.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ExtrasControllerTests
    {
        private ExtrasController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new ExtrasController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }
    }
}