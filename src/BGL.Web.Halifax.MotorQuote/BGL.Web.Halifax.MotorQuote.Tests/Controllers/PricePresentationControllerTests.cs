using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.Halifax.MotorQuote.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class PricePresentationControllerTests
    {
        // Arrange
        private PricePresentationController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new PricePresentationController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }

        [Test]
        public void Breakdown()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Breakdown());
        }
    }
}