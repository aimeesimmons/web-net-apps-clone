using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.Halifax.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class YourCarControllerTests
    {
        private YourCarController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new YourCarController();
        }

        [Test]
        public void VehicleLookup()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.VehicleLookup());
        }

        [Test]
        public void VehicleDetails()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.VehicleDetails());
        }

        [Test]
        public void OvernightParking()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.OvernightParking());
        }
    }
}