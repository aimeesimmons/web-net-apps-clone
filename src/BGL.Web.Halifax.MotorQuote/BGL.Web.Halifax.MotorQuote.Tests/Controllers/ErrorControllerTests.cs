using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.Halifax.MotorQuote.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ErrorControllerTests
    {
        private ErrorController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new ErrorController();
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }

        [Test]
        public void UnableToQuoteOnline()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.UnableToQuoteOnline());
        }
    }
}