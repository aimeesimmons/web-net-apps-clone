using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.Halifax.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class YourCoverControllerTests
    {
        private YourCoverController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new YourCoverController();
        }

        [Test]
        public void AboutRegisteredKeeper()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.AboutRegisteredKeeper());
        }

        [Test]
        public void AnnualMileage()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.AnnualMileage());
        }

        [Test]
        public void CoverExcesses()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.CoverExcesses());
        }

        [Test]
        public void CoverLevel()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.CoverLevel());
        }

        [Test]
        public void CoverStartDate()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.CoverStartDate());
        }

        [Test]
        public void MainDriver()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.MainDriver());
        }

        [Test]
        public void Index()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Index());
        }

        [Test]
        public void RegisteredKeeperMaritalStatus()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.RegisteredKeeperMaritalStatus());
        }

        [Test]
        public void RegisteredKeeperRelationship()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.RegisteredKeeperRelationship());
        }

        [Test]
        public void RegisteredKeeperSameAddress()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.RegisteredKeeperSameAddress());
        }

        [Test]
        public void VehicleUse()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.VehicleUse());
        }

        [Test]
        public void NcdYears()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.NcdYears());
        }

        [Test]
        public void NcdSource()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.NcdSource());
        }

        [Test]
        public void OtherVehicles()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.OtherVehicles());
        }
    }
}