using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.Halifax.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class DocumentDownloadControllerTests
    {
        private DocumentDownloadController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new DocumentDownloadController();
        }

        [Test]
        public void Document()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.Document());
        }
    }
}