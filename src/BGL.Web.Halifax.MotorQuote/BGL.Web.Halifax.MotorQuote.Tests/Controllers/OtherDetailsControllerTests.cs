using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using BGL.Web.Halifax.MotorQuote.Controllers;
using NUnit.Framework;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class OtherDetailsControllerTests
    {
        // Arrange
        private OtherDetailsController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new OtherDetailsController();
        }

        [Test]
        public void PaymentsRegularity()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.PaymentsRegularity());
        }

        [Test]
        public void AndFinally()
        {
            // Assert
            Assert.IsInstanceOf<ViewResult>(_controller.AndFinally());
        }
    }
}