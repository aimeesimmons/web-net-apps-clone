﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using NUnit.Framework;
using BGL.Web.Halifax.MotorQuote.Controllers;

namespace BGL.Web.Halifax.MotorQuote.Tests.Controllers
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class CoverFeaturesControllerTests
    {
        private CoverFeaturesController _controller;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _controller = new CoverFeaturesController();
        }

        [Test]
        public void DriveOtherCars()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.DriveOtherCars());
        }

        [Test]
        public void CourtesyCar()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.CourtesyCar());
        }

        [Test]
        public void WindscreenCover()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.WindscreenCover());
        }

        [Test]
        public void DrivingAbroad()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.DrivingAbroad());
        }

        [Test]
        public void PersonalBelongings()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.PersonalBelongings());
        }

        [Test]
        public void MedicalExpenses()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.MedicalExpenses());
        }

        [Test]
        public void PersonalAccidentBenefits()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.PersonalAccidentBenefits());
        }

        [Test]
        public void ReplacementLocks()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.ReplacementLocks());
        }

        [Test]
        public void AudioEquipment()
        {
            // Assert
            Assert.IsInstanceOf<PartialViewResult>(_controller.AudioEquipment());
        }
    }
}
