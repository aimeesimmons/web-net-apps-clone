<#
    Scans the solution folder (the parent of the .git folder) for xml config files 
    and auto-formats them to minimise the possibility of getting merge conflicts based on the ordering of 
    elements within these files.
#>

Function AutoFix-WebConfig([string] $rootDirectory)
{
    $files = Get-ChildItem -Path $rootDirectory -Filter web.config -Recurse -Depth 1

    return AutoFix-ConfigFiles($files)
}

Function AutoFix-AppConfig([string] $rootDirectory)
{
    $files = Get-ChildItem -Path $rootDirectory -Filter app.config -Recurse -Depth 1

    return AutoFix-ConfigFiles($files)
}

Function AutoFix-PackagesConfig([string] $rootDirectory)
{
    $files = Get-ChildItem -Path $rootDirectory -Filter packages.config -Recurse -Depth 1

    return AutoFix-ConfigFiles($files)
}

Function AutoFix-ConfigFiles([System.IO.FileInfo[]] $files)
{
    $modifiedfiles = @()

    foreach($file in $files)
    {
        $original = [xml] (Get-Content $file.FullName)
        $workingCopy = $original.Clone()

        if (($workingCopy.configuration.appSettings -ne $null) -and ($workingCopy.configuration.appSettings.Length -ge 2)) {
            $sorted = $workingCopy.configuration.appSettings.add | sort { [string]$_.key }
            $lastChild = $sorted[-1]
            $sorted[0..($sorted.Length-2)] | foreach {$workingCopy.configuration.appSettings.InsertBefore($_, $lastChild)} | Out-Null
        }

        if ($workingCopy.configuration.runtime.assemblyBinding -ne $null) {
            $sorted = $workingCopy.configuration.runtime.assemblyBinding.dependentAssembly | sort { [string]$_.assemblyIdentity.name }
            $lastChild = $sorted[-1]
            $sorted[0..($sorted.Length-2)] | foreach {$workingCopy.configuration.runtime.assemblyBinding.InsertBefore($_,$lastChild)} | Out-Null
        }

        if ($workingCopy.packages.package -ne $null) {
            $sorted = $workingCopy.packages.package | sort { [string]$_.id }
            $lastChild = $sorted[-1]
            $sorted[0..($sorted.Length-2)] | foreach {$workingCopy.packages.InsertBefore($_,$lastChild)} | Out-Null
        }

        $differencesCount = (Compare-Object -ReferenceObject (Select-Xml -Xml $original -XPath "//*") -DifferenceObject (Select-Xml -Xml $workingCopy -XPath "//*")).Length

        if ($differencesCount -ne 0)
        {
            $workingCopy.Save($file.FullName) | Out-Null
            $modifiedfiles += $file.FullName
        }
    }

    return $modifiedfiles
}

Function AutoFix-ApiJson([string] $rootDirectory)
{
    $files = Get-ChildItem -Path $rootDirectory -Filter api.json -Recurse -Depth 1

    $modifiedfiles = @()

    foreach($file in $files)
    {
        $original = Get-Content $file.FullName | ConvertFrom-Json

        $sortedProperties = [ordered] @{}
        Get-Member -Type NoteProperty -InputObject $original | Sort-Object Name | ForEach-Object { $sortedProperties[$_.Name] = $original.$($_.Name) }

        $workingCopy = New-Object PSCustomObject
        Add-Member -InputObject $workingCopy -NotePropertyMembers $sortedProperties

        $differencesCount = (Compare-Object -ReferenceObject ($original | ConvertTo-Json) -DifferenceObject ($workingCopy | ConvertTo-Json)).Length

        if ($differencesCount -ne 0)
        {
            Set-Content -Path $file.FullName -Value ($workingCopy | ConvertTo-Json)
            $modifiedfiles += $file.FullName
        }
    }

    return $modifiedfiles
}

Function AutoFix-CsProj([string] $rootDirectory)
{
    $files = Get-ChildItem -Path $rootDirectory -Filter *.csproj -Recurse -Depth 1
    $modifiedfiles = @()

    foreach($file in $files)
    {
        $original = [xml] (Get-Content $file.FullName)
        $workingCopy = $original.Clone()

        foreach($itemGroup in $workingCopy.Project.ItemGroup){

            # Sort the reference elements
            if ($itemGroup.Reference -ne $null){

                $sorted = $itemGroup.Reference | sort { [string]$_.Include }

                $itemGroup.RemoveAll() | Out-Null
 
                foreach($item in $sorted){
                    $itemGroup.AppendChild($item) | Out-Null
                }
            }

            # Sort the project references elements
            if ($itemGroup.ProjectReference -ne $null){

                $sorted = $itemGroup.ProjectReference | sort { [string]$_.Include }

                $itemGroup.RemoveAll() | Out-Null
 
                foreach($item in $sorted){
                    $itemGroup.AppendChild($item) | Out-Null
                }
            }

            # Sort the compile/content/folder/none elements
            if ($itemGroup.Compile -ne $null -or
                $itemGroup.Content -ne $null -or
                $itemGroup.Folder -ne $null -or
                $itemGroup.None -ne $null){

                $sortedCompile = $itemGroup.Compile | sort { [string]$_.Include }
                $sortedContent = $itemGroup.Content | sort { [string]$_.Include }
                $sortedFolder = $itemGroup.Folder | sort { [string]$_.Include }
                $sortedNone = $itemGroup.None | sort { [string]$_.Include }

                $itemGroup.RemoveAll() | Out-Null
 
                foreach($item in $sortedCompile){
                    $itemGroup.AppendChild($item) | Out-Null
                }
                foreach($item in $sortedContent){
                    $itemGroup.AppendChild($item) | Out-Null
                }
                foreach($item in $sortedFolder){
                    $itemGroup.AppendChild($item) | Out-Null
                }
                foreach($item in $sortedNone){
                    $itemGroup.AppendChild($item) | Out-Null
                }
            }
        }

        $differencesCount = (Compare-Object -ReferenceObject (Select-Xml -Xml $original -XPath "//*") -DifferenceObject (Select-Xml -Xml $workingCopy -XPath "//*")).Length

        if ($differencesCount -ne 0)
        {
            $workingCopy.Save($file.FullName) | Out-Null
            $modifiedfiles += $file.FullName
        }
    }

    return $modifiedfiles
}

Write-Host "=== Post-build event ==="
Write-Host "Auto-formatting project files..."

$rootDirectory = $pwd

$exitCode = 0;

$changedfiles = @()
$changedfiles += AutoFix-AppConfig($rootDirectory)
$changedfiles += AutoFix-PackagesConfig($rootDirectory)
# Uncomment this line when we can neatly auto-format api.json (currently the JSON outputted by PowerShell is quite ugly)
# $changedfiles += AutoFix-ApiJson($rootDirectory)
$changedfiles += AutoFix-CsProj($rootDirectory)

# Uncomment the below line if we want to start auto-formatting Web.config files
# $changedfiles += AutoFix-WebConfig($rootDirectory)

if ($changedfiles.Count -gt 0)
{
    Write-Host "The following files have been auto-formatted to reduce the likelyhood of merge conflicts:"
    
    foreach($file in $changedfiles)
    {
        Write-Host $file
    }

	# Uncomment this line if you want the build to fail if files have been changed
    # $exitCode = 1;
}

Write-Host "Finished auto-formatting project files."

exit $exitcode