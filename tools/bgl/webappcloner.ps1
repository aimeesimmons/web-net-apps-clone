﻿## TE BRAND APP CLONER
## **********************************************************************************
##
## Create a TE.Net brand application from an existing app
## 
## Expected location of this ps1 file is in WEB-NET-APPS/Tools/bgl
## 
## Before running this script please check and follow the instructions on confluence
## http://confluence.bglgroup.net/x/24LDAw
##

param(
    [parameter(Mandatory = $true)][string]$brandName,
    [parameter(Mandatory = $true)][string]$newBrandName,
    [parameter(Mandatory = $true)][string]$appName,
    [parameter(Mandatory = $true)][string]$newAppName
)

function CopyFolder {
    param(
        [parameter(Mandatory = $true)][string]$folderPath,
        [parameter(Mandatory = $true)][string]$destination
    )
    $fileCount = 0
    Get-ChildItem $folderPath -Recurse | 
        Where {$_.FullName -notlike "*\apimock\*" -and $_.FullName -notlike "*\node_modules\*" -and $_.FullName -notlike "*\packages\*"-and $_.FullName -notlike "*\bin\*"-and $_.FullName -notlike "*\obj\*"} | 
        %{
            $thisName = $_.FullName
            $fileCount = $fileCount + 1
            $target = $thisName.Replace($folderPath.Replace("./","").Replace("/", "\"), $destination.Replace("./","").Replace("/", "\"))
            Copy-Item -LiteralPath $thisName -Destination $target -Force -Verbose
        }
    Write-Host "File Count: $fileCount"
}

function CheckIsCorrectPath(){
    if(Test-Path ./web-net-apps){
        return $true;
    }
    return $false;
}

function CheckIsNotMasterBranch(){
    cd ./web-net-apps
    $check = (git rev-parse --abbrev-ref HEAD) -ne "master"
    cd ../
    return $check
}

function CheckUIResourcesExist(){
    #Write-Host "Checking for BGL.UI.$newBrandName.Resources.nupkg"
    #$uiBrandResourcePackage = .\web-net-apps\tools\Nuget\nuget.exe list BGL.UI.$newBrandName.Resources
    #return $uiBrandResourcePackage -ne "No packages found."
    return $true
}

function ReplaceInFile {
    param(
        [parameter(Mandatory = $true)][string]$filePath,
        [parameter(Mandatory = $true)][string]$searchString,
        [parameter(Mandatory = $true)][string]$replaceString
    )
    $searchRegex = '\b'+$searchString+'\b'
    (Get-Content $filePath -raw) -replace $searchRegex, $replaceString | Set-Content $filePath
    Get-Content $filePath
}

$sw = [Diagnostics.Stopwatch]::StartNew()

## Check in right place
if(CheckIsCorrectPath){
    if(CheckIsNotMasterBranch){
        if(CheckUIResourcesExist){
 
            ##Copy folders and files
            CopyFolder -folderPath "./web-net-apps/src/BGL.Web.$brandName.$appName" -destination "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName"

            ##Rename Solution
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.sln" -NewName "BGL.Web.$newBrandName.$newAppName.sln" -Force -Verbose
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.sln.DotSettings.user" -NewName "BGL.Web.$newBrandName.$newAppName.sln.DotSettings.user" -Force -Verbose

            ##Rename Folder
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName" -NewName "BGL.Web.$newBrandName.$newAppName" -Force -Verbose
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.FunctionalTests" -NewName "BGL.Web.$newBrandName.$newAppName.FunctionalTests" -Force -Verbose
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.Tests" -NewName "BGL.Web.$newBrandName.$newAppName.Tests" -Force -Verbose

            #Function Test Project
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$newBrandName.$newAppName.FunctionalTests/BGL.Web.$brandName.$appName.FunctionalTests.csproj" -NewName "BGL.Web.$newBrandName.$newAppName.FunctionalTests.csproj" -Force -Verbose

            #Test Project
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$newBrandName.$newAppName.Tests/BGL.Web.$brandName.$appName.Tests.csproj" -NewName "BGL.Web.$newBrandName.$newAppName.Tests.csproj" -Force -Verbose

            #App Project
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.csproj" -NewName "BGL.Web.$newBrandName.$newAppName.csproj" -Force -Verbose
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.csproj.user" -NewName "BGL.Web.$newBrandName.$newAppName.csproj.user" -Force -Verbose
            Rename-Item -Path "./web-net-apps/src/BGL.Web.$newBrandName.$newAppName/BGL.Web.$newBrandName.$newAppName/BGL.Web.$brandName.$appName.nuspec" -NewName "BGL.Web.$newBrandName.$newAppName.nuspec" -Force -Verbose


            ##Update File Contents
            Get-ChildItem -Path ".\web-net-apps\src\BGL.Web.$newBrandName.$newAppName" -Recurse | 
                Where-Object {
                    ($_.FullName.EndsWith(".json") -or $_.FullName.EndsWith(".cs") -or $_.FullName.EndsWith(".cshtml") -or $_.FullName.EndsWith(".config") -or $_.FullName.EndsWith(".csproj") -or $_.FullName.EndsWith(".sln")) `
                         -and $_.FullName -notlike "*\Content\*" `
                         -and $_.FullName -notlike "*\api\*" `
                         -and !$_.PSIsContainer
                } | %{ReplaceInFile -filePath $_.FullName -searchString $brandName -replaceString $newBrandName}
            Get-ChildItem -Path ".\web-net-apps\src\BGL.Web.$newBrandName.$newAppName" -Recurse | 
                Where-Object {
                    ($_.FullName.EndsWith(".json") -or $_.FullName.EndsWith(".cs") -or $_.FullName.EndsWith(".cshtml") -or $_.FullName.EndsWith(".config") -or $_.FullName.EndsWith(".csproj") -or $_.FullName.EndsWith(".sln")) `
                         -and $_.FullName -notlike "*\Content\*" `
                         -and $_.FullName -notlike "*\api\*" `
                         -and !$_.PSIsContainer
                } | %{ReplaceInFile -filePath $_.FullName -searchString "$newbrandName.$appName" -replaceString "$newBrandName.$newAppName"}

             ##Manage $newBrandName UI Resources
             $uiPackage = .\web-net-apps\tools\Nuget\nuget.exe list BGL.UI.$newBrandName.Resources | %{$_.Split(" ")}
             Get-ChildItem -Path ".\web-net-apps\src\BGL.Web.$newBrandName.$newAppName\BGL.Web.$newBrandName.$newAppName\packages.config" | %{
                   $searchStr =  "[id=""BGL.UI.$newBrandName.Resources"" version=""]([0-9]*\.[0-9]*\.[0-9]*)"
                   ReplaceInFile -filePath $_.FullName -searchString $searchStr -replaceString $uiPackage[1]
             }
            
        } else {
            Write-Host "WARNING: UI Resource does not exist for $newBrandName."
        }
    } else {
        Write-Host "WARNING: WEB-NET-APPS is currently pointing to MASTER"
    }
} else {
    Write-Host "WARNING: CANNOT FIND REQUIRED FOLDER"
}



$sw.Stop()
$sw.Elapsed