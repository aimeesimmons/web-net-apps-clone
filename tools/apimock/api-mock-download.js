#!/usr/bin/env node

var request = require('request');
var base64 = require('base-64');
var fs = require('fs-extra');
var unzip = require('unzip'); 

var zipFileName = "apimockserver.zip";
var apiMockServerFolder = 'api-experience-mock';

var bitbucketUrl = "https://bitbucket.org/BGL-IDO/api-experience-mock/get/HEAD.zip";

var bitbucketUserName = "BGL-IDO";
var bitbucketAppPassword = "Ls5iU2b6FgFP9QimVR7gssTQoiKokTkK";

var authorizationHeader = "Basic " + base64.encode(bitbucketUserName + ":" + bitbucketAppPassword);

var headers = {
    'Authorization': authorizationHeader
};

var options = {
    url: bitbucketUrl,
    method: 'GET',
    headers: headers,
    proxy: 'http://peg-proxy01:80',
    encoding: null
}

if (fs.existsSync(zipFileName)){
    fs.unlinkSync(zipFileName);
}

if (fs.existsSync(apiMockServerFolder)){
    fs.remove(apiMockServerFolder, err => {
        if (err){
            return console.log(err);
        }
    });
}

request(options, function(error, response, body){
    if (!error && response.statusCode == 200) {
        var fstream = fs.createWriteStream(zipFileName);
        fstream.write(body);
        fstream.end();

        console.log("File is downloaded!");
        console.log("Unziping file...");

        if (!fs.existsSync(apiMockServerFolder)){
            fs.mkdirSync(apiMockServerFolder);
        }

        fs.createReadStream(zipFileName).pipe(unzip.Extract({path: apiMockServerFolder}));

        console.log("Unzip success!");
    }
    else{
        console.log(error);
    }
});
